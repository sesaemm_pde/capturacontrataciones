/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.javabeans;

/**
 *
 * @author Ismael Ortiz Pulido
 */
public class CargaDeEvidenciaVista
{
    
    private String dependencia;
    private String clave;
    private String periodo;
    private String estado;
    private String estadoArchivoEvidencia;
    private String motivoArchivoEvidenciaDeclinado;
    private String fechaInicial;
    private String fechaFinal;
    private Boolean poseeUnArchivoDeEvidencia;
    private String idArchivoAsociado;

    public String getDependencia()
    {
        return dependencia;
    }

    public void setDependencia(String dependencia)
    {
        this.dependencia = dependencia;
    }

    public String getClave()
    {
        return clave;
    }

    public void setClave(String clave)
    {
        this.clave = clave;
    }

    public String getPeriodo()
    {
        return periodo;
    }

    public void setPeriodo(String periodo)
    {
        this.periodo = periodo;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public String getEstadoArchivoEvidencia()
    {
        return estadoArchivoEvidencia;
    }

    public void setEstadoArchivoEvidencia(String estadoArchivoEvidencia)
    {
        this.estadoArchivoEvidencia = estadoArchivoEvidencia;
    }

    public String getMotivoArchivoEvidenciaDeclinado()
    {
        return motivoArchivoEvidenciaDeclinado;
    }

    public void setMotivoArchivoEvidenciaDeclinado(String motivoArchivoEvidenciaDeclinado)
    {
        this.motivoArchivoEvidenciaDeclinado = motivoArchivoEvidenciaDeclinado;
    }
    
    public String getFechaInicial()
    {
        return fechaInicial;
    }

    public void setFechaInicial(String fechaInicial)
    {
        this.fechaInicial = fechaInicial;
    }

    public String getFechaFinal()
    {
        return fechaFinal;
    }

    public void setFechaFinal(String fechaFinal)
    {
        this.fechaFinal = fechaFinal;
    }

    public Boolean getPoseeUnArchivoDeEvidencia()
    {
        return poseeUnArchivoDeEvidencia;
    }

    public void setPoseeUnArchivoDeEvidencia(Boolean poseeUnArchivoDeEvidencia)
    {
        this.poseeUnArchivoDeEvidencia = poseeUnArchivoDeEvidencia;
    }

    public String getIdArchivoAsociado()
    {
        return idArchivoAsociado;
    }

    public void setIdArchivoAsociado(String idArchivoAsociado)
    {
        this.idArchivoAsociado = idArchivoAsociado;
    }

}
