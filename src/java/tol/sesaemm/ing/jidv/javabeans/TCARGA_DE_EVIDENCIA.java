/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.javabeans;

import org.bson.types.ObjectId;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class TCARGA_DE_EVIDENCIA
{
    private ObjectId _id;
    private String archivo;
    private String fecha_registro;
    private String fecha_modificacion;
    private String fecha_aprobacion;
    private String observaciones;
    private String usuario;
    private String dependencia;
    private String sistema;
    private String status;
    private String usuario_aprobacion;
    private String motivo_archivo_declinado;
    private CARGA_DE_EVIDENCIA_PERIODO periodo;

    public ObjectId getId()
    {
        return _id;
    }

    public void setId(ObjectId _id)
    {
        this._id = _id;
    }

    public String getArchivo()
    {
        return archivo;
    }

    public void setArchivo(String archivo)
    {
        this.archivo = archivo;
    }

    public String getFecha_registro()
    {
        return fecha_registro;
    }

    public void setFecha_registro(String fecha_registro)
    {
        this.fecha_registro = fecha_registro;
    }

    public String getFecha_modificacion()
    {
        return fecha_modificacion;
    }

    public void setFecha_modificacion(String fecha_modificacion)
    {
        this.fecha_modificacion = fecha_modificacion;
    }

    public String getFecha_aprobacion()
    {
        return fecha_aprobacion;
    }

    public void setFecha_aprobacion(String fecha_aprobacion)
    {
        this.fecha_aprobacion = fecha_aprobacion;
    }

    public String getObservaciones()
    {
        return observaciones;
    }

    public void setObservaciones(String observaciones)
    {
        this.observaciones = observaciones;
    }

    public String getUsuario()
    {
        return usuario;
    }

    public void setUsuario(String usuario)
    {
        this.usuario = usuario;
    }

    public String getDependencia()
    {
        return dependencia;
    }

    public void setDependencia(String dependencia)
    {
        this.dependencia = dependencia;
    }

    public String getSistema()
    {
        return sistema;
    }

    public void setSistema(String sistema)
    {
        this.sistema = sistema;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getUsuario_aprobacion()
    {
        return usuario_aprobacion;
    }

    public void setUsuario_aprobacion(String usuario_aprobacion)
    {
        this.usuario_aprobacion = usuario_aprobacion;
    }

    public String getMotivo_archivo_declinado()
    {
        return motivo_archivo_declinado;
    }

    public void setMotivo_archivo_declinado(String motivo_archivo_declinado)
    {
        this.motivo_archivo_declinado = motivo_archivo_declinado;
    }

    public CARGA_DE_EVIDENCIA_PERIODO getPeriodo()
    {
        return periodo;
    }

    public void setPeriodo(CARGA_DE_EVIDENCIA_PERIODO periodo)
    {
        this.periodo = periodo;
    }

}
