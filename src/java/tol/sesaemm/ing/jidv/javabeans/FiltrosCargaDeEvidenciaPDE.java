/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.javabeans;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class FiltrosCargaDeEvidenciaPDE
{
    private int periodo;
    private String dependencia;
    private String clave;
    private String fechaInicial;
    private String fechaFinal;
    private int anioActual;
    private String estadoGeneral;

    public int getPeriodo()
    {
        return periodo;
    }

    public void setPeriodo(int periodo)
    {
        this.periodo = periodo;
    }

    public String getDependencia()
    {
        return dependencia;
    }

    public void setDependencia(String dependencia)
    {
        this.dependencia = dependencia;
    }

    public String getFechaInicial()
    {
        return fechaInicial;
    }

    public void setFechaInicial(String fechaInicial)
    {
        this.fechaInicial = fechaInicial;
    }

    public String getFechaFinal()
    {
        return fechaFinal;
    }

    public void setFechaFinal(String fechaFinal)
    {
        this.fechaFinal = fechaFinal;
    }

    public int getAnioActual() {
        return anioActual;
    }

    public void setAnioActual(int anioActual) {
        this.anioActual = anioActual;
    }

    public String getEstadoGeneral() {
        return estadoGeneral;
    }

    public void setEstadoGeneral(String estadoGeneral) {
        this.estadoGeneral = estadoGeneral;
    }

    public String getClave()
    {
        return clave;
    }

    public void setClave(String clave)
    {
        this.clave = clave;
    }

}
