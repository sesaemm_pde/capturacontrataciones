/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.javabeans.s6contrataciones;

/**
 *
 * @author Ismael Ortiz ismael.ortizqsesaemm.org.mx
 */
public class TIPO_DOCUMENTO_CONTRATACIONES
{
    
    private String categoria;
    private String seccion;
    private String clave;
    private String valor;

    public String getCategoria()
    {
        return categoria;
    }

    public void setCategoria(String categoria)
    {
        this.categoria = categoria;
    }

    public String getSeccion()
    {
        return seccion;
    }

    public void setSeccion(String seccion)
    {
        this.seccion = seccion;
    }

    public String getClave()
    {
        return clave;
    }

    public void setClave(String clave)
    {
        this.clave = clave;
    }

    public String getValor()
    {
        return valor;
    }

    public void setValor(String valor)
    {
        this.valor = valor;
    }
    
}
