/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.javabeans.s6contrataciones;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class FiltrosDeBusquedaContratacion {
    
    private Integer ciclo;
    private String publicador;
    private String dependencia;
    private String idReferencia;
    int numeroPagina;       
    int registrosMostrar;
    String orden;
    String origen;
    boolean busquedaGeneral;

    public Integer getCiclo() {
        return ciclo;
    }

    public void setCiclo(Integer ciclo) {
        this.ciclo = ciclo;
    }

    public String getPublicador() {
        return publicador;
    }

    public void setPublicador(String publicador) {
        this.publicador = publicador;
    }

    public String getDependencia() {
        return dependencia;
    }

    public void setDependencia(String dependencia) {
        this.dependencia = dependencia;
    }

    public String getIdReferencia() {
        return idReferencia;
    }

    public void setIdReferencia(String idReferencia) {
        this.idReferencia = idReferencia;
    }

    public int getNumeroPagina() {
        return numeroPagina;
    }

    public void setNumeroPagina(int numeroPagina) {
        this.numeroPagina = numeroPagina;
    }

    public int getRegistrosMostrar() {
        return registrosMostrar;
    }

    public void setRegistrosMostrar(int registrosMostrar) {
        this.registrosMostrar = registrosMostrar;
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public boolean isBusquedaGeneral() {
        return busquedaGeneral;
    }

    public void setBusquedaGeneral(boolean busquedaGeneral) {
        this.busquedaGeneral = busquedaGeneral;
    }

}