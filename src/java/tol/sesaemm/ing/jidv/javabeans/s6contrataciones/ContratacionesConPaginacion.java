/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.javabeans.s6contrataciones;

import java.util.ArrayList;
import tol.sesaemm.ing.jidv.javabeans.PAGINACION;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ContratacionesConPaginacion
{

    ArrayList<ContratacionesVista> listaDeContrataciones;
    PAGINACION filtroPaginacion;

    public ArrayList<ContratacionesVista> getListaDeContrataciones()
    {
        return listaDeContrataciones;
    }

    public void setListaDeContrataciones(ArrayList<ContratacionesVista> listaDeContrataciones)
    {
        this.listaDeContrataciones = listaDeContrataciones;
    }

    public PAGINACION getFiltroPaginacion()
    {
        return filtroPaginacion;
    }

    public void setFiltroPaginacion(PAGINACION filtroPaginacion)
    {
        this.filtroPaginacion = filtroPaginacion;
    }

}
