package tol.sesaemm.ing.jidv.javabeans;

import java.util.ArrayList;

/**
 *
 * @author Ismael Ortiz Pulido
 */
public class TBITACORA_BUSQUEDAS_PDE 
{

    private String fechaConsulta;
    private String sistema;
    private String privacidadSistema;
    private ArrayList<FiltrosBusquedaPDE> filtrosDeBusqueda;
    private FiltrosBusquedaPDE filtroDeOrdenamiento;

    public TBITACORA_BUSQUEDAS_PDE(){}

    public TBITACORA_BUSQUEDAS_PDE(String fechaConsulta, String sistema, String privacidadSistema, ArrayList<FiltrosBusquedaPDE> filtrosDeBusqueda, FiltrosBusquedaPDE filtroDeOrdenamiento) {
        this.fechaConsulta = fechaConsulta;
        this.sistema = sistema;
        this.privacidadSistema = privacidadSistema;
        this.filtrosDeBusqueda = filtrosDeBusqueda;
        this.filtroDeOrdenamiento = filtroDeOrdenamiento;
    }
    
    public String getFechaConsulta() {
        return fechaConsulta;
    }

    public void setFechaConsulta(String fechaConsulta) {
        this.fechaConsulta = fechaConsulta;
    }

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public ArrayList<FiltrosBusquedaPDE> getFiltrosDeBusqueda() {
        return filtrosDeBusqueda;
    }

    public void setFiltrosDeBusqueda(ArrayList<FiltrosBusquedaPDE> filtrosDeBusqueda) {
        this.filtrosDeBusqueda = filtrosDeBusqueda;
    }

    public FiltrosBusquedaPDE getFiltroDeOrdenamiento() {
        return filtroDeOrdenamiento;
    }

    public void setFiltroDeOrdenamiento(FiltrosBusquedaPDE filtroDeOrdenamiento) {
        this.filtroDeOrdenamiento = filtroDeOrdenamiento;
    }

    public String getPrivacidadSistema() {
        return privacidadSistema;
    }

    public void setPrivacidadSistema(String privacidadSistema) {
        this.privacidadSistema = privacidadSistema;
    }
    
}
