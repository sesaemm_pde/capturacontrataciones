/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.javabeans;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class CARGA_DE_EVIDENCIA_PERIODO
{
    private String fechaInicial;
    private String fechaFinal;

    public String getFechaInicial()
    {
        return fechaInicial;
    }

    public void setFechaInicial(String fechaInicial)
    {
        this.fechaInicial = fechaInicial;
    }

    public String getFechaFinal()
    {
        return fechaFinal;
    }

    public void setFechaFinal(String fechaFinal)
    {
        this.fechaFinal = fechaFinal;
    }

}
