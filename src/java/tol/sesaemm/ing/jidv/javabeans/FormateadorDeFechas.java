/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.javabeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Cristian Luna cristian.luna@sesaemm.org.mx
 */
public class FormateadorDeFechas
{    
    public FormateadorDeFechas(){}
    
    public String obtenerFormatoFechaDescriptivaDeFormatoISO8601(String strFecha) throws Exception
    { // funcion que obtiene el formato de fecha dd de mm de aaaa a partir de una fecha en formato ISO 8601 (TOP)
        
        // arreglo que almacena los meses del anio
        String arregloMeses[] = {"", "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"};
        // formateador para fechas en formato dd-mm-aaaa
        SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
        // arreglo que almacena en cada posicion los elementos de una fecha en ISO 8601 a dd/mm/aaaa
        String arregloElementosFecha[];
        // variable que almacena el formato de la fecha de salida dd/mm/aaaa
        String strFechaConFormato;
        // fecha original
        Date fechaFormatoOriginal;
        // formateador de la fecha original
        DateFormat formatoFecha;
        
        // crear objeto de la fecha en formato ISO 8601 (TOP)
        formatoFecha = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        fechaFormatoOriginal = formatoFecha.parse(strFecha);
        // crear objeto de la fecha en formato ISO 8601 (BOTTOM)
        
        // construir fecha en formato ISO 8601 a dd de mm de aaaa (TOP)
        strFechaConFormato = formateador.format(fechaFormatoOriginal);
        arregloElementosFecha = strFechaConFormato.split("-");
        strFechaConFormato =  arregloElementosFecha[0] + " DE " + arregloMeses[Integer.parseInt(arregloElementosFecha[1])] + " DE " + arregloElementosFecha[2];      
        // construir fecha en formato ISO 8601 a dd de mm de aaaa (BOTTOM)
        
        return strFechaConFormato;
    } // funcion que obtiene el formato de fecha dd de mm de aaaa a partir de una fecha en formato ISO 8601 (BOTTOM)
    
    public String obtenerFormatoFechaDiaMesAnioDeFormatoISO8601(String strFecha) throws Exception
    { // funcion que obtiene el formato de fecha en dd-mm-aaaa a partir de una fecha en formato ISO 8601 (TOP)
        
        // formateador para fechas en formato dd-mm-aaaa
        SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
        // variable que almacena el formato de la fecha de salida dd/mm/aaaa
        String strFechaConFormato;
        // fecha original
        Date fechaFormatoOriginal;
        // formateador de la fecha original
        DateFormat formatoFecha;
        
        // crear objeto de la fecha en formato ISO 8601 (TOP)
        formatoFecha = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        fechaFormatoOriginal = formatoFecha.parse(strFecha);
        // crear objeto de la fecha en formato ISO 8601 (BOTTOM)
        
        // construir fecha en formato ISO 8601 a dd-mm-aaaa (TOP)
        strFechaConFormato = formateador.format(fechaFormatoOriginal);
        // construir fecha en formato ISO 8601 a dd-mm-aaaa (BOTTOM)
        
        return strFechaConFormato;
    } // funcion que obtiene el formato de fecha en dd-mm-aaaa a partir de una fecha en formato ISO 8601 (BOTTOM)
    
    public String obtenerFormatoFechaDescriptivaDiaMesAnio(String strFecha) throws Exception
    { // funcion que obtiene el formato de fecha dd de mm de aaaa a partir de una fecha en formato aaaa-mm-dd (TOP)
        
        // arreglo que almacena los meses del anio
        String arregloMeses[] = {"", "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"};
        // formateador para fechas en formato dd-mm-aaaa
        SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
        // arreglo que almacena en cada posicion los elementos de una fecha en ISO 8601 a dd/mm/aaaa
        String arregloElementosFecha[];
        // variable que almacena el formato de la fecha de salida dd/mm/aaaa
        String strFechaConFormato;
        // fecha original
        Date fechaFormatoOriginal;
        // formateador de la fecha original
        DateFormat formatoFecha;
        
        // crear objeto de la fecha en formato aaaa-mm-dd (TOP)
        formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
        fechaFormatoOriginal = formatoFecha.parse(strFecha);
        // crear objeto de la fecha en formato aaaa-mm-dd (BOTTOM)
        
        // construir fecha en formato aaaa-mm-dd a dd de mm de aaaa (TOP)
        strFechaConFormato = formateador.format(fechaFormatoOriginal);
        arregloElementosFecha = strFechaConFormato.split("-");
        strFechaConFormato =  arregloElementosFecha[0] + " DE " + arregloMeses[Integer.parseInt(arregloElementosFecha[1])] + " DE " + arregloElementosFecha[2];      
        // construir fecha en formato aaaa-mm-dd a dd de mm de aaaa (BOTTOM)
        
        return strFechaConFormato;
    } // funcion que obtiene el formato de fecha dd de mm de aaaa a partir de una fecha en formato aaaa-mm-dd (BOTTOM)
    
    public String obtenerFormatoFechaFormatoISO8601DeAnioMesDia(String strFecha) throws Exception
    { // funcion que obtiene el formato de fecha en formato ISO 8601 a partir de una fecha en formato aaaa-mm-dd (TOP)
        
        // formateador para fechas en formato ISO 8601
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        // variable que almacena el formato de la fecha de salida en formato ISO 8601
        String strFechaConFormato;
        // fecha original
        Date fechaFormatoOriginal;
        // formateador de la fecha original
        DateFormat formatoFecha;
        
        // crear objeto de la fecha en formato yyyy-MM-dd (TOP)
        formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
        fechaFormatoOriginal = formatoFecha.parse(strFecha);
        // crear objeto de la fecha en formato yyyy-MM-dd (BOTTOM)
        
        // construir fecha en formato yyyy-MM-dd a ISO 8601 (TOP)
        strFechaConFormato = formateador.format(fechaFormatoOriginal);
        // construir fecha en formato yyyy-MM-dd a ISO 8601 (BOTTOM)
        
        return strFechaConFormato;
        
    } // funcion que obtiene el formato de fecha en formato ISO 8601 a partir de una fecha en formato aaaa-mm-dd (BOTTOM)
 
    public String obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(String strFecha)
    { // funcion que obtiene el formato de fecha en formato ISO 8601 a partir de una fecha en formato aaaa-mm-dd (TOP)
        
        String strFechaConFormato = "";
        String[] arregloPartesFecha;
        
        if(strFecha != null)
        {
            if(strFecha.equals("") == false)
            {
                if(strFecha.contains("T") == true)
                { // construir fecha en formato yyyy-MM-dd de fecha ISO 8601 (TOP)
                    arregloPartesFecha = strFecha.split("T");
                    strFechaConFormato = arregloPartesFecha[0];
                } // construir fecha en formato yyyy-MM-dd de fecha ISO 8601 (BOTTOM)
            }
        }
        
        return strFechaConFormato;
        
    } // funcion que obtiene el formato de fecha en formato ISO 8601 a partir de una fecha en formato aaaa-mm-dd (BOTTOM)
}