/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.javabeans;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class TSEGUIMIENTO_CARGA_DE_EVIDENCIA
{
    
    private String clave;
    private String siglas;
    private String dependencia;
    private String fechaInicioConvenio;

    public String getClave()
    {
        return clave;
    }

    public void setClave(String clave)
    {
        this.clave = clave;
    }

    public String getSiglas()
    {
        return siglas;
    }

    public void setSiglas(String siglas)
    {
        this.siglas = siglas;
    }

    public String getDependencia()
    {
        return dependencia;
    }

    public void setDependencia(String dependencia)
    {
        this.dependencia = dependencia;
    }

    public String getFechaInicioConvenio()
    {
        return fechaInicioConvenio;
    }

    public void setFechaInicioConvenio(String fechaInicioConvenio)
    {
        this.fechaInicioConvenio = fechaInicioConvenio;
    }
    
}
