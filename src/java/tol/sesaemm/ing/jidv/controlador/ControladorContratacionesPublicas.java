/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.controlador;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import tol.sesaemm.funciones.BitacoraDeBusquedasPDE;
import tol.sesaemm.funciones.ManejoDeFechas;
import tol.sesaemm.ing.jidv.config.ConfVariables;
import tol.sesaemm.ing.jidv.javabeans.FiltrosBusquedaPDE;
import tol.sesaemm.ing.jidv.javabeans.Miga;
import tol.sesaemm.ing.jidv.javabeans.NIVEL_ACCESO;
import tol.sesaemm.ing.jidv.javabeans.PAGINACION;
import tol.sesaemm.ing.jidv.javabeans.TBITACORA_BUSQUEDAS_PDE;
import tol.sesaemm.ing.jidv.javabeans.USUARIO;
import tol.sesaemm.ing.jidv.javabeans.FormateadorDeFechas;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ContratacionesConPaginacion;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ContratacionesVista;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.FiltrosDeBusquedaContratacion;
import tol.sesaemm.ing.jidv.logicaDeNegocio.LogicaDeNegocioS6V01;
import tol.sesaemm.ing.jidv.logicaDeNegocio.LogicaDeNegocioV01;
import tol.sesaemm.javabeans.ENTE_PUBLICO;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesContratacion;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class ControladorContratacionesPublicas extends tol.sesaemm.ing.jidv.controlador.ControladorBase
{

    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException
    {
        String strAccion = "";
        LogicaDeNegocioS6V01 LNS6 = new LogicaDeNegocioS6V01();
        HttpSession sesion = request.getSession();
        USUARIO usuario;

        usuario = (USUARIO) sesion.getAttribute("usuario");
        strAccion = request.getParameter("accion");
       
        try
        {
            if (strAccion == null)
            {
                if (sesion != null)
                {
                    sesion.invalidate();
                    sesion = null;
                }

                this.fordward(request, resp, "/frmPrincipal.jsp");
            }
            else if (usuario != null)
            {

                ArrayList<Miga> migas;                                                                 
                ArrayList<ContratacionesVista> contrataciones;                                         
                FiltrosDeBusquedaContratacion filtroBusquedaOrdenamiento;                              
                TBITACORA_BUSQUEDAS_PDE bitacoraDeBusquedaPDE;                                           
                ArrayList<FiltrosBusquedaPDE> listaFiltrosUtilizadosEnBusqueda;                                
                PAGINACION paginacion;                                                                 
                ContratacionesConPaginacion objetos;                                                   
                ContratacionesContratacion contratacion;                                                             
                ArrayList<String> dependencias;                                                        
                LogicaDeNegocioV01 LN;                                                                 
                NIVEL_ACCESO nivelAcceso;                                                              
                DecimalFormat formateador;                                                             
                FormateadorDeFechas formateadorDeFechas;                                               
                ArrayList<ENTE_PUBLICO> dependenciasUsuario;
                String mensaje;
                String idContratacion;
                        
                LN = new LogicaDeNegocioV01();
                nivelAcceso = LN.tipoDeUsuario(usuario, 6);
                
                switch (strAccion)
                {
                    case "ingresarSistema":
                    {
                        migas = new ArrayList<>();
                        paginacion = new PAGINACION();
                       
                        migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                        migas.add(new Miga("Contrataciones", "", "contrataciones"));
                       
                        request.setAttribute("migas", migas);
                        request.setAttribute("paginacion", paginacion);
                        request.setAttribute("nivelDeAcceso", nivelAcceso);
                        this.fordward(request, resp, "s6Contrataciones/frmPaginaPrincipalContrataciones.jsp");
                        break;
                    }
                    case "consultarContrataciones":
                    {
                       
                        migas = new ArrayList<>();
                        migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                        migas.add(new Miga("Contrataciones", "", "contrataciones"));
                       
                       
                        dependenciasUsuario = LNS6.obtenerDependenciasUsuario(usuario);
                       
                       
                        filtroBusquedaOrdenamiento = LNS6.modelarRequestParaConsulta(request);
                       
                       
                        bitacoraDeBusquedaPDE = new TBITACORA_BUSQUEDAS_PDE();
                        bitacoraDeBusquedaPDE.setFechaConsulta(new ManejoDeFechas().getDateNowAsString());
                        bitacoraDeBusquedaPDE.setSistema(BitacoraDeBusquedasPDE.contrataciones);
                        bitacoraDeBusquedaPDE.setPrivacidadSistema("privado");
                        bitacoraDeBusquedaPDE.setFiltroDeOrdenamiento(new FiltrosBusquedaPDE(filtroBusquedaOrdenamiento.getOrden(), "asc"));
                       
                        listaFiltrosUtilizadosEnBusqueda = new ArrayList<>();
                        listaFiltrosUtilizadosEnBusqueda.add(new FiltrosBusquedaPDE("ciclo", filtroBusquedaOrdenamiento.getCiclo().toString()));
                        listaFiltrosUtilizadosEnBusqueda.add(new FiltrosBusquedaPDE("publicador", filtroBusquedaOrdenamiento.getPublicador()));
                        listaFiltrosUtilizadosEnBusqueda.add(new FiltrosBusquedaPDE("dependencia", filtroBusquedaOrdenamiento.getDependencia()));
                        bitacoraDeBusquedaPDE.setFiltrosDeBusqueda(listaFiltrosUtilizadosEnBusqueda);
                       
                        BitacoraDeBusquedasPDE.registrarBitacoraDeBusquedasPDE(bitacoraDeBusquedaPDE);
                       
                       
                        paginacion = new PAGINACION();
                        contrataciones = new ArrayList<>();

                        objetos = LNS6.obtenerContratacionesPublicas(usuario, nivelAcceso, dependenciasUsuario, filtroBusquedaOrdenamiento);
                        if (objetos != null)
                        {
                            paginacion = objetos.getFiltroPaginacion();
                            contrataciones = objetos.getListaDeContrataciones();
                        }
                       
                        request.setAttribute("contrataciones", contrataciones);
                        request.setAttribute("paginacion", paginacion);
                        request.setAttribute("migas", migas);
                        request.setAttribute("filtros", filtroBusquedaOrdenamiento);
                        request.setAttribute("nivelDeAcceso", nivelAcceso);
                        request.setAttribute("irASeccion", true);
                        this.fordward(request, resp, "s6Contrataciones/frmPaginaPrincipalContrataciones.jsp");
                        break;
                    }
                    case "verDetalleDeContratacion":
                    {
                       
                        migas = new ArrayList<>();
                        migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                        migas.add(new Miga("Contrataciones", "ingresarSistema", "s_contrataciones"));
                        migas.add(new Miga("Detalle contrataci&oacute;n", ""));
                       
                       
                        filtroBusquedaOrdenamiento = LNS6.modelarRequestParaConsulta(request);
                       
                       
                        formateador = new DecimalFormat("###,###.##");
                        formateadorDeFechas = new FormateadorDeFechas();
                        paginacion = new PAGINACION();
                       
                       
                        contratacion = LNS6.obtenerContratacion(filtroBusquedaOrdenamiento.getIdReferencia());
                       
                        request.setAttribute("migas", migas);
                        request.setAttribute("filtros", filtroBusquedaOrdenamiento);
                        request.setAttribute("paginacion", paginacion);
                        request.setAttribute("contratacion", contratacion);
                        request.setAttribute("formateador", formateador);
                        request.setAttribute("formateadorDeFechas", formateadorDeFechas);
                        request.setAttribute("nivelDeAcceso", nivelAcceso);
                        this.fordward(request, resp, "s6Contrataciones/frmDetalleContratacion.jsp");
                        break;
                    }
                    case "eliminarContratacion":
                    {
                       
                        migas = new ArrayList<>();
                        migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                        migas.add(new Miga("Contrataciones", "", "contrataciones"));
                       
                       
                        dependenciasUsuario = LNS6.obtenerDependenciasUsuario(usuario);
                       
                       
                        idContratacion = (request.getParameter("txtID") == null) ? "" : request.getParameter("txtID");
                        contratacion = LNS6.obtenerContratacion(idContratacion);
                        mensaje = LNS6.eliminarContratacion(idContratacion, usuario, nivelAcceso, contratacion);
                       
                       
                        filtroBusquedaOrdenamiento = LNS6.modelarRequestParaConsulta(request);
                       
                       
                        paginacion = new PAGINACION();
                        contrataciones = new ArrayList<>();

                        objetos = LNS6.obtenerContratacionesPublicas(usuario, nivelAcceso, dependenciasUsuario, filtroBusquedaOrdenamiento);
                        if (objetos != null)
                        {
                            paginacion = objetos.getFiltroPaginacion();
                            contrataciones = objetos.getListaDeContrataciones();
                        }
                       
                        request.setAttribute("contrataciones", contrataciones);
                        request.setAttribute("paginacion", paginacion);
                        request.setAttribute("migas", migas);
                        request.setAttribute("filtros", filtroBusquedaOrdenamiento);
                        request.setAttribute("nivelDeAcceso", nivelAcceso);
                        request.setAttribute("irASeccion", true);
                        request.setAttribute("mostrarMensaje", true);
                        request.setAttribute("mensaje", mensaje);
                        this.fordward(request, resp, "s6Contrataciones/frmPaginaPrincipalContrataciones.jsp");
                        break;
                    }
                    case "publicarContratacion":
                    {
                       
                        migas = new ArrayList<>();
                        migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                        migas.add(new Miga("Contrataciones", "", "contrataciones"));
                       
                       
                        dependenciasUsuario = LNS6.obtenerDependenciasUsuario(usuario);
                       
                       
                        idContratacion = (request.getParameter("txtID") == null) ? "" : request.getParameter("txtID");
                        contratacion = LNS6.obtenerContratacion(idContratacion);
                        mensaje = LNS6.publicarContratacion(idContratacion, usuario, nivelAcceso, contratacion);
                       
                       
                        filtroBusquedaOrdenamiento = LNS6.modelarRequestParaConsulta(request);
                       
                       
                        paginacion = new PAGINACION();
                        contrataciones = new ArrayList<>();

                        objetos = LNS6.obtenerContratacionesPublicas(usuario, nivelAcceso, dependenciasUsuario, filtroBusquedaOrdenamiento);
                        if (objetos != null)
                        {
                            paginacion = objetos.getFiltroPaginacion();
                            contrataciones = objetos.getListaDeContrataciones();
                        }
                       
                        request.setAttribute("contrataciones", contrataciones);
                        request.setAttribute("paginacion", paginacion);
                        request.setAttribute("migas", migas);
                        request.setAttribute("filtros", filtroBusquedaOrdenamiento);
                        request.setAttribute("nivelDeAcceso", nivelAcceso);
                        request.setAttribute("irASeccion", true);
                        request.setAttribute("mostrarMensaje", true);
                        request.setAttribute("mensaje", mensaje);
                        this.fordward(request, resp, "s6Contrataciones/frmPaginaPrincipalContrataciones.jsp");
                        break;
                    }
                    case "despublicarContratacion":
                    {
                       
                        migas = new ArrayList<>();
                        migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                        migas.add(new Miga("Contrataciones", "", "contrataciones"));
                       
                       
                        dependenciasUsuario = LNS6.obtenerDependenciasUsuario(usuario);
                       
                       
                        idContratacion = (request.getParameter("txtID") == null) ? "" : request.getParameter("txtID");
                        contratacion = LNS6.obtenerContratacion(idContratacion);
                        mensaje = LNS6.despublicarContratacion(idContratacion, usuario, nivelAcceso, contratacion);
                       
                       
                        filtroBusquedaOrdenamiento = LNS6.modelarRequestParaConsulta(request);
                       
                       
                        paginacion = new PAGINACION();
                        contrataciones = new ArrayList<>();

                        objetos = LNS6.obtenerContratacionesPublicas(usuario, nivelAcceso, dependenciasUsuario, filtroBusquedaOrdenamiento);
                        if (objetos != null)
                        {
                            paginacion = objetos.getFiltroPaginacion();
                            contrataciones = objetos.getListaDeContrataciones();
                        }
                       
                        request.setAttribute("contrataciones", contrataciones);
                        request.setAttribute("paginacion", paginacion);
                        request.setAttribute("migas", migas);
                        request.setAttribute("filtros", filtroBusquedaOrdenamiento);
                        request.setAttribute("nivelDeAcceso", nivelAcceso);
                        request.setAttribute("irASeccion", true);
                        request.setAttribute("mostrarMensaje", true);
                        request.setAttribute("mensaje", mensaje);
                        this.fordward(request, resp, "s6Contrataciones/frmPaginaPrincipalContrataciones.jsp");
                        break;
                    }
                    case "mostrarPaginaRegistroNuevaContratacion":
                    {
                       
                        migas = new ArrayList<>();
                        migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                        migas.add(new Miga("Contrataciones", "ingresarSistema", "s_contrataciones"));
                        migas.add(new Miga("Registro de contrataci&oacute;n", ""));
                       
                       
                        filtroBusquedaOrdenamiento = LNS6.modelarRequestParaConsulta(request);
                        paginacion = new PAGINACION();
                       
                       
                        idContratacion = (request.getParameter("txtID") == null) ? "" : request.getParameter("txtID");
                        
                        if(LNS6.existeContratacion(idContratacion) == true)
                        {
                            contratacion = LNS6.obtenerContratacion(idContratacion);
                            request.setAttribute("idContratacion", contratacion.getIdSpdn());
                            request.setAttribute("estadoSeccionesContratacion", contratacion.getEstado_secciones_contratacion());
                            request.setAttribute("seContinuaRegistro", true);
                        }
                        else
                        {
                            request.setAttribute("seContinuaRegistro", false);
                        }
                       
                        request.setAttribute("migas", migas);
                        request.setAttribute("filtros", filtroBusquedaOrdenamiento);
                        request.setAttribute("paginacion", paginacion);
                        request.setAttribute("nivelDeAcceso", nivelAcceso);
                        this.fordward(request, resp, "s6Contrataciones/frmPaginaInicialRegistroContratacion.jsp");
                        break;
                    }
                    case "mostrarPaginaEditarContratacion":
                    {
                        
                        int seccionConFoco;
                        int bloqueConFoco;
                        
                       
                        migas = new ArrayList<>();
                        migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                        migas.add(new Miga("Contrataciones", "ingresarSistema", "s_contrataciones"));
                        migas.add(new Miga("Edici&oacute;n de contrataci&oacute;n", ""));
                       
                       
                        filtroBusquedaOrdenamiento = LNS6.modelarRequestParaConsulta(request);
                        paginacion = new PAGINACION();
                       
                        
                        idContratacion = (request.getParameter("txtID") == null) ? "" : request.getParameter("txtID");
                        seccionConFoco = (request.getParameter("txtSeccionConFoco") == null) ? 1 : Integer.parseInt(request.getParameter("txtSeccionConFoco"));
                        bloqueConFoco = (request.getParameter("txtBloqueConFoco") == null) ? 0 : Integer.parseInt(request.getParameter("txtBloqueConFoco"));
                        
                       
                        switch(seccionConFoco)
                        {
                            case 1: 
                               
                                request.setAttribute("tiposDeInicio", LNS6.obtenerTipoInicio());
                                request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                               
                                break;
                            case 3:
                               
                                request.setAttribute("etiquetasDeEntrega", LNS6.obtenerEtiquetasDeEntrega());
                               
                                break;
                            case 4:
                               
                                request.setAttribute("rolesPartesInvolucradas", LNS6.obtenerRolesPartesInvolucradas());
                                request.setAttribute("escalasPartesInvolucradas", LNS6.obtenerEscalasPartesInvolucradas());
                               
                                break;
                            case 5:
                               
                                request.setAttribute("dependenciasUsuario", LNS6.obtenerEntesPublicos(usuario.getDependencias(), usuario));
                               
                                break;
                            case 6:
                               
                               
                                if(bloqueConFoco == 2)
                                {
                                    request.setAttribute("tipoMoneda", LNS6.obtenerTipoDeMoneda());
                                }
                               
                               
                                if(bloqueConFoco == 3)
                                {
                                    request.setAttribute("tiposDeDocumentoPlaneacion", LNS6.obtenerTipoDocumento(6, 0));
                                    request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                                }
                               
                               
                                if(bloqueConFoco == 4)
                                {
                                    request.setAttribute("tiposDeDocumentoPlaneacion", LNS6.obtenerTipoDocumento(6, 0));
                                    request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                                    request.setAttribute("tiposDeHito", LNS6.obtenerTipoHito());
                                    request.setAttribute("estadosHito", LNS6.obtenerEstadoHito());
                                }
                               
                               
                                break;
                            case 7:
                               
                               
                                if(bloqueConFoco == 1)
                                {
                                    request.setAttribute("tiposDePresentacionDeMetodos", LNS6.obtenerMetodosDePresentacion());
                                    request.setAttribute("criterioDeAdjudicacion", LNS6.obtenerCriteriosDeAdjudicacion());
                                    request.setAttribute("categoriasPrincipales", LNS6.obtenerCategoriasPrincipales());
                                    request.setAttribute("categoriasAdicionales", LNS6.obtenerCategoriasAdicionales());
                                    request.setAttribute("metodosLicitacion", LNS6.obtenerMetodoContratacion());
                                    request.setAttribute("estadosLicitacion", LNS6.obtenerEstadoLicitacion());
                                    request.setAttribute("tipoMoneda", LNS6.obtenerTipoDeMoneda());
                                }
                               
                               
                                if(bloqueConFoco == 6)
                                {
                                    request.setAttribute("esquemasDeClasificacion", LNS6.obtenerEsquemaClasificacion());
                                    request.setAttribute("esquemasDeUnidad", LNS6.obtenerEsquemaUnidad());
                                    request.setAttribute("tipoMoneda", LNS6.obtenerTipoDeMoneda());
                                }
                               
                               
                                if(bloqueConFoco == 8)
                                {
                                    request.setAttribute("tiposDeDocumentoLicitacion", LNS6.obtenerTipoDocumento(7, 0));
                                    request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                                }
                               
                               
                                if(bloqueConFoco == 9)
                                {
                                    request.setAttribute("tiposDeDocumentoLicitacion", LNS6.obtenerTipoDocumento(7, 0));
                                    request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                                    request.setAttribute("tiposDeHito", LNS6.obtenerTipoHito());
                                    request.setAttribute("estadosHito", LNS6.obtenerEstadoHito());
                                }
                               
                               
                                break;
                            case 8:
                               
                                request.setAttribute("esquemasDeClasificacion", LNS6.obtenerEsquemaClasificacion());
                                request.setAttribute("esquemasDeUnidad", LNS6.obtenerEsquemaUnidad());
                                request.setAttribute("tipoMoneda", LNS6.obtenerTipoDeMoneda());
                                request.setAttribute("tiposDeDocumento", LNS6.obtenerTipoDocumento(8, 0));
                                request.setAttribute("estadosAdjudicacion", LNS6.obtenerEstadosAdjudicacion());
                                request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                               
                                break;
                            case 9:
                               
                                request.setAttribute("esquemasDeClasificacion", LNS6.obtenerEsquemaClasificacion());
                                request.setAttribute("esquemasDeUnidad", LNS6.obtenerEsquemaUnidad());
                                request.setAttribute("tipoMoneda", LNS6.obtenerTipoDeMoneda());
                                request.setAttribute("tiposDeDocumentoContrato", LNS6.obtenerTipoDocumento(9, 1));
                                request.setAttribute("tiposDeDocumentoImplementacion", LNS6.obtenerTipoDocumento(9, 2));
                                request.setAttribute("estadosContrato", LNS6.obtenerEstadosContrato());
                                request.setAttribute("tiposDeHito", LNS6.obtenerTipoHito());
                                request.setAttribute("estadosHito", LNS6.obtenerEstadoHito());
                                request.setAttribute("relacionesProcesosRelacionados", LNS6.obtenerRelacionProceso());
                                request.setAttribute("esquemasProcesosRelacionados", LNS6.obtenerEsquemaProceso());
                                request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                               
                                break;
                        }
                       
                        
                        formateadorDeFechas = new FormateadorDeFechas();
                        request.setAttribute("formateadorDeFechas", formateadorDeFechas);
                        contratacion = LNS6.obtenerContratacion(idContratacion);
                        request.setAttribute("contratacion", contratacion);
                        request.setAttribute("idContratacion", contratacion.getIdSpdn());
                        request.setAttribute("migas", migas);
                        request.setAttribute("filtros", filtroBusquedaOrdenamiento);
                        request.setAttribute("paginacion", paginacion);
                        request.setAttribute("seccionConFoco", seccionConFoco);
                        request.setAttribute("bloqueConFoco", bloqueConFoco);
                        request.setAttribute("nivelDeAcceso", nivelAcceso);
                        request.setAttribute("irASeccion", true);
                        request.setAttribute("bolEsFormulariosDeEdicion", true);
                        this.fordward(request, resp, "s6Contrataciones/frmEditarContratacion.jsp");
                        break;
                    }
                    case "iniciarContinuarRegistroDeContratacion":
                    {
                        
                        boolean bolExisteContratacion;
                        int seccionConFoco;
                        int bloqueConFoco = 0;
                                                                               
                       
                        migas = new ArrayList<>();
                        migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                        migas.add(new Miga("Contrataciones", "ingresarSistema", "s_contrataciones"));
                        migas.add(new Miga("Registro de contrataci&oacute;n", ""));
                       
                        
                        idContratacion = (request.getParameter("txtID_CONTRATACION_SPDN") == null || request.getParameter("txtID_CONTRATACION_SPDN").equals("") == true) ? "" : request.getParameter("txtID_CONTRATACION_SPDN");
                        bolExisteContratacion = LNS6.existeContratacion(idContratacion);
                        
                        if(bolExisteContratacion == false)
                        {
                            contratacion = LNS6.modelarEstructuraPorDefectoContratacion();
                        }
                        else
                        {
                            contratacion = LNS6.obtenerContratacion(idContratacion);
                            request.setAttribute("usuarioCaptura", contratacion.getMetadatos().getUsuario_registro());
                            request.setAttribute("fechaCaptura", contratacion.getMetadatos().getFecha_registro());
                            request.setAttribute("estadoRegistro", contratacion.getEstado_registro());
                        }
                        
                        seccionConFoco = LNS6.obtenerSeccionConFoco(contratacion.getEstado_secciones_contratacion());
                        
                        if(seccionConFoco == 6)
                        {
                            bloqueConFoco = LNS6.obtenerBloqueConFoco(contratacion.getEstado_secciones_contratacion().get(5).getBloques_seccion());
                        }
                        if(seccionConFoco == 7)
                        {
                            bloqueConFoco = LNS6.obtenerBloqueConFoco(contratacion.getEstado_secciones_contratacion().get(6).getBloques_seccion());
                        }
                        
                       
                        switch(seccionConFoco)
                        {
                            case 1: 
                               
                                request.setAttribute("tiposDeInicio", LNS6.obtenerTipoInicio());
                                request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                               
                                break;
                            case 3:
                               
                                request.setAttribute("etiquetasDeEntrega", LNS6.obtenerEtiquetasDeEntrega());
                               
                                break;
                            case 4:
                               
                                request.setAttribute("rolesPartesInvolucradas", LNS6.obtenerRolesPartesInvolucradas());
                                request.setAttribute("escalasPartesInvolucradas", LNS6.obtenerEscalasPartesInvolucradas());
                               
                                break;
                            case 5:
                               
                                request.setAttribute("dependenciasUsuario", LNS6.obtenerEntesPublicos(usuario.getDependencias(), usuario));
                               
                                break;
                            case 6:
                               
                               
                                if(bloqueConFoco == 2)
                                {
                                    request.setAttribute("tipoMoneda", LNS6.obtenerTipoDeMoneda());
                                }
                               
                               
                                if(bloqueConFoco == 3)
                                {
                                    request.setAttribute("tiposDeDocumentoPlaneacion", LNS6.obtenerTipoDocumento(6, 0));
                                    request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                                }
                               
                               
                                if(bloqueConFoco == 4)
                                {
                                    request.setAttribute("tiposDeDocumentoPlaneacion", LNS6.obtenerTipoDocumento(6, 0));
                                    request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                                    request.setAttribute("tiposDeHito", LNS6.obtenerTipoHito());
                                    request.setAttribute("estadosHito", LNS6.obtenerEstadoHito());
                                }
                               
                               
                                break;
                            case 7:
                               
                               
                                if(bloqueConFoco == 1)
                                {
                                    request.setAttribute("tiposDePresentacionDeMetodos", LNS6.obtenerMetodosDePresentacion());
                                    request.setAttribute("criterioDeAdjudicacion", LNS6.obtenerCriteriosDeAdjudicacion());
                                    request.setAttribute("categoriasPrincipales", LNS6.obtenerCategoriasPrincipales());
                                    request.setAttribute("categoriasAdicionales", LNS6.obtenerCategoriasAdicionales());
                                    request.setAttribute("metodosLicitacion", LNS6.obtenerMetodoContratacion());
                                    request.setAttribute("estadosLicitacion", LNS6.obtenerEstadoLicitacion());
                                    request.setAttribute("tipoMoneda", LNS6.obtenerTipoDeMoneda());
                                }
                               
                               
                                if(bloqueConFoco == 6)
                                {
                                    request.setAttribute("esquemasDeClasificacion", LNS6.obtenerEsquemaClasificacion());
                                    request.setAttribute("esquemasDeUnidad", LNS6.obtenerEsquemaUnidad());
                                    request.setAttribute("tipoMoneda", LNS6.obtenerTipoDeMoneda());
                                }
                               
                               
                                if(bloqueConFoco == 8)
                                {
                                    request.setAttribute("tiposDeDocumentoLicitacion", LNS6.obtenerTipoDocumento(7, 0));
                                    request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                                }
                               
                               
                                if(bloqueConFoco == 9)
                                {
                                    request.setAttribute("tiposDeDocumentoLicitacion", LNS6.obtenerTipoDocumento(7, 0));
                                    request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                                    request.setAttribute("tiposDeHito", LNS6.obtenerTipoHito());
                                    request.setAttribute("estadosHito", LNS6.obtenerEstadoHito());
                                }
                               
                               
                                break;
                            case 8:
                               
                                request.setAttribute("esquemasDeClasificacion", LNS6.obtenerEsquemaClasificacion());
                                request.setAttribute("esquemasDeUnidad", LNS6.obtenerEsquemaUnidad());
                                request.setAttribute("tipoMoneda", LNS6.obtenerTipoDeMoneda());
                                request.setAttribute("tiposDeDocumento", LNS6.obtenerTipoDocumento(8, 0));
                                request.setAttribute("estadosAdjudicacion", LNS6.obtenerEstadosAdjudicacion());
                                request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                               
                                break;
                            case 9:
                               
                                request.setAttribute("esquemasDeClasificacion", LNS6.obtenerEsquemaClasificacion());
                                request.setAttribute("esquemasDeUnidad", LNS6.obtenerEsquemaUnidad());
                                request.setAttribute("tipoMoneda", LNS6.obtenerTipoDeMoneda());
                                request.setAttribute("tiposDeDocumentoContrato", LNS6.obtenerTipoDocumento(9, 1));
                                request.setAttribute("tiposDeDocumentoImplementacion", LNS6.obtenerTipoDocumento(9, 2));
                                request.setAttribute("estadosContrato", LNS6.obtenerEstadosContrato());
                                request.setAttribute("tiposDeHito", LNS6.obtenerTipoHito());
                                request.setAttribute("estadosHito", LNS6.obtenerEstadoHito());
                                request.setAttribute("relacionesProcesosRelacionados", LNS6.obtenerRelacionProceso());
                                request.setAttribute("esquemasProcesosRelacionados", LNS6.obtenerEsquemaProceso());
                                request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                               
                                break;
                        }
                       
                             
                        formateadorDeFechas = new FormateadorDeFechas();
                        request.setAttribute("formateadorDeFechas", formateadorDeFechas);
                        request.setAttribute("idContratacion", contratacion.getIdSpdn().toHexString());
                        request.setAttribute("estadoSeccionesContratacion", contratacion.getEstado_secciones_contratacion());
                        request.setAttribute("seccionConFoco", seccionConFoco);
                        request.setAttribute("bloqueConFoco", bloqueConFoco);
                        request.setAttribute("nivelDeAcceso", nivelAcceso);
                        request.setAttribute("migas", migas);
                        this.fordward(request, resp, "s6Contrataciones/frmRegistroContratacion.jsp");
                        break;
                    }
                    case "registrarSeccion":
                    {
                        
                        ContratacionesContratacion contratacionModelada;
                        boolean bolExisteContratacion;
                        boolean bolSeRegistra;
                        int intNumeroDeSeccion;
                        int intNumeroDeBloque;
                        int seccionConFoco;
                        int bloqueConFoco = 0;
                                                       
                       
                        migas = new ArrayList<>();
                        migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                        migas.add(new Miga("Contrataciones", "ingresarSistema", "s_contrataciones"));
                        migas.add(new Miga("Registro de contrataci&oacute;n", ""));
                       
                        
                        bolSeRegistra = true;
                        
                        idContratacion = (request.getParameter("txtID_CONTRATACION_SPDN") == null || request.getParameter("txtID_CONTRATACION_SPDN").equals("") == true) ? "" : request.getParameter("txtID_CONTRATACION_SPDN");
                        intNumeroDeSeccion = (request.getParameter("txtNUMERO_SECCION") == null || request.getParameter("txtNUMERO_SECCION").equals("") == true) ? 1 : Integer.parseInt(request.getParameter("txtNUMERO_SECCION"));
                        intNumeroDeBloque = (request.getParameter("txtNUMERO_BLOQUE") == null || request.getParameter("txtNUMERO_BLOQUE").equals("") == true) ? 0 : Integer.parseInt(request.getParameter("txtNUMERO_BLOQUE"));
                        bolExisteContratacion = LNS6.existeContratacion(idContratacion);
                        
                        if(bolExisteContratacion == false)
                        {
                            contratacion = LNS6.modelarEstructuraPorDefectoContratacion();
                        }
                        else
                        {
                            contratacion = LNS6.obtenerContratacion(idContratacion);
                        }
                        
                        contratacionModelada = LNS6.modelarRequestSeccion(request, contratacion, intNumeroDeSeccion, intNumeroDeBloque, usuario, bolSeRegistra);
                        seccionConFoco = LNS6.obtenerSeccionConFoco(contratacionModelada.getEstado_secciones_contratacion());
                        
                        if(seccionConFoco == 6)
                        {
                            bloqueConFoco = LNS6.obtenerBloqueConFoco(contratacionModelada.getEstado_secciones_contratacion().get(5).getBloques_seccion());
                        }
                        if(seccionConFoco == 7)
                        {
                            bloqueConFoco = LNS6.obtenerBloqueConFoco(contratacionModelada.getEstado_secciones_contratacion().get(6).getBloques_seccion());
                        }
                        
                        mensaje = LNS6.registrarActualizarContratacion(contratacionModelada, usuario, nivelAcceso, intNumeroDeSeccion, intNumeroDeBloque, bolSeRegistra);
                       
                       
                        switch(seccionConFoco)
                        {
                            case 1: 
                               
                                request.setAttribute("tiposDeInicio", LNS6.obtenerTipoInicio());
                                request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                               
                                break;
                            case 3:
                               
                                request.setAttribute("etiquetasDeEntrega", LNS6.obtenerEtiquetasDeEntrega());
                               
                                break;
                            case 4:
                               
                                request.setAttribute("rolesPartesInvolucradas", LNS6.obtenerRolesPartesInvolucradas());
                                request.setAttribute("escalasPartesInvolucradas", LNS6.obtenerEscalasPartesInvolucradas());
                               
                                break;
                            case 5:
                               
                                request.setAttribute("dependenciasUsuario", LNS6.obtenerEntesPublicos(usuario.getDependencias(), usuario));
                               
                                break;
                            case 6:
                               
                               
                                if(bloqueConFoco == 2)
                                {
                                    request.setAttribute("tipoMoneda", LNS6.obtenerTipoDeMoneda());
                                }
                               
                               
                                if(bloqueConFoco == 3)
                                {
                                    request.setAttribute("tiposDeDocumentoPlaneacion", LNS6.obtenerTipoDocumento(6, 0));
                                    request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                                }
                               
                               
                                if(bloqueConFoco == 4)
                                {
                                    request.setAttribute("tiposDeDocumentoPlaneacion", LNS6.obtenerTipoDocumento(6, 0));
                                    request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                                    request.setAttribute("tiposDeHito", LNS6.obtenerTipoHito());
                                    request.setAttribute("estadosHito", LNS6.obtenerEstadoHito());
                                }
                               
                               
                                break;
                            case 7:
                               
                               
                                if(bloqueConFoco == 1)
                                {
                                    request.setAttribute("tiposDePresentacionDeMetodos", LNS6.obtenerMetodosDePresentacion());
                                    request.setAttribute("criterioDeAdjudicacion", LNS6.obtenerCriteriosDeAdjudicacion());
                                    request.setAttribute("categoriasPrincipales", LNS6.obtenerCategoriasPrincipales());
                                    request.setAttribute("categoriasAdicionales", LNS6.obtenerCategoriasAdicionales());
                                    request.setAttribute("metodosLicitacion", LNS6.obtenerMetodoContratacion());
                                    request.setAttribute("estadosLicitacion", LNS6.obtenerEstadoLicitacion());
                                    request.setAttribute("tipoMoneda", LNS6.obtenerTipoDeMoneda());
                                }
                               
                               
                                if(bloqueConFoco == 6)
                                {
                                    request.setAttribute("esquemasDeClasificacion", LNS6.obtenerEsquemaClasificacion());
                                    request.setAttribute("esquemasDeUnidad", LNS6.obtenerEsquemaUnidad());
                                    request.setAttribute("tipoMoneda", LNS6.obtenerTipoDeMoneda());
                                }
                               
                               
                                if(bloqueConFoco == 8)
                                {
                                    request.setAttribute("tiposDeDocumentoLicitacion", LNS6.obtenerTipoDocumento(7, 0));
                                    request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                                }
                               
                               
                                if(bloqueConFoco == 9)
                                {
                                    request.setAttribute("tiposDeDocumentoLicitacion", LNS6.obtenerTipoDocumento(7, 0));
                                    request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                                    request.setAttribute("tiposDeHito", LNS6.obtenerTipoHito());
                                    request.setAttribute("estadosHito", LNS6.obtenerEstadoHito());
                                }
                               
                               
                                break;
                            case 8:
                               
                                request.setAttribute("esquemasDeClasificacion", LNS6.obtenerEsquemaClasificacion());
                                request.setAttribute("esquemasDeUnidad", LNS6.obtenerEsquemaUnidad());
                                request.setAttribute("tipoMoneda", LNS6.obtenerTipoDeMoneda());
                                request.setAttribute("tiposDeDocumento", LNS6.obtenerTipoDocumento(8, 0));
                                request.setAttribute("estadosAdjudicacion", LNS6.obtenerEstadosAdjudicacion());
                                request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                               
                                break;
                            case 9:
                               
                                request.setAttribute("esquemasDeClasificacion", LNS6.obtenerEsquemaClasificacion());
                                request.setAttribute("esquemasDeUnidad", LNS6.obtenerEsquemaUnidad());
                                request.setAttribute("tipoMoneda", LNS6.obtenerTipoDeMoneda());
                                request.setAttribute("tiposDeDocumentoContrato", LNS6.obtenerTipoDocumento(9, 1));
                                request.setAttribute("tiposDeDocumentoImplementacion", LNS6.obtenerTipoDocumento(9, 2));
                                request.setAttribute("estadosContrato", LNS6.obtenerEstadosContrato());
                                request.setAttribute("tiposDeHito", LNS6.obtenerTipoHito());
                                request.setAttribute("estadosHito", LNS6.obtenerEstadoHito());
                                request.setAttribute("relacionesProcesosRelacionados", LNS6.obtenerRelacionProceso());
                                request.setAttribute("esquemasProcesosRelacionados", LNS6.obtenerEsquemaProceso());
                                request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                               
                                break;
                        }
                       
                        
                        formateadorDeFechas = new FormateadorDeFechas();
                        request.setAttribute("formateadorDeFechas", formateadorDeFechas);
                        request.setAttribute("nivelDeAcceso", nivelAcceso);
                        request.setAttribute("idContratacion", contratacionModelada.getIdSpdn().toHexString());
                        request.setAttribute("usuarioCaptura", contratacionModelada.getMetadatos().getUsuario_registro());
                        request.setAttribute("fechaCaptura", contratacionModelada.getMetadatos().getFecha_registro());
                        request.setAttribute("estadoRegistro", contratacionModelada.getEstado_registro());
                        request.setAttribute("estadoSeccionesContratacion", contratacionModelada.getEstado_secciones_contratacion());
                        request.setAttribute("seccionConFoco", seccionConFoco);
                        request.setAttribute("bloqueConFoco", bloqueConFoco);
                        request.setAttribute("mensaje", mensaje);
                        request.setAttribute("mostrarMensaje", true);
                        request.setAttribute("migas", migas);
                        this.fordward(request, resp, "s6Contrataciones/frmRegistroContratacion.jsp");
                        break;
                    }
                    case "editarSeccion":
                    {
                        
                        ContratacionesContratacion contratacionModelada;
                        boolean bolSeRegistra;
                        int intNumeroDeSeccion;
                        int intNumeroDeBloque;
                        int seccionConFoco;
                        int bloqueConFoco;
                        
                       
                        migas = new ArrayList<>();
                        migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                        migas.add(new Miga("Contrataciones", "ingresarSistema", "s_contrataciones"));
                        migas.add(new Miga("Edici&oacute;n de contrataci&oacute;n", ""));
                       
                        
                        bolSeRegistra = false;
                        
                        idContratacion = request.getParameter("txtID_CONTRATACION_SPDN");
                        intNumeroDeSeccion = (request.getParameter("txtNUMERO_SECCION") == null || request.getParameter("txtNUMERO_SECCION").equals("") == true) ? 1 : Integer.parseInt(request.getParameter("txtNUMERO_SECCION"));
                        intNumeroDeBloque = (request.getParameter("txtNUMERO_BLOQUE") == null || request.getParameter("txtNUMERO_BLOQUE").equals("") == true) ? 0 : Integer.parseInt(request.getParameter("txtNUMERO_BLOQUE"));
                        
                        seccionConFoco = intNumeroDeSeccion;
                        bloqueConFoco = intNumeroDeBloque;
                        
                        contratacion = LNS6.obtenerContratacion(idContratacion);
                        
                        contratacionModelada = LNS6.modelarRequestSeccion(request, contratacion, intNumeroDeSeccion, intNumeroDeBloque, usuario, bolSeRegistra);
                        
                        mensaje = LNS6.registrarActualizarContratacion(contratacionModelada, usuario, nivelAcceso, intNumeroDeSeccion, intNumeroDeBloque, bolSeRegistra);
                        
                       
                        switch(seccionConFoco)
                        {
                            case 1: 
                               
                                request.setAttribute("tiposDeInicio", LNS6.obtenerTipoInicio());
                                request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                               
                                break;
                            case 3:
                               
                                request.setAttribute("etiquetasDeEntrega", LNS6.obtenerEtiquetasDeEntrega());
                               
                                break;
                            case 4:
                               
                                request.setAttribute("rolesPartesInvolucradas", LNS6.obtenerRolesPartesInvolucradas());
                                request.setAttribute("escalasPartesInvolucradas", LNS6.obtenerEscalasPartesInvolucradas());
                               
                                break;
                            case 5:
                               
                                request.setAttribute("dependenciasUsuario", LNS6.obtenerEntesPublicos(usuario.getDependencias(), usuario));
                               
                                break;
                            case 6:
                               
                               
                                if(bloqueConFoco == 2)
                                {
                                    request.setAttribute("tipoMoneda", LNS6.obtenerTipoDeMoneda());
                                }
                               
                               
                                if(bloqueConFoco == 3)
                                {
                                    request.setAttribute("tiposDeDocumentoPlaneacion", LNS6.obtenerTipoDocumento(6, 0));
                                    request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                                }
                               
                               
                                if(bloqueConFoco == 4)
                                {
                                    request.setAttribute("tiposDeDocumentoPlaneacion", LNS6.obtenerTipoDocumento(6, 0));
                                    request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                                    request.setAttribute("tiposDeHito", LNS6.obtenerTipoHito());
                                    request.setAttribute("estadosHito", LNS6.obtenerEstadoHito());
                                }
                               
                               
                                break;
                            case 7:
                               
                               
                                if(bloqueConFoco == 1)
                                {
                                    request.setAttribute("tiposDePresentacionDeMetodos", LNS6.obtenerMetodosDePresentacion());
                                    request.setAttribute("criterioDeAdjudicacion", LNS6.obtenerCriteriosDeAdjudicacion());
                                    request.setAttribute("categoriasPrincipales", LNS6.obtenerCategoriasPrincipales());
                                    request.setAttribute("categoriasAdicionales", LNS6.obtenerCategoriasAdicionales());
                                    request.setAttribute("metodosLicitacion", LNS6.obtenerMetodoContratacion());
                                    request.setAttribute("estadosLicitacion", LNS6.obtenerEstadoLicitacion());
                                    request.setAttribute("tipoMoneda", LNS6.obtenerTipoDeMoneda());
                                }
                               
                               
                                if(bloqueConFoco == 6)
                                {
                                    request.setAttribute("esquemasDeClasificacion", LNS6.obtenerEsquemaClasificacion());
                                    request.setAttribute("esquemasDeUnidad", LNS6.obtenerEsquemaUnidad());
                                    request.setAttribute("tipoMoneda", LNS6.obtenerTipoDeMoneda());
                                }
                               
                               
                                if(bloqueConFoco == 8)
                                {
                                    request.setAttribute("tiposDeDocumentoLicitacion", LNS6.obtenerTipoDocumento(7, 0));
                                    request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                                }
                               
                               
                                if(bloqueConFoco == 9)
                                {
                                    request.setAttribute("tiposDeDocumentoLicitacion", LNS6.obtenerTipoDocumento(7, 0));
                                    request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                                    request.setAttribute("tiposDeHito", LNS6.obtenerTipoHito());
                                    request.setAttribute("estadosHito", LNS6.obtenerEstadoHito());
                                }
                               
                               
                                break;
                            case 8:
                               
                                request.setAttribute("esquemasDeClasificacion", LNS6.obtenerEsquemaClasificacion());
                                request.setAttribute("esquemasDeUnidad", LNS6.obtenerEsquemaUnidad());
                                request.setAttribute("tipoMoneda", LNS6.obtenerTipoDeMoneda());
                                request.setAttribute("tiposDeDocumento", LNS6.obtenerTipoDocumento(8, 0));
                                request.setAttribute("estadosAdjudicacion", LNS6.obtenerEstadosAdjudicacion());
                                request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                               
                                break;
                            case 9:
                               
                                request.setAttribute("esquemasDeClasificacion", LNS6.obtenerEsquemaClasificacion());
                                request.setAttribute("esquemasDeUnidad", LNS6.obtenerEsquemaUnidad());
                                request.setAttribute("tipoMoneda", LNS6.obtenerTipoDeMoneda());
                                request.setAttribute("tiposDeDocumentoContrato", LNS6.obtenerTipoDocumento(9, 1));
                                request.setAttribute("tiposDeDocumentoImplementacion", LNS6.obtenerTipoDocumento(9, 2));
                                request.setAttribute("estadosContrato", LNS6.obtenerEstadosContrato());
                                request.setAttribute("tiposDeHito", LNS6.obtenerTipoHito());
                                request.setAttribute("estadosHito", LNS6.obtenerEstadoHito());
                                request.setAttribute("relacionesProcesosRelacionados", LNS6.obtenerRelacionProceso());
                                request.setAttribute("esquemasProcesosRelacionados", LNS6.obtenerEsquemaProceso());
                                request.setAttribute("tiposDeIdioma", LNS6.obtenerListaDeTiposDeIdioma());
                               
                                break;
                        }
                       
                        formateadorDeFechas = new FormateadorDeFechas();
                        request.setAttribute("formateadorDeFechas", formateadorDeFechas);
                        request.setAttribute("nivelDeAcceso", nivelAcceso);
                        request.setAttribute("idContratacion", contratacionModelada.getIdSpdn().toHexString());
                        request.setAttribute("contratacion", contratacionModelada);
                        request.setAttribute("mensaje", mensaje);
                        request.setAttribute("mostrarMensaje", true);
                        request.setAttribute("bolEsFormulariosDeEdicion", true);
                        request.setAttribute("seccionConFoco", seccionConFoco);
                        request.setAttribute("bloqueConFoco", bloqueConFoco);
                        request.setAttribute("irASeccion", true);
                        request.setAttribute("migas", migas);
                        this.fordward(request, resp, "s6Contrataciones/frmEditarContratacion.jsp");
                        break;
                    }
                    default:
                        break;
                }
            }
            /**
             * *********************USUARIO EN SESION ***************
             */
            else
            {
                if (sesion != null)
                {
                    sesion.invalidate();
                    sesion = null;
                }

                request.setAttribute("mensaje", "Su sesi&oacute;n ha expirado ");
                this.fordward(request, resp, "/frmPrincipal.jsp");
            }
        }
        catch (ServletException | IOException ex)
        {
            request.setAttribute("mensaje", "Error : " + ex.toString());
            this.fordward(request, resp, "/error.jsp");
        }
        catch (Exception ex)
        {
            request.setAttribute("mensaje", "Error " + ex.toString());
            this.fordward(request, resp, "/error.jsp");
        }
    }

    public void fordward(HttpServletRequest req, HttpServletResponse resp, String ruta) throws ServletException, IOException
    {
        req.setAttribute("datosDeLaUltimaActualizacion", ConfVariables.getDATOS_ACTUALIZACION());
        RequestDispatcher despachador = req.getRequestDispatcher(ruta);
        despachador.forward(req, resp);
    }

}