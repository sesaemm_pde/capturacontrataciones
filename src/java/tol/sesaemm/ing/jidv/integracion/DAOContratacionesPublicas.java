/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.integracion;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Collation;
import com.mongodb.client.model.UpdateOptions;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.bson.Document;
import org.bson.json.JsonMode;
import org.bson.json.JsonWriterSettings;
import org.bson.types.ObjectId;
import tol.sesaemm.annotations.Exclude;
import tol.sesaemm.funciones.BitacoraPde;
import tol.sesaemm.funciones.ExpresionesRegulares;
import tol.sesaemm.funciones.ManejoDeFechas;
import tol.sesaemm.funciones.TBITACORAS_PDE;
import tol.sesaemm.ing.jidv.config.ConfVariables;
import tol.sesaemm.ing.jidv.javabeans.CARGA_DE_EVIDENCIA_PERIODO;
import tol.sesaemm.ing.jidv.javabeans.CargaDeEvidenciaVista;
import tol.sesaemm.ing.jidv.javabeans.FiltrosCargaDeEvidenciaPDE;
import tol.sesaemm.ing.jidv.javabeans.NIVEL_ACCESO;
import tol.sesaemm.ing.jidv.javabeans.PAGINACION;
import tol.sesaemm.ing.jidv.javabeans.TSEGUIMIENTO_CARGA_DE_EVIDENCIA;
import tol.sesaemm.ing.jidv.javabeans.USUARIO;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.CATEGORIA_ADICIONAL_CONTRATACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.CATEGORIA_PRINCIPAL_CONTRATACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.CRITERIO_ADJUDICACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ContratacionesConPaginacion;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ContratacionesVista;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESCALAS_PARTES_INVOLUCRADAS;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESQUEMA_CLASIFICACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESQUEMA_PROCESO;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESQUEMA_UNIDAD;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESTADO_ADJUDICACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESTADO_CONTRATO;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESTADO_HITO;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESTADO_LICITACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.TIPO_ETIQUETA_ENTREGA;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.FiltrosDeBusquedaContratacion;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.IDIOMA_CONTRATACIONES;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.METODO_CONTRATACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.METODO_PRESENTACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.RELACION_PROCESO;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ROLES_PARTES_INVOLUCRADAS;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.TIPO_DOCUMENTO_CONTRATACIONES;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.TIPO_HITO;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.TIPO_INICIO_CONTRATACIONES;
import tol.sesaemm.javabeans.ENTE_PUBLICO;
import tol.sesaemm.javabeans.MONEDA;
import tol.sesaemm.javabeans.SISTEMAS;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesContratacion;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesEstadoSeccion;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx 
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DAOContratacionesPublicas
{

    /**
     * Consulta los campos publicos de contrataciones
     *
     * @param coleccion nombre de la coleccion que se consulta
     *
     * @return Contratacion
     * @throws java.lang.Exception
     */
    public static ContratacionesContratacion obtenerEstatusDePrivacidadCamposContratacion(String coleccion) throws Exception
    {
        ContratacionesContratacion contratacion;                                              // Privacidad de datos
        MongoClient conectarBaseDatos = null;                                   // Conexion a la base de datos
        MongoDatabase database;                                                 // Obtener esquema de base de datos
        MongoCollection<Document> collection;                                   // Obtener coleccion de datos
        FindIterable<Document> aICamposPublicos;                                // Resultados
        MongoCursor<Document> cursor = null;                                    // Cursor de la busqueda
        Gson gson;
        ConfVariables confVariables;

        confVariables = new ConfVariables();

        gson = new Gson();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();
            contratacion = new ContratacionesContratacion();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("campos_publicos");

                aICamposPublicos = collection.find(new Document("coleccion", coleccion));

                cursor = aICamposPublicos.iterator();
                while (cursor.hasNext())
                { // Lectura de los datos (TOP)                    
                    Document next = cursor.next();
                    contratacion = gson.fromJson(next.toJson(), ContratacionesContratacion.class);
                    contratacion.setIdSpdn(next.get("_id", ObjectId.class));
                } // Lectura de los datos (BOTTOM)

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida");
            } // No se ha establecido la conexion (BOTTOM)
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener privacidad de campos: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursor);
        }

        return contratacion;
    }

    /**
     * Obtener listado de contrataciones publicas
     *
     * @param usuario
     * @param nivelAcceso
     * @param filtroBusqueda Listado de parametros de busqueda
     * @param dependencias
     *
     * @return Listado de datos principales del servidor publico
     * @throws Exception
     */
    public static ContratacionesConPaginacion obtenerContratacionesPublicas(USUARIO usuario, NIVEL_ACCESO nivelAcceso, ArrayList<ENTE_PUBLICO> dependencias, FiltrosDeBusquedaContratacion filtroBusqueda) throws Exception
    { // metodo para obtenr el listado de contrataciones publicas que se muestar en la vista principal del sistema 6 (TOP)

        ArrayList<ContratacionesVista> listaDeContrataciones;                               // almacena lista de contrataciones publicas        
        MongoClient conectarBaseDatos = null;                                               // conexion a la base de datos
        MongoDatabase database;                                                             // obtener esquema de base de datos
        MongoCollection<Document> coleccion;                                                // obtener coleccion de datos
        AggregateIterable<Document> iteradorDeResultados;                                   // objeto iterable para la consulta
        MongoCursor<Document> cursorDeResultados = null;                                    // cursor de recorrido de resultados para la consulta                                   
        Document where = new Document();                                                    // almacena las condiciones para la consulta        
        Document order = new Document();                                                    // almacena las condiciones de ordenamiento para la consulta  
        List<Document> where_dependencia;                                                   // Condiciones de b&uacute;squeda
        Document or;                                                                        // Condici&oacute;n OR
        List<Document> and;                                                                 // Condici&oacute;n AND
        ArrayList<Document> query;                                                          // lista de sentencias que confirman la consulta
        ContratacionesConPaginacion objetos;                                                // lista de objetos que contiene la paginacion y la lista de servidores publicos
        PAGINACION filtroPaginacion;                                                        // parametros de paginacion
        int totalRegFiltro;                                                                 // numero total de registros a devolver
        int totalPaginas;                                                                   // numero total de paginas que seran mostradas
        int saltos = 0;                                                                     // numero de registros que seran omitidos
        Collation collation = Collation.builder().locale("es").caseLevel(true).build();     // indica el ordenamiento para la consulta omitiendo mayusculas de minusculas y acentos
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        filtroPaginacion = new PAGINACION();
        query = new ArrayList<>();
        objetos = new ContratacionesConPaginacion();
        where_dependencia = new ArrayList<>();
        or = new Document();
        and = new ArrayList<>();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Conexion a base de datos (TOP)

                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                coleccion = database.getCollection("contrataciones");

                //<editor-fold defaultstate="collapsed" desc="filtros de busqueda">
                if (nivelAcceso.getId() == 2)
                { // usuario con privilegiso de ver los registros que agrego (TOP)
                    where.append("metadatos.usuario_registro", new Document("$eq", usuario.getUsuario()));
                } // usuario con privilegiso de ver los registros que agrego (BOTTOM)
                else if (nivelAcceso.getId() == 3)
                { // Usuario con privilegiso de ver los registros de la dependencia asociada (TOP)
                    for (ENTE_PUBLICO dependencia : dependencias)
                    { // Filtrar por dependencia asignada (TOP)
                        where_dependencia.add(new Document("buyer.id", dependencia.getClave()));
                    } // Filtrar por dependencia asignada (BOTTOM)
                    or.append("$or", where_dependencia);
                    and.add(or);
                    and.add(new Document("publicar", new Document("$ne", "0")));
                } // Usuario con privilegiso de ver los registros de la dependencia asociada (BOTTOM)
                else if (nivelAcceso.getId() == 5)
                { // usuario con vista general (TOP)
                    where.append("publicar", new Document("$ne", "0"));
                } // usuario con vista general (BOTTOM)
                
                if (filtroBusqueda.getCiclo() != null && filtroBusqueda.getCiclo() > 0)
                { // filtrado por ciclo (TOP)
                    where.append("cycle", filtroBusqueda.getCiclo());
                } // filtrado por ciclo  (BOTTOM)

                if (filtroBusqueda.getPublicador() != null && filtroBusqueda.getPublicador().isEmpty() == false)
                { // filtrado por publicador (TOP)
                    where.append("publisher.name",
                            new Document("$regex", ExpresionesRegulares.crearExpresionRegularDePalabra(filtroBusqueda.getPublicador())).append("$options", "i"));
                } // filtrado por publicador (BOTTOM)   

                if (filtroBusqueda.getDependencia() != null && filtroBusqueda.getDependencia().isEmpty() == false)
                { // filtrado por dependencia asociada a la contratacion (TOP)
                    where.append("buyer.name",
                            new Document("$regex", ExpresionesRegulares.crearExpresionRegularDePalabra(filtroBusqueda.getDependencia())).append("$options", "i"));
                } // filtrado por dependencia asociada a la contratacion (BOTTOM)  
     
                if (and.size() > 0)
                {
                    query.add(new Document("$match", new Document("$and", and)));
                }
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="orden para la consulta">
                switch (filtroBusqueda.getOrden())
                {
                    // ordenar por publicador (TOP)
                    case "publicador":
                        order.append("publicador", 1);
                        break;
                    // ordenar por publicador (BOTTOM)
                    // ordenar por dependencia (TOP)
                    case "dependencia":
                        order.append("dependencia", 1);
                        break;
                    // ordenar por dependencia (BOTTOM)
                    // ordenar por ciclo (TOP)
                    default:
                        order.append("ciclo", 1);
                        break;
                    // ordenar por ciclo (BOTTOM)
                }
                //</editor-fold>

                query.add(new Document("$match", where));
                query.add(new Document("$addFields", new Document()
                        .append("estado_registro", new Document()
                                .append("$cond", new Document()
                                        .append("if", new Document("$eq", Arrays.asList("$estado_registro","1")))
                                        .append("then", true)
                                        .append("else", false)
                                )
                        )
                ));
                query.add(new Document("$project", new Document()
                        .append("idRegistro", new Document("$toString", "$_id"))
                        .append("ciclo", new Document("$toString", "$cycle"))
                        .append("dependencia", "$buyer.name")
                        .append("publicador", "$publisher.name")
                        .append("usuarioCaptura", "$metadatos.usuario_registro")
                        .append("contratacionCompleta", "$estado_registro")
                        .append("publicar", "$publicar")
                )
                );
                
                //<editor-fold defaultstate="collapsed" desc="Obtencion de la paginaci&iacute;n ">
                query.add(new Document("$count", "totalCount"));
                iteradorDeResultados = coleccion.aggregate(query).collation(collation).allowDiskUse(true);
                cursorDeResultados = iteradorDeResultados.iterator();
                if (cursorDeResultados.hasNext())
                { // Lectura de los datos (TOP)
                    totalRegFiltro = cursorDeResultados.next().get("totalCount", Integer.class);

                    // Calculo de los parametros de la paginacion
                    totalPaginas = (int) Math.ceil((float) totalRegFiltro / filtroBusqueda.getRegistrosMostrar());

                    if (filtroBusqueda.getNumeroPagina() > 1)
                    { // Si el numero de paginas es mayor que 1 (TOP)
                        saltos = (filtroBusqueda.getNumeroPagina() - 1) * filtroBusqueda.getRegistrosMostrar();
                    } // Si el numero de paginas es mayor que 1 (BOTTOM)

                    filtroPaginacion.setRegistrosMostrar(filtroBusqueda.getRegistrosMostrar());
                    filtroPaginacion.setNumeroPagina(filtroBusqueda.getNumeroPagina());
                    filtroPaginacion.setTotalPaginas((totalPaginas < 1) ? 1 : totalPaginas);
                    filtroPaginacion.setNumeroSaltos(saltos);
                    filtroPaginacion.setTotalRegistros(totalRegFiltro);
                    filtroPaginacion.setIsEmpty(false);
                } // Lectura de los datos (BOTTOM)

                query.remove(new Document("$count", "totalCount"));
                //</editor-fold>

                query.add(new Document("$sort",
                        order
                ));

                //<editor-fold defaultstate="collapsed" desc="Busca, ordena y asigna documentos segun las condiciones de consulta">
                if (filtroPaginacion.isEmpty() == false)
                { // valida que la paginacion contenga elementos (TOP)
                    query.add(new Document("$skip", filtroPaginacion.getNumeroSaltos()));
                    query.add(new Document("$limit", filtroPaginacion.getRegistrosMostrar()));
                } // valida que la paginacion contenga elementos (BOTTOM)

                listaDeContrataciones = coleccion.aggregate(query, ContratacionesVista.class).collation(collation).allowDiskUse(true).into(new ArrayList<>());
                //</editor-fold>                                

                objetos.setFiltroPaginacion(filtroPaginacion);
                objetos.setListaDeContrataciones(listaDeContrataciones);

            } // Conexion a base de datos (BOTTOM)
            else
            { // lanzar excepcion de conexion a base de datos (TOP)
                throw new Exception("Conexion no establecida");
            } // lanzar excepcion de conexion a base de datos (BOTTOM)
        }
        catch (Exception ex)
        { // lanzar excepcion (TOP)
            throw new Exception("Error al obtener el listado de contrataciones publicas: " + ex.toString());
        } // lanzar excepcion (BOTTOM)                  
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursorDeResultados);
        }

        return objetos;

    } // metodo para obtenr el listado de contrataciones publicas que se muestar en la vista principal del sistema 6 (BOTTOM)

    /**
     * Metodo para obtener el nombre de las dependencias que estan registradas
     * en el sistema de contrataciones
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    public static ArrayList<String> obtenerDependenciasDeRegistros() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                               // Conexi&oacute;n a la base de datos
        MongoDatabase database;                                                             // Obtener esquema de base de datos
        MongoCollection<Document> collection;                                               // Obtener colecci&oacute;n de datos
        ArrayList<Document> query;                                                          // Lista de sentencias que confirman la consulta
        ArrayList<String> dependencias;                                                     // lista de dependencias
        AggregateIterable<Document> aggregateIterable;                                      // Resultados
        MongoCursor<Document> cursor = null;                                                // Cursor de la b&uacute;squeda
        Collation collation = Collation.builder().locale("es").caseLevel(true).build();     // indica el ordenamiento para la consulta omitiendo mayusculas de minusculas y acentos
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        query = new ArrayList<>();
        dependencias = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)

                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("contrataciones");

                //<editor-fold defaultstate="collapsed" desc="Consulta">
                query.add(new Document("$match",
                        new Document("publicar", new Document().append("$ne", "0"))
                ));

                query.add(new Document("$project", new Document()
                        .append("institucionDependencia", new Document()
                                .append("$toLower", "$metadata.institucion")
                        )
                )
                );

                query.add(
                        new Document("$group",
                                new Document("_id", "$institucionDependencia")
                        )
                );

                query.add(
                        new Document("$sort",
                                new Document("_id", 1)
                        )
                );
                //</editor-fold>

                aggregateIterable = collection.aggregate(query).collation(collation);
                cursor = aggregateIterable.iterator();

                while (cursor.hasNext())
                { // Lectura de los datos (TOP)                    
                    Document next = cursor.next();
                    dependencias.add(next.get("_id", String.class));
                } // Lectura de los datos (BOTTOM)
            } // Si la conexion se ha establecido(BOTTOM)
            else
            {
                throw new Exception("Conexion no establecida");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener la lista de dependencias de los registros de contrataciones: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursor);
        }
        return dependencias;
    }

    /**
     * Obtener contratacion publica por ID
     *
     * @param id
     *
     * @return contratacion
     *
     * @throws Exception
     */
    public static ContratacionesContratacion obtenerContratacion(String id) throws Exception
    {
        ContratacionesContratacion contratacion;                                                  //        
        MongoClient conectarBaseDatos = null;                                       // Conexi&oacute;n a la base de datos
        MongoDatabase database;                                                     // Obtener esquema de base de datos
        MongoCollection<Document> coleccion;                                        // Obtener coleccion de datos
        FindIterable<Document> iteradorDeResultados;                                // Objeto iterable para la consulta
        MongoCursor<Document> cursorDeResultados = null;                            // Cursor de recorrido de resultados para la consulta                                   
        Gson gson;                                                                  // Liberia para el mapeo de datos
        JsonWriterSettings settings;                                                // Configuraci&oacute;n para la preservación de tipos en mongo
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        contratacion = new ContratacionesContratacion();
        gson = new Gson();

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Conexion a base de datos (TOP)

                //<editor-fold defaultstate="collapsed" desc="hasNext">
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                coleccion = database.getCollection("contrataciones");

                iteradorDeResultados = coleccion.find(new Document("_id", new ObjectId(id)));
                cursorDeResultados = iteradorDeResultados.iterator();

                settings = JsonWriterSettings.builder().outputMode(JsonMode.RELAXED).build();

                if (cursorDeResultados.hasNext())
                { // Obtiene los documentos (TOP)                                            
                    Document next = cursorDeResultados.next();
                    contratacion = gson.fromJson(next.toJson(settings), ContratacionesContratacion.class);
                    contratacion.setIdSpdn(next.get("_id", ObjectId.class));
                } // Obtiene los documentos (BOTTOM)
                //</editor-fold>
               
            } // Conexion a base de datos (BOTTOM)
            else
            { // Lanzar excepcion de conexion a base de datos (TOP)
                throw new Exception("Conexion no establecida");
            } // Lanzar excepcion de conexion a base de datos (BOTTOM)
        }
        catch (Exception ex)
        { // Lanzar excepcion (TOP)
            throw new Exception("Error al obtener la contratacion publica: " + ex.toString());
        } // Lanzar excepcion (BOTTOM)      
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursorDeResultados);
        }

        return contratacion;
    }

    /**
     * Obtener contratacion publica por ID
     *
     * @param id
     *
     * @return contratacion
     *
     * @throws Exception
     */
    public static boolean existeContratacion(String id) throws Exception
    {
        boolean bolExiste;                                                      // variable de ayuda para verificar si existe una contratacion       
        MongoClient conectarBaseDatos = null;                                   // Conexi&oacute;n a la base de datos
        MongoDatabase database;                                                 // Obtener esquema de base de datos
        MongoCollection<Document> coleccion;                                    // Obtener coleccion de datos
        FindIterable<Document> iteradorDeResultados;                            // Objeto iterable para la consulta
        MongoCursor<Document> cursorDeResultados = null;                        // Cursor de recorrido de resultados para la consulta                                   
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        bolExiste = false;

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Conexion a base de datos (TOP)

                if(id.equals("") == false)
                {
                    //<editor-fold defaultstate="collapsed" desc="hasNext">
                    database = conectarBaseDatos.getDatabase(confVariables.getBD());
                    coleccion = database.getCollection("contrataciones");

                    iteradorDeResultados = coleccion.find(new Document("_id", new ObjectId(id)));
                    cursorDeResultados = iteradorDeResultados.iterator();

                    if (cursorDeResultados.hasNext())
                    { // existe la contratacion (TOP)                               
                        bolExiste = true;
                    } // existe la contratacion (BOTTOM)
                    //</editor-fold>
                }
                
            } // Conexion a base de datos (BOTTOM)
            else
            { // Lanzar excepcion de conexion a base de datos (TOP)
                throw new Exception("Conexion no establecida");
            } // Lanzar excepcion de conexion a base de datos (BOTTOM)
        }
        catch (Exception ex)
        { // Lanzar excepcion (TOP)
            throw new Exception("Error verificar si existe una contratacion publica: " + ex.toString());
        } // Lanzar excepcion (BOTTOM)      
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursorDeResultados);
        }

        return bolExiste;
    }
    
    /**
     * Registrar contratacion publica
     *
     * @param contratacion
     * @param usuario Usuario de registro
     * @param nivelAcceso Nivel que tiene el usuario para interactuar en el
     * sistema
     * @param bolSeRegistra
     *
     * @return boolean
     *
     * @throws java.lang.Exception
     */
    public static boolean registrarActualizarContratacion(ContratacionesContratacion contratacion, USUARIO usuario, NIVEL_ACCESO nivelAcceso, boolean bolSeRegistra) throws Exception
    {
        
        MongoClient conectarBaseDatos = null;                                   // Conexi&oacute;n a la base de datos
        MongoDatabase database;                                                 // Obtener esquema de base de datos
        MongoCollection<Document> collection;                                   // Obtener colecci&oacute;n de datos   ExclusionStrategy strategy;                                             // permite excluir ciertos campos de ser serializados
        ExclusionStrategy strategy;                                             // permite excluir ciertos campos de ser serializados
        Gson gson;                                                              // liberia que permite el mapeo de datos
        Document contratacionPublica;                                           // Estructura final que se guarda a la base de datos
        boolean seRegistra;                                                     // bandera que indica si el registro fue exitoso
        UpdateOptions options;
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        strategy = new ExclusionStrategy()
        {
            @Override
            public boolean shouldSkipClass(Class<?> clazz)
            {
                return false;
            }

            @Override
            public boolean shouldSkipField(FieldAttributes field)
            {
                return field.getAnnotation(Exclude.class) != null;
            }
        };

        gson = new GsonBuilder()
                .serializeNulls()
                .addSerializationExclusionStrategy(strategy)
                .create();

        seRegistra = false;
        
        try
        {

            options = new UpdateOptions().upsert(true);
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("contrataciones");

                if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
                { // si es admin o puede capturar (TOP)
                    contratacionPublica = Document.parse(gson.toJson(contratacion));
                    
                    collection.updateOne(
                        new Document("_id", contratacion.getIdSpdn()),
                        new Document("$set", contratacionPublica),
                        options
                    );
                    
                    if(bolSeRegistra == true)
                    { // se registra la contratacion (TOP)
                        BitacoraPde.registrarBitacora(new TBITACORAS_PDE(new ManejoDeFechas().getDateNowAsString(), usuario.getUsuario(), BitacoraPde.contrataciones, "SAEMM", contratacion.getIdSpdn().toHexString(), contratacionPublica.toJson(), BitacoraPde.registro));
                    } // se registra la contratacion (BOTTOM)
                    else
                    { // se actualiza la contratacion (TOP)
                        BitacoraPde.registrarBitacora(new TBITACORAS_PDE(new ManejoDeFechas().getDateNowAsString(), usuario.getUsuario(), BitacoraPde.contrataciones, "SAEMM", contratacion.getIdSpdn().toHexString(), contratacionPublica.toJson(), BitacoraPde.modificacion));
                    } // se actualiza la contratacion (BOTTOM)
                    
                    seRegistra = true;
                    
                } // si es admin o puede capturar (BOTTOM)

            } // Si la conexion se ha establecido (BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
        }
        catch (Exception ex)
        {
            throw new Exception("Error al registrar contrataci&oacute;n p&uacute;blica : " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return seRegistra;
        
    }
    
    /**
     * Obtener contratacion publica por ID
     *
     * @param id
     * @param intNumeroDeSeccion
     *
     * @return contratacion
     *
     * @throws Exception
     */
    public static ContratacionesEstadoSeccion obtenerEstadoDeSeccion(String id, int intNumeroDeSeccion) throws Exception
    {
        
        MongoClient conectarBaseDatos = null;                                               // Conexi&oacute;n a la base de datos
        MongoDatabase database;                                                             // Obtener esquema de base de datos
        MongoCollection<Document> collection;                                               // Obtener colecci&oacute;n de datos
        ArrayList<Document> query;                                                          // Lista de sentencias que confirman la consulta
        ContratacionesEstadoSeccion estadoSeccion;                                          // estado de la seccion
        AggregateIterable<Document> aggregateIterable;                                      // Resultados
        MongoCursor<Document> cursor = null;                                                // Cursor de la b&uacute;squeda
        Collation collation = Collation.builder().locale("es").caseLevel(true).build();     // indica el ordenamiento para la consulta omitiendo mayusculas de minusculas y acentos
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        query = new ArrayList<>();
        estadoSeccion = new ContratacionesEstadoSeccion();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)

                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("contrataciones");

                //<editor-fold defaultstate="collapsed" desc="Consulta">
                query.add(new Document("$project", new Document()
                        .append("identificador", new Document("$toString","$_id"))
                        .append("estado_secciones", "$estado_secciones_contratacion")
                    )
                );
                query.add(
                        new Document("$unwind","$estado_secciones")
                );
                query.add(new Document("$match", new Document()
                        .append("identificador", id)
                        .append("estado_secciones.numero_seccion", intNumeroDeSeccion)
                ));
                //</editor-fold>

                aggregateIterable = collection.aggregate(query).collation(collation);
                cursor = aggregateIterable.iterator();

                while (cursor.hasNext())
                { // Lectura de los datos (TOP)    
                    
                    Document next = cursor.next();
                    estadoSeccion = next.get("estado_secciones", ContratacionesEstadoSeccion.class);
                    
                } // Lectura de los datos (BOTTOM)
                
            } // Si la conexion se ha establecido(BOTTOM)
            else
            {
                throw new Exception("Conexion no establecida");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el estado de secci&oacute;n de una contrataci&oacute;n: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursor);
        }
        
        return estadoSeccion;
        
    }
    
    /**
     * Registrar contratacion publica
     *
     *
     * @param contratacion
     * @param intNumeroDeSeccion
     * @param estadoSeccion
     * @param nivelAcceso
     * @return boolean
     *
     * @throws java.lang.Exception
     */
    public static boolean actualizarEstadoDeSeccion(ContratacionesContratacion contratacion, int intNumeroDeSeccion, ContratacionesEstadoSeccion estadoSeccion, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        
        MongoClient conectarBaseDatos = null;                                   // Conexi&oacute;n a la base de datos
        MongoDatabase database;                                                 // Obtener esquema de base de datos
        MongoCollection<Document> collection;                                   // Obtener colecci&oacute;n de datos   ExclusionStrategy strategy;                                             // permite excluir ciertos campos de ser serializados
        ExclusionStrategy strategy;                                             // permite excluir ciertos campos de ser serializados
        Gson gson;                                                              // liberia que permite el mapeo de datos
        Document contratacionPublica;                                           // Estructura final que se guarda a la base de datos
        boolean seRegistra;                                                     // bandera que indica si el registro fue exitoso
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        strategy = new ExclusionStrategy()
        {
            @Override
            public boolean shouldSkipClass(Class<?> clazz)
            {
                return false;
            }

            @Override
            public boolean shouldSkipField(FieldAttributes field)
            {
                return field.getAnnotation(Exclude.class) != null;
            }
        };

        gson = new GsonBuilder()
                .serializeNulls()
                .addSerializationExclusionStrategy(strategy)
                .create();

        seRegistra = false;
        
        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("contrataciones");

                if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
                { // si es admin o puede capturar (TOP)
                    contratacionPublica = Document.parse(gson.toJson(contratacion));
                    collection.updateOne(
                        new Document("_id", contratacion.getIdSpdn()),
                        new Document("$set", contratacionPublica)
                    );
                    
                    seRegistra = true;
                    
                } // si es admin o puede capturar (BOTTOM)

            } // Si la conexion se ha establecido (BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
        }
        catch (Exception ex)
        {
            throw new Exception("Error al actualizar el estado de secci&oacute;n de la contrataci&oacute;n p&uacute;blica : " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return seRegistra;
        
    }
    
    /**
     * Metodo para obtener las etiquetas de entrega permitidas para una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    public static ArrayList<TIPO_ETIQUETA_ENTREGA> obtenerEtiquetasDeEntrega() throws Exception
    { // metodo para obtener las etiquetas de entrega permitidas para una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                   // conexion a la base de datos
        MongoDatabase database;                                                 // obtener esquema de base de datos
        MongoCollection<Document> collection;                                   // obtener coleccion de datos
        ArrayList<TIPO_ETIQUETA_ENTREGA> listadoEtiquetasDeEntrega;                 // listado de etiquetas de entrega
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_etiqueta_entrega");

                listadoEtiquetasDeEntrega = collection.find(TIPO_ETIQUETA_ENTREGA.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
            
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener las etiquetas de entrega de una contrataci&oacute;n p&uacute;plica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return listadoEtiquetasDeEntrega;
        
    } // metodo para obtener las etiquetas de entrega permitidas para una contratacion publica (BOTTOM)
    
    /**
     * Metodo para obtener el tipo de inicio para una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    public static ArrayList<TIPO_INICIO_CONTRATACIONES> obtenerTipoInicio() throws Exception
    { // metodo para obtener el tipo de inicio para una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                   // conexion a la base de datos
        MongoDatabase database;                                                 // obtener esquema de base de datos
        MongoCollection<Document> collection;                                   // obtener coleccion de datos
        ArrayList<TIPO_INICIO_CONTRATACIONES> listadoTipoInicio;                // listado de tipos de inicio
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_inicio_contrataciones");

                listadoTipoInicio = collection.find(TIPO_INICIO_CONTRATACIONES.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
            
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el tipo de inicio de una contrataci&oacute;n p&uacute;plica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return listadoTipoInicio;
        
    } // metodo para obtener el tipo de inicio para una contratacion publica (BOTTOM)
    
    /**
     * Metodo para obtener el estado de una licitacion en una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    public static ArrayList<ESTADO_LICITACION> obtenerEstadoLicitacion() throws Exception
    { // metodo para obtener el estado de una licitacion en una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                   // conexion a la base de datos
        MongoDatabase database;                                                 // obtener esquema de base de datos
        MongoCollection<Document> collection;                                   // obtener coleccion de datos
        ArrayList<ESTADO_LICITACION> listadoEstados;                            // listado de estados para una licitacion
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("estado_licitacion");

                listadoEstados = collection.find(ESTADO_LICITACION.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
            
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el estado de un licitaci&oacute;n en una contrataci&oacute;n p&uacute;plica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return listadoEstados;
        
    } // metodo para obtener el estado de una licitacion en una contratacion publica (BOTTOM)
    
    /**
     * Metodo para obtener los roles permitidos para las partes involucradas en una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    public static ArrayList<ROLES_PARTES_INVOLUCRADAS> obtenerRolesPartesInvolucradas() throws Exception
    { // metodo para obtener los roles permitidos para las partes involucradas en una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                   // conexion a la base de datos
        MongoDatabase database;                                                 // obtener esquema de base de datos
        MongoCollection<Document> collection;                                   // obtener coleccion de datos
        ArrayList<ROLES_PARTES_INVOLUCRADAS> listadoRoles;                        // listado de roles
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("roles_partes_involucradas");

                listadoRoles = collection.find(ROLES_PARTES_INVOLUCRADAS.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
            
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener los roles de las partes involucradas en una contrataci&oacute;n p&uacute;plica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return listadoRoles;
        
    } // metodo para obtener los roles permitidos para las partes involucradas en una contratacion publica (BOTTOM)
    
    /**
     * Metodo para obtener las escalas permitidas para las partes involucradas en una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    public static ArrayList<ESCALAS_PARTES_INVOLUCRADAS> obtenerEscalasPartesInvolucradas() throws Exception
    { // metodo para obtener las escalas permitidas para las partes involucradas en una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                   // conexion a la base de datos
        MongoDatabase database;                                                 // obtener esquema de base de datos
        MongoCollection<Document> collection;                                   // obtener coleccion de datos
        ArrayList<ESCALAS_PARTES_INVOLUCRADAS> listadoEscalas;                    // listado de escalas
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("escalas_partes_involucradas");

                listadoEscalas = collection.find(ESCALAS_PARTES_INVOLUCRADAS.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
            
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener las escalas de las partes involucradas en una contrataci&oacute;n p&uacute;plica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return listadoEscalas;
        
    } // metodo para obtener las escalas permitidas para las partes involucradas en una contratacion publica (BOTTOM)
    
    /**
     * Metodo para obtener el tipo de documento segun la seccion consultada
     *
     * @param intNumeroDeSeccion
     * @param intNumeroApartado
     *
     * @return ArrayList
     *
     * @throws Exception
     */
    public static ArrayList<TIPO_DOCUMENTO_CONTRATACIONES> obtenerTipoDocumento(int intNumeroDeSeccion, int intNumeroApartado) throws Exception
    { // metodo para obtener el tipo de documento segun la seccion consultada (TOP)
         
        MongoClient conectarBaseDatos = null;                                               // Conexi&oacute;n a la base de datos
        MongoDatabase database;                                                             // Obtener esquema de base de datos
        MongoCollection<Document> collection;                                               // Obtener colecci&oacute;n de datos
        ArrayList<Document> query;                                                          // Lista de sentencias que confirman la consulta
        Collation collation = Collation.builder().locale("es").caseLevel(true).build();     // indica el ordenamiento para la consulta omitiendo mayusculas de minusculas y acentos
        String strNombreSeccion = "";                                                       // indica el nombre de la seccion a utilizar para filtrar sus tipos de documentos asociados
        ArrayList<TIPO_DOCUMENTO_CONTRATACIONES> listadoTipoDocumento;                          // listado de tipos de documento
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        query = new ArrayList<>();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)

                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_documento_contrataciones");

                switch(intNumeroDeSeccion)
                { // obtener el nombre de la seccion para el filtrado de la informacion (TOP)
                    case 6:
                        strNombreSeccion = "planning";
                        break;
                    case 7:
                        strNombreSeccion = "tender";
                        break;
                    case 8:
                        strNombreSeccion = "award";
                        break;
                    case 9:
                        if(intNumeroApartado == 1)
                        {
                            strNombreSeccion = "contract";
                        }
                        if(intNumeroApartado == 2)
                        {
                            strNombreSeccion = "implementation";
                        }
                        break;
                } // obtener el nombre de la seccion para el filtrado de la informacion (BOTTOM)
                
                //<editor-fold defaultstate="collapsed" desc="Consulta">
                query.add(new Document("$addFields", new Document()
                        .append("secciones", new Document()
                            .append("$split", Arrays.asList("$seccion",", "))
                        )
                    )
                );
                query.add(new Document("$project", new Document()
                        .append("seccion", new Document()
                            .append("$filter", new Document()
                                .append("input", "$secciones")
                                .append("as", "seccion")
                                .append("cond", new Document("$eq", Arrays.asList("$$seccion", strNombreSeccion)))
                            )
                        )
                        .append("clave", "$clave")
                        .append("valor", "$valor")
                        .append("categoria", "$categoria")
                ));
                query.add(
                        new Document("$unwind","$seccion")
                );
                //</editor-fold>

                listadoTipoDocumento = collection.aggregate(query, TIPO_DOCUMENTO_CONTRATACIONES.class).collation(collation).allowDiskUse(true).into(new ArrayList<>());
                
            } // Si la conexion se ha establecido(BOTTOM)
            else
            {
                throw new Exception("Conexion no establecida");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el listado de tipos de documentos de una contrataci&oacute;n: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return listadoTipoDocumento;
        
    } // metodo para obtener el tipo de documento segun la seccion consultada (BOTTOM)
    
    /**
     * Metodo para obtener los tipos de hitos permitidos para una contratacion publica 
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    public static ArrayList<TIPO_HITO> obtenerTipoHito() throws Exception
    { // metodo para obtener los tipos de hitos permitidos para una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                   // conexion a la base de datos
        MongoDatabase database;                                                 // obtener esquema de base de datos
        MongoCollection<Document> collection;                                   // obtener coleccion de datos
        ArrayList<TIPO_HITO> listadoTiposDeHito;                                 // listado de tipos de hitos
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_hito");

                listadoTiposDeHito = collection.find(TIPO_HITO.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
            
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener los tipos de hitos de una contrataci&oacute;n p&uacute;plica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return listadoTiposDeHito;
        
    } // metodo para obtener los tipos de hitos permitidos para una contratacion publica (BOTTOM)
    
    /**
     * Metodo para obtener los estados de un hito permitidos para una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    public static ArrayList<ESTADO_HITO> obtenerEstadoHito() throws Exception
    { // metodo para obtener los estados de un hito permitidos para una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                   // conexion a la base de datos
        MongoDatabase database;                                                 // obtener esquema de base de datos
        MongoCollection<Document> collection;                                   // obtener coleccion de datos
        ArrayList<ESTADO_HITO> listadoEstadosDeHito;                             // listado de estados de un hito
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("estado_hito");

                listadoEstadosDeHito = collection.find(ESTADO_HITO.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
            
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener los estados de un hito de una contrataci&oacute;n p&uacute;plica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return listadoEstadosDeHito;
        
    } // metodo para obtener los estados de un hito permitidos para una contratacion publica (BOTTOM)
    
    /**
     * Metodo para obtener el metodo de contratacion para una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    public static ArrayList<METODO_CONTRATACION> obtenerMetodoContratacion() throws Exception
    { // metodo para obtener el metodo de contratacion para una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                   // conexion a la base de datos
        MongoDatabase database;                                                 // obtener esquema de base de datos
        MongoCollection<Document> collection;                                   // obtener coleccion de datos
        ArrayList<METODO_CONTRATACION> listadoMetodosDeContratacion;            // listado de metodos de contratacion
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("metodo_contratacion");

                listadoMetodosDeContratacion = collection.find(METODO_CONTRATACION.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
            
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener los metodos de contrataci&oacute;n para una contrataci&oacute;n p&uacute;plica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return listadoMetodosDeContratacion;
        
    } // metodo para obtener el metodo de contratacion para una contratacion publica (BOTTOM)
    
    /**
     * Metodo para obtener la categoria principal de una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    public static ArrayList<CATEGORIA_PRINCIPAL_CONTRATACION> obtenerCategoriasPrincipales() throws Exception
    { // metodo para obtener la categoria principal de una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                           // conexion a la base de datos
        MongoDatabase database;                                                         // obtener esquema de base de datos
        MongoCollection<Document> collection;                                           // obtener coleccion de datos
        ArrayList<CATEGORIA_PRINCIPAL_CONTRATACION> listadoCategoriasPrincipales;       // listado de categorias principales
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("categoria_principal_contratacion");

                listadoCategoriasPrincipales = collection.find(CATEGORIA_PRINCIPAL_CONTRATACION.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
            
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener las categor&iacute;as principales de una contrataci&oacute;n p&uacute;plica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return listadoCategoriasPrincipales;
        
    } // metodo para obtener la categoria principal de una contratacion publica (BOTTOM)
    
    /**
     * Metodo para obtener las categorias adicionales de una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    public static ArrayList<CATEGORIA_ADICIONAL_CONTRATACION> obtenerCategoriasAdicionales() throws Exception
    { // metodo para obtener las categorias adicionales de una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                           // conexion a la base de datos
        MongoDatabase database;                                                         // obtener esquema de base de datos
        MongoCollection<Document> collection;                                           // obtener coleccion de datos
        ArrayList<CATEGORIA_ADICIONAL_CONTRATACION> listadoCategoriasAdicionales;       // listado de categorias adicionales
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("categoria_adicional_contratacion");

                listadoCategoriasAdicionales = collection.find(CATEGORIA_ADICIONAL_CONTRATACION.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
            
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener las categor&iacute;as adicionales de una contrataci&oacute;n p&uacute;plica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return listadoCategoriasAdicionales;
        
    } // metodo para obtener las categorias adicionales de una contratacion publica (BOTTOM)
    
    /**
     * Metodo para obtener los criterios de adjudicacions de una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    public static ArrayList<CRITERIO_ADJUDICACION> obtenerCriteriosDeAdjudicacion() throws Exception
    { // metodo para obtener los criterios de adjudicacions de una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                           // conexion a la base de datos
        MongoDatabase database;                                                         // obtener esquema de base de datos
        MongoCollection<Document> collection;                                           // obtener coleccion de datos
        ArrayList<CRITERIO_ADJUDICACION> listadoCriterios;                              // listado de criterios de adjudicacion
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("criterio_adjudicacion");

                listadoCriterios = collection.find(CRITERIO_ADJUDICACION.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
            
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener los criterios de adjudicac&oacute;n de una contrataci&oacute;n p&uacute;plica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return listadoCriterios;
        
    } // metodo para obtener los criterios de adjudicacions de una contratacion publica (BOTTOM)
    
    /**
     * Metodo para obtener los metodos de presentacion de una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    public static ArrayList<METODO_PRESENTACION> obtenerMetodosDePresentacion() throws Exception
    { // metodo para obtener los metodos de presentacion de una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                           // conexion a la base de datos
        MongoDatabase database;                                                         // obtener esquema de base de datos
        MongoCollection<Document> collection;                                           // obtener coleccion de datos
        ArrayList<METODO_PRESENTACION> listadoMetodos;                                  // listado de metodos de presentacion
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("metodo_presentacion");

                listadoMetodos = collection.find(METODO_PRESENTACION.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
            
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener los metodos de presentacic&oacute;n de una contrataci&oacute;n p&uacute;plica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return listadoMetodos;
        
    } // metodo para obtener los metodos de presentacion de una contratacion publica (BOTTOM)
    
    /**
     * Metodo para obtener los estados de una adjudicacion en una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    public static ArrayList<ESTADO_ADJUDICACION> obtenerEstadosAdjudicacion() throws Exception
    { // metodo para obtener los estados de una adjudicacion en una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                           // conexion a la base de datos
        MongoDatabase database;                                                         // obtener esquema de base de datos
        MongoCollection<Document> collection;                                           // obtener coleccion de datos
        ArrayList<ESTADO_ADJUDICACION> listadoEstados;                                  // listado de estados de una adjudicacion
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("estado_adjudicacion");

                listadoEstados = collection.find(ESTADO_ADJUDICACION.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
            
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener los estados de una adjudicacic&oacute;n en una contrataci&oacute;n p&uacute;plica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return listadoEstados;
        
    } // metodo para obtener los estados de una adjudicacion en una contratacion publica (BOTTOM)
    
    /**
     * Metodo para obtener los estados de un contrato en una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    public static ArrayList<ESTADO_CONTRATO> obtenerEstadosContrato() throws Exception
    { // metodo para obtener los estados de un contrato en una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                           // conexion a la base de datos
        MongoDatabase database;                                                         // obtener esquema de base de datos
        MongoCollection<Document> collection;                                           // obtener coleccion de datos
        ArrayList<ESTADO_CONTRATO> listadoEstados;                                      // listado de estados de un contrato
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("estado_contrato");

                listadoEstados = collection.find(ESTADO_CONTRATO.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
            
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener los estados de un contrato en una contrataci&oacute;n p&uacute;plica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return listadoEstados;
        
    } // metodo para obtener los estados de un contrato en una contratacion publica (BOTTOM)
    
    /**
     * Metodo para obtener los esquemas de una unidad en una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    public static ArrayList<ESQUEMA_UNIDAD> obtenerEsquemaUnidad() throws Exception
    { // metodo para obtener los esquemas de una unidad en una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                           // conexion a la base de datos
        MongoDatabase database;                                                         // obtener esquema de base de datos
        MongoCollection<Document> collection;                                           // obtener coleccion de datos
        ArrayList<ESQUEMA_UNIDAD> listadoEsquemas;                                      // listado de esquemas de una unidad
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("esquema_unidad");

                listadoEsquemas = collection.find(ESQUEMA_UNIDAD.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
            
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener los esquemas de una unidad en una contrataci&oacute;n p&uacute;plica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return listadoEsquemas;
        
    } // metodo para obtener los esquemas de una unidad en una contratacion publica (BOTTOM)
    
    /**
     * Metodo para obtener los esquemas de una clasificacion en una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    public static ArrayList<ESQUEMA_CLASIFICACION> obtenerEsquemaClasificacion() throws Exception
    { // metodo para obtener los esquemas de una clasificacion en una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                           // conexion a la base de datos
        MongoDatabase database;                                                         // obtener esquema de base de datos
        MongoCollection<Document> collection;                                           // obtener coleccion de datos
        ArrayList<ESQUEMA_CLASIFICACION> listadoEsquemas;                               // listado de esquemas de una clasificacion
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("esquema_clasificacion");

                listadoEsquemas = collection.find(ESQUEMA_CLASIFICACION.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
            
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener los esquemas de una clasificaci&oacute;n en una contrataci&oacute;n p&uacute;plica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return listadoEsquemas;
        
    } // metodo para obtener los esquemas de una clasificacion en una contratacion publica (BOTTOM)
    
    /**
     * Metodo para obtener las relaciones de un proceso en una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    public static ArrayList<RELACION_PROCESO> obtenerRelacionProceso() throws Exception
    { // metodo para obtener las relaciones de un proceso en una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                           // conexion a la base de datos
        MongoDatabase database;                                                         // obtener esquema de base de datos
        MongoCollection<Document> collection;                                           // obtener coleccion de datos
        ArrayList<RELACION_PROCESO> listadoRelacion;                                    // listado de relaciones de un proceso
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("relacion_proceso");

                listadoRelacion = collection.find(RELACION_PROCESO.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
            
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el listado de relaciones de un proceso en una contrataci&oacute;n p&uacute;plica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return listadoRelacion;
        
    } // metodo para obtener las relaciones de un proceso en una contratacion publica (BOTTOM)
    
    /**
     * Metodo para obtener el esquema de un proceso en una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    public static ArrayList<ESQUEMA_PROCESO> obtenerEsquemaProceso() throws Exception
    { // metodo para obtener el esquema de un proceso en una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                           // conexion a la base de datos
        MongoDatabase database;                                                         // obtener esquema de base de datos
        MongoCollection<Document> collection;                                           // obtener coleccion de datos
        ArrayList<ESQUEMA_PROCESO> listadoEsquema;                                     // listado de esquemas de un proceso
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("esquema_proceso");

                listadoEsquema = collection.find(ESQUEMA_PROCESO.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
            
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el esquema de un proceso en una contrataci&oacute;n p&uacute;plica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return listadoEsquema;
        
    } // metodo para obtener el esquema de un proceso en una contratacion publica (BOTTOM)
    
    /**
     * Metodo para obtener el tipos de moneda permitido para una contratacion publica 
     *
     * @return ArrayList MONEDA
     * @throws java.lang.Exception
     */
    public static ArrayList<MONEDA> obtenerListaDeTiposDeMoneda() throws Exception
    { // metodo para obtener el tipos de moneda permitido para una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                               // Conexi&oacute;n a la base de datos
        MongoDatabase database;                                                             // Obtener esquema de base de datos
        MongoCollection<Document> collection;                                               // Obtener colecci&oacute;n de datos
        ArrayList<Document> query;                                                          // Lista de sentencias que confirman la consulta
        Collation collation = Collation.builder().locale("es").caseLevel(true).build();     // indica el ordenamiento para la consulta omitiendo mayusculas de minusculas y acentos
        ArrayList<MONEDA> listadoMonedas;                                                   // listado de monedas
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        query = new ArrayList<>();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)

                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("moneda");
                
                //<editor-fold defaultstate="collapsed" desc="Consulta">
                query.add(new Document("$match", new Document()
                        .append("$or",  Arrays.asList(
                                new Document("clave", new Document("$eq", "MXN")),
                                new Document("clave", new Document("$eq", "USD"))
                            )
                        )
                    )
                );
                //</editor-fold>

                listadoMonedas = collection.aggregate(query, MONEDA.class).collation(collation).allowDiskUse(true).into(new ArrayList<>());
                
            } // Si la conexion se ha establecido(BOTTOM)
            else
            {
                throw new Exception("Conexion no establecida");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el tipo de moneda de una contrataci&oacute;n p&uacute;plica:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return listadoMonedas;
        
    } // metodo para obtener el tipos de moneda permitido para una contratacion publica (BOTTOM)
    
    /**
     * Metodo para obtener el tipo de idioma permitido para una contratacion publica 
     *
     * @return ArrayList IDIOMA_CONTRATACIONES
     * @throws java.lang.Exception
     */
    public static ArrayList<IDIOMA_CONTRATACIONES> obtenerListaDeTiposDeIdioma() throws Exception
    { // metodo para obtener el tipo de idioma permitido para una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                           // conexion a la base de datos
        MongoDatabase database;                                                         // obtener esquema de base de datos
        MongoCollection<Document> collection;                                           // obtener coleccion de datos
        ArrayList<IDIOMA_CONTRATACIONES> listadoDeIdiomas;                              // listado de idiomas
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("idioma_contrataciones");

                listadoDeIdiomas = collection.find(IDIOMA_CONTRATACIONES.class).sort(new Document("nombre", 1)).into(new ArrayList<>());

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
            
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el tipo de idioma de una contrataci&oacute;n p&uacute;plica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        
        return listadoDeIdiomas;
        
    } // metodo para obtener el tipo de idioma permitido para una contratacion publica (BOTTOM)
    
    /**
     * Metodo para eliminar una contratacion publica
     *
     * @param id Identificador del servidor p&uacute;blico
     * @param usuario Usuario que realiza la modificaci&oacute;n
     * @param nivelAcceso Nivel que tiene el usuario para interactuar en el
     * sistema
     * @param contratacion
     *
     * @return Boolean
     *
     * @throws Exception
     */
    public static boolean eliminarContratacion(String id, USUARIO usuario, NIVEL_ACCESO nivelAcceso, ContratacionesContratacion contratacion) throws Exception
    { // metodo para eliminar una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                   // Conexi&oacute;n a la base de datos
        MongoDatabase database;                                                 // Obtener esquema de base de datos
        MongoCollection<ContratacionesContratacion> collection;                               // Obtener colecci&oacute;n de datos        
        Document query;                                                         // Documento
        boolean seBorra;                                                        // Permite saber si el registro se manipula
        ExclusionStrategy strategy;                                             // permite excluir ciertos campos de ser serializados
        Gson gson;                                                              // liberia que permite el mapeo de datos
        Document registroContratacion;                                          // Estructura final que se guarda a la base de datos
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        strategy = new ExclusionStrategy()
        {
            @Override
            public boolean shouldSkipClass(Class<?> clazz)
            {
                return false;
            }

            @Override
            public boolean shouldSkipField(FieldAttributes field)
            {
                return field.getAnnotation(Exclude.class) != null;
            }
        };

        gson = new GsonBuilder()
                .serializeNulls()
                .addSerializationExclusionStrategy(strategy)
                .create();

        query = new Document();
        seBorra = false;

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("contrataciones", ContratacionesContratacion.class);

                if (nivelAcceso.getId() == 1)
                { // si es admin (TOP)
                    query.append("_id", new ObjectId(id));
                    seBorra = true;
                } // si es admin (BOTTOM)
                else if (nivelAcceso.getId() == 2)
                { // si puede capturar (TOP)
                    query.append("_id", new ObjectId(id));
                    query.append("metadatos.usuario_registro", usuario.getUsuario());
                    seBorra = true;
                } // si puede capturar (BOTTOM)
                if (seBorra)
                { // en caso de los niveles 1, 2 (TOP)
                    collection.deleteOne(query);
                    registroContratacion = Document.parse(gson.toJson(contratacion));
                    //<editor-fold defaultstate="collapsed" desc="asignar y registrar informacion en la bitacora">
                    BitacoraPde.registrarBitacora(new TBITACORAS_PDE(new ManejoDeFechas().getDateNowAsString(), usuario.getUsuario(), BitacoraPde.contrataciones, "SAEMM", contratacion.getId(), registroContratacion.toJson(), BitacoraPde.borrado));
                    //</editor-fold>
                } // en caso de los niveles 1, 2 (BOTTOM)

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida");
            } // No se ha establecido la conexion (BOTTOM)
        }
        catch (Exception ex)
        {
            throw new Exception("Error al eliminar una contrataci&oacute;n p&uacute;blica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return seBorra;
        
    } // metodo para eliminar una contratacion publica (BOTTOM)
    
    /**
     * Metodo para publicar una contratacion publica 
     *
     * @param id Id del registro a publicar
     * @param contratacion
     * @param usuario Usuario que publica
     * @param nivelAcceso Nivel que tiene el usuario para interactuar en el
     * sistema
     *
     * @return
     *
     * @throws Exception
     */
    public static boolean publicarContratacion(String id, ContratacionesContratacion contratacion, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    { // metodo para publicar una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                   // Conexi&oacute;n a la base de datos
        MongoDatabase database;                                                 // Obtener esquema de base de datos
        MongoCollection<Document> collection;                                   // Obtener colecci&oacute;n de datos        
        Document where;                                                         // Condiciones
        boolean sePublica;                                                      // Permite saber si el registro se manipula
        ExclusionStrategy strategy;                                             // permite excluir ciertos campos de ser serializados
        Gson gson;                                                              // liberia que permite el mapeo de datos
        Document registroContratacion;                                          // Estructura final que se guarda a la base de datos
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        strategy = new ExclusionStrategy()
        {
            @Override
            public boolean shouldSkipClass(Class<?> clazz)
            {
                return false;
            }

            @Override
            public boolean shouldSkipField(FieldAttributes field)
            {
                return field.getAnnotation(Exclude.class) != null;
            }
        };

        gson = new GsonBuilder()
                .serializeNulls()
                .addSerializationExclusionStrategy(strategy)
                .create();
        where = new Document();
        sePublica = false;
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("contrataciones");

                if (nivelAcceso.getId() == 1)
                { // si es admin (TOP)
                    where.append("_id", new ObjectId(id));
                    sePublica = true;
                } // si es admin (BOTTOM)
                else if (nivelAcceso.getId() == 2)
                { // si puede capturar (TOP)
                    where.append("_id", new ObjectId(id));
                    where.append("metadatos.usuario_registro", usuario.getUsuario());
                    sePublica = true;
                } // si puede capturar (BOTTOM)
                if (sePublica)
                { // en caso de los niveles 1, 2 (TOP)
                    registroContratacion = Document.parse(gson.toJson(contratacion));
                    collection.updateOne(where, new Document("$set", registroContratacion));
                    //<editor-fold defaultstate="collapsed" desc="asignar y registrar informacion en la bitacora">
                    BitacoraPde.registrarBitacora(new TBITACORAS_PDE(new ManejoDeFechas().getDateNowAsString(), usuario.getUsuario(), BitacoraPde.contrataciones, "SAEMM", contratacion.getId(), registroContratacion.toJson(), BitacoraPde.publicacion));
                    //</editor-fold>
                } // en caso de los niveles 1, 2 (BOTTOM)

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida");
            } // No se ha establecido la conexion (BOTTOM)
        }
        catch (Exception ex)
        {
            throw new Exception("Error al publicar una contrataci&oacute;n p&uacute;blica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return sePublica;
        
    } // metodo para publicar una contratacion publica (BOTTOM)
    
    /**
     * Metodo para despublicar una contratacion publica
     *
     * @param id 
     * @param contratacion
     * @param usuario 
     * @param nivelAcceso 
     * sistema
     *
     * @return Boolean
     *
     * @throws Exception
     */
    public static boolean despublicarContrataciones(String id, ContratacionesContratacion contratacion, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    { // metodo para despublicar una contratacion publica (TOP)
        
        MongoClient conectarBaseDatos = null;                                   // Conexi&oacute;n a la base de datos
        MongoDatabase database;                                                 // Obtener esquema de base de datos
        MongoCollection<Document> collection;                                   // Obtener colecci&oacute;n de datos        
        Document where;                                                         // Condiciones
        boolean seDesPublica;                                                   // Permite saber si el registro se manipula
        ExclusionStrategy strategy;                                             // permite excluir ciertos campos de ser serializados
        Gson gson;                                                              // liberia que permite el mapeo de datos
        Document registroContratacion;                                          // Estructura final que se guarda a la base de datos
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        strategy = new ExclusionStrategy()
        {
            @Override
            public boolean shouldSkipClass(Class<?> clazz)
            {
                return false;
            }

            @Override
            public boolean shouldSkipField(FieldAttributes field)
            {
                return field.getAnnotation(Exclude.class) != null;
            }
        };

        gson = new GsonBuilder()
                .serializeNulls()
                .addSerializationExclusionStrategy(strategy)
                .create();
        where = new Document();
        seDesPublica = false;
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("contrataciones");

                if (nivelAcceso.getId() == 1)
                { // si es admin (TOP)
                    where.append("_id", new ObjectId(id));
                    seDesPublica = true;
                } // si es admin (BOTTOM)
                else if (nivelAcceso.getId() == 2)
                { // si puede capturar (TOP)
                    where.append("_id", new ObjectId(id));
                    where.append("metadatos.usuario_registro", usuario.getUsuario());
                    seDesPublica = true;
                } // si puede capturar (BOTTOM)
                if (seDesPublica)
                { // en caso de los niveles 1, 2 (TOP)
                    registroContratacion = Document.parse(gson.toJson(contratacion));
                    collection.updateOne(where, new Document("$set", registroContratacion));
                    //<editor-fold defaultstate="collapsed" desc="asignar y registrar informacion en la bitacora">
                    BitacoraPde.registrarBitacora(new TBITACORAS_PDE(new ManejoDeFechas().getDateNowAsString(), usuario.getUsuario(), BitacoraPde.contrataciones, "SAEMM", contratacion.getId(), registroContratacion.toJson(), BitacoraPde.despublicacion));
                    //</editor-fold>
                } // en caso de los niveles 1, 2 (BOTTOM)

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida");
            } // No se ha establecido la conexion (BOTTOM)
        }
        catch (Exception ex)
        {
            throw new Exception("Error al despublicar una cntrataci&oacute;n p&uacute;blica: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return seDesPublica;
        
    }  // metodo para despublicar una contratacion publica (BOTTOM)
    
    /**
     * obtener listado de entes publicos asignados al usuario, si es
     * administrador obtendrá toda la lista
     *
     * @param dependencias 
     * @param usuario 
     *
     * @return Lista de entes publicos asignados al usuario
     *
     * @throws Exception
     */
    public static ArrayList<ENTE_PUBLICO> obtenerEntesPublicos(ArrayList<ENTE_PUBLICO> dependencias, USUARIO usuario) throws Exception
    { // obtener listado de entes publicos asignados al usuario (TOP)
        
        MongoClient conectarBaseDatos = null;                                   // conexion a la base de datos
        MongoDatabase database;                                                 // obtener esquema de base de datos
        MongoCollection<Document> collection;                                   // obtener coleccion de datos
        ArrayList<ENTE_PUBLICO> listadoProcedimientos;                          // listado del tipo de area
        Boolean esAdmin = false;                                                // identificar si es admin, para saber cuantos entes publicos obtener
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // si la conexion se ha establecido (TOP)
                
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("ente_publico");
                List<Document> where = new ArrayList<>();

                Document or = new Document();

                for (SISTEMAS sistema : usuario.getSistemas())
                { // verificacion en sistemas asignados (TOP)
                    if (sistema.getSistema().getId() == 6)
                    { // validacion de nivel de acceso admin al sistema 6 (TOP)
                        if (sistema.getNivel_acceso().getId() == 1)
                        { // es admin (TOP)
                            esAdmin = true;
                        } // es admin (BOTTOM)
                    } // validacion de nivel de acceso admin al sistema 6 (BOTTOM)
                } // verificacion en sistemas asignados (BOTTOM)

                if (!esAdmin)
                { // no es usuario con privilegios de administrador (TOP)
                    for (ENTE_PUBLICO dependencia : dependencias)
                    { // Dependencias asignadas al servidor publico (TOP)
                        where.add(new Document("clave", dependencia.getClave()));
                    } // Dependencias asignadas al servidor publico (BOTTOM)
                    or.append("$or", where);
                } // no es usuario con privilegios de administrador (BOTTOM)

                listadoProcedimientos = collection.find(or, ENTE_PUBLICO.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } // si la conexion se ha establecido(BOTTOM)
            else
            { // no se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // no se ha establecido la conexion (BOTTOM)
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener entes p&uacute;blicos:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listadoProcedimientos;
        
    } // obtener listado de entes publicos asignados al usuario (BOTTOM)
    
    /**
     * Metodo para obtener el estado general de las dependencias con respecto a la carga de evidencia por anio
     *
     *
     * @param arregloDePerios
     * @param dependencia
     * @param filtroBusqueda
     * 
     * @return Estado de seguimiento
     *
     * @throws Exception
     */
    public static CargaDeEvidenciaVista obtenerSeguimientoCargaDeEvidenciaContrataciones(ArrayList<CARGA_DE_EVIDENCIA_PERIODO> arregloDePerios, TSEGUIMIENTO_CARGA_DE_EVIDENCIA dependencia, FiltrosCargaDeEvidenciaPDE filtroBusqueda) throws Exception
    { // metodo para obtener el estado general de las dependencias con respecto a la carga de evidencia por anio (TOP)
        
        MongoClient conectarBaseDatos = null;                                   // conexion a la base de datos
        MongoDatabase database;                                                 // obtener esquema de base de datos
        MongoCollection<Document> collection;                                   // obtener coleccion de datos
        AggregateIterable<Document> aIPuesto;                                   // iterable de objetos obtenidos                
        MongoCursor<Document> cursor = null;                                    // cursor del documento
        JsonParser parser;                                                      // transforma el resultado de la consulta
        CargaDeEvidenciaVista cargaDeEvidencia = null;                                 // modelo que almacenara los resultados de la busqueda
        List<Document> script;                                                  // documento que almacena la estructura de toda la consulta
        JsonElement jsonTree;                                                   // almacena el resultado de la consulta en json para el mapeo resultante
        String strFechaInicialIterador;                                         // variable de ayuda para identificar la fecha inicial en los periodos de tiempo utilizados (String)
        String strFechaFinalIterador;                                           // variable de ayuda para identificar la fecha final en los periodos de tiempo utilizados (String)     
        Date fechaInicialIterador;                                              // variable de ayuda para identificar la fecha inicial en los periodos de tiempo utilizados (Date)
        Date fechaFinalIterador;                                                // variable de ayuda para identificar la fecha inicial en los periodos de tiempo utilizados (Date)
        Document groupRegistros;                                                
        Document addFieldsRegistros;    
        Document groupLookup;
        Document addFieldsLookup;
        Document projectRegistros;
        Document addFieldsConteo;
        Document addFieldsEstadoMes;
        List<String> projectIntervalos;
        Document groupDependencias;
        Document projectDependencias;
        List<Document> dataDependencias;
        int intTotalIntervalos;
        int intervaloEsperadoPorAnio;
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        groupDependencias = new Document();
        projectDependencias = new Document();
        dataDependencias = new ArrayList<>();
        script = new ArrayList();
        parser = new JsonParser();
        groupRegistros = new Document();
        addFieldsRegistros = new Document();
        groupLookup = new Document();
        addFieldsLookup = new Document();
        projectRegistros = new Document();
        addFieldsConteo = new Document();
        addFieldsEstadoMes = new Document();
        projectIntervalos = new ArrayList();
        intTotalIntervalos = 0;
        
        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // si la conexion se ha establecido(TOP)

                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("contrataciones");

                //<editor-fold defaultstate="collapsed" desc="consulta">
                //<editor-fold defaultstate="collapsed" desc="agrupar por dependencia">
                groupDependencias.append("_id", 0);
                groupDependencias.append("vectorDependencia", new Document()
                    .append("$push", new Document()
                        .append("$cond", Arrays.asList(
                            new Document("$eq", Arrays.asList("$metadata.institucion", dependencia.getDependencia())),
                            "$fechaRegistro",
                            "$$REMOVE"
                        ))
                    )
                );
                projectDependencias.append("dep", new Document()
                    .append("dependencia", dependencia.getDependencia())
                    .append("clave", dependencia.getClave())
                    .append("registros", "$vectorDependencia")
                );
                dataDependencias.add(new Document()
                    .append("dependencia", "$dep.dependencia")
                    .append("clave", "$dep.clave")
                    .append("registros", "$dep.registros")
                );
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="armado para iteraciones">
                groupRegistros.append("_id", new Document()
                    .append("dependencia", "$datos.dependencia")
                    .append("clave", "$datos.clave")
                );
                groupLookup.append("_id","$dependencia");
                
                for(int i = 0; i < arregloDePerios.size(); i++)
                { // armar consulta utilizando los intervalos de tiempo obtenidos (TOP)
                    
                    strFechaInicialIterador = arregloDePerios.get(i).getFechaInicial();
                    strFechaFinalIterador = arregloDePerios.get(i).getFechaFinal();

                    fechaInicialIterador = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(strFechaInicialIterador);
                    fechaFinalIterador = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(strFechaFinalIterador);
                    
                    //<editor-fold defaultstate="collapsed" desc="armado consulta principal">
                    groupRegistros.append("intervalo_registros_".concat(String.valueOf(i + 1)), new Document()
                        .append("$push", new Document()
                            .append("$cond", Arrays.asList(
                                new Document("$and", Arrays.asList(
                                    new Document("$gte", Arrays.asList("$datos.registros", fechaInicialIterador)),
                                    new Document("$lte", Arrays.asList("$datos.registros", fechaFinalIterador))
                                )),
                                "$datos.registros",
                                "$$REMOVE"
                            ))
                        )
                    );
                    addFieldsRegistros.append("intervalo_".concat(String.valueOf(i + 1)), new Document()
                        .append("fechaInicialString", strFechaInicialIterador)
                        .append("fechaFinalString", strFechaFinalIterador)
                        .append("numeroDeIntervalo", i + 1)
                        .append("registros", new Document()
                            .append("$size","$intervalo_registros_".concat(String.valueOf(i + 1)))
                        )
                    );
                    projectRegistros.append("intervalo_".concat(String.valueOf(i + 1)), new Document()
                        .append("fechaInicialString","$intervalo_".concat(String.valueOf(i + 1)).concat(".fechaInicialString"))
                        .append("fechaFinalString","$intervalo_".concat(String.valueOf(i + 1)).concat(".fechaFinalString"))
                        .append("totalRegistros","$intervalo_".concat(String.valueOf(i + 1)).concat(".registros"))
                        .append("numeroDeIntervalo", i + 1)
                        .append("archivos", new Document()
                            .append("$filter", new Document()
                                .append("input","$archivos")
                                .append("as","archivo")
                                .append("cond", new Document()
                                    .append("$eq", Arrays.asList("$_id.dependencia", "$$archivo._id"))
                                )
                            )
                        )
                    );
                    addFieldsConteo.append("intervalo_".concat(String.valueOf(i + 1)), new Document()
                        .append("totalArchivos", new Document()
                            .append("$cond", Arrays.asList(
                                new Document("$eq", Arrays.asList(
                                    "$intervalo_".concat(String.valueOf(i + 1)).concat(".numeroDeIntervalo"), "$intervalo_".concat(String.valueOf(i + 1)).concat(".archivos.intervalo_").concat(String.valueOf(i + 1)).concat(".numeroDeIntervalo")
                                )),
                                "$intervalo_".concat(String.valueOf(i + 1)).concat(".archivos.intervalo_").concat(String.valueOf(i + 1)).concat(".registros"),
                                0
                            ))
                        )
                        .append("fechaAuxiliar", new Document()
                            .append("$dateToString", new Document()
                                .append("format", "%Y-%m-%d")
                                .append("date", new Document()
                                    .append("$dateFromString", new Document()
                                        .append("dateString", "$intervalo_".concat(String.valueOf(i + 1)).concat(".fechaInicialString"))
                                    )
                                )
                            )
                        )
                    );
                    addFieldsEstadoMes.append("intervalo_".concat(String.valueOf(i + 1)), new Document()
                        .append("anio", new Document()
                            .append("$year", new Document()
                                .append("$dateFromString", new Document()
                                    .append("dateString","$intervalo_".concat(String.valueOf(i + 1)).concat(".fechaAuxiliar"))
                                    .append("timezone","America/Mexico_City")
                                )
                            )
                        )
                        .append("estadoIntervalo", new Document()
                            .append("$cond", Arrays.asList(
                                new Document("$gt", Arrays.asList(
                                    new Document("$sum", Arrays.asList(
                                        "$intervalo_".concat(String.valueOf(i + 1)).concat(".totalArchivos"),
                                        "$intervalo_".concat(String.valueOf(i + 1)).concat(".totalRegistros")
                                    )),
                                    0    
                                )),
                                "1",
                                "0"
                            ))
                        )
                    );
                    projectIntervalos.add("$intervalo_".concat(String.valueOf(i + 1)));
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="armado consultas lookup">
                    groupLookup.append("intervalo_archivos_".concat(String.valueOf(i + 1)), new Document()
                        .append("$push", new Document()
                            .append("$cond", Arrays.asList(
                                new Document("$and", Arrays.asList(
                                    new Document("$gte", Arrays.asList("$fechaInicial", fechaInicialIterador)),
                                    new Document("$lte", Arrays.asList("$fechaFinal", fechaFinalIterador))
                                )),
                                "$fecha_registro",
                                "$$REMOVE"
                            ))
                        )
                    );
                    addFieldsLookup.append("intervalo_".concat(String.valueOf(i + 1)), new Document()
                        .append("fechaInicialString", strFechaInicialIterador)
                        .append("fechaFinalString", strFechaFinalIterador)
                        .append("numeroDeIntervalo", i + 1)
                        .append("registros", new Document()
                            .append("$size","$intervalo_archivos_".concat(String.valueOf(i + 1)))
                        )
                    );
                    //</editor-fold>
                    
                    intTotalIntervalos++;
                    
                } // armar consulta utilizando los intervalos de tiempo obtenidos (BOTTOM)
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="obtener intervalos por anio">
                intervaloEsperadoPorAnio = intTotalIntervalos;
                //</editor-fold>
                script.add(new Document()
                    .append("$addFields", new Document()
                        .append("fechaRegistro", new Document()
                            .append("$dateToString", new Document()
                                .append("format", "%Y-%m-%d")
                                .append("date", new Document()
                                    .append("$dateFromString", new Document()
                                        .append("dateString", "$metadatos.fecha_registro")
                                    )
                                )
                            )
                        )
                    )
                );
                script.add(new Document()
                    .append("$addFields", new Document()
                        .append("fechaRegistro", new Document()
                            .append("$dateFromString", new Document()
                                .append("dateString","$fechaRegistro")
                                .append("timezone","America/Mexico_City")
                            )
                        )
                    )
                );
                script.add(new Document("$group", groupDependencias));
                script.add(new Document("$project", projectDependencias));
                script.add(new Document("$project", new Document()
                    .append("datos", dataDependencias)
                ));
                script.add(new Document()
                    .append("$unwind", new Document()
                        .append("path","$datos")
                        .append("preserveNullAndEmptyArrays",true)
                    )
                );
                script.add(new Document()
                    .append("$unwind", new Document()
                        .append("path","$datos.registros")
                        .append("preserveNullAndEmptyArrays",true)
                    )
                );
                script.add(new Document("$group", groupRegistros));
                script.add(new Document("$addFields", addFieldsRegistros));
                script.add(new Document()
                    .append("$lookup", new Document()
                        .append("from", "tcarga_de_evidencia")
                        .append("pipeline", Arrays.asList(
                            new Document("$addFields", new Document()
                                .append("fechaInicial", new Document()
                                    .append("$dateToString", new Document()
                                        .append("format", "%Y-%m-%d")
                                        .append("date", new Document()
                                            .append("$dateFromString", new Document()
                                                .append("dateString", "$periodo.fechaInicial")
                                            )
                                        )
                                    )
                                )
                                .append("fechaFinal", new Document()
                                    .append("$dateToString", new Document()
                                        .append("format", "%Y-%m-%d")
                                        .append("date", new Document()
                                            .append("$dateFromString", new Document()
                                                .append("dateString", "$periodo.fechaFinal")
                                            )
                                        )
                                    )
                                )
                            ),
                            new Document("$addFields", new Document()
                                .append("fechaInicial", new Document()
                                    .append("$dateFromString", new Document()
                                        .append("dateString","$fechaInicial")
                                        .append("timezone","America/Mexico_City")
                                    )
                                )
                                .append("fechaFinal", new Document()
                                    .append("$dateFromString", new Document()
                                        .append("dateString","$fechaFinal")
                                        .append("timezone","America/Mexico_City")
                                    )
                                )
                            ),
                            new Document("$match", new Document()
                                .append("sistema", "6")
                                .append("status", "1")
                            ),
                            new Document("$group", groupLookup),
                            new Document("$addFields", addFieldsLookup)
                        ))
                        .append("as", "archivos")
                    )
                );
                script.add(new Document("$project", projectRegistros));
                //<editor-fold defaultstate="collapsed" desc="separar arreglos">
                while(intTotalIntervalos > 0)
                {
                    script.add(new Document()
                        .append("$unwind", new Document()
                            .append("path","$intervalo_".concat(String.valueOf(intTotalIntervalos)).concat(".archivos"))
                            .append("preserveNullAndEmptyArrays",true)
                        )
                    );
                    intTotalIntervalos--;
                }
                //</editor-fold>
                script.add(new Document("$addFields", addFieldsConteo));
                script.add(new Document("$addFields", addFieldsEstadoMes));
                script.add(new Document("$project", new Document()
                    .append("intervalos", projectIntervalos)
                ));
                script.add(new Document("$unwind", new Document()
                    .append("path", "$intervalos")
                    .append("preserveNullAndEmptyArrays", true)
                ));
                script.add(new Document()
                    .append("$group", new Document()
                        .append("_id", new Document()
                            .append("dependencia", "$_id.dependencia")
                            .append("clave", "$_id.clave")
                            .append("anio","$intervalos.anio")
                        )
                        .append("estadosCompleto", new Document()
                            .append("$push", new Document()
                                .append("$cond", Arrays.asList(
                                    new Document("$eq", Arrays.asList(
                                        "$intervalos.estadoIntervalo", "1"
                                    )),
                                    "$intervalos.estadoIntervalo",
                                    "$$REMOVE"
                                ))
                            )
                        )
                        .append("estadosSinSeguimiento", new Document()
                            .append("$push", new Document()
                                .append("$cond", Arrays.asList(
                                    new Document("$eq", Arrays.asList(
                                        "$intervalos.estadoIntervalo", "0"
                                    )),
                                    "$intervalos.estadoIntervalo",
                                    "$$REMOVE"
                                ))
                            )
                        )
                    )
                );
                script.add(new Document()
                    .append("$addFields", new Document()
                        .append("totalIteracionesEsperadas", intervaloEsperadoPorAnio)
                    )
                );
                script.add(new Document()
                    .append("$addFields", new Document()
                        .append("estadoGeneral", new Document()
                            .append("$switch", new Document()
                                .append("branches", Arrays.asList(
                                    new Document("case", new Document()
                                        .append("$eq", Arrays.asList(new Document("$size","$estadosCompleto"), "$totalIteracionesEsperadas"))
                                    )
                                    .append("then", "1"),
                                    new Document("case", new Document()
                                        .append("$eq", Arrays.asList(new Document("$size","$estadosSinSeguimiento"), "$totalIteracionesEsperadas"))
                                    )
                                    .append("then", "0")
                                ))
                                .append("default", "2")
                            )
                        )
                    )
                );
                script.add(new Document()
                    .append("$sort", new Document()
                        .append("_id.dependencias", 1)
                    )
                );
                //</editor-fold>
                Collation collation = Collation.builder()
                        .locale("es")
                        .caseLevel(true)
                        .build();
                aIPuesto = collection.aggregate(script).collation(collation);
                cursor = aIPuesto.iterator();

                if (cursor.hasNext())
                { // si la consulta contiene elementos (TOP)
                    
                    jsonTree = parser.parse(cursor.next().toJson());

                    if (jsonTree.isJsonObject())
                    { 

                        cargaDeEvidencia = new CargaDeEvidenciaVista();
                        JsonObject jsonObject = jsonTree.getAsJsonObject();
                        JsonObject elementosDelId = jsonObject.get("_id").getAsJsonObject();

                        if (elementosDelId.isJsonObject())
                        { // si el id puede ser tratados como un objeto json (TOP)
                            cargaDeEvidencia.setDependencia(elementosDelId.get("dependencia").getAsString());
                            cargaDeEvidencia.setClave(elementosDelId.get("clave").getAsString());
                            cargaDeEvidencia.setPeriodo(String.valueOf(elementosDelId.get("anio").getAsInt()));    
                        } // si el id puede ser tratados como un objeto json (BOTTOM)

                        cargaDeEvidencia.setEstado(jsonObject.get("estadoGeneral").getAsString());

                    } 
                    
                } // si la consulta contiene elementos (BOTTOM)
                
            } // si la conexion se ha establecido(BOTTOM)
            else
            { // no se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // no se ha establecido la conexion (BOTTOM)
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el estado general de seguimiento:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursor);
        }

        return cargaDeEvidencia;
        
    } // metodo para obtener el estado general de las dependencias con respecto a la carga de evidencia por anio (BOTTOM)
    
    /**
     * Metodo para obtener los estados de una dependencias con respecto a la carga de evidencia por un anio en especifico
     *
     * @param arregloDePerios
     * @param dependencia
     * @param filtroBusqueda
     *
     * @return Listado de estados generales
     *
     * @throws Exception
     */
    public static ArrayList<CargaDeEvidenciaVista> obtenerSeguimientoDetalladoCargaDeEvidenciaContrataciones(ArrayList<CARGA_DE_EVIDENCIA_PERIODO> arregloDePerios, TSEGUIMIENTO_CARGA_DE_EVIDENCIA dependencia, FiltrosCargaDeEvidenciaPDE filtroBusqueda) throws Exception
    { // metodo para obtener los estados de una dependencias con respecto a la carga de evidencia por un anio en especifico (TOP)
        
        MongoClient conectarBaseDatos = null;                                   // conexion a la base de datos
        MongoDatabase database;                                                 // obtener esquema de base de datos
        MongoCollection<Document> collection;                                   // obtener coleccion de datos
        AggregateIterable<Document> aIPuesto;                                   // iterable de objetos obtenidos                
        MongoCursor<Document> cursor = null;                                    // cursor del documento
        JsonParser parser;                                                      // transforma el resultado de la consulta
        CargaDeEvidenciaVista cargaDeEvidencia;                                 // modelo que almacenara los resultados de la busqueda
        ArrayList<CargaDeEvidenciaVista> arregloCargaDeEvidencia;               // lista que almacena las filas resulatantes para la vista de usuario
        Document where;                                                         // almacena las condiciones  para la busqueda
        List<Document> script;                                                  // documento que almacena la estructura de toda la consulta
        JsonElement jsonTree;                                                   // almacena el resultado de la consulta en json para el mapeo resultante
        String strFechaInicialIterador;                                         // variable de ayuda para identificar la fecha inicial en los periodos de tiempo utilizados (String)
        String strFechaFinalIterador;                                           // variable de ayuda para identificar la fecha final en los periodos de tiempo utilizados (String)     
        Date fechaInicialIterador;                                              // variable de ayuda para identificar la fecha inicial en los periodos de tiempo utilizados (Date)
        Date fechaFinalIterador;                                                // variable de ayuda para identificar la fecha inicial en los periodos de tiempo utilizados (Date)
        Document groupRegistros;                                                
        Document addFieldsRegistros;                                            
        Document whereLookup;
        Document groupLookup;
        Document addFieldsLookup;
        Document addFieldsLookup_2;
        Document addFieldsLookup_3;
        Document projectRegistros;
        Document addFieldsConteo;
        Document addFieldsEstadoMes;
        Document groupDependencias;
        Document projectDependencias;
        List<Document> dataDependencias;
        List<String> projectIntervalos;
        int intTotalIntervalos;
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        groupDependencias = new Document();
        projectDependencias = new Document();
        dataDependencias = new ArrayList<>();
        where = new Document();
        script = new ArrayList();
        arregloCargaDeEvidencia = new ArrayList();
        parser = new JsonParser();
        groupRegistros = new Document();
        addFieldsRegistros = new Document();
        whereLookup = new Document();
        groupLookup = new Document();
        addFieldsLookup = new Document();
        addFieldsLookup_2 = new Document();
        addFieldsLookup_3 = new Document();
        projectRegistros = new Document();
        addFieldsConteo = new Document();
        addFieldsEstadoMes = new Document();
        projectIntervalos = new ArrayList();
        intTotalIntervalos = 0;
        
        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // si la conexion se ha establecido(TOP)

                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("contrataciones");

                //<editor-fold defaultstate="collapsed" desc="filtros de busqueda">
                //<editor-fold defaultstate="collapsed" desc="dependencia">
                where.append("_id", new Document()
                    .append("$regex", ExpresionesRegulares.crearExpresionRegularDePalabra(filtroBusqueda.getDependencia().trim()))
                    .append("$options", "i")
                );
                whereLookup.append("dependencia", new Document()
                    .append("$regex", ExpresionesRegulares.crearExpresionRegularDePalabra(filtroBusqueda.getDependencia().trim()))
                    .append("$options", "i")
                );
                whereLookup.append("sistema", "6");
                //</editor-fold>
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="consulta">
                //<editor-fold defaultstate="collapsed" desc="agrupar por dependencia">
                groupDependencias.append("_id", 0);
                groupDependencias.append("vectorDependencia", new Document()
                    .append("$push", new Document()
                        .append("$cond", Arrays.asList(
                            new Document("$eq", Arrays.asList("$metadata.institucion", dependencia.getDependencia())),
                            "$fechaRegistro",
                            "$$REMOVE"
                        ))
                    )
                );
                projectDependencias.append("dep", new Document()
                    .append("dependencia", dependencia.getDependencia())
                    .append("clave", dependencia.getClave())
                    .append("registros", "$vectorDependencia")
                );
                dataDependencias.add(new Document()
                    .append("dependencia", "$dep.dependencia")
                    .append("clave", "$dep.clave")
                    .append("registros", "$dep.registros")
                );
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="armado para iteraciones">
                groupRegistros.append("_id",new Document()
                    .append("dependencia", "$datos.dependencia")
                    .append("clave", "$datos.clave")
                );
                groupLookup.append("_id","$dependencia");
                
                for(int i = 0; i < arregloDePerios.size(); i++)
                { // armar consulta utilizando los intervalos de tiempo obtenidos (TOP)
                    
                    strFechaInicialIterador = arregloDePerios.get(i).getFechaInicial();
                    strFechaFinalIterador = arregloDePerios.get(i).getFechaFinal();

                    fechaInicialIterador = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(strFechaInicialIterador);
                    fechaFinalIterador = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(strFechaFinalIterador);
                    
                    //<editor-fold defaultstate="collapsed" desc="armado consulta principal">
                    groupRegistros.append("intervalo_registros_".concat(String.valueOf(i + 1)), new Document()
                        .append("$push", new Document()
                            .append("$cond", Arrays.asList(
                                new Document("$and", Arrays.asList(
                                    new Document("$gte", Arrays.asList("$datos.registros", fechaInicialIterador)),
                                    new Document("$lte", Arrays.asList("$datos.registros", fechaFinalIterador))
                                )),
                                "$datos.registros",
                                "$$REMOVE"
                            ))
                        )
                    );
                    addFieldsRegistros.append("intervalo_".concat(String.valueOf(i + 1)), new Document()
                        .append("fechaInicialString", strFechaInicialIterador)
                        .append("fechaFinalString", strFechaFinalIterador)
                        .append("numeroDeIntervalo", i + 1)
                        .append("registros", new Document()
                            .append("$size","$intervalo_registros_".concat(String.valueOf(i + 1)))
                        )
                    );
                    projectRegistros.append("intervalo_".concat(String.valueOf(i + 1)), new Document()
                        .append("fechaInicialString","$intervalo_".concat(String.valueOf(i + 1)).concat(".fechaInicialString"))
                        .append("fechaFinalString","$intervalo_".concat(String.valueOf(i + 1)).concat(".fechaFinalString"))
                        .append("totalRegistros","$intervalo_".concat(String.valueOf(i + 1)).concat(".registros"))
                        .append("numeroDeIntervalo", i + 1)
                        .append("archivos", new Document()
                            .append("$filter", new Document()
                                .append("input","$archivos")
                                .append("as","archivo")
                                .append("cond", new Document()
                                    .append("$eq", Arrays.asList("$_id.dependencia", "$$archivo._id"))
                                )
                            )
                        )
                    );
                    addFieldsConteo.append("intervalo_".concat(String.valueOf(i + 1)), new Document()
                        .append("totalArchivos", new Document()
                            .append("$cond", Arrays.asList(
                                new Document("$eq", Arrays.asList(
                                    "$intervalo_".concat(String.valueOf(i + 1)).concat(".numeroDeIntervalo"), "$intervalo_".concat(String.valueOf(i + 1)).concat(".archivos.intervalo_").concat(String.valueOf(i + 1)).concat(".numeroDeIntervalo")
                                )),
                                "$intervalo_".concat(String.valueOf(i + 1)).concat(".archivos.intervalo_").concat(String.valueOf(i + 1)).concat(".registrosAprobados"),
                                0
                            ))
                        )
                        .append("fechaAuxiliar", new Document()
                            .append("$dateToString", new Document()
                                .append("format", "%Y-%m-%d")
                                .append("date", new Document()
                                    .append("$dateFromString", new Document()
                                        .append("dateString", "$intervalo_".concat(String.valueOf(i + 1)).concat(".fechaInicialString"))
                                    )
                                )
                            )
                        )
                    );
                    addFieldsEstadoMes.append("intervalo_".concat(String.valueOf(i + 1)), new Document()
                        .append("anio", new Document()
                            .append("$year", new Document()
                                .append("$dateFromString", new Document()
                                    .append("dateString","$intervalo_".concat(String.valueOf(i + 1)).concat(".fechaAuxiliar"))
                                    .append("timezone","America/Mexico_City")
                                )
                            )
                        )
                        .append("estadoIntervalo", new Document()
                            .append("$cond", Arrays.asList(
                                new Document("$gt", Arrays.asList(
                                    new Document("$sum", Arrays.asList(
                                        "$intervalo_".concat(String.valueOf(i + 1)).concat(".totalArchivos"),
                                        "$intervalo_".concat(String.valueOf(i + 1)).concat(".totalRegistros")
                                    )),
                                    0    
                                )),
                                "1",
                                "0"
                            ))
                        )
                        .append("poseeUnArchivo", new Document()
                            .append("$cond", Arrays.asList(
                                new Document("$gt", Arrays.asList(
                                    "$intervalo_".concat(String.valueOf(i + 1)).concat(".archivos.intervalo_").concat(String.valueOf(i + 1)).concat(".totalArchivos"),
                                    0    
                                )),
                                true,
                                false
                            ))
                        )
                        .append("informacionArchivoAsociado", new Document()
                            .append("$arrayElemAt", Arrays.asList(
                                "$intervalo_".concat(String.valueOf(i + 1)).concat(".archivos.intervalo_".concat(String.valueOf(i + 1)).concat(".registros")), 0
                            ))
                        )
                    );
                    projectIntervalos.add("$intervalo_".concat(String.valueOf(i + 1)));
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="armado consultas lookup">
                    groupLookup.append("intervalo_archivos_".concat(String.valueOf(i + 1)), new Document()
                        .append("$push", new Document()
                            .append("$cond", Arrays.asList(
                                new Document("$and", Arrays.asList(
                                    new Document("$gte", Arrays.asList("$fechaInicial", fechaInicialIterador)),
                                    new Document("$lte", Arrays.asList("$fechaFinal", fechaFinalIterador))
                                )),
                                new Document()
                                    .append("idArchivoAsociado", "$_id")
                                    .append("statusArchivo", "$status")
                                    .append("motivoArchivoDeclinado", "$motivo_archivo_declinado"),
                                "$$REMOVE"
                            ))
                        )
                    );
                    addFieldsLookup.append("intervalo_".concat(String.valueOf(i + 1)), new Document()
                        .append("fechaInicialString", strFechaInicialIterador)
                        .append("fechaFinalString", strFechaFinalIterador)
                        .append("numeroDeIntervalo", i + 1)
                        .append("archivosAprobados", new Document()
                            .append("$filter", new Document()
                                .append("input", "$intervalo_archivos_".concat(String.valueOf(i + 1)))
                                .append("as", "archivo")
                                .append("cond", new Document()
                                    .append("$eq", Arrays.asList("$$archivo.statusArchivo", "1"))
                                )
                            )
                        )
                        .append("archivosDeclinados", new Document()
                            .append("$filter", new Document()
                                .append("input", "$intervalo_archivos_".concat(String.valueOf(i + 1)))
                                .append("as", "archivo")
                                .append("cond", new Document()
                                    .append("$eq", Arrays.asList("$$archivo.statusArchivo", "2"))
                                )
                            )
                        )
                        .append("archivosEnProceso", new Document()
                            .append("$filter", new Document()
                                .append("input", "$intervalo_archivos_".concat(String.valueOf(i + 1)))
                                .append("as", "archivo")
                                .append("cond", new Document()
                                    .append("$eq", Arrays.asList("$$archivo.statusArchivo", "0"))
                                )
                            )
                        )
                    );
                    addFieldsLookup_2.append("intervalo_".concat(String.valueOf(i + 1)), new Document()
                        .append("registrosAprobados", new Document()
                            .append("$size","$intervalo_".concat(String.valueOf(i + 1)).concat(".archivosAprobados"))
                        )
                        .append("registrosDeclinados", new Document()
                            .append("$size","$intervalo_".concat(String.valueOf(i + 1)).concat(".archivosDeclinados"))
                        )
                        .append("registrosEnProceso", new Document()
                            .append("$size","$intervalo_".concat(String.valueOf(i + 1)).concat(".archivosEnProceso"))
                        )
                    );    
                    addFieldsLookup_3.append("intervalo_".concat(String.valueOf(i + 1)), new Document()
                        .append("registros", new Document()
                            .append("$cond", Arrays.asList(
                                new Document("$gt", Arrays.asList("$intervalo_".concat(String.valueOf(i + 1)).concat(".registrosAprobados"), 0)),
                                "$intervalo_".concat(String.valueOf(i + 1)).concat(".archivosAprobados"),
                                new Document()
                                    .append("$cond", Arrays.asList(
                                        new Document("$gt", Arrays.asList("$intervalo_".concat(String.valueOf(i + 1)).concat(".registrosDeclinados"), 0)),
                                        "$intervalo_".concat(String.valueOf(i + 1)).concat(".archivosDeclinados"),
                                        new Document()
                                            .append("$cond", Arrays.asList(
                                                new Document("$gt", Arrays.asList("$intervalo_".concat(String.valueOf(i + 1)).concat(".registrosEnProceso"), 0)),
                                                "$intervalo_".concat(String.valueOf(i + 1)).concat(".archivosEnProceso"),
                                                Arrays.asList()
                                            ))
                                    ))
                            ))
                        )
                        .append("totalArchivos", new Document()
                            .append("$sum", Arrays.asList(
                                "$intervalo_".concat(String.valueOf(i + 1)).concat(".registrosAprobados"),
                                "$intervalo_".concat(String.valueOf(i + 1)).concat(".registrosDeclinados"),
                                "$intervalo_".concat(String.valueOf(i + 1)).concat(".registrosEnProceso")
                            ))
                        )
                    );               
                    //</editor-fold>
                    
                    intTotalIntervalos++;
                    
                } // armar consulta utilizando los intervalos de tiempo obtenidos (BOTTOM)
                //</editor-fold>
                script.add(new Document()
                    .append("$addFields", new Document()
                        .append("fechaRegistro", new Document()
                            .append("$dateToString", new Document()
                                .append("format", "%Y-%m-%d")
                                .append("date", new Document()
                                    .append("$dateFromString", new Document()
                                        .append("dateString", "$metadatos.fecha_registro")
                                    )
                                )
                            )
                        )
                    )
                );
                script.add(new Document()
                    .append("$addFields", new Document()
                        .append("fechaRegistro", new Document()
                            .append("$dateFromString", new Document()
                                .append("dateString","$fechaRegistro")
                                .append("timezone","America/Mexico_City")
                            )
                        )
                    )
                );
                script.add(new Document("$group", groupDependencias));
                script.add(new Document("$project", projectDependencias));
                script.add(new Document("$project", new Document()
                    .append("datos", dataDependencias)
                ));
                script.add(new Document()
                    .append("$unwind", new Document()
                        .append("path","$datos")
                        .append("preserveNullAndEmptyArrays",true)
                    )
                );
                script.add(new Document()
                    .append("$unwind", new Document()
                        .append("path","$datos.registros")
                        .append("preserveNullAndEmptyArrays",true)
                    )
                );
                script.add(new Document("$group", groupRegistros));
                script.add(new Document("$addFields", addFieldsRegistros));
                script.add(new Document()
                    .append("$lookup", new Document()
                        .append("from", "tcarga_de_evidencia")
                        .append("pipeline", Arrays.asList(
                            new Document("$addFields", new Document()
                                .append("fechaInicial", new Document()
                                    .append("$dateToString", new Document()
                                        .append("format", "%Y-%m-%d")
                                        .append("date", new Document()
                                            .append("$dateFromString", new Document()
                                                .append("dateString", "$periodo.fechaInicial")
                                            )
                                        )
                                    )
                                )
                                .append("fechaFinal", new Document()
                                    .append("$dateToString", new Document()
                                        .append("format", "%Y-%m-%d")
                                        .append("date", new Document()
                                            .append("$dateFromString", new Document()
                                                .append("dateString", "$periodo.fechaFinal")
                                            )
                                        )
                                    )
                                )
                            ),
                            new Document("$addFields", new Document()
                                .append("fechaInicial", new Document()
                                    .append("$dateFromString", new Document()
                                        .append("dateString","$fechaInicial")
                                        .append("timezone","America/Mexico_City")
                                    )
                                )
                                .append("fechaFinal", new Document()
                                    .append("$dateFromString", new Document()
                                        .append("dateString","$fechaFinal")
                                        .append("timezone","America/Mexico_City")
                                    )
                                )
                            ),
                            new Document("$match", whereLookup),
                            new Document("$group", groupLookup),
                            new Document("$addFields", addFieldsLookup),
                            new Document("$addFields", addFieldsLookup_2),
                            new Document("$addFields", addFieldsLookup_3)
                        ))
                        .append("as", "archivos")
                    )
                );
                script.add(new Document("$project", projectRegistros));
                //<editor-fold defaultstate="collapsed" desc="separar arreglos">
                while(intTotalIntervalos > 0)
                {
                    script.add(new Document()
                        .append("$unwind", new Document()
                            .append("path","$intervalo_".concat(String.valueOf(intTotalIntervalos)).concat(".archivos"))
                            .append("preserveNullAndEmptyArrays",true)
                        )
                    );
                    intTotalIntervalos--;
                }
                //</editor-fold>
                script.add(new Document("$addFields", addFieldsConteo));
                script.add(new Document("$addFields", addFieldsEstadoMes));
                script.add(new Document("$project", new Document()
                    .append("intervalos", projectIntervalos)
                ));
                script.add(new Document("$unwind", new Document()
                    .append("path", "$intervalos")
                    .append("preserveNullAndEmptyArrays", true)
                ));
                script.add(new Document("$addFields", new Document()
                    .append("intervalos", new Document()
                        .append("idArchivoAsociado", new Document()
                            .append("$cond", Arrays.asList(
                                new Document("$eq", Arrays.asList("$intervalos.poseeUnArchivo", true)),
                                new Document("$toString", "$intervalos.informacionArchivoAsociado.idArchivoAsociado"),
                                ""
                            ))
                        )
                        .append("statusArchivoAsociado", new Document()
                            .append("$cond", Arrays.asList(
                                new Document("$eq", Arrays.asList("$intervalos.poseeUnArchivo", true)),
                                "$intervalos.informacionArchivoAsociado.statusArchivo",
                                ""
                            ))
                        )
                        .append("motivoArchivoDeclinado", new Document()
                            .append("$cond", Arrays.asList(
                                new Document("$eq", Arrays.asList("$intervalos.poseeUnArchivo", true)),
                                "$intervalos.informacionArchivoAsociado.motivoArchivoDeclinado",
                                ""
                            ))
                        )
                    )
                ));
                script.add(new Document("$project", new Document()
                    .append("intervalos.archivos", 0)
                    .append("intervalos.informacionArchivoAsociado", 0)
                    .append("intervalos.totalRegistros", 0)
                    .append("intervalos.totalArchivos", 0)
                ));
                script.add(new Document("$sort", new Document()
                    .append("intervalos.numeroDeIntervalo", 1)
                ));
                //</editor-fold>
                Collation collation = Collation.builder()
                        .locale("es")
                        .caseLevel(true)
                        .build();
                aIPuesto = collection.aggregate(script).collation(collation);
                cursor = aIPuesto.iterator();
                if (cursor.hasNext())
                { // si la consulta contiene elementos (TOP)
                    while (cursor.hasNext())
                    { // lectura de los datos (TOP)

                        jsonTree = parser.parse(cursor.next().toJson());

                        if (jsonTree.isJsonObject())
                        { 

                            cargaDeEvidencia = new CargaDeEvidenciaVista();
                            JsonObject jsonObject = jsonTree.getAsJsonObject();
                            JsonObject elementosDelIntervalo = jsonObject.get("intervalos").getAsJsonObject();
                            JsonObject elementosDelId = jsonObject.get("_id").getAsJsonObject();

                            if (elementosDelId.isJsonObject())
                            { // si el id puede ser tratados como un objeto json (TOP)
                                cargaDeEvidencia.setDependencia(elementosDelId.get("dependencia").getAsString());  
                                cargaDeEvidencia.setClave(elementosDelId.get("clave").getAsString());  
                            } // si el id puede ser tratados como un objeto json (BOTTOM)
                            
                            if (elementosDelIntervalo.isJsonObject())
                            { // si el intevalo puede ser tratados como un objeto json (TOP)
                                cargaDeEvidencia.setFechaInicial(elementosDelIntervalo.get("fechaInicialString").getAsString());
                                cargaDeEvidencia.setFechaFinal(elementosDelIntervalo.get("fechaFinalString").getAsString());
                                cargaDeEvidencia.setPeriodo(String.valueOf(elementosDelIntervalo.get("anio").getAsInt()));  
                                cargaDeEvidencia.setEstado(elementosDelIntervalo.get("estadoIntervalo").getAsString());  
                                cargaDeEvidencia.setPoseeUnArchivoDeEvidencia(elementosDelIntervalo.get("poseeUnArchivo").getAsBoolean());  
                                cargaDeEvidencia.setIdArchivoAsociado(elementosDelIntervalo.get("idArchivoAsociado").getAsString());  
                                cargaDeEvidencia.setEstadoArchivoEvidencia(elementosDelIntervalo.get("statusArchivoAsociado").getAsString());  
                                cargaDeEvidencia.setMotivoArchivoEvidenciaDeclinado(elementosDelIntervalo.get("motivoArchivoDeclinado").getAsString());  
                            } // si el intevalo puede ser tratados como un objeto json (BOTTOM)
                            
                            arregloCargaDeEvidencia.add(cargaDeEvidencia);
                             
                        } 
                    } // lectura de los datos (BOTTOM)
                } // si la consulta contiene elementos (BOTTOM)
                
            } // si la conexion se ha establecido(BOTTOM)
            else
            { // no se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // no se ha establecido la conexion (BOTTOM)
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el estado general de seguimiento:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursor);
        }

        return arregloCargaDeEvidencia;
        
    } // metodo para obtener los estados de una dependencias con respecto a la carga de evidencia por un anio en especifico (BOTTOM)
    
    /**
     * Metodo de ayuda para cerrar las conexiones activas
     *
     *
     * @param conectarBaseDatos
     * @param cursor
     */
    public static void cerrarRecursos(MongoClient conectarBaseDatos, MongoCursor<Document> cursor)
    { // metodo de ayuda para cerrar las conexiones activas (TOP)

        if (cursor != null)
        { // cerrar iterador de documentos para consulta de base de datos (TOP)
            cursor.close();
            cursor = null;
        } // cerrar iterador de documentos para consulta de base de datos (BOTTOM)

        if (conectarBaseDatos != null)
        { // cerrar conexion a base de datos (TOP)
            conectarBaseDatos.close();
            conectarBaseDatos = null;
        } // cerrar conexion a base de datos (BOTTOM)

    } // metodo de ayuda para cerrar las conexiones activas (BOTTOM)

}
