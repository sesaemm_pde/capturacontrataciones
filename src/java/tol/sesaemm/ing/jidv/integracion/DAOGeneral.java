/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.integracion;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Collation;
import com.mongodb.client.model.UpdateOptions;
import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import org.bson.types.ObjectId;
import tol.sesaemm.ing.jidv.config.ConfVariables;
import tol.sesaemm.ing.jidv.javabeans.TCARGA_DE_EVIDENCIA;
import tol.sesaemm.ing.jidv.javabeans.TSEGUIMIENTO_CARGA_DE_EVIDENCIA;
import tol.sesaemm.ing.jidv.javabeans.USUARIO;
import tol.sesaemm.javabeans.ENTE_PUBLICO;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DAOGeneral 
{
    
    /**
     * Metodo para obtener las dependencias que poseen registros dentro del sistema 2 
     *
     * @param usuario
     * @param nivelDeAcceso
     * @return ArrayList String
     *
     * @throws Exception
     */
    public static ArrayList<TSEGUIMIENTO_CARGA_DE_EVIDENCIA> obtenerDependenciasParaSeguimiento(USUARIO usuario, int nivelDeAcceso) throws Exception
    { // metodo para obtener las dependencias que poseen registros dentro del sistema 2 (TOP) 
        
        MongoClient conectarBaseDatos = null;                                   // conexion a la base de datos
        MongoDatabase database;                                                 // obtener esquema de base de datos
        MongoCollection<Document> collection;                                   // obtener coleccion de datos
        ArrayList<TSEGUIMIENTO_CARGA_DE_EVIDENCIA> listadoDependencias;          // listado de dependencias
        List<Document> where = new ArrayList<>();
        Document or = new Document();
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tseguimiento_carga_de_evidencia");

                if(nivelDeAcceso != 1)
                { // no es usuario con privilegios de administrador (TOP)
                    for (ENTE_PUBLICO dependencia : usuario.getDependencias())
                    { // obtener dependencias asignadas al usuario (TOP)
                        where.add(new Document("clave", dependencia.getClave()));
                    } // obtener dependencias asignadas alusuario (BOTTOM)
                    or.append("$or", where);
                } // no es usuario con privilegios de administrador (BOTTOM)

                listadoDependencias = collection.find(or, TSEGUIMIENTO_CARGA_DE_EVIDENCIA.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } // si la conexion se ha establecido(BOTTOM)
            else
            { // no se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // no se ha establecido la conexion (BOTTOM)
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener la lista de dependencias para el seguimiento de SPIC: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listadoDependencias;
        
    }// metodo para obtener las dependencias que poseen registros dentro del sistema 2 (BOTTOM) 
    
    /**
     * Metodo para obtener una dependencia especifica para el seguimiento del sistema 2
     *
     * @param claveDependencia
     * 
     * @return ArrayList String
     *
     * @throws Exception
     */
    public static TSEGUIMIENTO_CARGA_DE_EVIDENCIA obtenerDependenciaParaSeguimiento(String claveDependencia) throws Exception
    { // metodo para obtener una dependencia especifica para el seguimiento del sistema 2 (TOP) 
        
        MongoClient conectarBaseDatos = null;                                   // conexion a la base de datos
        MongoDatabase database;                                                 // obtener esquema de base de datos
        MongoCollection<Document> collection;                                   // obtener coleccion de datos
        TSEGUIMIENTO_CARGA_DE_EVIDENCIA dependencia;                             // listado de dependencias
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tseguimiento_carga_de_evidencia");

                dependencia = collection.find(new Document("clave", claveDependencia), TSEGUIMIENTO_CARGA_DE_EVIDENCIA.class).first();

            } // si la conexion se ha establecido(BOTTOM)
            else
            { // no se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // no se ha establecido la conexion (BOTTOM)
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener la dependencia para el seguimiento de SPIC: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return dependencia;
        
    } // metodo para obtener una dependencia especifica para el seguimiento del sistema 2 (BOTTOM) 
    
    /**
     *
     * Metodo para registrar o modificar el archivo de evidencia sobre el seguimiento de informacion registrada
     *
     * @param cargaDeEvidencia
     * @param idMongoCargaDeEvidencia
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static boolean registrarModificarCargaDeEvidencia(TCARGA_DE_EVIDENCIA cargaDeEvidencia, String idMongoCargaDeEvidencia) throws Exception
    { // metodo para registrar o modificar el archivo de evidencia sobre el seguimiento de informacion registrada (TOP)

        MongoClient conectarBaseDatos = null;                                   // conexio;n a la base de datos
        MongoDatabase database;                                                 // obtener esquema de base de datos
        MongoCollection<TCARGA_DE_EVIDENCIA> collection;                         // obtener coleccion de datos   
        boolean seRegistra;                                                     // bandera que indica si el registro fue exitoso
        UpdateOptions opcionesExtra;                                            // opciones extra que permiten manipular el documento, upsert con valor verdadero para insertar un documento en caso de no encontrase en la base de datos
        Document filtro;                                                        // contiene el o los elementos que delimitan la actualizacion o insercion del documento
        Document documetoConComodinSet;                                         // representa el formato del json final con el comodin $set que permite la actualizacion o insercion del documento
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {

            seRegistra = false;
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tcarga_de_evidencia", TCARGA_DE_EVIDENCIA.class);

                filtro = new Document();
                filtro.append("_id", new ObjectId(idMongoCargaDeEvidencia));

                documetoConComodinSet = new Document();
                documetoConComodinSet.append("$set", cargaDeEvidencia);

                opcionesExtra = new UpdateOptions().upsert(true);

                collection.updateOne(filtro, documetoConComodinSet, opcionesExtra);
                seRegistra = true;

            } // si la conexion se ha establecido(BOTTOM)
            else
            { // no se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // no se ha establecido la conexion (BOTTOM)
        }
        catch (Exception ex)
        {
            throw new Exception("Error al registrar el archivo de evidencia sobre el seguimiento de informaci&oacute;n registrada: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return seRegistra;

    } // metodo para registrar o modificar el archivo de evidencia sobre el seguimiento de informacion registrada (BOTTOM)
    
    /**
     *
     * Metodo para registrar o modificar el archivo de evidencia sobre el seguimiento de informacion registrada
     *
     * @param cargaDeEvidencia
     * @param idMongoRegistro
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static boolean actualizarEstadoArchivoDeEvidencia(TCARGA_DE_EVIDENCIA cargaDeEvidencia, String idMongoRegistro) throws Exception
    { // metodo para actualizar el estado del archivo de evidencia (TOP)

        MongoClient conectarBaseDatos = null;                                   // conexio;n a la base de datos
        MongoDatabase database;                                                 // obtener esquema de base de datos
        MongoCollection<TCARGA_DE_EVIDENCIA> collection;                         // obtener coleccion de datos   
        boolean seRegistra;                                                     // bandera que indica si el registro fue exitoso
        Document filtro;                                                        // contiene el o los elementos que delimitan la actualizacion o insercion del documento
        Document documetoConComodinSet;                                         // representa el formato del json final con el comodin $set que permite la actualizacion o insercion del documento
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {

            seRegistra = false;
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tcarga_de_evidencia", TCARGA_DE_EVIDENCIA.class);

                filtro = new Document();
                filtro.append("_id", new ObjectId(idMongoRegistro));

                documetoConComodinSet = new Document();
                documetoConComodinSet.append("$set", cargaDeEvidencia);

                collection.updateOne(filtro, documetoConComodinSet);
                seRegistra = true;

            } // si la conexion se ha establecido(BOTTOM)
            else
            { // no se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // no se ha establecido la conexion (BOTTOM)
        }
        catch (Exception ex)
        {
            throw new Exception("Error al actualizar el estado del archivo de evidencia: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return seRegistra;

    } // metodo para actualizar el estado del archivo de evidencia (BOTTOM)
    
    /**
     *
     * Metodo para obtener la informacion relacionada a un archivo de evidencia referente al seguimiento de una dependencia
     *
     * @param idArchivoAsociado
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static TCARGA_DE_EVIDENCIA obtenerInformacionArchivoEvidencia(String idArchivoAsociado) throws Exception
    { // metodo para obtener la informacion relacionada a un archivo de evidencia referente al seguimiento de una dependencia (TOP)
        
        TCARGA_DE_EVIDENCIA cargaDeEvidencia;                                                   // privacidad de datos del servidor publico
        MongoClient conectarBaseDatos = null;                                                   // conexion a la base de datos
        MongoDatabase database;                                                                 // obtener esquema de base de datos
        MongoCollection<Document> collection;                                                   // obtener colecci&oacute;n de datos
        ArrayList<Document> query;                                                              // consulta
        Collation collation = Collation.builder().locale("es").caseLevel(true).build();
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            cargaDeEvidencia = new TCARGA_DE_EVIDENCIA();

            if (conectarBaseDatos != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tcarga_de_evidencia");

                query = new ArrayList<>();

                query.add(new Document("$match", new Document("_id", new ObjectId(idArchivoAsociado))));

                cargaDeEvidencia = collection.aggregate(query, TCARGA_DE_EVIDENCIA.class).collation(collation).first();

            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener la informaci&oacute;n del archivo de evidencia: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return cargaDeEvidencia;
        
    } // metodo para obtener la informacion relacionada a un archivo de evidencia referente al seguimiento de una dependencia (BOTTOM)
    
    /**
     * Metodo de ayuda para cerrar las conexiones activas
     *
     *
     * @param conectarBaseDatos
     * @param cursor
     */
    public static void cerrarRecursos(MongoClient conectarBaseDatos, MongoCursor<Document> cursor)
    { 

        if (cursor != null)
        { 
            cursor.close();
            cursor = null;
        } 

        if (conectarBaseDatos != null)
        { 
            conectarBaseDatos.close();
            conectarBaseDatos = null;
        } 

    } 
    
}