/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.interfaces;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import tol.sesaemm.ing.jidv.javabeans.NIVEL_ACCESO;
import tol.sesaemm.ing.jidv.javabeans.USUARIO;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.CATEGORIA_ADICIONAL_CONTRATACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.CATEGORIA_PRINCIPAL_CONTRATACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.CRITERIO_ADJUDICACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ContratacionesConPaginacion;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESCALAS_PARTES_INVOLUCRADAS;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESQUEMA_CLASIFICACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESQUEMA_PROCESO;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESQUEMA_UNIDAD;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESTADO_ADJUDICACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESTADO_CONTRATO;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESTADO_HITO;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESTADO_LICITACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.TIPO_ETIQUETA_ENTREGA;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.FiltrosDeBusquedaContratacion;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.IDIOMA_CONTRATACIONES;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.METODO_CONTRATACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.METODO_PRESENTACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.RELACION_PROCESO;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ROLES_PARTES_INVOLUCRADAS;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.TIPO_DOCUMENTO_CONTRATACIONES;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.TIPO_HITO;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.TIPO_INICIO_CONTRATACIONES;
import tol.sesaemm.javabeans.ENTE_PUBLICO;
import tol.sesaemm.javabeans.MONEDA;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesContratacion;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesAwards;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesBuyer;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesContracts;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesEstadoBloque;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesEstadoSeccion;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesParties;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesPlanning;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesPublisher;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesTender;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public interface LogicaDeNegocioS6
{

    public ContratacionesContratacion obtenerEstatusDePrivacidadCamposContratacion() throws Exception;
    public FiltrosDeBusquedaContratacion modelarRequestParaConsulta(HttpServletRequest request);
    public ContratacionesConPaginacion obtenerContratacionesPublicas(USUARIO usuario, NIVEL_ACCESO nivelAcceso, ArrayList<ENTE_PUBLICO> dependencias, FiltrosDeBusquedaContratacion filtroBusqueda) throws Exception;
    public ArrayList<String> obtenerDependenciasDeRegistros() throws Exception;
    public ContratacionesContratacion obtenerContratacion(String id) throws Exception;
    public ArrayList<ENTE_PUBLICO> obtenerDependenciasUsuario(USUARIO usuario) throws Exception;

    public ArrayList<TIPO_ETIQUETA_ENTREGA> obtenerEtiquetasDeEntrega() throws Exception;
    public ArrayList<TIPO_INICIO_CONTRATACIONES> obtenerTipoInicio() throws Exception;
    public ArrayList<ESTADO_LICITACION> obtenerEstadoLicitacion() throws Exception;
    public ArrayList<ROLES_PARTES_INVOLUCRADAS> obtenerRolesPartesInvolucradas() throws Exception;
    public ArrayList<ESCALAS_PARTES_INVOLUCRADAS> obtenerEscalasPartesInvolucradas() throws Exception;
    public ArrayList<TIPO_DOCUMENTO_CONTRATACIONES> obtenerTipoDocumento(int intNumeroDeSeccion, int intNumeroApartado) throws Exception;
    public ArrayList<TIPO_HITO> obtenerTipoHito() throws Exception;
    public ArrayList<ESTADO_HITO> obtenerEstadoHito() throws Exception;
    public ArrayList<METODO_CONTRATACION> obtenerMetodoContratacion() throws Exception;
    public ArrayList<CATEGORIA_PRINCIPAL_CONTRATACION> obtenerCategoriasPrincipales() throws Exception;
    public ArrayList<CATEGORIA_ADICIONAL_CONTRATACION> obtenerCategoriasAdicionales() throws Exception;
    public ArrayList<CRITERIO_ADJUDICACION> obtenerCriteriosDeAdjudicacion() throws Exception;
    public ArrayList<METODO_PRESENTACION> obtenerMetodosDePresentacion() throws Exception;
    public ArrayList<ESTADO_ADJUDICACION> obtenerEstadosAdjudicacion() throws Exception;
    public ArrayList<ESTADO_CONTRATO> obtenerEstadosContrato() throws Exception;
    public ArrayList<ESQUEMA_UNIDAD> obtenerEsquemaUnidad() throws Exception;
    public ArrayList<ESQUEMA_CLASIFICACION> obtenerEsquemaClasificacion() throws Exception;
    public ArrayList<RELACION_PROCESO> obtenerRelacionProceso() throws Exception;
    public ArrayList<ESQUEMA_PROCESO> obtenerEsquemaProceso() throws Exception;
    public ArrayList<MONEDA> obtenerTipoDeMoneda() throws Exception;
    public ArrayList<IDIOMA_CONTRATACIONES> obtenerListaDeTiposDeIdioma() throws Exception;
    public ArrayList<ENTE_PUBLICO> obtenerEntesPublicos(ArrayList<ENTE_PUBLICO> dependencias, USUARIO usuario) throws Exception;
    
    public boolean existeContratacion(String id) throws Exception;
    public ContratacionesContratacion modelarEstructuraPorDefectoContratacion();
    public ContratacionesContratacion modelarRequestSeccion(HttpServletRequest request, ContratacionesContratacion contratacion, int intSeccion, int intBloque, USUARIO usuario, boolean bolSeRegistra) throws Exception;
    public ContratacionesContratacion modelarEstadoDeSeccion(USUARIO usuario, ContratacionesContratacion contratacion, int intSeccion, int intBloque, boolean bolEsRegistro);
    public ContratacionesContratacion modelarSeccionDatosgenerales(HttpServletRequest request) throws Exception;
    public ContratacionesPublisher modelarSeccionPublicador(HttpServletRequest request) throws Exception;
    public ArrayList<String> modelarSeccionEtiquetasDeEntrega(HttpServletRequest request) throws Exception;
    public ArrayList<ContratacionesParties> modelarSeccionPartesInvolucradas(HttpServletRequest request) throws Exception;
    public ContratacionesBuyer modelarSeccionComprador(HttpServletRequest request) throws Exception;
    public ContratacionesPlanning modelarSeccionPlaneacion(HttpServletRequest request, ContratacionesPlanning objetoOriginalPlaneacion, int intNumeroDeBloque) throws Exception;
    public ContratacionesTender modelarSeccionLicitacion(HttpServletRequest request, ContratacionesTender objetoOriginalLicitacion, int intNumeroDeBloque) throws Exception;
    public ArrayList<ContratacionesAwards> modelarSeccionAdjudicaciones(HttpServletRequest request) throws Exception;
    public ArrayList<ContratacionesContracts> modelarSeccionContratos(HttpServletRequest request) throws Exception;
    
    public int obtenerSeccionConFoco(ArrayList<ContratacionesEstadoSeccion> estadoDeSecciones);
    public int obtenerBloqueConFoco(ArrayList<ContratacionesEstadoBloque> estadoDeBloques);
    public String registrarActualizarContratacion(ContratacionesContratacion contratacion, USUARIO usuario, NIVEL_ACCESO nivelAcceso, int intNumeroDeSeccion, int intNumeroDeBloque, boolean bolSeRegistra) throws Exception;
    public ContratacionesEstadoSeccion obtenerEstadoDeSeccion(String id, int intNumeroDeSeccion) throws Exception;
    public String eliminarContratacion(String id, USUARIO usuario, NIVEL_ACCESO nivelAcceso, ContratacionesContratacion contratacion) throws Exception;
    public String publicarContratacion(String id, USUARIO usuario, NIVEL_ACCESO nivelAcceso, ContratacionesContratacion contratacion) throws Exception;
    public String despublicarContratacion(String id, USUARIO usuario, NIVEL_ACCESO nivelAcceso, ContratacionesContratacion contratacion) throws Exception;
   
}