/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.logicaDeNegocio;

import java.util.ArrayList;
import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import org.bson.Document;
import org.bson.types.ObjectId;
import tol.sesaemm.funciones.ManejoDeFechas;
import tol.sesaemm.ing.jidv.integracion.DAOContratacionesPublicas;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocioS6;
import tol.sesaemm.ing.jidv.javabeans.NIVEL_ACCESO;
import tol.sesaemm.ing.jidv.javabeans.USUARIO;
import tol.sesaemm.ing.jidv.javabeans.FormateadorDeFechas;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.CATEGORIA_ADICIONAL_CONTRATACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.CATEGORIA_PRINCIPAL_CONTRATACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.CRITERIO_ADJUDICACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ContratacionesConPaginacion;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESCALAS_PARTES_INVOLUCRADAS;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESQUEMA_CLASIFICACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESQUEMA_PROCESO;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESQUEMA_UNIDAD;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESTADO_ADJUDICACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESTADO_CONTRATO;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESTADO_HITO;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ESTADO_LICITACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.TIPO_ETIQUETA_ENTREGA;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.FiltrosDeBusquedaContratacion;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.IDIOMA_CONTRATACIONES;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.METODO_CONTRATACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.METODO_PRESENTACION;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.RELACION_PROCESO;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.ROLES_PARTES_INVOLUCRADAS;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.TIPO_DOCUMENTO_CONTRATACIONES;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.TIPO_HITO;
import tol.sesaemm.ing.jidv.javabeans.s6contrataciones.TIPO_INICIO_CONTRATACIONES;
import tol.sesaemm.javabeans.ENTE_PUBLICO;
import tol.sesaemm.javabeans.METADATA;
import tol.sesaemm.javabeans.METADATOS;
import tol.sesaemm.javabeans.MONEDA;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesContratacion;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesAddress;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesAmendments;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesAmount;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesAwards;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesBudget;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesBuyer;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesChanges;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesClassification;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesContactPoint;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesContracts;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesDetails;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesDocuments;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesEstadoBloque;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesEstadoSeccion;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesIdentifier;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesImplementation;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesItems;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesMilestones;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesParties;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesPlanning;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesProcuringEntity;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesPublisher;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesRelatedProcesses;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesTender;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesTenderPeriod;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesTenderers;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesTransactions;
import tol.sesaemm.javabeans.s6contrataciones.ContratacionesUnit;

/**
 *
 * @author Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class LogicaDeNegocioS6V01 implements LogicaDeNegocioS6
{

    /**
     * Consulta los campos p&uacute;blicos de contrataciones
     *
     *
     * @return Contratacion
     * @throws java.lang.Exception
     */
    @Override
    public ContratacionesContratacion obtenerEstatusDePrivacidadCamposContratacion() throws Exception
    {
        return DAOContratacionesPublicas.obtenerEstatusDePrivacidadCamposContratacion("contratacionesPublicas");
    }

    /**
     * Metodo que modela la informacion de la peticion realizada a traves del
     * buscador principal del sistema 6
     *
     * @param request
     *
     * @return filtroDeBusqueda
     */
    @Override
    public FiltrosDeBusquedaContratacion modelarRequestParaConsulta(HttpServletRequest request)
    { 

        FiltrosDeBusquedaContratacion filtroDeBusqueda;                         
        int numeroPagina;                                                       
        int registrosMostrar;                                                   
        int cmbCiclo;                                                           
        String txtPublicador;                                                   
        String txtOrigen;                                                       
        String cmbOrdenacion;                                                   
        String txtDependencia;                                                  
        String txtID;
        boolean blnBusquedaGeneral;

        filtroDeBusqueda = new FiltrosDeBusquedaContratacion();

        txtID = (request.getParameter("txtID") == null) ? "" : request.getParameter("txtID");
        cmbCiclo = (request.getParameter("cmbCiclo") == null || request.getParameter("cmbCiclo").equals("") == true) ? 0 : Integer.parseInt(request.getParameter("cmbCiclo"));
        txtPublicador = (request.getParameter("txtPublicador") == null) ? "" : request.getParameter("txtPublicador");
        txtDependencia = (request.getParameter("txtDependencia") == null) ? "" : request.getParameter("txtDependencia");
        txtOrigen = (request.getParameter("txtOrigen") == null) ? "" : request.getParameter("txtOrigen");
        cmbOrdenacion = (request.getParameter("cmbOrdenacion") == null) ? "nombres" : request.getParameter("cmbOrdenacion");
        registrosMostrar = (request.getParameter("cmbPaginacion") == null) ? 10 : Integer.parseInt(request.getParameter("cmbPaginacion"));
        numeroPagina = (request.getParameter("txtNumeroPagina") == null ? 1 : Integer.parseInt(request.getParameter("txtNumeroPagina")));
        blnBusquedaGeneral = (request.getParameter("txtBusquedaGeneral") == null) ? false : Boolean.valueOf(request.getParameter("txtBusquedaGeneral"));

        filtroDeBusqueda.setIdReferencia(txtID);
        filtroDeBusqueda.setCiclo(cmbCiclo);
        filtroDeBusqueda.setPublicador(txtPublicador);
        filtroDeBusqueda.setDependencia(txtDependencia);
        filtroDeBusqueda.setRegistrosMostrar(registrosMostrar);
        filtroDeBusqueda.setNumeroPagina(numeroPagina);
        filtroDeBusqueda.setOrden(cmbOrdenacion);
        filtroDeBusqueda.setOrigen(txtOrigen);
        filtroDeBusqueda.setBusquedaGeneral(blnBusquedaGeneral);

        return filtroDeBusqueda;

    } 

    /**
     * Metodo que obtiene el listado de contrataciones publicas mostradas en la
     * vista principal del sistema 6
     *
     * @param usuario
     * @param nivelAcceso
     * @param dependencias
     * @param filtroBusqueda
     *
     * @return filtroDeBusqueda
     *
     * @throws java.lang.Exception
     */
    @Override
    public ContratacionesConPaginacion obtenerContratacionesPublicas(USUARIO usuario, NIVEL_ACCESO nivelAcceso, ArrayList<ENTE_PUBLICO> dependencias, FiltrosDeBusquedaContratacion filtroBusqueda) throws Exception
    { 
        return DAOContratacionesPublicas.obtenerContratacionesPublicas(usuario, nivelAcceso, dependencias, filtroBusqueda);
    } 

    /**
     *
     * @return ArrayList
     *
     * @throws java.lang.Exception
     */
    @Override
    public ArrayList<String> obtenerDependenciasDeRegistros() throws Exception
    { 
        return DAOContratacionesPublicas.obtenerDependenciasDeRegistros();
    } 

    /**
     * Metodo para obtener las dependencias que el usuario puede modificar o consultar
     *
     * @param usuario Usuario en sesión
     *
     * @return ArrayList Listado de dependencias
     *
     * @throws Exception
     */
    @Override
    public ArrayList<ENTE_PUBLICO> obtenerDependenciasUsuario(USUARIO usuario) throws Exception
    { 
        ArrayList<ENTE_PUBLICO> dependencias = usuario.getDependencias();
        return dependencias;
    } 
    
    /**
     *  Metodo para obtener una contratacion por medio de su id
     * 
     * @param id
     * @return Contratacion
     * @throws java.lang.Exception
     *
     */
    @Override
    public ContratacionesContratacion obtenerContratacion(String id) throws Exception
    { 
        return DAOContratacionesPublicas.obtenerContratacion(id);
    } 

    /**
     *  Metodo para verificar si existe una contratacion publica en base de datos
     * 
     * @param id
     * @return ArrayList
     *
     * @throws java.lang.Exception
     */
    @Override
    public boolean existeContratacion(String id) throws Exception
    { 
        return DAOContratacionesPublicas.existeContratacion(id);
    } 

    /**
     * Metodo para obtener las etiquetas de entrega permitidas para una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    @Override
    public ArrayList<TIPO_ETIQUETA_ENTREGA> obtenerEtiquetasDeEntrega() throws Exception
    { 
        return DAOContratacionesPublicas.obtenerEtiquetasDeEntrega();
    } 

    /**
     * Metodo para obtener el tipo de inicio para una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    @Override
    public ArrayList<TIPO_INICIO_CONTRATACIONES> obtenerTipoInicio() throws Exception
    { 
        return DAOContratacionesPublicas.obtenerTipoInicio();
    } 

    /**
     * Metodo para obtener el estado de una licitacion en una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    @Override
    public ArrayList<ESTADO_LICITACION> obtenerEstadoLicitacion() throws Exception
    { 
        return DAOContratacionesPublicas.obtenerEstadoLicitacion();
    } 

    /**
     * Metodo para obtener las escalas permitidas para las partes involucradas en una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    @Override
    public ArrayList<ESCALAS_PARTES_INVOLUCRADAS> obtenerEscalasPartesInvolucradas() throws Exception
    { 
        return DAOContratacionesPublicas.obtenerEscalasPartesInvolucradas();
    } 
    
    /**
     * Metodo para obtener los roles permitidos para las partes involucradas en una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    @Override
    public ArrayList<ROLES_PARTES_INVOLUCRADAS> obtenerRolesPartesInvolucradas() throws Exception
    { 
        return DAOContratacionesPublicas.obtenerRolesPartesInvolucradas();
    } 

    /**
     * Metodo para obtener el tipo de documento segun la seccion consultada
     *
     * @param intNumeroDeSeccion
     * @param intNumeroApartado
     *
     * @return ArrayList
     *
     * @throws Exception
     */
    @Override
    public ArrayList<TIPO_DOCUMENTO_CONTRATACIONES> obtenerTipoDocumento(int intNumeroDeSeccion, int intNumeroApartado) throws Exception
    { 
        return DAOContratacionesPublicas.obtenerTipoDocumento(intNumeroDeSeccion, intNumeroApartado);
    } 

    /**
     * Metodo para obtener los estados de un hito en una contratacion publica
     *
     * @return ArrayList
     *
     * @throws Exception
     */
    @Override
    public ArrayList<ESTADO_HITO> obtenerEstadoHito() throws Exception
    { 
        return DAOContratacionesPublicas.obtenerEstadoHito();
    } 

    /**
     * Metodo para obtener los tipos de hitos en una contratacion publica
     *
     * @return ArrayList
     *
     * @throws Exception
     */
    @Override
    public ArrayList<TIPO_HITO> obtenerTipoHito() throws Exception
    { 
        return DAOContratacionesPublicas.obtenerTipoHito();
    } 
    
    /**
     * Metodo para obtener el metodo de contratacion para una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    @Override
    public ArrayList<METODO_CONTRATACION> obtenerMetodoContratacion() throws Exception
    { 
        return DAOContratacionesPublicas.obtenerMetodoContratacion();
    } 

    /**
     * Metodo para obtener la categoria principal de una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    @Override
    public ArrayList<CATEGORIA_PRINCIPAL_CONTRATACION> obtenerCategoriasPrincipales() throws Exception
    { 
        return DAOContratacionesPublicas.obtenerCategoriasPrincipales();
    } 

    /**
     * Metodo para obtener las categorias adicionales de una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    @Override
    public ArrayList<CATEGORIA_ADICIONAL_CONTRATACION> obtenerCategoriasAdicionales() throws Exception
    { 
        return DAOContratacionesPublicas.obtenerCategoriasAdicionales();
    } 

    /**
     * Metodo para obtener los criterios de adjudicacions de una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    @Override
    public ArrayList<CRITERIO_ADJUDICACION> obtenerCriteriosDeAdjudicacion() throws Exception
    { 
        return DAOContratacionesPublicas.obtenerCriteriosDeAdjudicacion();
    } 

    /**
     * Metodo para obtener los metodos de presentacion de una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    @Override
    public ArrayList<METODO_PRESENTACION> obtenerMetodosDePresentacion() throws Exception
    { 
        return DAOContratacionesPublicas.obtenerMetodosDePresentacion();
    } 

    /**
     * Metodo para obtener los estados de una adjudicacion en una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    @Override
    public ArrayList<ESTADO_ADJUDICACION> obtenerEstadosAdjudicacion() throws Exception
    { 
        return DAOContratacionesPublicas.obtenerEstadosAdjudicacion();
    } 

    /**
     * Metodo para obtener los estados de un contrato en una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    @Override
    public ArrayList<ESTADO_CONTRATO> obtenerEstadosContrato() throws Exception
    { 
        return DAOContratacionesPublicas.obtenerEstadosContrato();
    } 

    /**
     * Metodo para obtener los esquemas de una unidad en una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    @Override
    public ArrayList<ESQUEMA_UNIDAD> obtenerEsquemaUnidad() throws Exception
    { 
        return DAOContratacionesPublicas.obtenerEsquemaUnidad();
    } 

    /**
     * Metodo para obtener los esquemas de una clasificacion en una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    @Override
    public ArrayList<ESQUEMA_CLASIFICACION> obtenerEsquemaClasificacion() throws Exception
    { 
        return DAOContratacionesPublicas.obtenerEsquemaClasificacion();
    } 

    /**
     * Metodo para obtener las relaciones de un proceso en una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    @Override
    public ArrayList<RELACION_PROCESO> obtenerRelacionProceso() throws Exception
    { 
        return DAOContratacionesPublicas.obtenerRelacionProceso();
    } 

    /**
     * Metodo para obtener el esquema de un proceso en una contratacion publica
     *
     * @return ArrayList String
     *
     * @throws Exception
     */
    @Override
    public ArrayList<ESQUEMA_PROCESO> obtenerEsquemaProceso() throws Exception
    { 
        return DAOContratacionesPublicas.obtenerEsquemaProceso();
    } 
    
    /**
     * Metodo para obtener el tipos de moneda permitido para una contratacion publica
     *
     * @return ArrayList
     *
     * @throws Exception
     */
    @Override
    public ArrayList<MONEDA> obtenerTipoDeMoneda() throws Exception
    { 
        return DAOContratacionesPublicas.obtenerListaDeTiposDeMoneda();
    } 
    
    /**
     * Metodo para obtener el tipo de idioma permitido para una contratacion publica 
     *
     * @return ArrayList IDIOMA_CONTRATACIONES
     * @throws java.lang.Exception
     */
    @Override
    public ArrayList<IDIOMA_CONTRATACIONES> obtenerListaDeTiposDeIdioma() throws Exception
    { 
        return DAOContratacionesPublicas.obtenerListaDeTiposDeIdioma();
    } 

    /**
     * Metodo para obtener la informacion de los ente publicos asignados al usuario
     *
     * @param dependencias
     * @param usuario
     * 
     * @return 
     * 
     * @throws java.lang.Exception
     */
    @Override
    public ArrayList<ENTE_PUBLICO> obtenerEntesPublicos(ArrayList<ENTE_PUBLICO> dependencias, USUARIO usuario) throws Exception
    { 
        return DAOContratacionesPublicas.obtenerEntesPublicos(dependencias, usuario);
    } 
    
    /**
     *  Metodo que obtiene la estructura por defecto para el registro de una contratacion publica
     * 
     * @return Contratacion
     *
     */
    @Override
    public ContratacionesContratacion modelarEstructuraPorDefectoContratacion()
    { 
        
        ContratacionesContratacion contratacion;                                              
        ContratacionesEstadoSeccion estadoSeccion;                              
        ContratacionesEstadoBloque estadoBloque;                                
        ArrayList<ContratacionesEstadoSeccion> estadoSeccionesContratacion;     
        ArrayList<ContratacionesEstadoBloque> estadoBloquesSeccion;             
        ContratacionesPublisher publicador;                                     
        ContratacionesBuyer comprador;                                          
        ContratacionesPlanning planeacion;                                      
        ContratacionesTender licitacion;                                        
        ContratacionesAmendments enmienda;
        
        contratacion = new ContratacionesContratacion();
        
        enmienda = new ContratacionesAmendments();
        enmienda.setChanges(new ArrayList<>());
        
        
        contratacion.setIdSpdn(new ObjectId());
        
        publicador = new ContratacionesPublisher();
        contratacion.setPublisher(publicador);
        
        
        contratacion.setTag(new ArrayList<>());
        
        
        contratacion.setParties(new ArrayList<>());
        
        
        comprador = new ContratacionesBuyer();
        comprador.setAdditionalIdentifiers(new ArrayList<>());
        contratacion.setBuyer(comprador);
        
        
        planeacion = new ContratacionesPlanning();
        ContratacionesBudget planeacionBudget = new ContratacionesBudget();
        planeacionBudget.setAmount(new ContratacionesAmount());
        planeacion.setBudget(planeacionBudget);
        planeacion.setDocuments(new ArrayList<>());
        planeacion.setMilestones(new ArrayList<>());
        contratacion.setPlanning(planeacion);
        
        
        licitacion = new ContratacionesTender();
        ContratacionesProcuringEntity procuringEntity = new ContratacionesProcuringEntity();
        procuringEntity.setAdditionalIdentifiers(new ArrayList<>());
        licitacion.setProcuringEntity(procuringEntity);
        licitacion.setItems(new ArrayList<>());
        licitacion.setValue(new ContratacionesAmount());
        licitacion.setMinValue(new ContratacionesAmount());
        licitacion.setAdditionalProcurementCategories(new ArrayList<>());
        licitacion.setSubmissionMethod(new ArrayList<>());
        licitacion.setTenderPeriod(new ContratacionesTenderPeriod());
        licitacion.setEnquiryPeriod(new ContratacionesTenderPeriod());
        licitacion.setAwardPeriod(new ContratacionesTenderPeriod());
        licitacion.setContractPeriod(new ContratacionesTenderPeriod());
        licitacion.setTenderers(new ArrayList<>());
        licitacion.setDocuments(new ArrayList<>());
        licitacion.setMilestones(new ArrayList<>());
        licitacion.setAmendments(new ArrayList<>());
        licitacion.setAmendment(enmienda);
        contratacion.setTender(licitacion);        
        
        
        contratacion.setAwards(new ArrayList<>());
        
        
        contratacion.setContracts(new ArrayList<>());
        
        
        
        
        estadoSeccionesContratacion = new ArrayList<>();
        
        
        estadoSeccion = new ContratacionesEstadoSeccion();
        estadoSeccion.setNumero_seccion(1);
        estadoSeccion.setNombre_seccion("datos_generales");
        estadoSeccion.setEstado_seccion("0");
        estadoSeccion.setFecha_registro("");
        estadoSeccion.setFecha_modificacion("");
        estadoSeccion.setUsuario_registro("");
        estadoSeccion.setUsuario_modificacion("");
        estadoSeccion.setBloques_seccion(new ArrayList<>());
        estadoSeccionesContratacion.add(estadoSeccion);
        
        
        estadoSeccion = new ContratacionesEstadoSeccion();
        estadoSeccion.setNumero_seccion(2);
        estadoSeccion.setNombre_seccion("publicador");
        estadoSeccion.setEstado_seccion("0");
        estadoSeccion.setFecha_registro("");
        estadoSeccion.setFecha_modificacion("");
        estadoSeccion.setUsuario_registro("");
        estadoSeccion.setUsuario_modificacion("");
        estadoSeccion.setBloques_seccion(new ArrayList<>());
        estadoSeccionesContratacion.add(estadoSeccion);
        
        
        estadoSeccion = new ContratacionesEstadoSeccion();
        estadoSeccion.setNumero_seccion(3);
        estadoSeccion.setNombre_seccion("etiquetas_de_entrega");
        estadoSeccion.setEstado_seccion("0");
        estadoSeccion.setFecha_registro("");
        estadoSeccion.setFecha_modificacion("");
        estadoSeccion.setUsuario_registro("");
        estadoSeccion.setUsuario_modificacion("");
        estadoSeccion.setBloques_seccion(new ArrayList<>());
        estadoSeccionesContratacion.add(estadoSeccion);
        
        
        estadoSeccion = new ContratacionesEstadoSeccion();
        estadoSeccion.setNumero_seccion(4);
        estadoSeccion.setNombre_seccion("partes_involucradas");
        estadoSeccion.setEstado_seccion("0");
        estadoSeccion.setFecha_registro("");
        estadoSeccion.setFecha_modificacion("");
        estadoSeccion.setUsuario_registro("");
        estadoSeccion.setUsuario_modificacion("");
        estadoSeccion.setBloques_seccion(new ArrayList<>());
        estadoSeccionesContratacion.add(estadoSeccion);
        
        
        estadoSeccion = new ContratacionesEstadoSeccion();
        estadoSeccion.setNumero_seccion(5);
        estadoSeccion.setNombre_seccion("comprador");
        estadoSeccion.setEstado_seccion("0");
        estadoSeccion.setFecha_registro("");
        estadoSeccion.setFecha_modificacion("");
        estadoSeccion.setUsuario_registro("");
        estadoSeccion.setUsuario_modificacion("");
        estadoSeccion.setBloques_seccion(new ArrayList<>());
        estadoSeccionesContratacion.add(estadoSeccion);
        
        
        estadoSeccion = new ContratacionesEstadoSeccion();
        estadoBloquesSeccion = new ArrayList<>();
        
        estadoSeccion.setNumero_seccion(6);
        estadoSeccion.setNombre_seccion("planeacion");
        estadoSeccion.setEstado_seccion("0");
        estadoSeccion.setFecha_registro("");
        estadoSeccion.setFecha_modificacion("");
        estadoSeccion.setUsuario_registro("");
        estadoSeccion.setUsuario_modificacion("");
        
        
        estadoBloque = new ContratacionesEstadoBloque();
        estadoBloque.setNumero_bloque(1);
        estadoBloque.setNombre_bloque("informacion_general");
        estadoBloque.setEstado_bloque("0");
        estadoBloquesSeccion.add(estadoBloque);
        
        estadoBloque = new ContratacionesEstadoBloque();
        estadoBloque.setNumero_bloque(2);
        estadoBloque.setNombre_bloque("presupuesto");
        estadoBloque.setEstado_bloque("0");
        estadoBloquesSeccion.add(estadoBloque);
        
        estadoBloque = new ContratacionesEstadoBloque();
        estadoBloque.setNumero_bloque(3);
        estadoBloque.setNombre_bloque("documentos");
        estadoBloque.setEstado_bloque("0");
        estadoBloquesSeccion.add(estadoBloque);
        
        estadoBloque = new ContratacionesEstadoBloque();
        estadoBloque.setNumero_bloque(4);
        estadoBloque.setNombre_bloque("hitos");
        estadoBloque.setEstado_bloque("0");
        estadoBloquesSeccion.add(estadoBloque);
        
        
        estadoSeccion.setBloques_seccion(estadoBloquesSeccion);
        estadoSeccionesContratacion.add(estadoSeccion);
        
        
        estadoSeccion = new ContratacionesEstadoSeccion();
        estadoBloquesSeccion = new ArrayList<>();
        
        estadoSeccion.setNumero_seccion(7);
        estadoSeccion.setNombre_seccion("licitacion");
        estadoSeccion.setEstado_seccion("0");
        estadoSeccion.setFecha_registro("");
        estadoSeccion.setFecha_modificacion("");
        estadoSeccion.setUsuario_registro("");
        estadoSeccion.setUsuario_modificacion("");
        
        
        estadoBloque = new ContratacionesEstadoBloque();
        estadoBloque.setNumero_bloque(1);
        estadoBloque.setNombre_bloque("informacion_general");
        estadoBloque.setEstado_bloque("0");
        estadoBloquesSeccion.add(estadoBloque);
        
        estadoBloque = new ContratacionesEstadoBloque();
        estadoBloque.setNumero_bloque(2);
        estadoBloque.setNombre_bloque("periodo_licitacion");
        estadoBloque.setEstado_bloque("0");
        estadoBloquesSeccion.add(estadoBloque);
        
        estadoBloque = new ContratacionesEstadoBloque();
        estadoBloque.setNumero_bloque(3);
        estadoBloque.setNombre_bloque("periodo_consulta");
        estadoBloque.setEstado_bloque("0");
        estadoBloquesSeccion.add(estadoBloque);
        
        estadoBloque = new ContratacionesEstadoBloque();
        estadoBloque.setNumero_bloque(4);
        estadoBloque.setNombre_bloque("periodo_evaluacion_adjudicacion");
        estadoBloque.setEstado_bloque("0");
        estadoBloquesSeccion.add(estadoBloque);
        
        estadoBloque = new ContratacionesEstadoBloque();
        estadoBloque.setNumero_bloque(5);
        estadoBloque.setNombre_bloque("periodo_contrato");
        estadoBloque.setEstado_bloque("0");
        estadoBloquesSeccion.add(estadoBloque);
        
        estadoBloque = new ContratacionesEstadoBloque();
        estadoBloque.setNumero_bloque(6);
        estadoBloque.setNombre_bloque("articulos_que_se_adquiriran");
        estadoBloque.setEstado_bloque("0");
        estadoBloquesSeccion.add(estadoBloque);
        
        estadoBloque = new ContratacionesEstadoBloque();
        estadoBloque.setNumero_bloque(7);
        estadoBloque.setNombre_bloque("licitantes");
        estadoBloque.setEstado_bloque("0");
        estadoBloquesSeccion.add(estadoBloque);
        
        estadoBloque = new ContratacionesEstadoBloque();
        estadoBloque.setNumero_bloque(8);
        estadoBloque.setNombre_bloque("documentos");
        estadoBloque.setEstado_bloque("0");
        estadoBloquesSeccion.add(estadoBloque);
        
        estadoBloque = new ContratacionesEstadoBloque();
        estadoBloque.setNumero_bloque(9);
        estadoBloque.setNombre_bloque("hitos");
        estadoBloque.setEstado_bloque("0");
        estadoBloquesSeccion.add(estadoBloque);
        
        estadoBloque = new ContratacionesEstadoBloque();
        estadoBloque.setNumero_bloque(10);
        estadoBloque.setNombre_bloque("enmiendas");
        estadoBloque.setEstado_bloque("0");
        estadoBloquesSeccion.add(estadoBloque);
        
        
        estadoSeccion.setBloques_seccion(estadoBloquesSeccion);
        estadoSeccionesContratacion.add(estadoSeccion);
        
        
        estadoSeccion = new ContratacionesEstadoSeccion();
        estadoSeccion.setNumero_seccion(8);
        estadoSeccion.setNombre_seccion("adjudicaciones");
        estadoSeccion.setEstado_seccion("0");
        estadoSeccion.setFecha_registro("");
        estadoSeccion.setFecha_modificacion("");
        estadoSeccion.setUsuario_registro("");
        estadoSeccion.setUsuario_modificacion("");
        estadoSeccion.setBloques_seccion(new ArrayList<>());
        estadoSeccionesContratacion.add(estadoSeccion);
        
        
        estadoSeccion = new ContratacionesEstadoSeccion();
        estadoSeccion.setNumero_seccion(9);
        estadoSeccion.setNombre_seccion("contratos");
        estadoSeccion.setEstado_seccion("0");
        estadoSeccion.setFecha_registro("");
        estadoSeccion.setFecha_modificacion("");
        estadoSeccion.setUsuario_registro("");
        estadoSeccion.setUsuario_modificacion("");
        estadoSeccion.setBloques_seccion(new ArrayList<>());
        estadoSeccionesContratacion.add(estadoSeccion);
        
        
        contratacion.setEstado_registro("2");
        contratacion.setEstado_secciones_contratacion(estadoSeccionesContratacion);
        
        
        
        return contratacion;
        
    } 

    /**
     *  Metodo para modelar la estructura de una contratacion para ser registrada o actualizada en base de datos
     * 
     * @param request
     * @param contratacion
     * @param intSeccion
     * @param intBloque
     * @param usuario
     * @param bolSeRegistra
     * 
     * @return Contratacion
     * @throws java.lang.Exception
     *
     */
    @Override
    public ContratacionesContratacion modelarRequestSeccion(HttpServletRequest request, ContratacionesContratacion contratacion, int intSeccion, int intBloque, USUARIO usuario, boolean bolSeRegistra) throws Exception
    { 

        ContratacionesContratacion datosGenerales;                                            
        ContratacionesPublisher publicador;                                     
        ArrayList<String> etiquetasDeEntrega;                                   
        ArrayList<ContratacionesParties> partesInvolucradas;                    
        ContratacionesBuyer comprador;                                          
        ContratacionesPlanning planeacion;                                      
        ContratacionesTender licitacion;                                        
        ArrayList<ContratacionesAwards> adjudicaciones;                         
        ArrayList<ContratacionesContracts> contratatos;                         
        
        switch (intSeccion)
        { 
            case 1:
                
                try
                {
                    datosGenerales = modelarSeccionDatosgenerales(request);
                    contratacion.setId(datosGenerales.getId());
                    contratacion.setCycle(datosGenerales.getCycle());
                    contratacion.setOcid(datosGenerales.getOcid());
                    contratacion.setDate(datosGenerales.getDate());
                    contratacion.setInitiationType(datosGenerales.getInitiationType());
                    contratacion.setLanguage(datosGenerales.getLanguage());
                }
                catch (Exception e)
                {
                    throw new Exception("Error al modelar contrataci&oacute;n -> Datos generales: " + e.getMessage());
                }
                
            break;
            case 2:
                
                try
                {
                    publicador = modelarSeccionPublicador(request);
                    contratacion.setPublisher(publicador);
                }
                catch (Exception e)
                {
                    throw new Exception("Error al modelar contrataci&oacute;n -> Publicador: " + e.getMessage());
                }
                
            break;
            case 3:
                
                try
                {
                    etiquetasDeEntrega = modelarSeccionEtiquetasDeEntrega(request);
                    contratacion.setTag(etiquetasDeEntrega);
                }
                catch (Exception e)
                {
                    throw new Exception("Error al modelar contrataci&oacute;n -> Etiquetas de entrega: " + e.getMessage());
                }
                
            break;
            case 4:
                
                try
                {
                    partesInvolucradas = modelarSeccionPartesInvolucradas(request);
                    contratacion.setParties(partesInvolucradas);
                }
                catch (Exception e)
                {
                    throw new Exception("Error al modelar contrataci&oacute;n -> Partes involucradas: " + e.getMessage());
                }
                
            break;
            case 5:
                
                try
                {
                    comprador = modelarSeccionComprador(request);
                    contratacion.setBuyer(comprador);
                }
                catch (Exception e)
                {
                    throw new Exception("Error al modelar contrataci&oacute;n -> Comprador: " + e.getMessage());
                }
                
            break;
            case 6:
                
                try
                {
                    planeacion = modelarSeccionPlaneacion(request, contratacion.getPlanning(), intBloque);
                    contratacion.setPlanning(planeacion);
                }
                catch (Exception e)
                {
                    throw new Exception("Error al modelar contrataci&oacute;n -> Planeaci&oacute;n: " + e.getMessage());
                }
                
            break;
            case 7:
                
                try
                {
                    licitacion = modelarSeccionLicitacion(request, contratacion.getTender(), intBloque);
                    contratacion.setTender(licitacion);
                }
                catch (Exception e)
                {
                    throw new Exception("Error al modelar contrataci&oacute;n -> Licitaci&oacute;n: " + e.getMessage());
                }
                
            break;
            case 8:
                
                try
                {
                    adjudicaciones = modelarSeccionAdjudicaciones(request);
                    contratacion.setAwards(adjudicaciones);
                }
                catch (Exception e)
                {
                    throw new Exception("Error al modelar contrataci&oacute;n -> Adjudicaciones: " + e.getMessage());
                }
                
            break;
            case 9:
                
                try
                {
                    contratatos = modelarSeccionContratos(request);
                    contratacion.setContracts(contratatos);
                }
                catch (Exception e)
                {
                    throw new Exception("Error al modelar contrataci&oacute;n -> Contrataciones: " + e.getMessage());
                }
                
            break;
            default:
                break;
        } 

        try
        {
            contratacion = modelarEstadoDeSeccion(usuario, contratacion, intSeccion, intBloque, bolSeRegistra);
        }
        catch (Exception e)
        {
            throw new Exception("Error al modelar contrataci&oacute;n -> Estado de contrataci&oacute;n: " + e.getMessage());
        }
        
        return contratacion;

    } 

    /**
     *  Metodo que modela lainformacion del estado de las secciones
     * 
     * @param contratacion
     * @param intSeccion
     * @param intBloque
     * @param usuario
     * @param bolEsRegistro
     * 
     * @return Contratacion
     *
     */
    @Override
    public ContratacionesContratacion modelarEstadoDeSeccion(USUARIO usuario, ContratacionesContratacion contratacion, int intSeccion, int intBloque, boolean bolEsRegistro)
    { 
        
        ArrayList<ContratacionesEstadoSeccion> estadoSeccionesContratacion;
        ArrayList<ContratacionesEstadoBloque> estadoBloquesSeccion;
        int intTotalBloquesSeccion;
        int intTotalBloquesCompletados;
        ManejoDeFechas manejoDeFechas;
        int intTotalSecciones;
        int intTotalSeccionesCompletadas;
        
        manejoDeFechas = new ManejoDeFechas();
        
        estadoSeccionesContratacion = contratacion.getEstado_secciones_contratacion();
        intTotalSecciones = estadoSeccionesContratacion.size();
        intTotalSeccionesCompletadas = 0;

        for(int j = 0; j < intTotalSecciones; j++)
        { 

            if(estadoSeccionesContratacion.get(j).getNumero_seccion() == intSeccion)
            { 

                if(bolEsRegistro)
                { 
                    estadoSeccionesContratacion.get(j).setFecha_registro(manejoDeFechas.getDateNowAsString());
                    estadoSeccionesContratacion.get(j).setUsuario_registro(usuario.getUsuario());
                } 
                else
                { 
                    estadoSeccionesContratacion.get(j).setFecha_modificacion(manejoDeFechas.getDateNowAsString());
                    estadoSeccionesContratacion.get(j).setUsuario_modificacion(usuario.getUsuario());
                } 

                if(intSeccion == 6 || intSeccion == 7)
                { 

                    estadoBloquesSeccion = estadoSeccionesContratacion.get(j).getBloques_seccion();
                    intTotalBloquesSeccion = estadoBloquesSeccion.size();
                    intTotalBloquesCompletados = 0;

                    for(int i = 0; i < intTotalBloquesSeccion; i++)
                    { 

                        if(estadoBloquesSeccion.get(i).getNumero_bloque() == intBloque)
                        { 
                            estadoBloquesSeccion.get(i).setEstado_bloque("1");
                        } 

                        if(estadoBloquesSeccion.get(i).getEstado_bloque().equals("1") == true)
                        { 
                            intTotalBloquesCompletados++;
                        } 

                    } 

                    if(intTotalBloquesSeccion == intTotalBloquesCompletados)
                    { 
                        estadoSeccionesContratacion.get(j).setEstado_seccion("1");
                    } 
                    else
                    { 
                        estadoSeccionesContratacion.get(j).setEstado_seccion("2");
                    } 

                } 
                else
                { 
                    estadoSeccionesContratacion.get(j).setEstado_seccion("1");
                } 

            } 

            if(estadoSeccionesContratacion.get(j).getEstado_seccion().equals("1") == true)
            { 
                intTotalSeccionesCompletadas++;
            } 

        } 

        if(intTotalSecciones == intTotalSeccionesCompletadas)
        {
            contratacion.setEstado_registro("1");
        }
        else
        {
            contratacion.setEstado_registro("2");
        }

        return contratacion;
        
    } 
    
    /**
     *  Metodo para modelar la informacion capturada en la seccion 'Datos generales' de una contratacion
     * 
     * @param request
     * 
     * @return Contratacion
     * @throws java.lang.Exception
     *
     */
    @Override
    public ContratacionesContratacion modelarSeccionDatosgenerales(HttpServletRequest request) throws Exception
    { 
        
        String id;                                                              
        Integer cycle;                                                          
        String ocid;                                                            
        String date;                                                            
        String initiationType;                                                  
        String language;                                                        
        ContratacionesContratacion datosGeneralesModelado;                                    
        FormateadorDeFechas formateadorDeFechas;                                

        formateadorDeFechas = new FormateadorDeFechas();

        
        datosGeneralesModelado = new ContratacionesContratacion();
        
        if(request.getParameter("txtID") != null && request.getParameter("txtID").trim().equals("") == false)
        { 
            id = request.getParameter("txtID");
        } 
        else
        { 
            id = null;
        } 
        
        
        if(request.getParameter("txtCYCLE") != null && request.getParameter("txtCYCLE").trim().equals("") == false)
        { 
            cycle = Integer.parseInt(request.getParameter("txtCYCLE"));
        } 
        else
        { 
            cycle = null;
        } 
        
        
        if(request.getParameter("txtID_OCID") != null && request.getParameter("txtID_OCID").trim().equals("") == false)
        { 
            ocid = request.getParameter("txtID_OCID");
        } 
        else
        { 
            ocid = null;
        } 
        
        
        if(request.getParameter("txtDATE") != null && request.getParameter("txtDATE").trim().equals("") == false)
        { 
            date = formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtDATE"));
        } 
        else
        { 
            date = null;
        } 
        
        
        if(request.getParameter("chckINITIATION_TYPE") != null && request.getParameter("chckINITIATION_TYPE").trim().equals("") == false)
        { 
            initiationType = request.getParameter("chckINITIATION_TYPE");
        } 
        else
        { 
            initiationType = null;
        } 
        
        
        if(request.getParameter("cmbLANGUAGE") != null && request.getParameter("cmbLANGUAGE").trim().equals("") == false)
        { 
            language = request.getParameter("cmbLANGUAGE");
        } 
        else
        { 
            language = null;
        } 
        
        
        
        datosGeneralesModelado.setId(id);
        datosGeneralesModelado.setCycle(cycle);
        datosGeneralesModelado.setOcid(ocid);
        datosGeneralesModelado.setDate(date);
        datosGeneralesModelado.setInitiationType(initiationType);
        datosGeneralesModelado.setLanguage(language);
        
            
        return datosGeneralesModelado;

    } 

    /**
     *  Metodo para modelar la informacion capturada en la seccion 'Publicador' de una contratacion
     * 
     * @param request
     * 
     * @return Contratacion
     * @throws java.lang.Exception
     *
     */
    @Override
    public ContratacionesPublisher modelarSeccionPublicador(HttpServletRequest request) throws Exception
    { 

        String uid;                                                             
        String name;                                                            
        String scheme;                                                          
        String uri;                                                             
        ContratacionesPublisher publisherModelado;                              

        
        publisherModelado = new ContratacionesPublisher();
        
        if(request.getParameter("txtUID") != null && request.getParameter("txtUID").trim().equals("") == false)
        { 
            uid = request.getParameter("txtUID");
        } 
        else
        { 
            uid = null;
        } 
        
        
        if(request.getParameter("txtNAME") != null && request.getParameter("txtNAME").trim().equals("") == false)
        { 
            name = request.getParameter("txtNAME");
        } 
        else
        { 
            name = null;
        } 
        
        
        if(request.getParameter("txtSCHEME") != null && request.getParameter("txtSCHEME").trim().equals("") == false)
        { 
            scheme = request.getParameter("txtSCHEME");
        } 
        else
        { 
            scheme = null;
        } 
        
        
        if(request.getParameter("txtURI") != null && request.getParameter("txtURI").trim().equals("") == false)
        { 
            uri = request.getParameter("txtURI");
        } 
        else
        { 
            uri = null;
        } 
        
        
        
        publisherModelado.setUid(uid);
        publisherModelado.setName(name);
        publisherModelado.setScheme(scheme);
        publisherModelado.setUri(uri);
        

        return publisherModelado;

    } 
    
    /**
     *  Metodo para modelar la informacion capturada en la seccion 'Etiquetas de entrega' de una contratacion
     * 
     * @param request
     * 
     * @return ArrayList
     * @throws java.lang.Exception
     *
     */
    @Override
    public ArrayList<String> modelarSeccionEtiquetasDeEntrega(HttpServletRequest request) throws Exception
    { 

        ArrayList<String> etiquetas;                              

        
        if (request.getParameterValues("chckTAG") == null || request.getParameterValues("chckTAG").length == 0)
        { 
            etiquetas = new ArrayList();
        } 
        else
        { 
            etiquetas = new ArrayList(Arrays.asList(request.getParameterValues("chckTAG")));
        } 
        

        return etiquetas;

    } 
    
    /**
     *  Metodo para modelar la informacion capturada en la seccion 'Partes involucradas' de una contratacion
     * 
     * @param request
     * 
     * @return ArrayList
     * @throws java.lang.Exception
     *
     */
    @Override
    public ArrayList<ContratacionesParties> modelarSeccionPartesInvolucradas(HttpServletRequest request) throws Exception
    { 
        
        ArrayList<ContratacionesParties> listaDePartesInvolucradas;
        ContratacionesParties parteInvolucrada;
        ArrayList<ContratacionesIdentifier> listaDeIdentificadoresAdicionales;
        ContratacionesIdentifier identificadorAdicional; 
        ContratacionesIdentifier identificador;
        ContratacionesAddress address;
        ContratacionesContactPoint contactPoint;
        ContratacionesDetails details;
        ArrayList<String> arregloDeStrings;
        
        String [] listaDeIdentificadoresARegistrarNivelUno;
        String [] listaDeIdentificadoresARegistrarNivelDos; 
        String [] listaDeValoresInputCheck;
        
        listaDePartesInvolucradas = new ArrayList<>();
        
        
        if(request.getParameter("lista-identificadores-por-registrar-parties") != null)
        { 
            
            listaDeIdentificadoresARegistrarNivelUno = request.getParameter("lista-identificadores-por-registrar-parties").split(",");

            for (String identificadorParteInvolucrada : listaDeIdentificadoresARegistrarNivelUno)
            { 
                
                parteInvolucrada = new ContratacionesParties();
                        
                identificadorParteInvolucrada = identificadorParteInvolucrada.trim();
                
                if(identificadorParteInvolucrada.equals("") == false)
                { 
                
                    if(request.getParameter("txtPARTIES_ID_" + identificadorParteInvolucrada) != null)
                    { 
                        if(request.getParameter("txtPARTIES_ID_" + identificadorParteInvolucrada).equals("") == false)
                        { 
                            
                            parteInvolucrada.setId(request.getParameter("txtPARTIES_ID_" + identificadorParteInvolucrada));
                            
                            
                            if(request.getParameter("txtPARTIES_NAME_" + identificadorParteInvolucrada) != null && request.getParameter("txtPARTIES_NAME_" + identificadorParteInvolucrada).trim().equals("") == false)
                            { 
                                parteInvolucrada.setName(request.getParameter("txtPARTIES_NAME_" + identificadorParteInvolucrada));
                            } 
                            else
                            { 
                                parteInvolucrada.setName(null);
                            } 
                            
                            
                            identificador = new ContratacionesIdentifier();
                            
                            if(request.getParameter("txtPARTIES_IDENTIFIER_ID_" + identificadorParteInvolucrada) != null && request.getParameter("txtPARTIES_IDENTIFIER_ID_" + identificadorParteInvolucrada).trim().equals("") == false)
                            { 
                                identificador.setId(request.getParameter("txtPARTIES_IDENTIFIER_ID_" + identificadorParteInvolucrada));
                            } 
                            else
                            { 
                                identificador.setId(null);
                            } 
                            
                            
                            if(request.getParameter("txtPARTIES_IDENTIFIER_SCHEME_" + identificadorParteInvolucrada) != null && request.getParameter("txtPARTIES_IDENTIFIER_SCHEME_" + identificadorParteInvolucrada).trim().equals("") == false)
                            { 
                                identificador.setScheme(request.getParameter("txtPARTIES_IDENTIFIER_SCHEME_" + identificadorParteInvolucrada));
                            } 
                            else
                            { 
                                identificador.setScheme(null);
                            } 
                            
                            
                            if(request.getParameter("txtPARTIES_IDENTIFIER_LEGAL_NAME_" + identificadorParteInvolucrada) != null && request.getParameter("txtPARTIES_IDENTIFIER_LEGAL_NAME_" + identificadorParteInvolucrada).trim().equals("") == false)
                            { 
                                identificador.setLegalName(request.getParameter("txtPARTIES_IDENTIFIER_LEGAL_NAME_" + identificadorParteInvolucrada));
                            } 
                            else
                            { 
                                identificador.setLegalName(null);
                            } 
                            
                            
                            if(request.getParameter("txtPARTIES_IDENTIFIER_URI_" + identificadorParteInvolucrada) != null && request.getParameter("txtPARTIES_IDENTIFIER_URI_" + identificadorParteInvolucrada).trim().equals("") == false)
                            { 
                                identificador.setUri(request.getParameter("txtPARTIES_IDENTIFIER_URI_" + identificadorParteInvolucrada));
                            } 
                            else
                            { 
                                identificador.setUri(null);
                            } 
                            
                            parteInvolucrada.setIdentifier(identificador);
                            
                            
                            listaDeIdentificadoresAdicionales = new ArrayList<>();
                            if(request.getParameter("lista-identificadores-por-registrar-identifier_" + identificadorParteInvolucrada) != null)
                            {
                                listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-identifier_" + identificadorParteInvolucrada).split(",");

                                for (String identificadorIdentifierAdicional : listaDeIdentificadoresARegistrarNivelDos)
                                { 

                                    identificadorIdentifierAdicional = identificadorIdentifierAdicional.trim();

                                    if(identificadorIdentifierAdicional.equals("") == false)
                                    { 

                                        if(request.getParameter("txtIDENTIFIER_ID_" + identificadorParteInvolucrada + "_" + identificadorIdentifierAdicional) != null)
                                        { 
                                            if(request.getParameter("txtIDENTIFIER_ID_" + identificadorParteInvolucrada + "_" + identificadorIdentifierAdicional).equals("") == false)
                                            { 
                                                identificadorAdicional = new ContratacionesIdentifier();
                                                
                                                identificadorAdicional.setId(request.getParameter("txtIDENTIFIER_ID_" + identificadorParteInvolucrada + "_" + identificadorIdentifierAdicional));
                                                
                                                
                                                if(request.getParameter("txtIDENTIFIER_SCHEME_" + identificadorParteInvolucrada + "_" + identificadorIdentifierAdicional) != null && request.getParameter("txtIDENTIFIER_SCHEME_" + identificadorParteInvolucrada + "_" + identificadorIdentifierAdicional).trim().equals("") == false)
                                                { 
                                                    identificadorAdicional.setScheme(request.getParameter("txtIDENTIFIER_SCHEME_" + identificadorParteInvolucrada + "_" + identificadorIdentifierAdicional));
                                                } 
                                                else
                                                { 
                                                    identificadorAdicional.setScheme(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIDENTIFIER_LEGAL_NAME_" + identificadorParteInvolucrada + "_" + identificadorIdentifierAdicional) != null && request.getParameter("txtIDENTIFIER_LEGAL_NAME_" + identificadorParteInvolucrada + "_" + identificadorIdentifierAdicional).trim().equals("") == false)
                                                { 
                                                    identificadorAdicional.setLegalName(request.getParameter("txtIDENTIFIER_LEGAL_NAME_" + identificadorParteInvolucrada + "_" + identificadorIdentifierAdicional));
                                                } 
                                                else
                                                { 
                                                    identificadorAdicional.setLegalName(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIDENTIFIER_URI_" + identificadorParteInvolucrada + "_" + identificadorIdentifierAdicional) != null && request.getParameter("txtIDENTIFIER_URI_" + identificadorParteInvolucrada + "_" + identificadorIdentifierAdicional).trim().equals("") == false)
                                                { 
                                                    identificadorAdicional.setUri(request.getParameter("txtIDENTIFIER_URI_" + identificadorParteInvolucrada + "_" + identificadorIdentifierAdicional));
                                                } 
                                                else
                                                { 
                                                    identificadorAdicional.setUri(null);
                                                } 
                                                
                                                listaDeIdentificadoresAdicionales.add(identificadorAdicional);
                                            } 
                                        } 

                                    } 

                                } 
                                parteInvolucrada.setAdditionalIdentifiers(listaDeIdentificadoresAdicionales);
                            }
                            
                            
                            address = new ContratacionesAddress();
                            
                            if(request.getParameter("txtPARTIES_ADDRESS_STREET_ADDRESS_" + identificadorParteInvolucrada) != null && request.getParameter("txtPARTIES_ADDRESS_STREET_ADDRESS_" + identificadorParteInvolucrada).trim().equals("") == false)
                            { 
                                address.setStreetAddress(request.getParameter("txtPARTIES_ADDRESS_STREET_ADDRESS_" + identificadorParteInvolucrada));
                            } 
                            else
                            { 
                                address.setStreetAddress(null);
                            } 
                            
                            
                            if(request.getParameter("txtPARTIES_ADDRESS_LOCALITY_" + identificadorParteInvolucrada) != null && request.getParameter("txtPARTIES_ADDRESS_LOCALITY_" + identificadorParteInvolucrada).trim().equals("") == false)
                            { 
                                address.setLocality(request.getParameter("txtPARTIES_ADDRESS_LOCALITY_" + identificadorParteInvolucrada));
                            } 
                            else
                            { 
                                address.setLocality(null);
                            } 
                            
                            
                            if(request.getParameter("txtPARTIES_ADDRESS_REGION_" + identificadorParteInvolucrada) != null && request.getParameter("txtPARTIES_ADDRESS_REGION_" + identificadorParteInvolucrada).trim().equals("") == false)
                            { 
                                address.setRegion(request.getParameter("txtPARTIES_ADDRESS_REGION_" + identificadorParteInvolucrada));
                            } 
                            else
                            { 
                                address.setRegion(null);
                            } 
                            
                            
                            if(request.getParameter("txtPARTIES_ADDRESS_POSTAL_CODE_" + identificadorParteInvolucrada) != null && request.getParameter("txtPARTIES_ADDRESS_POSTAL_CODE_" + identificadorParteInvolucrada).trim().equals("") == false)
                            { 
                                address.setPostalCode(request.getParameter("txtPARTIES_ADDRESS_POSTAL_CODE_" + identificadorParteInvolucrada));
                            } 
                            else
                            { 
                                address.setPostalCode(null);
                            } 
                            
                            
                            if(request.getParameter("txtPARTIES_ADDRESS_COUNTRY_NAME_" + identificadorParteInvolucrada) != null && request.getParameter("txtPARTIES_ADDRESS_COUNTRY_NAME_" + identificadorParteInvolucrada).trim().equals("") == false)
                            { 
                                address.setCountryName(request.getParameter("txtPARTIES_ADDRESS_COUNTRY_NAME_" + identificadorParteInvolucrada));
                            } 
                            else
                            { 
                                address.setCountryName(null);
                            } 
                            
                            parteInvolucrada.setAddress(address);
                            
                            
                            contactPoint = new ContratacionesContactPoint();
                            
                            if(request.getParameter("txtPARTIES_CONTACT_NAME_" + identificadorParteInvolucrada) != null && request.getParameter("txtPARTIES_CONTACT_NAME_" + identificadorParteInvolucrada).trim().equals("") == false)
                            { 
                                contactPoint.setName(request.getParameter("txtPARTIES_CONTACT_NAME_" + identificadorParteInvolucrada));
                            } 
                            else
                            { 
                                contactPoint.setName(null);
                            } 
                            
                            
                            if(request.getParameter("txtPARTIES_CONTACT_EMAIL_" + identificadorParteInvolucrada) != null && request.getParameter("txtPARTIES_CONTACT_EMAIL_" + identificadorParteInvolucrada).trim().equals("") == false)
                            { 
                                contactPoint.setEmail(request.getParameter("txtPARTIES_CONTACT_EMAIL_" + identificadorParteInvolucrada));
                            } 
                            else
                            { 
                                contactPoint.setEmail(null);
                            } 
                            
                            
                            if(request.getParameter("txtPARTIES_CONTACT_TELEPHONE_" + identificadorParteInvolucrada) != null && request.getParameter("txtPARTIES_CONTACT_TELEPHONE_" + identificadorParteInvolucrada).trim().equals("") == false)
                            { 
                                contactPoint.setTelephone(request.getParameter("txtPARTIES_CONTACT_TELEPHONE_" + identificadorParteInvolucrada));
                            } 
                            else
                            { 
                                contactPoint.setTelephone(null);
                            } 
                            
                            
                            if(request.getParameter("txtPARTIES_CONTACT_FAXNUMBER_" + identificadorParteInvolucrada) != null && request.getParameter("txtPARTIES_CONTACT_FAXNUMBER_" + identificadorParteInvolucrada).trim().equals("") == false)
                            { 
                                contactPoint.setFaxNumber(request.getParameter("txtPARTIES_CONTACT_FAXNUMBER_" + identificadorParteInvolucrada));
                            } 
                            else
                            { 
                                contactPoint.setFaxNumber(null);
                            } 
                            
                            
                            if(request.getParameter("txtPARTIES_CONTACT_URL_" + identificadorParteInvolucrada) != null && request.getParameter("txtPARTIES_CONTACT_URL_" + identificadorParteInvolucrada).trim().equals("") == false)
                            { 
                                contactPoint.setUrl(request.getParameter("txtPARTIES_CONTACT_URL_" + identificadorParteInvolucrada));
                            } 
                            else
                            { 
                                contactPoint.setUrl(null);
                            } 
                            
                            parteInvolucrada.setContactPoint(contactPoint);
                            
                            
                            arregloDeStrings = new ArrayList<>();
                            if (request.getParameterValues("chckPARTIES_ROLES_" + identificadorParteInvolucrada) != null)
                            { 
                                listaDeValoresInputCheck = request.getParameter("chckPARTIES_ROLES_" + identificadorParteInvolucrada).split(",");

                                for (String relacion : listaDeValoresInputCheck)
                                { 
                                    if(relacion.trim().equals("") == false)
                                    { 
                                        arregloDeStrings.add(relacion.trim());
                                    } 
                                } 

                            } 
                            parteInvolucrada.setRoles(arregloDeStrings);
                            
                            
                            details = new ContratacionesDetails();
                            
                            if(request.getParameter("radPARTIES_DETAILS_SCALE_" + identificadorParteInvolucrada) != null && request.getParameter("radPARTIES_DETAILS_SCALE_" + identificadorParteInvolucrada).trim().equals("") == false)
                            { 
                                details.setScale(request.getParameter("radPARTIES_DETAILS_SCALE_" + identificadorParteInvolucrada));
                            } 
                            else
                            { 
                                details.setScale(null);
                            } 
                            
                            parteInvolucrada.setDetails(details);
                            

                            listaDePartesInvolucradas.add(parteInvolucrada);
                            
                        } 
                    } 
                    
                } 
                
            } 
            
        } 
        
        
        return listaDePartesInvolucradas;
        
    } 

    /**
     *  Metodo para modelar la informacion capturada en la seccion 'Comprador' de una contratacion
     * 
     * @param request
     * 
     * @return ContratacionesBuyer
     * @throws java.lang.Exception
     *
     */
    @Override
    public ContratacionesBuyer modelarSeccionComprador(HttpServletRequest request) throws Exception
    { 
        
        ContratacionesBuyer comprador;
        comprador = new ContratacionesBuyer();
        ContratacionesIdentifier identificador;
        ArrayList<ContratacionesIdentifier> listaDeIdentificadoresAdicionales;
        ContratacionesIdentifier identificadorAdicional;
        ContratacionesAddress address;
        ContratacionesContactPoint contactPoint;
        String [] listaDeIdentificadoresARegistrarNivelUno;
        
        
        if(request.getParameter("txtBUYER_ID") != null && request.getParameter("txtBUYER_ID").trim().equals("") == false)
        { 
            comprador.setId(request.getParameter("txtBUYER_ID"));
        } 
        else
        { 
            comprador.setId(null);
        } 
        
        
        if(request.getParameter("txtBUYER_NAME") != null && request.getParameter("txtBUYER_NAME").trim().equals("") == false)
        { 
            comprador.setName(request.getParameter("txtBUYER_NAME"));
        } 
        else
        { 
            comprador.setName(null);
        } 
        
        
        identificador = new ContratacionesIdentifier();
        
        if(request.getParameter("txtBUYER_IDENTIFIER_ID") != null && request.getParameter("txtBUYER_IDENTIFIER_ID").trim().equals("") == false)
        { 
            identificador.setId(request.getParameter("txtBUYER_IDENTIFIER_ID"));
        } 
        else
        { 
            identificador.setId(null);
        } 
        
        
        if(request.getParameter("txtBUYER_IDENTIFIER_SCHEME") != null && request.getParameter("txtBUYER_IDENTIFIER_SCHEME").trim().equals("") == false)
        { 
            identificador.setScheme(request.getParameter("txtBUYER_IDENTIFIER_SCHEME"));
        } 
        else
        { 
            identificador.setScheme(null);
        } 
        
        
        if(request.getParameter("txtBUYER_IDENTIFIER_LEGAL_NAME") != null && request.getParameter("txtBUYER_IDENTIFIER_LEGAL_NAME").trim().equals("") == false)
        { 
            identificador.setLegalName(request.getParameter("txtBUYER_IDENTIFIER_LEGAL_NAME"));
        } 
        else
        { 
            identificador.setLegalName(null);
        } 
        
        
        if(request.getParameter("txtBUYER_IDENTIFIER_URI") != null && request.getParameter("txtBUYER_IDENTIFIER_URI").trim().equals("") == false)
        { 
            identificador.setUri(request.getParameter("txtBUYER_IDENTIFIER_URI"));
        } 
        else
        { 
            identificador.setUri(null);
        } 
        
        comprador.setIdentifier(identificador);
        
        
        address = new ContratacionesAddress();
        
        if(request.getParameter("txtBUYER_ADDRESS_STREET_ADDRESS") != null && request.getParameter("txtBUYER_ADDRESS_STREET_ADDRESS").trim().equals("") == false)
        { 
            address.setStreetAddress(request.getParameter("txtBUYER_ADDRESS_STREET_ADDRESS"));
        } 
        else
        { 
            address.setStreetAddress(null);
        } 
        
        
        if(request.getParameter("txtBUYER_ADDRESS_LOCALITY") != null && request.getParameter("txtBUYER_ADDRESS_LOCALITY").trim().equals("") == false)
        { 
            address.setLocality(request.getParameter("txtBUYER_ADDRESS_LOCALITY"));
        } 
        else
        { 
            address.setLocality(null);
        } 
        
        
        if(request.getParameter("txtBUYER_ADDRESS_REGION") != null && request.getParameter("txtBUYER_ADDRESS_REGION").trim().equals("") == false)
        { 
            address.setRegion(request.getParameter("txtBUYER_ADDRESS_REGION"));
        } 
        else
        { 
            address.setRegion(null);
        } 
        
        
        if(request.getParameter("txtBUYER_ADDRESS_POSTAL_CODE") != null && request.getParameter("txtBUYER_ADDRESS_POSTAL_CODE").trim().equals("") == false)
        { 
            address.setPostalCode(request.getParameter("txtBUYER_ADDRESS_POSTAL_CODE"));
        } 
        else
        { 
            address.setPostalCode(null);
        } 
        
        
        if(request.getParameter("txtBUYER_ADDRESS_COUNTRY_NAME") != null && request.getParameter("txtBUYER_ADDRESS_COUNTRY_NAME").trim().equals("") == false)
        { 
            address.setCountryName(request.getParameter("txtBUYER_ADDRESS_COUNTRY_NAME"));
        } 
        else
        { 
            address.setCountryName(null);
        } 
        
        comprador.setAddress(address);
        
        
        contactPoint = new ContratacionesContactPoint();
        
        if(request.getParameter("txtBUYER_CONTACT_NAME") != null && request.getParameter("txtBUYER_CONTACT_NAME").trim().equals("") == false)
        { 
            contactPoint.setName(request.getParameter("txtBUYER_CONTACT_NAME"));
        } 
        else
        { 
            contactPoint.setName(null);
        } 
        
        
        if(request.getParameter("txtBUYER_CONTACT_EMAIL") != null && request.getParameter("txtBUYER_CONTACT_EMAIL").trim().equals("") == false)
        { 
            contactPoint.setEmail(request.getParameter("txtBUYER_CONTACT_EMAIL"));
        } 
        else
        { 
            contactPoint.setEmail(null);
        } 
        
        
        if(request.getParameter("txtBUYER_CONTACT_TELEPHONE") != null && request.getParameter("txtBUYER_CONTACT_TELEPHONE").trim().equals("") == false)
        { 
            contactPoint.setTelephone(request.getParameter("txtBUYER_CONTACT_TELEPHONE"));
        } 
        else
        { 
            contactPoint.setTelephone(null);
        } 
        
        
        if(request.getParameter("txtBUYER_CONTACT_FAXNUMBER") != null && request.getParameter("txtBUYER_CONTACT_FAXNUMBER").trim().equals("") == false)
        { 
            contactPoint.setFaxNumber(request.getParameter("txtBUYER_CONTACT_FAXNUMBER"));
        } 
        else
        { 
            contactPoint.setFaxNumber(null);
        } 
        
        
        if(request.getParameter("txtBUYER_CONTACT_URL") != null && request.getParameter("txtBUYER_CONTACT_URL").trim().equals("") == false)
        { 
            contactPoint.setUrl(request.getParameter("txtBUYER_CONTACT_URL"));
        } 
        else
        { 
            contactPoint.setUrl(null);
        } 
        
        comprador.setContactPoint(contactPoint);
        
        
        listaDeIdentificadoresAdicionales = new ArrayList<>();
        if(request.getParameter("lista-identificadores-por-registrar-identifier") != null)
        { 

            listaDeIdentificadoresARegistrarNivelUno = request.getParameter("lista-identificadores-por-registrar-identifier").split(",");

            for (String identificadorIdentifierAdicional : listaDeIdentificadoresARegistrarNivelUno)
            { 

                identificadorIdentifierAdicional = identificadorIdentifierAdicional.trim();

                if(identificadorIdentifierAdicional.equals("") == false)
                { 

                    if(request.getParameter("txtIDENTIFIER_ID_" + identificadorIdentifierAdicional) != null)
                    { 
                        if(request.getParameter("txtIDENTIFIER_ID_" + identificadorIdentifierAdicional).equals("") == false)
                        { 
                            identificadorAdicional = new ContratacionesIdentifier();
                            
                            identificadorAdicional.setId(request.getParameter("txtIDENTIFIER_ID_" + identificadorIdentifierAdicional));
                            
                            
                            if(request.getParameter("txtIDENTIFIER_SCHEME_" + identificadorIdentifierAdicional) != null && request.getParameter("txtIDENTIFIER_SCHEME_" + identificadorIdentifierAdicional).trim().equals("") == false)
                            { 
                                identificadorAdicional.setScheme(request.getParameter("txtIDENTIFIER_SCHEME_" + identificadorIdentifierAdicional));
                            } 
                            else
                            { 
                                identificadorAdicional.setScheme(null);
                            } 
                            
                            
                            if(request.getParameter("txtIDENTIFIER_LEGAL_NAME_" + identificadorIdentifierAdicional) != null && request.getParameter("txtIDENTIFIER_LEGAL_NAME_" + identificadorIdentifierAdicional).trim().equals("") == false)
                            { 
                                identificadorAdicional.setLegalName(request.getParameter("txtIDENTIFIER_LEGAL_NAME_" + identificadorIdentifierAdicional));
                            } 
                            else
                            { 
                                identificadorAdicional.setLegalName(null);
                            } 
                            
                            
                            if(request.getParameter("txtIDENTIFIER_URI_" + identificadorIdentifierAdicional) != null && request.getParameter("txtIDENTIFIER_URI_" + identificadorIdentifierAdicional).trim().equals("") == false)
                            { 
                                identificadorAdicional.setUri(request.getParameter("txtIDENTIFIER_URI_" + identificadorIdentifierAdicional));
                            } 
                            else
                            { 
                                identificadorAdicional.setUri(null);
                            } 
                            
                            listaDeIdentificadoresAdicionales.add(identificadorAdicional);
                        } 
                    } 

                } 

            } 

        } 

        comprador.setAdditionalIdentifiers(listaDeIdentificadoresAdicionales);
        
                
        return comprador;
        
    } 

    /**
     *  Metodo para modelar la informacion capturada en la seccion 'Planeacion' de una contratacion
     * 
     * @param request
     * @param objetoOriginalPlaneacion
     * @param intNumeroDeBloque
     * 
     * @return ContratacionesPlanning
     * @throws java.lang.Exception
     *
     */
    @Override
    public ContratacionesPlanning modelarSeccionPlaneacion(HttpServletRequest request, ContratacionesPlanning objetoOriginalPlaneacion, int intNumeroDeBloque) throws Exception
    { 
        
        ContratacionesPlanning planeacion;
        planeacion = objetoOriginalPlaneacion;
        FormateadorDeFechas formateadorDeFechas;
        ArrayList<ContratacionesMilestones> listaDeHitos;
        ContratacionesMilestones hito;
        ArrayList<ContratacionesDocuments> listaDeDocumentos;
        ContratacionesDocuments documento;
        String [] listaDeIdentificadoresARegistrarNivelUno;
        String [] listaDeIdentificadoresARegistrarNivelDos;
        
        formateadorDeFechas = new FormateadorDeFechas();
        
        switch(intNumeroDeBloque)
        { 
            case 1:
                
                
                if(request.getParameter("txtRATIONALE") != null && request.getParameter("txtRATIONALE").trim().equals("") == false)
                { 
                    planeacion.setRationale(request.getParameter("txtRATIONALE"));
                } 
                else
                { 
                    planeacion.setRationale(null);
                } 
                
                
                break;
            case 2:
                
                ContratacionesBudget presupuesto;
                ContratacionesAmount monto;
                
                presupuesto = new ContratacionesBudget();
                
                
                if(request.getParameter("txtID") != null && request.getParameter("txtID").trim().equals("") == false)
                { 
                    presupuesto.setId(request.getParameter("txtID"));
                } 
                else
                { 
                    presupuesto.setId(null);
                } 
                
                
                if(request.getParameter("txtPROJECT_ID") != null && request.getParameter("txtPROJECT_ID").trim().equals("") == false)
                { 
                    presupuesto.setProjectID(request.getParameter("txtPROJECT_ID"));
                } 
                else
                { 
                    presupuesto.setProjectID(null);
                } 
                
                
                if(request.getParameter("txtPROJECT") != null && request.getParameter("txtPROJECT").trim().equals("") == false)
                { 
                    presupuesto.setProject(request.getParameter("txtPROJECT"));
                } 
                else
                { 
                    presupuesto.setProject(null);
                } 
                
                
                if(request.getParameter("txtDESCRIPTION") != null && request.getParameter("txtDESCRIPTION").trim().equals("") == false)
                { 
                    presupuesto.setDescription(request.getParameter("txtDESCRIPTION"));
                } 
                else
                { 
                    presupuesto.setDescription(null);
                } 
                
                
                if(request.getParameter("txtURI") != null && request.getParameter("txtURI").trim().equals("") == false)
                { 
                    presupuesto.setUri(request.getParameter("txtURI"));
                } 
                else
                { 
                    presupuesto.setUri(null);
                } 
                
                
                if(request.getParameter("txtSOURCE") != null && request.getParameter("txtSOURCE").trim().equals("") == false)
                { 
                    presupuesto.setSource(request.getParameter("txtSOURCE"));
                } 
                else
                { 
                    presupuesto.setSource(null);
                } 
                
                
                monto = new ContratacionesAmount();
                
                if(request.getParameter("txtAMOUNT") != null && request.getParameter("txtAMOUNT").trim().equals("") == false)
                { 
                    monto.setAmount(Float.parseFloat(request.getParameter("txtAMOUNT")));
                } 
                else
                { 
                    monto.setAmount(null);
                } 
                
                
                if(request.getParameter("txtCURRENCY") != null && request.getParameter("txtCURRENCY").trim().equals("") == false)
                { 
                    monto.setCurrency(request.getParameter("txtCURRENCY"));
                } 
                else
                { 
                    monto.setCurrency(null);
                } 
                
                presupuesto.setAmount(monto);
                
                
                planeacion.setBudget(presupuesto);
                
                break;
            case 3: 
                
                listaDeDocumentos = new ArrayList<>();
                if(request.getParameter("lista-identificadores-por-registrar-document") != null)
                { 

                    listaDeIdentificadoresARegistrarNivelUno = request.getParameter("lista-identificadores-por-registrar-document").split(",");

                    for (String identificadorDocumento : listaDeIdentificadoresARegistrarNivelUno)
                    { 

                        identificadorDocumento = identificadorDocumento.trim();

                        if(identificadorDocumento.equals("") == false)
                        { 

                            if(request.getParameter("txtDOCUMENT_ID_"  + identificadorDocumento) != null)
                            { 
                                if(request.getParameter("txtDOCUMENT_ID_"  + identificadorDocumento).equals("") == false)
                                { 
                                    documento = new ContratacionesDocuments();
                                    
                                    documento.setId(request.getParameter("txtDOCUMENT_ID_"  + identificadorDocumento));
                                    
                                    
                                    if(request.getParameter("cmbDOCUMENT_TYPE_"  + identificadorDocumento) != null && request.getParameter("cmbDOCUMENT_TYPE_"  + identificadorDocumento).trim().equals("") == false)
                                    { 
                                        documento.setDocumentType(request.getParameter("cmbDOCUMENT_TYPE_"  + identificadorDocumento));
                                    } 
                                    else
                                    { 
                                        documento.setDocumentType(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtDOCUMENT_TITLE_"  + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_TITLE_"  + identificadorDocumento).trim().equals("") == false)
                                    { 
                                        documento.setTitle(request.getParameter("txtDOCUMENT_TITLE_"  + identificadorDocumento));
                                    } 
                                    else
                                    { 
                                        documento.setTitle(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtDOCUMENT_DESCRIPTION_"  + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_DESCRIPTION_"  + identificadorDocumento).trim().equals("") == false)
                                    { 
                                        documento.setDescription(request.getParameter("txtDOCUMENT_DESCRIPTION_"  + identificadorDocumento));
                                    } 
                                    else
                                    { 
                                        documento.setDescription(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtDOCUMENT_URL_"  + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_URL_"  + identificadorDocumento).trim().equals("") == false)
                                    { 
                                        documento.setUrl(request.getParameter("txtDOCUMENT_URL_"  + identificadorDocumento));
                                    } 
                                    else
                                    { 
                                        documento.setUrl(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtDOCUMENT_DATE_PUBLISHED_"  + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_DATE_PUBLISHED_"  + identificadorDocumento).trim().equals("") == false)
                                    { 
                                        documento.setDatePublished(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtDOCUMENT_DATE_PUBLISHED_"  + identificadorDocumento)));
                                    } 
                                    else
                                    { 
                                        documento.setDatePublished(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtDOCUMENT_DATE_MODIFIED_"  + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_DATE_MODIFIED_"  + identificadorDocumento).trim().equals("") == false)
                                    { 
                                        documento.setDateModified(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtDOCUMENT_DATE_MODIFIED_"  + identificadorDocumento)));
                                    } 
                                    else
                                    { 
                                        documento.setDateModified(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtDOCUMENT_FORMAT_"  + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_FORMAT_"  + identificadorDocumento).trim().equals("") == false)
                                    { 
                                        documento.setFormat(request.getParameter("txtDOCUMENT_FORMAT_"  + identificadorDocumento));
                                    } 
                                    else
                                    { 
                                        documento.setFormat(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("cmbDOCUMENT_LANGUAGE_"  + identificadorDocumento) != null && request.getParameter("cmbDOCUMENT_LANGUAGE_"  + identificadorDocumento).trim().equals("") == false)
                                    { 
                                        documento.setLanguage(request.getParameter("cmbDOCUMENT_LANGUAGE_"  + identificadorDocumento));
                                    } 
                                    else
                                    { 
                                        documento.setLanguage(null);
                                    } 
                                    
                                    listaDeDocumentos.add(documento);
                                } 
                            } 

                        } 

                    } 

                } 
                
                planeacion.setDocuments(listaDeDocumentos);
                
                break;
            case 4: 
                
                listaDeHitos = new ArrayList<>();
                if(request.getParameter("lista-identificadores-por-registrar-milestone") != null)
                { 

                    listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-milestone").split(",");

                    for (String identificadorHito : listaDeIdentificadoresARegistrarNivelDos)
                    { 

                        identificadorHito = identificadorHito.trim();

                        if(identificadorHito.equals("") == false)
                        { 

                            if(request.getParameter("txtMILESTONE_ID_" + identificadorHito) != null)
                            { 
                                if(request.getParameter("txtMILESTONE_ID_" + identificadorHito).equals("") == false)
                                { 
                                    hito = new ContratacionesMilestones();
                                    
                                    hito.setId(request.getParameter("txtMILESTONE_ID_" + identificadorHito));
                                    
                                    
                                    if(request.getParameter("cmbMILESTONE_TYPE_" + identificadorHito) != null && request.getParameter("cmbMILESTONE_TYPE_" + identificadorHito).trim().equals("") == false)
                                    { 
                                        hito.setType(request.getParameter("cmbMILESTONE_TYPE_" + identificadorHito));
                                    } 
                                    else
                                    { 
                                        hito.setType(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtMILESTONE_TITLE_" + identificadorHito) != null && request.getParameter("txtMILESTONE_TITLE_" + identificadorHito).trim().equals("") == false)
                                    { 
                                        hito.setTitle(request.getParameter("txtMILESTONE_TITLE_" + identificadorHito));
                                    } 
                                    else
                                    { 
                                        hito.setTitle(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtMILESTONE_DESCRIPTION_" + identificadorHito) != null && request.getParameter("txtMILESTONE_DESCRIPTION_" + identificadorHito).trim().equals("") == false)
                                    { 
                                        hito.setDescription(request.getParameter("txtMILESTONE_DESCRIPTION_" + identificadorHito));
                                    } 
                                    else
                                    { 
                                        hito.setDescription(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtMILESTONE_CODE_" + identificadorHito) != null && request.getParameter("txtMILESTONE_CODE_" + identificadorHito).trim().equals("") == false)
                                    { 
                                        hito.setCode(request.getParameter("txtMILESTONE_CODE_" + identificadorHito));
                                    } 
                                    else
                                    { 
                                        hito.setCode(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtMILESTONE_DUE_DATE_" + identificadorHito) != null && request.getParameter("txtMILESTONE_DUE_DATE_" + identificadorHito).trim().equals("") == false)
                                    { 
                                        hito.setDueDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtMILESTONE_DUE_DATE_" + identificadorHito)));
                                    } 
                                    else
                                    { 
                                        hito.setDueDate(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtMILESTONE_DATE_MET_" + identificadorHito) != null && request.getParameter("txtMILESTONE_DATE_MET_" + identificadorHito).trim().equals("") == false)
                                    { 
                                        hito.setDateMet(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtMILESTONE_DATE_MET_" + identificadorHito)));
                                    } 
                                    else
                                    { 
                                        hito.setDateMet(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtMILESTONE_DATE_MODIFIED_" + identificadorHito) != null && request.getParameter("txtMILESTONE_DATE_MODIFIED_" + identificadorHito).trim().equals("") == false)
                                    { 
                                        hito.setDateModified(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtMILESTONE_DATE_MODIFIED_" + identificadorHito)));
                                    } 
                                    else
                                    { 
                                        hito.setDateModified(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("cmbMILESTONE_STATUS_" + identificadorHito) != null && request.getParameter("cmbMILESTONE_STATUS_" + identificadorHito).trim().equals("") == false)
                                    { 
                                        hito.setStatus(request.getParameter("cmbMILESTONE_STATUS_" + identificadorHito));
                                    } 
                                    else
                                    { 
                                        hito.setStatus(null);
                                    } 
                                    
                                    
                                    listaDeDocumentos = new ArrayList<>();
                                    if(request.getParameter("lista-identificadores-por-registrar-milestone-document_" + identificadorHito) != null)
                                    { 

                                        listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-milestone-document_" + identificadorHito).split(",");

                                        for (String identificadorDocumento : listaDeIdentificadoresARegistrarNivelDos)
                                        { 

                                            identificadorDocumento = identificadorDocumento.trim();

                                            if(identificadorDocumento.equals("") == false)
                                            { 

                                                if(request.getParameter("txtMILESTONE_DOCUMENT_ID_" + identificadorHito + "_" + identificadorDocumento) != null)
                                                { 
                                                    if(request.getParameter("txtMILESTONE_DOCUMENT_ID_" + identificadorHito + "_" + identificadorDocumento).equals("") == false)
                                                    { 
                                                        documento = new ContratacionesDocuments();
                                                        
                                                        documento.setId(request.getParameter("txtMILESTONE_DOCUMENT_ID_" + identificadorHito + "_" + identificadorDocumento));
                                                        
                                                        
                                                        if(request.getParameter("cmbMILESTONE_DOCUMENT_TYPE_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("cmbMILESTONE_DOCUMENT_TYPE_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                        { 
                                                            documento.setDocumentType(request.getParameter("cmbMILESTONE_DOCUMENT_TYPE_" + identificadorHito + "_" + identificadorDocumento));
                                                        } 
                                                        else
                                                        { 
                                                            documento.setDocumentType(null);
                                                        } 
                                                        
                                                        
                                                        if(request.getParameter("txtMILESTONE_DOCUMENT_TITLE_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtMILESTONE_DOCUMENT_TITLE_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                        { 
                                                            documento.setTitle(request.getParameter("txtMILESTONE_DOCUMENT_TITLE_" + identificadorHito + "_" + identificadorDocumento));
                                                        } 
                                                        else
                                                        { 
                                                            documento.setTitle(null);
                                                        } 
                                                        
                                                        
                                                        if(request.getParameter("txtMILESTONE_DOCUMENT_DESCRIPTION_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtMILESTONE_DOCUMENT_DESCRIPTION_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                        { 
                                                            documento.setDescription(request.getParameter("txtMILESTONE_DOCUMENT_DESCRIPTION_" + identificadorHito + "_" + identificadorDocumento));
                                                        } 
                                                        else
                                                        { 
                                                            documento.setDescription(null);
                                                        } 
                                                        
                                                        
                                                        if(request.getParameter("txtMILESTONE_DOCUMENT_URL_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtMILESTONE_DOCUMENT_URL_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                        { 
                                                            documento.setUrl(request.getParameter("txtMILESTONE_DOCUMENT_URL_" + identificadorHito + "_" + identificadorDocumento));
                                                        } 
                                                        else
                                                        { 
                                                            documento.setUrl(null);
                                                        } 
                                                        
                                                        
                                                        if(request.getParameter("txtMILESTONE_DOCUMENT_DATE_PUBLISHED_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtMILESTONE_DOCUMENT_DATE_PUBLISHED_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                        { 
                                                            documento.setDatePublished(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtMILESTONE_DOCUMENT_DATE_PUBLISHED_" + identificadorHito + "_" + identificadorDocumento)));
                                                        } 
                                                        else
                                                        { 
                                                            documento.setDatePublished(null);
                                                        } 
                                                        
                                                        
                                                        if(request.getParameter("txtMILESTONE_DOCUMENT_DATE_MODIFIED_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtMILESTONE_DOCUMENT_DATE_MODIFIED_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                        { 
                                                            documento.setDateModified(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtMILESTONE_DOCUMENT_DATE_MODIFIED_" + identificadorHito + "_" + identificadorDocumento)));
                                                        } 
                                                        else
                                                        { 
                                                            documento.setDateModified(null);
                                                        } 
                                                        
                                                        
                                                        if(request.getParameter("txtMILESTONE_DOCUMENT_FORMAT_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtMILESTONE_DOCUMENT_FORMAT_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                        { 
                                                            documento.setFormat(request.getParameter("txtMILESTONE_DOCUMENT_FORMAT_" + identificadorHito + "_" + identificadorDocumento));
                                                        } 
                                                        else
                                                        { 
                                                            documento.setFormat(null);
                                                        } 
                                                        
                                                        
                                                        if(request.getParameter("cmbMILESTONE_DOCUMENT_LANGUAGE_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("cmbMILESTONE_DOCUMENT_LANGUAGE_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                        { 
                                                            documento.setLanguage(request.getParameter("cmbMILESTONE_DOCUMENT_LANGUAGE_" + identificadorHito + "_" + identificadorDocumento));
                                                        } 
                                                        else
                                                        { 
                                                            documento.setLanguage(null);
                                                        } 
                                                        
                                                        listaDeDocumentos.add(documento);
                                                    } 
                                                } 

                                            } 

                                        } 

                                    } 

                                    hito.setDocuments(listaDeDocumentos);
                                    
                                    listaDeHitos.add(hito);   

                                } 
                            } 
                        } 

                    } 

                } 
                
                planeacion.setMilestones(listaDeHitos);
                
                break;
        } 
        
        return planeacion;
        
    } 
    
    /**
     *  Metodo para modelar la informacion capturada en la seccion 'Licitacion' de una contratacion
     * 
     * @param request
     * @param objetoOriginalLicitacion
     * @param intNumeroDeBloque
     * 
     * @return ContratacionesTender
     * @throws java.lang.Exception
     *
     */
    @Override
    public ContratacionesTender modelarSeccionLicitacion(HttpServletRequest request, ContratacionesTender objetoOriginalLicitacion, int intNumeroDeBloque) throws Exception
    { 
        
        ContratacionesTender licitacion;
        licitacion = objetoOriginalLicitacion;
        FormateadorDeFechas formateadorDeFechas;
        ArrayList<ContratacionesMilestones> listaDeHitos;
        ContratacionesMilestones hito;
        ArrayList<ContratacionesDocuments> listaDeDocumentos;
        ContratacionesDocuments documento;
        ArrayList<ContratacionesAmendments> listaDeEnmiendas;
        ContratacionesAmendments enmienda;
        ArrayList<ContratacionesChanges> listaDeCambios;
        ContratacionesChanges cambio;
        ArrayList<ContratacionesItems> listaDeItems;
        ContratacionesItems item;
        ContratacionesClassification clasificacion;
        ArrayList<ContratacionesClassification> listaDeClasificacionesAdicionales;
        ContratacionesClassification clasificacionAdicional;
        ContratacionesUnit unit;
        ContratacionesAmount amount;
        ContratacionesIdentifier identificador;
        ArrayList<ContratacionesIdentifier> listaDeIdentificadoresAdicionales;
        ContratacionesIdentifier identificadorAdicional;
        ContratacionesAddress address;
        ContratacionesContactPoint contactPoint;
        String [] listaDeIdentificadoresARegistrarNivelUno;
        String [] listaDeIdentificadoresARegistrarNivelDos;
        
        formateadorDeFechas = new FormateadorDeFechas();
                
        switch(intNumeroDeBloque)
        { 
            case 1:
                
                
                if(request.getParameter("txtID") != null && request.getParameter("txtID").trim().equals("") == false)
                { 
                    licitacion.setId(request.getParameter("txtID"));
                } 
                else
                { 
                    licitacion.setId(null);
                } 
                
                
                if(request.getParameter("txtTITLE") != null && request.getParameter("txtTITLE").trim().equals("") == false)
                { 
                    licitacion.setTitle(request.getParameter("txtTITLE"));
                } 
                else
                { 
                    licitacion.setTitle(null);
                } 
                
                
                if(request.getParameter("txtDESCRIPTION") != null && request.getParameter("txtDESCRIPTION").trim().equals("") == false)
                { 
                    licitacion.setDescription(request.getParameter("txtDESCRIPTION"));
                } 
                else
                { 
                    licitacion.setDescription(null);
                } 
                
                
                if(request.getParameter("cmbSTATUS") != null && request.getParameter("cmbSTATUS").trim().equals("") == false)
                { 
                    licitacion.setStatus(request.getParameter("cmbSTATUS"));
                } 
                else
                { 
                    licitacion.setStatus(null);
                } 
                
                
                ContratacionesProcuringEntity entidadContratante = new ContratacionesProcuringEntity();
                
                if(request.getParameter("txtPROCURING_ENTITY_ID") != null && request.getParameter("txtPROCURING_ENTITY_ID").trim().equals("") == false)
                { 
                    entidadContratante.setId(request.getParameter("txtPROCURING_ENTITY_ID"));
                } 
                else
                { 
                    entidadContratante.setId(null);
                } 
                
                
                if(request.getParameter("txtPROCURING_ENTITY_NAME") != null && request.getParameter("txtPROCURING_ENTITY_NAME").trim().equals("") == false)
                { 
                    entidadContratante.setName(request.getParameter("txtPROCURING_ENTITY_NAME"));
                } 
                else
                { 
                    entidadContratante.setName(null);
                } 
                
                
                identificador = new ContratacionesIdentifier();
                
                if(request.getParameter("txtPROCURING_ENTITY_IDENTIFIER_ID") != null && request.getParameter("txtPROCURING_ENTITY_IDENTIFIER_ID").trim().equals("") == false)
                { 
                    identificador.setId(request.getParameter("txtPROCURING_ENTITY_IDENTIFIER_ID"));
                } 
                else
                { 
                    identificador.setId(null);
                } 
                
                
                if(request.getParameter("txtPROCURING_ENTITY_IDENTIFIER_SCHEME") != null && request.getParameter("txtPROCURING_ENTITY_IDENTIFIER_SCHEME").trim().equals("") == false)
                { 
                    identificador.setScheme(request.getParameter("txtPROCURING_ENTITY_IDENTIFIER_SCHEME"));
                } 
                else
                { 
                    identificador.setScheme(null);
                } 
                
                
                if(request.getParameter("txtPROCURING_ENTITY_IDENTIFIER_LEGAL_NAME") != null && request.getParameter("txtPROCURING_ENTITY_IDENTIFIER_LEGAL_NAME").trim().equals("") == false)
                { 
                    identificador.setLegalName(request.getParameter("txtPROCURING_ENTITY_IDENTIFIER_LEGAL_NAME"));
                } 
                else
                { 
                    identificador.setLegalName(null);
                } 
                
                
                if(request.getParameter("txtPROCURING_ENTITY_IDENTIFIER_URI") != null && request.getParameter("txtPROCURING_ENTITY_IDENTIFIER_URI").trim().equals("") == false)
                { 
                    identificador.setUri(request.getParameter("txtPROCURING_ENTITY_IDENTIFIER_URI"));
                } 
                else
                { 
                    identificador.setUri(null);
                } 
                
                entidadContratante.setIdentifier(identificador);
                
                
                address = new ContratacionesAddress();
                
                if(request.getParameter("txtPROCURING_ENTITY_ADDRESS_STREET_ADDRESS") != null && request.getParameter("txtPROCURING_ENTITY_ADDRESS_STREET_ADDRESS").trim().equals("") == false)
                { 
                    address.setStreetAddress(request.getParameter("txtPROCURING_ENTITY_ADDRESS_STREET_ADDRESS"));
                } 
                else
                { 
                    address.setStreetAddress(null);
                } 
                
                
                if(request.getParameter("txtPROCURING_ENTITY_ADDRESS_LOCALITY") != null && request.getParameter("txtPROCURING_ENTITY_ADDRESS_LOCALITY").trim().equals("") == false)
                { 
                    address.setLocality(request.getParameter("txtPROCURING_ENTITY_ADDRESS_LOCALITY"));
                } 
                else
                { 
                    address.setLocality(null);
                } 
                
                
                if(request.getParameter("txtPROCURING_ENTITY_ADDRESS_REGION") != null && request.getParameter("txtPROCURING_ENTITY_ADDRESS_REGION").trim().equals("") == false)
                { 
                    address.setRegion(request.getParameter("txtPROCURING_ENTITY_ADDRESS_REGION"));
                } 
                else
                { 
                    address.setRegion(null);
                } 
                
                
                if(request.getParameter("txtPROCURING_ENTITY_ADDRESS_POSTAL_CODE") != null && request.getParameter("txtPROCURING_ENTITY_ADDRESS_POSTAL_CODE").trim().equals("") == false)
                { 
                    address.setPostalCode(request.getParameter("txtPROCURING_ENTITY_ADDRESS_POSTAL_CODE"));
                } 
                else
                { 
                    address.setPostalCode(null);
                } 
                
                
                if(request.getParameter("txtPROCURING_ENTITY_ADDRESS_COUNTRY_NAME") != null && request.getParameter("txtPROCURING_ENTITY_ADDRESS_COUNTRY_NAME").trim().equals("") == false)
                { 
                    address.setCountryName(request.getParameter("txtPROCURING_ENTITY_ADDRESS_COUNTRY_NAME"));
                } 
                else
                { 
                    address.setCountryName(null);
                } 
                
                entidadContratante.setAddress(address);
                
                
                contactPoint = new ContratacionesContactPoint();
                
                if(request.getParameter("txtPROCURING_ENTITY_CONTACT_NAME") != null && request.getParameter("txtPROCURING_ENTITY_CONTACT_NAME").trim().equals("") == false)
                { 
                    contactPoint.setName(request.getParameter("txtPROCURING_ENTITY_CONTACT_NAME"));
                } 
                else
                { 
                    contactPoint.setName(null);
                } 
                
                
                if(request.getParameter("txtPROCURING_ENTITY_CONTACT_EMAIL") != null && request.getParameter("txtPROCURING_ENTITY_CONTACT_EMAIL").trim().equals("") == false)
                { 
                    contactPoint.setEmail(request.getParameter("txtPROCURING_ENTITY_CONTACT_EMAIL"));
                } 
                else
                { 
                    contactPoint.setEmail(null);
                } 
                
                
                if(request.getParameter("txtPROCURING_ENTITY_CONTACT_TELEPHONE") != null && request.getParameter("txtPROCURING_ENTITY_CONTACT_TELEPHONE").trim().equals("") == false)
                { 
                    contactPoint.setTelephone(request.getParameter("txtPROCURING_ENTITY_CONTACT_TELEPHONE"));
                } 
                else
                { 
                    contactPoint.setTelephone(null);
                } 
                
                
                if(request.getParameter("txtPROCURING_ENTITY_CONTACT_FAXNUMBER") != null && request.getParameter("txtPROCURING_ENTITY_CONTACT_FAXNUMBER").trim().equals("") == false)
                { 
                    contactPoint.setFaxNumber(request.getParameter("txtPROCURING_ENTITY_CONTACT_FAXNUMBER"));
                } 
                else
                { 
                    contactPoint.setFaxNumber(null);
                } 
                
                
                if(request.getParameter("txtPROCURING_ENTITY_CONTACT_URL") != null && request.getParameter("txtPROCURING_ENTITY_CONTACT_URL").trim().equals("") == false)
                { 
                    contactPoint.setUrl(request.getParameter("txtPROCURING_ENTITY_CONTACT_URL"));
                } 
                else
                { 
                    contactPoint.setUrl(null);
                } 
                
                entidadContratante.setContactPoint(contactPoint);
                
                
                listaDeIdentificadoresAdicionales = new ArrayList<>();
                if(request.getParameter("lista-identificadores-por-registrar-identifier") != null)
                { 

                    listaDeIdentificadoresARegistrarNivelUno = request.getParameter("lista-identificadores-por-registrar-identifier").split(",");

                    for (String identificadorIdentifierAdicional : listaDeIdentificadoresARegistrarNivelUno)
                    { 

                        identificadorIdentifierAdicional = identificadorIdentifierAdicional.trim();

                        if(identificadorIdentifierAdicional.equals("") == false)
                        { 

                            if(request.getParameter("txtIDENTIFIER_ID" + "_" + identificadorIdentifierAdicional) != null)
                            { 
                                if(request.getParameter("txtIDENTIFIER_ID" + "_" + identificadorIdentifierAdicional).equals("") == false)
                                { 
                                    identificadorAdicional = new ContratacionesIdentifier();
                                    
                                    identificadorAdicional.setId(request.getParameter("txtIDENTIFIER_ID_" + identificadorIdentifierAdicional));
                                    
                                    
                                    if(request.getParameter("txtIDENTIFIER_SCHEME_" + identificadorIdentifierAdicional) != null && request.getParameter("txtIDENTIFIER_SCHEME_" + identificadorIdentifierAdicional).trim().equals("") == false)
                                    { 
                                        identificadorAdicional.setScheme(request.getParameter("txtIDENTIFIER_SCHEME_" + identificadorIdentifierAdicional));
                                    } 
                                    else
                                    { 
                                        identificadorAdicional.setScheme(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtIDENTIFIER_LEGAL_NAME_" + identificadorIdentifierAdicional) != null && request.getParameter("txtIDENTIFIER_LEGAL_NAME_" + identificadorIdentifierAdicional).trim().equals("") == false)
                                    { 
                                        identificadorAdicional.setLegalName(request.getParameter("txtIDENTIFIER_LEGAL_NAME_" + identificadorIdentifierAdicional));
                                    } 
                                    else
                                    { 
                                        identificadorAdicional.setLegalName(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtIDENTIFIER_URI_" + identificadorIdentifierAdicional) != null && request.getParameter("txtIDENTIFIER_URI_" + identificadorIdentifierAdicional).trim().equals("") == false)
                                    { 
                                        identificadorAdicional.setUri(request.getParameter("txtIDENTIFIER_URI_" + identificadorIdentifierAdicional));
                                    } 
                                    else
                                    { 
                                        identificadorAdicional.setUri(null);
                                    } 
                                    
                                    listaDeIdentificadoresAdicionales.add(identificadorAdicional);
                                } 
                            } 

                        } 

                    } 

                } 

                entidadContratante.setAdditionalIdentifiers(listaDeIdentificadoresAdicionales);
                
                licitacion.setProcuringEntity(entidadContratante);
                
                
                enmienda = new ContratacionesAmendments();
                
                if(request.getParameter("txtAMENDMENT_ID") != null && request.getParameter("txtAMENDMENT_ID").trim().equals("") == false)
                { 
                    enmienda.setId(request.getParameter("txtAMENDMENT_ID"));
                } 
                else
                { 
                    enmienda.setId(null);
                } 
                
                
                if(request.getParameter("txtAMENDMENT_DATE") != null && request.getParameter("txtAMENDMENT_DATE").trim().equals("") == false)
                { 
                    enmienda.setDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtAMENDMENT_DATE")));
                } 
                else
                { 
                    enmienda.setDate(null);
                } 
                
                
                if(request.getParameter("txtAMENDMENT_RATIONALE") != null && request.getParameter("txtAMENDMENT_RATIONALE").trim().equals("") == false)
                { 
                    enmienda.setRationale(request.getParameter("txtAMENDMENT_RATIONALE"));
                } 
                else
                { 
                    enmienda.setRationale(null);
                } 
                
                
                if(request.getParameter("txtAMENDMENT_DESCRIPTION") != null && request.getParameter("txtAMENDMENT_DESCRIPTION").trim().equals("") == false)
                { 
                    enmienda.setDescription(request.getParameter("txtAMENDMENT_DESCRIPTION"));
                } 
                else
                { 
                    enmienda.setDescription(null);
                } 
                
                
                if(request.getParameter("txtAMENDMENT_AMENDS_RELEASE_ID") != null && request.getParameter("txtAMENDMENT_AMENDS_RELEASE_ID").trim().equals("") == false)
                { 
                    enmienda.setAmendsReleaseID(request.getParameter("txtAMENDMENT_AMENDS_RELEASE_ID"));
                } 
                else
                { 
                    enmienda.setAmendsReleaseID(null);
                } 
                
                
                if(request.getParameter("txtAMENDMENT_RELEASE_ID") != null && request.getParameter("txtAMENDMENT_RELEASE_ID").trim().equals("") == false)
                { 
                    enmienda.setReleaseID(request.getParameter("txtAMENDMENT_RELEASE_ID"));
                } 
                else
                { 
                    enmienda.setReleaseID(null);
                } 
                
                
                listaDeCambios = new ArrayList<>();
                if(request.getParameter("lista-identificadores-por-registrar-change") != null)
                { 

                    listaDeIdentificadoresARegistrarNivelUno = request.getParameter("lista-identificadores-por-registrar-change").split(",");

                    for (String identificadorElementoChange : listaDeIdentificadoresARegistrarNivelUno)
                    { 

                        identificadorElementoChange = identificadorElementoChange.trim();

                        if(identificadorElementoChange.equals("") == false)
                        { 

                            if(request.getParameter("txtCHANGE_PROPERTY_" + identificadorElementoChange) != null)
                            { 
                                if(request.getParameter("txtCHANGE_PROPERTY_" + identificadorElementoChange).equals("") == false)
                                { 
                                    cambio = new ContratacionesChanges();
                                    
                                    cambio.setProperty(request.getParameter("txtCHANGE_PROPERTY_" + identificadorElementoChange));
                                    
                                    
                                    if(request.getParameter("txtCHANGE_FORMATER_VALUE_" + identificadorElementoChange) != null && request.getParameter("txtCHANGE_FORMATER_VALUE_" + identificadorElementoChange).trim().equals("") == false)
                                    { 
                                        cambio.setFormer_value(request.getParameter("txtCHANGE_FORMATER_VALUE_" + identificadorElementoChange));
                                    } 
                                    else
                                    { 
                                        cambio.setFormer_value(null);
                                    } 
                                    
                                    listaDeCambios.add(cambio);
                                } 
                            } 

                        } 

                    } 

                } 

                enmienda.setChanges(listaDeCambios);
                
                licitacion.setAmendment(enmienda);
                
                
                ContratacionesAmount valor;
                valor = new ContratacionesAmount();
                
                if(request.getParameter("txtVALUE_AMOUNT") != null && request.getParameter("txtVALUE_AMOUNT").trim().equals("") == false)
                { 
                    valor.setAmount(Float.parseFloat(request.getParameter("txtVALUE_AMOUNT")));
                } 
                else
                { 
                    valor.setAmount(null);
                } 
                
                
                if(request.getParameter("txtVALUE_CURRENCY") != null && request.getParameter("txtVALUE_CURRENCY").trim().equals("") == false)
                { 
                    valor.setCurrency(request.getParameter("txtVALUE_CURRENCY"));
                } 
                else
                { 
                    valor.setCurrency(null);
                } 
                
                licitacion.setValue(valor);
                
                
                ContratacionesAmount valorMinimo;
                valorMinimo = new ContratacionesAmount();
                
                if(request.getParameter("txtMIN_VALUE_AMOUNT") != null && request.getParameter("txtMIN_VALUE_AMOUNT").trim().equals("") == false)
                { 
                    valorMinimo.setAmount(Float.parseFloat(request.getParameter("txtMIN_VALUE_AMOUNT")));
                } 
                else
                { 
                    valorMinimo.setAmount(null);
                } 
                
                
                if(request.getParameter("txtMIN_VALUE_CURRENCY") != null && request.getParameter("txtMIN_VALUE_CURRENCY").trim().equals("") == false)
                { 
                    valorMinimo.setCurrency(request.getParameter("txtMIN_VALUE_CURRENCY"));
                } 
                else
                { 
                    valorMinimo.setCurrency(null);
                } 
                
                licitacion.setMinValue(valorMinimo);
                
                
                if(request.getParameter("cmbPROCUREMENT_METHOD") != null && request.getParameter("cmbPROCUREMENT_METHOD").trim().equals("") == false)
                { 
                    licitacion.setProcurementMethod(request.getParameter("cmbPROCUREMENT_METHOD"));
                } 
                else
                { 
                    licitacion.setProcurementMethod(null);
                } 
                
                
                if(request.getParameter("txtPROCUREMENT_METHOD_DETAILS") != null && request.getParameter("txtPROCUREMENT_METHOD_DETAILS").trim().equals("") == false)
                { 
                    licitacion.setProcurementMethodDetails(request.getParameter("txtPROCUREMENT_METHOD_DETAILS"));
                } 
                else
                { 
                    licitacion.setProcurementMethodDetails(null);
                } 
                
                
                if(request.getParameter("txtPROCUREMENT_METHOD_RATIONALE") != null && request.getParameter("txtPROCUREMENT_METHOD_RATIONALE").trim().equals("") == false)
                { 
                    licitacion.setProcurementMethodRationale(request.getParameter("txtPROCUREMENT_METHOD_RATIONALE"));
                } 
                else
                { 
                    licitacion.setProcurementMethodRationale(null);
                } 
                
                
                if(request.getParameter("cmbMAIN_PROCUREMENT_CATEGORY") != null && request.getParameter("cmbMAIN_PROCUREMENT_CATEGORY").trim().equals("") == false)
                { 
                    licitacion.setMainProcurementCategory(request.getParameter("cmbMAIN_PROCUREMENT_CATEGORY"));
                } 
                else
                { 
                    licitacion.setMainProcurementCategory(null);
                } 
                
                
                if (request.getParameterValues("chckADD_PROCUREMENT_CATEGORIES") == null || request.getParameterValues("chckADD_PROCUREMENT_CATEGORIES").length == 0)
                { 
                    licitacion.setAdditionalProcurementCategories(new ArrayList<>());
                } 
                else
                { 
                    licitacion.setAdditionalProcurementCategories(new ArrayList(Arrays.asList(request.getParameterValues("chckADD_PROCUREMENT_CATEGORIES"))));
                } 
                
                
                if(request.getParameter("cmbAWARD_CRITERIA") != null && request.getParameter("cmbAWARD_CRITERIA").trim().equals("") == false)
                { 
                    licitacion.setAwardCriteria(request.getParameter("cmbAWARD_CRITERIA"));
                } 
                else
                { 
                    licitacion.setAwardCriteria(null);
                } 
                
                
                if(request.getParameter("txtAWARD_CRITERIA_DETAILS") != null && request.getParameter("txtAWARD_CRITERIA_DETAILS").trim().equals("") == false)
                { 
                    licitacion.setAwardCriteriaDetails(request.getParameter("txtAWARD_CRITERIA_DETAILS"));
                } 
                else
                { 
                    licitacion.setAwardCriteriaDetails(null);
                } 
                
                
                if (request.getParameterValues("chckSUBMISSION_METHOD") == null || request.getParameterValues("chckSUBMISSION_METHOD").length == 0)
                { 
                    licitacion.setSubmissionMethod(new ArrayList<>());
                } 
                else
                { 
                    licitacion.setSubmissionMethod(new ArrayList(Arrays.asList(request.getParameterValues("chckSUBMISSION_METHOD"))));
                } 
                
                
                if(request.getParameter("txtSUBMISSION_METHOD_DETAILS") != null && request.getParameter("txtSUBMISSION_METHOD_DETAILS").trim().equals("") == false)
                { 
                    licitacion.setSubmissionMethodDetails(request.getParameter("txtSUBMISSION_METHOD_DETAILS"));
                } 
                else
                { 
                    licitacion.setSubmissionMethodDetails(null);
                } 
                
                
                if(request.getParameter("txtELIGIBILITY_CRITERIA") != null && request.getParameter("txtELIGIBILITY_CRITERIA").trim().equals("") == false)
                { 
                    licitacion.setEligibilityCriteria(request.getParameter("txtELIGIBILITY_CRITERIA"));
                } 
                else
                { 
                    licitacion.setEligibilityCriteria(null);
                } 
                
                
                Boolean hasEnquiries = null;
                if((request.getParameter("radHAS_ENQUIRIES") != null))
                { 
                    if(request.getParameter("radHAS_ENQUIRIES").equals("si") == true)
                    { 
                        hasEnquiries = true;
                    } 
                    if(request.getParameter("radHAS_ENQUIRIES").equals("no") == true)
                    { 
                        hasEnquiries = false;
                    } 
                } 
                licitacion.setHasEnquiries(hasEnquiries);
                
                
                break;
            case 2:
                
                ContratacionesTenderPeriod periodoLicitacion;
                periodoLicitacion = new ContratacionesTenderPeriod();
                
                if(request.getParameter("txtSTART_DATE") != null && request.getParameter("txtSTART_DATE").trim().equals("") == false)
                { 
                    periodoLicitacion.setStartDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtSTART_DATE")));
                } 
                else
                { 
                    periodoLicitacion.setStartDate(null);
                } 
                
                
                if(request.getParameter("txtEND_DATE") != null && request.getParameter("txtEND_DATE").trim().equals("") == false)
                { 
                    periodoLicitacion.setEndDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtEND_DATE")));
                } 
                else
                { 
                    periodoLicitacion.setEndDate(null);
                } 
                
                
                if(request.getParameter("txtMAX_EXTENT_DATE") != null && request.getParameter("txtMAX_EXTENT_DATE").trim().equals("") == false)
                { 
                    periodoLicitacion.setMaxExtentDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtMAX_EXTENT_DATE")));
                } 
                else
                { 
                    periodoLicitacion.setMaxExtentDate(null);
                } 
                
                
                if(request.getParameter("txtDURATION_IN_DATE") != null && request.getParameter("txtDURATION_IN_DATE").trim().equals("") == false)
                { 
                    periodoLicitacion.setDurationInDays(Integer.parseInt(request.getParameter("txtDURATION_IN_DATE")));
                } 
                else
                { 
                    periodoLicitacion.setDurationInDays(null);
                } 
                
                licitacion.setTenderPeriod(periodoLicitacion);
                
                break;
            case 3:
                
                ContratacionesTenderPeriod periodoConsulta;
                periodoConsulta = new ContratacionesTenderPeriod();
                
                if(request.getParameter("txtSTART_DATE") != null && request.getParameter("txtSTART_DATE").trim().equals("") == false)
                { 
                    periodoConsulta.setStartDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtSTART_DATE")));
                } 
                else
                { 
                    periodoConsulta.setStartDate(null);
                } 
                
                
                if(request.getParameter("txtEND_DATE") != null && request.getParameter("txtEND_DATE").trim().equals("") == false)
                { 
                    periodoConsulta.setEndDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtEND_DATE")));
                } 
                else
                { 
                    periodoConsulta.setEndDate(null);
                } 
                
                
                if(request.getParameter("txtMAX_EXTENT_DATE") != null && request.getParameter("txtMAX_EXTENT_DATE").trim().equals("") == false)
                { 
                    periodoConsulta.setMaxExtentDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtMAX_EXTENT_DATE")));
                } 
                else
                { 
                    periodoConsulta.setMaxExtentDate(null);
                } 
                
                
                if(request.getParameter("txtDURATION_IN_DATE") != null && request.getParameter("txtDURATION_IN_DATE").trim().equals("") == false)
                { 
                    periodoConsulta.setDurationInDays(Integer.parseInt(request.getParameter("txtDURATION_IN_DATE")));
                } 
                else
                { 
                    periodoConsulta.setDurationInDays(null);
                } 
                
                licitacion.setEnquiryPeriod(periodoConsulta);
                
                break;
            case 4:
                
                ContratacionesTenderPeriod periodoEvaluacionAdjudicacion;
                periodoEvaluacionAdjudicacion = new ContratacionesTenderPeriod();
                
                if(request.getParameter("txtSTART_DATE") != null && request.getParameter("txtSTART_DATE").trim().equals("") == false)
                { 
                    periodoEvaluacionAdjudicacion.setStartDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtSTART_DATE")));
                } 
                else
                { 
                    periodoEvaluacionAdjudicacion.setStartDate(null);
                } 
                
                
                if(request.getParameter("txtEND_DATE") != null && request.getParameter("txtEND_DATE").trim().equals("") == false)
                { 
                    periodoEvaluacionAdjudicacion.setEndDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtEND_DATE")));
                } 
                else
                { 
                    periodoEvaluacionAdjudicacion.setEndDate(null);
                } 
                
                
                if(request.getParameter("txtMAX_EXTENT_DATE") != null && request.getParameter("txtMAX_EXTENT_DATE").trim().equals("") == false)
                { 
                    periodoEvaluacionAdjudicacion.setMaxExtentDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtMAX_EXTENT_DATE")));
                } 
                else
                { 
                    periodoEvaluacionAdjudicacion.setMaxExtentDate(null);
                } 
                
                
                if(request.getParameter("txtDURATION_IN_DATE") != null && request.getParameter("txtDURATION_IN_DATE").trim().equals("") == false)
                { 
                    periodoEvaluacionAdjudicacion.setDurationInDays(Integer.parseInt(request.getParameter("txtDURATION_IN_DATE")));
                } 
                else
                { 
                    periodoEvaluacionAdjudicacion.setDurationInDays(null);
                } 
                
                licitacion.setAwardPeriod(periodoEvaluacionAdjudicacion);
                
                break;
            case 5:
                
                ContratacionesTenderPeriod periodoContrato;
                periodoContrato = new ContratacionesTenderPeriod();
                
                if(request.getParameter("txtSTART_DATE") != null && request.getParameter("txtSTART_DATE").trim().equals("") == false)
                { 
                    periodoContrato.setStartDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtSTART_DATE")));
                } 
                else
                { 
                    periodoContrato.setStartDate(null);
                } 
                
                
                if(request.getParameter("txtEND_DATE") != null && request.getParameter("txtEND_DATE").trim().equals("") == false)
                { 
                    periodoContrato.setEndDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtEND_DATE")));
                } 
                else
                { 
                    periodoContrato.setEndDate(null);
                } 
                
                
                if(request.getParameter("txtMAX_EXTENT_DATE") != null && request.getParameter("txtMAX_EXTENT_DATE").trim().equals("") == false)
                { 
                    periodoContrato.setMaxExtentDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtMAX_EXTENT_DATE")));
                } 
                else
                { 
                    periodoContrato.setMaxExtentDate(null);
                } 
                
                
                if(request.getParameter("txtDURATION_IN_DATE") != null && request.getParameter("txtDURATION_IN_DATE").trim().equals("") == false)
                { 
                    periodoContrato.setDurationInDays(Integer.parseInt(request.getParameter("txtDURATION_IN_DATE")));
                } 
                else
                { 
                    periodoContrato.setDurationInDays(null);
                } 
                
                licitacion.setContractPeriod(periodoContrato);
                
                break;
            case 6:
                
                listaDeItems = new ArrayList<>();
                if(request.getParameter("lista-identificadores-por-registrar-item") != null)
                { 

                    listaDeIdentificadoresARegistrarNivelUno = request.getParameter("lista-identificadores-por-registrar-item").split(",");

                    for (String identificadorItem : listaDeIdentificadoresARegistrarNivelUno)
                    { 

                        identificadorItem = identificadorItem.trim();

                        if(identificadorItem.equals("") == false)
                        { 

                            if(request.getParameter("txtITEM_ID_" + identificadorItem) != null)
                            { 
                                if(request.getParameter("txtITEM_ID_" + identificadorItem).equals("") == false)
                                { 
                                    item = new ContratacionesItems();
                                    
                                    item.setId(request.getParameter("txtITEM_ID_" + identificadorItem));
                                    
                                    
                                    if(request.getParameter("txtITEM_DESCRIPTION_" + identificadorItem) != null && request.getParameter("txtITEM_DESCRIPTION_" + identificadorItem).trim().equals("") == false)
                                    { 
                                        item.setDescription(request.getParameter("txtITEM_DESCRIPTION_" + identificadorItem));
                                    } 
                                    else
                                    { 
                                        item.setDescription(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtITEM_QUANTITY_" + identificadorItem) != null && request.getParameter("txtITEM_QUANTITY_" + identificadorItem).trim().equals("") == false)
                                    { 
                                        item.setQuantity(Integer.parseInt(request.getParameter("txtITEM_QUANTITY_" + identificadorItem)));
                                    } 
                                    else
                                    { 
                                        item.setQuantity(null);
                                    } 
                                    
                                    
                                    clasificacion = new ContratacionesClassification();
                                    
                                    if(request.getParameter("txtITEM_CLASSIFICATION_ID_" + identificadorItem) != null && request.getParameter("txtITEM_CLASSIFICATION_ID_" + identificadorItem).trim().equals("") == false)
                                    { 
                                        clasificacion.setId(request.getParameter("txtITEM_CLASSIFICATION_ID_" + identificadorItem));
                                    } 
                                    else
                                    { 
                                        clasificacion.setId(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("cmbITEM_CLASSIFICATION_SCHEME_" + identificadorItem) != null && request.getParameter("cmbITEM_CLASSIFICATION_SCHEME_" + identificadorItem).trim().equals("") == false)
                                    { 
                                        clasificacion.setScheme(request.getParameter("cmbITEM_CLASSIFICATION_SCHEME_" + identificadorItem));
                                    } 
                                    else
                                    { 
                                        clasificacion.setScheme(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtITEM_CLASSIFICATION_DESCRIPTION_" + identificadorItem) != null && request.getParameter("txtITEM_CLASSIFICATION_DESCRIPTION_" + identificadorItem).trim().equals("") == false)
                                    { 
                                        clasificacion.setDescription(request.getParameter("txtITEM_CLASSIFICATION_DESCRIPTION_" + identificadorItem));
                                    } 
                                    else
                                    { 
                                        clasificacion.setDescription(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtITEM_CLASSIFICATION_URI_" + identificadorItem) != null && request.getParameter("txtITEM_CLASSIFICATION_URI_" + identificadorItem).trim().equals("") == false)
                                    { 
                                        clasificacion.setUri(request.getParameter("txtITEM_CLASSIFICATION_URI_" + identificadorItem));
                                    } 
                                    else
                                    { 
                                        clasificacion.setUri(null);
                                    } 
                                    
                                    item.setClassification(clasificacion);
                                    
                                    
                                    listaDeClasificacionesAdicionales = new ArrayList<>();
                                    if(request.getParameter("lista-identificadores-por-registrar-classification_" + identificadorItem) != null)
                                    { 

                                        listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-classification_" + identificadorItem).split(",");

                                        for (String identificadorClasificacionAdicional : listaDeIdentificadoresARegistrarNivelDos)
                                        { 

                                            identificadorClasificacionAdicional = identificadorClasificacionAdicional.trim();

                                            if(identificadorClasificacionAdicional.equals("") == false)
                                            { 

                                                if(request.getParameter("txtCLASSIFICATION_ID_" + identificadorItem + "_" + identificadorClasificacionAdicional) != null)
                                                { 
                                                    if(request.getParameter("txtCLASSIFICATION_ID_" + identificadorItem + "_" + identificadorClasificacionAdicional).equals("") == false)
                                                    { 
                                                        clasificacionAdicional = new ContratacionesClassification();
                                                        
                                                        clasificacionAdicional.setId(request.getParameter("txtCLASSIFICATION_ID_" + identificadorItem + "_" + identificadorClasificacionAdicional));
                                                        
                                                        
                                                        if(request.getParameter("cmbCLASSIFICATION_SCHEME_" + identificadorItem + "_" + identificadorClasificacionAdicional) != null && request.getParameter("cmbCLASSIFICATION_SCHEME_" + identificadorItem + "_" + identificadorClasificacionAdicional).trim().equals("") == false)
                                                        { 
                                                            clasificacionAdicional.setScheme(request.getParameter("cmbCLASSIFICATION_SCHEME_" + identificadorItem + "_" + identificadorClasificacionAdicional));
                                                        } 
                                                        else
                                                        { 
                                                            clasificacionAdicional.setScheme(null);
                                                        } 
                                                        
                                                        
                                                        if(request.getParameter("txtCLASSIFICATION_DESCRIPTION_" + identificadorItem + "_" + identificadorClasificacionAdicional) != null && request.getParameter("txtCLASSIFICATION_DESCRIPTION_" + identificadorItem + "_" + identificadorClasificacionAdicional).trim().equals("") == false)
                                                        { 
                                                            clasificacionAdicional.setDescription(request.getParameter("txtCLASSIFICATION_DESCRIPTION_" + identificadorItem + "_" + identificadorClasificacionAdicional));
                                                        } 
                                                        else
                                                        { 
                                                            clasificacionAdicional.setDescription(null);
                                                        } 
                                                        
                                                        
                                                        if(request.getParameter("txtCLASSIFICATION_URI_" + identificadorItem + "_" + identificadorClasificacionAdicional) != null && request.getParameter("txtCLASSIFICATION_URI_" + identificadorItem + "_" + identificadorClasificacionAdicional).trim().equals("") == false)
                                                        { 
                                                            clasificacionAdicional.setUri(request.getParameter("txtCLASSIFICATION_URI_" + identificadorItem + "_" + identificadorClasificacionAdicional));
                                                        } 
                                                        else
                                                        { 
                                                            clasificacionAdicional.setUri(null);
                                                        } 
                                                        
                                                        listaDeClasificacionesAdicionales.add(clasificacionAdicional);
                                                    } 
                                                } 

                                            } 

                                        } 

                                    } 

                                    item.setAdditionalClassifications(listaDeClasificacionesAdicionales);
                                    
                                    
                                    unit = new ContratacionesUnit();
                                    
                                    if(request.getParameter("txtITEM_UNIT_ID_" + identificadorItem) != null && request.getParameter("txtITEM_UNIT_ID_" + identificadorItem).trim().equals("") == false)
                                    { 
                                        unit.setId(request.getParameter("txtITEM_UNIT_ID_" + identificadorItem));
                                    } 
                                    else
                                    { 
                                        unit.setId(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtITEM_UNIT_NAME_" + identificadorItem) != null && request.getParameter("txtITEM_UNIT_NAME_" + identificadorItem).trim().equals("") == false)
                                    { 
                                        unit.setName(request.getParameter("txtITEM_UNIT_NAME_" + identificadorItem));
                                    } 
                                    else
                                    { 
                                        unit.setName(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("cmbITEM_UNIT_SCHEME_" + identificadorItem) != null && request.getParameter("cmbITEM_UNIT_SCHEME_" + identificadorItem).trim().equals("") == false)
                                    { 
                                        unit.setScheme(request.getParameter("cmbITEM_UNIT_SCHEME_" + identificadorItem));
                                    } 
                                    else
                                    { 
                                        unit.setScheme(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtITEM_UNIT_URI_" + identificadorItem) != null && request.getParameter("txtITEM_UNIT_URI_" + identificadorItem).trim().equals("") == false)
                                    { 
                                        unit.setUri(request.getParameter("txtITEM_UNIT_URI_" + identificadorItem));
                                    } 
                                    else
                                    { 
                                        unit.setUri(null);
                                    } 
                                    
                                    
                                    amount = new ContratacionesAmount();
                                    
                                    if(request.getParameter("txtITEM_UNIT_AMOUNT_" + identificadorItem) != null && request.getParameter("txtITEM_UNIT_AMOUNT_" + identificadorItem).trim().equals("") == false)
                                    { 
                                        amount.setAmount(Float.parseFloat(request.getParameter("txtITEM_UNIT_AMOUNT_" + identificadorItem)));
                                    } 
                                    else
                                    { 
                                        amount.setAmount(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("cmbITEM_UNIT_CURRENCY_" + identificadorItem) != null && request.getParameter("cmbITEM_UNIT_CURRENCY_" + identificadorItem).trim().equals("") == false)
                                    { 
                                        amount.setCurrency(request.getParameter("cmbITEM_UNIT_CURRENCY_" + identificadorItem));
                                    } 
                                    else
                                    { 
                                        amount.setCurrency(null);
                                    } 
                                    
                                    unit.setValue(amount);
                                    
                                    item.setUnit(unit);
                                    
                                    listaDeItems.add(item); 
                                } 
                            } 

                        } 

                    } 

                } 

                licitacion.setItems(listaDeItems);
                
                break;
            case 7:
                
                ArrayList<ContratacionesTenderers> listaDeLicitantes;
                ContratacionesTenderers licitante;
                int intTotalLicitantes; 
                
                listaDeLicitantes = new ArrayList<>();
                if(request.getParameter("lista-identificadores-por-registrar-tenderer") != null)
                { 

                    listaDeIdentificadoresARegistrarNivelUno = request.getParameter("lista-identificadores-por-registrar-tenderer").split(",");

                    for (String identificadorLicitante : listaDeIdentificadoresARegistrarNivelUno)
                    { 

                        identificadorLicitante = identificadorLicitante.trim();

                        if(identificadorLicitante.equals("") == false)
                        { 

                            if(request.getParameter("txtTENDERER_ID_" + identificadorLicitante) != null)
                            { 
                                if(request.getParameter("txtTENDERER_ID_" + identificadorLicitante).equals("") == false)
                                { 
                                    licitante = new ContratacionesTenderers();
                                    
                                    licitante.setId(request.getParameter("txtTENDERER_ID_" + identificadorLicitante));
                                    
                                    
                                    if(request.getParameter("txtTENDERER_NAME_" + identificadorLicitante) != null && request.getParameter("txtTENDERER_NAME_" + identificadorLicitante).trim().equals("") == false)
                                    { 
                                        licitante.setName(request.getParameter("txtTENDERER_NAME_" + identificadorLicitante));
                                    } 
                                    else
                                    { 
                                        licitante.setName(null);
                                    } 
                                    
                                    
                                    identificador = new ContratacionesIdentifier();
                                    
                                    if(request.getParameter("txtTENDERER_IDENTIFIER_ID_" + identificadorLicitante) != null && request.getParameter("txtTENDERER_IDENTIFIER_ID_" + identificadorLicitante).trim().equals("") == false)
                                    { 
                                        identificador.setId(request.getParameter("txtTENDERER_IDENTIFIER_ID_" + identificadorLicitante));
                                    } 
                                    else
                                    { 
                                        identificador.setId(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtTENDERER_IDENTIFIER_SCHEME_" + identificadorLicitante) != null && request.getParameter("txtTENDERER_IDENTIFIER_SCHEME_" + identificadorLicitante).trim().equals("") == false)
                                    { 
                                        identificador.setScheme(request.getParameter("txtTENDERER_IDENTIFIER_SCHEME_" + identificadorLicitante));
                                    } 
                                    else
                                    { 
                                        identificador.setScheme(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtTENDERER_IDENTIFIER_LEGAL_NAME_" + identificadorLicitante) != null && request.getParameter("txtTENDERER_IDENTIFIER_LEGAL_NAME_" + identificadorLicitante).trim().equals("") == false)
                                    { 
                                        identificador.setLegalName(request.getParameter("txtTENDERER_IDENTIFIER_LEGAL_NAME_" + identificadorLicitante));
                                    } 
                                    else
                                    { 
                                        identificador.setLegalName(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtTENDERER_IDENTIFIER_URI_" + identificadorLicitante) != null && request.getParameter("txtTENDERER_IDENTIFIER_URI_" + identificadorLicitante).trim().equals("") == false)
                                    { 
                                        identificador.setUri(request.getParameter("txtTENDERER_IDENTIFIER_URI_" + identificadorLicitante));
                                    } 
                                    else
                                    { 
                                        identificador.setUri(null);
                                    } 
                                    
                                    licitante.setIdentifier(identificador);
                                    
                                    
                                    address = new ContratacionesAddress();
                                    
                                    if(request.getParameter("txtTENDERER_ADDRESS_STREET_ADDRESS_" + identificadorLicitante) != null && request.getParameter("txtTENDERER_ADDRESS_STREET_ADDRESS_" + identificadorLicitante).trim().equals("") == false)
                                    { 
                                        address.setStreetAddress(request.getParameter("txtTENDERER_ADDRESS_STREET_ADDRESS_" + identificadorLicitante));
                                    } 
                                    else
                                    { 
                                        address.setStreetAddress(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtTENDERER_ADDRESS_LOCALITY_" + identificadorLicitante) != null && request.getParameter("txtTENDERER_ADDRESS_LOCALITY_" + identificadorLicitante).trim().equals("") == false)
                                    { 
                                        address.setLocality(request.getParameter("txtTENDERER_ADDRESS_LOCALITY_" + identificadorLicitante));
                                    } 
                                    else
                                    { 
                                        address.setLocality(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtTENDERER_ADDRESS_REGION_" + identificadorLicitante) != null && request.getParameter("txtTENDERER_ADDRESS_REGION_" + identificadorLicitante).trim().equals("") == false)
                                    { 
                                        address.setRegion(request.getParameter("txtTENDERER_ADDRESS_REGION_" + identificadorLicitante));
                                    } 
                                    else
                                    { 
                                        address.setRegion(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtTENDERER_ADDRESS_POSTAL_CODE_" + identificadorLicitante) != null && request.getParameter("txtTENDERER_ADDRESS_POSTAL_CODE_" + identificadorLicitante).trim().equals("") == false)
                                    { 
                                        address.setPostalCode(request.getParameter("txtTENDERER_ADDRESS_POSTAL_CODE_" + identificadorLicitante));
                                    } 
                                    else
                                    { 
                                        address.setPostalCode(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtTENDERER_ADDRESS_COUNTRY_NAME_" + identificadorLicitante) != null && request.getParameter("txtTENDERER_ADDRESS_COUNTRY_NAME_" + identificadorLicitante).trim().equals("") == false)
                                    { 
                                        address.setCountryName(request.getParameter("txtTENDERER_ADDRESS_COUNTRY_NAME_" + identificadorLicitante));
                                    } 
                                    else
                                    { 
                                        address.setCountryName(null);
                                    } 
                                    
                                    licitante.setAddress(address);
                                    
                                    
                                    contactPoint = new ContratacionesContactPoint();
                                    
                                    if(request.getParameter("txtTENDERER_CONTACT_NAME_" + identificadorLicitante) != null && request.getParameter("txtTENDERER_CONTACT_NAME_" + identificadorLicitante).trim().equals("") == false)
                                    { 
                                        contactPoint.setName(request.getParameter("txtTENDERER_CONTACT_NAME_" + identificadorLicitante));
                                    } 
                                    else
                                    { 
                                        contactPoint.setName(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtTENDERER_CONTACT_EMAIL_" + identificadorLicitante) != null && request.getParameter("txtTENDERER_CONTACT_EMAIL_" + identificadorLicitante).trim().equals("") == false)
                                    { 
                                        contactPoint.setEmail(request.getParameter("txtTENDERER_CONTACT_EMAIL_" + identificadorLicitante));
                                    } 
                                    else
                                    { 
                                        contactPoint.setEmail(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtTENDERER_CONTACT_TELEPHONE_" + identificadorLicitante) != null && request.getParameter("txtTENDERER_CONTACT_TELEPHONE_" + identificadorLicitante).trim().equals("") == false)
                                    { 
                                        contactPoint.setTelephone(request.getParameter("txtTENDERER_CONTACT_TELEPHONE_" + identificadorLicitante));
                                    } 
                                    else
                                    { 
                                        contactPoint.setTelephone(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtTENDERER_CONTACT_FAXNUMBER_" + identificadorLicitante) != null && request.getParameter("txtTENDERER_CONTACT_FAXNUMBER_" + identificadorLicitante).trim().equals("") == false)
                                    { 
                                        contactPoint.setFaxNumber(request.getParameter("txtTENDERER_CONTACT_FAXNUMBER_" + identificadorLicitante));
                                    } 
                                    else
                                    { 
                                        contactPoint.setFaxNumber(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtTENDERER_CONTACT_URL_" + identificadorLicitante) != null && request.getParameter("txtTENDERER_CONTACT_URL_" + identificadorLicitante).trim().equals("") == false)
                                    { 
                                        contactPoint.setUrl(request.getParameter("txtTENDERER_CONTACT_URL_" + identificadorLicitante));
                                    } 
                                    else
                                    { 
                                        contactPoint.setUrl(null);
                                    } 
                                    
                                    licitante.setContactPoint(contactPoint);
                                    
                                    
                                    listaDeIdentificadoresAdicionales = new ArrayList<>();
                                    if(request.getParameter("lista-identificadores-por-registrar-identifier_" + identificadorLicitante) != null)
                                    { 

                                        listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-identifier_" + identificadorLicitante).split(",");

                                        for (String identificadorIdentifierAdicional : listaDeIdentificadoresARegistrarNivelDos)
                                        { 

                                            identificadorIdentifierAdicional = identificadorIdentifierAdicional.trim();

                                            if(identificadorIdentifierAdicional.equals("") == false)
                                            { 

                                                if(request.getParameter("txtIDENTIFIER_ID_" + identificadorLicitante + "_" + identificadorIdentifierAdicional) != null)
                                                { 
                                                    if(request.getParameter("txtIDENTIFIER_ID_" + identificadorLicitante + "_" + identificadorIdentifierAdicional).equals("") == false)
                                                    { 
                                                        identificadorAdicional = new ContratacionesIdentifier();
                                                        
                                                        identificadorAdicional.setId(request.getParameter("txtIDENTIFIER_ID_" + identificadorLicitante + "_" + identificadorIdentifierAdicional));
                                                        
                                                        
                                                        if(request.getParameter("txtIDENTIFIER_SCHEME_" + identificadorLicitante + "_" + identificadorIdentifierAdicional) != null && request.getParameter("txtIDENTIFIER_SCHEME_" + identificadorLicitante + "_" + identificadorIdentifierAdicional).trim().equals("") == false)
                                                        { 
                                                            identificadorAdicional.setScheme(request.getParameter("txtIDENTIFIER_SCHEME_" + identificadorLicitante + "_" + identificadorIdentifierAdicional));
                                                        } 
                                                        else
                                                        { 
                                                            identificadorAdicional.setScheme(null);
                                                        } 
                                                        
                                                        
                                                        if(request.getParameter("txtIDENTIFIER_LEGAL_NAME_" + identificadorLicitante + "_" + identificadorIdentifierAdicional) != null && request.getParameter("txtIDENTIFIER_LEGAL_NAME_" + identificadorLicitante + "_" + identificadorIdentifierAdicional).trim().equals("") == false)
                                                        { 
                                                            identificadorAdicional.setLegalName(request.getParameter("txtIDENTIFIER_LEGAL_NAME_" + identificadorLicitante + "_" + identificadorIdentifierAdicional));
                                                        } 
                                                        else
                                                        { 
                                                            identificadorAdicional.setLegalName(null);
                                                        } 
                                                        
                                                        
                                                        if(request.getParameter("txtIDENTIFIER_URI_" + identificadorLicitante + "_" + identificadorIdentifierAdicional) != null && request.getParameter("txtIDENTIFIER_URI_" + identificadorLicitante + "_" + identificadorIdentifierAdicional).trim().equals("") == false)
                                                        { 
                                                            identificadorAdicional.setUri(request.getParameter("txtIDENTIFIER_URI_" + identificadorLicitante + "_" + identificadorIdentifierAdicional));
                                                        } 
                                                        else
                                                        { 
                                                            identificadorAdicional.setUri(null);
                                                        } 
                                                        
                                                        listaDeIdentificadoresAdicionales.add(identificadorAdicional);
                                                    } 
                                                } 

                                            } 

                                        } 

                                    } 

                                    licitante.setAdditionalIdentifiers(listaDeIdentificadoresAdicionales);
                                    
                                    listaDeLicitantes.add(licitante);
                                } 
                            } 

                        } 

                    } 

                } 
                intTotalLicitantes = listaDeLicitantes.size();
                licitacion.setTenderers(listaDeLicitantes);
                licitacion.setNumberOfTenderers(intTotalLicitantes);
                
                break;
            case 8:
                
                listaDeDocumentos = new ArrayList<>();
                if(request.getParameter("lista-identificadores-por-registrar-document") != null)
                { 

                    listaDeIdentificadoresARegistrarNivelUno = request.getParameter("lista-identificadores-por-registrar-document").split(",");

                    for (String identificadorDocumento : listaDeIdentificadoresARegistrarNivelUno)
                    { 

                        identificadorDocumento = identificadorDocumento.trim();

                        if(identificadorDocumento.equals("") == false)
                        { 

                            if(request.getParameter("txtDOCUMENT_ID_"  + identificadorDocumento) != null)
                            { 
                                if(request.getParameter("txtDOCUMENT_ID_"  + identificadorDocumento).equals("") == false)
                                { 
                                    documento = new ContratacionesDocuments();
                                    
                                    documento.setId(request.getParameter("txtDOCUMENT_ID_"  + identificadorDocumento));
                                    
                                    
                                    if(request.getParameter("cmbDOCUMENT_TYPE_"  + identificadorDocumento) != null && request.getParameter("cmbDOCUMENT_TYPE_"  + identificadorDocumento).trim().equals("") == false)
                                    { 
                                        documento.setDocumentType(request.getParameter("cmbDOCUMENT_TYPE_"  + identificadorDocumento));
                                    } 
                                    else
                                    { 
                                        documento.setDocumentType(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtDOCUMENT_TITLE_"  + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_TITLE_"  + identificadorDocumento).trim().equals("") == false)
                                    { 
                                        documento.setTitle(request.getParameter("txtDOCUMENT_TITLE_"  + identificadorDocumento));
                                    } 
                                    else
                                    { 
                                        documento.setTitle(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtDOCUMENT_DESCRIPTION_"  + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_DESCRIPTION_"  + identificadorDocumento).trim().equals("") == false)
                                    { 
                                        documento.setDescription(request.getParameter("txtDOCUMENT_DESCRIPTION_"  + identificadorDocumento));
                                    } 
                                    else
                                    { 
                                        documento.setDescription(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtDOCUMENT_URL_"  + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_URL_"  + identificadorDocumento).trim().equals("") == false)
                                    { 
                                        documento.setUrl(request.getParameter("txtDOCUMENT_URL_"  + identificadorDocumento));
                                    } 
                                    else
                                    { 
                                        documento.setUrl(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtDOCUMENT_DATE_PUBLISHED_"  + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_DATE_PUBLISHED_"  + identificadorDocumento).trim().equals("") == false)
                                    { 
                                        documento.setDatePublished(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtDOCUMENT_DATE_PUBLISHED_"  + identificadorDocumento)));
                                    } 
                                    else
                                    { 
                                        documento.setDatePublished(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtDOCUMENT_DATE_MODIFIED_"  + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_DATE_MODIFIED_"  + identificadorDocumento).trim().equals("") == false)
                                    { 
                                        documento.setDateModified(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtDOCUMENT_DATE_MODIFIED_"  + identificadorDocumento)));
                                    } 
                                    else
                                    { 
                                        documento.setDateModified(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtDOCUMENT_FORMAT_"  + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_FORMAT_"  + identificadorDocumento).trim().equals("") == false)
                                    { 
                                        documento.setFormat(request.getParameter("txtDOCUMENT_FORMAT_"  + identificadorDocumento));
                                    } 
                                    else
                                    { 
                                        documento.setFormat(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("cmbDOCUMENT_LANGUAGE_"  + identificadorDocumento) != null && request.getParameter("cmbDOCUMENT_LANGUAGE_"  + identificadorDocumento).trim().equals("") == false)
                                    { 
                                        documento.setLanguage(request.getParameter("cmbDOCUMENT_LANGUAGE_"  + identificadorDocumento));
                                    } 
                                    else
                                    { 
                                        documento.setLanguage(null);
                                    } 
                                    
                                    listaDeDocumentos.add(documento);
                                } 
                            } 

                        } 

                    } 

                } 
                
                licitacion.setDocuments(listaDeDocumentos);
                
                break;
            case 9: 
                
                listaDeHitos = new ArrayList<>();
                if(request.getParameter("lista-identificadores-por-registrar-milestone") != null)
                { 

                    listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-milestone").split(",");

                    for (String identificadorHito : listaDeIdentificadoresARegistrarNivelDos)
                    { 

                        identificadorHito = identificadorHito.trim();

                        if(identificadorHito.equals("") == false)
                        { 

                            if(request.getParameter("txtMILESTONE_ID_" + identificadorHito) != null)
                            { 
                                if(request.getParameter("txtMILESTONE_ID_" + identificadorHito).equals("") == false)
                                { 
                                    hito = new ContratacionesMilestones();
                                    
                                    hito.setId(request.getParameter("txtMILESTONE_ID_" + identificadorHito));
                                    
                                    
                                    if(request.getParameter("cmbMILESTONE_TYPE_" + identificadorHito) != null && request.getParameter("cmbMILESTONE_TYPE_" + identificadorHito).trim().equals("") == false)
                                    { 
                                        hito.setType(request.getParameter("cmbMILESTONE_TYPE_" + identificadorHito));
                                    } 
                                    else
                                    { 
                                        hito.setType(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtMILESTONE_TITLE_" + identificadorHito) != null && request.getParameter("txtMILESTONE_TITLE_" + identificadorHito).trim().equals("") == false)
                                    { 
                                        hito.setTitle(request.getParameter("txtMILESTONE_TITLE_" + identificadorHito));
                                    } 
                                    else
                                    { 
                                        hito.setTitle(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtMILESTONE_DESCRIPTION_" + identificadorHito) != null && request.getParameter("txtMILESTONE_DESCRIPTION_" + identificadorHito).trim().equals("") == false)
                                    { 
                                        hito.setDescription(request.getParameter("txtMILESTONE_DESCRIPTION_" + identificadorHito));
                                    } 
                                    else
                                    { 
                                        hito.setDescription(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtMILESTONE_CODE_" + identificadorHito) != null && request.getParameter("txtMILESTONE_CODE_" + identificadorHito).trim().equals("") == false)
                                    { 
                                        hito.setCode(request.getParameter("txtMILESTONE_CODE_" + identificadorHito));
                                    } 
                                    else
                                    { 
                                        hito.setCode(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtMILESTONE_DUE_DATE_" + identificadorHito) != null && request.getParameter("txtMILESTONE_DUE_DATE_" + identificadorHito).trim().equals("") == false)
                                    { 
                                        hito.setDueDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtMILESTONE_DUE_DATE_" + identificadorHito)));
                                    } 
                                    else
                                    { 
                                        hito.setDueDate(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtMILESTONE_DATE_MET_" + identificadorHito) != null && request.getParameter("txtMILESTONE_DATE_MET_" + identificadorHito).trim().equals("") == false)
                                    { 
                                        hito.setDateMet(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtMILESTONE_DATE_MET_" + identificadorHito)));
                                    } 
                                    else
                                    { 
                                        hito.setDateMet(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtMILESTONE_DATE_MODIFIED_" + identificadorHito) != null && request.getParameter("txtMILESTONE_DATE_MODIFIED_" + identificadorHito).trim().equals("") == false)
                                    { 
                                        hito.setDateModified(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtMILESTONE_DATE_MODIFIED_" + identificadorHito)));
                                    } 
                                    else
                                    { 
                                        hito.setDateModified(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("cmbMILESTONE_STATUS_" + identificadorHito) != null && request.getParameter("cmbMILESTONE_STATUS_" + identificadorHito).trim().equals("") == false)
                                    { 
                                        hito.setStatus(request.getParameter("cmbMILESTONE_STATUS_" + identificadorHito));
                                    } 
                                    else
                                    { 
                                        hito.setStatus(null);
                                    } 
                                    
                                    
                                    listaDeDocumentos = new ArrayList<>();
                                    if(request.getParameter("lista-identificadores-por-registrar-milestone-document_" + identificadorHito) != null)
                                    { 

                                        listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-milestone-document_" + identificadorHito).split(",");

                                        for (String identificadorDocumento : listaDeIdentificadoresARegistrarNivelDos)
                                        { 

                                            identificadorDocumento = identificadorDocumento.trim();

                                            if(identificadorDocumento.equals("") == false)
                                            { 

                                                if(request.getParameter("txtMILESTONE_DOCUMENT_ID_" + identificadorHito + "_" + identificadorDocumento) != null)
                                                { 
                                                    if(request.getParameter("txtMILESTONE_DOCUMENT_ID_" + identificadorHito + "_" + identificadorDocumento).equals("") == false)
                                                    { 
                                                        documento = new ContratacionesDocuments();
                                                        
                                                        documento.setId(request.getParameter("txtMILESTONE_DOCUMENT_ID_" + identificadorHito + "_" + identificadorDocumento));
                                                        
                                                        
                                                        if(request.getParameter("cmbMILESTONE_DOCUMENT_TYPE_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("cmbMILESTONE_DOCUMENT_TYPE_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                        { 
                                                            documento.setDocumentType(request.getParameter("cmbMILESTONE_DOCUMENT_TYPE_" + identificadorHito + "_" + identificadorDocumento));
                                                        } 
                                                        else
                                                        { 
                                                            documento.setDocumentType(null);
                                                        } 
                                                        
                                                        
                                                        if(request.getParameter("txtMILESTONE_DOCUMENT_TITLE_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtMILESTONE_DOCUMENT_TITLE_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                        { 
                                                            documento.setTitle(request.getParameter("txtMILESTONE_DOCUMENT_TITLE_" + identificadorHito + "_" + identificadorDocumento));
                                                        } 
                                                        else
                                                        { 
                                                            documento.setTitle(null);
                                                        } 
                                                        
                                                        
                                                        if(request.getParameter("txtMILESTONE_DOCUMENT_DESCRIPTION_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtMILESTONE_DOCUMENT_DESCRIPTION_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                        { 
                                                            documento.setDescription(request.getParameter("txtMILESTONE_DOCUMENT_DESCRIPTION_" + identificadorHito + "_" + identificadorDocumento));
                                                        } 
                                                        else
                                                        { 
                                                            documento.setDescription(null);
                                                        } 
                                                        
                                                        
                                                        if(request.getParameter("txtMILESTONE_DOCUMENT_URL_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtMILESTONE_DOCUMENT_URL_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                        { 
                                                            documento.setUrl(request.getParameter("txtMILESTONE_DOCUMENT_URL_" + identificadorHito + "_" + identificadorDocumento));
                                                        } 
                                                        else
                                                        { 
                                                            documento.setUrl(null);
                                                        } 
                                                        
                                                        
                                                        if(request.getParameter("txtMILESTONE_DOCUMENT_DATE_PUBLISHED_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtMILESTONE_DOCUMENT_DATE_PUBLISHED_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                        { 
                                                            documento.setDatePublished(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtMILESTONE_DOCUMENT_DATE_PUBLISHED_" + identificadorHito + "_" + identificadorDocumento)));
                                                        } 
                                                        else
                                                        { 
                                                            documento.setDatePublished(null);
                                                        } 
                                                        
                                                        
                                                        if(request.getParameter("txtMILESTONE_DOCUMENT_DATE_MODIFIED_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtMILESTONE_DOCUMENT_DATE_MODIFIED_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                        { 
                                                            documento.setDateModified(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtMILESTONE_DOCUMENT_DATE_MODIFIED_" + identificadorHito + "_" + identificadorDocumento)));
                                                        } 
                                                        else
                                                        { 
                                                            documento.setDateModified(null);
                                                        } 
                                                        
                                                        
                                                        if(request.getParameter("txtMILESTONE_DOCUMENT_FORMAT_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtMILESTONE_DOCUMENT_FORMAT_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                        { 
                                                            documento.setFormat(request.getParameter("txtMILESTONE_DOCUMENT_FORMAT_" + identificadorHito + "_" + identificadorDocumento));
                                                        } 
                                                        else
                                                        { 
                                                            documento.setFormat(null);
                                                        } 
                                                        
                                                        
                                                        if(request.getParameter("cmbMILESTONE_DOCUMENT_LANGUAGE_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("cmbMILESTONE_DOCUMENT_LANGUAGE_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                        { 
                                                            documento.setLanguage(request.getParameter("cmbMILESTONE_DOCUMENT_LANGUAGE_" + identificadorHito + "_" + identificadorDocumento));
                                                        } 
                                                        else
                                                        { 
                                                            documento.setLanguage(null);
                                                        } 
                                                        
                                                        listaDeDocumentos.add(documento);
                                                    } 
                                                } 

                                            } 

                                        } 

                                    } 

                                    hito.setDocuments(listaDeDocumentos);
                                    
                                    listaDeHitos.add(hito);   
                                } 
                            } 
                        } 

                    } 

                } 
                
                licitacion.setMilestones(listaDeHitos);
                
                break;
            case 10: 
                
                listaDeEnmiendas = new ArrayList<>();
                if(request.getParameter("lista-identificadores-por-registrar-amendment") != null)
                { 

                    listaDeIdentificadoresARegistrarNivelUno = request.getParameter("lista-identificadores-por-registrar-amendment").split(",");

                    for (String identificadorEnmienda : listaDeIdentificadoresARegistrarNivelUno)
                    { 

                        identificadorEnmienda = identificadorEnmienda.trim();

                        if(identificadorEnmienda.equals("") == false)
                        { 

                            if(request.getParameter("txtAMENDMENT_ID_" + identificadorEnmienda) != null)
                            { 
                                if(request.getParameter("txtAMENDMENT_ID_" + identificadorEnmienda).equals("") == false)
                                { 
                                    enmienda = new ContratacionesAmendments();
                                    
                                    enmienda.setId(request.getParameter("txtAMENDMENT_ID_" + identificadorEnmienda));
                                    
                                    
                                    if(request.getParameter("txtAMENDMENT_DATE_" + identificadorEnmienda) != null && request.getParameter("txtAMENDMENT_DATE_" + identificadorEnmienda).trim().equals("") == false)
                                    { 
                                        enmienda.setDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtAMENDMENT_DATE_" + identificadorEnmienda)));
                                    } 
                                    else
                                    { 
                                        enmienda.setDate(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtAMENDMENT_RATIONALE_" + identificadorEnmienda) != null && request.getParameter("txtAMENDMENT_RATIONALE_" + identificadorEnmienda).trim().equals("") == false)
                                    { 
                                        enmienda.setRationale(request.getParameter("txtAMENDMENT_RATIONALE_" + identificadorEnmienda));
                                    } 
                                    else
                                    { 
                                        enmienda.setRationale(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtAMENDMENT_DESCRIPTION_" + identificadorEnmienda) != null && request.getParameter("txtAMENDMENT_DESCRIPTION_" + identificadorEnmienda).trim().equals("") == false)
                                    { 
                                        enmienda.setDescription(request.getParameter("txtAMENDMENT_DESCRIPTION_" + identificadorEnmienda));
                                    } 
                                    else
                                    { 
                                        enmienda.setDescription(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtAMENDMENT_AMENDS_RELEASE_ID_" + identificadorEnmienda) != null && request.getParameter("txtAMENDMENT_AMENDS_RELEASE_ID_" + identificadorEnmienda).trim().equals("") == false)
                                    { 
                                        enmienda.setAmendsReleaseID(request.getParameter("txtAMENDMENT_AMENDS_RELEASE_ID_" + identificadorEnmienda));
                                    } 
                                    else
                                    { 
                                        enmienda.setAmendsReleaseID(null);
                                    } 
                                    
                                    
                                    if(request.getParameter("txtAMENDMENT_RELEASE_ID_" + identificadorEnmienda) != null && request.getParameter("txtAMENDMENT_RELEASE_ID_" + identificadorEnmienda).trim().equals("") == false)
                                    { 
                                        enmienda.setReleaseID(request.getParameter("txtAMENDMENT_RELEASE_ID_" + identificadorEnmienda));
                                    } 
                                    else
                                    { 
                                        enmienda.setReleaseID(null);
                                    } 
                                    
                                    
                                    listaDeCambios = new ArrayList<>();
                                    if(request.getParameter("lista-identificadores-por-registrar-change_" + identificadorEnmienda) != null)
                                    { 

                                        listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-change_" + identificadorEnmienda).split(",");

                                        for (String identificadorElementoChange : listaDeIdentificadoresARegistrarNivelDos)
                                        { 

                                            identificadorElementoChange = identificadorElementoChange.trim();

                                            if(identificadorElementoChange.equals("") == false)
                                            { 

                                                if(request.getParameter("txtCHANGE_PROPERTY_" + identificadorEnmienda + "_" + identificadorElementoChange) != null)
                                                { 
                                                    if(request.getParameter("txtCHANGE_PROPERTY_" + identificadorEnmienda + "_" + identificadorElementoChange).equals("") == false)
                                                    { 
                                                        cambio = new ContratacionesChanges();
                                                        
                                                        cambio.setProperty(request.getParameter("txtCHANGE_PROPERTY_" + identificadorEnmienda + "_" + identificadorElementoChange));
                                                        
                                                        
                                                        if(request.getParameter("txtCHANGE_FORMATER_VALUE_" + identificadorEnmienda + "_" + identificadorElementoChange) != null && request.getParameter("txtCHANGE_FORMATER_VALUE_" + identificadorEnmienda + "_" + identificadorElementoChange).trim().equals("") == false)
                                                        { 
                                                            cambio.setFormer_value(request.getParameter("txtCHANGE_FORMATER_VALUE_" + identificadorEnmienda + "_" + identificadorElementoChange));
                                                        } 
                                                        else
                                                        { 
                                                            cambio.setFormer_value(null);
                                                        } 
                                                        
                                                        listaDeCambios.add(cambio);
                                                    } 
                                                } 

                                            } 

                                        } 

                                    } 

                                    enmienda.setChanges(listaDeCambios);
                                    
                                    listaDeEnmiendas.add(enmienda); 
                                } 
                            } 

                        } 

                    } 

                } 

                licitacion.setAmendments(listaDeEnmiendas);
                
                break;
        } 
        
        return licitacion;
        
    } 

    /**
     *  Metodo para modelar la informacion capturada en la seccion 'Adjudicaciones' de una contratacion
     * 
     * @param request
     * 
     * @return ArrayList
     * @throws java.lang.Exception
     *
     */
    @Override
    public ArrayList<ContratacionesAwards> modelarSeccionAdjudicaciones(HttpServletRequest request) throws Exception
    { 
        
        ArrayList<ContratacionesAwards> listaDeAdjudicaciones;
        ContratacionesAwards adjudicacion;
        ArrayList<ContratacionesItems> listaDeItems;
        ContratacionesItems item;
        ContratacionesClassification clasificacion;
        ArrayList<ContratacionesClassification> listaDeClasificacionesAdicionales;
        ContratacionesClassification clasificacionAdicional;
        ArrayList<ContratacionesDocuments> listaDeDocumentos;
        ContratacionesDocuments documento;
        ArrayList<ContratacionesAmendments> listaDeEnmiendas;
        ContratacionesAmendments enmienda;
        ArrayList<ContratacionesTenderers> listaDeProveedores;
        ContratacionesTenderers supplier;
        ContratacionesIdentifier identificador;
        ArrayList<ContratacionesIdentifier> listaDeIdentificadoresAdicionales;
        ContratacionesIdentifier identificadorAdicional;
        ArrayList<ContratacionesChanges> listaDeCambios;
        ContratacionesChanges cambio;
        ContratacionesUnit unit;
        ContratacionesAmount amount;
        ContratacionesTenderPeriod period;
        ContratacionesAddress address;
        ContratacionesContactPoint contactPoint;
        ContratacionesAmendments enmiendaPrincipal;
        
        String [] listaDeIdentificadoresARegistrarNivelUno;
        String [] listaDeIdentificadoresARegistrarNivelDos;
        String [] listaDeIdentificadoresARegistrarNivelTres;
        
        FormateadorDeFechas formateadorDeFechas;
        
        formateadorDeFechas = new FormateadorDeFechas();
        listaDeAdjudicaciones = new ArrayList<>();
        
        
        if(request.getParameter("lista-identificadores-por-registrar-award") != null)
        { 

            listaDeIdentificadoresARegistrarNivelUno = request.getParameter("lista-identificadores-por-registrar-award").split(",");

            for (String identificadorAdjudicacion : listaDeIdentificadoresARegistrarNivelUno)
            { 

                identificadorAdjudicacion = identificadorAdjudicacion.trim();

                if(identificadorAdjudicacion.equals("") == false)
                { 
                    
                    if(request.getParameter("txtAWARD_ID_" + identificadorAdjudicacion) != null)
                    { 
                        if(request.getParameter("txtAWARD_ID_" + identificadorAdjudicacion).equals("") == false)
                        { 
                            adjudicacion = new ContratacionesAwards();
                            
                            adjudicacion.setId(request.getParameter("txtAWARD_ID_" + identificadorAdjudicacion));
                            
                            
                            if(request.getParameter("txtAWARD_TITLE_" + identificadorAdjudicacion) != null && request.getParameter("txtAWARD_TITLE_" + identificadorAdjudicacion).trim().equals("") == false)
                            { 
                                adjudicacion.setTitle(request.getParameter("txtAWARD_TITLE_" + identificadorAdjudicacion));
                            } 
                            else
                            { 
                                adjudicacion.setTitle(null);
                            } 
                            
                            
                            if(request.getParameter("txtAWARD_DESCRIPTION_" + identificadorAdjudicacion) != null && request.getParameter("txtAWARD_DESCRIPTION_" + identificadorAdjudicacion).trim().equals("") == false)
                            { 
                                adjudicacion.setDescription(request.getParameter("txtAWARD_DESCRIPTION_" + identificadorAdjudicacion));
                            } 
                            else
                            { 
                                adjudicacion.setDescription(null);
                            } 
                            
                            
                            if(request.getParameter("cmbAWARD_STATUS_" + identificadorAdjudicacion) != null && request.getParameter("cmbAWARD_STATUS_" + identificadorAdjudicacion).trim().equals("") == false)
                            { 
                                adjudicacion.setStatus(request.getParameter("cmbAWARD_STATUS_" + identificadorAdjudicacion));
                            } 
                            else
                            { 
                                adjudicacion.setStatus(null);
                            } 
                            
                            
                            if(request.getParameter("txtAWARD_DATE_" + identificadorAdjudicacion) != null && request.getParameter("txtAWARD_DATE_" + identificadorAdjudicacion).trim().equals("") == false)
                            { 
                                adjudicacion.setDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtAWARD_DATE_" + identificadorAdjudicacion)));
                            } 
                            else
                            { 
                                adjudicacion.setDate(null);
                            } 
                            
                            
                            amount = new ContratacionesAmount();
                            
                            if(request.getParameter("txtAWARD_AMOUNT_" + identificadorAdjudicacion) != null && request.getParameter("txtAWARD_AMOUNT_" + identificadorAdjudicacion).trim().equals("") == false)
                            { 
                                amount.setAmount(Float.parseFloat(request.getParameter("txtAWARD_AMOUNT_" + identificadorAdjudicacion)));
                            } 
                            else
                            { 
                                amount.setAmount(null);
                            } 
                            
                            
                            if(request.getParameter("cmbAWARD_CURRENCY_" + identificadorAdjudicacion) != null && request.getParameter("cmbAWARD_CURRENCY_" + identificadorAdjudicacion).trim().equals("") == false)
                            { 
                                amount.setCurrency(request.getParameter("cmbAWARD_CURRENCY_" + identificadorAdjudicacion));
                            } 
                            else
                            { 
                                amount.setCurrency(null);
                            } 
                            
                            adjudicacion.setValue(amount);
                            
                            
                            period = new ContratacionesTenderPeriod();
                            
                            if(request.getParameter("txtAWARD_PERIOD_START_DATE_" + identificadorAdjudicacion) != null && request.getParameter("txtAWARD_PERIOD_START_DATE_" + identificadorAdjudicacion).trim().equals("") == false)
                            { 
                                period.setStartDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtAWARD_PERIOD_START_DATE_" + identificadorAdjudicacion)));
                            } 
                            else
                            { 
                                period.setStartDate(null);
                            } 
                            
                            
                            if(request.getParameter("txtAWARD_PERIOD_END_DATE_" + identificadorAdjudicacion) != null && request.getParameter("txtAWARD_PERIOD_END_DATE_" + identificadorAdjudicacion).trim().equals("") == false)
                            { 
                                period.setEndDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtAWARD_PERIOD_END_DATE_" + identificadorAdjudicacion)));
                            } 
                            else
                            { 
                                period.setEndDate(null);
                            } 
                            
                            
                            if(request.getParameter("txtAWARD_PERIOD_MAX_EXTENT_DATE_" + identificadorAdjudicacion) != null && request.getParameter("txtAWARD_PERIOD_MAX_EXTENT_DATE_" + identificadorAdjudicacion).trim().equals("") == false)
                            { 
                                period.setMaxExtentDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtAWARD_PERIOD_MAX_EXTENT_DATE_" + identificadorAdjudicacion)));
                            } 
                            else
                            { 
                                period.setMaxExtentDate(null);
                            } 
                            
                            
                            if(request.getParameter("txtAWARD_PERIOD_DURATION_IN_DATE_" + identificadorAdjudicacion) != null && request.getParameter("txtAWARD_PERIOD_DURATION_IN_DATE_" + identificadorAdjudicacion).trim().equals("") == false)
                            { 
                                period.setDurationInDays(Integer.parseInt(request.getParameter("txtAWARD_PERIOD_DURATION_IN_DATE_" + identificadorAdjudicacion)));
                            } 
                            else
                            { 
                                period.setDurationInDays(null);
                            } 
                            
                            adjudicacion.setContractPeriod(period);
                            
                            
                            listaDeProveedores = new ArrayList<>();
                            if(request.getParameter("lista-identificadores-por-registrar-supplier_" + identificadorAdjudicacion) != null)
                            { 

                                listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-supplier_" + identificadorAdjudicacion).split(",");

                                for (String identificadorProveedor : listaDeIdentificadoresARegistrarNivelDos)
                                { 

                                    identificadorProveedor = identificadorProveedor.trim();

                                    if(identificadorProveedor.equals("") == false)
                                    { 
                                        
                                        if(request.getParameter("txtSUPPLIER_ID_" + identificadorAdjudicacion + "_" + identificadorProveedor) != null)
                                        { 
                                            if(request.getParameter("txtSUPPLIER_ID_" + identificadorAdjudicacion + "_" + identificadorProveedor).equals("") == false)
                                            { 
                                                supplier = new ContratacionesTenderers();
                                                
                                                supplier.setId(request.getParameter("txtSUPPLIER_ID_" + identificadorAdjudicacion + "_" + identificadorProveedor));
                                                
                                                
                                                if(request.getParameter("txtSUPPLIER_NAME_" + identificadorAdjudicacion + "_" + identificadorProveedor) != null && request.getParameter("txtSUPPLIER_NAME_" + identificadorAdjudicacion + "_" + identificadorProveedor).trim().equals("") == false)
                                                { 
                                                    supplier.setName(request.getParameter("txtSUPPLIER_NAME_" + identificadorAdjudicacion + "_" + identificadorProveedor));
                                                } 
                                                else
                                                { 
                                                    supplier.setName(null);
                                                } 
                                                
                                                
                                                identificador = new ContratacionesIdentifier();
                                                
                                                if(request.getParameter("txtSUPPLIER_IDENTIFIER_ID_" + identificadorAdjudicacion + "_" + identificadorProveedor) != null && request.getParameter("txtSUPPLIER_IDENTIFIER_ID_" + identificadorAdjudicacion + "_" + identificadorProveedor).trim().equals("") == false)
                                                { 
                                                    identificador.setId(request.getParameter("txtSUPPLIER_IDENTIFIER_ID_" + identificadorAdjudicacion + "_" + identificadorProveedor));
                                                } 
                                                else
                                                { 
                                                    identificador.setId(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtSUPPLIER_IDENTIFIER_SCHEME_" + identificadorAdjudicacion + "_" + identificadorProveedor) != null && request.getParameter("txtSUPPLIER_IDENTIFIER_SCHEME_" + identificadorAdjudicacion + "_" + identificadorProveedor).trim().equals("") == false)
                                                { 
                                                    identificador.setScheme(request.getParameter("txtSUPPLIER_IDENTIFIER_SCHEME_" + identificadorAdjudicacion + "_" + identificadorProveedor));
                                                } 
                                                else
                                                { 
                                                    identificador.setScheme(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtSUPPLIER_IDENTIFIER_LEGAL_NAME_" + identificadorAdjudicacion + "_" + identificadorProveedor) != null && request.getParameter("txtSUPPLIER_IDENTIFIER_LEGAL_NAME_" + identificadorAdjudicacion + "_" + identificadorProveedor).trim().equals("") == false)
                                                { 
                                                    identificador.setLegalName(request.getParameter("txtSUPPLIER_IDENTIFIER_LEGAL_NAME_" + identificadorAdjudicacion + "_" + identificadorProveedor));
                                                } 
                                                else
                                                { 
                                                    identificador.setLegalName(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtSUPPLIER_IDENTIFIER_URI_" + identificadorAdjudicacion + "_" + identificadorProveedor) != null && request.getParameter("txtSUPPLIER_IDENTIFIER_URI_" + identificadorAdjudicacion + "_" + identificadorProveedor).trim().equals("") == false)
                                                { 
                                                    identificador.setUri(request.getParameter("txtSUPPLIER_IDENTIFIER_URI_" + identificadorAdjudicacion + "_" + identificadorProveedor));
                                                } 
                                                else
                                                { 
                                                    identificador.setUri(null);
                                                } 
                                                
                                                supplier.setIdentifier(identificador);
                                                
                                                
                                                address = new ContratacionesAddress();
                                                
                                                if(request.getParameter("txtSUPPLIER_ADDRESS_STREET_ADDRESS_" + identificadorAdjudicacion + "_" + identificadorProveedor) != null && request.getParameter("txtSUPPLIER_ADDRESS_STREET_ADDRESS_" + identificadorAdjudicacion + "_" + identificadorProveedor).trim().equals("") == false)
                                                { 
                                                    address.setStreetAddress(request.getParameter("txtSUPPLIER_ADDRESS_STREET_ADDRESS_" + identificadorAdjudicacion + "_" + identificadorProveedor));
                                                } 
                                                else
                                                { 
                                                    address.setStreetAddress(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtSUPPLIER_ADDRESS_LOCALITY_" + identificadorAdjudicacion + "_" + identificadorProveedor) != null && request.getParameter("txtSUPPLIER_ADDRESS_LOCALITY_" + identificadorAdjudicacion + "_" + identificadorProveedor).trim().equals("") == false)
                                                { 
                                                    address.setLocality(request.getParameter("txtSUPPLIER_ADDRESS_LOCALITY_" + identificadorAdjudicacion + "_" + identificadorProveedor));
                                                } 
                                                else
                                                { 
                                                    address.setLocality(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtSUPPLIER_ADDRESS_REGION_" + identificadorAdjudicacion + "_" + identificadorProveedor) != null && request.getParameter("txtSUPPLIER_ADDRESS_REGION_" + identificadorAdjudicacion + "_" + identificadorProveedor).trim().equals("") == false)
                                                { 
                                                    address.setRegion(request.getParameter("txtSUPPLIER_ADDRESS_REGION_" + identificadorAdjudicacion + "_" + identificadorProveedor));
                                                } 
                                                else
                                                { 
                                                    address.setRegion(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtSUPPLIER_ADDRESS_POSTAL_CODE_" + identificadorAdjudicacion + "_" + identificadorProveedor) != null && request.getParameter("txtSUPPLIER_ADDRESS_POSTAL_CODE_" + identificadorAdjudicacion + "_" + identificadorProveedor).trim().equals("") == false)
                                                { 
                                                    address.setPostalCode(request.getParameter("txtSUPPLIER_ADDRESS_POSTAL_CODE_" + identificadorAdjudicacion + "_" + identificadorProveedor));
                                                } 
                                                else
                                                { 
                                                    address.setPostalCode(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtSUPPLIER_ADDRESS_COUNTRY_NAME_" + identificadorAdjudicacion + "_" + identificadorProveedor) != null && request.getParameter("txtSUPPLIER_ADDRESS_COUNTRY_NAME_" + identificadorAdjudicacion + "_" + identificadorProveedor).trim().equals("") == false)
                                                { 
                                                    address.setCountryName(request.getParameter("txtSUPPLIER_ADDRESS_COUNTRY_NAME_" + identificadorAdjudicacion + "_" + identificadorProveedor));
                                                } 
                                                else
                                                { 
                                                    address.setCountryName(null);
                                                } 
                                                
                                                supplier.setAddress(address);
                                                
                                                
                                                contactPoint = new ContratacionesContactPoint();
                                                
                                                if(request.getParameter("txtSUPPLIER_CONTACT_NAME_" + identificadorAdjudicacion + "_" + identificadorProveedor) != null && request.getParameter("txtSUPPLIER_CONTACT_NAME_" + identificadorAdjudicacion + "_" + identificadorProveedor).trim().equals("") == false)
                                                { 
                                                    contactPoint.setName(request.getParameter("txtSUPPLIER_CONTACT_NAME_" + identificadorAdjudicacion + "_" + identificadorProveedor));
                                                } 
                                                else
                                                { 
                                                    contactPoint.setName(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtSUPPLIER_CONTACT_EMAIL_" + identificadorAdjudicacion + "_" + identificadorProveedor) != null && request.getParameter("txtSUPPLIER_CONTACT_EMAIL_" + identificadorAdjudicacion + "_" + identificadorProveedor).trim().equals("") == false)
                                                { 
                                                    contactPoint.setEmail(request.getParameter("txtSUPPLIER_CONTACT_EMAIL_" + identificadorAdjudicacion + "_" + identificadorProveedor));
                                                } 
                                                else
                                                { 
                                                    contactPoint.setEmail(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtSUPPLIER_CONTACT_TELEPHONE_" + identificadorAdjudicacion + "_" + identificadorProveedor) != null && request.getParameter("txtSUPPLIER_CONTACT_TELEPHONE_" + identificadorAdjudicacion + "_" + identificadorProveedor).trim().equals("") == false)
                                                { 
                                                    contactPoint.setTelephone(request.getParameter("txtSUPPLIER_CONTACT_TELEPHONE_" + identificadorAdjudicacion + "_" + identificadorProveedor));
                                                } 
                                                else
                                                { 
                                                    contactPoint.setTelephone(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtSUPPLIER_CONTACT_FAXNUMBER_" + identificadorAdjudicacion + "_" + identificadorProveedor) != null && request.getParameter("txtSUPPLIER_CONTACT_FAXNUMBER_" + identificadorAdjudicacion + "_" + identificadorProveedor).trim().equals("") == false)
                                                { 
                                                    contactPoint.setFaxNumber(request.getParameter("txtSUPPLIER_CONTACT_FAXNUMBER_" + identificadorAdjudicacion + "_" + identificadorProveedor));
                                                } 
                                                else
                                                { 
                                                    contactPoint.setFaxNumber(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtSUPPLIER_CONTACT_URL_" + identificadorAdjudicacion + "_" + identificadorProveedor) != null && request.getParameter("txtSUPPLIER_CONTACT_URL_" + identificadorAdjudicacion + "_" + identificadorProveedor).trim().equals("") == false)
                                                { 
                                                    contactPoint.setUrl(request.getParameter("txtSUPPLIER_CONTACT_URL_" + identificadorAdjudicacion + "_" + identificadorProveedor));
                                                } 
                                                else
                                                { 
                                                    contactPoint.setUrl(null);
                                                } 
                                                
                                                supplier.setContactPoint(contactPoint);
                                                
                                                
                                                listaDeIdentificadoresAdicionales = new ArrayList<>();
                                                if(request.getParameter("lista-identificadores-por-registrar-identifier_" + identificadorAdjudicacion + "_" + identificadorProveedor) != null)
                                                { 

                                                    listaDeIdentificadoresARegistrarNivelTres = request.getParameter("lista-identificadores-por-registrar-identifier_" + identificadorAdjudicacion + "_" + identificadorProveedor).split(",");

                                                    for (String identificadorIdentifierAdicional : listaDeIdentificadoresARegistrarNivelTres)
                                                    { 

                                                        identificadorIdentifierAdicional = identificadorIdentifierAdicional.trim();

                                                        if(identificadorIdentifierAdicional.equals("") == false)
                                                        { 

                                                            if(request.getParameter("txtIDENTIFIER_ID_" + identificadorAdjudicacion + "_" + identificadorProveedor + "_" + identificadorIdentifierAdicional) != null)
                                                            { 
                                                                if(request.getParameter("txtIDENTIFIER_ID_" + identificadorAdjudicacion + "_" + identificadorProveedor + "_" + identificadorIdentifierAdicional).equals("") == false)
                                                                { 
                                                                    identificadorAdicional = new ContratacionesIdentifier();
                                                                    
                                                                    identificadorAdicional.setId(request.getParameter("txtIDENTIFIER_ID_" + identificadorAdjudicacion + "_" + identificadorProveedor + "_" + identificadorIdentifierAdicional));
                                                                    
                                                                    
                                                                    if(request.getParameter("txtIDENTIFIER_SCHEME_" + identificadorAdjudicacion + "_" + identificadorProveedor + "_" + identificadorIdentifierAdicional) != null && request.getParameter("txtIDENTIFIER_SCHEME_" + identificadorAdjudicacion + "_" + identificadorProveedor + "_" + identificadorIdentifierAdicional).trim().equals("") == false)
                                                                    { 
                                                                        identificadorAdicional.setScheme(request.getParameter("txtIDENTIFIER_SCHEME_" + identificadorAdjudicacion + "_" + identificadorProveedor + "_" + identificadorIdentifierAdicional));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        identificadorAdicional.setScheme(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtIDENTIFIER_LEGAL_NAME_" + identificadorAdjudicacion + "_" + identificadorProveedor + "_" + identificadorIdentifierAdicional) != null && request.getParameter("txtIDENTIFIER_LEGAL_NAME_" + identificadorAdjudicacion + "_" + identificadorProveedor + "_" + identificadorIdentifierAdicional).trim().equals("") == false)
                                                                    { 
                                                                        identificadorAdicional.setLegalName(request.getParameter("txtIDENTIFIER_LEGAL_NAME_" + identificadorAdjudicacion + "_" + identificadorProveedor + "_" + identificadorIdentifierAdicional));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        identificadorAdicional.setLegalName(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtIDENTIFIER_URI_" + identificadorAdjudicacion + "_" + identificadorProveedor + "_" + identificadorIdentifierAdicional) != null && request.getParameter("txtIDENTIFIER_URI_" + identificadorAdjudicacion + "_" + identificadorProveedor + "_" + identificadorIdentifierAdicional).trim().equals("") == false)
                                                                    { 
                                                                        identificadorAdicional.setUri(request.getParameter("txtIDENTIFIER_URI_" + identificadorAdjudicacion + "_" + identificadorProveedor + "_" + identificadorIdentifierAdicional));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        identificadorAdicional.setUri(null);
                                                                    } 
                                                                    
                                                                    listaDeIdentificadoresAdicionales.add(identificadorAdicional);
                                                                } 
                                                            } 

                                                        } 

                                                    } 

                                                } 

                                                supplier.setAdditionalIdentifiers(listaDeIdentificadoresAdicionales);
                                                
                                                listaDeProveedores.add(supplier);
                                            } 
                                        } 
                                           
                                    } 

                                } 

                            } 

                            adjudicacion.setSuppliers(listaDeProveedores);
                            
                            
                            listaDeItems = new ArrayList<>();
                            if(request.getParameter("lista-identificadores-por-registrar-item_" + identificadorAdjudicacion) != null)
                            { 

                                listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-item_" + identificadorAdjudicacion).split(",");

                                for (String identificadorItem : listaDeIdentificadoresARegistrarNivelDos)
                                { 

                                    identificadorItem = identificadorItem.trim();

                                    if(identificadorItem.equals("") == false)
                                    { 

                                        if(request.getParameter("txtITEM_ID_" + identificadorAdjudicacion + "_" + identificadorItem) != null)
                                        { 
                                            if(request.getParameter("txtITEM_ID_" + identificadorAdjudicacion + "_" + identificadorItem).equals("") == false)
                                            { 
                                                item = new ContratacionesItems();
                                                
                                                item.setId(request.getParameter("txtITEM_ID_" + identificadorAdjudicacion + "_" + identificadorItem));
                                                
                                                
                                                if(request.getParameter("txtITEM_DESCRIPTION_" + identificadorAdjudicacion + "_" + identificadorItem) != null && request.getParameter("txtITEM_DESCRIPTION_" + identificadorAdjudicacion + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    item.setDescription(request.getParameter("txtITEM_DESCRIPTION_" + identificadorAdjudicacion + "_" + identificadorItem));
                                                } 
                                                else
                                                { 
                                                    item.setDescription(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtITEM_QUANTITY_" + identificadorAdjudicacion + "_" + identificadorItem) != null && request.getParameter("txtITEM_QUANTITY_" + identificadorAdjudicacion + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    item.setQuantity(Integer.parseInt(request.getParameter("txtITEM_QUANTITY_" + identificadorAdjudicacion + "_" + identificadorItem)));
                                                } 
                                                else
                                                { 
                                                    item.setQuantity(null);
                                                } 
                                                
                                                
                                                clasificacion = new ContratacionesClassification();
                                                
                                                if(request.getParameter("txtITEM_CLASSIFICATION_ID_" + identificadorAdjudicacion + "_" + identificadorItem) != null && request.getParameter("txtITEM_CLASSIFICATION_ID_" + identificadorAdjudicacion + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    clasificacion.setId(request.getParameter("txtITEM_CLASSIFICATION_ID_" + identificadorAdjudicacion + "_" + identificadorItem));
                                                } 
                                                else
                                                { 
                                                    clasificacion.setId(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("cmbITEM_CLASSIFICATION_SCHEME_" + identificadorAdjudicacion + "_" + identificadorItem) != null && request.getParameter("cmbITEM_CLASSIFICATION_SCHEME_" + identificadorAdjudicacion + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    clasificacion.setScheme(request.getParameter("cmbITEM_CLASSIFICATION_SCHEME_" + identificadorAdjudicacion + "_" + identificadorItem));
                                                } 
                                                else
                                                { 
                                                    clasificacion.setScheme(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtITEM_CLASSIFICATION_DESCRIPTION_" + identificadorAdjudicacion + "_" + identificadorItem) != null && request.getParameter("txtITEM_CLASSIFICATION_DESCRIPTION_" + identificadorAdjudicacion + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    clasificacion.setDescription(request.getParameter("txtITEM_CLASSIFICATION_DESCRIPTION_" + identificadorAdjudicacion + "_" + identificadorItem));
                                                } 
                                                else
                                                { 
                                                    clasificacion.setDescription(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtITEM_CLASSIFICATION_URI_" + identificadorAdjudicacion + "_" + identificadorItem) != null && request.getParameter("txtITEM_CLASSIFICATION_URI_" + identificadorAdjudicacion + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    clasificacion.setUri(request.getParameter("txtITEM_CLASSIFICATION_URI_" + identificadorAdjudicacion + "_" + identificadorItem));
                                                } 
                                                else
                                                { 
                                                    clasificacion.setUri(null);
                                                } 
                                                
                                                item.setClassification(clasificacion);
                                                
                                                
                                                listaDeClasificacionesAdicionales = new ArrayList<>();
                                                if(request.getParameter("lista-identificadores-por-registrar-classification_" + identificadorAdjudicacion + "_" + identificadorItem) != null)
                                                { 

                                                    listaDeIdentificadoresARegistrarNivelTres = request.getParameter("lista-identificadores-por-registrar-classification_" + identificadorAdjudicacion + "_" + identificadorItem).split(",");

                                                    for (String identificadorClasificacionAdicional : listaDeIdentificadoresARegistrarNivelTres)
                                                    { 

                                                        identificadorClasificacionAdicional = identificadorClasificacionAdicional.trim();

                                                        if(identificadorClasificacionAdicional.equals("") == false)
                                                        { 

                                                            if(request.getParameter("txtCLASSIFICATION_ID_" + identificadorAdjudicacion + "_" + identificadorItem + "_" + identificadorClasificacionAdicional) != null)
                                                            { 
                                                                if(request.getParameter("txtCLASSIFICATION_ID_" + identificadorAdjudicacion + "_" + identificadorItem + "_" + identificadorClasificacionAdicional).equals("") == false)
                                                                { 
                                                                    clasificacionAdicional = new ContratacionesClassification();
                                                                    
                                                                    clasificacionAdicional.setId(request.getParameter("txtCLASSIFICATION_ID_" + identificadorAdjudicacion + "_" + identificadorItem + "_" + identificadorClasificacionAdicional));
                                                                    
                                                                    
                                                                    if(request.getParameter("cmbCLASSIFICATION_SCHEME_" + identificadorAdjudicacion + "_" + identificadorItem + "_" + identificadorClasificacionAdicional) != null && request.getParameter("cmbCLASSIFICATION_SCHEME_" + identificadorAdjudicacion + "_" + identificadorItem + "_" + identificadorClasificacionAdicional).trim().equals("") == false)
                                                                    { 
                                                                        clasificacionAdicional.setScheme(request.getParameter("cmbCLASSIFICATION_SCHEME_" + identificadorAdjudicacion + "_" + identificadorItem + "_" + identificadorClasificacionAdicional));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        clasificacionAdicional.setScheme(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtCLASSIFICATION_DESCRIPTION_" + identificadorAdjudicacion + "_" + identificadorItem + "_" + identificadorClasificacionAdicional) != null && request.getParameter("txtCLASSIFICATION_DESCRIPTION_" + identificadorAdjudicacion + "_" + identificadorItem + "_" + identificadorClasificacionAdicional).trim().equals("") == false)
                                                                    { 
                                                                        clasificacionAdicional.setDescription(request.getParameter("txtCLASSIFICATION_DESCRIPTION_" + identificadorAdjudicacion + "_" + identificadorItem + "_" + identificadorClasificacionAdicional));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        clasificacionAdicional.setDescription(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtCLASSIFICATION_URI_" + identificadorAdjudicacion + "_" + identificadorItem + "_" + identificadorClasificacionAdicional) != null && request.getParameter("txtCLASSIFICATION_URI_" + identificadorAdjudicacion + "_" + identificadorItem + "_" + identificadorClasificacionAdicional).trim().equals("") == false)
                                                                    { 
                                                                        clasificacionAdicional.setUri(request.getParameter("txtCLASSIFICATION_URI_" + identificadorAdjudicacion + "_" + identificadorItem + "_" + identificadorClasificacionAdicional));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        clasificacionAdicional.setUri(null);
                                                                    } 
                                                                    
                                                                    listaDeClasificacionesAdicionales.add(clasificacionAdicional);
                                                                } 
                                                            } 

                                                        } 

                                                    } 

                                                } 

                                                item.setAdditionalClassifications(listaDeClasificacionesAdicionales);
                                                
                                                
                                                unit = new ContratacionesUnit();
                                                
                                                if(request.getParameter("txtITEM_UNIT_ID_" + identificadorAdjudicacion + "_" + identificadorItem) != null && request.getParameter("txtITEM_UNIT_ID_" + identificadorAdjudicacion + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    unit.setId(request.getParameter("txtITEM_UNIT_ID_" + identificadorAdjudicacion + "_" + identificadorItem));
                                                } 
                                                else
                                                { 
                                                    unit.setId(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtITEM_UNIT_NAME_" + identificadorAdjudicacion + "_" + identificadorItem) != null && request.getParameter("txtITEM_UNIT_NAME_" + identificadorAdjudicacion + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    unit.setName(request.getParameter("txtITEM_UNIT_NAME_" + identificadorAdjudicacion + "_" + identificadorItem));
                                                } 
                                                else
                                                { 
                                                    unit.setName(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("cmbITEM_UNIT_SCHEME_" + identificadorAdjudicacion + "_" + identificadorItem) != null && request.getParameter("cmbITEM_UNIT_SCHEME_" + identificadorAdjudicacion + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    unit.setScheme(request.getParameter("cmbITEM_UNIT_SCHEME_" + identificadorAdjudicacion + "_" + identificadorItem));
                                                } 
                                                else
                                                { 
                                                    unit.setScheme(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtITEM_UNIT_URI_" + identificadorAdjudicacion + "_" + identificadorItem) != null && request.getParameter("txtITEM_UNIT_URI_" + identificadorAdjudicacion + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    unit.setUri(request.getParameter("txtITEM_UNIT_URI_" + identificadorAdjudicacion + "_" + identificadorItem));
                                                } 
                                                else
                                                { 
                                                    unit.setUri(null);
                                                } 
                                                
                                                
                                                amount = new ContratacionesAmount();
                                                
                                                if(request.getParameter("txtITEM_UNIT_AMOUNT_" + identificadorAdjudicacion + "_" + identificadorItem) != null && request.getParameter("txtITEM_UNIT_AMOUNT_" + identificadorAdjudicacion + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    amount.setAmount(Float.parseFloat(request.getParameter("txtITEM_UNIT_AMOUNT_" + identificadorAdjudicacion + "_" + identificadorItem)));
                                                } 
                                                else
                                                { 
                                                    amount.setAmount(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("cmbITEM_UNIT_CURRENCY_" + identificadorAdjudicacion + "_" + identificadorItem) != null && request.getParameter("cmbITEM_UNIT_CURRENCY_" + identificadorAdjudicacion + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    amount.setCurrency(request.getParameter("cmbITEM_UNIT_CURRENCY_" + identificadorAdjudicacion + "_" + identificadorItem));
                                                } 
                                                else
                                                { 
                                                    amount.setCurrency(null);
                                                } 
                                                
                                                unit.setValue(amount);
                                                
                                                item.setUnit(unit);
                                                
                                                listaDeItems.add(item); 
                                            } 
                                        } 

                                    } 

                                } 

                            } 

                            adjudicacion.setItems(listaDeItems);
                            
                            
                            listaDeDocumentos = new ArrayList<>();
                            if(request.getParameter("lista-identificadores-por-registrar-document_" + identificadorAdjudicacion) != null)
                            { 

                                listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-document_" + identificadorAdjudicacion).split(",");

                                for (String identificadorDocumento : listaDeIdentificadoresARegistrarNivelDos)
                                { 

                                    identificadorDocumento = identificadorDocumento.trim();

                                    if(identificadorDocumento.equals("") == false)
                                    { 

                                        if(request.getParameter("txtDOCUMENT_ID_" + identificadorAdjudicacion + "_" + identificadorDocumento) != null)
                                        { 
                                            if(request.getParameter("txtDOCUMENT_ID_" + identificadorAdjudicacion + "_" + identificadorDocumento).equals("") == false)
                                            { 
                                                documento = new ContratacionesDocuments();
                                                
                                                documento.setId(request.getParameter("txtDOCUMENT_ID_" + identificadorAdjudicacion + "_" + identificadorDocumento));
                                                
                                                
                                                if(request.getParameter("cmbDOCUMENT_TYPE_" + identificadorAdjudicacion + "_" + identificadorDocumento) != null && request.getParameter("cmbDOCUMENT_TYPE_" + identificadorAdjudicacion + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setDocumentType(request.getParameter("cmbDOCUMENT_TYPE_" + identificadorAdjudicacion + "_" + identificadorDocumento));
                                                } 
                                                else
                                                { 
                                                    documento.setDocumentType(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtDOCUMENT_TITLE_" + identificadorAdjudicacion + "_" + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_TITLE_" + identificadorAdjudicacion + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setTitle(request.getParameter("txtDOCUMENT_TITLE_" + identificadorAdjudicacion + "_" + identificadorDocumento));
                                                } 
                                                else
                                                { 
                                                    documento.setTitle(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtDOCUMENT_DESCRIPTION_" + identificadorAdjudicacion + "_" + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_DESCRIPTION_" + identificadorAdjudicacion + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setDescription(request.getParameter("txtDOCUMENT_DESCRIPTION_" + identificadorAdjudicacion + "_" + identificadorDocumento));
                                                } 
                                                else
                                                { 
                                                    documento.setDescription(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtDOCUMENT_URL_" + identificadorAdjudicacion + "_" + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_URL_" + identificadorAdjudicacion + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setUrl(request.getParameter("txtDOCUMENT_URL_" + identificadorAdjudicacion + "_" + identificadorDocumento));
                                                } 
                                                else
                                                { 
                                                    documento.setUrl(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtDOCUMENT_DATE_PUBLISHED_" + identificadorAdjudicacion + "_" + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_DATE_PUBLISHED_" + identificadorAdjudicacion + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setDatePublished(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtDOCUMENT_DATE_PUBLISHED_" + identificadorAdjudicacion + "_" + identificadorDocumento)));
                                                } 
                                                else
                                                { 
                                                    documento.setDatePublished(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtDOCUMENT_DATE_MODIFIED_" + identificadorAdjudicacion + "_" + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_DATE_MODIFIED_" + identificadorAdjudicacion + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setDateModified(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtDOCUMENT_DATE_MODIFIED_" + identificadorAdjudicacion + "_" + identificadorDocumento)));
                                                } 
                                                else
                                                { 
                                                    documento.setDateModified(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtDOCUMENT_FORMAT_" + identificadorAdjudicacion + "_" + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_FORMAT_" + identificadorAdjudicacion + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setFormat(request.getParameter("txtDOCUMENT_FORMAT_" + identificadorAdjudicacion + "_" + identificadorDocumento));
                                                } 
                                                else
                                                { 
                                                    documento.setFormat(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("cmbDOCUMENT_LANGUAGE_" + identificadorAdjudicacion + "_" + identificadorDocumento) != null && request.getParameter("cmbDOCUMENT_LANGUAGE_" + identificadorAdjudicacion + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setLanguage(request.getParameter("cmbDOCUMENT_LANGUAGE_" + identificadorAdjudicacion + "_" + identificadorDocumento));
                                                } 
                                                else
                                                { 
                                                    documento.setLanguage(null);
                                                } 
                                                
                                                listaDeDocumentos.add(documento);
                                            } 
                                        } 

                                    } 

                                } 

                            } 
                            adjudicacion.setDocuments(listaDeDocumentos);
                            
                            
                            listaDeEnmiendas = new ArrayList<>();
                            if(request.getParameter("lista-identificadores-por-registrar-amendment_" + identificadorAdjudicacion) != null)
                            { 

                                listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-amendment_" + identificadorAdjudicacion).split(",");

                                for (String identificadorEnmienda : listaDeIdentificadoresARegistrarNivelDos)
                                { 

                                    identificadorEnmienda = identificadorEnmienda.trim();

                                    if(identificadorEnmienda.equals("") == false)
                                    { 

                                        if(request.getParameter("txtAMENDMENT_ID_" + identificadorAdjudicacion + "_" + identificadorEnmienda) != null)
                                        { 
                                            if(request.getParameter("txtAMENDMENT_ID_" + identificadorAdjudicacion + "_" + identificadorEnmienda).equals("") == false)
                                            { 
                                                enmienda = new ContratacionesAmendments();
                                                
                                                enmienda.setId(request.getParameter("txtAMENDMENT_ID_" + identificadorAdjudicacion + "_" + identificadorEnmienda));
                                                
                                                
                                                if(request.getParameter("txtAMENDMENT_DATE_" + identificadorAdjudicacion + "_" + identificadorEnmienda) != null && request.getParameter("txtAMENDMENT_DATE_" + identificadorAdjudicacion + "_" + identificadorEnmienda).trim().equals("") == false)
                                                { 
                                                    enmienda.setDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtAMENDMENT_DATE_" + identificadorAdjudicacion + "_" + identificadorEnmienda)));
                                                } 
                                                else
                                                { 
                                                    enmienda.setDate(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtAMENDMENT_RATIONALE_" + identificadorAdjudicacion + "_" + identificadorEnmienda) != null && request.getParameter("txtAMENDMENT_RATIONALE_" + identificadorAdjudicacion + "_" + identificadorEnmienda).trim().equals("") == false)
                                                { 
                                                    enmienda.setRationale(request.getParameter("txtAMENDMENT_RATIONALE_" + identificadorAdjudicacion + "_" + identificadorEnmienda));
                                                } 
                                                else
                                                { 
                                                    enmienda.setRationale(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtAMENDMENT_DESCRIPTION_" + identificadorAdjudicacion + "_" + identificadorEnmienda) != null && request.getParameter("txtAMENDMENT_DESCRIPTION_" + identificadorAdjudicacion + "_" + identificadorEnmienda).trim().equals("") == false)
                                                { 
                                                    enmienda.setDescription(request.getParameter("txtAMENDMENT_DESCRIPTION_" + identificadorAdjudicacion + "_" + identificadorEnmienda));
                                                } 
                                                else
                                                { 
                                                    enmienda.setDescription(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtAMENDMENT_AMENDS_RELEASE_ID_" + identificadorAdjudicacion + "_" + identificadorEnmienda) != null && request.getParameter("txtAMENDMENT_AMENDS_RELEASE_ID_" + identificadorAdjudicacion + "_" + identificadorEnmienda).trim().equals("") == false)
                                                { 
                                                    enmienda.setAmendsReleaseID(request.getParameter("txtAMENDMENT_AMENDS_RELEASE_ID_" + identificadorAdjudicacion + "_" + identificadorEnmienda));
                                                } 
                                                else
                                                { 
                                                    enmienda.setAmendsReleaseID(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtAMENDMENT_RELEASE_ID_" + identificadorAdjudicacion + "_" + identificadorEnmienda) != null && request.getParameter("txtAMENDMENT_RELEASE_ID_" + identificadorAdjudicacion + "_" + identificadorEnmienda).trim().equals("") == false)
                                                { 
                                                    enmienda.setReleaseID(request.getParameter("txtAMENDMENT_RELEASE_ID_" + identificadorAdjudicacion + "_" + identificadorEnmienda));
                                                } 
                                                else
                                                { 
                                                    enmienda.setReleaseID(null);
                                                } 
                                                
                                                
                                                listaDeCambios = new ArrayList<>();
                                                if(request.getParameter("lista-identificadores-por-registrar-change_" + identificadorAdjudicacion + "_" + identificadorEnmienda) != null)
                                                { 

                                                    listaDeIdentificadoresARegistrarNivelTres = request.getParameter("lista-identificadores-por-registrar-change_" + identificadorAdjudicacion + "_" + identificadorEnmienda).split(",");

                                                    for (String identificadorElementoChange : listaDeIdentificadoresARegistrarNivelTres)
                                                    { 

                                                        identificadorElementoChange = identificadorElementoChange.trim();

                                                        if(identificadorElementoChange.equals("") == false)
                                                        { 

                                                            if(request.getParameter("txtCHANGE_PROPERTY_" + identificadorAdjudicacion + "_" + identificadorEnmienda + "_" + identificadorElementoChange) != null)
                                                            { 
                                                                if(request.getParameter("txtCHANGE_PROPERTY_" + identificadorAdjudicacion + "_" + identificadorEnmienda + "_" + identificadorElementoChange).equals("") == false)
                                                                { 
                                                                    cambio = new ContratacionesChanges();
                                                                    
                                                                    cambio.setProperty(request.getParameter("txtCHANGE_PROPERTY_" + identificadorAdjudicacion + "_" + identificadorEnmienda + "_" + identificadorElementoChange));
                                                                    
                                                                    
                                                                    if(request.getParameter("txtCHANGE_FORMATER_VALUE_" + identificadorAdjudicacion + "_" + identificadorEnmienda + "_" + identificadorElementoChange) != null && request.getParameter("txtCHANGE_FORMATER_VALUE_" + identificadorAdjudicacion + "_" + identificadorEnmienda + "_" + identificadorElementoChange).trim().equals("") == false)
                                                                    { 
                                                                        cambio.setFormer_value(request.getParameter("txtCHANGE_FORMATER_VALUE_" + identificadorAdjudicacion + "_" + identificadorEnmienda + "_" + identificadorElementoChange));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        cambio.setFormer_value(null);
                                                                    } 
                                                                    
                                                                    listaDeCambios.add(cambio);
                                                                } 
                                                            } 

                                                        } 

                                                    } 

                                                } 

                                                enmienda.setChanges(listaDeCambios);
                                                
                                                listaDeEnmiendas.add(enmienda); 
                                            } 
                                        } 

                                    } 

                                } 

                            } 

                            adjudicacion.setAmendments(listaDeEnmiendas);
                            
                            
                            enmiendaPrincipal = new ContratacionesAmendments();
                            
                            if(request.getParameter("txtAWARD_AMENDMENT_ID_" + identificadorAdjudicacion) != null && request.getParameter("txtAWARD_AMENDMENT_ID_" + identificadorAdjudicacion).trim().equals("") == false)
                            { 
                                enmiendaPrincipal.setId(request.getParameter("txtAWARD_AMENDMENT_ID_" + identificadorAdjudicacion));
                            } 
                            else
                            { 
                                enmiendaPrincipal.setId(null);
                            } 
                            
                            
                            if(request.getParameter("txtAWARD_AMENDMENT_DATE_" + identificadorAdjudicacion) != null && request.getParameter("txtAWARD_AMENDMENT_DATE_" + identificadorAdjudicacion).trim().equals("") == false)
                            { 
                                enmiendaPrincipal.setDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtAWARD_AMENDMENT_DATE_" + identificadorAdjudicacion)));
                            } 
                            else
                            { 
                                enmiendaPrincipal.setDate(null);
                            } 
                            
                            
                            if(request.getParameter("txtAWARD_AMENDMENT_RATIONALE_" + identificadorAdjudicacion) != null && request.getParameter("txtAWARD_AMENDMENT_RATIONALE_" + identificadorAdjudicacion).trim().equals("") == false)
                            { 
                                enmiendaPrincipal.setRationale(request.getParameter("txtAWARD_AMENDMENT_RATIONALE_" + identificadorAdjudicacion));
                            } 
                            else
                            { 
                                enmiendaPrincipal.setRationale(null);
                            } 
                            
                            
                            if(request.getParameter("txtAWARD_AMENDMENT_DESCRIPTION_" + identificadorAdjudicacion) != null && request.getParameter("txtAWARD_AMENDMENT_DESCRIPTION_" + identificadorAdjudicacion).trim().equals("") == false)
                            { 
                                enmiendaPrincipal.setDescription(request.getParameter("txtAWARD_AMENDMENT_DESCRIPTION_" + identificadorAdjudicacion));
                            } 
                            else
                            { 
                                enmiendaPrincipal.setDescription(null);
                            } 
                            
                            
                            if(request.getParameter("txtAWARD_AMENDMENT_AMENDS_RELEASE_ID_" + identificadorAdjudicacion) != null && request.getParameter("txtAWARD_AMENDMENT_AMENDS_RELEASE_ID_" + identificadorAdjudicacion).trim().equals("") == false)
                            { 
                                enmiendaPrincipal.setAmendsReleaseID(request.getParameter("txtAWARD_AMENDMENT_AMENDS_RELEASE_ID_" + identificadorAdjudicacion));
                            } 
                            else
                            { 
                                enmiendaPrincipal.setAmendsReleaseID(null);
                            } 
                            
                            
                            if(request.getParameter("txtAWARD_AMENDMENT_RELEASE_ID_" + identificadorAdjudicacion) != null && request.getParameter("txtAWARD_AMENDMENT_RELEASE_ID_" + identificadorAdjudicacion).trim().equals("") == false)
                            { 
                                enmiendaPrincipal.setReleaseID(request.getParameter("txtAWARD_AMENDMENT_RELEASE_ID_" + identificadorAdjudicacion));
                            } 
                            else
                            { 
                                enmiendaPrincipal.setReleaseID(null);
                            } 
                            
                            
                            listaDeCambios = new ArrayList<>();
                            if(request.getParameter("lista-identificadores-por-registrar-amendment-principal-change_" + identificadorAdjudicacion) != null)
                            { 

                                listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-amendment-principal-change_" + identificadorAdjudicacion).split(",");

                                for (String identificadorElementoChange : listaDeIdentificadoresARegistrarNivelDos)
                                { 

                                    identificadorElementoChange = identificadorElementoChange.trim();

                                    if(identificadorElementoChange.equals("") == false)
                                    { 

                                        if(request.getParameter("txtAMENDMENT_PRINCIPAL_CHANGE_PROPERTY_" + identificadorAdjudicacion + "_" + identificadorElementoChange) != null)
                                        { 
                                            if(request.getParameter("txtAMENDMENT_PRINCIPAL_CHANGE_PROPERTY_" + identificadorAdjudicacion  + "_" + identificadorElementoChange).equals("") == false)
                                            { 
                                                cambio = new ContratacionesChanges();
                                                
                                                cambio.setProperty(request.getParameter("txtAMENDMENT_PRINCIPAL_CHANGE_PROPERTY_" + identificadorAdjudicacion + "_" + identificadorElementoChange));
                                                
                                                
                                                if(request.getParameter("txtAMENDMENT_PRINCIPAL_CHANGE_FORMATER_VALUE_" + identificadorAdjudicacion + "_" + identificadorElementoChange) != null && request.getParameter("txtAMENDMENT_PRINCIPAL_CHANGE_FORMATER_VALUE_" + identificadorAdjudicacion + "_" + identificadorElementoChange).trim().equals("") == false)
                                                { 
                                                    cambio.setFormer_value(request.getParameter("txtAMENDMENT_PRINCIPAL_CHANGE_FORMATER_VALUE_" + identificadorAdjudicacion + "_" + identificadorElementoChange));
                                                } 
                                                else
                                                { 
                                                    cambio.setFormer_value(null);
                                                } 
                                                
                                                listaDeCambios.add(cambio);
                                            } 
                                        } 

                                    } 

                                } 

                            } 

                            enmiendaPrincipal.setChanges(listaDeCambios);
                            
                            adjudicacion.setAmendment(enmiendaPrincipal);
                            
                            listaDeAdjudicaciones.add(adjudicacion); 
                        } 
                    } 
                    
                    
                } 

            } 

        } 
        
        
        return listaDeAdjudicaciones;
        
    } 

    /**
     *  Metodo para modelar la informacion capturada en la seccion 'Contratos' de una contratacion
     * 
     * @param request
     * 
     * @return ArrayList
     * @throws java.lang.Exception
     *
     */
    @Override
    public ArrayList<ContratacionesContracts> modelarSeccionContratos(HttpServletRequest request) throws Exception
    { 
        
        ArrayList<ContratacionesContracts> listaDeContratos;
        ContratacionesContracts contrato;
        ArrayList<ContratacionesItems> listaDeItems;
        ContratacionesItems item;
        ContratacionesClassification clasificacion;
        ArrayList<ContratacionesClassification> listaDeClasificacionesAdicionales;
        ContratacionesClassification clasificacionAdicional;
        ArrayList<ContratacionesDocuments> listaDeDocumentos;
        ContratacionesDocuments documento;
        ArrayList<ContratacionesMilestones> listaDeHitos;
        ContratacionesMilestones hito;
        ArrayList<ContratacionesAmendments> listaDeEnmiendas;
        ContratacionesAmendments enmienda;
        ArrayList<ContratacionesTransactions> listaDeTransacciones;
        ContratacionesTransactions transaccion;
        ContratacionesIdentifier identificador;
        ArrayList<ContratacionesIdentifier> listaDeIdentificadoresAdicionales;
        ContratacionesIdentifier identificadorAdicional;
        ArrayList<ContratacionesChanges> listaDeCambios;
        ContratacionesChanges cambio;
        ArrayList<ContratacionesRelatedProcesses> listaDeProcesosRelacionados;
        ContratacionesRelatedProcesses procesoRelacionado;
        ContratacionesImplementation implementation;
        ContratacionesUnit unit;
        ContratacionesAmount amount;
        ContratacionesTenderPeriod period;
        ContratacionesTenderers tender;
        ContratacionesAddress address;
        ContratacionesContactPoint contactPoint;
        ContratacionesAmendments enmiendaPrincipal;
        ArrayList<String> arregloDeStrings;
        
        String [] listaDeIdentificadoresARegistrarNivelUno;
        String [] listaDeIdentificadoresARegistrarNivelDos;
        String [] listaDeIdentificadoresARegistrarNivelTres;
        String [] listaDeValoresInputCheck;
        
        FormateadorDeFechas formateadorDeFechas;
        
        formateadorDeFechas = new FormateadorDeFechas();
        listaDeContratos = new ArrayList<>();
        
        
        if(request.getParameter("lista-identificadores-por-registrar-contract") != null)
        { 

            listaDeIdentificadoresARegistrarNivelUno = request.getParameter("lista-identificadores-por-registrar-contract").split(",");

            for (String identificadorContrato : listaDeIdentificadoresARegistrarNivelUno)
            { 

                identificadorContrato = identificadorContrato.trim();

                if(identificadorContrato.equals("") == false)
                { 
                    
                    if(request.getParameter("txtCONTRACT_ID_" + identificadorContrato) != null)
                    { 
                        if(request.getParameter("txtCONTRACT_ID_" + identificadorContrato).equals("") == false)
                        { 
                            contrato = new ContratacionesContracts();
                            
                            contrato.setId(request.getParameter("txtCONTRACT_ID_" + identificadorContrato));
                            
                            
                            if(request.getParameter("txtCONTRACT_AWARD_ID_" + identificadorContrato) != null && request.getParameter("txtCONTRACT_AWARD_ID_" + identificadorContrato).trim().equals("") == false)
                            { 
                                contrato.setAwardID(request.getParameter("txtCONTRACT_AWARD_ID_" + identificadorContrato));
                            } 
                            else
                            { 
                                contrato.setAwardID(null);
                            } 
                            
                            
                            if(request.getParameter("txtCONTRACT_TITLE_" + identificadorContrato) != null && request.getParameter("txtCONTRACT_TITLE_" + identificadorContrato).trim().equals("") == false)
                            { 
                                contrato.setTitle(request.getParameter("txtCONTRACT_TITLE_" + identificadorContrato));
                            } 
                            else
                            { 
                                contrato.setTitle(null);
                            } 
                            
                            
                            if(request.getParameter("txtCONTRACT_DESCRIPTION_" + identificadorContrato) != null && request.getParameter("txtCONTRACT_DESCRIPTION_" + identificadorContrato).trim().equals("") == false)
                            { 
                                contrato.setDescription(request.getParameter("txtCONTRACT_DESCRIPTION_" + identificadorContrato));
                            } 
                            else
                            { 
                                contrato.setDescription(null);
                            } 
                            
                            
                            if(request.getParameter("cmbCONTRACT_STATUS_" + identificadorContrato) != null && request.getParameter("cmbCONTRACT_STATUS_" + identificadorContrato).trim().equals("") == false)
                            { 
                                contrato.setStatus(request.getParameter("cmbCONTRACT_STATUS_" + identificadorContrato));
                            } 
                            else
                            { 
                                contrato.setStatus(null);
                            } 
                            
                            
                            if(request.getParameter("txtCONTRACT_DATE_SIGNED_" + identificadorContrato) != null && request.getParameter("txtCONTRACT_DATE_SIGNED_" + identificadorContrato).trim().equals("") == false)
                            { 
                                contrato.setDateSigned(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtCONTRACT_DATE_SIGNED_" + identificadorContrato)));
                            } 
                            else
                            { 
                                contrato.setDateSigned(null);
                            } 
                            
                            
                            period = new ContratacionesTenderPeriod();
                            
                            if(request.getParameter("txtCONTRACT_PERIOD_START_DATE_" + identificadorContrato) != null && request.getParameter("txtCONTRACT_PERIOD_START_DATE_" + identificadorContrato).trim().equals("") == false)
                            { 
                                period.setStartDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtCONTRACT_PERIOD_START_DATE_" + identificadorContrato)));
                            } 
                            else
                            { 
                                period.setStartDate(null);
                            } 
                            
                            
                            if(request.getParameter("txtCONTRACT_PERIOD_END_DATE_" + identificadorContrato) != null && request.getParameter("txtCONTRACT_PERIOD_END_DATE_" + identificadorContrato).trim().equals("") == false)
                            { 
                                period.setEndDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtCONTRACT_PERIOD_END_DATE_" + identificadorContrato)));
                            } 
                            else
                            { 
                                period.setEndDate(null);
                            } 
                            
                            
                            if(request.getParameter("txtCONTRACT_PERIOD_MAX_EXTENT_DATE_" + identificadorContrato) != null && request.getParameter("txtCONTRACT_PERIOD_MAX_EXTENT_DATE_" + identificadorContrato).trim().equals("") == false)
                            { 
                                period.setMaxExtentDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtCONTRACT_PERIOD_MAX_EXTENT_DATE_" + identificadorContrato)));
                            } 
                            else
                            { 
                                period.setMaxExtentDate(null);
                            } 
                            
                            
                            if(request.getParameter("txtCONTRACT_PERIOD_DURATION_IN_DATE_" + identificadorContrato) != null && request.getParameter("txtCONTRACT_PERIOD_DURATION_IN_DATE_" + identificadorContrato).trim().equals("") == false)
                            { 
                                period.setDurationInDays(Integer.parseInt(request.getParameter("txtCONTRACT_PERIOD_DURATION_IN_DATE_" + identificadorContrato)));
                            } 
                            else
                            { 
                                period.setDurationInDays(null);
                            } 
                            
                            contrato.setPeriod(period);
                            
                            
                            amount = new ContratacionesAmount();
                            
                            if(request.getParameter("txtCONTRACT_AMOUNT_" + identificadorContrato) != null && request.getParameter("txtCONTRACT_AMOUNT_" + identificadorContrato).trim().equals("") == false)
                            { 
                                amount.setAmount(Float.parseFloat(request.getParameter("txtCONTRACT_AMOUNT_" + identificadorContrato)));
                            } 
                            else
                            { 
                                amount.setAmount(null);
                            } 
                            
                            
                            if(request.getParameter("cmbCONTRACT_CURRENCY_" + identificadorContrato) != null && request.getParameter("cmbCONTRACT_CURRENCY_" + identificadorContrato).trim().equals("") == false)
                            { 
                                amount.setCurrency(request.getParameter("cmbCONTRACT_CURRENCY_" + identificadorContrato));
                            } 
                            else
                            { 
                                amount.setCurrency(null);
                            } 
                            
                            contrato.setValue(amount);
                            
                            
                            listaDeItems = new ArrayList<>();
                            if(request.getParameter("lista-identificadores-por-registrar-item_" + identificadorContrato) != null)
                            { 

                                listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-item_" + identificadorContrato).split(",");

                                for (String identificadorItem : listaDeIdentificadoresARegistrarNivelDos)
                                { 

                                    identificadorItem = identificadorItem.trim();

                                    if(identificadorItem.equals("") == false)
                                    { 

                                        if(request.getParameter("txtITEM_ID_" + identificadorContrato + "_" + identificadorItem) != null)
                                        { 
                                            if(request.getParameter("txtITEM_ID_" + identificadorContrato + "_" + identificadorItem).equals("") == false)
                                            { 
                                                item = new ContratacionesItems();
                                                
                                                item.setId(request.getParameter("txtITEM_ID_" + identificadorContrato + "_" + identificadorItem));
                                                
                                                
                                                if(request.getParameter("txtITEM_DESCRIPTION_" + identificadorContrato + "_" + identificadorItem) != null && request.getParameter("txtITEM_DESCRIPTION_" + identificadorContrato + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    item.setDescription(request.getParameter("txtITEM_DESCRIPTION_" + identificadorContrato + "_" + identificadorItem));
                                                } 
                                                else
                                                { 
                                                    item.setDescription(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtITEM_QUANTITY_" + identificadorContrato + "_" + identificadorItem) != null && request.getParameter("txtITEM_QUANTITY_" + identificadorContrato + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    item.setQuantity(Integer.parseInt(request.getParameter("txtITEM_QUANTITY_" + identificadorContrato + "_" + identificadorItem)));
                                                } 
                                                else
                                                { 
                                                    item.setQuantity(null);
                                                } 
                                                
                                                
                                                clasificacion = new ContratacionesClassification();
                                                
                                                if(request.getParameter("txtITEM_CLASSIFICATION_ID_" + identificadorContrato + "_" + identificadorItem) != null && request.getParameter("txtITEM_CLASSIFICATION_ID_" + identificadorContrato + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    clasificacion.setId(request.getParameter("txtITEM_CLASSIFICATION_ID_" + identificadorContrato + "_" + identificadorItem));
                                                } 
                                                else
                                                { 
                                                    clasificacion.setId(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("cmbITEM_CLASSIFICATION_SCHEME_" + identificadorContrato + "_" + identificadorItem) != null && request.getParameter("cmbITEM_CLASSIFICATION_SCHEME_" + identificadorContrato + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    clasificacion.setScheme(request.getParameter("cmbITEM_CLASSIFICATION_SCHEME_" + identificadorContrato + "_" + identificadorItem));
                                                } 
                                                else
                                                { 
                                                    clasificacion.setScheme(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtITEM_CLASSIFICATION_DESCRIPTION_" + identificadorContrato + "_" + identificadorItem) != null && request.getParameter("txtITEM_CLASSIFICATION_DESCRIPTION_" + identificadorContrato + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    clasificacion.setDescription(request.getParameter("txtITEM_CLASSIFICATION_DESCRIPTION_" + identificadorContrato + "_" + identificadorItem));
                                                } 
                                                else
                                                { 
                                                    clasificacion.setDescription(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtITEM_CLASSIFICATION_URI_" + identificadorContrato + "_" + identificadorItem) != null && request.getParameter("txtITEM_CLASSIFICATION_URI_" + identificadorContrato + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    clasificacion.setUri(request.getParameter("txtITEM_CLASSIFICATION_URI_" + identificadorContrato + "_" + identificadorItem));
                                                } 
                                                else
                                                { 
                                                    clasificacion.setUri(null);
                                                } 
                                                
                                                item.setClassification(clasificacion);
                                                
                                                
                                                listaDeClasificacionesAdicionales = new ArrayList<>();
                                                if(request.getParameter("lista-identificadores-por-registrar-classification_" + identificadorContrato + "_" + identificadorItem) != null)
                                                { 

                                                    listaDeIdentificadoresARegistrarNivelTres = request.getParameter("lista-identificadores-por-registrar-classification_" + identificadorContrato + "_" + identificadorItem).split(",");

                                                    for (String identificadorClasificacionAdicional : listaDeIdentificadoresARegistrarNivelTres)
                                                    { 

                                                        identificadorClasificacionAdicional = identificadorClasificacionAdicional.trim();

                                                        if(identificadorClasificacionAdicional.equals("") == false)
                                                        { 

                                                            if(request.getParameter("txtCLASSIFICATION_ID_" + identificadorContrato + "_" + identificadorItem + "_" + identificadorClasificacionAdicional) != null)
                                                            { 
                                                                if(request.getParameter("txtCLASSIFICATION_ID_" + identificadorContrato + "_" + identificadorItem + "_" + identificadorClasificacionAdicional).equals("") == false)
                                                                { 
                                                                    clasificacionAdicional = new ContratacionesClassification();
                                                                    
                                                                    clasificacionAdicional.setId(request.getParameter("txtCLASSIFICATION_ID_" + identificadorContrato + "_" + identificadorItem + "_" + identificadorClasificacionAdicional));
                                                                    
                                                                    
                                                                    if(request.getParameter("cmbCLASSIFICATION_SCHEME_" + identificadorContrato + "_" + identificadorItem + "_" + identificadorClasificacionAdicional) != null && request.getParameter("cmbCLASSIFICATION_SCHEME_" + identificadorContrato + "_" + identificadorItem + "_" + identificadorClasificacionAdicional).trim().equals("") == false)
                                                                    { 
                                                                        clasificacionAdicional.setScheme(request.getParameter("cmbCLASSIFICATION_SCHEME_" + identificadorContrato + "_" + identificadorItem + "_" + identificadorClasificacionAdicional));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        clasificacionAdicional.setScheme(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtCLASSIFICATION_DESCRIPTION_" + identificadorContrato + "_" + identificadorItem + "_" + identificadorClasificacionAdicional) != null && request.getParameter("txtCLASSIFICATION_DESCRIPTION_" + identificadorContrato + "_" + identificadorItem + "_" + identificadorClasificacionAdicional).trim().equals("") == false)
                                                                    { 
                                                                        clasificacionAdicional.setDescription(request.getParameter("txtCLASSIFICATION_DESCRIPTION_" + identificadorContrato + "_" + identificadorItem + "_" + identificadorClasificacionAdicional));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        clasificacionAdicional.setDescription(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtCLASSIFICATION_URI_" + identificadorContrato + "_" + identificadorItem + "_" + identificadorClasificacionAdicional) != null && request.getParameter("txtCLASSIFICATION_URI_" + identificadorContrato + "_" + identificadorItem + "_" + identificadorClasificacionAdicional).trim().equals("") == false)
                                                                    { 
                                                                        clasificacionAdicional.setUri(request.getParameter("txtCLASSIFICATION_URI_" + identificadorContrato + "_" + identificadorItem + "_" + identificadorClasificacionAdicional));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        clasificacionAdicional.setUri(null);
                                                                    } 
                                                                    
                                                                    listaDeClasificacionesAdicionales.add(clasificacionAdicional);
                                                                } 
                                                            } 
                                                            
                                                        } 

                                                    } 

                                                } 

                                                item.setAdditionalClassifications(listaDeClasificacionesAdicionales);
                                                
                                                
                                                unit = new ContratacionesUnit();
                                                
                                                if(request.getParameter("txtITEM_UNIT_ID_" + identificadorContrato + "_" + identificadorItem) != null && request.getParameter("txtITEM_UNIT_ID_" + identificadorContrato + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    unit.setId(request.getParameter("txtITEM_UNIT_ID_" + identificadorContrato + "_" + identificadorItem));
                                                } 
                                                else
                                                { 
                                                    unit.setId(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtITEM_UNIT_NAME_" + identificadorContrato + "_" + identificadorItem) != null && request.getParameter("txtITEM_UNIT_NAME_" + identificadorContrato + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    unit.setName(request.getParameter("txtITEM_UNIT_NAME_" + identificadorContrato + "_" + identificadorItem));
                                                } 
                                                else
                                                { 
                                                    unit.setName(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("cmbITEM_UNIT_SCHEME_" + identificadorContrato + "_" + identificadorItem) != null && request.getParameter("cmbITEM_UNIT_SCHEME_" + identificadorContrato + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    unit.setScheme(request.getParameter("cmbITEM_UNIT_SCHEME_" + identificadorContrato + "_" + identificadorItem));
                                                } 
                                                else
                                                { 
                                                    unit.setScheme(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtITEM_UNIT_URI_" + identificadorContrato + "_" + identificadorItem) != null && request.getParameter("txtITEM_UNIT_URI_" + identificadorContrato + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    unit.setUri(request.getParameter("txtITEM_UNIT_URI_" + identificadorContrato + "_" + identificadorItem));
                                                } 
                                                else
                                                { 
                                                    unit.setUri(null);
                                                } 
                                                
                                                
                                                amount = new ContratacionesAmount();
                                                
                                                if(request.getParameter("txtITEM_UNIT_AMOUNT_" + identificadorContrato + "_" + identificadorItem) != null && request.getParameter("txtITEM_UNIT_AMOUNT_" + identificadorContrato + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    amount.setAmount(Float.parseFloat(request.getParameter("txtITEM_UNIT_AMOUNT_" + identificadorContrato + "_" + identificadorItem)));
                                                } 
                                                else
                                                { 
                                                    amount.setAmount(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("cmbITEM_UNIT_CURRENCY_" + identificadorContrato + "_" + identificadorItem) != null && request.getParameter("cmbITEM_UNIT_CURRENCY_" + identificadorContrato + "_" + identificadorItem).trim().equals("") == false)
                                                { 
                                                    amount.setCurrency(request.getParameter("cmbITEM_UNIT_CURRENCY_" + identificadorContrato + "_" + identificadorItem));
                                                } 
                                                else
                                                { 
                                                    amount.setCurrency(null);
                                                } 
                                                
                                                unit.setValue(amount);
                                                
                                                item.setUnit(unit);
                                                
                                                listaDeItems.add(item); 
                                            } 
                                        } 
                                        
                                    } 

                                } 

                            } 

                            contrato.setItems(listaDeItems);
                            
                            
                            listaDeDocumentos = new ArrayList<>();
                            if(request.getParameter("lista-identificadores-por-registrar-document_" + identificadorContrato) != null)
                            { 

                                listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-document_" + identificadorContrato).split(",");

                                for (String identificadorDocumento : listaDeIdentificadoresARegistrarNivelDos)
                                { 

                                    identificadorDocumento = identificadorDocumento.trim();

                                    if(identificadorDocumento.equals("") == false)
                                    { 

                                        if(request.getParameter("txtDOCUMENT_ID_" + identificadorContrato + "_" + identificadorDocumento) != null)
                                        { 
                                            if(request.getParameter("txtDOCUMENT_ID_" + identificadorContrato + "_" + identificadorDocumento).equals("") == false)
                                            { 
                                                documento = new ContratacionesDocuments();
                                                
                                                documento.setId(request.getParameter("txtDOCUMENT_ID_" + identificadorContrato + "_" + identificadorDocumento));
                                                
                                                
                                                if(request.getParameter("cmbDOCUMENT_TYPE_" + identificadorContrato + "_" + identificadorDocumento) != null && request.getParameter("cmbDOCUMENT_TYPE_" + identificadorContrato + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setDocumentType(request.getParameter("cmbDOCUMENT_TYPE_" + identificadorContrato + "_" + identificadorDocumento));
                                                } 
                                                else
                                                { 
                                                    documento.setDocumentType(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtDOCUMENT_TITLE_" + identificadorContrato + "_" + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_TITLE_" + identificadorContrato + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setTitle(request.getParameter("txtDOCUMENT_TITLE_" + identificadorContrato + "_" + identificadorDocumento));
                                                } 
                                                else
                                                { 
                                                    documento.setTitle(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtDOCUMENT_DESCRIPTION_" + identificadorContrato + "_" + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_DESCRIPTION_" + identificadorContrato + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setDescription(request.getParameter("txtDOCUMENT_DESCRIPTION_" + identificadorContrato + "_" + identificadorDocumento));
                                                } 
                                                else
                                                { 
                                                    documento.setDescription(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtDOCUMENT_URL_" + identificadorContrato + "_" + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_URL_" + identificadorContrato + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setUrl(request.getParameter("txtDOCUMENT_URL_" + identificadorContrato + "_" + identificadorDocumento));
                                                } 
                                                else
                                                { 
                                                    documento.setUrl(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtDOCUMENT_DATE_PUBLISHED_" + identificadorContrato + "_" + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_DATE_PUBLISHED_" + identificadorContrato + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setDatePublished(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtDOCUMENT_DATE_PUBLISHED_" + identificadorContrato + "_" + identificadorDocumento)));
                                                } 
                                                else
                                                { 
                                                    documento.setDatePublished(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtDOCUMENT_DATE_MODIFIED_" + identificadorContrato + "_" + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_DATE_MODIFIED_" + identificadorContrato + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setDateModified(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtDOCUMENT_DATE_MODIFIED_" + identificadorContrato + "_" + identificadorDocumento)));
                                                } 
                                                else
                                                { 
                                                    documento.setDateModified(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtDOCUMENT_FORMAT_" + identificadorContrato + "_" + identificadorDocumento) != null && request.getParameter("txtDOCUMENT_FORMAT_" + identificadorContrato + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setFormat(request.getParameter("txtDOCUMENT_FORMAT_" + identificadorContrato + "_" + identificadorDocumento));
                                                } 
                                                else
                                                { 
                                                    documento.setFormat(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("cmbDOCUMENT_LANGUAGE_" + identificadorContrato + "_" + identificadorDocumento) != null && request.getParameter("cmbDOCUMENT_LANGUAGE_" + identificadorContrato + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setLanguage(request.getParameter("cmbDOCUMENT_LANGUAGE_" + identificadorContrato + "_" + identificadorDocumento));
                                                } 
                                                else
                                                { 
                                                    documento.setLanguage(null);
                                                } 
                                                
                                                listaDeDocumentos.add(documento);
                                            } 
                                        } 
                                        
                                    } 

                                } 

                            } 
                            contrato.setDocuments(listaDeDocumentos);
                            
                            
                            implementation = new ContratacionesImplementation();
                            
                            listaDeTransacciones = new ArrayList<>();
                            if(request.getParameter("lista-identificadores-por-registrar-implementation-transaction_" + identificadorContrato) != null)
                            { 

                                listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-implementation-transaction_" + identificadorContrato).split(",");

                                for (String identificadorTransaccion : listaDeIdentificadoresARegistrarNivelDos)
                                { 

                                    identificadorTransaccion = identificadorTransaccion.trim();

                                    if(identificadorTransaccion.equals("") == false)
                                    { 

                                        if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_ID_" + identificadorContrato + "_" + identificadorTransaccion) != null)
                                        { 
                                            if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_ID_" + identificadorContrato + "_" + identificadorTransaccion).equals("") == false)
                                            { 
                                                transaccion = new ContratacionesTransactions();
                                                
                                                transaccion.setId(request.getParameter("txtIMPLEMENTATION_TRANSACTION_ID_" + identificadorContrato + "_" + identificadorTransaccion));
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_SOURCE_" + identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_SOURCE_" + identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    transaccion.setSource(request.getParameter("txtIMPLEMENTATION_TRANSACTION_SOURCE_" + identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    transaccion.setSource(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_DATE_PUBLISHED_" + identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_DATE_PUBLISHED_" + identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    transaccion.setDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtIMPLEMENTATION_TRANSACTION_DATE_PUBLISHED_" + identificadorContrato + "_" + identificadorTransaccion)));
                                                } 
                                                else
                                                { 
                                                    transaccion.setDate(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_URI_" + identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_URI_" + identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    transaccion.setUri(request.getParameter("txtIMPLEMENTATION_TRANSACTION_URI_" + identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    transaccion.setUri(null);
                                                } 
                                                
                                                
                                                amount = new ContratacionesAmount();
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_VALUE_AMOUNT_" + identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_VALUE_AMOUNT_" + identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    amount.setAmount(Float.parseFloat(request.getParameter("txtIMPLEMENTATION_TRANSACTION_VALUE_AMOUNT_" + identificadorContrato + "_" + identificadorTransaccion)));
                                                } 
                                                else
                                                { 
                                                    amount.setAmount(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("cmbIMPLEMENTATION_TRANSACTION_VALUE_CURRENCY_" + identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("cmbIMPLEMENTATION_TRANSACTION_VALUE_CURRENCY_" + identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    amount.setCurrency(request.getParameter("cmbIMPLEMENTATION_TRANSACTION_VALUE_CURRENCY_" + identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    amount.setCurrency(null);
                                                } 
                                                
                                                transaccion.setValue(amount);
                                                
                                                
                                                tender = new ContratacionesTenderers();
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_ID_" + identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_ID_" + identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    tender.setId(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_ID_" + identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    tender.setId(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_NAME_" + identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_NAME_" + identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    tender.setName(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_NAME_" + identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    tender.setName(null);
                                                } 
                                                
                                                
                                                identificador = new ContratacionesIdentifier();
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_ID_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_ID_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    identificador.setId(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_ID_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    identificador.setId(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_SCHEME_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_SCHEME_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    identificador.setScheme(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_SCHEME_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    identificador.setScheme(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_LEGAL_NAME_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_LEGAL_NAME_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    identificador.setLegalName(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_LEGAL_NAME_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    identificador.setLegalName(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_URI_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_URI_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    identificador.setUri(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_URI_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    identificador.setUri(null);
                                                } 
                                                
                                                tender.setIdentifier(identificador);
                                                
                                                
                                                address = new ContratacionesAddress();
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_STREET_ADDRESS_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_STREET_ADDRESS_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    address.setStreetAddress(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_STREET_ADDRESS_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    address.setStreetAddress(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_LOCALITY_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_LOCALITY_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    address.setLocality(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_LOCALITY_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    address.setLocality(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_REGION_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_REGION_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    address.setRegion(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_REGION_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    address.setRegion(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_POSTAL_CODE_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_POSTAL_CODE_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    address.setPostalCode(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_POSTAL_CODE_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    address.setPostalCode(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_COUNTRY_NAME_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_COUNTRY_NAME_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    address.setCountryName(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_COUNTRY_NAME_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    address.setCountryName(null);
                                                } 
                                                
                                                tender.setAddress(address);
                                                
                                                
                                                contactPoint = new ContratacionesContactPoint();
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_NAME_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_NAME_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    contactPoint.setName(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_NAME_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    contactPoint.setName(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_EMAIL_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_EMAIL_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    contactPoint.setEmail(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_EMAIL_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    contactPoint.setEmail(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_TELEPHONE_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_TELEPHONE_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    contactPoint.setTelephone(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_TELEPHONE_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    contactPoint.setTelephone(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_FAXNUMBER_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_FAXNUMBER_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    contactPoint.setFaxNumber(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_FAXNUMBER_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    contactPoint.setFaxNumber(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_URL_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_URL_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    contactPoint.setUrl(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_URL_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    contactPoint.setUrl(null);
                                                } 
                                                
                                                tender.setContactPoint(contactPoint);
                                                
                                                
                                                listaDeIdentificadoresAdicionales = new ArrayList<>();
                                                if(request.getParameter("lista-identificadores-por-registrar-identifier-payer_" + identificadorContrato + "_" + identificadorTransaccion) != null)
                                                { 

                                                    listaDeIdentificadoresARegistrarNivelTres = request.getParameter("lista-identificadores-por-registrar-identifier-payer_" + identificadorContrato + "_" + identificadorTransaccion).split(",");

                                                    for (String identificadorIdentifierAdicional : listaDeIdentificadoresARegistrarNivelTres)
                                                    { 

                                                        identificadorIdentifierAdicional = identificadorIdentifierAdicional.trim();

                                                        if(identificadorIdentifierAdicional.equals("") == false)
                                                        { 

                                                            if(request.getParameter("txtIDENTIFIER_PAYER_ID_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional) != null)
                                                            { 
                                                                if(request.getParameter("txtIDENTIFIER_PAYER_ID_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional).equals("") == false)
                                                                { 
                                                                    identificadorAdicional = new ContratacionesIdentifier();
                                                                    
                                                                    identificadorAdicional.setId(request.getParameter("txtIDENTIFIER_PAYER_ID_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional));
                                                                    
                                                                    
                                                                    if(request.getParameter("txtIDENTIFIER_PAYER_SCHEME_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional) != null && request.getParameter("txtIDENTIFIER_PAYER_SCHEME_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional).trim().equals("") == false)
                                                                    { 
                                                                        identificadorAdicional.setScheme(request.getParameter("txtIDENTIFIER_PAYER_SCHEME_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        identificadorAdicional.setScheme(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtIDENTIFIER_PAYER_LEGAL_NAME_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional) != null && request.getParameter("txtIDENTIFIER_PAYER_LEGAL_NAME_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional).trim().equals("") == false)
                                                                    { 
                                                                        identificadorAdicional.setLegalName(request.getParameter("txtIDENTIFIER_PAYER_LEGAL_NAME_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        identificadorAdicional.setLegalName(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtIDENTIFIER_PAYER_URI_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional) != null && request.getParameter("txtIDENTIFIER_PAYER_URI_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional).trim().equals("") == false)
                                                                    { 
                                                                        identificadorAdicional.setUri(request.getParameter("txtIDENTIFIER_PAYER_URI_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        identificadorAdicional.setUri(null);
                                                                    } 
                                                                    
                                                                    listaDeIdentificadoresAdicionales.add(identificadorAdicional);
                                                                } 
                                                            } 
                                                            
                                                        } 

                                                    } 

                                                } 

                                                tender.setAdditionalIdentifiers(listaDeIdentificadoresAdicionales);
                                                
                                                transaccion.setPayer(tender);
                                                
                                                
                                                tender = new ContratacionesTenderers();
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_ID_" + identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_ID_" + identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    tender.setId(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_ID_" + identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    tender.setId(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_NAME_" + identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_NAME_" + identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    tender.setName(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_NAME_" + identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    tender.setName(null);
                                                } 
                                                
                                                
                                                identificador = new ContratacionesIdentifier();
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_ID_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_ID_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    identificador.setId(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_ID_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    identificador.setId(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_SCHEME_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_SCHEME_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    identificador.setScheme(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_SCHEME_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    identificador.setScheme(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_LEGAL_NAME_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_LEGAL_NAME_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    identificador.setLegalName(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_LEGAL_NAME_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    identificador.setLegalName(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_URI_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_URI_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    identificador.setUri(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_URI_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    identificador.setUri(null);
                                                } 
                                                
                                                tender.setIdentifier(identificador);
                                                
                                                
                                                address = new ContratacionesAddress();
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_STREET_ADDRESS_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_STREET_ADDRESS_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    address.setStreetAddress(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_STREET_ADDRESS_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    address.setStreetAddress(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_LOCALITY_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_LOCALITY_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    address.setLocality(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_LOCALITY_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    address.setLocality(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_REGION_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_REGION_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    address.setRegion(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_REGION_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    address.setRegion(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_POSTAL_CODE_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_POSTAL_CODE_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    address.setPostalCode(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_POSTAL_CODE_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    address.setPostalCode(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_COUNTRY_NAME_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_COUNTRY_NAME_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    address.setCountryName(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_COUNTRY_NAME_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    address.setCountryName(null);
                                                } 
                                                
                                                tender.setAddress(address);
                                                
                                                
                                                contactPoint = new ContratacionesContactPoint();
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_NAME_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_NAME_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    contactPoint.setName(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_NAME_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    contactPoint.setName(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_EMAIL_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_EMAIL_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    contactPoint.setEmail(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_EMAIL_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    contactPoint.setEmail(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_TELEPHONE_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_TELEPHONE_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    contactPoint.setTelephone(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_TELEPHONE_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    contactPoint.setTelephone(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_FAXNUMBER_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_FAXNUMBER_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    contactPoint.setFaxNumber(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_FAXNUMBER_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    contactPoint.setFaxNumber(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_URL_"+ identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_URL_"+ identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    contactPoint.setUrl(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_URL_"+ identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    contactPoint.setUrl(null);
                                                } 
                                                
                                                tender.setContactPoint(contactPoint);
                                                
                                                
                                                listaDeIdentificadoresAdicionales = new ArrayList<>();
                                                if(request.getParameter("lista-identificadores-por-registrar-identifier-payee_" + identificadorContrato + "_" + identificadorTransaccion) != null)
                                                { 

                                                    listaDeIdentificadoresARegistrarNivelTres = request.getParameter("lista-identificadores-por-registrar-identifier-payee_" + identificadorContrato + "_" + identificadorTransaccion).split(",");

                                                    for (String identificadorIdentifierAdicional : listaDeIdentificadoresARegistrarNivelTres)
                                                    { 

                                                        identificadorIdentifierAdicional = identificadorIdentifierAdicional.trim();

                                                        if(identificadorIdentifierAdicional.equals("") == false)
                                                        { 

                                                            if(request.getParameter("txtIDENTIFIER_PAYEE_ID_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional) != null)
                                                            { 
                                                                if(request.getParameter("txtIDENTIFIER_PAYEE_ID_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional).equals("") == false)
                                                                { 
                                                                    identificadorAdicional = new ContratacionesIdentifier();
                                                                    
                                                                    identificadorAdicional.setId(request.getParameter("txtIDENTIFIER_PAYEE_ID_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional));
                                                                    
                                                                    
                                                                    if(request.getParameter("txtIDENTIFIER_PAYEE_SCHEME_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional) != null && request.getParameter("txtIDENTIFIER_PAYEE_SCHEME_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional).trim().equals("") == false)
                                                                    { 
                                                                        identificadorAdicional.setScheme(request.getParameter("txtIDENTIFIER_PAYEE_SCHEME_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        identificadorAdicional.setScheme(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtIDENTIFIER_PAYEE_LEGAL_NAME_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional) != null && request.getParameter("txtIDENTIFIER_PAYEE_LEGAL_NAME_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional).trim().equals("") == false)
                                                                    { 
                                                                        identificadorAdicional.setLegalName(request.getParameter("txtIDENTIFIER_PAYEE_LEGAL_NAME_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        identificadorAdicional.setLegalName(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtIDENTIFIER_PAYEE_URI_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional) != null && request.getParameter("txtIDENTIFIER_PAYEE_URI_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional).trim().equals("") == false)
                                                                    { 
                                                                        identificadorAdicional.setUri(request.getParameter("txtIDENTIFIER_PAYEE_URI_" + identificadorContrato + "_" + identificadorTransaccion + "_" + identificadorIdentifierAdicional));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        identificadorAdicional.setUri(null);
                                                                    } 
                                                                    
                                                                    listaDeIdentificadoresAdicionales.add(identificadorAdicional);
                                                                } 
                                                            } 

                                                        } 

                                                    } 

                                                } 

                                                tender.setAdditionalIdentifiers(listaDeIdentificadoresAdicionales);
                                                
                                                transaccion.setPayee(tender);
                                                
                                                
                                                amount = new ContratacionesAmount();
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_AMOUNT_AMOUNT_" + identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_AMOUNT_AMOUNT_" + identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    amount.setAmount(Float.parseFloat(request.getParameter("txtIMPLEMENTATION_TRANSACTION_AMOUNT_AMOUNT_" + identificadorContrato + "_" + identificadorTransaccion)));
                                                } 
                                                else
                                                { 
                                                    amount.setAmount(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("cmbIMPLEMENTATION_TRANSACTION_AMOUNT_CURRENCY_" + identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("cmbIMPLEMENTATION_TRANSACTION_AMOUNT_CURRENCY_" + identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    amount.setCurrency(request.getParameter("cmbIMPLEMENTATION_TRANSACTION_AMOUNT_CURRENCY_" + identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    amount.setCurrency(null);
                                                } 
                                                
                                                transaccion.setAmount(amount);
                                                
                                                
                                                identificador = new ContratacionesIdentifier();
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_ID_" + identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_ID_" + identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    identificador.setId(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_ID_" + identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    identificador.setId(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_SCHEME_" + identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_SCHEME_" + identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    identificador.setScheme(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_SCHEME_" + identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    identificador.setScheme(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_LEGAL_NAME_" + identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_LEGAL_NAME_" + identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    identificador.setLegalName(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_LEGAL_NAME_" + identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    identificador.setLegalName(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_URI_" + identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_URI_" + identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    identificador.setUri(request.getParameter("txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_URI_" + identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    identificador.setUri(null);
                                                } 
                                                
                                                transaccion.setProviderOrganization(identificador);
                                                
                                                
                                                identificador = new ContratacionesIdentifier();
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_ID_" + identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_ID_" + identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    identificador.setId(request.getParameter("txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_ID_" + identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    identificador.setId(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_SCHEME_" + identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_SCHEME_" + identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    identificador.setScheme(request.getParameter("txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_SCHEME_" + identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    identificador.setScheme(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_LEGAL_NAME_" + identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_LEGAL_NAME_" + identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    identificador.setLegalName(request.getParameter("txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_LEGAL_NAME_" + identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    identificador.setLegalName(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_URI_" + identificadorContrato + "_" + identificadorTransaccion) != null && request.getParameter("txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_URI_" + identificadorContrato + "_" + identificadorTransaccion).trim().equals("") == false)
                                                { 
                                                    identificador.setUri(request.getParameter("txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_URI_" + identificadorContrato + "_" + identificadorTransaccion));
                                                } 
                                                else
                                                { 
                                                    identificador.setUri(null);
                                                } 
                                                
                                                transaccion.setReceiverOrganization(identificador);
                                                
                                                listaDeTransacciones.add(transaccion);   
                                            } 
                                        } 
                                    } 

                                } 

                            } 
                            
                            implementation.setTransactions(listaDeTransacciones);
                            
                            
                            listaDeHitos = new ArrayList<>();
                            if(request.getParameter("lista-identificadores-por-registrar-implementation-milestone_" + identificadorContrato) != null)
                            { 

                                listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-implementation-milestone_" + identificadorContrato).split(",");

                                for (String identificadorHito : listaDeIdentificadoresARegistrarNivelDos)
                                { 

                                    identificadorHito = identificadorHito.trim();

                                    if(identificadorHito.equals("") == false)
                                    { 

                                        if(request.getParameter("txtIMPLEMENTATION_MILESTONE_ID_" + identificadorContrato + "_" + identificadorHito) != null)
                                        { 
                                            if(request.getParameter("txtIMPLEMENTATION_MILESTONE_ID_" + identificadorContrato + "_" + identificadorHito).equals("") == false)
                                            { 
                                                hito = new ContratacionesMilestones();
                                                
                                                hito.setId(request.getParameter("txtIMPLEMENTATION_MILESTONE_ID_" + identificadorContrato + "_" + identificadorHito));
                                                
                                                
                                                if(request.getParameter("cmbIMPLEMENTATION_MILESTONE_TYPE_" + identificadorContrato + "_" + identificadorHito) != null && request.getParameter("cmbIMPLEMENTATION_MILESTONE_TYPE_" + identificadorContrato + "_" + identificadorHito).trim().equals("") == false)
                                                { 
                                                    hito.setType(request.getParameter("cmbIMPLEMENTATION_MILESTONE_TYPE_" + identificadorContrato + "_" + identificadorHito));
                                                } 
                                                else
                                                { 
                                                    hito.setType(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_MILESTONE_TITLE_" + identificadorContrato + "_" + identificadorHito) != null && request.getParameter("txtIMPLEMENTATION_MILESTONE_TITLE_" + identificadorContrato + "_" + identificadorHito).trim().equals("") == false)
                                                { 
                                                    hito.setTitle(request.getParameter("txtIMPLEMENTATION_MILESTONE_TITLE_" + identificadorContrato + "_" + identificadorHito));
                                                } 
                                                else
                                                { 
                                                    hito.setTitle(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_MILESTONE_DESCRIPTION_" + identificadorContrato + "_" + identificadorHito) != null && request.getParameter("txtIMPLEMENTATION_MILESTONE_DESCRIPTION_" + identificadorContrato + "_" + identificadorHito).trim().equals("") == false)
                                                { 
                                                    hito.setDescription(request.getParameter("txtIMPLEMENTATION_MILESTONE_DESCRIPTION_" + identificadorContrato + "_" + identificadorHito));
                                                } 
                                                else
                                                { 
                                                    hito.setDescription(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_MILESTONE_CODE_" + identificadorContrato + "_" + identificadorHito) != null && request.getParameter("txtIMPLEMENTATION_MILESTONE_CODE_" + identificadorContrato + "_" + identificadorHito).trim().equals("") == false)
                                                { 
                                                    hito.setCode(request.getParameter("txtIMPLEMENTATION_MILESTONE_CODE_" + identificadorContrato + "_" + identificadorHito));
                                                } 
                                                else
                                                { 
                                                    hito.setCode(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_MILESTONE_DUE_DATE_" + identificadorContrato + "_" + identificadorHito) != null && request.getParameter("txtIMPLEMENTATION_MILESTONE_DUE_DATE_" + identificadorContrato + "_" + identificadorHito).trim().equals("") == false)
                                                { 
                                                    hito.setDueDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtIMPLEMENTATION_MILESTONE_DUE_DATE_" + identificadorContrato + "_" + identificadorHito)));
                                                } 
                                                else
                                                { 
                                                    hito.setDueDate(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_MILESTONE_DATE_MET_" + identificadorContrato + "_" + identificadorHito) != null && request.getParameter("txtIMPLEMENTATION_MILESTONE_DATE_MET_" + identificadorContrato + "_" + identificadorHito).trim().equals("") == false)
                                                { 
                                                    hito.setDateMet(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtIMPLEMENTATION_MILESTONE_DATE_MET_" + identificadorContrato + "_" + identificadorHito)));
                                                } 
                                                else
                                                { 
                                                    hito.setDateMet(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_MILESTONE_DATE_MODIFIED_" + identificadorContrato + "_" + identificadorHito) != null && request.getParameter("txtIMPLEMENTATION_MILESTONE_DATE_MODIFIED_" + identificadorContrato + "_" + identificadorHito).trim().equals("") == false)
                                                { 
                                                    hito.setDateModified(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtIMPLEMENTATION_MILESTONE_DATE_MODIFIED_" + identificadorContrato + "_" + identificadorHito)));
                                                } 
                                                else
                                                { 
                                                    hito.setDateModified(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("cmbIMPLEMENTATION_MILESTONE_STATUS_" + identificadorContrato + "_" + identificadorHito) != null && request.getParameter("cmbIMPLEMENTATION_MILESTONE_STATUS_" + identificadorContrato + "_" + identificadorHito).trim().equals("") == false)
                                                { 
                                                    hito.setStatus(request.getParameter("cmbIMPLEMENTATION_MILESTONE_STATUS_" + identificadorContrato + "_" + identificadorHito));
                                                } 
                                                else
                                                { 
                                                    hito.setStatus(null);
                                                } 
                                                
                                                
                                                listaDeDocumentos = new ArrayList<>();
                                                if(request.getParameter("lista-identificadores-por-registrar-implementation-milestone-document_" + identificadorContrato + "_" + identificadorHito) != null)
                                                { 

                                                    listaDeIdentificadoresARegistrarNivelTres = request.getParameter("lista-identificadores-por-registrar-implementation-milestone-document_" + identificadorContrato + "_" + identificadorHito).split(",");

                                                    for (String identificadorDocumento : listaDeIdentificadoresARegistrarNivelTres)
                                                    { 

                                                        identificadorDocumento = identificadorDocumento.trim();

                                                        if(identificadorDocumento.equals("") == false)
                                                        { 

                                                            if(request.getParameter("txtIMPLEMENTATION_MILESTONE_DOCUMENT_ID_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento) != null)
                                                            { 
                                                                if(request.getParameter("txtIMPLEMENTATION_MILESTONE_DOCUMENT_ID_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento).equals("") == false)
                                                                { 
                                                                    documento = new ContratacionesDocuments();
                                                                    
                                                                    documento.setId(request.getParameter("txtIMPLEMENTATION_MILESTONE_DOCUMENT_ID_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento));
                                                                    
                                                                    
                                                                    if(request.getParameter("cmbIMPLEMENTATION_MILESTONE_DOCUMENT_TYPE_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("cmbIMPLEMENTATION_MILESTONE_DOCUMENT_TYPE_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                                    { 
                                                                        documento.setDocumentType(request.getParameter("cmbIMPLEMENTATION_MILESTONE_DOCUMENT_TYPE_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        documento.setDocumentType(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtIMPLEMENTATION_MILESTONE_DOCUMENT_TITLE_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtIMPLEMENTATION_MILESTONE_DOCUMENT_TITLE_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                                    { 
                                                                        documento.setTitle(request.getParameter("txtIMPLEMENTATION_MILESTONE_DOCUMENT_TITLE_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        documento.setTitle(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtIMPLEMENTATION_MILESTONE_DOCUMENT_DESCRIPTION_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtIMPLEMENTATION_MILESTONE_DOCUMENT_DESCRIPTION_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                                    { 
                                                                        documento.setDescription(request.getParameter("txtIMPLEMENTATION_MILESTONE_DOCUMENT_DESCRIPTION_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        documento.setDescription(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtIMPLEMENTATION_MILESTONE_DOCUMENT_URL_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtIMPLEMENTATION_MILESTONE_DOCUMENT_URL_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                                    { 
                                                                        documento.setUrl(request.getParameter("txtIMPLEMENTATION_MILESTONE_DOCUMENT_URL_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        documento.setUrl(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtIMPLEMENTATION_MILESTONE_DOCUMENT_DATE_PUBLISHED_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtIMPLEMENTATION_MILESTONE_DOCUMENT_DATE_PUBLISHED_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                                    { 
                                                                        documento.setDatePublished(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtIMPLEMENTATION_MILESTONE_DOCUMENT_DATE_PUBLISHED_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento)));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        documento.setDatePublished(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtIMPLEMENTATION_MILESTONE_DOCUMENT_DATE_MODIFIED_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtIMPLEMENTATION_MILESTONE_DOCUMENT_DATE_MODIFIED_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                                    { 
                                                                        documento.setDateModified(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtIMPLEMENTATION_MILESTONE_DOCUMENT_DATE_MODIFIED_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento)));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        documento.setDateModified(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtIMPLEMENTATION_MILESTONE_DOCUMENT_FORMAT_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtIMPLEMENTATION_MILESTONE_DOCUMENT_FORMAT_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                                    { 
                                                                        documento.setFormat(request.getParameter("txtIMPLEMENTATION_MILESTONE_DOCUMENT_FORMAT_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        documento.setFormat(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("cmbIMPLEMENTATION_MILESTONE_DOCUMENT_LANGUAGE_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("cmbIMPLEMENTATION_MILESTONE_DOCUMENT_LANGUAGE_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                                    { 
                                                                        documento.setLanguage(request.getParameter("cmbIMPLEMENTATION_MILESTONE_DOCUMENT_LANGUAGE_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        documento.setLanguage(null);
                                                                    } 
                                                                    
                                                                    listaDeDocumentos.add(documento);
                                                                } 
                                                            } 

                                                        } 

                                                    } 

                                                } 

                                                hito.setDocuments(listaDeDocumentos);
                                                
                                                listaDeHitos.add(hito);   
                                            } 
                                        } 
                                    } 

                                } 

                            } 
                            
                            implementation.setMilestones(listaDeHitos);
                            
                            
                            listaDeDocumentos = new ArrayList<>();
                            if(request.getParameter("lista-identificadores-por-registrar-implementation-document_" + identificadorContrato) != null)
                            { 

                                listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-implementation-document_" + identificadorContrato).split(",");

                                for (String identificadorDocumento : listaDeIdentificadoresARegistrarNivelDos)
                                { 

                                    identificadorDocumento = identificadorDocumento.trim();

                                    if(identificadorDocumento.equals("") == false)
                                    { 

                                        if(request.getParameter("txtIMPLEMENTATION_DOCUMENT_ID_" + identificadorContrato + "_" + identificadorDocumento) != null)
                                        { 
                                            if(request.getParameter("txtIMPLEMENTATION_DOCUMENT_ID_" + identificadorContrato + "_" + identificadorDocumento).equals("") == false)
                                            { 
                                                documento = new ContratacionesDocuments();
                                                
                                                documento.setId(request.getParameter("txtIMPLEMENTATION_DOCUMENT_ID_" + identificadorContrato + "_" + identificadorDocumento));
                                                
                                                
                                                if(request.getParameter("cmbIMPLEMENTATION_DOCUMENT_TYPE_" + identificadorContrato + "_" + identificadorDocumento) != null && request.getParameter("cmbIMPLEMENTATION_DOCUMENT_TYPE_" + identificadorContrato + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setDocumentType(request.getParameter("cmbIMPLEMENTATION_DOCUMENT_TYPE_" + identificadorContrato + "_" + identificadorDocumento));
                                                } 
                                                else
                                                { 
                                                    documento.setDocumentType(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_DOCUMENT_TITLE_" + identificadorContrato + "_" + identificadorDocumento) != null && request.getParameter("txtIMPLEMENTATION_DOCUMENT_TITLE_" + identificadorContrato + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setTitle(request.getParameter("txtIMPLEMENTATION_DOCUMENT_TITLE_" + identificadorContrato + "_" + identificadorDocumento));
                                                } 
                                                else
                                                { 
                                                    documento.setTitle(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_DOCUMENT_DESCRIPTION_" + identificadorContrato + "_" + identificadorDocumento) != null && request.getParameter("txtIMPLEMENTATION_DOCUMENT_DESCRIPTION_" + identificadorContrato + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setDescription(request.getParameter("txtIMPLEMENTATION_DOCUMENT_DESCRIPTION_" + identificadorContrato + "_" + identificadorDocumento));
                                                } 
                                                else
                                                { 
                                                    documento.setDescription(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_DOCUMENT_URL_" + identificadorContrato + "_" + identificadorDocumento) != null && request.getParameter("txtIMPLEMENTATION_DOCUMENT_URL_" + identificadorContrato + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setUrl(request.getParameter("txtIMPLEMENTATION_DOCUMENT_URL_" + identificadorContrato + "_" + identificadorDocumento));
                                                } 
                                                else
                                                { 
                                                    documento.setUrl(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_DOCUMENT_DATE_PUBLISHED_" + identificadorContrato + "_" + identificadorDocumento) != null && request.getParameter("txtIMPLEMENTATION_DOCUMENT_DATE_PUBLISHED_" + identificadorContrato + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setDatePublished(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtIMPLEMENTATION_DOCUMENT_DATE_PUBLISHED_" + identificadorContrato + "_" + identificadorDocumento)));
                                                } 
                                                else
                                                { 
                                                    documento.setDatePublished(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_DOCUMENT_DATE_MODIFIED_" + identificadorContrato + "_" + identificadorDocumento) != null && request.getParameter("txtIMPLEMENTATION_DOCUMENT_DATE_MODIFIED_" + identificadorContrato + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setDateModified(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtIMPLEMENTATION_DOCUMENT_DATE_MODIFIED_" + identificadorContrato + "_" + identificadorDocumento)));
                                                } 
                                                else
                                                { 
                                                    documento.setDateModified(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtIMPLEMENTATION_DOCUMENT_FORMAT_" + identificadorContrato + "_" + identificadorDocumento) != null && request.getParameter("txtIMPLEMENTATION_DOCUMENT_FORMAT_" + identificadorContrato + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setFormat(request.getParameter("txtIMPLEMENTATION_DOCUMENT_FORMAT_" + identificadorContrato + "_" + identificadorDocumento));
                                                } 
                                                else
                                                { 
                                                    documento.setFormat(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("cmbIMPLEMENTATION_DOCUMENT_LANGUAGE_" + identificadorContrato + "_" + identificadorDocumento) != null && request.getParameter("cmbIMPLEMENTATION_DOCUMENT_LANGUAGE_" + identificadorContrato + "_" + identificadorDocumento).trim().equals("") == false)
                                                { 
                                                    documento.setLanguage(request.getParameter("cmbIMPLEMENTATION_DOCUMENT_LANGUAGE_" + identificadorContrato + "_" + identificadorDocumento));
                                                } 
                                                else
                                                { 
                                                    documento.setLanguage(null);
                                                } 
                                                
                                                listaDeDocumentos.add(documento);
                                            } 
                                        } 
                                        
                                    } 

                                } 

                            } 
                            
                            implementation.setDocuments(listaDeDocumentos);
                            
                            contrato.setImplementation(implementation);
                            
                            
                            listaDeProcesosRelacionados = new ArrayList<>();
                            if(request.getParameter("lista-identificadores-por-registrar-relatedProcess_" + identificadorContrato) != null)
                            { 

                                listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-relatedProcess_" + identificadorContrato).split(",");

                                for (String identificadorProcesoRelacionado : listaDeIdentificadoresARegistrarNivelDos)
                                { 

                                    identificadorProcesoRelacionado = identificadorProcesoRelacionado.trim();

                                    if(identificadorProcesoRelacionado.equals("") == false)
                                    { 

                                        if(request.getParameter("txtRELATED_PROCESS_ID_" + identificadorContrato + "_" + identificadorProcesoRelacionado) != null)
                                        { 
                                            if(request.getParameter("txtRELATED_PROCESS_ID_" + identificadorContrato + "_" + identificadorProcesoRelacionado).equals("") == false)
                                            { 
                                                procesoRelacionado = new ContratacionesRelatedProcesses();
                                                arregloDeStrings = new ArrayList<>();
                                                
                                                procesoRelacionado.setId(request.getParameter("txtRELATED_PROCESS_ID_" + identificadorContrato + "_" + identificadorProcesoRelacionado));
                                                
                                                
                                                if(request.getParameter("txtRELATED_PROCESS_TITLE_" + identificadorContrato + "_" + identificadorProcesoRelacionado) != null && request.getParameter("txtRELATED_PROCESS_TITLE_" + identificadorContrato + "_" + identificadorProcesoRelacionado).trim().equals("") == false)
                                                { 
                                                    procesoRelacionado.setTitle(request.getParameter("txtRELATED_PROCESS_TITLE_" + identificadorContrato + "_" + identificadorProcesoRelacionado));
                                                } 
                                                else
                                                { 
                                                    procesoRelacionado.setTitle(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("cmbRELATED_PROCESS_SCHEME_" + identificadorContrato + "_" + identificadorProcesoRelacionado) != null && request.getParameter("cmbRELATED_PROCESS_SCHEME_" + identificadorContrato + "_" + identificadorProcesoRelacionado).trim().equals("") == false)
                                                { 
                                                    procesoRelacionado.setScheme(request.getParameter("cmbRELATED_PROCESS_SCHEME_" + identificadorContrato + "_" + identificadorProcesoRelacionado));
                                                } 
                                                else
                                                { 
                                                    procesoRelacionado.setScheme(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtRELATED_PROCESS_IDENTIFIER_" + identificadorContrato + "_" + identificadorProcesoRelacionado) != null && request.getParameter("txtRELATED_PROCESS_IDENTIFIER_" + identificadorContrato + "_" + identificadorProcesoRelacionado).trim().equals("") == false)
                                                { 
                                                    procesoRelacionado.setIdentifier(request.getParameter("txtRELATED_PROCESS_IDENTIFIER_" + identificadorContrato + "_" + identificadorProcesoRelacionado));
                                                } 
                                                else
                                                { 
                                                    procesoRelacionado.setIdentifier(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtRELATED_PROCESS_URI_" + identificadorContrato + "_" + identificadorProcesoRelacionado) != null && request.getParameter("txtRELATED_PROCESS_URI_" + identificadorContrato + "_" + identificadorProcesoRelacionado).trim().equals("") == false)
                                                { 
                                                    procesoRelacionado.setUri(request.getParameter("txtRELATED_PROCESS_URI_" + identificadorContrato + "_" + identificadorProcesoRelacionado));
                                                } 
                                                else
                                                { 
                                                    procesoRelacionado.setUri(null);
                                                } 
                                                
                                                
                                                if (request.getParameterValues("chckRELATED_PROCESS_RELATION_SHIP_" + identificadorContrato + "_" + identificadorProcesoRelacionado) != null)
                                                { 
                                                    listaDeValoresInputCheck = request.getParameter("chckRELATED_PROCESS_RELATION_SHIP_" + identificadorContrato + "_" + identificadorProcesoRelacionado).split(",");
                                                    
                                                    for (String relacion : listaDeValoresInputCheck)
                                                    { 
                                                        if(relacion.trim().equals("") == false)
                                                        { 
                                                            arregloDeStrings.add(relacion.trim());
                                                        } 
                                                    } 
                                                    
                                                } 
                                                procesoRelacionado.setRelationship(arregloDeStrings);
                                                
                                                listaDeProcesosRelacionados.add(procesoRelacionado); 
                                            } 
                                        } 
                                        
                                    } 

                                } 

                            } 

                            contrato.setRelatedProcesses(listaDeProcesosRelacionados);
                            
                            
                            listaDeHitos = new ArrayList<>();
                            if(request.getParameter("lista-identificadores-por-registrar-milestone_" + identificadorContrato) != null)
                            { 

                                listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-milestone_" + identificadorContrato).split(",");

                                for (String identificadorHito : listaDeIdentificadoresARegistrarNivelDos)
                                { 

                                    identificadorHito = identificadorHito.trim();

                                    if(identificadorHito.equals("") == false)
                                    { 

                                        if(request.getParameter("txtMILESTONE_ID_" + identificadorContrato + "_" + identificadorHito) != null)
                                        { 
                                            if(request.getParameter("txtMILESTONE_ID_" + identificadorContrato + "_" + identificadorHito).equals("") == false)
                                            { 
                                                hito = new ContratacionesMilestones();
                                                
                                                hito.setId(request.getParameter("txtMILESTONE_ID_" + identificadorContrato + "_" + identificadorHito));
                                                
                                                
                                                if(request.getParameter("cmbMILESTONE_TYPE_" + identificadorContrato + "_" + identificadorHito) != null && request.getParameter("cmbMILESTONE_TYPE_" + identificadorContrato + "_" + identificadorHito).trim().equals("") == false)
                                                { 
                                                    hito.setType(request.getParameter("cmbMILESTONE_TYPE_" + identificadorContrato + "_" + identificadorHito));
                                                } 
                                                else
                                                { 
                                                    hito.setType(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtMILESTONE_TITLE_" + identificadorContrato + "_" + identificadorHito) != null && request.getParameter("txtMILESTONE_TITLE_" + identificadorContrato + "_" + identificadorHito).trim().equals("") == false)
                                                { 
                                                    hito.setTitle(request.getParameter("txtMILESTONE_TITLE_" + identificadorContrato + "_" + identificadorHito));
                                                } 
                                                else
                                                { 
                                                    hito.setTitle(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtMILESTONE_DESCRIPTION_" + identificadorContrato + "_" + identificadorHito) != null && request.getParameter("txtMILESTONE_DESCRIPTION_" + identificadorContrato + "_" + identificadorHito).trim().equals("") == false)
                                                { 
                                                    hito.setDescription(request.getParameter("txtMILESTONE_DESCRIPTION_" + identificadorContrato + "_" + identificadorHito));
                                                } 
                                                else
                                                { 
                                                    hito.setDescription(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtMILESTONE_CODE_" + identificadorContrato + "_" + identificadorHito) != null && request.getParameter("txtMILESTONE_CODE_" + identificadorContrato + "_" + identificadorHito).trim().equals("") == false)
                                                { 
                                                    hito.setCode(request.getParameter("txtMILESTONE_CODE_" + identificadorContrato + "_" + identificadorHito));
                                                } 
                                                else
                                                { 
                                                    hito.setCode(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtMILESTONE_DUE_DATE_" + identificadorContrato + "_" + identificadorHito) != null && request.getParameter("txtMILESTONE_DUE_DATE_" + identificadorContrato + "_" + identificadorHito).trim().equals("") == false)
                                                { 
                                                    hito.setDueDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtMILESTONE_DUE_DATE_" + identificadorContrato + "_" + identificadorHito)));
                                                } 
                                                else
                                                { 
                                                    hito.setDueDate(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtMILESTONE_DATE_MET_" + identificadorContrato + "_" + identificadorHito) != null && request.getParameter("txtMILESTONE_DATE_MET_" + identificadorContrato + "_" + identificadorHito).trim().equals("") == false)
                                                { 
                                                    hito.setDateMet(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtMILESTONE_DATE_MET_" + identificadorContrato + "_" + identificadorHito)));
                                                } 
                                                else
                                                { 
                                                    hito.setDateMet(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtMILESTONE_DATE_MODIFIED_" + identificadorContrato + "_" + identificadorHito) != null && request.getParameter("txtMILESTONE_DATE_MODIFIED_" + identificadorContrato + "_" + identificadorHito).trim().equals("") == false)
                                                { 
                                                    hito.setDateModified(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtMILESTONE_DATE_MODIFIED_" + identificadorContrato + "_" + identificadorHito)));
                                                } 
                                                else
                                                { 
                                                    hito.setDateModified(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("cmbMILESTONE_STATUS_" + identificadorContrato + "_" + identificadorHito) != null && request.getParameter("cmbMILESTONE_STATUS_" + identificadorContrato + "_" + identificadorHito).trim().equals("") == false)
                                                { 
                                                    hito.setStatus(request.getParameter("cmbMILESTONE_STATUS_" + identificadorContrato + "_" + identificadorHito));
                                                } 
                                                else
                                                { 
                                                    hito.setStatus(null);
                                                } 
                                                
                                                
                                                listaDeDocumentos = new ArrayList<>();
                                                if(request.getParameter("lista-identificadores-por-registrar-milestone-document_" + identificadorContrato + "_" + identificadorHito) != null)
                                                { 

                                                    listaDeIdentificadoresARegistrarNivelTres = request.getParameter("lista-identificadores-por-registrar-milestone-document_" + identificadorContrato + "_" + identificadorHito).split(",");

                                                    for (String identificadorDocumento : listaDeIdentificadoresARegistrarNivelTres)
                                                    { 

                                                        identificadorDocumento = identificadorDocumento.trim();

                                                        if(identificadorDocumento.equals("") == false)
                                                        { 

                                                            if(request.getParameter("txtMILESTONE_DOCUMENT_ID_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento) != null)
                                                            { 
                                                                if(request.getParameter("txtMILESTONE_DOCUMENT_ID_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento).equals("") == false)
                                                                { 
                                                                    documento = new ContratacionesDocuments();
                                                                    
                                                                    if(request.getParameter("txtMILESTONE_DOCUMENT_ID_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtMILESTONE_DOCUMENT_ID_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                                    { 
                                                                        documento.setId(request.getParameter("txtMILESTONE_DOCUMENT_ID_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        documento.setId(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("cmbMILESTONE_DOCUMENT_TYPE_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("cmbMILESTONE_DOCUMENT_TYPE_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                                    { 
                                                                        documento.setDocumentType(request.getParameter("cmbMILESTONE_DOCUMENT_TYPE_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        documento.setDocumentType(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtMILESTONE_DOCUMENT_TITLE_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtMILESTONE_DOCUMENT_TITLE_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                                    { 
                                                                        documento.setTitle(request.getParameter("txtMILESTONE_DOCUMENT_TITLE_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        documento.setTitle(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtMILESTONE_DOCUMENT_DESCRIPTION_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtMILESTONE_DOCUMENT_DESCRIPTION_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                                    { 
                                                                        documento.setDescription(request.getParameter("txtMILESTONE_DOCUMENT_DESCRIPTION_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        documento.setDescription(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtMILESTONE_DOCUMENT_URL_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtMILESTONE_DOCUMENT_URL_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                                    { 
                                                                        documento.setUrl(request.getParameter("txtMILESTONE_DOCUMENT_URL_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        documento.setUrl(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtMILESTONE_DOCUMENT_DATE_PUBLISHED_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtMILESTONE_DOCUMENT_DATE_PUBLISHED_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                                    { 
                                                                        documento.setDatePublished(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtMILESTONE_DOCUMENT_DATE_PUBLISHED_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento)));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        documento.setDatePublished(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtMILESTONE_DOCUMENT_DATE_MODIFIED_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtMILESTONE_DOCUMENT_DATE_MODIFIED_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                                    { 
                                                                        documento.setDateModified(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtMILESTONE_DOCUMENT_DATE_MODIFIED_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento)));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        documento.setDateModified(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("txtMILESTONE_DOCUMENT_FORMAT_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("txtMILESTONE_DOCUMENT_FORMAT_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                                    { 
                                                                        documento.setFormat(request.getParameter("txtMILESTONE_DOCUMENT_FORMAT_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        documento.setFormat(null);
                                                                    } 
                                                                    
                                                                    
                                                                    if(request.getParameter("cmbMILESTONE_DOCUMENT_LANGUAGE_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento) != null && request.getParameter("cmbMILESTONE_DOCUMENT_LANGUAGE_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento).trim().equals("") == false)
                                                                    { 
                                                                        documento.setLanguage(request.getParameter("cmbMILESTONE_DOCUMENT_LANGUAGE_" + identificadorContrato + "_" + identificadorHito + "_" + identificadorDocumento));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        documento.setLanguage(null);
                                                                    } 
                                                                    
                                                                    listaDeDocumentos.add(documento);
                                                                } 
                                                            } 

                                                        } 

                                                    } 

                                                } 

                                                hito.setDocuments(listaDeDocumentos);
                                                
                                                listaDeHitos.add(hito);   
                                            } 
                                        } 
                                    } 

                                } 

                            } 

                            contrato.setMilestones(listaDeHitos);
                            
                            
                            listaDeEnmiendas = new ArrayList<>();
                            if(request.getParameter("lista-identificadores-por-registrar-amendment_" + identificadorContrato) != null)
                            { 

                                listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-amendment_" + identificadorContrato).split(",");

                                for (String identificadorEnmienda : listaDeIdentificadoresARegistrarNivelDos)
                                { 

                                    identificadorEnmienda = identificadorEnmienda.trim();

                                    if(identificadorEnmienda.equals("") == false)
                                    { 

                                        if(request.getParameter("txtAMENDMENT_ID_" + identificadorContrato + "_" + identificadorEnmienda) != null)
                                        { 
                                            if(request.getParameter("txtAMENDMENT_ID_" + identificadorContrato + "_" + identificadorEnmienda).equals("") == false)
                                            { 
                                                enmienda = new ContratacionesAmendments();
                                                
                                                enmienda.setId(request.getParameter("txtAMENDMENT_ID_" + identificadorContrato + "_" + identificadorEnmienda));
                                                
                                                
                                                if(request.getParameter("txtAMENDMENT_DATE_" + identificadorContrato + "_" + identificadorEnmienda) != null && request.getParameter("txtAMENDMENT_DATE_" + identificadorContrato + "_" + identificadorEnmienda).trim().equals("") == false)
                                                { 
                                                    enmienda.setDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtAMENDMENT_DATE_" + identificadorContrato + "_" + identificadorEnmienda)));
                                                } 
                                                else
                                                { 
                                                    enmienda.setDate(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtAMENDMENT_RATIONALE_" + identificadorContrato + "_" + identificadorEnmienda) != null && request.getParameter("txtAMENDMENT_RATIONALE_" + identificadorContrato + "_" + identificadorEnmienda).trim().equals("") == false)
                                                { 
                                                    enmienda.setRationale(request.getParameter("txtAMENDMENT_RATIONALE_" + identificadorContrato + "_" + identificadorEnmienda));
                                                } 
                                                else
                                                { 
                                                    enmienda.setRationale(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtAMENDMENT_DESCRIPTION_" + identificadorContrato + "_" + identificadorEnmienda) != null && request.getParameter("txtAMENDMENT_DESCRIPTION_" + identificadorContrato + "_" + identificadorEnmienda).trim().equals("") == false)
                                                { 
                                                    enmienda.setDescription(request.getParameter("txtAMENDMENT_DESCRIPTION_" + identificadorContrato + "_" + identificadorEnmienda));
                                                } 
                                                else
                                                { 
                                                    enmienda.setDescription(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtAMENDMENT_AMENDS_RELEASE_ID_" + identificadorContrato + "_" + identificadorEnmienda) != null && request.getParameter("txtAMENDMENT_AMENDS_RELEASE_ID_" + identificadorContrato + "_" + identificadorEnmienda).trim().equals("") == false)
                                                { 
                                                    enmienda.setAmendsReleaseID(request.getParameter("txtAMENDMENT_AMENDS_RELEASE_ID_" + identificadorContrato + "_" + identificadorEnmienda));
                                                } 
                                                else
                                                { 
                                                    enmienda.setAmendsReleaseID(null);
                                                } 
                                                
                                                
                                                if(request.getParameter("txtAMENDMENT_RELEASE_ID_" + identificadorContrato + "_" + identificadorEnmienda) != null && request.getParameter("txtAMENDMENT_RELEASE_ID_" + identificadorContrato + "_" + identificadorEnmienda).trim().equals("") == false)
                                                { 
                                                    enmienda.setReleaseID(request.getParameter("txtAMENDMENT_RELEASE_ID_" + identificadorContrato + "_" + identificadorEnmienda));
                                                } 
                                                else
                                                { 
                                                    enmienda.setReleaseID(null);
                                                } 
                                                
                                                
                                                listaDeCambios = new ArrayList<>();
                                                if(request.getParameter("lista-identificadores-por-registrar-change_" + identificadorContrato + "_" + identificadorEnmienda) != null)
                                                { 

                                                    listaDeIdentificadoresARegistrarNivelTres = request.getParameter("lista-identificadores-por-registrar-change_" + identificadorContrato + "_" + identificadorEnmienda).split(",");

                                                    for (String identificadorElementoChange : listaDeIdentificadoresARegistrarNivelTres)
                                                    { 

                                                        identificadorElementoChange = identificadorElementoChange.trim();

                                                        if(identificadorElementoChange.equals("") == false)
                                                        { 

                                                            if(request.getParameter("txtCHANGE_PROPERTY_" + identificadorContrato + "_" + identificadorEnmienda + "_" + identificadorElementoChange) != null)
                                                            { 
                                                                if(request.getParameter("txtCHANGE_PROPERTY_" + identificadorContrato + "_" + identificadorEnmienda + "_" + identificadorElementoChange).equals("") == false)
                                                                { 
                                                                    cambio = new ContratacionesChanges();
                                                                    
                                                                    cambio.setProperty(request.getParameter("txtCHANGE_PROPERTY_" + identificadorContrato + "_" + identificadorEnmienda + "_" + identificadorElementoChange));
                                                                    
                                                                    
                                                                    if(request.getParameter("txtCHANGE_FORMATER_VALUE_" + identificadorContrato + "_" + identificadorEnmienda + "_" + identificadorElementoChange) != null && request.getParameter("txtCHANGE_FORMATER_VALUE_" + identificadorContrato + "_" + identificadorEnmienda + "_" + identificadorElementoChange).trim().equals("") == false)
                                                                    { 
                                                                        cambio.setFormer_value(request.getParameter("txtCHANGE_FORMATER_VALUE_" + identificadorContrato + "_" + identificadorEnmienda + "_" + identificadorElementoChange));
                                                                    } 
                                                                    else
                                                                    { 
                                                                        cambio.setFormer_value(null);
                                                                    } 
                                                                    
                                                                    listaDeCambios.add(cambio);
                                                                } 
                                                            } 

                                                        } 

                                                    } 

                                                } 

                                                enmienda.setChanges(listaDeCambios);
                                                
                                                listaDeEnmiendas.add(enmienda); 
                                            } 
                                        } 
                                        
                                    } 

                                } 

                            } 

                            contrato.setAmendments(listaDeEnmiendas);
                            
                            
                            enmiendaPrincipal = new ContratacionesAmendments();
                            
                            if(request.getParameter("txtCONTRACT_AMENDMENT_ID_" + identificadorContrato) != null && request.getParameter("txtCONTRACT_AMENDMENT_ID_" + identificadorContrato).trim().equals("") == false)
                            { 
                                enmiendaPrincipal.setId(request.getParameter("txtCONTRACT_AMENDMENT_ID_" + identificadorContrato));
                            } 
                            else
                            { 
                                enmiendaPrincipal.setId(null);
                            } 
                            
                            
                            if(request.getParameter("txtCONTRACT_AMENDMENT_DATE_" + identificadorContrato) != null && request.getParameter("txtCONTRACT_AMENDMENT_DATE_" + identificadorContrato).trim().equals("") == false)
                            { 
                                enmiendaPrincipal.setDate(formateadorDeFechas.obtenerFormatoFechaFormatoISO8601DeAnioMesDia(request.getParameter("txtCONTRACT_AMENDMENT_DATE_" + identificadorContrato)));
                            } 
                            else
                            { 
                                enmiendaPrincipal.setDate(null);
                            } 
                            
                            
                            if(request.getParameter("txtCONTRACT_AMENDMENT_RATIONALE_" + identificadorContrato) != null && request.getParameter("txtCONTRACT_AMENDMENT_RATIONALE_" + identificadorContrato).trim().equals("") == false)
                            { 
                                enmiendaPrincipal.setRationale(request.getParameter("txtCONTRACT_AMENDMENT_RATIONALE_" + identificadorContrato));
                            } 
                            else
                            { 
                                enmiendaPrincipal.setRationale(null);
                            } 
                            
                            
                            if(request.getParameter("txtCONTRACT_AMENDMENT_DESCRIPTION_" + identificadorContrato) != null && request.getParameter("txtCONTRACT_AMENDMENT_DESCRIPTION_" + identificadorContrato).trim().equals("") == false)
                            { 
                                enmiendaPrincipal.setDescription(request.getParameter("txtCONTRACT_AMENDMENT_DESCRIPTION_" + identificadorContrato));
                            } 
                            else
                            { 
                                enmiendaPrincipal.setDescription(null);
                            } 
                            
                            
                            if(request.getParameter("txtCONTRACT_AMENDMENT_AMENDS_RELEASE_ID_" + identificadorContrato) != null && request.getParameter("txtCONTRACT_AMENDMENT_AMENDS_RELEASE_ID_" + identificadorContrato).trim().equals("") == false)
                            { 
                                enmiendaPrincipal.setAmendsReleaseID(request.getParameter("txtCONTRACT_AMENDMENT_AMENDS_RELEASE_ID_" + identificadorContrato));
                            } 
                            else
                            { 
                                enmiendaPrincipal.setAmendsReleaseID(null);
                            } 
                            
                            
                            if(request.getParameter("txtCONTRACT_AMENDMENT_RELEASE_ID_" + identificadorContrato) != null && request.getParameter("txtCONTRACT_AMENDMENT_RELEASE_ID_" + identificadorContrato).trim().equals("") == false)
                            { 
                                enmiendaPrincipal.setReleaseID(request.getParameter("txtCONTRACT_AMENDMENT_RELEASE_ID_" + identificadorContrato));
                            } 
                            else
                            { 
                                enmiendaPrincipal.setReleaseID(null);
                            } 
                            
                            
                            listaDeCambios = new ArrayList<>();
                            if(request.getParameter("lista-identificadores-por-registrar-amendment-principal-change_" + identificadorContrato) != null)
                            { 

                                listaDeIdentificadoresARegistrarNivelDos = request.getParameter("lista-identificadores-por-registrar-amendment-principal-change_" + identificadorContrato).split(",");

                                for (String identificadorElementoChange : listaDeIdentificadoresARegistrarNivelDos)
                                { 

                                    identificadorElementoChange = identificadorElementoChange.trim();

                                    if(identificadorElementoChange.equals("") == false)
                                    { 

                                        if(request.getParameter("txtAMENDMENT_PRINCIPAL_CHANGE_PROPERTY_" + identificadorContrato + "_" + identificadorElementoChange) != null)
                                        { 
                                            if(request.getParameter("txtAMENDMENT_PRINCIPAL_CHANGE_PROPERTY_" + identificadorContrato  + "_" + identificadorElementoChange).equals("") == false)
                                            { 
                                                cambio = new ContratacionesChanges();
                                                
                                                cambio.setProperty(request.getParameter("txtAMENDMENT_PRINCIPAL_CHANGE_PROPERTY_" + identificadorContrato + "_" + identificadorElementoChange));
                                                
                                                
                                                if(request.getParameter("txtAMENDMENT_PRINCIPAL_CHANGE_FORMATER_VALUE_" + identificadorContrato + "_" + identificadorElementoChange) != null && request.getParameter("txtAMENDMENT_PRINCIPAL_CHANGE_FORMATER_VALUE_" + identificadorContrato + "_" + identificadorElementoChange).trim().equals("") == false)
                                                { 
                                                    cambio.setFormer_value(request.getParameter("txtAMENDMENT_PRINCIPAL_CHANGE_FORMATER_VALUE_" + identificadorContrato + "_" + identificadorElementoChange));
                                                } 
                                                else
                                                { 
                                                    cambio.setFormer_value(null);
                                                } 
                                                
                                                listaDeCambios.add(cambio);
                                            } 
                                        } 

                                    } 

                                } 

                            } 

                            enmiendaPrincipal.setChanges(listaDeCambios);
                            
                            contrato.setAmendment(enmiendaPrincipal);
                            
                            listaDeContratos.add(contrato); 
                    
                        } 
                    } 
                    
                } 

            } 

        } 
        
        
        return listaDeContratos;
        
    } 
    

    /**
     *  Metodo para obtener la informacion relacionada al estado de las secciones de una contratacion publica
     * 
     * @param id
     * @param intNumeroDeSeccion
     * 
     * @return ContratacionesEstadoSeccion
     * 
     * @throws java.lang.Exception
     *
     */
    @Override
    public ContratacionesEstadoSeccion obtenerEstadoDeSeccion(String id, int intNumeroDeSeccion) throws Exception
    { 
        return DAOContratacionesPublicas.obtenerEstadoDeSeccion(id, intNumeroDeSeccion);
    } 

    /**
     *  Metodo para identificar el numero de la seccion que se debe mostrar al usuario para la captura de su informacion
     * 
     * @param estadoDeSecciones
     * @return int intSeccionConFoco
     *
     */
    @Override
    public int obtenerSeccionConFoco(ArrayList<ContratacionesEstadoSeccion> estadoDeSecciones)
    { 
        
        int intSeccionConFoco = 0;      
        
        for(ContratacionesEstadoSeccion estadoSeccion : estadoDeSecciones)
        { 
            if(estadoSeccion.getNumero_seccion() == 1)
            { 
                if(estadoSeccion.getEstado_seccion().equals("1") == false)
                { 
                    intSeccionConFoco = 1;
                } 
            } 
            if(estadoSeccion.getEstado_seccion().equals("1") == true)
            { 
                intSeccionConFoco = (estadoSeccion.getNumero_seccion() + 1);
            } 
        } 
        
        return intSeccionConFoco;
        
    } 

    @Override
    public int obtenerBloqueConFoco(ArrayList<ContratacionesEstadoBloque> estadoDeBloques)
    { 
        
        int intBloqueConFoco = 0;      
        
        for(ContratacionesEstadoBloque estadoBloque : estadoDeBloques)
        { 
            if(estadoBloque.getNumero_bloque() == 1)
            { 
                if(estadoBloque.getEstado_bloque().equals("1") == false)
                { 
                    intBloqueConFoco = 1;
                } 
            } 
            if(estadoBloque.getEstado_bloque().equals("1") == true)
            { 
                intBloqueConFoco = (estadoBloque.getNumero_bloque() + 1);
            } 
        } 
        
        return intBloqueConFoco;
        
    } 
    
    /**
     * Metodo para registrar o actualizar una contratacion publica
     *
     * @param contratacion Informacion de la contratacion
     * @param usuario Usuario que registra
     * @param nivelAcceso Nivel que tiene el usuario para interactuar en el
     * sistema
     * @param intNumeroDeSeccion
     * @param intNumeroDeBloque
     * @param bolSeRegistra
     *
     * @return boolean TRUE- Se guardo FALSE - No se guardo
     *
     * @throws Exception
     */
    @Override
    public String registrarActualizarContratacion(ContratacionesContratacion contratacion, USUARIO usuario, NIVEL_ACCESO nivelAcceso, int intNumeroDeSeccion, int intNumeroDeBloque, boolean bolSeRegistra) throws Exception
    { 
        
        Document mensaje;                                                       
        METADATA metadata;                                                      
        METADATOS metadatos;                                                    
        String strNombreSeccion = "";                                           
        String descripcionMensaje;
        
        metadata = new METADATA();
        metadatos = new METADATOS();
        mensaje = new Document();
        
        if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
        { 
                
            
            switch(intNumeroDeSeccion)
            { 
                case 1: 
                    strNombreSeccion = "Datos generales";
                    break;
                case 2: 
                    strNombreSeccion = "Publicador";
                    break;
                case 3: 
                    strNombreSeccion = "Etiquetas de entrega";
                    break;
                case 4: 
                    strNombreSeccion = "Partes involucradas";
                    break;
                case 5: 
                    strNombreSeccion = "Comprador";
                    break;
                case 6: 
                    if(intNumeroDeBloque == 1) 
                    {
                        strNombreSeccion = "Informaci\u00F3n general";
                    }
                    if(intNumeroDeBloque == 2) 
                    {
                        strNombreSeccion = "Presupuesto";
                    }
                    if(intNumeroDeBloque == 3) 
                    {
                        strNombreSeccion = "Documentos";
                    }
                    if(intNumeroDeBloque == 4) 
                    {
                        strNombreSeccion = "Hitos de planeaci\u00F3n";
                    }
                    break;
                case 7: 
                    if(intNumeroDeBloque == 1) 
                    {
                        strNombreSeccion = "Informaci\u00F3n general";
                    }
                    if(intNumeroDeBloque == 2) 
                    {
                        strNombreSeccion = "Periodo de licitaci\u00F3n";
                    }
                    if(intNumeroDeBloque == 3) 
                    {
                        strNombreSeccion = "Periodo de consulta";
                    }
                    if(intNumeroDeBloque == 4) 
                    {
                        strNombreSeccion = "Periodo de evaluaci\u00F3n y adjudicaci\u00F3n";
                    }
                    if(intNumeroDeBloque == 5) 
                    {
                        strNombreSeccion = "Periodo de contrato";
                    }
                    if(intNumeroDeBloque == 6) 
                    {
                        strNombreSeccion = "Art\u00EDculos que se adquirir\u00E1n";
                    }
                    if(intNumeroDeBloque == 7) 
                    {
                        strNombreSeccion = "Licitantes";
                    }
                    if(intNumeroDeBloque == 8) 
                    {
                        strNombreSeccion = "Documentos";
                    }
                    if(intNumeroDeBloque == 9) 
                    {
                        strNombreSeccion = "Hitos";
                    }
                    if(intNumeroDeBloque == 10) 
                    {
                        strNombreSeccion = "Enmiendas";
                    }
                    break;
                case 8: 
                    strNombreSeccion = "Adjudicaciones";
                    break;
                case 9: 
                    strNombreSeccion = "Contratos";
                    break;
            } 
            
            
            if(bolSeRegistra == true && intNumeroDeSeccion == 1)
            { 
                
                metadata.setActualizacion(new ManejoDeFechas().getDateNowAsString());
                metadata.setInstitucion(usuario.getDependencia().getValor());
                metadata.setContacto(usuario.getCorreo_electronico());
                metadata.setPersonaContacto(usuario.getNombreCompleto());
                contratacion.setMetadata(metadata);

                contratacion.setDependencia("SAEMM");
                metadatos.setUsuario_registro(usuario.getUsuario());
                metadatos.setFecha_registro(new ManejoDeFechas().getDateNowAsString());
                contratacion.setMetadatos(metadatos);
                contratacion.setPublicar("0");

            } 
            else
            { 
                
                metadatos = contratacion.getMetadatos();
                metadatos.setUsuario_modificacion(usuario.getUsuario());
                metadatos.setFecha_modificacion(new ManejoDeFechas().getDateNowAsString());
                
                contratacion.setMetadatos(metadatos);
                
            } 
            
            if (DAOContratacionesPublicas.registrarActualizarContratacion(contratacion, usuario, nivelAcceso, bolSeRegistra))
            {   
                
                
                if(bolSeRegistra == true)
                { 
                    switch (intNumeroDeSeccion)
                    { 
                        case 6:
                            descripcionMensaje = "Se ha registrado correctamente la informaci\u00F3n del bloque '" + strNombreSeccion + "' perteneciente a la secci\u00F3n 'Planeaci\u00F3n'.";
                            break;
                        case 7:
                            descripcionMensaje = "Se ha registrado correctamente la informaci\u00F3n del bloque '" + strNombreSeccion + "' perteneciente a la secci\u00F3n 'Licitaci\u00F3n'.";
                            break;
                        default:
                            descripcionMensaje = "Se ha registrado correctamente la informaci\u00F3n de la secci\u00F3n '" + strNombreSeccion + "'.";
                            break;
                    } 
                } 
                else
                { 
                    switch (intNumeroDeSeccion)
                    { 
                        case 6:
                            descripcionMensaje = "Se ha modificado correctamente la informaci\u00F3n del bloque '" + strNombreSeccion + "' perteneciente a la secci\u00F3n 'Planeaci\u00F3n'.";
                            break;
                        case 7:
                            descripcionMensaje = "Se ha modificado correctamente la informaci\u00F3n del bloque '" + strNombreSeccion + "' perteneciente a la secci\u00F3n 'Licitaci\u00F3n'.";
                            break;
                        default:
                            descripcionMensaje = "Se ha modificado correctamente la informaci\u00F3n de la secci\u00F3n '" + strNombreSeccion + "'.";
                            break;
                    } 
                } 
                
                
                mensaje.append("tipo", "success");
                mensaje.append("mensaje", descripcionMensaje);

            } 
            else
            { 
                
                
                if(bolSeRegistra == true)
                { 
                    switch (intNumeroDeSeccion)
                    { 
                        case 6:
                            descripcionMensaje = "No fue posible regitrar la informaci\u00F3n del bloque '" + strNombreSeccion + "' perteneciente a la secci\u00F3n 'Planeaci\u00F3n', intente de nuevo.";
                            break;
                        case 7:
                            descripcionMensaje = "No fue posible regitrar la informaci\u00F3n del bloque '" + strNombreSeccion + "' perteneciente a la secci\u00F3n 'Licitaci\u00F3n', intente de nuevo.";
                            break;
                        default:
                            descripcionMensaje = "No fue posible regitrar la informaci\u00F3n de la secci\u00F3n '" + strNombreSeccion + "', intente de nuevo.";
                            break;
                    } 
                } 
                else
                { 
                    switch (intNumeroDeSeccion)
                    { 
                       case 6:
                            descripcionMensaje = "No fue posible modificar la informaci\u00F3n del bloque '" + strNombreSeccion + "' perteneciente a la secci\u00F3n 'Planeaci\u00F3n', intente de nuevo.";
                            break;
                        case 7:
                            descripcionMensaje = "No fue posible modificar la informaci\u00F3n del bloque '" + strNombreSeccion + "' perteneciente a la secci\u00F3n 'Licitaci\u00F3n', intente de nuevo.";
                            break;
                        default:
                            descripcionMensaje = "No fue posible modificar la informaci\u00F3n de la secci\u00F3n '" + strNombreSeccion + "', intente de nuevo.";
                            break;
                    } 
                } 
                
                
                mensaje.append("tipo", "error");
                mensaje.append("mensaje", descripcionMensaje);
                
            } 
            
        } 
        else
        { 
            mensaje.append("tipo", "error");
            mensaje.append("mensaje", "No cuentas con los permisos necesarios para realizar esta acci\u00F3n!");
        } 
        
        return mensaje.toJson();
        
    } 

    @Override
    public String eliminarContratacion(String id, USUARIO usuario, NIVEL_ACCESO nivelAcceso, ContratacionesContratacion contratacion) throws Exception
    {
        Document mensaje;                                                       

        mensaje = new Document();
        if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
        { 
            if (DAOContratacionesPublicas.eliminarContratacion(id, usuario, nivelAcceso, contratacion))
            {
                mensaje.append("tipo", "success");
                mensaje.append("mensaje", "Se ha eliminado correctamente el registro");
            }
            else
            {
                mensaje.append("tipo", "error");
                mensaje.append("mensaje", "No fue posible eliminar el registro!");
            }
        } 
        else
        { 
            mensaje.append("tipo", "error");
            mensaje.append("mensaje", "No cuentas con los permisos necesarios para realizar esta acci\u00F3n!");
        } 

        return mensaje.toJson();
    }

    @Override
    public String publicarContratacion(String id, USUARIO usuario, NIVEL_ACCESO nivelAcceso, ContratacionesContratacion contratacion) throws Exception
    {
        Document mensaje;                                                       

        mensaje = new Document();
        if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
        { 

            contratacion.getMetadatos().setUsuario_publicacion(usuario.getUsuario());
            contratacion.getMetadatos().setFecha_publicacion(new ManejoDeFechas().getDateNowAsString());
            contratacion.setPublicar("1");

            if (DAOContratacionesPublicas.publicarContratacion(id, contratacion, usuario, nivelAcceso))
            {
                mensaje.append("tipo", "success");
                mensaje.append("mensaje", "Se ha publicado correctamente el registro");
            }
            else
            {
                mensaje.append("tipo", "error");
                mensaje.append("mensaje", "No fue posible publicar el registro!");
            }
        } 
        else
        { 
            mensaje.append("tipo", "error");
            mensaje.append("mensaje", "No cuentas con los permisos necesarios para realizar esta acci\u00F3n!");
        } 

        return mensaje.toJson();
    }

    @Override
    public String despublicarContratacion(String id, USUARIO usuario, NIVEL_ACCESO nivelAcceso, ContratacionesContratacion contratacion) throws Exception
    {
        Document mensaje;                                                       

        mensaje = new Document();
        if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
        { 

            contratacion.getMetadatos().setUsuario_despublicacion(usuario.getUsuario());
            contratacion.getMetadatos().setFecha_despublicacion(new ManejoDeFechas().getDateNowAsString());
            contratacion.setPublicar("0");

            if (DAOContratacionesPublicas.despublicarContrataciones(id, contratacion, usuario, nivelAcceso))
            {
                mensaje.append("tipo", "success");
                mensaje.append("mensaje", "Se ha despublicado correctamente el registro");
            }
            else
            {
                mensaje.append("tipo", "error");
                mensaje.append("mensaje", "No fue posible despublicar el registro!");
            }
        } 
        else
        { 
            mensaje.append("tipo", "error");
            mensaje.append("mensaje", "No cuentas con los permisos necesarios para realizar esta acci\u00F3n!");
        } 

        return mensaje.toJson();
    }

}