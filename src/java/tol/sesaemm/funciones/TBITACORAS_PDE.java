package tol.sesaemm.funciones;

import java.io.Serializable;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.types.ObjectId;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */

public class TBITACORAS_PDE implements Serializable
{

    @BsonId
    private ObjectId _id;
    private String fecha;
    private String usuario;
    private String sistema;
    private String dependencia;
    private String idDato;
    private String datos;
    private String accion;

    public TBITACORAS_PDE()
    {
    }

    /**
     * Registro de los eventos
     *
     * @param fecha fecha de captura
     * @param usuario nombre del usuario que realiza la accion
     * @param sistema sistema al que pertenece n los datos
     * @param dependencia siglas de la dependencia
     * @param idDato id del registro enviado por la dependencia
     * @param datos datos json para guardar
     * @param accion accion que se realiza
     */
    public TBITACORAS_PDE(String fecha, String usuario, String sistema, String dependencia, String idDato, String datos, String accion)
    {
        this.fecha = fecha;
        this.usuario = usuario;
        this.sistema = sistema;
        this.dependencia = dependencia;
        this.idDato = idDato;
        this.datos = datos;
        this.accion = accion;
    }

    public ObjectId getId()
    {
        return _id;
    }

    public void setId(ObjectId _id)
    {
        this._id = _id;
    }

    public String getFecha()
    {
        return fecha;
    }

    public void setFecha(String fecha)
    {
        this.fecha = fecha;
    }

    public String getUsuario()
    {
        return usuario;
    }

    public void setUsuario(String usuario)
    {
        this.usuario = usuario;
    }

    public String getSistema()
    {
        return sistema;
    }

    public void setSistema(String sistema)
    {
        this.sistema = sistema;
    }

    public String getDependencia()
    {
        return dependencia;
    }

    public void setDependencia(String dependencia)
    {
        this.dependencia = dependencia;
    }

    public String getIdDato()
    {
        return idDato;
    }

    public void setIdDato(String idDato)
    {
        this.idDato = idDato;
    }

    public String getDatos()
    {
        return datos;
    }

    public void setDatos(String datos)
    {
        this.datos = datos;
    }

    public String getAccion()
    {
        return accion;
    }

    public void setAccion(String accion)
    {
        this.accion = accion;
    }

}