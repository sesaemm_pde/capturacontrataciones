package tol.sesaemm.funciones;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import tol.sesaemm.ing.jidv.config.ConfVariables;
import tol.sesaemm.ing.jidv.integracion.DAOBase;
import tol.sesaemm.ing.jidv.javabeans.TBITACORA_BUSQUEDAS_PDE;

/**
 *
 * @author Ismael Ortiz Pulido
 */
public class BitacoraDeBusquedasPDE
{

    public static String contrataciones = "CO";

    /**
     * Metodo para registrar en base de datos una bitacora de las busquedas
     * realizadas en los sistemas de la PDE
     *
     * @param bitacora
     *
     * @return blnSeRegistro
     *
     * @throws java.lang.Exception
     */
    public static boolean registrarBitacoraDeBusquedasPDE(TBITACORA_BUSQUEDAS_PDE bitacora) throws Exception
    {

        MongoClient conectarBaseDatos = null;                                   // Conexi&oacute;n a la base de datos
        MongoDatabase database;                                                 // Obtener esquema de base de datos
        MongoCollection<TBITACORA_BUSQUEDAS_PDE> collection;                      // Obtener colecci&oacute;n
        boolean blnSeRegistro = false;                                          // bandera de salida
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { // si la conexion se ha establecido(TOP)

                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tbitacora_busquedas_pde", TBITACORA_BUSQUEDAS_PDE.class);
                collection.insertOne(bitacora);

                blnSeRegistro = true;

            } // i la conexion se ha establecido(BOTTOM)
            else
            { // no se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // no se ha establecido la conexion (BOTTOM)
        }
        catch (Exception ex)
        {
            throw new Exception("Error al registrar bitacora de busqueda de la PDE: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return blnSeRegistro;

    }

    /**
     * Metodo de ayuda para cerrar las conexiones activas
     *
     *
     * @param conectarBaseDatos
     * @param cursor
     */
    public static void cerrarRecursos(MongoClient conectarBaseDatos, MongoCursor<Document> cursor)
    { // metodo de ayuda para cerrar las conexiones activas (TOP)

        if (cursor != null)
        { // cerrar iterador de documentos para consulta de base de datos (TOP)
            cursor.close();
            cursor = null;
        } // cerrar iterador de documentos para consulta de base de datos (BOTTOM)

        if (conectarBaseDatos != null)
        { // cerrar conexion a base de datos (TOP)
            conectarBaseDatos.close();
            conectarBaseDatos = null;
        } // cerrar conexion a base de datos (BOTTOM)

    } // metodo de ayuda para cerrar las conexiones activas (BOTTOM)

}
