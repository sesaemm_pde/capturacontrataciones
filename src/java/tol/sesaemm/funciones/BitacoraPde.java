package tol.sesaemm.funciones;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import tol.sesaemm.ing.jidv.config.ConfVariables;
import tol.sesaemm.ing.jidv.integracion.DAOBase;

public class BitacoraPde
{

    public static String contrataciones = "CO";

    public static String registro = "registro";
    public static String modificacion = "modificacion";
    public static String borrado = "borrado";
    public static String publicacion = "publicacion";
    public static String despublicacion = "despublicacion";

    /**
     *
     * @param bitacora
     *
     * @return
     */
    public static boolean registrarBitacora(TBITACORAS_PDE bitacora) throws Exception
    {
        MongoClient conectarBaseDatosBusBus = null;                             // Conexi&oacute;n a la base de datos
        MongoDatabase database;                                                 // Obtener esquema de base de datos
        MongoCollection<TBITACORAS_PDE> collection;                             // Obtener colecci&oacute;n
        boolean blnSeRegistro = false;                                          // bandera de salida
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        
        try
        {
            conectarBaseDatosBusBus = DAOBase.conectarBaseDatos();

            if (conectarBaseDatosBusBus != null)
            { // Si la conexion se ha establecido(TOP)
                database = conectarBaseDatosBusBus.getDatabase(confVariables.getBD());
                collection = database.getCollection("tbitacoras_pde", TBITACORAS_PDE.class);
                collection.insertOne(bitacora);

                blnSeRegistro = true;
            } // Si la conexion se ha establecido(BOTTOM)
            else
            { // No se ha establecido la conexion (TOP)
                throw new Exception("Conexion no establecida.");
            } // No se ha establecido la conexion (BOTTOM)
        }
        catch (Exception ex)
        {
            throw new Exception("Error al registrar bitacora de la PDE: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatosBusBus, null);
        }

        return blnSeRegistro;
    }

    /**
     * Metodo de ayuda para cerrar las conexiones activas
     *
     *
     * @param conectarBaseDatos
     * @param cursor
     */
    public static void cerrarRecursos(MongoClient conectarBaseDatos, MongoCursor<Document> cursor)
    { // metodo de ayuda para cerrar las conexiones activas (TOP)

        if (cursor != null)
        { // cerrar iterador de documentos para consulta de base de datos (TOP)
            cursor.close();
            cursor = null;
        } // cerrar iterador de documentos para consulta de base de datos (BOTTOM)

        if (conectarBaseDatos != null)
        { // cerrar conexion a base de datos (TOP)
            conectarBaseDatos.close();
            conectarBaseDatos = null;
        } // cerrar conexion a base de datos (BOTTOM)

    } // metodo de ayuda para cerrar las conexiones activas (BOTTOM)

}
