<h1>Secretaría Ejecutiva del Sistema Estatal Anticorrupción</h1>
<h2>Dirección General de Servicios Tecnológicos y Plataforma Digital</h2>
<h3>CAPTURA CONTRATACIONES</h3>
<h1>Tecnologías</h1>
<ul>
<li>Java Development Kit 1.8.172</li>
<li>Apache Tomcat 9.0.19</li>
<li>MongoDB Enterprise 4.4</li>
<li>Apache Ant 1.10.10</li>
<h1>Manuales</h1>
<a href="https://gitlab.com/sesaemm_pde/capturacontrataciones/-/blob/5276b7937f7b6a775359d2290a3c6c164b6abb83/Documentacion/M_I_CAPTURA_S6_V1.pdf">Manual de instalación</a>
<br>
<a href="https://gitlab.com/sesaemm_pde/capturacontrataciones/-/blob/5276b7937f7b6a775359d2290a3c6c164b6abb83/Documentacion/BD_CAPTURA_S6_V1%20(1).pdf">Manual de BD</a>
<br>
<a href="https://gitlab.com/sesaemm_pde/capturacontrataciones/-/blob/5276b7937f7b6a775359d2290a3c6c164b6abb83/Documentacion/M_U_CAPTURA_S6_V1.pdf">Manual de usuario</a>
<br>
</ul>