<%-- 
    Autor: Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>
<div id="seccion5">
    <h3>Comprador</h3>
    <div class="callout padding-horizontal-3">
        <form name="frmRegistrarSeccionComprador" id="frmRegistrarSeccionComprador" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post" data-abide novalidate onSubmit="return false;" autocomplete="off">
            <input type="hidden" name="accion" value="${(bolEsFormulariosDeEdicion == true) ? 'editarSeccion' : 'registrarSeccion'}"/>
            <input type="hidden" name="txtEsFormularioDeEdicion" id="txtEsFormularioDeEdicion" value="${bolEsFormulariosDeEdicion}"/>
            <input type="hidden" name="txtNUMERO_SECCION" value="5"/>
            <input type="hidden" name="txtID_CONTRATACION_SPDN" value="${idContratacion}"/>
            <fieldset class="margin-vertical-2">
                <div class="grid-x grid-margin-x">
                   <label class="cell">Comprador <span style="color: red">*</span>
                       <select required name="cmbBUYER" id="cmbBUYER" onchange="validarSelectBuyer()" title="Un comprador es una entidad cuyo presupuesto se utilizar&aacute; para pagar bienes, obras o servicios relacionados con un contrato. Este puede ser diferente de la entidad contratante que puede especificarse en los datos de licitaci&oacute;n.">
                            <option value="">--Elige una opci&oacute;n--</option>
                            <c:forEach items="${dependenciasUsuario}" var="dependencia">
                                <option value="${dependencia.clave}" ${contratacion.buyer.id == dependencia.clave ? 'selected': ''} >${dependencia.valor}</option>
                            </c:forEach>
                        </select>
                        <span class="form-error" data-form-error-for="cmbBUYER">
                            Elige una opci&oacute;n.
                        </span>
                    </label> 
                    <input type="hidden" name="txtBUYER_ID" id="txtBUYER_ID" value="${contratacion.buyer.id}" data-valor-original="${contratacion.buyer.id}"/>
                    <input type="hidden" name="txtBUYER_NAME" id="txtBUYER_NAME" value="${contratacion.buyer.name}" data-valor-original="${contratacion.buyer.name}"/>
                    <fieldset class="cell fieldset">
                        <legend title="El identificador primario para esta organizaci&oacute;n o participante. Son preferibles los identificadores que denotan de forma &uacute;nica a una entidad legal. Consulta la [gu&iacute;a de identificadores de organizaci&oacute;n] para el esquema e identificador preferido.">Identificador principal</legend>
                        <div class="grid-margin-x grid-x">
                            <label class="cell medium-6">ID
                                <input type="text" id="txtBUYER_IDENTIFIER_ID" name="txtBUYER_IDENTIFIER_ID" placeholder="p. ej. 1" value="${contratacion.buyer.identifier.id}" title="El identificador de la organizaci&oacute;n en el esquema seleccionado."/>
                                <span class="form-error" data-form-error-for="txtBUYER_IDENTIFIER_ID" id="spanBUYER_IDENTIFIER_ID">
                                    Campo requerido, verifica el formato de tu registro.
                                </span>
                            </label>
                            <label class="cell medium-6">Esquema
                                <input type="text" id="txtBUYER_IDENTIFIER_SCHEME" name="txtBUYER_IDENTIFIER_SCHEME" placeholder="p. ej. Esquema" value="${contratacion.buyer.identifier.scheme}" title="Los identificadores de organizaci&oacute;n deben de obtenerse de una lista de identificadores de organizaciones existentes. El campo scheme se usa para indicar o registrar de d&oacute;nde se toma el identificador. Este valor deber&iacute;a tomarse del [Esquema de Identificadores de Organizaciones]."/>
                                <span class="form-error" data-form-error-for="txtBUYER_IDENTIFIER_SCHEME" id="spanBUYER_IDENTIFIER_SCHEME">
                                    Campo requerido, verifica el formato de tu registro.
                                </span>
                            </label>
                            <label class="cell medium-6">Nombre Legal
                                <input type="text" id="txtBUYER_IDENTIFIER_LEGAL_NAME" name="txtBUYER_IDENTIFIER_LEGAL_NAME" placeholder="p. ej. Nombre Legal" value="${contratacion.buyer.identifier.legalName}" title="El nombre legalmente registrado de la organizaci&oacute;n." />
                                <span class="form-error" data-form-error-for="txtBUYER_IDENTIFIER_LEGAL_NAME" id="spanBUYER_IDENTIFIER_LEGAL_NAME">
                                    Campo requerido, verifica el formato de tu registro.
                                </span>
                            </label>
                            <label class="cell medium-6">URI
                                <input type="text" id="txtBUYER_IDENTIFIER_URI" name="txtBUYER_IDENTIFIER_URI" placeholder="p. ej. www.url.com" value="${contratacion.buyer.identifier.uri}" pattern="url" title="Una URI para identificar a la organizaci&oacute;n, como los prove&iacute;dos por [Open Corporates] o alg&uacute;n otro proveedor relevante de URIs. Este campo no debe ser utilizado para especificar el sitio web de la organizaci&oacute;n, el cual puede ser especificado en el campo URL del punto de contacto de la Organizaci&oacute;n."/>
                                <span class="form-error" data-form-error-for="txtBUYER_IDENTIFIER_URI" id="spanBUYER_IDENTIFIER_URI">
                                    Campo requerido, verifica el formato de tu registro.
                                </span>
                            </label>
                        </div>
                    </fieldset>
                    <fieldset class="cell fieldset"> 
                        <input type="hidden" id="estructura-por-defecto-identifier" value=""> 
                        <input type="hidden" id="siguiente-identificador-identifier" value="" data-valor-por-defecto=""> 
                        <input type="hidden" id="lista-identificadores-por-registrar-identifier" name="lista-identificadores-por-registrar-identifier" value="" data-valor-por-defecto="">
                        <legend title="Una lista adicional/suplementaria de identificadores para la organizaci�n, utilizando la [gu�a de identificadores de organizaciones]. Esto puede usarse para dar un identificador utilizado internamente por esta organizaci�n adem�s del identificador primario legal de la entidad.">Identificadores adicionales</legend>
                        <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                            <a onclick="restaurarEstadoInicialElemento('identifier', [])"><i class="material-icons secondary">restart_alt</i></a> 
                            <a onclick="abrirNuevoFormulario('identifier', [])"><i class="material-icons secondary">add_circle</i></a> 
                        </div>
                        <c:choose>
                            <c:when test="${contratacion.buyer.additionalIdentifiers.size() > 0}">
                                <c:set var="contadorElementosIdentifier" value="0"></c:set>
                                <div id="contenedor-identifier" style="display: flex; flex-direction: column; gap: 10px;">
                                <c:forEach items="${contratacion.buyer.additionalIdentifiers}" var="identifier">
                                    <div id="identifier_${contadorElementosIdentifier}">
                                        <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                            <div class="cell medium-8">
                                                <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">identificador</span> - <span id="titulo-elemento-identifier_${contadorElementosIdentifier}">${identifier.id}</span></h6>
                                            </div>
                                            <div id="datos-elemento-identifier_${contadorElementosIdentifier}">
                                                <input type="hidden" name="txtIDENTIFIER_ID_${contadorElementosIdentifier}" id="txtIDENTIFIER_ID_${contadorElementosIdentifier}" value="${identifier.id}" >
                                                <input type="hidden" name="txtIDENTIFIER_SCHEME_${contadorElementosIdentifier}" id="txtIDENTIFIER_SCHEME_${contadorElementosIdentifier}" value="${identifier.scheme}" >
                                                <input type="hidden" name="txtIDENTIFIER_LEGAL_NAME_${contadorElementosIdentifier}" id="txtIDENTIFIER_LEGAL_NAME_${contadorElementosIdentifier}" value="${identifier.legalName}" >
                                                <input type="hidden" name="txtIDENTIFIER_URI_${contadorElementosIdentifier}" id="txtIDENTIFIER_URI_${contadorElementosIdentifier}" value="${identifier.uri}" >                                                        
                                            </div>
                                            <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                <button class="button margin-0" onclick="abrirFormularioPrevio('identifier', [${contadorElementosIdentifier}])"><i class="material-icons">edit</i> Editar</button>
                                                <button class="button margin-0" onclick="eliminarElemento('identifier', 'identifier_${contadorElementosIdentifier}', [], '${contadorElementosIdentifier}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                            </div>
                                        </div>
                                    </div>
                                    <c:set var="contadorElementosIdentifier" value="${contadorElementosIdentifier + 1}"></c:set>
                                </c:forEach>
                                </div>
                                <input type="hidden" name="ultimo-identificador-elementos-identifier" id="ultimo-identificador-elementos-identifier" value="${contadorElementosIdentifier}">
                            </c:when>
                            <c:otherwise>
                                <div id="contenedor-identifier" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                <input type="hidden" name="ultimo-identificador-elementos-identifier" id="ultimo-identificador-elementos-identifier" value="0">
                            </c:otherwise>
                        </c:choose>
                        <script>
                            document.getElementById("estructura-por-defecto-identifier").value = obtenerDatosInicialesElementos("identifier", []);
                        </script>
                    </fieldset>
                    <fieldset class="cell fieldset">
                        <legend title="Una direcci&oacute;n. Esta puede ser la direcci&oacute;n legalmente registrada de la organizaci&oacute;n o puede ser una direcci&oacute;n donde se reciba correspondencia para este proceso de contrataci&oacute;n particular.">Direcci&oacute;n</legend>
                        <div class="grid-margin-x grid-x">
                            <label class="cell medium-4">Direcci&oacute;n
                                <input type="text" id="txtBUYER_ADDRESS_STREET_ADDRESS" name="txtBUYER_ADDRESS_STREET_ADDRESS" placeholder="p. ej. 1600 Amphitheatre Pkwy." value="${contratacion.buyer.address.streetAddress}" title="La direcci&oacute;n de la calle."/>
                                <span class="form-error" data-form-error-for="txtBUYER_ADDRESS_STREET_ADDRESS" id="spanBUYER_ADDRESS_STREET_ADDRESS">
                                    Campo requerido, verifica el formato de tu registro.
                                </span>
                            </label>
                            <label class="cell medium-4">Localidad
                                <input type="text" id="txtBUYER_ADDRESS_LOCALITY" name="txtBUYER_ADDRESS_LOCALITY" placeholder="p. ej. Mountain View." value="${contratacion.buyer.address.locality}" title="La localidad." />
                                <span class="form-error" data-form-error-for="txtBUYER_ADDRESS_LOCALITY" id="spanBUYER_ADDRESS_LOCALITY">
                                    Campo requerido, verifica el formato de tu registro.
                                </span>
                            </label>
                            <label class="cell medium-4">Regi&oacute;n
                                <input type="text" id="txtBUYER_ADDRESS_REGION" name="txtBUYER_ADDRESS_REGION" placeholder="p. ej. CA." value="${contratacion.buyer.address.region}" title="La regi&oacute;n."/>
                                <span class="form-error" data-form-error-for="txtBUYER_ADDRESS_REGION" id="spanBUYER_ADDRESS_REGION">
                                    Campo requerido, verifica el formato de tu registro.
                                </span>
                            </label>
                            <label class="cell medium-4">C&oacute;digo postal
                                <input type="text" id="txtBUYER_ADDRESS_POSTAL_CODE" name="txtBUYER_ADDRESS_POSTAL_CODE" placeholder="p. ej. 94043" value="${contratacion.buyer.address.postalCode}" pattern="codigo_postal" title="El c&oacute;digo postal." />
                                <span class="form-error" data-form-error-for="txtBUYER_ADDRESS_POSTAL_CODE" id="spanBUYER_ADDRESS_POSTAL_CODE">
                                    Campo requerido, verifica el formato de tu registro.
                                </span>
                            </label>
                            <label class="cell medium-4">Pa&iacute;s
                                <input type="text" id="txtBUYER_ADDRESS_COUNTRY_NAME" name="txtBUYER_ADDRESS_COUNTRY_NAME" placeholder="p. ej. M&eacute;xico" value="${contratacion.buyer.address.countryName}" title="El nombre del pa&iacute;s." />
                                <span class="form-error" data-form-error-for="txtBUYER_ADDRESS_COUNTRY_NAME" id="spanBUYER_ADDRESS_COUNTRY_NAME">
                                    Campo requerido, verifica el formato de tu registro.
                                </span>
                            </label>
                        </div>
                    </fieldset>
                    <fieldset class="cell fieldset">
                        <legend title="Detalles de contacto que pueden usarse para esta parte involucrada.">Punto de contacto</legend>
                        <div class="grid-margin-x grid-x">
                            <label class="cell medium-4">Nombre
                                <input type="text" id="txtBUYER_CONTACT_NAME" name="txtBUYER_CONTACT_NAME" placeholder="p. ej. Carlos" value="${contratacion.buyer.contactPoint.name}" title="El nombre de la persona de contacto, departamento o punto de contacto en relaci&oacute;n a este proceso de contrataci&oacute;n."/>
                                <span class="form-error" data-form-error-for="txtBUYER_CONTACT_NAME" id="spanBUYER_CONTACT_NAME">
                                    Campo requerido, verifica el formato de tu registro.
                                </span>
                            </label>
                            <label class="cell medium-4">Correo electr&oacute;nico
                                <input type="text" id="txtBUYER_CONTACT_EMAIL" name="txtBUYER_CONTACT_EMAIL" placeholder="p. ej. email@gmail.com" value="${contratacion.buyer.contactPoint.email}" title="La direcci&oacute;n de correo del punto o persona de contacto."/>
                                <span class="form-error" data-form-error-for="txtBUYER_CONTACT_EMAIL" id="spanBUYER_CONTACT_EMAIL">
                                    Campo requerido, verifica el formato de tu registro.
                                </span>
                            </label>
                            <label class="cell medium-4">Tel&eacute;fono
                                <input type="text" id="txtBUYER_CONTACT_TELEPHONE" name="txtBUYER_CONTACT_TELEPHONE" placeholder="p. ej. 5555555555 o 555-555-5555" value="${contratacion.buyer.contactPoint.telephone}" title="El n&uacute;mero de tel&eacute;fono del punto o persona de contacto. Este debe de incluir el c&oacute;digo de marcaci&oacute;n internacional." />
                                <span class="form-error" data-form-error-for="txtBUYER_CONTACT_TELEPHONE" id="spanBUYER_CONTACT_TELEPHONE">
                                    Campo requerido, verifica el formato de tu registro.
                                </span>
                            </label>
                            <label class="cell medium-4">N&uacute;mero de fax
                                <input type="text" id="txtBUYER_CONTACT_FAXNUMBER" name="txtBUYER_CONTACT_FAXNUMBER" placeholder="p. ej. 5555555555 o 555-555-5555" value="${contratacion.buyer.contactPoint.faxNumber}" title="El n&uacute;mero de fax del punto o persona de contacto. Este debe de incluir el c&oacute;digo de marcaci&oacute;n internacional."/>
                                <span class="form-error" data-form-error-for="txtBUYER_CONTACT_FAXNUMBER" id="spanBUYER_CONTACT_FAXNUMBER">
                                    Campo requerido, verifica el formato de tu registro.
                                </span>
                            </label>
                            <label class="cell medium-4">URL
                                <input type="text" id="txtBUYER_CONTACT_URL" name="txtBUYER_CONTACT_URL" placeholder="p. ej. www.url.com" value="${contratacion.buyer.contactPoint.url}" pattern="url" title="Una direcci&oacute;n web para el punto o persona de contacto."/>
                                <span class="form-error" data-form-error-for="txtBUYER_CONTACT_URL" id="spanBUYER_CONTACT_URL">
                                    Campo requerido, verifica el formato de tu registro.
                                </span>
                            </label>
                        </div>
                    </fieldset>
                </div>
            </fieldset>
            <div class="grid-x grid-margin-x">
                <fieldset class="cell medium-6">
                    <input type="reset" class="button expanded secondary" onclick="restaurarValoresBuyer();" id="btnResetComprador" name="btnResetComprador" value="${(bolEsFormulariosDeEdicion == true) ? 'Restaurar' : 'Reiniciar'}"/>
                </fieldset>
                <fieldset class="cell medium-6">
                    <input type="button" onclick="validarFormulario('frmRegistrarSeccionComprador');" class="button expanded" name="btnRegistrarComprador" value="${(bolEsFormulariosDeEdicion == true) ? 'Modificar datos' : 'Continuar'}"/>
                </fieldset>
            </div>
        </form>
        <div data-abide-error="data-abide-error" class="alert callout alertFooter" style="display: none;">
            <p>
                <i class="material-icons">warning</i>
                Existen algunos errores en tu registro de la secci&oacute;n 'Comprador'. Verifica nuevamente.
            </p>
        </div>
    </div>
    
</div>
   
<%-- formulario de un identificador (TOP) --%>
<div id="modalFrmIdentifier" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Identificador</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmIdentifier" id="frmIdentifier" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-identifier" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-6">ID <span style="color: red">**</span>
                            <input type="text" required id="txtIDENTIFIER_ID" placeholder="p. ej. 1" value="" title="El identificador de la organizaci&oacute;n en el esquema seleccionado."/>
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_ID" id="spanIDENTIFIER_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Esquema
                            <input type="text" id="txtIDENTIFIER_SCHEME" placeholder="p. ej. Esquema" value="" title="Los identificadores de organizaci&oacute;n deben de obtenerse de una lista de identificadores de organizaciones existentes. El campo scheme se usa para indicar o registrar de d&oacute;nde se toma el identificador. Este valor deber&iacute;a tomarse del [Esquema de Identificadores de Organizaciones]."/>
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_SCHEME" id="spanIDENTIFIER_SCHEME">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Nombre Legal
                            <input type="text" id="txtIDENTIFIER_LEGAL_NAME" placeholder="p. ej. Nombre Legal" value="" title="El nombre legalmente registrado de la organizaci&oacute;n." />
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_LEGAL_NAME" id="spanIDENTIFIER_LEGAL_NAME">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">URI
                            <input type="text" id="txtIDENTIFIER_URI" placeholder="p. ej. www.url.com" value="" pattern="url" title="Una URI para identificar a la organizaci&oacute;n, como los prove&iacute;dos por [Open Corporates] o alg&uacute;n otro proveedor relevante de URIs. Este campo no debe ser utilizado para especificar el sitio web de la organizaci&oacute;n, el cual puede ser especificado en el campo URL del punto de contacto de la Organizaci&oacute;n." />
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_URI" id="spanIDENTIFIER_URI">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-identifier"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-identifier"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un identificador (BOTTOM) --%>
                
<script>
                
    function validarFormulario(idFormulario)
    { 
        
        $("#frmRegistrarSeccionComprador").foundation('validateForm');
        $("#frmRegistrarSeccionComprador").on("formvalid.zf.abide", function (ev, frm){
            
            let strMensajeFrm;          
            let strTituloMensaje;       
            let bolTipoFormulario;      
            
            bolTipoFormulario = document.getElementById("txtEsFormularioDeEdicion").value;
            bolTipoFormulario = Boolean(bolTipoFormulario);
            strMensajeFrm = (bolTipoFormulario === true) ? "\u00bf Los datos son correctos\u003F" : "Una vez guardada la informaci\u00F3n de esta secci\u00F3n no se podr\u00E1 volver a modificar los valores ingresados hasta que se finalice con el proceso de registro de la contrataci\u00F3n p\u00FAblica.";
            strTituloMensaje = (bolTipoFormulario === true) ? "Confirmar" : "\u00bf Los datos son correctos\u003F";
            
            $(".alertFooter").css("display", "none");
            
            swal({
                title: strTituloMensaje,
                text: strMensajeFrm,
                icon: "warning",
                buttons: ["Cancelar", "Aceptar"],
                dangerMode: true,
                closeOnClickOutside: false,
                closeOnEsc: false
            })
            .then((seAcepta) => {
                if (seAcepta)
                { 
                    document.getElementById(idFormulario).submit();
                } 
                else
                { 
                    swal("Se ha cancelado la operaci\u00f3n", {
                        icon: "error",
                        buttons: false,
                        timer: 2000
                    });
                    return false;
                } 
            });
            
        })
        .on("forminvalid.zf.abide", function(ev,frm) {
            $(".alertFooter").css("display", "block");
        });
                    
    } 

    function validarSelectBuyer()
    {
        
        let select = document.getElementById('cmbBUYER');
        let valor = select.value;
        let texto = select.options[select.selectedIndex].text;
        
        document.getElementById('txtBUYER_ID').value = valor;
        document.getElementById('txtBUYER_NAME').value = texto;
        
    }
    
    function restaurarValoresBuyer() 
    {

        let valorOriginalId = document.getElementById('txtBUYER_ID').dataset.valorOriginal;
        let valorOriginalName = document.getElementById('txtBUYER_NAME').dataset.valorOriginal;

        document.getElementById('txtBUYER_ID').value = valorOriginalId;
        document.getElementById('txtBUYER_NAME').value = valorOriginalName;
        
        restaurarEstadoInicialElemento('identifier', []);
        
    }
    
</script> 