<%-- 
    Autor: Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<div id="seccion6-bloque1">
    <h4>Informaci&oacute;n general</h4>
    <div class="callout padding-horizontal-3">
        <form name="frmRegistrarSeccionPlaneacionBloqueInformacionGeneral" id="frmRegistrarSeccionPlaneacionBloqueInformacionGeneral" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post" data-abide novalidate onSubmit="event.preventDefault(); validarFormularioPlaneacionInformacionGeneral(this.id);" autocomplete="off">
            <input type="hidden" name="accion" value="${(bolEsFormulariosDeEdicion == true) ? 'editarSeccion' : 'registrarSeccion'}"/>
            <input type="hidden" name="txtEsFormularioDeEdicion" id="txtEsFormularioDeEdicion" value="${bolEsFormulariosDeEdicion}"/>
            <input type="hidden" name="txtNUMERO_SECCION" value="6"/>
            <input type="hidden" name="txtNUMERO_BLOQUE" value="1"/>
            <input type="hidden" name="txtID_CONTRATACION_SPDN" value="${idContratacion}"/>
            <div class="grid-x grid-margin-x">
                <label class="cell">Justificaci&oacute;n
                    <input type="text" name="txtRATIONALE" id="txtAreaRATIONALE" placeholder="p. ej. Justificaci&oacute;n" value="${contratacion.planning.rationale}" title="La justificaci&oacute;n para la adquisici&oacute;n provista en texto libre. Se puede proveer m&aacute;s detalle en un documento adjunto." />
                    <span class="form-error" data-form-error-for="txtRATIONALE" id="spanRATIONALE">
                        Campo requerido, verifica el formato de tu registro.
                    </span>
                </label>
            </div>
            <div class="grid-x grid-margin-x">
                <fieldset class="cell medium-6">
                    <input type="reset" class="button expanded secondary" name="btnResetPlaneacionHitos" value="${(bolEsFormulariosDeEdicion == true) ? 'Restaurar' : 'Reiniciar'}"/>
                </fieldset>
                <fieldset class="cell medium-6">
                    <input type="submit" class="button expanded" name="btnRegistrarPlaneacionHitos" value="${(bolEsFormulariosDeEdicion == true) ? 'Modificar datos' : 'Continuar'}"/>
                </fieldset>
            </div>
        </form>
        <div data-abide-error="data-abide-error" class="alert callout alertFooter" style="display: none;">
            <p>
                <i class="material-icons">warning</i>
                Existen algunos errores en tu registro del bloque 'Informaci&oacute;n general' perteneciente a la secci&oacute;n 'Planeaci&oacute;n'. Verifica nuevamente.
            </p>
        </div>
    </div>
    
</div>
            
<script>
                
    function validarFormularioPlaneacionInformacionGeneral(idFormulario)
    { 
        
        $("#frmRegistrarSeccionPlaneacionBloqueInformacionGeneral").on("formvalid.zf.abide", function (ev, frm){
            
            let strMensajeFrm;          
            let strTituloMensaje;       
            let bolTipoFormulario;      
            
            bolTipoFormulario = document.getElementById("txtEsFormularioDeEdicion").value;
            bolTipoFormulario = Boolean(bolTipoFormulario);
            strMensajeFrm = (bolTipoFormulario === true) ? "\u00bf Los datos son correctos\u003F" : "Una vez guardada la informaci\u00F3n de esta secci\u00F3n no se podr\u00E1 volver a modificar los valores ingresados hasta que se finalice con el proceso de registro de la contrataci\u00F3n p\u00FAblica.";
            strTituloMensaje = (bolTipoFormulario === true) ? "Confirmar" : "\u00bf Los datos son correctos\u003F";
            
            $(".alertFooter").css("display", "none");
            
            swal({
                title: strTituloMensaje,
                text: strMensajeFrm,
                icon: "warning",
                buttons: ["Cancelar", "Aceptar"],
                dangerMode: true,
                closeOnClickOutside: false,
                closeOnEsc: false
            })
            .then((seAcepta) => {
                if (seAcepta)
                { 
                    document.getElementById(idFormulario).submit();
                } 
                else
                { 
                    swal("Se ha cancelado la operaci\u00f3n", {
                        icon: "error",
                        buttons: false,
                        timer: 2000
                    });
                    return false;
                } 
            });
            
        })
        .on("forminvalid.zf.abide", function(ev,frm) {
            $(".alertFooter").css("display", "block");
        });
                    
    } 
    
</script> 
