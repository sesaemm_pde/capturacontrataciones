<%-- 
    Autor: Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>
<div id="seccion6-bloque4">
    <h4>Hitos</h4>
    <div class="button-group">
        <button type="button" class="button" onclick="abrirNuevoFormulario('milestone', []);">Agregar hito</button>
    </div>
    <div class="callout padding-horizontal-3">
        <form name="frmRegistrarSeccionPlaneacionHitos" id="frmRegistrarSeccionPlaneacionHitos" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post" data-abide novalidate onSubmit="return false;" autocomplete="off">
            <input type="hidden" name="accion" value="${(bolEsFormulariosDeEdicion == true) ? 'editarSeccion' : 'registrarSeccion'}"/>
            <input type="hidden" name="txtEsFormularioDeEdicion" id="txtEsFormularioDeEdicion" value="${bolEsFormulariosDeEdicion}"/>
            <input type="hidden" name="txtNUMERO_SECCION" value="6"/>
            <input type="hidden" name="txtNUMERO_BLOQUE" value="4"/>
            <input type="hidden" name="txtID_CONTRATACION_SPDN" value="${idContratacion}"/>
            <input type="hidden" id="estructura-por-defecto-milestone" value=""> 
            <input type="hidden" id="siguiente-identificador-milestone" value="" data-valor-por-defecto=""> 
            <input type="hidden" id="lista-identificadores-por-registrar-milestone" name="lista-identificadores-por-registrar-milestone" value="" data-valor-por-defecto="">
            <c:choose>
                <c:when test="${contratacion.planning.milestones.size() > 0}">
                    <c:set var="contadorElementosMilestone" value="0"></c:set>
                    <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-milestone">
                    <c:forEach items="${contratacion.planning.milestones}" var="milestone">
                        <li class="accordion-item" data-accordion-item id="milestone_${contadorElementosMilestone}">
                            <a href="#" class="accordion-title"><span style="text-transform: capitalize;">hito</span> - <span id="titulo-elemento-milestone_${contadorElementosMilestone}">${milestone.id}</span></a>
                            <div class="accordion-content" data-tab-content>
                                <div class="grid-margin-x grid-x">
                                    <div class="cell padding-horizontal-1" style="display:flex; justify-content: flex-end; gap: 10px; flex-wrap: wrap;">
                                        <button class="button margin-0 btnFrmContrataciones" onclick="abrirFormularioPrevio('milestone', [${contadorElementosMilestone}])"><i class="material-icons">edit</i> Editar hito</button>
                                        <button class="button margin-0 btnFrmContrataciones" onclick="eliminarElemento('milestone', 'milestone_${contadorElementosMilestone}', [], '${contadorElementosMilestone}')"><i class="material-icons">remove_circle</i> Eliminar hito</button>
                                    </div>
                                    <div id="datos-elemento-milestone_${contadorElementosMilestone}">
                                        <input type="hidden" name="txtMILESTONE_ID_${contadorElementosMilestone}" id="txtMILESTONE_ID_${contadorElementosMilestone}" value="${milestone.id}">
                                        <input type="hidden" name="cmbMILESTONE_TYPE_${contadorElementosMilestone}" id="cmbMILESTONE_TYPE_${contadorElementosMilestone}" value="${milestone.type}">
                                        <input type="hidden" name="txtMILESTONE_TITLE_${contadorElementosMilestone}" id="txtMILESTONE_TITLE_${contadorElementosMilestone}" value="${milestone.title}">
                                        <input type="hidden" name="txtMILESTONE_DESCRIPTION_${contadorElementosMilestone}" id="txtMILESTONE_DESCRIPTION_${contadorElementosMilestone}" value="${milestone.description}">
                                        <input type="hidden" name="txtMILESTONE_CODE_${contadorElementosMilestone}" id="txtMILESTONE_CODE_${contadorElementosMilestone}" value="${milestone.code}">
                                        <input type="hidden" name="txtMILESTONE_DUE_DATE_${contadorElementosMilestone}" id="txtMILESTONE_DUE_DATE_${contadorElementosMilestone}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(milestone.dueDate)}">
                                        <input type="hidden" name="txtMILESTONE_DATE_MET_${contadorElementosMilestone}" id="txtMILESTONE_DATE_MET_${contadorElementosMilestone}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(milestone.dateMet)}">
                                        <input type="hidden" name="txtMILESTONE_DATE_MODIFIED_${contadorElementosMilestone}" id="txtMILESTONE_DATE_MODIFIED_${contadorElementosMilestone}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(milestone.dateModified)}">
                                        <input type="hidden" name="cmbMILESTONE_STATUS_${contadorElementosMilestone}" id="cmbMILESTONE_STATUS_${contadorElementosMilestone}" value="${milestone.status}">
                                    </div>
                                    <fieldset class="cell fieldset"> 
                                        <input type="hidden" id="estructura-por-defecto-milestone-document_${contadorElementosMilestone}" value=""> 
                                        <input type="hidden" id="siguiente-identificador-milestone-document_${contadorElementosMilestone}" value="" data-valor-por-defecto=""> 
                                        <input type="hidden" id="lista-identificadores-por-registrar-milestone-document_${contadorElementosMilestone}" name="lista-identificadores-por-registrar-milestone-document_${contadorElementosMilestone}" value="" data-valor-por-defecto="">
                                        <legend title="Lista de documentos asociados con este hito.">Documentos</legend>
                                        <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                            <a onclick="restaurarEstadoInicialElemento('milestone-document', [ ${contadorElementosMilestone}])"><i class="material-icons secondary">restart_alt</i></a> 
                                            <a onclick="abrirNuevoFormulario('milestone-document', [ ${contadorElementosMilestone}])"><i class="material-icons secondary">add_circle</i></a> 
                                        </div>
                                        <c:choose>
                                            <c:when test="${milestone.documents.size() > 0}">
                                                <c:set var="contadorElementosMilestoneDocument" value="0"></c:set>
                                                <div id="contenedor-milestone-document_${contadorElementosMilestone}" style="display: flex; flex-direction: column; gap: 10px;">
                                                <c:forEach items="${milestone.documents}" var="milestoneDocument">
                                                    <div id="milestone-document_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}">
                                                        <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                                            <div class="cell medium-8">
                                                                <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">documento</span> - <span id="titulo-elemento-milestone-document_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}">${milestoneDocument.id}</span></h6>
                                                            </div>
                                                            <div id="datos-elemento-milestone-document_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}">
                                                                <input type="hidden" name="txtMILESTONE_DOCUMENT_ID_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" id="txtMILESTONE_DOCUMENT_ID_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" value="${milestoneDocument.id}">
                                                                <input type="hidden" name="cmbMILESTONE_DOCUMENT_TYPE_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" id="cmbMILESTONE_DOCUMENT_TYPE_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" value="${milestoneDocument.documentType}">
                                                                <input type="hidden" name="txtMILESTONE_DOCUMENT_TITLE_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" id="txtMILESTONE_DOCUMENT_TITLE_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" value="${milestoneDocument.title}">
                                                                <input type="hidden" name="txtMILESTONE_DOCUMENT_DESCRIPTION_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" id="txtMILESTONE_DOCUMENT_DESCRIPTION_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" value="${milestoneDocument.description}">
                                                                <input type="hidden" name="txtMILESTONE_DOCUMENT_URL_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" id="txtMILESTONE_DOCUMENT_URL_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" value="${milestoneDocument.url}">
                                                                <input type="hidden" name="txtMILESTONE_DOCUMENT_DATE_PUBLISHED_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" id="txtMILESTONE_DOCUMENT_DATE_PUBLISHED_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(milestoneDocument.datePublished)}">
                                                                <input type="hidden" name="txtMILESTONE_DOCUMENT_DATE_MODIFIED_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" id="txtMILESTONE_DOCUMENT_DATE_MODIFIED_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(milestoneDocument.dateModified)}">
                                                                <input type="hidden" name="txtMILESTONE_DOCUMENT_FORMAT_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" id="txtMILESTONE_DOCUMENT_FORMAT_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" value="${milestoneDocument.format}">
                                                                <input type="hidden" name="cmbMILESTONE_DOCUMENT_LANGUAGE_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" id="cmbMILESTONE_DOCUMENT_LANGUAGE_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" value="${milestoneDocument.language}">
                                                            </div>
                                                            <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                                <button class="button margin-0" onclick="abrirFormularioPrevio('milestone-document', [${contadorElementosMilestone}, ${contadorElementosMilestoneDocument}])"><i class="material-icons">edit</i> Editar</button>
                                                                <button class="button margin-0" onclick="eliminarElemento('milestone-document', 'milestone-document_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}', [${contadorElementosMilestone}], '${contadorElementosMilestoneDocument}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <c:set var="contadorElementosMilestoneDocument" value="${contadorElementosMilestoneDocument + 1}"></c:set>
                                                </c:forEach>
                                                </div>
                                                <input type="hidden" name="ultimo-identificador-elementos-milestone-document_${contadorElementosMilestone}" id="ultimo-identificador-elementos-milestone-document_${contadorElementosMilestone}" value="${contadorElementosMilestoneDocument}">
                                            </c:when>
                                            <c:otherwise>
                                                <div id="contenedor-milestone-document_${contadorElementosMilestone}" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                                <input type="hidden" name="ultimo-identificador-elementos-milestone-document_${contadorElementosMilestone}" id="ultimo-identificador-elementos-milestone-document_${contadorElementosMilestone}" value="0">
                                            </c:otherwise>
                                        </c:choose>
                                        <script>
                                            document.getElementById("estructura-por-defecto-milestone-document_${contadorElementosMilestone}").value = obtenerDatosInicialesElementos("milestone-document", [${contadorElementosMilestone}]);
                                        </script>
                                    </fieldset>
                                </div>
                            </div>
                        </li>
                        <c:set var="contadorElementosMilestone" value="${contadorElementosMilestone + 1}"></c:set>
                    </c:forEach>
                    </ul>
                    <input type="hidden" name="ultimo-identificador-elementos-milestone" id="ultimo-identificador-elementos-milestone" value="${contadorElementosMilestone}">
                </c:when>
                <c:otherwise>
                    <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-milestone"></ul>
                    <input type="hidden" name="ultimo-identificador-elementos-milestone" id="ultimo-identificador-elementos-milestone" value="0">
                </c:otherwise>
            </c:choose>
            <div class="grid-x grid-margin-x margin-top-1">
                <fieldset class="cell medium-6">
                    <input type="button" class="button expanded secondary" onclick="restaurarEstadoInicialElemento('milestone', []);" value="${(bolEsFormulariosDeEdicion == true) ? 'Restaurar' : 'Reiniciar'}"/>
                </fieldset>
                <fieldset class="cell medium-6">
                    <input type="button" onclick="validarFormulario('frmRegistrarSeccionPlaneacionHitos');" class="button expanded" id="btnRegistrarValoresFormulario" value="${(bolEsFormulariosDeEdicion == true) ? 'Modificar datos' : 'Continuar'}"/>
                </fieldset>
            </div>
        </form>
    </div>
</div>
          
<%-- formulario de un documento - hito (TOP) --%>
<div id="modalFrmMilestoneDocument" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Documento - Hito</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmMilestoneDocument" id="frmMilestoneDocument" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-milestone-document" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-4">ID <span style="color: red">**</span>
                            <input type="text" id="txtMILESTONE_DOCUMENT_ID" required placeholder="p. ej. 1" value="" title="Identificador local y &uacute;nico para este documento. Este campo se utiliza para darle seguimiento a las m&uacute;ltiples versiones de un documento en el proceso de creaci&oacute;n de un registro del proceso de contrataci&oacute;n (record) que se genera a partir de las entregas (release)."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_DOCUMENT_ID" id="spanMILESTONE_DOCUMENT_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Tipo de Documento
                            <select id="cmbMILESTONE_DOCUMENT_TYPE" title="Una clasificaci&oacute;n del documento descrito.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${tiposDeDocumentoPlaneacion}" var="tipoDocumento">
                                    <option value="${tipoDocumento.clave}">${tipoDocumento.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbMILESTONE_DOCUMENT_TYPE" id="spanMILESTONE_DOCUMENT_TYPE">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                        <label class="cell medium-4">T&iacute;tulo
                            <input type="text" id="txtMILESTONE_DOCUMENT_TITLE" placeholder="p. ej. T&iacute;tulo del documento" value="" title="El t&iacute;tulo del documento."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_DOCUMENT_TITLE" id="spanMILESTONE_DOCUMENT_TITLE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Descripci&oacute;n
                            <input type="text" id="txtMILESTONE_DOCUMENT_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n del documento" value="" title="Una descripci&oacute;n corta del documento. Se recomienda que las descripciones no excedan 250 palabras. En el caso en que el documento no est&eacute; accesible en l&iacute;nea, el campo de descripci&oacute;n puede utilizarse para describir los arreglos necesarios para obtener una copia del documento."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_DOCUMENT_DESCRIPTION" id="spanMILESTONE_DOCUMENT_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">URL
                            <input type="text" id="txtMILESTONE_DOCUMENT_URL" placeholder="p. ej. www.url.com" value="" pattern="url" title="Un enlace directo al documento o archivo adjunto. El servidor que da acceso a este documento debe de estar configurado para reportar correctamente el tipo MIME de documento."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_DOCUMENT_URL" id="spanMILESTONE_DOCUMENT_URL">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de publicaci&oacute;n
                            <input type="date" id="txtMILESTONE_DOCUMENT_DATE_PUBLISHED" value="" pattern="fecha" title="La fecha de publicaci&oacute;n del documento. Esto es particularmente importante para documentos relevantes desde el punto de vista legal, como los avisos de licitaci&oacute;n."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_DOCUMENT_DATE_PUBLISHED" id="spanMILESTONE_DOCUMENT_DATE_PUBLISHED">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de modificaci&oacute;n
                            <input type="date" id="txtMILESTONE_DOCUMENT_DATE_MODIFIED" value="" pattern="fecha" title="Fecha en que se modific&oacute; por &uacute;ltima vez el documento."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_DOCUMENT_DATE_MODIFIED" id="spanMILESTONE_DOCUMENT_DATE_MODIFIED">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Formato
                            <input type="text" id="txtMILESTONE_DOCUMENT_FORMAT" placeholder="p. ej. text/html" value="" title="El formato del documento, utilizando la lista de c&oacute;digos abierta [IANA Media Types] (vea los valores en la columna &#39;Template&#39;), o utilizando el c&oacute;digo &#39;offline/print&#39;  si el documento descrito se publica offline."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_DOCUMENT_FORMAT" id="spanMILESTONE_DOCUMENT_FORMAT">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Idioma
                            <select id="cmbMILESTONE_DOCUMENT_LANGUAGE" title="El idioma predeterminado de los datos con dos letras [ISO639-1].">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${tiposDeIdioma}" var="idioma">
                                    <option value="${idioma.codigo}" >${idioma.nombre}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbMILESTONE_DOCUMENT_LANGUAGE" id="spanMILESTONE_DOCUMENT_LANGUAGE">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-milestone-document"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-milestone-document"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un documento - hito (BOTTOM) --%>
<%-- formulario de un hito (TOP) --%>
<div id="modalFrmMilestone" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Hito</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmMilestone" id="frmMilestone" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-milestone" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-4">ID
                            <input type="text" id="txtMILESTONE_ID" required placeholder="p. ej. 1" value="" title="Un identificador local para este hito, &uacute;nico dentro de este bloque. Este campo se usa para llevar registro de m&uacute;ltiples revisiones de un hito a trav&eacute;s de la compilaci&oacute;n de entrega a mecanismo de registro." />
                            <span class="form-error" data-form-error-for="txtMILESTONE_ID" id="spanMILESTONE_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Tipo de hito
                            <select id="cmbMILESTONE_TYPE" title="El tipo del hito.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${tiposDeHito}" var="tipo">
                                    <option value="${tipo.clave}">${tipo.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbMILESTONE_TYPE" id="spanMILESTONE_TYPE">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                        <label class="cell medium-4">T&iacute;tulo
                            <input type="text" id="txtMILESTONE_TITLE" placeholder="p. ej. T&iacute;tulo del hito" value="" title="T&iacute;tulo del hito." />
                            <span class="form-error" data-form-error-for="txtMILESTONE_TITLE" id="spanMILESTONE_TITLE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Descripci&oacute;n
                            <input type="text" id="txtMILESTONE_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n del hito" value="" title="Una descripci&oacute;n del hito." />
                            <span class="form-error" data-form-error-for="txtMILESTONE_DESCRIPTION" id="spanMILESTONE_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">C&oacute;digo de hito
                            <input type="text" id="txtMILESTONE_CODE" placeholder="p. ej. C&oacute;digo de hito" value="" title="Los c&oacute;digos de hito pueden usarse para seguir eventos espec&iacute;ficos que toman lugar para un tipo particular de proceso de contrataciones. Por ejemplo, un c&oacute;digo de &#39;approvalLetter&#39; puede usarse para permitir a las aplicaciones entender que el hito representa una fecha cuando el approvalLetter debe entregarse o firmarse."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_CODE" id="spanMILESTONE_CODE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha l&iacute;mite
                            <input type="date" id="txtMILESTONE_DUE_DATE" value="" pattern="fecha" title="La fecha l&iacute;mite del hito."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_DUE_DATE" id="spanMILESTONE_DUE_DATE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de cumplimiento
                            <input type="date" id="txtMILESTONE_DATE_MET" value="" pattern="fecha" title="La fecha en que el hito se cumpli&oacute;."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_DATE_MET" id="spanMILESTONE_DATE_MET">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de modificaci&oacute;n
                            <input type="date" id="txtMILESTONE_DATE_MODIFIED" value="" pattern="fecha" title="La fecha en que el hito fue revisado o modificado y se cambi&oacute; el estado o se confirm&oacute; que continuaba siendo correcto."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_DATE_MODIFIED" id="spanMILESTONE_DATE_MODIFIED">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Estado
                            <select id="cmbMILESTONE_STATUS" title="El estatus que se realiz&oacute; en la fecha en &#39;dateModified&#39;.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${estadosHito}" var="estado">
                                    <option value="${estado.clave}" >${estado.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbMILESTONE_STATUS" id="spanMILESTONE_STATUS">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-milestone"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-milestone"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un hito (BOTTOM) --%>

<script>
                
    function validarFormulario(idFormulario)
    { 
        
        let strMensajeFrm;          
        let strTituloMensaje;       
        let bolTipoFormulario;      

        bolTipoFormulario = document.getElementById("txtEsFormularioDeEdicion").value;
        bolTipoFormulario = Boolean(bolTipoFormulario);
        strMensajeFrm = (bolTipoFormulario === true) ? "\u00bfLos datos son correctos\u003F" : "Una vez guardada la informaci\u00F3n de esta secci\u00F3n no se podr\u00E1 volver a modificar los valores ingresados hasta que se finalice con el proceso de registro de la contrataci\u00F3n p\u00FAblica.";
        strTituloMensaje = (bolTipoFormulario === true) ? "Confirmar" : "\u00bfLos datos son correctos\u003F";

        swal({
            title: strTituloMensaje,
            text: strMensajeFrm,
            icon: "warning",
            buttons: ["Cancelar", "Aceptar"],
            dangerMode: true,
            closeOnClickOutside: false,
            closeOnEsc: false
        })
        .then((seAcepta) => {
            if (seAcepta)
            { 
                document.getElementById(idFormulario).submit();
            } 
            else
            { 
                swal("Se ha cancelado la operaci\u00f3n", {
                    icon: "error",
                    buttons: false,
                    timer: 2000
                });
                return false;
            } 
        });
                    
    } 
    
    window.onload = function (){
        
        document.getElementById("estructura-por-defecto-milestone").value = obtenerDatosInicialesElementos("milestone", []);
        
    };
    
</script> 
