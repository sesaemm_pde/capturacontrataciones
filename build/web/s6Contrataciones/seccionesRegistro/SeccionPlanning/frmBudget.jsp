<%-- 
    Autor: Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<div id="seccion6-bloque2">
    <h4>Presupuesto</h4>
    <div class="callout padding-horizontal-3">
        <form name="frmRegistrarSeccionPlaneacionBloquePresupuesto" id="frmRegistrarSeccionPlaneacionBloquePresupuesto" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post" data-abide novalidate onSubmit="event.preventDefault(); validarFormularioPlaneacionPresupuesto(this.id);" autocomplete="off">
            <input type="hidden" name="accion" value="${(bolEsFormulariosDeEdicion == true) ? 'editarSeccion' : 'registrarSeccion'}"/>
            <input type="hidden" name="txtEsFormularioDeEdicion" id="txtEsFormularioDeEdicion" value="${bolEsFormulariosDeEdicion}"/>
            <input type="hidden" name="txtNUMERO_SECCION" value="6"/>
            <input type="hidden" name="txtNUMERO_BLOQUE" value="2"/>
            <input type="hidden" name="txtID_CONTRATACION_SPDN" value="${idContratacion}"/>
            <fieldset class="fieldset">
                <div class="grid-margin-x grid-x">
                    <label class="cell medium-4">ID
                        <input type="text" name="txtID" id="txtID" placeholder="p. ej. 1" value="${contratacion.planning.budget.id}" title="Un identificador para la partida presupuestaria que provee fondos para este proceso de contrataci&oacute;n. Este identificador debe de poder hacer una referencia cruzada contra la fuente de datos."/>
                        <span class="form-error" data-form-error-for="txtID" id="spanID">
                            Campo requerido, verifica el formato de tu registro.
                        </span>
                    </label>
                    <label class="cell medium-4">Identificador de proyecto
                        <input type="text" name="txtPROJECT_ID" id="txtPROJECT_ID" placeholder="p. ej. 1" value="${contratacion.planning.budget.projectID}" title="Un identificador externo para el proyecto del que forma parte este proceso de contrataci&oacute;n o a trav&eacute;s del cual est&aacute; financiado (si aplica). Algunas organizaciones mantienen un registro de proyectos y los datos deben usar el identificador del registro de proyectos relevante."/>
                        <span class="form-error" data-form-error-for="txtPROJECT_ID" id="spanPROJECT_ID">
                            Campo requerido, verifica el formato de tu registro.
                        </span>
                    </label>
                    <label class="cell medium-4">T&iacute;tulo del proyecto
                        <input type="text" name="txtPROJECT" id="txtPROJECT" placeholder="p. ej. T&iacute;tulo del proyecto" value="${contratacion.planning.budget.project}" title="El nombre del proyecto a trav&eacute;s del cual el proceso de contrataciones est&aacute; financiado (si aplica). Algunas organizaciones mantienen un registro de proyectos, y los datos deben de usar el nombre por el cual se conoce al proyecto en ese registro. No se ofrece una opci&oacute;n de traducci&oacute;n para esta cadena, ya que los valores traducidos pueden darse en datos de terceros, ligados a la fuente de datos anteriormente presentada."/>
                        <span class="form-error" data-form-error-for="txtPROJECT" id="spanPROJECT">
                            Campo requerido, verifica el formato de tu registro.
                        </span>
                    </label>
                    <label class="cell medium-4">Fuente del Presupuesto
                        <input type="text" name="txtDESCRIPTION" id="txtDESCRIPTION" placeholder="p. ej. Descripci&oacute;n breve de la fuente del presupuesto" value="${contratacion.planning.budget.description}" title="Un texto descriptivo breve de la fuente del presupuesto. Puede usarse para proveer el t&iacute;tulo del la partida presupuestaria o el programa usado para financiar este proyecto."/>
                        <span class="form-error" data-form-error-for="txtDESCRIPTION" id="spanDESCRIPTION">
                            Campo requerido, verifica el formato de tu registro.
                        </span>
                    </label>
                    <label class="cell medium-4">Informac&oacute;n vinculada de presupuesto
                        <input type="text" name="txtURI" id="txtURI" placeholder="p. ej. www.url.com" value="${contratacion.planning.budget.uri}" pattern="url" title="Una URI que apunta a un registro le&iacute;ble por m&aacute;quina sobre la l&iacute;nea (o l&iacute;neas) de presupuesto que financian este proceso de contrataciones. Se puede dar informaci&oacute;n en un rango de formatos, incluyendo IATI, el Est&aacute;ndar de Datos Fiscales Abiertos o cualquier otro est&aacute;ndar que d&eacute; datos estructurados sobre fuentes de presupuesto. Los documentos le&iacute;bles por humanos pueden incluirse utilizando el bloque planning.documents."/>
                        <span class="form-error" data-form-error-for="txtURI" id="spanURI">
                            Campo requerido, verifica el formato de tu registro.
                        </span>
                    </label>
                    <label class="cell medium-4">Fuente de los Datos
                        <input type="text" name="txtSOURCE" id="txtURI" placeholder="p. ej. www.url.com" value="${contratacion.planning.budget.source}" pattern="url" title="Usado para apuntar a un Paquete de Datos de Presupuesto, o una fuente legible por computadora o por un humano donde los usuarios puedan obtener m�s informaci&oacute;n sobre los identificadores de partida presupuestaria o identificadores de proyectos, provistos aqu&iacute;."/>
                        <span class="form-error" data-form-error-for="txtSOURCE" id="spanSOURCE">
                            Campo requerido, verifica el formato de tu registro.
                        </span>
                    </label>
                    <fieldset class="fieldset cell">
                        <legend>Monto</legend>
                        <div class="grid-margin-x grid-x">
                            <label class="cell medium-6">Monto
                                <input type="number" name="txtAMOUNT" id="txtAMOUNT" placeholder="p. ej. 0" min="0" value="${contratacion.planning.budget.amount.amount}" title="Monto como una cifra."/>
                                <span class="form-error" data-form-error-for="txtAMOUNT" id="spanAMOUNT">
                                    Campo requerido, verifica el formato de tu registro.
                                </span>
                            </label>
                            <label class="cell medium-6">Moneda
                                <select name="txtCURRENCY" id="txtCURRENCY" title="La moneda del monto, de la lista de c&oacute;digo cerrada.">
                                    <option value="">--Elige una opci&oacute;n--</option>
                                    <c:forEach items="${tipoMoneda}" var="moneda">
                                        <option value="${moneda.clave}" ${contratacion.planning.budget.amount.currency == moneda.clave ? 'selected': ''} >${moneda.valor}</option>
                                    </c:forEach>
                                </select>
                                <span class="form-error" data-form-error-for="txtCURRENCY" id="spanCURRENCY">
                                    Elige una opci&oacute;n.
                                </span>
                            </label>
                        </div>
                    </fieldset>
                </div>
            </fieldset>
            <div class="grid-x grid-margin-x">
                <fieldset class="cell medium-6">
                    <input type="reset" class="button expanded secondary" name="btnResetPlaneacionPresupuesto" value="${(bolEsFormulariosDeEdicion == true) ? 'Restaurar' : 'Reiniciar'}"/>
                </fieldset>
                <fieldset class="cell medium-6">
                    <input type="submit" class="button expanded" name="btnRegistrarPlaneacionPresupuesto" value="${(bolEsFormulariosDeEdicion == true) ? 'Modificar datos' : 'Continuar'}"/>
                </fieldset>
            </div>
        </form>
        <div data-abide-error="data-abide-error" class="alert callout alertFooter" style="display: none;">
            <p>
                <i class="material-icons">warning</i>
                Existen algunos errores en tu registro del bloque 'Presupuesto' perteneciente a la secci&oacute;n 'Planeaci&oacute;n'. Verifica nuevamente.
            </p>
        </div>
    </div>
    
</div>
            
<script>
                
    function validarFormularioPlaneacionPresupuesto(idFormulario)
    {
        
        $("#frmRegistrarSeccionPlaneacionBloquePresupuesto").on("formvalid.zf.abide", function (ev, frm){
            
            let strMensajeFrm;
            let strTituloMensaje;
            let bolTipoFormulario;
            
            bolTipoFormulario = document.getElementById("txtEsFormularioDeEdicion").value;
            bolTipoFormulario = Boolean(bolTipoFormulario);
            strMensajeFrm = (bolTipoFormulario === true) ? "\u00bf Los datos son correctos\u003F" : "Una vez guardada la informaci\u00F3n de esta secci\u00F3n no se podr\u00E1 volver a modificar los valores ingresados hasta que se finalice con el proceso de registro de la contrataci\u00F3n p\u00FAblica.";
            strTituloMensaje = (bolTipoFormulario === true) ? "Confirmar" : "\u00bf Los datos son correctos\u003F";
            
            $(".alertFooter").css("display", "none");
            
            swal({
                title: strTituloMensaje,
                text: strMensajeFrm,
                icon: "warning",
                buttons: ["Cancelar", "Aceptar"],
                dangerMode: true,
                closeOnClickOutside: false,
                closeOnEsc: false
            })
            .then((seAcepta) => {
                if (seAcepta)
                {
                    document.getElementById(idFormulario).submit();
                }
                else
                {
                    swal("Se ha cancelado la operaci\u00f3n", {
                        icon: "error",
                        buttons: false,
                        timer: 2000
                    });
                    return false;
                }
            });
            
        })
        .on("forminvalid.zf.abide", function(ev,frm) {
            $(".alertFooter").css("display", "block");
        });
                    
    }

</script> 
