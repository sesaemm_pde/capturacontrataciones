<%-- 
    Autor: Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<div id="seccion1">
    <h3>Datos generales</h3>
    <div class="callout padding-horizontal-3">
        <form name="frmRegistrarSeccionDatosGenerales" id="frmRegistrarSeccionDatosGenerales" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post" data-abide novalidate onSubmit="event.preventDefault(); validarFormularioDatosGenerales(this.id);" autocomplete="off">
            <input type="hidden" name="accion" value="${(bolEsFormulariosDeEdicion == true) ? 'editarSeccion' : 'registrarSeccion'}"/>
            <input type="hidden" name="txtEsFormularioDeEdicion" id="txtEsFormularioDeEdicion" value="${bolEsFormulariosDeEdicion}"/>
            <input type="hidden" name="txtNUMERO_SECCION" value="1"/>
            <input type="hidden" name="txtID_CONTRATACION_SPDN" value="${idContratacion}"/>
            <fieldset class="margin-vertical-2">
                <div class="grid-margin-x grid-x">
                    <label class="cell medium-4">Open Contracting ID <span style="color:red;">*</span>
                        <input type="text" name="txtID_OCID" id="txtID_OCID" placeholder="p. ej. Open Contracting ID" value="${contratacion.ocid}" required title="Un identificador &uacute;nico global para este proceso de Contrataciones Abiertas. Se compone de un prefijo ocid y un identificador para el proceso de contrataciones. Para m&aacute;s informaci&oacute;n, vea la [Gu&iacute;a de Identificadores de Contrataciones Abiertas]"/>
                        <span class="form-error" data-form-error-for="txtID_OCID">
                            Campo requerido, verifica el formato de tu registro.
                        </span>
                    </label>
                    <label class="cell medium-4">ID de entrega <span style="color:red;">*</span>
                        <input type="text" name="txtID" id="txtID" placeholder="p. ej. ID de Entrega" value="${contratacion.id}" required title="Un identificador para esta entrega de informaci&oacute;n particular. Un identificador de entrega debe ser &uacute;nico en el &aacute;mbito del proceso de contrataciones relacionado (definido por un ocid com&uacute;n). Un identificador de entrega no debe contener el caracter #."/>
                        <span class="form-error" data-form-error-for="txtID">
                            Campo requerido, verifica el formato de tu registro.
                        </span>
                    </label>
                    <label class="cell medium-4">Ciclo <span style="color:red;">*</span>
                        <input type="number" min="2000" name="txtCYCLE" id="txtCYCLE" placeholder="p. ej. 2021" value="${contratacion.cycle}" required title="A&ntilde;o y/o Ejercicio fiscal"/>
                        <span class="form-error" data-form-error-for="txtCYCLE">
                            Campo requerido, verifica el formato de tu registro.
                        </span>
                    </label>
                    <fieldset class="cell medium-4">
                        <legend style="font-size: 14px;" title="El tipo de proceso de iniciaci&oacute;n utilizado para este contrato, de la lista de c&oacute;digo cerrada.">Tipo de inicio <span style="color:red;">*</span></legend>
                        <c:forEach items="${tiposDeInicio}" var="tipoInicio">
                            <label class="text-truncate">
                                <input type="checkbox" name="chckINITIATION_TYPE" required value="${tipoInicio.clave}" ${contratacion.initiationType.equals(tipoInicio.clave) ? 'checked' : ''} />
                                ${tipoInicio.valor}
                            </label>
                        </c:forEach>
                        <span class="form-error" id="spanINITIATION_TYPE">
                            Campo requerido, selecciona al menos una opci&oacute;n.
                        </span>
                    </fieldset>
                    <label class="cell medium-4">Fecha de entrega <span style="color:red;">*</span>
                        <input type="date" name="txtDATE" required id="txtDATE" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(contratacion.date)}" title="Fecha en la cual esta informaci&oacute;n se public&oacute; por primera vez."/>
                        <span class="form-error" data-form-error-for="txtDATE">
                            Campo requerido, verifica el formato de tu registro.
                        </span>
                    </label>
                    <label class="cell medium-4">Idioma de la entrega
                        <select name="cmbLANGUAGE" id="cmbLANGUAGE" title="El idioma predeterminado de los datos con dos letras [ISO639-1].">
                            <option value="">--Elige una opci&oacute;n--</option>
                            <c:forEach items="${tiposDeIdioma}" var="idioma">
                                <option value="${idioma.codigo}" ${contratacion.language == idioma.codigo ? 'selected': ''} >${idioma.nombre}</option>
                            </c:forEach>
                        </select>
                        <span class="form-error" data-form-error-for="cmbLANGUAGE">
                            Elige una opci&oacute;n.
                        </span>
                    </label>
                </div>
            </fieldset>
            <div class="grid-x grid-margin-x">
                <fieldset class="cell medium-6">
                    <input type="reset" class="button expanded secondary" name="btnResetDatosGenerales" value="${(bolEsFormulariosDeEdicion == true) ? 'Restaurar' : 'Reiniciar'}"/>
                </fieldset>
                <fieldset class="cell medium-6">
                    <input type="submit" class="button expanded" name="btnRegistrarDatosGenerales" value="${(bolEsFormulariosDeEdicion == true) ? 'Modificar datos' : 'Continuar'}"/>
                </fieldset>
            </div>
        </form>
        <div data-abide-error="data-abide-error" class="alert callout alertFooter" style="display: none;">
            <p>
                <i class="material-icons">warning</i>
                Existen algunos errores en tu registro de la secci&oacute;n 'Datos generales'. Verifica nuevamente.
            </p>
        </div>
    </div>
    
</div>

<script>
                
    function validarFormularioDatosGenerales (idFormulario)
    { 
        
        $("#frmRegistrarSeccionDatosGenerales").on("formvalid.zf.abide", function (ev, frm){
            
            let strMensajeFrm;          
            let strTituloMensaje;       
            let bolTipoFormulario;      
            
            bolTipoFormulario = document.getElementById("txtEsFormularioDeEdicion").value;
            bolTipoFormulario = Boolean(bolTipoFormulario);
            strMensajeFrm = (bolTipoFormulario === true) ? "\u00bf Los datos son correctos\u003F" : "Una vez guardada la informaci\u00F3n de esta secci\u00F3n no se podr\u00E1 volver a modificar los valores ingresados hasta que se finalice con el proceso de registro de la contrataci\u00F3n p\u00FAblica.";
            strTituloMensaje = (bolTipoFormulario === true) ? "Confirmar" : "\u00bf Los datos son correctos\u003F";
            
            $(".alertFooter").css("display", "none");
        
            swal({
                title: strTituloMensaje,
                text: strMensajeFrm,
                icon: "warning",
                buttons: ["Cancelar", "Aceptar"],
                dangerMode: true,
                closeOnClickOutside: false,
                closeOnEsc: false
            })
            .then((seAcepta) => {
                if (seAcepta)
                { 
                    document.getElementById(idFormulario).submit();
                } 
                else
                { 
                    swal("Se ha cancelado la operaci\u00f3n", {
                        icon: "error",
                        buttons: false,
                        timer: 2000
                    });
                    return false;
                } 
            });
            
        })
        .on("forminvalid.zf.abide", function(ev,frm) {
            $(".alertFooter").css("display", "block");
        });
              
    }; 
       
</script>       