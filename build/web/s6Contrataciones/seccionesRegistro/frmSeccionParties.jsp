<%-- 
    Autor: Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="seccion4">
    <h3>Partes involucradas</h3>
    <div class="button-group">
        <button type="button" class="button" onclick="abrirNuevoFormulario('parties', []);">Agregar parte involucrada</button>
    </div>
    <div class="callout padding-horizontal-3">
        <form name="frmRegistrarSeccionPartesInvolucradas" id="frmRegistrarSeccionPartesInvolucradas" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post" data-abide novalidate onSubmit="return false;" autocomplete="off">
            <input type="hidden" name="accion" value="${(bolEsFormulariosDeEdicion == true) ? 'editarSeccion' : 'registrarSeccion'}"/>
            <input type="hidden" name="txtEsFormularioDeEdicion" id="txtEsFormularioDeEdicion" value="${bolEsFormulariosDeEdicion}"/>
            <input type="hidden" name="txtNUMERO_SECCION" value="4"/>
            <input type="hidden" name="txtID_CONTRATACION_SPDN" value="${idContratacion}"/>
            <input type="hidden" id="estructura-por-defecto-parties" value="">
            <input type="hidden" id="siguiente-identificador-parties" value="" data-valor-por-defecto="">
            <input type="hidden" name="lista-identificadores-por-registrar-parties" id="lista-identificadores-por-registrar-parties" value="" data-valor-por-defecto="">
            <c:choose>
                <c:when test="${contratacion.parties.size() > 0}">
                    <c:set var="contadorElementosParties" value="0"></c:set>
                    <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-parties">
                    <c:forEach items="${contratacion.parties}" var="parties">
                        <li class="accordion-item" data-accordion-item id="parties_${contadorElementosParties}">
                            <a href="#" class="accordion-title"><span style="text-transform: capitalize;">parte involucrada</span> - <span id="titulo-elemento-parties_${contadorElementosParties}">${parties.id}</span></a>
                            <div class="accordion-content" data-tab-content>
                                <div class="grid-margin-x grid-x">
                                    <div class="cell padding-horizontal-1" style="display:flex; justify-content: flex-end; gap: 10px; flex-wrap: wrap;">
                                        <button class="button margin-0 btnFrmContrataciones" onclick="abrirFormularioPrevio('parties', [${contadorElementosParties}])"><i class="material-icons">edit</i> Editar parte involucrada</button>
                                        <button class="button margin-0 btnFrmContrataciones" onclick="eliminarElemento('parties', 'parties_${contadorElementosParties}', [], '${contadorElementosParties}')"><i class="material-icons">remove_circle</i> Eliminar parte involucrada</button>
                                    </div>
                                    <div id="datos-elemento-parties_${contadorElementosParties}">
                                        <input type="hidden" name="txtPARTIES_ID_${contadorElementosParties}" id="txtPARTIES_ID_${contadorElementosParties}" value="${parties.id}">
                                        <input type="hidden" name="txtPARTIES_NAME_${contadorElementosParties}" id="txtPARTIES_NAME_${contadorElementosParties}" value="${parties.name}">
                                        <input type="hidden" name="txtPARTIES_IDENTIFIER_ID_${contadorElementosParties}" id="txtPARTIES_IDENTIFIER_ID_${contadorElementosParties}" value="${parties.identifier.id}">
                                        <input type="hidden" name="txtPARTIES_IDENTIFIER_SCHEME_${contadorElementosParties}" id="txtPARTIES_IDENTIFIER_SCHEME_${contadorElementosParties}" value="${parties.identifier.scheme}">
                                        <input type="hidden" name="txtPARTIES_IDENTIFIER_LEGAL_NAME_${contadorElementosParties}" id="txtPARTIES_IDENTIFIER_LEGAL_NAME_${contadorElementosParties}" value="${parties.identifier.legalName}">
                                        <input type="hidden" name="txtPARTIES_IDENTIFIER_URI_${contadorElementosParties}" id="txtPARTIES_IDENTIFIER_URI_${contadorElementosParties}" value="${parties.identifier.uri}">
                                        <input type="hidden" name="txtPARTIES_ADDRESS_STREET_ADDRESS_${contadorElementosParties}" id="txtPARTIES_ADDRESS_STREET_ADDRESS_${contadorElementosParties}" value="${parties.address.streetAddress}">
                                        <input type="hidden" name="txtPARTIES_ADDRESS_LOCALITY_${contadorElementosParties}" id="txtPARTIES_ADDRESS_LOCALITY_${contadorElementosParties}" value="${parties.address.locality}">
                                        <input type="hidden" name="txtPARTIES_ADDRESS_REGION_${contadorElementosParties}" id="txtPARTIES_ADDRESS_REGION_${contadorElementosParties}" value="${parties.address.region}">
                                        <input type="hidden" name="txtPARTIES_ADDRESS_POSTAL_CODE_${contadorElementosParties}" id="txtPARTIES_ADDRESS_POSTAL_CODE_${contadorElementosParties}" value="${parties.address.postalCode}">
                                        <input type="hidden" name="txtPARTIES_ADDRESS_COUNTRY_NAME_${contadorElementosParties}" id="txtPARTIES_ADDRESS_COUNTRY_NAME_${contadorElementosParties}" value="${parties.address.countryName}">
                                        <input type="hidden" name="txtPARTIES_CONTACT_NAME_${contadorElementosParties}" id="txtPARTIES_CONTACT_NAME_${contadorElementosParties}" value="${parties.contactPoint.name}">
                                        <input type="hidden" name="txtPARTIES_CONTACT_EMAIL_${contadorElementosParties}" id="txtPARTIES_CONTACT_EMAIL_${contadorElementosParties}" value="${parties.contactPoint.email}">
                                        <input type="hidden" name="txtPARTIES_CONTACT_TELEPHONE_${contadorElementosParties}" id="txtPARTIES_CONTACT_TELEPHONE_${contadorElementosParties}" value="${parties.contactPoint.telephone}">
                                        <input type="hidden" name="txtPARTIES_CONTACT_FAXNUMBER_${contadorElementosParties}" id="txtPARTIES_CONTACT_FAXNUMBER_${contadorElementosParties}" value="${parties.contactPoint.faxNumber}">
                                        <input type="hidden" name="txtPARTIES_CONTACT_URL_${contadorElementosParties}" id="txtPARTIES_CONTACT_URL_${contadorElementosParties}" value="${parties.contactPoint.url}">
                                        <c:set var="valorPartiesRoles" value=""></c:set>
                                        <c:forEach items="${parties.roles}" var="rol">
                                            <c:if test="${rol != null}">
                                                <c:set var="valorPartiesRoles" value="${valorPartiesRoles.concat(rol).concat(',')}"></c:set>
                                            </c:if>
                                        </c:forEach>
                                        <input type="hidden" name="chckPARTIES_ROLES_${contadorElementosParties}" id="chckPARTIES_ROLES_${contadorElementosParties}" value="${valorPartiesRoles}">
                                        <input type="hidden" name="radPARTIES_DETAILS_SCALE_${contadorElementosParties}" id="radPARTIES_DETAILS_SCALE_${contadorElementosParties}" value="${parties.details.scale}">
                                    </div>
                                    <fieldset class="cell fieldset"> 
                                        <input type="hidden" id="estructura-por-defecto-identifier_${contadorElementosParties}" value=""> 
                                        <input type="hidden" id="siguiente-identificador-identifier_${contadorElementosParties}" value="" data-valor-por-defecto=""> 
                                        <input type="hidden" id="lista-identificadores-por-registrar-identifier_${contadorElementosParties}" name="lista-identificadores-por-registrar-identifier_${contadorElementosParties}" value="" data-valor-por-defecto="">
                                        <legend title="Una lista adicional/suplementaria de identificadores para la organizaci�n, utilizando la [gu�a de identificadores de organizaciones]. Esto puede usarse para dar un identificador utilizado internamente por esta organizaci�n adem�s del identificador primario legal de la entidad.">Identificadores adicionales</legend>
                                        <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                            <a onclick="restaurarEstadoInicialElemento('identifier', [${contadorElementosParties}])"><i class="material-icons secondary">restart_alt</i></a> 
                                            <a onclick="abrirNuevoFormulario('identifier', [${contadorElementosParties}])"><i class="material-icons secondary">add_circle</i></a> 
                                        </div>
                                        <c:choose>
                                            <c:when test="${parties.additionalIdentifiers.size() > 0}">
                                                <c:set var="contadorElementosIdentifier" value="0"></c:set>
                                                <div id="contenedor-identifier_${contadorElementosParties}" style="display: flex; flex-direction: column; gap: 10px;">
                                                <c:forEach items="${parties.additionalIdentifiers}" var="identifier">
                                                    <div id="identifier_${contadorElementosParties}_${contadorElementosIdentifier}">
                                                        <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                                            <div class="cell medium-8">
                                                                <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">identificador</span> - <span id="titulo-elemento-identifier_${contadorElementosParties}_${contadorElementosIdentifier}">${identifier.id}</span></h6>
                                                            </div>
                                                            <div id="datos-elemento-identifier_${contadorElementosParties}_${contadorElementosIdentifier}">
                                                                <input type="hidden" name="txtIDENTIFIER_ID_${contadorElementosParties}_${contadorElementosIdentifier}" id="txtIDENTIFIER_ID_${contadorElementosParties}_${contadorElementosIdentifier}" value="${identifier.id}" >
                                                                <input type="hidden" name="txtIDENTIFIER_SCHEME_${contadorElementosParties}_${contadorElementosIdentifier}" id="txtIDENTIFIER_SCHEME_${contadorElementosParties}_${contadorElementosIdentifier}" value="${identifier.scheme}" >
                                                                <input type="hidden" name="txtIDENTIFIER_LEGAL_NAME_${contadorElementosParties}_${contadorElementosIdentifier}" id="txtIDENTIFIER_LEGAL_NAME_${contadorElementosParties}_${contadorElementosIdentifier}" value="${identifier.legalName}" >
                                                                <input type="hidden" name="txtIDENTIFIER_URI_${contadorElementosParties}_${contadorElementosIdentifier}" id="txtIDENTIFIER_URI_${contadorElementosParties}_${contadorElementosIdentifier}" value="${identifier.uri}" >  
                                                            </div>
                                                            <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                                <button class="button margin-0" onclick="abrirFormularioPrevio('identifier', [${contadorElementosParties}, ${contadorElementosIdentifier}])"><i class="material-icons">edit</i> Editar</button>
                                                                <button class="button margin-0" onclick="eliminarElemento('identifier', 'identifier_${contadorElementosParties}_${contadorElementosIdentifier}', [${contadorElementosParties}], '${contadorElementosIdentifier}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <c:set var="contadorElementosIdentifier" value="${contadorElementosIdentifier + 1}"></c:set>
                                                </c:forEach>
                                                </div>
                                                <input type="hidden" name="ultimo-identificador-elementos-identifier_${contadorElementosParties}" id="ultimo-identificador-elementos-identifier_${contadorElementosParties}" value="${contadorElementosIdentifier}">
                                            </c:when>
                                            <c:otherwise>
                                                <div id="contenedor-identifier_${contadorElementosParties}" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                                <input type="hidden" name="ultimo-identificador-elementos-identifier_${contadorElementosParties}" id="ultimo-identificador-elementos-identifier_${contadorElementosParties}" value="0">
                                            </c:otherwise>
                                        </c:choose>
                                        <script>
                                            document.getElementById("estructura-por-defecto-identifier_${contadorElementosParties}").value = obtenerDatosInicialesElementos("identifier", [${contadorElementosParties}]);
                                        </script>
                                    </fieldset>
                                </div>
                            </div>
                        </li>
                        <c:set var="contadorElementosParties" value="${contadorElementosParties + 1}"></c:set>
                    </c:forEach>
                    </ul>
                    <input type="hidden" name="ultimo-identificador-elementos-parties" id="ultimo-identificador-elementos-parties" value="${contadorElementosParties}">
                </c:when>
                <c:otherwise>
                    <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-parties"></ul>
                    <input type="hidden" name="ultimo-identificador-elementos-parties" id="ultimo-identificador-elementos-parties" value="0">
                </c:otherwise>
            </c:choose>   
            <div class="grid-x grid-margin-x margin-top-1">
                <fieldset class="cell medium-6">
                    <input type="button" class="button expanded secondary" onclick="restaurarEstadoInicialElemento('parties', []);" value="${(bolEsFormulariosDeEdicion == true) ? 'Restaurar' : 'Reiniciar'}"/>
                </fieldset>
                <fieldset class="cell medium-6">
                    <input type="button" onclick="validarFormulario('frmRegistrarSeccionPartesInvolucradas');" class="button expanded" id="btnRegistrarValoresFormulario" value="${(bolEsFormulariosDeEdicion == true) ? 'Modificar datos' : 'Continuar'}"/>
                </fieldset>
            </div>
        </form>
    </div>
</div>
       
<%-- formulario de una parte involucrada (TOP) --%>
<div id="modalFrmParties" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Parte involucrada</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmParties" id="frmParties" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-parties" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-6">ID de Entidad <span style="color: red">**</span>
                            <input type="text" id="txtPARTIES_ID" required placeholder="p. ej. 1" value="" title="El ID utilizado para hacer referencia a esta parte involucrada desde otras secciones de la entrega. Este campo puede construirse con la siguiente estructura {identifier.scheme}-{identifier.id}(-{department-identifier})."/>
                            <span class="form-error" data-form-error-for="txtPARTIES_ID" id="spanPARTIES_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Nombre
                            <input type="text" id="txtPARTIES_NAME" placeholder="p. ej. Nombre com&uacute;n" value="" title="Un nombre com&uacute;n para esta organizaci&oacute;n u otro participante en el proceso de contrataciones. El objeto identificador da un espacio para un nombre legal formal, y esto podr&iacute;a repetir el valor o dar un nombre com&uacute;n por el cual se conoce a la organizaci&oacute;n o entidad. Este campo tambi&eacute;n pude incluir detalles del departamento o sub-unidad involucrada en este proceso de contrataciones."/>
                            <span class="form-error" data-form-error-for="txtPARTIES_NAME" id="spanNAME">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <fieldset class="cell fieldset">
                            <legend title="El identificador primario para esta organizaci&oacute;n o participante. Son preferibles los identificadores que denotan de forma &uacute;nica a una entidad legal. Consulta la [gu&iacute;a de identificadores de organizaci&oacute;n] para el esquema e identificador preferido.">Identificador principal</legend>
                            <div class="grid-margin-x grid-x">
                                <label class="cell medium-6">ID
                                    <input type="text" id="txtPARTIES_IDENTIFIER_ID" placeholder="p. ej. 1" value="" title="El identificador de la organizaci&oacute;n en el esquema seleccionado."/>
                                    <span class="form-error" data-form-error-for="txtPARTIES_IDENTIFIER_ID" id="spanPARTIES_IDENTIFIER_ID">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Esquema
                                    <input type="text" id="txtPARTIES_IDENTIFIER_SCHEME" placeholder="p. ej. Esquema" value="" title="Los identificadores de organizaci&oacute;n deben de obtenerse de una lista de identificadores de organizaciones existentes. El campo scheme se usa para indicar o registrar de d&oacute;nde se toma el identificador. Este valor deber&iacute;a tomarse del [Esquema de Identificadores de Organizaciones]."/>
                                    <span class="form-error" data-form-error-for="txtPARTIES_IDENTIFIER_SCHEME" id="spanPARTIES_IDENTIFIER_SCHEME">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Nombre Legal
                                    <input type="text" id="txtPARTIES_IDENTIFIER_LEGAL_NAME" placeholder="p. ej. Nombre Legal" value="" title="El nombre legalmente registrado de la organizaci&oacute;n." />
                                    <span class="form-error" data-form-error-for="txtPARTIES_IDENTIFIER_LEGAL_NAME" id="spanPARTIES_IDENTIFIER_LEGAL_NAME">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">URI
                                    <input type="text" id="txtPARTIES_IDENTIFIER_URI" placeholder="p. ej. www.url.com" value="" pattern="url" title="Una URI para identificar a la organizaci&oacute;n, como los prove&iacute;dos por [Open Corporates] o alg&uacute;n otro proveedor relevante de URIs. Este campo no debe ser utilizado para especificar el sitio web de la organizaci&oacute;n, el cual puede ser especificado en el campo URL del punto de contacto de la Organizaci&oacute;n."/>
                                    <span class="form-error" data-form-error-for="txtPARTIES_IDENTIFIER_URI" id="spanPARTIES_IDENTIFIER_URI">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="cell fieldset">
                            <legend title="Una direcci&oacute;n. Esta puede ser la direcci&oacute;n legalmente registrada de la organizaci&oacute;n o puede ser una direcci&oacute;n donde se reciba correspondencia para este proceso de contrataci&oacute;n particular.">Direcci&oacute;n</legend>
                            <div class="grid-margin-x grid-x">
                                <label class="cell medium-4">Direcci&oacute;n
                                    <input type="text" id="txtPARTIES_ADDRESS_STREET_ADDRESS" placeholder="p. ej. 1600 Amphitheatre Pkwy." value="" title="La direcci&oacute;n de la calle."/>
                                    <span class="form-error" data-form-error-for="txtPARTIES_ADDRESS_STREET_ADDRESS" id="spanPARTIES_ADDRESS_STREET_ADDRESS">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Localidad
                                    <input type="text" id="txtPARTIES_ADDRESS_LOCALITY" placeholder="p. ej. Mountain View." value="" title="La localidad." />
                                    <span class="form-error" data-form-error-for="txtPARTIES_ADDRESS_LOCALITY" id="spanPARTIES_ADDRESS_LOCALITY">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Regi&oacute;n
                                    <input type="text" id="txtPARTIES_ADDRESS_REGION" placeholder="p. ej. CA." value="" title="La regi&oacute;n."/>
                                    <span class="form-error" data-form-error-for="txtPARTIES_ADDRESS_REGION" id="spanPARTIES_ADDRESS_REGION">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">C&oacute;digo postal
                                    <input type="text" id="txtPARTIES_ADDRESS_POSTAL_CODE" placeholder="p. ej. 94043" value="" pattern="codigo_postal" title="El c&oacute;digo postal." />
                                    <span class="form-error" data-form-error-for="txtPARTIES_ADDRESS_POSTAL_CODE" id="spanPARTIES_ADDRESS_POSTAL_CODE">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Pa&iacute;s
                                    <input type="text" id="txtPARTIES_ADDRESS_COUNTRY_NAME" placeholder="p. ej. M&eacute;xico" value="" title="El nombre del pa&iacute;s." />
                                    <span class="form-error" data-form-error-for="txtPARTIES_ADDRESS_COUNTRY_NAME" id="spanPARTIES_ADDRESS_COUNTRY_NAME">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="cell fieldset">
                            <legend title="Detalles de contacto que pueden usarse para esta parte involucrada.">Punto de contacto</legend>
                            <div class="grid-margin-x grid-x">
                                <label class="cell medium-4">Nombre
                                    <input type="text" id="txtPARTIES_CONTACT_NAME" placeholder="p. ej. Carlos" value="" title="El nombre de la persona de contacto, departamento o punto de contacto en relaci&oacute;n a este proceso de contrataci&oacute;n."/>
                                    <span class="form-error" data-form-error-for="txtPARTIES_CONTACT_NAME" id="spanPARTIES_CONTACT_NAME">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Correo electr&oacute;nico
                                    <input type="text" id="txtPARTIES_CONTACT_EMAIL" placeholder="p. ej. email@gmail.com" value="" title="La direcci&oacute;n de correo del punto o persona de contacto."/>
                                    <span class="form-error" data-form-error-for="txtPARTIES_CONTACT_EMAIL" id="spanPARTIES_CONTACT_EMAIL">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Tel&eacute;fono
                                    <input type="text" id="txtPARTIES_CONTACT_TELEPHONE" placeholder="p. ej. 5555555555 o 555-555-5555" value="" title="El n&uacute;mero de tel&eacute;fono del punto o persona de contacto. Este debe de incluir el c&oacute;digo de marcaci&oacute;n internacional." />
                                    <span class="form-error" data-form-error-for="txtPARTIES_CONTACT_TELEPHONE" id="spanPARTIES_CONTACT_TELEPHONE">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">N&uacute;mero de fax
                                    <input type="text" id="txtPARTIES_CONTACT_FAXNUMBER" placeholder="p. ej. 5555555555 o 555-555-5555" value="" title="El n&uacute;mero de fax del punto o persona de contacto. Este debe de incluir el c&oacute;digo de marcaci&oacute;n internacional."/>
                                    <span class="form-error" data-form-error-for="txtPARTIES_CONTACT_FAXNUMBER" id="spanPARTIES_CONTACT_FAXNUMBER">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">URL
                                    <input type="text" id="txtPARTIES_CONTACT_URL" placeholder="p. ej. www.url.com" value="" pattern="url" title="Una direcci&oacute;n web para el punto o persona de contacto."/>
                                    <span class="form-error" data-form-error-for="txtPARTIES_CONTACT_URL" id="spanPARTIES_CONTACT_URL">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="cell medium-6 fieldset">
                            <legend title="El rol (o roles) de las partes involucradas en el proceso de contrataciones, utilizando la lista de c&oacute;digo abierta.">Roles de las partes</legend>
                            <div>
                                <c:forEach items="${rolesPartesInvolucradas}" var="rol">
                                    <label class="text-truncate">
                                        <input type="checkbox" name="chckPARTIES_ROLES" value="${rol.clave}"/>
                                        ${rol.valor}
                                    </label>
                                </c:forEach>
                                <span class="form-error" id="spanPARTIES_ROL">
                                    Campo requerido, selecciona al menos una opci&oacute;n.
                                </span>
                            </div>
                        </fieldset>
                        <fieldset class="cell medium-6 fieldset">
                            <legend title="Informaci&oacute;n adicional de clasificaci&oacute;n de las partes involucradas pueden ser prove&iacute;das usando las extensiones de partyDetail que definen propiedades particulares y esquemas de clasificaci&oacute;n.">Detalles</legend>
                            <div class="grid-margin-x grid-x">
                                <fieldset class="cell fieldset">
                                    <legend title="Para las organizaciones comerciales, es una entidad micro (micro), peque&ntilde;a o mediana empresa (sme) o grande (large) de acuerdo con las definiciones utilizadas por la entidad contratante o el comprador. Este campo puede dejarse en blanco si no tales conceptos se aplican.">Escala</legend>
                                    <c:forEach items="${escalasPartesInvolucradas}" var="escala">
                                        <label class="text-truncate">
                                            <input type="radio" name="radPARTIES_DETAILS_SCALE" value="${escala.clave}"/>
                                            ${escala.valor}
                                        </label>
                                    </c:forEach>
                                    <span class="form-error" id="spanPARTIES_DETAILS_SCALE">
                                        Campo requerido, selecciona al menos una opci&oacute;n.
                                    </span>
                                </fieldset>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-parties"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-parties"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de una parte involucrada (BOTTOM) --%>
<%-- formulario de un identificador (TOP) --%>
<div id="modalFrmIdentifier" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Identificador</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmIdentifier" id="frmIdentifier" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-identifier" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-6">ID <span style="color: red">**</span>
                            <input type="text" required id="txtIDENTIFIER_ID" placeholder="p. ej. 1" value="" title="El identificador de la organizaci&oacute;n en el esquema seleccionado."/>
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_ID" id="spanIDENTIFIER_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Esquema
                            <input type="text" id="txtIDENTIFIER_SCHEME" placeholder="p. ej. Esquema" value="" title="Los identificadores de organizaci&oacute;n deben de obtenerse de una lista de identificadores de organizaciones existentes. El campo scheme se usa para indicar o registrar de d&oacute;nde se toma el identificador. Este valor deber&iacute;a tomarse del [Esquema de Identificadores de Organizaciones]."/>
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_SCHEME" id="spanIDENTIFIER_SCHEME">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Nombre Legal
                            <input type="text" id="txtIDENTIFIER_LEGAL_NAME" placeholder="p. ej. Nombre Legal" value="" title="El nombre legalmente registrado de la organizaci&oacute;n." />
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_LEGAL_NAME" id="spanIDENTIFIER_LEGAL_NAME">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">URI
                            <input type="text" id="txtIDENTIFIER_URI" placeholder="p. ej. www.url.com" value="" pattern="url" title="Una URI para identificar a la organizaci&oacute;n, como los prove&iacute;dos por [Open Corporates] o alg&uacute;n otro proveedor relevante de URIs. Este campo no debe ser utilizado para especificar el sitio web de la organizaci&oacute;n, el cual puede ser especificado en el campo URL del punto de contacto de la Organizaci&oacute;n." />
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_URI" id="spanIDENTIFIER_URI">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-identifier"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-identifier"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un identificador (BOTTOM) --%>
            
<script>
                
    function validarFormulario(idFormulario)
    { 
        
        let strMensajeFrm;          
        let strTituloMensaje;       
        let bolTipoFormulario;      

        bolTipoFormulario = document.getElementById("txtEsFormularioDeEdicion").value;
        bolTipoFormulario = Boolean(bolTipoFormulario);
        strMensajeFrm = (bolTipoFormulario === true) ? "\u00bfLos datos son correctos\u003F" : "Una vez guardada la informaci\u00F3n de esta secci\u00F3n no se podr\u00E1 volver a modificar los valores ingresados hasta que se finalice con el proceso de registro de la contrataci\u00F3n p\u00FAblica.";
        strTituloMensaje = (bolTipoFormulario === true) ? "Confirmar" : "\u00bfLos datos son correctos\u003F";

        swal({
            title: strTituloMensaje,
            text: strMensajeFrm,
            icon: "warning",
            buttons: ["Cancelar", "Aceptar"],
            dangerMode: true,
            closeOnClickOutside: false,
            closeOnEsc: false
        })
        .then((seAcepta) => {
            if (seAcepta)
            { 
                document.getElementById(idFormulario).submit();
            } 
            else
            { 
                swal("Se ha cancelado la operaci\u00f3n", {
                    icon: "error",
                    buttons: false,
                    timer: 2000
                });
                return false;
            } 
        });
                    
    } 
    
    window.onload = function (){
        
        document.getElementById("estructura-por-defecto-parties").value = obtenerDatosInicialesElementos("parties", []);
        
    };
    
</script> 