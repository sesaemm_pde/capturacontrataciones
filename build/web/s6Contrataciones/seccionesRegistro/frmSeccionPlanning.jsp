<%-- 
    Autor: Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<div id="seccion6">
    
    <h3>Planeaci&oacute;n</h3>
    <div class="callout padding-horizontal-3">
        
        <c:if test="${bolEsFormulariosDeEdicion == true}">
            <div class="grid-x grid-margin-x">
                <div class="cell">
                    <ul class="menu-registro-contratacion" style="display:flex; margin-left: 0px;">
                        <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                            <div style="display: flex; justify-content: space-between; align-items: center;">
                                <button class="button margin-0 ${(bloqueConFoco == 1) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(bloqueConFoco == 1) ? '':'frmIrBloque1.submit()'}" title="Informaci&oacute;n general">Bloque - 1</button>
                            </div>
                        </li>
                        <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                            <div style="display: flex; justify-content: space-between; align-items: center;">
                                <button class="button margin-0 ${(bloqueConFoco == 2) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(bloqueConFoco == 2) ? '':'frmIrBloque2.submit()'}" title="Presupuesto">Bloque - 2</button>
                            </div>
                        </li>
                        <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                            <div style="display: flex; justify-content: space-between; align-items: center;">
                                <button class="button margin-0 ${(bloqueConFoco == 3) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(bloqueConFoco == 3) ? '':'frmIrBloque3.submit()'}" title="Documentos">Bloque - 3</button>
                            </div>
                        </li>
                        <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                            <div style="display: flex; justify-content: space-between; align-items: center;">
                                <button class="button margin-0 ${(bloqueConFoco == 4) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(bloqueConFoco == 4) ? '':'frmIrBloque4.submit()'}" title="Hitos de planeaci&oacute;n">Bloque - 4</button>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <form name="frmIrBloque1" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="6"/>
                <input type="hidden" name="txtBloqueConFoco" value="1"/>
            </form>
            <form name="frmIrBloque2" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="6"/>
                <input type="hidden" name="txtBloqueConFoco" value="2"/>
            </form>
            <form name="frmIrBloque3" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="6"/>
                <input type="hidden" name="txtBloqueConFoco" value="3"/>
            </form>
            <form name="frmIrBloque4" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="6"/>
                <input type="hidden" name="txtBloqueConFoco" value="4"/>
            </form>
        </c:if>
                
        <c:if test="${bloqueConFoco == 1}">
            <%@include file="SeccionPlanning/frmInformacionGeneral.jsp"%>
        </c:if>
        <c:if test="${bloqueConFoco == 2}">
            <%@include file="SeccionPlanning/frmBudget.jsp"%>
        </c:if>
        <c:if test="${bloqueConFoco == 3}">
            <%@include file="SeccionPlanning/frmDocuments.jsp"%>
        </c:if>
        <c:if test="${bloqueConFoco == 4}">
            <%@include file="SeccionPlanning/frmMilestones.jsp"%>
        </c:if>
        
    </div>
    
</div>