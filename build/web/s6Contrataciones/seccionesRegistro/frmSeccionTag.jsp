<%-- 
    Autor: Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<div id="seccion3">
    <h3>Etiquetas de entrega</h3>
    <div class="callout padding-horizontal-3">
        <form name="frmRegistrarSeccionEtiquetasDeEntrega" id="frmRegistrarSeccionEtiquetasDeEntrega" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post" data-abide novalidate onSubmit="event.preventDefault(); validarFormularioEtiquetasDeEntrega(this.id);" autocomplete="off">
            <input type="hidden" name="accion" value="${(bolEsFormulariosDeEdicion == true) ? 'editarSeccion' : 'registrarSeccion'}"/>
            <input type="hidden" name="txtEsFormularioDeEdicion" id="txtEsFormularioDeEdicion" value="${bolEsFormulariosDeEdicion}"/>
            <input type="hidden" name="txtNUMERO_SECCION" value="3"/>
            <input type="hidden" name="txtID_CONTRATACION_SPDN" value="${idContratacion}"/>
            <fieldset class="margin-1">
                <div class="grid-x grid-margin-x" title="Uno o m&aacute;s valores de la lista de c&oacute;digo cerrada. Las etiquetas se pueden usar para filtrar entregas y para entender el tipo de informaci&oacute;n que las entregas contienen.">
                    <c:forEach items="${etiquetasDeEntrega}" var="etiqueta">
                        <c:set var="found" value="false" scope="request" />
                        <c:forEach items="${contratacion.tag}" var="etiqueta_seleccionada">
                            <c:if test="${etiqueta_seleccionada == etiqueta.clave}">
                                <c:set var="found" value="true" scope="request" />
                            </c:if>
                        </c:forEach>
                        <label class="cell medium-4 text-truncate">
                            <input type="checkbox" name="chckTAG" value="${etiqueta.clave}" ${found ? 'checked' : ''} />
                            ${etiqueta.valor}
                        </label>
                    </c:forEach>
                </div>
                <span class="form-error margin-0" id="spanTAG">
                    Campo requerido, selecciona al menos una opci&oacute;n.
                </span>
            </fieldset>
            <div class="grid-x grid-margin-x">
                <fieldset class="cell medium-6">
                    <input type="reset" class="button expanded secondary" name="btnResetEtiquetasDeEntrega" id="btnResetEtiquetasDeEntrega" value="${(bolEsFormulariosDeEdicion == true) ? 'Restaurar' : 'Reiniciar'}"/>
                </fieldset>
                <fieldset class="cell medium-6">
                    <input type="submit" class="button expanded" name="btnRegistrarEtiquetasDeEntrega" value="${(bolEsFormulariosDeEdicion == true) ? 'Modificar datos' : 'Continuar'}"/>
                </fieldset>
            </div>
        </form>
        <div data-abide-error="data-abide-error" class="alert callout alertFooter" style="display: none;">
            <p>
                <i class="material-icons">warning</i>
                Existen algunos errores en tu registro de la secci&oacute;n 'Etiquetas de entrega'. Verifica nuevamente.
            </p>
        </div>
    </div>
    
</div>
                     
<script>
                
    function validarFormularioEtiquetasDeEntrega (idFormulario)
    { 
        
        $("#frmRegistrarSeccionEtiquetasDeEntrega").on("formvalid.zf.abide", function (ev, frm){
            
            let checkBoxEtiquetas;                                
            let bolCheckBoxEtiquetasSeleccionado;                 
            
            checkBoxEtiquetas = document.querySelectorAll('input[name="chckTAG"]:checked');
            bolCheckBoxEtiquetasSeleccionado = false;
            
            if (checkBoxEtiquetas.length === 0)
            { 
                $("#spanTAG").css("display", "block");
                $(".alertFooter").css("display", "block");
            } 
            else
            { 
                $("#spanTAG").css("display", "none");
                $(".alertFooter").css("display", "none");
                bolCheckBoxEtiquetasSeleccionado = true;
            } 
            
            if (bolCheckBoxEtiquetasSeleccionado === true)
            { 
                
                let strMensajeFrm;          
                let strTituloMensaje;       
                let bolTipoFormulario;      

                bolTipoFormulario = document.getElementById("txtEsFormularioDeEdicion").value;
                bolTipoFormulario = Boolean(bolTipoFormulario);
                strMensajeFrm = (bolTipoFormulario === true) ? "\u00bf Los datos son correctos\u003F" : "Una vez guardada la informaci\u00F3n de esta secci\u00F3n no se podr\u00E1 volver a modificar los valores ingresados hasta que se finalice con el proceso de registro de la contrataci\u00F3n p\u00FAblica.";
                strTituloMensaje = (bolTipoFormulario === true) ? "Confirmar" : "\u00bf Los datos son correctos\u003F";
            
                swal({
                    title: strTituloMensaje,
                    text: strMensajeFrm,
                    icon: "warning",
                    buttons: ["Cancelar", "Aceptar"],
                    dangerMode: true,
                    closeOnClickOutside: false,
                    closeOnEsc: false
                })
                .then((seAcepta) => {
                    if (seAcepta)
                    { 
                        document.getElementById(idFormulario).submit();
                    } 
                    else
                    { 
                        swal("Se ha cancelado la operaci\u00f3n", {
                            icon: "error",
                            buttons: false,
                            timer: 2000
                        });
                        return false;
                    } 
                });
                
            } 
            else
            {
                return false;
            }
            
        });
                    
    }; 

    window.onload = function (){
        document.getElementById("btnResetEtiquetasDeEntrega").onclick = function()
        { 
            $("#spanTAG").css("display", "none");
            $(".alertFooter").css("display", "none");
        }; 
    };

                
</script>      