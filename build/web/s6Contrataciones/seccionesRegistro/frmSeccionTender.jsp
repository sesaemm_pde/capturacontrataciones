<%-- 
    Autor: Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<div id="seccion7">
    
    <h3>Licitaci&oacute;n</h3>
    <div class="callout padding-horizontal-3">
        
        <c:if test="${bolEsFormulariosDeEdicion == true}">
            <div class="grid-x grid-margin-x">
                <div class="cell">
                    <ul class="menu-registro-contratacion" style="display:flex; margin-left: 0px;">
                        <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                            <div style="display: flex; justify-content: space-between; align-items: center;">
                                <button class="button margin-0 ${(bloqueConFoco == 1) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(bloqueConFoco == 1) ? '':'frmIrBloque1.submit()'}" title="Informaci&oacute;n general">Bloque - 1</button>
                            </div>
                        </li>
                        <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                            <div style="display: flex; justify-content: space-between; align-items: center;">
                                <button class="button margin-0 ${(bloqueConFoco == 2) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(bloqueConFoco == 2) ? '':'frmIrBloque2.submit()'}" title="Periodo de licitaci&oacute;n">Bloque - 2</button>
                            </div>
                        </li>
                        <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                            <div style="display: flex; justify-content: space-between; align-items: center;">
                                <button class="button margin-0 ${(bloqueConFoco == 3) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(bloqueConFoco == 3) ? '':'frmIrBloque3.submit()'}" title="Periodo de consulta">Bloque - 3</button>
                            </div>
                        </li>
                        <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                            <div style="display: flex; justify-content: space-between; align-items: center;">
                                <button class="button margin-0 ${(bloqueConFoco == 4) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(bloqueConFoco == 4) ? '':'frmIrBloque4.submit()'}" title="Periodo de evaluaci&oacute;n y adjudicaci&oacute;n">Bloque - 4</button>
                            </div>
                        </li>
                        <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                            <div style="display: flex; justify-content: space-between; align-items: center;">
                                <button class="button margin-0 ${(bloqueConFoco == 5) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(bloqueConFoco == 5) ? '':'frmIrBloque5.submit()'}" title="Periodo de contrato">Bloque - 5</button>
                            </div>
                        </li>
                        <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                            <div style="display: flex; justify-content: space-between; align-items: center;">
                                <button class="button margin-0 ${(bloqueConFoco == 6) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(bloqueConFoco == 6) ? '':'frmIrBloque6.submit()'}" title="Art&iacute;culos que se adquirir&aacute;n">Bloque - 6</button>
                            </div>
                        </li>
                        <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                            <div style="display: flex; justify-content: space-between; align-items: center;">
                                <button class="button margin-0 ${(bloqueConFoco == 7) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(bloqueConFoco == 7) ? '':'frmIrBloque7.submit()'}" title="Licitantes">Bloque - 7</button>
                            </div>
                        </li>
                        <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                            <div style="display: flex; justify-content: space-between; align-items: center;">
                                <button class="button margin-0 ${(bloqueConFoco == 8) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(bloqueConFoco == 8) ? '':'frmIrBloque8.submit()'}" title="Documentos">Bloque - 8</button>
                            </div>
                        </li>
                        <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                            <div style="display: flex; justify-content: space-between; align-items: center;">
                                <button class="button margin-0 ${(bloqueConFoco == 9) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(bloqueConFoco == 9) ? '':'frmIrBloque9.submit()'}" title="Hitos">Bloque - 9</button>
                            </div>
                        </li>
                        <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                            <div style="display: flex; justify-content: space-between; align-items: center;">
                                <button class="button margin-0 ${(bloqueConFoco == 10) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(bloqueConFoco == 10) ? '':'frmIrBloque10.submit()'}" title="Enmiendas">Bloque - 10</button>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <form name="frmIrBloque1" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="7"/>
                <input type="hidden" name="txtBloqueConFoco" value="1"/>
            </form>
            <form name="frmIrBloque2" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="7"/>
                <input type="hidden" name="txtBloqueConFoco" value="2"/>
            </form>
            <form name="frmIrBloque3" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="7"/>
                <input type="hidden" name="txtBloqueConFoco" value="3"/>
            </form>
            <form name="frmIrBloque4" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="7"/>
                <input type="hidden" name="txtBloqueConFoco" value="4"/>
            </form>
            <form name="frmIrBloque5" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="7"/>
                <input type="hidden" name="txtBloqueConFoco" value="5"/>
            </form>
            <form name="frmIrBloque6" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="7"/>
                <input type="hidden" name="txtBloqueConFoco" value="6"/>
            </form>
            <form name="frmIrBloque7" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="7"/>
                <input type="hidden" name="txtBloqueConFoco" value="7"/>
            </form>
            <form name="frmIrBloque8" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="7"/>
                <input type="hidden" name="txtBloqueConFoco" value="8"/>
            </form>
            <form name="frmIrBloque9" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="7"/>
                <input type="hidden" name="txtBloqueConFoco" value="9"/>
            </form>
            <form name="frmIrBloque10" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="7"/>
                <input type="hidden" name="txtBloqueConFoco" value="10"/>
            </form>
        </c:if>
                
        <c:if test="${bloqueConFoco == 1}">
            <%@include file="SeccionTender/frmInformacionGeneral.jsp"%>
        </c:if>
        <c:if test="${bloqueConFoco == 2}">
            <%@include file="SeccionTender/frmTenderPeriod.jsp"%>
        </c:if>
        <c:if test="${bloqueConFoco == 3}">
            <%@include file="SeccionTender/frmEnquiryPeriod.jsp"%>
        </c:if>
        <c:if test="${bloqueConFoco == 4}">
            <%@include file="SeccionTender/frmAwardPeriod.jsp"%>
        </c:if>
        <c:if test="${bloqueConFoco == 5}">
            <%@include file="SeccionTender/frmContractPeriod.jsp"%>
        </c:if>
        <c:if test="${bloqueConFoco == 6}">
            <%@include file="SeccionTender/frmItems.jsp"%>
        </c:if>
        <c:if test="${bloqueConFoco == 7}">
            <%@include file="SeccionTender/frmTenderers.jsp"%>
        </c:if>
        <c:if test="${bloqueConFoco == 8}">
            <%@include file="SeccionTender/frmDocuments.jsp"%>
        </c:if>
        <c:if test="${bloqueConFoco == 9}">
            <%@include file="SeccionTender/frmMilestones.jsp"%>
        </c:if>
        <c:if test="${bloqueConFoco == 10}">
            <%@include file="SeccionTender/frmAmendments.jsp"%>
        </c:if>
        
    </div>
    
</div>