<%-- 
    Autor: Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="seccion9">
    <h3>Contratos</h3>
    <div class="button-group">
        <button type="button" class="button" onclick="abrirNuevoFormulario('contract', []);">Agregar contrato</button>
    </div>
    <div class="callout padding-horizontal-3">
        <form name="frmRegistrarSeccionContratos" id="frmRegistrarSeccionContratos" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post" data-abide novalidate autocomplete="off" onsubmit="return false;">
            <input type="hidden" name="accion" value="${(bolEsFormulariosDeEdicion == true) ? 'editarSeccion' : 'registrarSeccion'}"/>
            <input type="hidden" name="txtEsFormularioDeEdicion" id="txtEsFormularioDeEdicion" value="${bolEsFormulariosDeEdicion}"/>
            <input type="hidden" name="txtNUMERO_SECCION" value="9"/>
            <input type="hidden" name="txtID_CONTRATACION_SPDN" value="${idContratacion}"/>
            <input type="hidden" id="estructura-por-defecto-contract" value="">
            <input type="hidden" id="siguiente-identificador-contract" value="" data-valor-por-defecto="">
            <input type="hidden" id="lista-identificadores-por-registrar-contract" name="lista-identificadores-por-registrar-contract" value="" data-valor-por-defecto="">
            <c:choose>
                <c:when test="${contratacion.contracts.size() > 0}">
                    <c:set var="contadorElementosContract" value="0"></c:set>
                    <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-contract">
                    <c:forEach items="${contratacion.contracts}" var="contrato">
                        <li class="accordion-item" data-accordion-item id="contract_${contadorElementosContract}">
                            <a href="#" class="accordion-title"><span style="text-transform: capitalize;">contrato</span> - <span id="titulo-elemento-contract_${contadorElementosContract}">${contrato.id}</span></a>
                            <div class="accordion-content" data-tab-content>
                                <div class="grid-margin-x grid-x">
                                    <div class="cell padding-horizontal-1" style="display:flex; justify-content: flex-end; gap: 10px; flex-wrap: wrap;">
                                        <button class="button margin-0 btnFrmContrataciones" onclick="abrirFormularioPrevio('contract', [${contadorElementosContract}])"><i class="material-icons">edit</i> Editar contrato</button>
                                        <button class="button margin-0 btnFrmContrataciones" onclick="eliminarElemento('contract', 'contract_${contadorElementosContract}', [], '${contadorElementosContract}')"><i class="material-icons">remove_circle</i> Eliminar contrato</button>
                                    </div>
                                    <div id="datos-elemento-contract_${contadorElementosContract}">
                                        <input type="hidden" name="txtCONTRACT_ID_${contadorElementosContract}" id="txtCONTRACT_ID_${contadorElementosContract}" value="${contrato.id}">
                                        <input type="hidden" name="txtCONTRACT_AWARD_ID_${contadorElementosContract}" id="txtCONTRACT_AWARD_ID_${contadorElementosContract}" value="${contrato.awardID}">
                                        <input type="hidden" name="txtCONTRACT_TITLE_${contadorElementosContract}" id="txtCONTRACT_TITLE_${contadorElementosContract}" value="${contrato.title}">
                                        <input type="hidden" name="txtCONTRACT_DESCRIPTION_${contadorElementosContract}" id="txtCONTRACT_DESCRIPTION_${contadorElementosContract}" value="${contrato.description}">
                                        <input type="hidden" name="cmbCONTRACT_STATUS_${contadorElementosContract}" id="cmbCONTRACT_STATUS_${contadorElementosContract}" value="${contrato.status}">
                                        <input type="hidden" name="txtCONTRACT_DATE_SIGNED_${contadorElementosContract}" id="txtCONTRACT_DATE_SIGNED_${contadorElementosContract}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(contrato.dateSigned)}">
                                        <input type="hidden" name="txtCONTRACT_PERIOD_START_DATE_${contadorElementosContract}" id="txtCONTRACT_PERIOD_START_DATE_${contadorElementosContract}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(contrato.period.startDate)}">
                                        <input type="hidden" name="txtCONTRACT_PERIOD_END_DATE_${contadorElementosContract}" id="txtCONTRACT_PERIOD_END_DATE_${contadorElementosContract}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(contrato.period.endDate)}">
                                        <input type="hidden" name="txtCONTRACT_PERIOD_MAX_EXTENT_DATE_${contadorElementosContract}" id="txtCONTRACT_PERIOD_MAX_EXTENT_DATE_${contadorElementosContract}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(contrato.period.maxExtentDate)}">
                                        <input type="hidden" name="txtCONTRACT_PERIOD_DURATION_IN_DATE_${contadorElementosContract}" id="txtCONTRACT_PERIOD_DURATION_IN_DATE_${contadorElementosContract}" value="${contrato.period.durationInDays}">
                                        <input type="hidden" name="txtCONTRACT_AMOUNT_${contadorElementosContract}" id="txtCONTRACT_AMOUNT_${contadorElementosContract}" value="${contrato.value.amount}">
                                        <input type="hidden" name="cmbCONTRACT_CURRENCY_${contadorElementosContract}" id="cmbCONTRACT_CURRENCY_${contadorElementosContract}" value="${contrato.value.currency}">
                                        <input type="hidden" name="txtCONTRACT_AMENDMENT_ID_${contadorElementosContract}" id="txtCONTRACT_AMENDMENT_ID_${contadorElementosContract}" value="${contrato.amendment.id}">
                                        <input type="hidden" name="txtCONTRACT_AMENDMENT_DATE_${contadorElementosContract}" id="txtCONTRACT_AMENDMENT_DATE_${contadorElementosContract}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(contrato.amendment.date)}">
                                        <input type="hidden" name="txtCONTRACT_AMENDMENT_RATIONALE_${contadorElementosContract}" id="txtCONTRACT_AMENDMENT_RATIONALE_${contadorElementosContract}" value="${contrato.amendment.rationale}">
                                        <input type="hidden" name="txtCONTRACT_AMENDMENT_DESCRIPTION_${contadorElementosContract}" id="txtCONTRACT_AMENDMENT_DESCRIPTION_${contadorElementosContract}" value="${contrato.amendment.description}">
                                        <input type="hidden" name="txtCONTRACT_AMENDMENT_AMENDS_RELEASE_ID_${contadorElementosContract}" id="txtCONTRACT_AMENDMENT_AMENDS_RELEASE_ID_${contadorElementosContract}" value="${contrato.amendment.amendsReleaseID}">
                                        <input type="hidden" name="txtCONTRACT_AMENDMENT_RELEASE_ID_${contadorElementosContract}" id="txtCONTRACT_AMENDMENT_RELEASE_ID_${contadorElementosContract}" value="${contrato.amendment.releaseID}">
                                    </div>
                                    <fieldset class="cell fieldset"> 
                                        <input type="hidden" id="estructura-por-defecto-item_${contadorElementosContract}" value=""> 
                                        <input type="hidden" id="siguiente-identificador-item_${contadorElementosContract}" value="" data-valor-por-defecto=""> 
                                        <input type="hidden" id="lista-identificadores-por-registrar-item_${contadorElementosContract}" name="lista-identificadores-por-registrar-item_${contadorElementosContract}" value="" data-valor-por-defecto="">
                                        <legend title="Los bienes, servicios y cualquier resultado intangible de este contrato. Nota: No repetir si los art&iacute;culos son los mismos que la adjudicaci&oacute;n.">Art&iacute;culos que se adquirir&aacute;n</legend>
                                        <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                            <a onclick="restaurarEstadoInicialElemento('item', [${contadorElementosContract}])"><i class="material-icons secondary">restart_alt</i></a> 
                                            <a onclick="abrirNuevoFormulario('item', [${contadorElementosContract}])"><i class="material-icons secondary">add_circle</i></a> 
                                        </div>
                                        <c:choose>
                                            <c:when test="${contrato.items.size() > 0}">
                                                <c:set var="contadorElementosItem" value="0"></c:set>
                                                <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-item_${contadorElementosContract}">
                                                <c:forEach items="${contrato.items}" var="item">
                                                    <li class="accordion-item" data-accordion-item id="item_${contadorElementosContract}_${contadorElementosItem}">
                                                        <a href="#" class="accordion-title"><span style="text-transform: capitalize;">art&iacute;culo</span> - <span id="titulo-elemento-item_${contadorElementosContract}_${contadorElementosItem}">${item.id}</span></a>
                                                        <div class="accordion-content" data-tab-content>
                                                            <div class="grid-margin-x grid-x">
                                                                <div class="cell padding-horizontal-1" style="display:flex; justify-content: flex-end; gap: 10px; flex-wrap: wrap;">
                                                                    <button class="button margin-0 btnFrmContrataciones" onclick="abrirFormularioPrevio('item', [${contadorElementosContract}, ${contadorElementosItem}])"><i class="material-icons">edit</i> Editar art&iacute;culo</button>
                                                                    <button class="button margin-0 btnFrmContrataciones" onclick="eliminarElemento('item', 'item_${contadorElementosContract}_${contadorElementosItem}', [${contadorElementosContract}], '${contadorElementosItem}')"><i class="material-icons">remove_circle</i> Eliminar art&iacute;culo</button>
                                                                </div>
                                                                <div id="datos-elemento-item_${contadorElementosContract}_${contadorElementosItem}">
                                                                    <input type="hidden" name="txtITEM_ID_${contadorElementosContract}_${contadorElementosItem}" id="txtITEM_ID_${contadorElementosContract}_${contadorElementosItem}" value="${item.id}">
                                                                    <input type="hidden" name="txtITEM_DESCRIPTION_${contadorElementosContract}_${contadorElementosItem}" id="txtITEM_DESCRIPTION_${contadorElementosContract}_${contadorElementosItem}" value="${item.description}">
                                                                    <input type="hidden" name="txtITEM_CLASSIFICATION_ID_${contadorElementosContract}_${contadorElementosItem}" id="txtITEM_CLASSIFICATION_ID_${contadorElementosContract}_${contadorElementosItem}" value="${item.classification.id}">
                                                                    <input type="hidden" name="cmbITEM_CLASSIFICATION_SCHEME_${contadorElementosContract}_${contadorElementosItem}" id="cmbITEM_CLASSIFICATION_SCHEME_${contadorElementosContract}_${contadorElementosItem}" value="${item.classification.scheme}">
                                                                    <input type="hidden" name="txtITEM_CLASSIFICATION_DESCRIPTION_${contadorElementosContract}_${contadorElementosItem}" id="txtITEM_CLASSIFICATION_DESCRIPTION_${contadorElementosContract}_${contadorElementosItem}" value="${item.classification.description}">
                                                                    <input type="hidden" name="txtITEM_CLASSIFICATION_URI_${contadorElementosContract}_${contadorElementosItem}" id="txtITEM_CLASSIFICATION_URI_${contadorElementosContract}_${contadorElementosItem}" value="${item.classification.uri}">
                                                                    <input type="hidden" name="txtITEM_QUANTITY_${contadorElementosContract}_${contadorElementosItem}" id="txtITEM_QUANTITY_${contadorElementosContract}_${contadorElementosItem}" value="${item.quantity}">
                                                                    <input type="hidden" name="txtITEM_UNIT_ID_${contadorElementosContract}_${contadorElementosItem}" id="txtITEM_UNIT_ID_${contadorElementosContract}_${contadorElementosItem}" value="${item.unit.id}">
                                                                    <input type="hidden" name="cmbITEM_UNIT_SCHEME_${contadorElementosContract}_${contadorElementosItem}" id="cmbITEM_UNIT_SCHEME_${contadorElementosContract}_${contadorElementosItem}" value="${item.unit.scheme}">
                                                                    <input type="hidden" name="txtITEM_UNIT_NAME_${contadorElementosContract}_${contadorElementosItem}" id="txtITEM_UNIT_NAME_${contadorElementosContract}_${contadorElementosItem}" value="${item.unit.name}">
                                                                    <input type="hidden" name="txtITEM_UNIT_URI_${contadorElementosContract}_${contadorElementosItem}" id="txtITEM_UNIT_URI_${contadorElementosContract}_${contadorElementosItem}" value="${item.unit.uri}">
                                                                    <input type="hidden" name="txtITEM_UNIT_AMOUNT_${contadorElementosContract}_${contadorElementosItem}" id="txtITEM_UNIT_AMOUNT_${contadorElementosContract}_${contadorElementosItem}" value="${item.unit.value.amount}">
                                                                    <input type="hidden" name="cmbITEM_UNIT_CURRENCY_${contadorElementosContract}_${contadorElementosItem}" id="cmbITEM_UNIT_CURRENCY_${contadorElementosContract}_${contadorElementosItem}" value="${item.unit.value.currency}">
                                                                </div>
                                                                <fieldset class="cell fieldset"> 
                                                                    <input type="hidden" id="estructura-por-defecto-classification_${contadorElementosContract}_${contadorElementosItem}" value=""> 
                                                                    <input type="hidden" id="siguiente-identificador-classification_${contadorElementosContract}_${contadorElementosItem}" value="" data-valor-por-defecto=""> 
                                                                    <input type="hidden" id="lista-identificadores-por-registrar-classification_${contadorElementosContract}_${contadorElementosItem}" name="lista-identificadores-por-registrar-classification_${contadorElementosContract}_${contadorElementosItem}" value="" data-valor-por-defecto="">
                                                                    <legend title="Una lista de clasificaciones adicionales para el art&iacute;culo.">Clasificaciones adicionales</legend>
                                                                    <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                                                        <a onclick="restaurarEstadoInicialElemento('classification', [${contadorElementosContract}, ${contadorElementosItem}])"><i class="material-icons secondary">restart_alt</i></a> 
                                                                        <a onclick="abrirNuevoFormulario('classification', [${contadorElementosContract}, ${contadorElementosItem}])"><i class="material-icons secondary">add_circle</i></a> 
                                                                    </div>
                                                                    <c:choose>
                                                                        <c:when test="${item.additionalClassifications.size() > 0}">
                                                                            <c:set var="contadorElementosClassification" value="0"></c:set>
                                                                            <div id="contenedor-classification_${contadorElementosContract}_${contadorElementosItem}" style="display: flex; flex-direction: column; gap: 10px;">
                                                                            <c:forEach items="${item.additionalClassifications}" var="clasificacion">
                                                                                <div id="classification_${contadorElementosContract}_${contadorElementosItem}_${contadorElementosClassification}">
                                                                                    <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                                                                        <div class="cell medium-8">
                                                                                            <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">clasificaci&oacute;n</span> - <span id="titulo-elemento-classification_${contadorElementosContract}_${contadorElementosItem}_${contadorElementosClassification}">${clasificacion.id}</span></h6>
                                                                                        </div>
                                                                                        <div id="datos-elemento-classification_${contadorElementosContract}_${contadorElementosItem}_${contadorElementosClassification}">
                                                                                            <input type="hidden" name="txtCLASSIFICATION_ID_${contadorElementosContract}_${contadorElementosItem}_${contadorElementosClassification}" id="txtCLASSIFICATION_ID_${contadorElementosContract}_${contadorElementosItem}_${contadorElementosClassification}" value="${clasificacion.id}">
                                                                                            <input type="hidden" name="cmbCLASSIFICATION_SCHEME_${contadorElementosContract}_${contadorElementosItem}_${contadorElementosClassification}" id="cmbCLASSIFICATION_SCHEME_${contadorElementosContract}_${contadorElementosItem}_${contadorElementosClassification}" value="${clasificacion.scheme}">
                                                                                            <input type="hidden" name="txtCLASSIFICATION_DESCRIPTION_${contadorElementosContract}_${contadorElementosItem}_${contadorElementosClassification}" id="txtCLASSIFICATION_DESCRIPTION_${contadorElementosContract}_${contadorElementosItem}_${contadorElementosClassification}" value="${clasificacion.description}">
                                                                                            <input type="hidden" name="txtCLASSIFICATION_URI_${contadorElementosContract}_${contadorElementosItem}_${contadorElementosClassification}" id="txtCLASSIFICATION_URI_${contadorElementosContract}_${contadorElementosItem}_${contadorElementosClassification}" value="${clasificacion.uri}">
                                                                                        </div>
                                                                                        <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                                                            <button class="button margin-0" onclick="abrirFormularioPrevio('classification', [${contadorElementosContract}, ${contadorElementosItem}, ${contadorElementosClassification}])"><i class="material-icons">edit</i> Editar</button>
                                                                                            <button class="button margin-0" onclick="eliminarElemento('classification', 'classification_${contadorElementosContract}_${contadorElementosItem}_${contadorElementosClassification}', [${contadorElementosContract}, ${contadorElementosItem}], '${contadorElementosClassification}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <c:set var="contadorElementosClassification" value="${contadorElementosClassification + 1}"></c:set>
                                                                            </c:forEach>
                                                                            </div>
                                                                            <input type="hidden" name="ultimo-identificador-elementos-classification_${contadorElementosContract}_${contadorElementosItem}" id="ultimo-identificador-elementos-classification_${contadorElementosContract}_${contadorElementosItem}" value="${contadorElementosClassification}">
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <div id="contenedor-classification_${contadorElementosContract}_${contadorElementosItem}" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                                                            <input type="hidden" name="ultimo-identificador-elementos-classification_${contadorElementosContract}_${contadorElementosItem}" id="ultimo-identificador-elementos-classification_${contadorElementosContract}_${contadorElementosItem}" value="0">
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                    <script>
                                                                        document.getElementById("estructura-por-defecto-classification_${contadorElementosContract}_${contadorElementosItem}").value = obtenerDatosInicialesElementos("classification", [${contadorElementosContract}, ${contadorElementosItem}]);
                                                                    </script>
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <c:set var="contadorElementosItem" value="${contadorElementosItem + 1}"></c:set>
                                                </c:forEach>
                                                </ul>
                                                <input type="hidden" name="ultimo-identificador-elementos-item_${contadorElementosContract}" id="ultimo-identificador-elementos-item_${contadorElementosContract}" value="${contadorElementosItem}">
                                            </c:when>
                                            <c:otherwise>
                                                <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-item_${contadorElementosContract}"></ul>
                                                <input type="hidden" name="ultimo-identificador-elementos-item_${contadorElementosContract}" id="ultimo-identificador-elementos-item_${contadorElementosContract}" value="0">
                                            </c:otherwise>
                                        </c:choose>
                                        <script>
                                            document.getElementById("estructura-por-defecto-item_${contadorElementosContract}").value = obtenerDatosInicialesElementos("item", [${contadorElementosContract}]);
                                        </script>
                                    </fieldset>
                                    <fieldset class="cell fieldset"> 
                                        <input type="hidden" id="estructura-por-defecto-document_${contadorElementosContract}" value=""> 
                                        <input type="hidden" id="siguiente-identificador-document_${contadorElementosContract}" value="" data-valor-por-defecto=""> 
                                        <input type="hidden" id="lista-identificadores-por-registrar-document_${contadorElementosContract}" name="lista-identificadores-por-registrar-document_${contadorElementosContract}" value="" data-valor-por-defecto="">
                                        <legend title="Todos los documentos y archivos adjuntos relacionados con el contrato, incluyendo cualquier aviso o notificaci&oacute;n.">Documentos</legend>
                                        <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                            <a onclick="restaurarEstadoInicialElemento('document', [${contadorElementosContract}])"><i class="material-icons secondary">restart_alt</i></a> 
                                            <a onclick="abrirNuevoFormulario('document', [${contadorElementosContract}])"><i class="material-icons secondary">add_circle</i></a> 
                                        </div>
                                        <c:choose>
                                            <c:when test="${contrato.documents.size() > 0}">
                                                <c:set var="contadorElementosDocument" value="0"></c:set>
                                                <div id="contenedor-document_${contadorElementosContract}" style="display: flex; flex-direction: column; gap: 10px;">
                                                <c:forEach items="${contrato.documents}" var="documento">
                                                    <div id="document_${contadorElementosContract}_${contadorElementosDocument}">
                                                        <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                                            <div class="cell medium-8">
                                                                <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">documento</span> - <span id="titulo-elemento-document_${contadorElementosContract}_${contadorElementosDocument}">${documento.id}</span></h6>
                                                            </div>
                                                            <div id="datos-elemento-document_${contadorElementosContract}_${contadorElementosDocument}">
                                                                <input type="hidden" name="txtDOCUMENT_ID_${contadorElementosContract}_${contadorElementosDocument}" id="txtDOCUMENT_ID_${contadorElementosContract}_${contadorElementosDocument}" value="${documento.id}">
                                                                <input type="hidden" name="cmbDOCUMENT_TYPE_${contadorElementosContract}_${contadorElementosDocument}" id="cmbDOCUMENT_TYPE_${contadorElementosContract}_${contadorElementosDocument}" value="${documento.documentType}">
                                                                <input type="hidden" name="txtDOCUMENT_TITLE_${contadorElementosContract}_${contadorElementosDocument}" id="txtDOCUMENT_TITLE_${contadorElementosContract}_${contadorElementosDocument}" value="${documento.title}">
                                                                <input type="hidden" name="txtDOCUMENT_DESCRIPTION_${contadorElementosContract}_${contadorElementosDocument}" id="txtDOCUMENT_DESCRIPTION_${contadorElementosContract}_${contadorElementosDocument}" value="${documento.description}">
                                                                <input type="hidden" name="txtDOCUMENT_URL_${contadorElementosContract}_${contadorElementosDocument}" id="txtDOCUMENT_URL_${contadorElementosContract}_${contadorElementosDocument}" value="${documento.url}">
                                                                <input type="hidden" name="txtDOCUMENT_DATE_PUBLISHED_${contadorElementosContract}_${contadorElementosDocument}" id="txtDOCUMENT_DATE_PUBLISHED_${contadorElementosContract}_${contadorElementosDocument}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(documento.datePublished)}">
                                                                <input type="hidden" name="txtDOCUMENT_DATE_MODIFIED_${contadorElementosContract}_${contadorElementosDocument}" id="txtDOCUMENT_DATE_MODIFIED_${contadorElementosContract}_${contadorElementosDocument}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(documento.dateModified)}">
                                                                <input type="hidden" name="txtDOCUMENT_FORMAT_${contadorElementosContract}_${contadorElementosDocument}" id="txtDOCUMENT_FORMAT_${contadorElementosContract}_${contadorElementosDocument}" value="${documento.format}">
                                                                <input type="hidden" name="cmbDOCUMENT_LANGUAGE_${contadorElementosContract}_${contadorElementosDocument}" id="cmbDOCUMENT_LANGUAGE_${contadorElementosContract}_${contadorElementosDocument}" value="${documento.language}">
                                                            </div>
                                                            <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                                <button class="button margin-0" onclick="abrirFormularioPrevio('document', [${contadorElementosContract}, ${contadorElementosDocument}])"><i class="material-icons">edit</i> Editar</button>
                                                                <button class="button margin-0" onclick="eliminarElemento('document', 'document_${contadorElementosContract}_${contadorElementosDocument}', [${contadorElementosContract}], '${contadorElementosDocument}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <c:set var="contadorElementosDocument" value="${contadorElementosDocument + 1}"></c:set>
                                                </c:forEach>
                                                </div>
                                                <input type="hidden" name="ultimo-identificador-elementos-document_${contadorElementosContract}" id="ultimo-identificador-elementos-document_${contadorElementosContract}" value="${contadorElementosDocument}">
                                            </c:when>
                                            <c:otherwise>
                                                <div id="contenedor-document_${contadorElementosContract}" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                                <input type="hidden" name="ultimo-identificador-elementos-document_${contadorElementosContract}" id="ultimo-identificador-elementos-document_${contadorElementosContract}" value="0">
                                            </c:otherwise>
                                        </c:choose>
                                        <script>
                                            document.getElementById("estructura-por-defecto-document_${contadorElementosContract}").value = obtenerDatosInicialesElementos("document", [${contadorElementosContract}]);
                                        </script>
                                    </fieldset>
                                    <fieldset class="cell fieldset">
                                        <legend title='La informaci&oacute;n relacionada con la implementaci&oacute;n del contrato de acuerdo con las obligaciones definidas en el mismo.'>Implementaci&oacute;n</legend>
                                        <div class="grid-x grid-margin-x">
                                            <fieldset class="cell fieldset"> 
                                                <input type="hidden" id="estructura-por-defecto-implementation-transaction_${contadorElementosContract}" value=""> 
                                                <input type="hidden" id="siguiente-identificador-implementation-transaction_${contadorElementosContract}" value="" data-valor-por-defecto=""> 
                                                <input type="hidden" id="lista-identificadores-por-registrar-implementation-transaction_${contadorElementosContract}" name="lista-identificadores-por-registrar-implementation-transaction_${contadorElementosContract}" value="" data-valor-por-defecto="">
                                                <legend title="Una lista de transacciones de pago realizadas contra este contrato.">Transacciones</legend>
                                                <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                                    <a onclick="restaurarEstadoInicialElemento('implementation-transaction', [${contadorElementosContract}])"><i class="material-icons secondary">restart_alt</i></a> 
                                                    <a onclick="abrirNuevoFormulario('implementation-transaction', [${contadorElementosContract}])"><i class="material-icons secondary">add_circle</i></a> 
                                                </div>
                                                <c:choose>
                                                    <c:when test="${contrato.implementation.transactions.size() > 0}">
                                                        <c:set var="contadorElementosImplementationTransaction" value="0"></c:set>
                                                        <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-implementation-transaction_${contadorElementosContract}">
                                                        <c:forEach items="${contrato.implementation.transactions}" var="implementationTransacion">
                                                            <li class="accordion-item" data-accordion-item id="implementation-transaction_${contadorElementosContract}_${contadorElementosImplementationTransaction}">
                                                                <a href="#" class="accordion-title"><span style="text-transform: capitalize;">transacci&oacute;n</span> - <span id="titulo-elemento-implementation-transaction_${contadorElementosContract}_${contadorElementosImplementationTransaction}">${implementationTransacion.id}</span></a>
                                                                <div class="accordion-content" data-tab-content>
                                                                    <div class="grid-margin-x grid-x">
                                                                        <div class="cell padding-horizontal-1" style="display:flex; justify-content: flex-end; gap: 10px; flex-wrap: wrap;">
                                                                            <button class="button margin-0 btnFrmContrataciones" onclick="abrirFormularioPrevio('implementation-transaction', [${contadorElementosContract}, ${contadorElementosImplementationTransaction}])"><i class="material-icons">edit</i> Editar transacci&oacute;n</button>
                                                                            <button class="button margin-0 btnFrmContrataciones" onclick="eliminarElemento('implementation-transaction', 'implementation-transaction_${contadorElementosContract}_${contadorElementosImplementationTransaction}', [${contadorElementosContract}], '${contadorElementosImplementationTransaction}')"><i class="material-icons">remove_circle</i> Eliminar transacci&oacute;n</button>
                                                                        </div>
                                                                        <div id="datos-elemento-implementation-transaction_${contadorElementosContract}_${contadorElementosImplementationTransaction}">
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_ID_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_ID_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.id}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_SOURCE_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_SOURCE_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.source}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_DATE_PUBLISHED_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_DATE_PUBLISHED_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(implementationTransacion.date)}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_URI_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_URI_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.uri}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_VALUE_AMOUNT_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_VALUE_AMOUNT_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.value.amount}" >
                                                                            <input type="hidden" name="cmbIMPLEMENTATION_TRANSACTION_VALUE_CURRENCY_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="cmbIMPLEMENTATION_TRANSACTION_VALUE_CURRENCY_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.value.currency}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_AMOUNT_AMOUNT_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_AMOUNT_AMOUNT_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.amount.amount}" >
                                                                            <input type="hidden" name="cmbIMPLEMENTATION_TRANSACTION_AMOUNT_CURRENCY_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="cmbIMPLEMENTATION_TRANSACTION_AMOUNT_CURRENCY_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.amount.currency}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYER_ID_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYER_ID_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payer.id}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYER_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYER_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payer.name}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_ID_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_ID_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payer.identifier.id}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_SCHEME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_SCHEME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payer.identifier.scheme}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_LEGAL_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_LEGAL_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payer.identifier.legalName}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_URI_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_URI_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payer.identifier.uri}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_STREET_ADDRESS_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_STREET_ADDRESS_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payer.address.streetAddress}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_LOCALITY_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_LOCALITY_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payer.address.locality}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_REGION_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_REGION_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payer.address.region}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_POSTAL_CODE_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_POSTAL_CODE_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payer.address.postalCode}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_COUNTRY_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_COUNTRY_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payer.address.countryName}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payer.contactPoint.name}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_EMAIL_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_EMAIL_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payer.contactPoint.email}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_TELEPHONE_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_TELEPHONE_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payer.contactPoint.telephone}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_FAXNUMBER_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_FAXNUMBER_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payer.contactPoint.faxNumber}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_URL_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_URL_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payer.contactPoint.url}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYEE_ID_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_ID_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payee.id}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYEE_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payee.name}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_ID_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_ID_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payee.identifier.id}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_SCHEME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_SCHEME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payee.identifier.scheme}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_LEGAL_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_LEGAL_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payee.identifier.legalName}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_URI_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_URI_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payee.identifier.uri}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_STREET_ADDRESS_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_STREET_ADDRESS_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payee.address.streetAddress}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_LOCALITY_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_LOCALITY_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payee.address.locality}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_REGION_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_REGION_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payee.address.region}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_POSTAL_CODE_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_POSTAL_CODE_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payee.address.postalCode}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_COUNTRY_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_COUNTRY_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payee.address.countryName}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payee.contactPoint.name}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_EMAIL_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_EMAIL_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payee.contactPoint.email}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_TELEPHONE_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_TELEPHONE_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payee.contactPoint.telephone}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_FAXNUMBER_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_FAXNUMBER_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payee.contactPoint.faxNumber}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_URL_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_URL_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.payee.contactPoint.url}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_ID_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_ID_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.providerOrganization.id}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_SCHEME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_SCHEME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.providerOrganization.scheme}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_LEGAL_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_LEGAL_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.providerOrganization.legalName}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_URI_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_URI_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.providerOrganization.uri}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_ID_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_ID_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.receiverOrganization.id}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_SCHEME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_SCHEME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.receiverOrganization.scheme}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_LEGAL_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_LEGAL_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.receiverOrganization.legalName}" >
                                                                            <input type="hidden" name="txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_URI_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_URI_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${implementationTransacion.receiverOrganization.uri}" >
                                                                        </div>
                                                                        <fieldset class="cell fieldset"> 
                                                                            <input type="hidden" id="estructura-por-defecto-identifier-payer_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value=""> 
                                                                            <input type="hidden" id="siguiente-identificador-identifier-payer_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="" data-valor-por-defecto=""> 
                                                                            <input type="hidden" id="lista-identificadores-por-registrar-identifier-payer_${contadorElementosContract}_${contadorElementosImplementationTransaction}" name="lista-identificadores-por-registrar-identifier-payer_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="" data-valor-por-defecto="">
                                                                            <legend title="Una lista adicional/suplementaria de identificadores para la organizaci�n, utilizando la [gu�a de identificadores de organizaciones]. Esto puede usarse para dar un identificador utilizado internamente por esta organizaci�n adem�s del identificador primario legal de la entidad.">Identificadores adicionales - Pagador</legend>
                                                                            <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                                                                <a onclick="restaurarEstadoInicialElemento('identifier-payer', [${contadorElementosContract}, ${contadorElementosImplementationTransaction}])"><i class="material-icons secondary">restart_alt</i></a> 
                                                                                <a onclick="abrirNuevoFormulario('identifier-payer', [${contadorElementosContract}, ${contadorElementosImplementationTransaction}])"><i class="material-icons secondary">add_circle</i></a> 
                                                                            </div>
                                                                            <c:choose>
                                                                                <c:when test="${implementationTransacion.payer.additionalIdentifiers.size() > 0}">
                                                                                    <c:set var="contadorElementosIdentifierPayer" value="0"></c:set>
                                                                                    <div id="contenedor-identifier-payer_${contadorElementosContract}_${contadorElementosImplementationTransaction}" style="display: flex; flex-direction: column; gap: 10px;">
                                                                                    <c:forEach items="${implementationTransacion.payer.additionalIdentifiers}" var="identificadorPayer">
                                                                                        <div id="identifier-payer_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayer}">
                                                                                            <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                                                                                <div class="cell medium-8">
                                                                                                    <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">identificador pagador</span> - <span id="titulo-elemento-identifier-payer_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayer}">${identificadorPayer.id}</span></h6>
                                                                                                </div>
                                                                                                <div id="datos-elemento-identifier-payer_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayer}">
                                                                                                    <input type="hidden" name="txtIDENTIFIER_PAYER_ID_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayer}" id="txtIDENTIFIER_PAYER_ID_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayer}" value="${identificadorPayer.id}" >
                                                                                                    <input type="hidden" name="txtIDENTIFIER_PAYER_SCHEME_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayer}" id="txtIDENTIFIER_PAYER_SCHEME_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayer}" value="${identificadorPayer.scheme}" >
                                                                                                    <input type="hidden" name="txtIDENTIFIER_PAYER_LEGAL_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayer}" id="txtIDENTIFIER_PAYER_LEGAL_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayer}" value="${identificadorPayer.legalName}" >
                                                                                                    <input type="hidden" name="txtIDENTIFIER_PAYER_URI_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayer}" id="txtIDENTIFIER_PAYER_URI_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayer}" value="${identificadorPayer.uri}" >                                                        
                                                                                                </div>
                                                                                                <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                                                                    <button class="button margin-0" onclick="abrirFormularioPrevio('identifier-payer', [${contadorElementosContract}, ${contadorElementosImplementationTransaction}, ${contadorElementosIdentifierPayer}])"><i class="material-icons">edit</i> Editar</button>
                                                                                                    <button class="button margin-0" onclick="eliminarElemento('identifier-payer', 'identifier-payer_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayer}', [${contadorElementosContract}, ${contadorElementosImplementationTransaction}], '${contadorElementosIdentifierPayer}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <c:set var="contadorElementosIdentifierPayer" value="${contadorElementosIdentifierPayer + 1}"></c:set>
                                                                                    </c:forEach>
                                                                                    </div>
                                                                                    <input type="hidden" name="ultimo-identificador-elementos-identifier-payer_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="ultimo-identificador-elementos-identifier-payer_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${contadorElementosIdentifierPayer}">
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    <div id="contenedor-identifier-payer_${contadorElementosContract}_${contadorElementosImplementationTransaction}" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                                                                    <input type="hidden" name="ultimo-identificador-elementos-identifier-payer_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="ultimo-identificador-elementos-identifier-payer_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="0">
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                            <script>
                                                                                document.getElementById("estructura-por-defecto-identifier-payer_${contadorElementosContract}_${contadorElementosImplementationTransaction}").value = obtenerDatosInicialesElementos("identifier-payer", [${contadorElementosContract}, ${contadorElementosImplementationTransaction}]);
                                                                            </script>
                                                                        </fieldset>
                                                                        <fieldset class="cell fieldset"> 
                                                                            <input type="hidden" id="estructura-por-defecto-identifier-payee_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value=""> 
                                                                            <input type="hidden" id="siguiente-identificador-identifier-payee_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="" data-valor-por-defecto=""> 
                                                                            <input type="hidden" id="lista-identificadores-por-registrar-identifier-payee_${contadorElementosContract}_${contadorElementosImplementationTransaction}" name="lista-identificadores-por-registrar-identifier-payee_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="" data-valor-por-defecto="">
                                                                            <legend title="Una lista adicional/suplementaria de identificadores para la organizaci�n, utilizando la [gu�a de identificadores de organizaciones]. Esto puede usarse para dar un identificador utilizado internamente por esta organizaci�n adem�s del identificador primario legal de la entidad.">Identificadores adicionales - Beneficiario</legend>
                                                                            <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                                                                <a onclick="restaurarEstadoInicialElemento('identifier-payee', [${contadorElementosContract}, ${contadorElementosImplementationTransaction}])"><i class="material-icons secondary">restart_alt</i></a> 
                                                                                <a onclick="abrirNuevoFormulario('identifier-payee', [${contadorElementosContract}, ${contadorElementosImplementationTransaction}])"><i class="material-icons secondary">add_circle</i></a> 
                                                                            </div>
                                                                            <c:choose>
                                                                                <c:when test="${implementationTransacion.payee.additionalIdentifiers.size() > 0}">
                                                                                    <c:set var="contadorElementosIdentifierPayee" value="0"></c:set>
                                                                                    <div id="contenedor-identifier-payee_${contadorElementosContract}_${contadorElementosImplementationTransaction}" style="display: flex; flex-direction: column; gap: 10px;">
                                                                                    <c:forEach items="${implementationTransacion.payee.additionalIdentifiers}" var="identificadorPayee">
                                                                                        <div id="identifier-payee_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayee}">
                                                                                            <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                                                                                <div class="cell medium-8">
                                                                                                    <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">identificador beneficiario</span> - <span id="titulo-elemento-identifier-payee_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayee}">${identificadorPayee.id}</span></h6>
                                                                                                </div>
                                                                                                <div id="datos-elemento-identifier-payee_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayee}">
                                                                                                    <input type="hidden" name="txtIDENTIFIER_PAYEE_ID_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayee}" id="txtIDENTIFIER_PAYEE_ID_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayee}" value="${identificadorPayee.id}" >
                                                                                                    <input type="hidden" name="txtIDENTIFIER_PAYEE_SCHEME_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayee}" id="txtIDENTIFIER_PAYEE_SCHEME_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayee}" value="${identificadorPayee.scheme}" >
                                                                                                    <input type="hidden" name="txtIDENTIFIER_PAYEE_LEGAL_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayee}" id="txtIDENTIFIER_PAYEE_LEGAL_NAME_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayee}" value="${identificadorPayee.legalName}" >
                                                                                                    <input type="hidden" name="txtIDENTIFIER_PAYEE_URI_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayee}" id="txtIDENTIFIER_PAYEE_URI_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayee}" value="${identificadorPayee.uri}" >                                                        
                                                                                                </div>
                                                                                                <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                                                                    <button class="button margin-0" onclick="abrirFormularioPrevio('identifier-payee', [${contadorElementosContract}, ${contadorElementosImplementationTransaction}, ${contadorElementosIdentifierPayee}])"><i class="material-icons">edit</i> Editar</button>
                                                                                                    <button class="button margin-0" onclick="eliminarElemento('identifier-payee', 'identifier-payee_${contadorElementosContract}_${contadorElementosImplementationTransaction}_${contadorElementosIdentifierPayee}', [${contadorElementosContract}, ${contadorElementosImplementationTransaction}], '${contadorElementosIdentifierPayee}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <c:set var="contadorElementosIdentifierPayee" value="${contadorElementosIdentifierPayee + 1}"></c:set>
                                                                                    </c:forEach>
                                                                                    </div>
                                                                                    <input type="hidden" name="ultimo-identificador-elementos-identifier-payee_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="ultimo-identificador-elementos-identifier-payee_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="${contadorElementosIdentifierPayee}">
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    <div id="contenedor-identifier-payee_${contadorElementosContract}_${contadorElementosImplementationTransaction}" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                                                                    <input type="hidden" name="ultimo-identificador-elementos-identifier-payee_${contadorElementosContract}_${contadorElementosImplementationTransaction}" id="ultimo-identificador-elementos-identifier-payee_${contadorElementosContract}_${contadorElementosImplementationTransaction}" value="0">
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                            <script>
                                                                                document.getElementById("estructura-por-defecto-identifier-payee_${contadorElementosContract}_${contadorElementosImplementationTransaction}").value = obtenerDatosInicialesElementos("identifier-payee", [${contadorElementosContract}, ${contadorElementosImplementationTransaction}]);
                                                                            </script>
                                                                        </fieldset>  
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <c:set var="contadorElementosImplementationTransaction" value="${contadorElementosImplementationTransaction + 1}"></c:set>
                                                        </c:forEach>
                                                        </ul>
                                                        <input type="hidden" name="ultimo-identificador-elementos-implementation-transaction_${contadorElementosContract}" id="ultimo-identificador-elementos-implementation-transaction_${contadorElementosContract}" value="${contadorElementosImplementationTransaction}">
                                                    </c:when>
                                                    <c:otherwise>
                                                        <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-implementation-transaction_${contadorElementosContract}"></ul>
                                                        <input type="hidden" name="ultimo-identificador-elementos-implementation-transaction_${contadorElementosContract}" id="ultimo-identificador-elementos-implementation-transaction_${contadorElementosContract}" value="0">
                                                    </c:otherwise>
                                                </c:choose>
                                                <script>
                                                    document.getElementById("estructura-por-defecto-implementation-transaction_${contadorElementosContract}").value = obtenerDatosInicialesElementos("implementation-transaction", [${contadorElementosContract}]);
                                                </script>
                                            </fieldset>
                                            <fieldset class="cell fieldset"> 
                                                <input type="hidden" id="estructura-por-defecto-implementation-milestone_${contadorElementosContract}" value=""> 
                                                <input type="hidden" id="siguiente-identificador-implementation-milestone_${contadorElementosContract}" value="" data-valor-por-defecto=""> 
                                                <input type="hidden" id="lista-identificadores-por-registrar-implementation-milestone_${contadorElementosContract}" name="lista-identificadores-por-registrar-implementation-milestone_${contadorElementosContract}" value="" data-valor-por-defecto="">
                                                <legend title="Cuando se completen los hitos, se deben de actualizar el estatus de los hitos y las fechas.">Hitos</legend>
                                                <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                                    <a onclick="restaurarEstadoInicialElemento('implementation-milestone', [${contadorElementosContract}])"><i class="material-icons secondary">restart_alt</i></a> 
                                                    <a onclick="abrirNuevoFormulario('implementation-milestone', [${contadorElementosContract}])"><i class="material-icons secondary">add_circle</i></a> 
                                                </div>
                                                <c:choose>
                                                    <c:when test="${contrato.implementation.milestones.size() > 0}">
                                                        <c:set var="contadorElementosImplementationMilestone" value="0"></c:set>
                                                        <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-implementation-milestone_${contadorElementosContract}">
                                                        <c:forEach items="${contrato.implementation.milestones}" var="implementationMilestone">
                                                            <li class="accordion-item" data-accordion-item id="implementation-milestone_${contadorElementosContract}_${contadorElementosImplementationMilestone}">
                                                                <a href="#" class="accordion-title"><span style="text-transform: capitalize;">hito</span> - <span id="titulo-elemento-implementation-milestone_${contadorElementosContract}_${contadorElementosImplementationMilestone}">${implementationMilestone.id}</span></a>
                                                                <div class="accordion-content" data-tab-content>
                                                                    <div class="grid-margin-x grid-x">
                                                                        <div class="cell padding-horizontal-1" style="display:flex; justify-content: flex-end; gap: 10px; flex-wrap: wrap;">
                                                                            <button class="button margin-0 btnFrmContrataciones" onclick="abrirFormularioPrevio('implementation-milestone', [${contadorElementosContract}, ${contadorElementosImplementationMilestone}])"><i class="material-icons">edit</i> Editar art&iacute;culo</button>
                                                                            <button class="button margin-0 btnFrmContrataciones" onclick="eliminarElemento('implementation-milestone', 'implementation-milestone_${contadorElementosContract}_${contadorElementosImplementationMilestone}', [${contadorElementosContract}], '${contadorElementosImplementationMilestone}')"><i class="material-icons">remove_circle</i> Eliminar art&iacute;culo</button>
                                                                        </div>
                                                                        <div id="datos-elemento-implementation-milestone_${contadorElementosContract}_${contadorElementosImplementationMilestone}">
                                                                            <input type="hidden" name="txtIMPLEMENTATION_MILESTONE_ID_${contadorElementosContract}_${contadorElementosImplementationMilestone}" id="txtIMPLEMENTATION_MILESTONE_ID_${contadorElementosContract}_${contadorElementosImplementationMilestone}" value="${implementationMilestone.id}">
                                                                            <input type="hidden" name="cmbIMPLEMENTATION_MILESTONE_TYPE_${contadorElementosContract}_${contadorElementosImplementationMilestone}" id="cmbIMPLEMENTATION_MILESTONE_TYPE_${contadorElementosContract}_${contadorElementosImplementationMilestone}" value="${implementationMilestone.type}">
                                                                            <input type="hidden" name="txtIMPLEMENTATION_MILESTONE_TITLE_${contadorElementosContract}_${contadorElementosImplementationMilestone}" id="txtIMPLEMENTATION_MILESTONE_TITLE_${contadorElementosContract}_${contadorElementosImplementationMilestone}" value="${implementationMilestone.title}">
                                                                            <input type="hidden" name="txtIMPLEMENTATION_MILESTONE_DESCRIPTION_${contadorElementosContract}_${contadorElementosImplementationMilestone}" id="txtIMPLEMENTATION_MILESTONE_DESCRIPTION_${contadorElementosContract}_${contadorElementosImplementationMilestone}" value="${implementationMilestone.description}">
                                                                            <input type="hidden" name="txtIMPLEMENTATION_MILESTONE_CODE_${contadorElementosContract}_${contadorElementosImplementationMilestone}" id="txtIMPLEMENTATION_MILESTONE_CODE_${contadorElementosContract}_${contadorElementosImplementationMilestone}" value="${implementationMilestone.code}">
                                                                            <input type="hidden" name="txtIMPLEMENTATION_MILESTONE_DUE_DATE_${contadorElementosContract}_${contadorElementosImplementationMilestone}" id="txtIMPLEMENTATION_MILESTONE_DUE_DATE_${contadorElementosContract}_${contadorElementosImplementationMilestone}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(implementationMilestone.dueDate)}">
                                                                            <input type="hidden" name="txtIMPLEMENTATION_MILESTONE_DATE_MET_${contadorElementosContract}_${contadorElementosImplementationMilestone}" id="txtIMPLEMENTATION_MILESTONE_DATE_MET_${contadorElementosContract}_${contadorElementosImplementationMilestone}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(implementationMilestone.dateMet)}">
                                                                            <input type="hidden" name="txtIMPLEMENTATION_MILESTONE_DATE_MODIFIED_${contadorElementosContract}_${contadorElementosImplementationMilestone}" id="txtIMPLEMENTATION_MILESTONE_DATE_MODIFIED_${contadorElementosContract}_${contadorElementosImplementationMilestone}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(implementationMilestone.dateModified)}">
                                                                            <input type="hidden" name="cmbIMPLEMENTATION_MILESTONE_STATUS_${contadorElementosContract}_${contadorElementosImplementationMilestone}" id="cmbIMPLEMENTATION_MILESTONE_STATUS_${contadorElementosContract}_${contadorElementosImplementationMilestone}" value="${implementationMilestone.status}">
                                                                        </div>
                                                                        <fieldset class="cell fieldset"> 
                                                                            <input type="hidden" id="estructura-por-defecto-implementation-milestone-document_${contadorElementosContract}_${contadorElementosImplementationMilestone}" value=""> 
                                                                            <input type="hidden" id="siguiente-identificador-implementation-milestone-document_${contadorElementosContract}_${contadorElementosImplementationMilestone}" value="" data-valor-por-defecto=""> 
                                                                            <input type="hidden" id="lista-identificadores-por-registrar-implementation-milestone-document_${contadorElementosContract}_${contadorElementosImplementationMilestone}" name="lista-identificadores-por-registrar-implementation-milestone-document_${contadorElementosContract}_${contadorElementosImplementationMilestone}" value="" data-valor-por-defecto="">
                                                                            <legend title="Lista de documentos asociados con este hito.">Documentos</legend>
                                                                            <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                                                                <a onclick="restaurarEstadoInicialElemento('implementation-milestone-document', [${contadorElementosContract}, ${contadorElementosImplementationMilestone}])"><i class="material-icons secondary">restart_alt</i></a> 
                                                                                <a onclick="abrirNuevoFormulario('implementation-milestone-document', [${contadorElementosContract}, ${contadorElementosImplementationMilestone}])"><i class="material-icons secondary">add_circle</i></a> 
                                                                            </div>
                                                                            <c:choose>
                                                                                <c:when test="${implementationMilestone.documents.size() > 0}">
                                                                                    <c:set var="contadorElementosImplementationMilestoneDocument" value="0"></c:set>
                                                                                    <div id="contenedor-implementation-milestone-document_${contadorElementosContract}_${contadorElementosImplementationMilestone}" style="display: flex; flex-direction: column; gap: 10px;">
                                                                                    <c:forEach items="${implementationMilestone.documents}" var="implementationMilestoneDocument">
                                                                                        <div id="implementation-milestone-document_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}">
                                                                                            <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                                                                                <div class="cell medium-8">
                                                                                                    <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">documento</span> - <span id="titulo-elemento-implementation-milestone-document_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}">${implementationMilestoneDocument.id}</span></h6>
                                                                                                </div>
                                                                                                <div id="datos-elemento-implementation-milestone-document_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}">
                                                                                                    <input type="hidden" name="txtIMPLEMENTATION_MILESTONE_DOCUMENT_ID_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}" id="txtIMPLEMENTATION_MILESTONE_DOCUMENT_ID_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}" value="${implementationMilestoneDocument.id}">
                                                                                                    <input type="hidden" name="cmbIMPLEMENTATION_MILESTONE_DOCUMENT_TYPE_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}" id="cmbIMPLEMENTATION_MILESTONE_DOCUMENT_TYPE_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}" value="${implementationMilestoneDocument.documentType}">
                                                                                                    <input type="hidden" name="txtIMPLEMENTATION_MILESTONE_DOCUMENT_TITLE_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}" id="txtIMPLEMENTATION_MILESTONE_DOCUMENT_TITLE_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}" value="${implementationMilestoneDocument.title}">
                                                                                                    <input type="hidden" name="txtIMPLEMENTATION_MILESTONE_DOCUMENT_DESCRIPTION_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}" id="txtIMPLEMENTATION_MILESTONE_DOCUMENT_DESCRIPTION_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}" value="${implementationMilestoneDocument.description}">
                                                                                                    <input type="hidden" name="txtIMPLEMENTATION_MILESTONE_DOCUMENT_URL_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}" id="txtIMPLEMENTATION_MILESTONE_DOCUMENT_URL_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}" value="${implementationMilestoneDocument.url}">
                                                                                                    <input type="hidden" name="txtIMPLEMENTATION_MILESTONE_DOCUMENT_DATE_PUBLISHED_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}" id="txtIMPLEMENTATION_MILESTONE_DOCUMENT_DATE_PUBLISHED_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(implementationMilestoneDocument.datePublished)}">
                                                                                                    <input type="hidden" name="txtIMPLEMENTATION_MILESTONE_DOCUMENT_DATE_MODIFIED_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}" id="txtIMPLEMENTATION_MILESTONE_DOCUMENT_DATE_MODIFIED_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(implementationMilestoneDocument.dateModified)}">
                                                                                                    <input type="hidden" name="txtIMPLEMENTATION_MILESTONE_DOCUMENT_FORMAT_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}" id="txtIMPLEMENTATION_MILESTONE_DOCUMENT_FORMAT_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}" value="${implementationMilestoneDocument.format}">
                                                                                                    <input type="hidden" name="cmbIMPLEMENTATION_MILESTONE_DOCUMENT_LANGUAGE_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}" id="cmbIMPLEMENTATION_MILESTONE_DOCUMENT_LANGUAGE_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}" value="${implementationMilestoneDocument.language}">
                                                                                                </div>
                                                                                                <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                                                                    <button class="button margin-0" onclick="abrirFormularioPrevio('implementation-milestone-document', [${contadorElementosContract}, ${contadorElementosImplementationMilestone}, ${contadorElementosImplementationMilestoneDocument}])"><i class="material-icons">edit</i> Editar</button>
                                                                                                    <button class="button margin-0" onclick="eliminarElemento('implementation-milestone-document', 'implementation-milestone-document_${contadorElementosContract}_${contadorElementosImplementationMilestone}_${contadorElementosImplementationMilestoneDocument}', [${contadorElementosContract}, ${contadorElementosImplementationMilestone}], '${contadorElementosImplementationMilestoneDocument}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <c:set var="contadorElementosImplementationMilestoneDocument" value="${contadorElementosImplementationMilestoneDocument + 1}"></c:set>
                                                                                    </c:forEach>
                                                                                    </div>
                                                                                    <input type="hidden" name="ultimo-identificador-elementos-implementation-milestone-document_${contadorElementosContract}_${contadorElementosImplementationMilestone}" id="ultimo-identificador-elementos-implementation-milestone-document_${contadorElementosContract}_${contadorElementosImplementationMilestone}" value="${contadorElementosImplementationMilestoneDocument}">
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    <div id="contenedor-implementation-milestone-document_${contadorElementosContract}_${contadorElementosImplementationMilestone}" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                                                                    <input type="hidden" name="ultimo-identificador-elementos-implementation-milestone-document_${contadorElementosContract}_${contadorElementosImplementationMilestone}" id="ultimo-identificador-elementos-implementation-milestone-document_${contadorElementosContract}_${contadorElementosImplementationMilestone}" value="0">
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                            <script>
                                                                                document.getElementById("estructura-por-defecto-implementation-milestone-document_${contadorElementosContract}_${contadorElementosImplementationMilestone}").value = obtenerDatosInicialesElementos("implementation-milestone-document", [${contadorElementosContract}, ${contadorElementosImplementationMilestone}]);
                                                                            </script>
                                                                        </fieldset>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <c:set var="contadorElementosImplementationMilestone" value="${contadorElementosImplementationMilestone + 1}"></c:set>
                                                        </c:forEach>
                                                        </ul>
                                                        <input type="hidden" name="ultimo-identificador-elementos-implementation-milestone_${contadorElementosContract}" id="ultimo-identificador-elementos-implementation-milestone_${contadorElementosContract}" value="${contadorElementosImplementationMilestone}">
                                                    </c:when>
                                                    <c:otherwise>
                                                        <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-implementation-milestone_${contadorElementosContract}"></ul>
                                                        <input type="hidden" name="ultimo-identificador-elementos-implementation-milestone_${contadorElementosContract}" id="ultimo-identificador-elementos-implementation-milestone_${contadorElementosContract}" value="0">
                                                    </c:otherwise>
                                                </c:choose>
                                                <script>
                                                    document.getElementById("estructura-por-defecto-implementation-milestone_${contadorElementosContract}").value = obtenerDatosInicialesElementos("implementation-milestone", [${contadorElementosContract}]);
                                                </script>
                                            </fieldset>
                                            <fieldset class="cell fieldset"> 
                                                <input type="hidden" id="estructura-por-defecto-implementation-document_${contadorElementosContract}" value=""> 
                                                <input type="hidden" id="siguiente-identificador-implementation-document_${contadorElementosContract}" value="" data-valor-por-defecto=""> 
                                                <input type="hidden" id="lista-identificadores-por-registrar-implementation-document_${contadorElementosContract}" name="lista-identificadores-por-registrar-implementation-document_${contadorElementosContract}" value="" data-valor-por-defecto="">
                                                <legend title="Documentos y reportes que son parte de la fase de implementaci&oacute;n, por ejemplo, reportes de auditor&iacute;a y evaluaci&oacute;n.">Documentos</legend>
                                                <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                                    <a onclick="restaurarEstadoInicialElemento('implementation-document', [${contadorElementosContract}])"><i class="material-icons secondary">restart_alt</i></a> 
                                                    <a onclick="abrirNuevoFormulario('implementation-document', [${contadorElementosContract}])"><i class="material-icons secondary">add_circle</i></a> 
                                                </div>
                                                <c:choose>
                                                    <c:when test="${contrato.implementation.documents.size() > 0}">
                                                        <c:set var="contadorElementosImplementationDocument" value="0"></c:set>
                                                        <div id="contenedor-implementation-document_${contadorElementosContract}" style="display: flex; flex-direction: column; gap: 10px;">
                                                        <c:forEach items="${contrato.implementation.documents}" var="implementationDocument">
                                                            <div id="implementation-document_${contadorElementosContract}_${contadorElementosImplementationDocument}">
                                                                <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                                                    <div class="cell medium-8">
                                                                        <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">documento</span> - <span id="titulo-elemento-implementation-document_${contadorElementosContract}_${contadorElementosImplementationDocument}">${implementationDocument.id}</span></h6>
                                                                    </div>
                                                                    <div id="datos-elemento-implementation-document_${contadorElementosContract}_${contadorElementosImplementationDocument}">
                                                                        <input type="hidden" name="txtIMPLEMENTATION_DOCUMENT_ID_${contadorElementosContract}_${contadorElementosImplementationDocument}" id="txtIMPLEMENTATION_DOCUMENT_ID_${contadorElementosContract}_${contadorElementosImplementationDocument}" value="${implementationDocument.id}">
                                                                        <input type="hidden" name="cmbIMPLEMENTATION_DOCUMENT_TYPE_${contadorElementosContract}_${contadorElementosImplementationDocument}" id="cmbIMPLEMENTATION_DOCUMENT_TYPE_${contadorElementosContract}_${contadorElementosImplementationDocument}" value="${implementationDocument.documentType}">
                                                                        <input type="hidden" name="txtIMPLEMENTATION_DOCUMENT_TITLE_${contadorElementosContract}_${contadorElementosImplementationDocument}" id="txtIMPLEMENTATION_DOCUMENT_TITLE_${contadorElementosContract}_${contadorElementosImplementationDocument}" value="${implementationDocument.title}">
                                                                        <input type="hidden" name="txtIMPLEMENTATION_DOCUMENT_DESCRIPTION_${contadorElementosContract}_${contadorElementosImplementationDocument}" id="txtIMPLEMENTATION_DOCUMENT_DESCRIPTION_${contadorElementosContract}_${contadorElementosImplementationDocument}" value="${implementationDocument.description}">
                                                                        <input type="hidden" name="txtIMPLEMENTATION_DOCUMENT_URL_${contadorElementosContract}_${contadorElementosImplementationDocument}" id="txtIMPLEMENTATION_DOCUMENT_URL_${contadorElementosContract}_${contadorElementosImplementationDocument}" value="${implementationDocument.url}">
                                                                        <input type="hidden" name="txtIMPLEMENTATION_DOCUMENT_DATE_PUBLISHED_${contadorElementosContract}_${contadorElementosImplementationDocument}" id="txtIMPLEMENTATION_DOCUMENT_DATE_PUBLISHED_${contadorElementosContract}_${contadorElementosImplementationDocument}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(implementationDocument.datePublished)}">
                                                                        <input type="hidden" name="txtIMPLEMENTATION_DOCUMENT_DATE_MODIFIED_${contadorElementosContract}_${contadorElementosImplementationDocument}" id="txtIMPLEMENTATION_DOCUMENT_DATE_MODIFIED_${contadorElementosContract}_${contadorElementosImplementationDocument}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(implementationDocument.dateModified)}">
                                                                        <input type="hidden" name="txtIMPLEMENTATION_DOCUMENT_FORMAT_${contadorElementosContract}_${contadorElementosImplementationDocument}" id="txtIMPLEMENTATION_DOCUMENT_FORMAT_${contadorElementosContract}_${contadorElementosImplementationDocument}" value="${implementationDocument.format}">
                                                                        <input type="hidden" name="cmbIMPLEMENTATION_DOCUMENT_LANGUAGE_${contadorElementosContract}_${contadorElementosImplementationDocument}" id="cmbIMPLEMENTATION_DOCUMENT_LANGUAGE_${contadorElementosContract}_${contadorElementosImplementationDocument}" value="${implementationDocument.language}">
                                                                    </div>
                                                                    <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                                        <button class="button margin-0" onclick="abrirFormularioPrevio('implementation-document', [${contadorElementosContract}, ${contadorElementosImplementationDocument}])"><i class="material-icons">edit</i> Editar</button>
                                                                        <button class="button margin-0" onclick="eliminarElemento('implementation-document', 'implementation-document_${contadorElementosContract}_${contadorElementosImplementationDocument}', [${contadorElementosContract}], '${contadorElementosImplementationDocument}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <c:set var="contadorElementosImplementationDocument" value="${contadorElementosImplementationDocument + 1}"></c:set>
                                                        </c:forEach>
                                                        </div>
                                                        <input type="hidden" name="ultimo-identificador-elementos-implementation-document_${contadorElementosContract}" id="ultimo-identificador-elementos-implementation-document_${contadorElementosContract}" value="${contadorElementosImplementationDocument}">
                                                    </c:when>
                                                    <c:otherwise>
                                                        <div id="contenedor-implementation-document_${contadorElementosContract}" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                                        <input type="hidden" name="ultimo-identificador-elementos-implementation-document_${contadorElementosContract}" id="ultimo-identificador-elementos-implementation-document_${contadorElementosContract}" value="0">
                                                    </c:otherwise>
                                                </c:choose>
                                                <script>
                                                    document.getElementById("estructura-por-defecto-implementation-document_${contadorElementosContract}").value = obtenerDatosInicialesElementos("implementation-document", [${contadorElementosContract}]);
                                                </script>
                                            </fieldset>
                                        </div>
                                    </fieldset>
                                    <fieldset class="cell fieldset"> 
                                        <input type="hidden" id="estructura-por-defecto-relatedProcess_${contadorElementosContract}" value=""> 
                                        <input type="hidden" id="siguiente-identificador-relatedProcess_${contadorElementosContract}" value="" data-valor-por-defecto=""> 
                                        <input type="hidden" id="lista-identificadores-por-registrar-relatedProcess_${contadorElementosContract}" name="lista-identificadores-por-registrar-relatedProcess_${contadorElementosContract}" value="" data-valor-por-defecto="">
                                        <legend title="Los detalles de procesos relacionados, por ejemplo, si el proceso es seguido por uno o m&aacute;s procesos de contrataciones, representados bajo diferentes identificadores de contrataciones abiertas (ocid). Esto se utiliza normalmente para referir a subcontratos o renovaciones y reemplazos de este contrato.">Procesos relacionados</legend>
                                        <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                            <a onclick="restaurarEstadoInicialElemento('relatedProcess', [${contadorElementosContract}])"><i class="material-icons secondary">restart_alt</i></a> 
                                            <a onclick="abrirNuevoFormulario('relatedProcess', [${contadorElementosContract}])"><i class="material-icons secondary">add_circle</i></a> 
                                        </div>
                                        <c:choose>
                                            <c:when test="${contrato.relatedProcesses.size() > 0}">
                                                <c:set var="contadorElementosRelatedProcess" value="0"></c:set>
                                                <div id="contenedor-relatedProcess_${contadorElementosContract}" style="display: flex; flex-direction: column; gap: 10px;">
                                                <c:forEach items="${contrato.relatedProcesses}" var="relatedProcess">
                                                    <div id="relatedProcess_${contadorElementosContract}_${contadorElementosRelatedProcess}">
                                                        <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                                            <div class="cell medium-8">
                                                                <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">proceso relacionado</span> - <span id="titulo-elemento-relatedProcess_${contadorElementosContract}_${contadorElementosRelatedProcess}">${relatedProcess.id}</span></h6>
                                                            </div>
                                                            <div id="datos-elemento-relatedProcess_${contadorElementosContract}_${contadorElementosRelatedProcess}">
                                                                <input type="hidden" name="txtRELATED_PROCESS_ID_${contadorElementosContract}_${contadorElementosRelatedProcess}" id="txtRELATED_PROCESS_ID_${contadorElementosContract}_${contadorElementosRelatedProcess}" value="${relatedProcess.id}">
                                                                <input type="hidden" name="txtRELATED_PROCESS_TITLE_${contadorElementosContract}_${contadorElementosRelatedProcess}" id="txtRELATED_PROCESS_TITLE_${contadorElementosContract}_${contadorElementosRelatedProcess}" value="${relatedProcess.title}">
                                                                <input type="hidden" name="cmbRELATED_PROCESS_SCHEME_${contadorElementosContract}_${contadorElementosRelatedProcess}" id="cmbRELATED_PROCESS_SCHEME_${contadorElementosContract}_${contadorElementosRelatedProcess}" value="${relatedProcess.scheme}">
                                                                <input type="hidden" name="txtRELATED_PROCESS_IDENTIFIER_${contadorElementosContract}_${contadorElementosRelatedProcess}" id="txtRELATED_PROCESS_IDENTIFIER_${contadorElementosContract}_${contadorElementosRelatedProcess}" value="${relatedProcess.identifier}">
                                                                <input type="hidden" name="txtRELATED_PROCESS_URI_${contadorElementosContract}_${contadorElementosRelatedProcess}" id="txtRELATED_PROCESS_URI_${contadorElementosContract}_${contadorElementosRelatedProcess}" value="${relatedProcess.uri}">
                                                                <c:set var="valorRelatedProcessRelationship" value=""></c:set>
                                                                <c:forEach items="${relatedProcess.relationship}" var="relacion" varStatus="contador">
                                                                    <c:if test="${relacion != null}">
                                                                        <c:set var="valorRelatedProcessRelationship" value="${valorRelatedProcessRelationship.concat(relacion).concat(',')}"></c:set>
                                                                    </c:if>
                                                                </c:forEach>
                                                                <input type="hidden" name="chckRELATED_PROCESS_RELATION_SHIP_${contadorElementosContract}_${contadorElementosRelatedProcess}" id="chckRELATED_PROCESS_RELATION_SHIP_${contadorElementosContract}_${contadorElementosRelatedProcess}" value="${valorRelatedProcessRelationship}">
                                                            </div>
                                                            <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                                <button class="button margin-0" onclick="abrirFormularioPrevio('relatedProcess', [${contadorElementosContract}, ${contadorElementosRelatedProcess}])"><i class="material-icons">edit</i> Editar</button>
                                                                <button class="button margin-0" onclick="eliminarElemento('relatedProcess', 'relatedProcess_${contadorElementosContract}_${contadorElementosRelatedProcess}', [${contadorElementosContract}], '${contadorElementosRelatedProcess}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <c:set var="contadorElementosRelatedProcess" value="${contadorElementosRelatedProcess + 1}"></c:set>
                                                </c:forEach>
                                                </div>
                                                <input type="hidden" name="ultimo-identificador-elementos-relatedProcess_${contadorElementosContract}" id="ultimo-identificador-elementos-relatedProcess_${contadorElementosContract}" value="${contadorElementosRelatedProcess}">
                                            </c:when>
                                            <c:otherwise>
                                                <div id="contenedor-relatedProcess_${contadorElementosContract}" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                                <input type="hidden" name="ultimo-identificador-elementos-relatedProcess_${contadorElementosContract}" id="ultimo-identificador-elementos-relatedProcess_${contadorElementosContract}" value="0">
                                            </c:otherwise>
                                        </c:choose>
                                        <script>
                                            document.getElementById("estructura-por-defecto-relatedProcess_${contadorElementosContract}").value = obtenerDatosInicialesElementos("relatedProcess", [${contadorElementosContract}]);
                                        </script>
                                    </fieldset>
                                    <fieldset class="cell fieldset"> 
                                        <input type="hidden" id="estructura-por-defecto-milestone_${contadorElementosContract}" value=""> 
                                        <input type="hidden" id="siguiente-identificador-milestone_${contadorElementosContract}" value="" data-valor-por-defecto=""> 
                                        <input type="hidden" id="lista-identificadores-por-registrar-milestone_${contadorElementosContract}" name="lista-identificadores-por-registrar-milestone_${contadorElementosContract}" value="" data-valor-por-defecto="">
                                        <legend title="Una lista de hitos asociados con la terminaci&oacute;n de este contrato.">Hitos</legend>
                                        <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                            <a onclick="restaurarEstadoInicialElemento('milestone', [${contadorElementosContract}])"><i class="material-icons secondary">restart_alt</i></a> 
                                            <a onclick="abrirNuevoFormulario('milestone', [${contadorElementosContract}])"><i class="material-icons secondary">add_circle</i></a> 
                                        </div>
                                        <c:choose>
                                            <c:when test="${contrato.milestones.size() > 0}">
                                                <c:set var="contadorElementosMilestone" value="0"></c:set>
                                                <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-milestone_${contadorElementosContract}">
                                                <c:forEach items="${contrato.milestones}" var="milestone">
                                                    <li class="accordion-item" data-accordion-item id="milestone_${contadorElementosContract}_${contadorElementosMilestone}">
                                                        <a href="#" class="accordion-title"><span style="text-transform: capitalize;">hito</span> - <span id="titulo-elemento-milestone_${contadorElementosContract}_${contadorElementosMilestone}">${milestone.id}</span></a>
                                                        <div class="accordion-content" data-tab-content>
                                                            <div class="grid-margin-x grid-x">
                                                                <div class="cell padding-horizontal-1" style="display:flex; justify-content: flex-end; gap: 10px; flex-wrap: wrap;">
                                                                    <button class="button margin-0 btnFrmContrataciones" onclick="abrirFormularioPrevio('milestone', [${contadorElementosContract}, ${contadorElementosMilestone}])"><i class="material-icons">edit</i> Editar hito</button>
                                                                    <button class="button margin-0 btnFrmContrataciones" onclick="eliminarElemento('milestone', 'milestone_${contadorElementosContract}_${contadorElementosMilestone}', [${contadorElementosContract}], '${contadorElementosMilestone}')"><i class="material-icons">remove_circle</i> Eliminar hito</button>
                                                                </div>
                                                                <div id="datos-elemento-milestone_${contadorElementosContract}_${contadorElementosMilestone}">
                                                                    <input type="hidden" name="txtMILESTONE_ID_${contadorElementosContract}_${contadorElementosMilestone}" id="txtMILESTONE_ID_${contadorElementosContract}_${contadorElementosMilestone}" value="${milestone.id}">
                                                                    <input type="hidden" name="cmbMILESTONE_TYPE_${contadorElementosContract}_${contadorElementosMilestone}" id="cmbMILESTONE_TYPE_${contadorElementosContract}_${contadorElementosMilestone}" value="${milestone.type}">
                                                                    <input type="hidden" name="txtMILESTONE_TITLE_${contadorElementosContract}_${contadorElementosMilestone}" id="txtMILESTONE_TITLE_${contadorElementosContract}_${contadorElementosMilestone}" value="${milestone.title}">
                                                                    <input type="hidden" name="txtMILESTONE_DESCRIPTION_${contadorElementosContract}_${contadorElementosMilestone}" id="txtMILESTONE_DESCRIPTION_${contadorElementosContract}_${contadorElementosMilestone}" value="${milestone.description}">
                                                                    <input type="hidden" name="txtMILESTONE_CODE_${contadorElementosContract}_${contadorElementosMilestone}" id="txtMILESTONE_CODE_${contadorElementosContract}_${contadorElementosMilestone}" value="${milestone.code}">
                                                                    <input type="hidden" name="txtMILESTONE_DUE_DATE_${contadorElementosContract}_${contadorElementosMilestone}" id="txtMILESTONE_DUE_DATE_${contadorElementosContract}_${contadorElementosMilestone}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(milestone.dueDate)}">
                                                                    <input type="hidden" name="txtMILESTONE_DATE_MET_${contadorElementosContract}_${contadorElementosMilestone}" id="txtMILESTONE_DATE_MET_${contadorElementosContract}_${contadorElementosMilestone}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(milestone.dateMet)}">
                                                                    <input type="hidden" name="txtMILESTONE_DATE_MODIFIED_${contadorElementosContract}_${contadorElementosMilestone}" id="txtMILESTONE_DATE_MODIFIED_${contadorElementosContract}_${contadorElementosMilestone}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(milestone.dateModified)}">
                                                                    <input type="hidden" name="cmbMILESTONE_STATUS_${contadorElementosContract}_${contadorElementosMilestone}" id="cmbMILESTONE_STATUS_${contadorElementosContract}_${contadorElementosMilestone}" value="${milestone.status}">
                                                                </div>
                                                                <fieldset class="cell fieldset"> 
                                                                    <input type="hidden" id="estructura-por-defecto-milestone-document_${contadorElementosContract}_${contadorElementosMilestone}" value=""> 
                                                                    <input type="hidden" id="siguiente-identificador-milestone-document_${contadorElementosContract}_${contadorElementosMilestone}" value="" data-valor-por-defecto=""> 
                                                                    <input type="hidden" id="lista-identificadores-por-registrar-milestone-document_${contadorElementosContract}_${contadorElementosMilestone}" name="lista-identificadores-por-registrar-milestone-document_${contadorElementosContract}_${contadorElementosMilestone}" value="" data-valor-por-defecto="">
                                                                    <legend title="Lista de documentos asociados con este hito.">Documentos</legend>
                                                                    <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                                                        <a onclick="restaurarEstadoInicialElemento('milestone-document', [${contadorElementosContract}, ${contadorElementosMilestone}])"><i class="material-icons secondary">restart_alt</i></a> 
                                                                        <a onclick="abrirNuevoFormulario('milestone-document', [${contadorElementosContract}, ${contadorElementosMilestone}])"><i class="material-icons secondary">add_circle</i></a> 
                                                                    </div>
                                                                    <c:choose>
                                                                        <c:when test="${milestone.documents.size() > 0}">
                                                                            <c:set var="contadorElementosMilestoneDocument" value="0"></c:set>
                                                                            <div id="contenedor-milestone-document_${contadorElementosContract}_${contadorElementosMilestone}" style="display: flex; flex-direction: column; gap: 10px;">
                                                                            <c:forEach items="${milestone.documents}" var="milestoneDocument">
                                                                                <div id="milestone-document_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}">
                                                                                    <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                                                                        <div class="cell medium-8">
                                                                                            <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">documento</span> - <span id="titulo-elemento-milestone-document_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}">${milestoneDocument.id}</span></h6>
                                                                                        </div>
                                                                                        <div id="datos-elemento-milestone-document_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}">
                                                                                            <input type="hidden" name="txtMILESTONE_DOCUMENT_ID_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" id="txtMILESTONE_DOCUMENT_ID_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" value="${milestoneDocument.id}">
                                                                                            <input type="hidden" name="cmbMILESTONE_DOCUMENT_TYPE_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" id="cmbMILESTONE_DOCUMENT_TYPE_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" value="${milestoneDocument.documentType}">
                                                                                            <input type="hidden" name="txtMILESTONE_DOCUMENT_TITLE_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" id="txtMILESTONE_DOCUMENT_TITLE_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" value="${milestoneDocument.title}">
                                                                                            <input type="hidden" name="txtMILESTONE_DOCUMENT_DESCRIPTION_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" id="txtMILESTONE_DOCUMENT_DESCRIPTION_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" value="${milestoneDocument.description}">
                                                                                            <input type="hidden" name="txtMILESTONE_DOCUMENT_URL_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" id="txtMILESTONE_DOCUMENT_URL_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" value="${milestoneDocument.url}">
                                                                                            <input type="hidden" name="txtMILESTONE_DOCUMENT_DATE_PUBLISHED_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" id="txtMILESTONE_DOCUMENT_DATE_PUBLISHED_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(milestoneDocument.datePublished)}">
                                                                                            <input type="hidden" name="txtMILESTONE_DOCUMENT_DATE_MODIFIED_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" id="txtMILESTONE_DOCUMENT_DATE_MODIFIED_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(milestoneDocument.dateModified)}">
                                                                                            <input type="hidden" name="txtMILESTONE_DOCUMENT_FORMAT_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" id="txtMILESTONE_DOCUMENT_FORMAT_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" value="${milestoneDocument.format}">
                                                                                            <input type="hidden" name="cmbMILESTONE_DOCUMENT_LANGUAGE_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" id="cmbMILESTONE_DOCUMENT_LANGUAGE_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}" value="${milestoneDocument.language}">
                                                                                        </div>
                                                                                        <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                                                            <button class="button margin-0" onclick="abrirFormularioPrevio('milestone-document', [${contadorElementosContract}, ${contadorElementosMilestone}, ${contadorElementosMilestoneDocument}])"><i class="material-icons">edit</i> Editar</button>
                                                                                            <button class="button margin-0" onclick="eliminarElemento('milestone-document', 'milestone-document_${contadorElementosContract}_${contadorElementosMilestone}_${contadorElementosMilestoneDocument}', [${contadorElementosContract}, ${contadorElementosMilestone}], '${contadorElementosMilestoneDocument}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <c:set var="contadorElementosMilestoneDocument" value="${contadorElementosMilestoneDocument + 1}"></c:set>
                                                                            </c:forEach>
                                                                            </div>
                                                                            <input type="hidden" name="ultimo-identificador-elementos-milestone-document_${contadorElementosContract}_${contadorElementosMilestone}" id="ultimo-identificador-elementos-milestone-document_${contadorElementosContract}_${contadorElementosMilestone}" value="${contadorElementosMilestoneDocument}">
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <div id="contenedor-milestone-document_${contadorElementosContract}_${contadorElementosMilestone}" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                                                            <input type="hidden" name="ultimo-identificador-elementos-milestone-document_${contadorElementosContract}_${contadorElementosMilestone}" id="ultimo-identificador-elementos-milestone-document_${contadorElementosContract}_${contadorElementosMilestone}" value="0">
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                    <script>
                                                                        document.getElementById("estructura-por-defecto-milestone-document_${contadorElementosContract}_${contadorElementosMilestone}").value = obtenerDatosInicialesElementos("milestone-document", [${contadorElementosContract}, ${contadorElementosMilestone}]);
                                                                    </script>
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <c:set var="contadorElementosMilestone" value="${contadorElementosMilestone + 1}"></c:set>
                                                </c:forEach>
                                                </ul>
                                                <input type="hidden" name="ultimo-identificador-elementos-milestone_${contadorElementosContract}" id="ultimo-identificador-elementos-milestone_${contadorElementosContract}" value="${contadorElementosMilestone}">
                                            </c:when>
                                            <c:otherwise>
                                                <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-milestone_${contadorElementosContract}"></ul>
                                                <input type="hidden" name="ultimo-identificador-elementos-milestone_${contadorElementosContract}" id="ultimo-identificador-elementos-milestone_${contadorElementosContract}" value="0">
                                            </c:otherwise>
                                        </c:choose>
                                        <script>
                                            document.getElementById("estructura-por-defecto-milestone_${contadorElementosContract}").value = obtenerDatosInicialesElementos("milestone", [${contadorElementosContract}]);
                                        </script>
                                    </fieldset>
                                    <fieldset class="cell fieldset"> 
                                        <input type="hidden" id="estructura-por-defecto-amendment_${contadorElementosContract}" value=""> 
                                        <input type="hidden" id="siguiente-identificador-amendment_${contadorElementosContract}" value="" data-valor-por-defecto=""> 
                                        <input type="hidden" id="lista-identificadores-por-registrar-amendment_${contadorElementosContract}" name="lista-identificadores-por-registrar-amendment_${contadorElementosContract}" value="" data-valor-por-defecto="">
                                        <legend title="Una enmienda de contrato es un cambio formal a los detalles del mismo y generalmente implica la publicaci&oacute;n de una nueva entrega o aviso de una nueva licitaci&oacute;n, as&iacute; como de otros documentos de detallan el cambio. La raz&oacute;n y una descripci&oacute;n de los cambios realizados se pueden proveer aqu&iacute;.">Enmiendas</legend>
                                        <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                            <a onclick="restaurarEstadoInicialElemento('amendment', [${contadorElementosContract}])"><i class="material-icons secondary">restart_alt</i></a> 
                                            <a onclick="abrirNuevoFormulario('amendment', [${contadorElementosContract}])"><i class="material-icons secondary">add_circle</i></a> 
                                        </div>
                                        <c:choose>
                                            <c:when test="${contrato.amendments.size() > 0}">
                                                <c:set var="contadorElementosAmendment" value="0"></c:set>
                                                <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-amendment_${contadorElementosContract}">
                                                <c:forEach items="${contrato.amendments}" var="amendment">
                                                    <li class="accordion-item" data-accordion-item id="amendment_${contadorElementosContract}_${contadorElementosAmendment}">
                                                        <a href="#" class="accordion-title"><span style="text-transform: capitalize;">enmienda</span> - <span id="titulo-elemento-amendment_${contadorElementosContract}_${contadorElementosAmendment}">${amendment.id}</span></a>
                                                        <div class="accordion-content" data-tab-content>
                                                            <div class="grid-margin-x grid-x">
                                                                <div class="cell padding-horizontal-1" style="display:flex; justify-content: flex-end; gap: 10px; flex-wrap: wrap;">
                                                                    <button class="button margin-0 btnFrmContrataciones" onclick="abrirFormularioPrevio('amendment', [${contadorElementosContract}, ${contadorElementosAmendment}])"><i class="material-icons">edit</i> Editar enmienda</button>
                                                                    <button class="button margin-0 btnFrmContrataciones" onclick="eliminarElemento('amendment', 'amendment_${contadorElementosContract}_${contadorElementosAmendment}', [${contadorElementosContract}], '${contadorElementosAmendment}')"><i class="material-icons">remove_circle</i> Eliminar enmienda</button>
                                                                </div>
                                                                <div id="datos-elemento-amendment_${contadorElementosContract}_${contadorElementosAmendment}">
                                                                    <input type="hidden" name="txtAMENDMENT_ID_${contadorElementosContract}_${contadorElementosAmendment}" id="txtAMENDMENT_ID_${contadorElementosContract}_${contadorElementosAmendment}" value="${amendment.id}">
                                                                    <input type="hidden" name="txtAMENDMENT_DATE_${contadorElementosContract}_${contadorElementosAmendment}" id="txtAMENDMENT_DATE_${contadorElementosContract}_${contadorElementosAmendment}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(amendment.date)}">
                                                                    <input type="hidden" name="txtAMENDMENT_RATIONALE_${contadorElementosContract}_${contadorElementosAmendment}" id="txtAMENDMENT_RATIONALE_${contadorElementosContract}_${contadorElementosAmendment}" value="${amendment.rationale}">
                                                                    <input type="hidden" name="txtAMENDMENT_DESCRIPTION_${contadorElementosContract}_${contadorElementosAmendment}" id="txtAMENDMENT_DESCRIPTION_${contadorElementosContract}_${contadorElementosAmendment}" value="${amendment.description}">
                                                                    <input type="hidden" name="txtAMENDMENT_AMENDS_RELEASE_ID_${contadorElementosContract}_${contadorElementosAmendment}" id="txtAMENDMENT_AMENDS_RELEASE_ID_${contadorElementosContract}_${contadorElementosAmendment}" value="${amendment.amendsReleaseID}">
                                                                    <input type="hidden" name="txtAMENDMENT_RELEASE_ID_${contadorElementosContract}_${contadorElementosAmendment}" id="txtAMENDMENT_RELEASE_ID_${contadorElementosContract}_${contadorElementosAmendment}" value="${amendment.releaseID}">
                                                                </div>
                                                                <fieldset class="cell fieldset"> 
                                                                    <input type="hidden" id="estructura-por-defecto-change_${contadorElementosContract}_${contadorElementosAmendment}" value=""> 
                                                                    <input type="hidden" id="siguiente-identificador-change_${contadorElementosContract}_${contadorElementosAmendment}" value="" data-valor-por-defecto=""> 
                                                                    <input type="hidden" id="lista-identificadores-por-registrar-change_${contadorElementosContract}_${contadorElementosAmendment}" name="lista-identificadores-por-registrar-change_${contadorElementosContract}_${contadorElementosAmendment}" value="" data-valor-por-defecto="">
                                                                    <legend title="Una lista de cambios que describen los campos que cambiaron y sus valores previos.">Campos enmendados</legend>
                                                                    <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                                                        <a onclick="restaurarEstadoInicialElemento('change', [${contadorElementosContract}, ${contadorElementosAmendment}])"><i class="material-icons secondary">restart_alt</i></a> 
                                                                        <a onclick="abrirNuevoFormulario('change', [${contadorElementosContract}, ${contadorElementosAmendment}])"><i class="material-icons secondary">add_circle</i></a> 
                                                                    </div>
                                                                    <c:choose>
                                                                        <c:when test="${amendment.changes.size() > 0}">
                                                                            <c:set var="contadorElementosAmendmentChange" value="0"></c:set>
                                                                            <div id="contenedor-change_${contadorElementosContract}_${contadorElementosAmendment}" style="display: flex; flex-direction: column; gap: 10px;">
                                                                            <c:forEach items="${amendment.changes}" var="amendmentChange">
                                                                                <div id="change_${contadorElementosContract}_${contadorElementosAmendment}_${contadorElementosAmendmentChange}">
                                                                                    <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                                                                        <div class="cell medium-8">
                                                                                            <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">cambio</span> - <span id="titulo-elemento-change_${contadorElementosContract}_${contadorElementosAmendment}_${contadorElementosAmendmentChange}">${amendmentChange.property}</span></h6>
                                                                                        </div>
                                                                                        <div id="datos-elemento-change_${contadorElementosContract}_${contadorElementosAmendment}_${contadorElementosAmendmentChange}">
                                                                                            <input type="hidden" name="txtCHANGE_PROPERTY_${contadorElementosContract}_${contadorElementosAmendment}_${contadorElementosAmendmentChange}" id="txtCHANGE_PROPERTY_${contadorElementosContract}_${contadorElementosAmendment}_${contadorElementosAmendmentChange}" value="${amendmentChange.property}">
                                                                                            <input type="hidden" name="txtCHANGE_FORMATER_VALUE_${contadorElementosContract}_${contadorElementosAmendment}_${contadorElementosAmendmentChange}" id="txtCHANGE_FORMATER_VALUE_${contadorElementosContract}_${contadorElementosAmendment}_${contadorElementosAmendmentChange}" value="${amendmentChange.former_value}">
                                                                                        </div>
                                                                                        <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                                                            <button class="button margin-0" onclick="abrirFormularioPrevio('change', [${contadorElementosContract}, ${contadorElementosAmendment}, ${contadorElementosAmendmentChange}])"><i class="material-icons">edit</i> Editar</button>
                                                                                            <button class="button margin-0" onclick="eliminarElemento('change', 'change_${contadorElementosContract}_${contadorElementosAmendment}_${contadorElementosAmendmentChange}', [${contadorElementosContract}, ${contadorElementosAmendment}], '${contadorElementosAmendmentChange}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <c:set var="contadorElementosAmendmentChange" value="${contadorElementosAmendmentChange + 1}"></c:set>
                                                                            </c:forEach>
                                                                            </div>
                                                                            <input type="hidden" name="ultimo-identificador-elementos-change_${contadorElementosContract}_${contadorElementosAmendment}" id="ultimo-identificador-elementos-change_${contadorElementosContract}_${contadorElementosAmendment}" value="${contadorElementosAmendmentChange}">
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <div id="contenedor-change_${contadorElementosContract}_${contadorElementosAmendment}" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                                                            <input type="hidden" name="ultimo-identificador-elementos-change_${contadorElementosContract}_${contadorElementosAmendment}" id="ultimo-identificador-elementos-change_${contadorElementosContract}_${contadorElementosAmendment}" value="0">
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                    <script>
                                                                        document.getElementById("estructura-por-defecto-change_${contadorElementosContract}_${contadorElementosAmendment}").value = obtenerDatosInicialesElementos("change", [${contadorElementosContract}, ${contadorElementosAmendment}]);
                                                                    </script>
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <c:set var="contadorElementosAmendment" value="${contadorElementosAmendment + 1}"></c:set>
                                                </c:forEach>
                                                </ul>
                                                <input type="hidden" name="ultimo-identificador-elementos-amendment_${contadorElementosContract}" id="ultimo-identificador-elementos-amendment_${contadorElementosContract}" value="${contadorElementosAmendment}">
                                            </c:when>
                                            <c:otherwise>
                                                <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-amendment_${contadorElementosContract}"></ul>
                                                <input type="hidden" name="ultimo-identificador-elementos-amendment_${contadorElementosContract}" id="ultimo-identificador-elementos-amendment_${contadorElementosContract}" value="0">
                                            </c:otherwise>
                                        </c:choose>
                                        <script>
                                            document.getElementById("estructura-por-defecto-amendment_${contadorElementosContract}").value = obtenerDatosInicialesElementos("amendment", [${contadorElementosContract}]);
                                        </script>
                                    </fieldset>
                                    <fieldset class="cell fieldset"> 
                                        <input type="hidden" id="estructura-por-defecto-amendment-principal-change_${contadorElementosContract}" value=""> 
                                        <input type="hidden" id="siguiente-identificador-amendment-principal-change_${contadorElementosContract}" value="" data-valor-por-defecto=""> 
                                        <input type="hidden" id="lista-identificadores-por-registrar-amendment-principal-change_${contadorElementosContract}" name="lista-identificadores-por-registrar-amendment-principal-change_${contadorElementosContract}" value="" data-valor-por-defecto="">
                                        <legend title="Una lista de cambios que describen los campos que cambiaron y sus valores previos.">Campos enmendados de la enmienda principal</legend>
                                        <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                            <a onclick="restaurarEstadoInicialElemento('amendment-principal-change', [${contadorElementosContract}])"><i class="material-icons secondary">restart_alt</i></a> 
                                            <a onclick="abrirNuevoFormulario('amendment-principal-change', [${contadorElementosContract}])"><i class="material-icons secondary">add_circle</i></a> 
                                        </div>
                                        <c:choose>
                                            <c:when test="${contrato.amendment.changes.size() > 0}">
                                                <c:set var="contadorElementosAmendmetPrincipalChange" value="0"></c:set>
                                                <div id="contenedor-amendment-principal-change_${contadorElementosContract}" style="display: flex; flex-direction: column; gap: 10px;">
                                                <c:forEach items="${contrato.amendment.changes}" var="amendmentPrincipalChange">
                                                    <div id="amendment-principal-change_${contadorElementosContract}_${contadorElementosAmendmetPrincipalChange}">
                                                        <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                                            <div class="cell medium-8">
                                                                <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">cambio</span> - <span id="titulo-elemento-amendment-principal-change_${contadorElementosContract}_${contadorElementosAmendmetPrincipalChange}">${amendmentPrincipalChange.property}</span></h6>
                                                            </div>
                                                            <div id="datos-elemento-amendment-principal-change_${contadorElementosContract}_${contadorElementosAmendmetPrincipalChange}">
                                                                <input type="hidden" name="txtAMENDMENT_PRINCIPAL_CHANGE_PROPERTY_${contadorElementosContract}_${contadorElementosAmendmetPrincipalChange}" id="txtAMENDMENT_PRINCIPAL_CHANGE_PROPERTY_${contadorElementosContract}_${contadorElementosAmendmetPrincipalChange}" value="${amendmentPrincipalChange.property}">
                                                                <input type="hidden" name="txtAMENDMENT_PRINCIPAL_CHANGE_FORMATER_VALUE_${contadorElementosContract}_${contadorElementosAmendmetPrincipalChange}" id="txtAMENDMENT_PRINCIPAL_CHANGE_FORMATER_VALUE_${contadorElementosContract}_${contadorElementosAmendmetPrincipalChange}" value="${amendmentPrincipalChange.former_value}">
                                                            </div>
                                                            <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                                <button class="button margin-0" onclick="abrirFormularioPrevio('amendment-principal-change', [${contadorElementosContract}, ${contadorElementosAmendmetPrincipalChange}])"><i class="material-icons">edit</i> Editar</button>
                                                                <button class="button margin-0" onclick="eliminarElemento('amendment-principal-change', 'amendment-principal-change_${contadorElementosContract}_${contadorElementosAmendmetPrincipalChange}', [${contadorElementosContract}], '${contadorElementosAmendmetPrincipalChange}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <c:set var="contadorElementosAmendmetPrincipalChange" value="${contadorElementosAmendmetPrincipalChange + 1}"></c:set>
                                                </c:forEach>
                                                </div>
                                                <input type="hidden" name="ultimo-identificador-elementos-amendment-principal-change_${contadorElementosContract}" id="ultimo-identificador-elementos-amendment-principal-change_${contadorElementosContract}" value="${contadorElementosAmendmetPrincipalChange}">
                                            </c:when>
                                            <c:otherwise>
                                                <div id="contenedor-amendment-principal-change_${contadorElementosContract}" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                                <input type="hidden" name="ultimo-identificador-elementos-amendment-principal-change_${contadorElementosContract}" id="ultimo-identificador-elementos-amendment-principal-change_${contadorElementosContract}" value="0">
                                            </c:otherwise>
                                        </c:choose>
                                        <script>
                                            document.getElementById("estructura-por-defecto-amendment-principal-change_${contadorElementosContract}").value = obtenerDatosInicialesElementos("amendment-principal-change", [${contadorElementosContract}]);
                                        </script>
                                    </fieldset>
                                </div>
                            </div>
                        </li>
                        <c:set var="contadorElementosContract" value="${contadorElementosContract + 1}"></c:set>
                    </c:forEach>
                    </ul>
                    <input type="hidden" name="ultimo-identificador-elementos-contract" id="ultimo-identificador-elementos-contract" value="${contadorElementosContract}">
                </c:when>
                <c:otherwise>
                    <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-contract"></ul>
                    <input type="hidden" name="ultimo-identificador-elementos-contract" id="ultimo-identificador-elementos-contract" value="0">
                </c:otherwise>
            </c:choose>   
            <div class="grid-x grid-margin-x margin-top-1">
                <fieldset class="cell medium-6">
                    <input type="button" class="button expanded secondary" onclick="restaurarEstadoInicialElemento('contract', []);" value="${(bolEsFormulariosDeEdicion == true) ? 'Restaurar' : 'Reiniciar'}"/>
                </fieldset>
                <fieldset class="cell medium-6">
                    <input type="button" onclick="validarFormulario('frmRegistrarSeccionContratos');" class="button expanded" id="btnRegistrarValoresFormulario" value="${(bolEsFormulariosDeEdicion == true) ? 'Modificar datos' : 'Continuar'}"/>
                </fieldset>
            </div>
        </form>
    </div>
</div>

<%-- formulario de un contrato (TOP) --%>
<div id="modalFrmContract" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Contrato</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmContract" id="frmContract" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-contract" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-4">ID de contrato
                            <input type="text" id="txtCONTRACT_ID" required placeholder="p. ej. 1" value="" title="El identificador para este contrato.  Debe ser &uacute;nico y no debe cambiar en el Proceso de Contrataciones Abiertas del cual es parte (definido por un ocid &uacute;nico)."/>
                            <span class="form-error" data-form-error-for="txtCONTRACT_ID" id="spanCONTRACT_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">ID de adjudicaci&oacute;n
                            <input type="text" class="" id="txtCONTRACT_AWARD_ID" placeholder="p. ej. 1" value="" title="El identificador de adjudicaci&oacute;n contra el cual se est&aacute; expidiendo este contrato."/>
                            <span class="form-error " data-form-error-for="txtCONTRACT_AWARD_ID" id="spanCONTRACT_AWARD_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">T&iacute;tulo
                            <input type="text" id="txtCONTRACT_TITLE" placeholder="p. ej. T&iacute;tulo del contrato" value="" title="T&iacute;tulo del contrato."/>
                            <span class="form-error" data-form-error-for="txtCONTRACT_TITLE" id="spanCONTRACT_TITLE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Descripci&oacute;n
                            <input type="text" id="txtCONTRACT_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n del contrato" value="" title="Descripci&oacute;n del contrato."/>
                            <span class="form-error" data-form-error-for="txtCONTRACT_DESCRIPTION" id="spanCONTRACT_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Estado del contrato
                            <select id="cmbCONTRACT_STATUS" title="El estatus actual del contrato.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${estadosContrato}" var="estado">
                                    <option value="${estado.clave}" >${estado.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbCONTRACT_STATUS" id="spanCONTRACT_STATUS">
                                Elige una opci&oacute;n.
                            </span>
                        </label>  
                        <label class="cell medium-4">Fecha de firma
                            <input type="date" id="txtCONTRACT_DATE_SIGNED" value="" title="La fecha en que se firm&oacute; el contrato. En el caso de m&uacute;ltiples firmas, la fecha de la &uacute;ltima firma."/>
                            <span class="form-error" data-form-error-for="txtCONTRACT_DATE_SIGNED" id="spanCONTRACT_DATE_SIGNED">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>        
                        <fieldset class="fieldset cell">
                            <legend title="Las fechas de inicio y fin del contrato.">Periodo</legend>
                            <div class="grid-margin-x grid-x">
                                <label class="cell medium-6">Fecha de inicio
                                    <input type="date" id="txtCONTRACT_PERIOD_START_DATE" value="" title="La fecha de inicio del per&iacute;odo. Cuando se sabe, se debe dar una fecha de inicio precisa." />
                                    <span class="form-error" data-form-error-for="txtCONTRACT_PERIOD_START_DATE" id="spanCONTRACT_PERIOD_START_DATE">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Fecha de fin
                                    <input type="date" id="txtCONTRACT_PERIOD_END_DATE" value="" title="La fecha de conclusi&oacute;n del per&iacute;odo. Cuando se sabe, se debe dar una fecha de conclusi&oacute;n precisa."/>
                                    <span class="form-error" data-form-error-for="txtCONTRACT_PERIOD_END_DATE" id="spanCONTRACT_PERIOD_END_DATE">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Extensi&oacute;n m&aacute;xima
                                    <input type="date" id="txtCONTRACT_PERIOD_MAX_EXTENT_DATE" value="" title="El per&iacute;odo no puede extenderse despu&eacute;s de esta fecha. Este campo debe usarse para expresar la fecha m&aacute;xima disponible para la extensi&oacute;n o renovaci&oacute;n de este per&iacute;odo."/>
                                    <span class="form-error" data-form-error-for="txtCONTRACT_PERIOD_MAX_EXTENT_DATE" id="spanCONTRACT_PERIOD_MAX_EXTENT_DATE">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Duraci&oacute;n (d&iacute;as)
                                    <input type="number" id="txtCONTRACT_PERIOD_DURATION_IN_DATE" value="" min="0" placeholder="p. ej. 0" title="El nivel m&aacute;ximo de duraci&oacute;n de este per&iacute;odo en d&iacute;as."/>
                                    <span class="form-error" data-form-error-for="txtCONTRACT_PERIOD_DURATION_IN_DATE" id="spanCONTRACT_PERIOD_DURATION_IN_DATE">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="fieldset cell">
                            <legend title="Un valor o monto total de este contrato. Un valor negativo indica que el contrato implicar&aacute; pagos del proveedor al comprador (com&uacute;nmente usado en contratos de concesi&oacute;n).">Valor</legend>
                            <div class="grid-margin-x grid-x">
                                <label class="cell medium-6">Monto
                                    <input type="number" id="txtCONTRACT_AMOUNT" placeholder="p. ej. 0" min="0" value="" title="Monto como una cifra."/>
                                    <span class="form-error" data-form-error-for="txtCONTRACT_AMOUNT" id="spanCONTRACT_AMOUNT">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Moneda
                                    <select id="cmbCONTRACT_CURRENCY" title="La moneda del monto.">
                                        <option value="">--Elige una opci&oacute;n--</option>
                                        <c:forEach items="${tipoMoneda}" var="moneda">
                                            <option value="${moneda.clave}" >${moneda.valor}</option>
                                        </c:forEach>
                                    </select>
                                    <span class="form-error" data-form-error-for="cmbCONTRACT_CURRENCY" id="spanCONTRACT_CURRENCY">
                                        Elige una opci&oacute;n.
                                    </span>
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="fieldset cell">
                            <legend title="Informaci�n de enmienda">Enmienda principal</legend>
                            <div class="grid-margin-x grid-x">
                                <label class="cell medium-4">ID <span style="color: red">**</span>
                                    <input type="text" id="txtCONTRACT_AMENDMENT_ID" placeholder="p. ej. 1" value="" title="Un identificador para esta enmienda: com&uacute;nmente el n&uacute;mero de enmienda."/>
                                    <span class="form-error" data-form-error-for="txtCONTRACT_AMENDMENT_ID" id="spanCONTRACT_CONTRACT_AMENDMENT_ID">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Fecha de enmienda
                                    <input type="date" id="txtCONTRACT_AMENDMENT_DATE" value="" title="La fecha de esta enmienda." />
                                    <span class="form-error" data-form-error-for="txtCONTRACT_AMENDMENT_DATE" id="spanCONTRACT_AMENDMENT_DATE">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Justificaci&oacute;n
                                    <input type="text" id="txtCONTRACT_AMENDMENT_RATIONALE" placeholder="p. ej. Explicaci&oacute;n de la enmienda" value="" title="Una explicaci&oacute;n de la enmienda."/>
                                    <span class="form-error" data-form-error-for="txtCONTRACT_AMENDMENT_RATIONALE" id="spanCONTRACT_AMENDMENT_RATIONALE">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Descripci&oacute;n
                                    <input type="text" id="txtCONTRACT_AMENDMENT_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n de la enmienda" value="" title="Un texto libre o semi estructurado, describiendo los cambios hechos en esta enmienda."/>
                                    <span class="form-error" data-form-error-for="txtCONTRACT_AMENDMENT_DESCRIPTION" id="spanCONTRACT_AMENDMENT_DESCRIPTION">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Entrega enmendada (identificador)
                                    <input type="text" id="txtCONTRACT_AMENDMENT_AMENDS_RELEASE_ID" placeholder="p. ej. 1" value="" title="Provee el identificador (release.id) de la entrega OCDS (de este proceso de contrataci&oacute;n) que provee los valores para este proceso de contrataci&oacute;n &#42;&#42;antes&#42;&#42; de realizada la enmienda."/>
                                    <span class="form-error" data-form-error-for="txtCONTRACT_AMENDMENT_AMENDS_RELEASE_ID" id="spanCONTRACT_AMENDMENT_AMENDS_RELEASE_ID">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Entrega de enmienda (identificador)
                                    <input type="text" id="txtCONTRACT_AMENDMENT_RELEASE_ID" placeholder="p. ej. 1" value="" title="Provee el identificador (release.id) de la entrega OCDS (de este proceso de contrataci&oacute;n) que provee los valores para este proceso de contrataci&oacute;n &#42;&#42;despu&eacute;s&#42;&#42; de realizada la enmienda." />
                                    <span class="form-error" data-form-error-for="txtCONTRACT_AMENDMENT_RELEASE_ID" id="spanCONTRACT_AMENDMENT_RELEASE_ID">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-contract"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-contract"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un contrato (BOTTOM) --%>
<%-- formulario de un articulo (TOP) --%>
<div id="modalFrmItem" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Art&iacute;culo</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmItem" id="frmItem" method="post" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-item" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-6">ID
                            <input type="text" id="txtITEM_ID" required placeholder="p. ej. 1" value="" title="Un identificador local al cual hacer referencia y con el cual unir los art&iacute;culos. Debe de ser &uacute;nico en relaci&oacute;n a los dem&aacute;s &iacute;tems del mismo proceso de contrataci&oacute;n presentes en la matriz de art&iacute;culos."/>
                            <span class="form-error" data-form-error-for="txtITEM_ID" id="spanITEM_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Descripci&oacute;n
                            <input type="text" id="txtITEM_DESCRIPTION" placeholder="p. ej. Breve descripci&oacute;n de los bienes o servicios" value="" title="Una descripci&oacute;n de los bienes o servicios objetos del procedimiento de contrataci&oacute;n." />
                            <span class="form-error" data-form-error-for="txtITEM_DESCRIPTION" id="spanITEM_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <fieldset class="fieldset cell">
                            <legend title="La clasificaci&oacute;n primaria para un art&iacute;culo.">Clasificaci&oacute;n</legend>
                            <div class="grid-x grid-margin-x">
                                <label class="cell medium-6">ID
                                    <input type="text" id="txtITEM_CLASSIFICATION_ID" placeholder="p. ej. 1" value="" title="El c&oacute;digo de clasificaci&oacute;n que se obtiene del esquema." />
                                    <span class="form-error" data-form-error-for="txtITEM_CLASSIFICATION_ID" id="spanITEM_CLASSIFICATION_ID">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Esquema
                                    <select id="cmbITEM_CLASSIFICATION_SCHEME" title="El esquema o lista de c&oacute;digo del cual se obtiene el c&oacute;digo de clasificaci&oacute;n. Para clasificaciones de art&iacute;culos de linea.">
                                        <option value="">--Elige una opci&oacute;n--</option>
                                        <c:forEach items="${esquemasDeClasificacion}" var="esquema">
                                            <option value="${esquema.clave}" >${esquema.valor}</option>
                                        </c:forEach>
                                    </select>
                                    <span class="form-error" data-form-error-for="cmbITEM_CLASSIFICATION_SCHEME" id="spanITEM_CLASSIFICATION_SCHEME">
                                        Elige una opci&oacute;n.
                                    </span>
                                </label>
                                <label class="cell medium-6">Descripci&oacute;n
                                    <input type="text" id="txtITEM_CLASSIFICATION_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n de la clasificaci&oacute;n" value="" title="Una descripci&oacute;n textual o t&iacute;tulo del c&oacute;digo de clasificaci&oacute;n." />
                                    <span class="form-error" data-form-error-for="txtITEM_CLASSIFICATION_DESCRIPTION" id="spanITEM_CLASSIFICATION_DESCRIPTION">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">URI
                                    <input type="text" id="txtITEM_CLASSIFICATION_URI" placeholder="p. ej. www.url.com" pattern="url" value="" title="Un URI para identificar &uacute;nicamente el c&oacute;digo de clasificaci&oacute;n."/>
                                    <span class="form-error" data-form-error-for="txtITEM_CLASSIFICATION_URI" id="spanITEM_CLASSIFICATION_URI">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                            </div>
                        </fieldset>
                        <label class="cell">Cantidad
                            <input type="number" id="txtITEM_QUANTITY" placeholder="p. ej. 1" min="0" value="" title="El n&uacute;mero de unidades que se dan."/>
                            <span class="form-error" data-form-error-for="txtITEM_QUANTITY" id="spanITEM_QUANTITY">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <fieldset class="fieldset cell">
                            <legend title="Una descripci&oacute;n de la unidad en la cual los suministros, servicios o trabajos est&aacute;n provistos (ej. horas, kilos) y el precio por unidad.">Unidad</legend>
                            <div class="grid-x grid-margin-x">
                                <label class="cell medium-6">ID
                                    <input type="text" id="txtITEM_UNIT_ID" placeholder="p. ej. 1" value="" title="El identificador de la lista de c&oacute;digos a la que se hace referencia en la propiedad de esquema."/>
                                    <span class="form-error" data-form-error-for="txtITEM_UNIT_ID" id="spanITEM_UNIT_ID">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Esquema
                                    <select id="cmbITEM_UNIT_SCHEME" title="La lista de la cual se obtienen los identificadores de unidades de medida. Se recomienda &#39;UNCEFACT&#39;.">
                                        <option value="">--Elige una opci&oacute;n--</option>
                                        <c:forEach items="${esquemasDeUnidad}" var="esquema">
                                            <option value="${esquema.clave}">${esquema.valor}</option>
                                        </c:forEach>
                                    </select>
                                    <span class="form-error" data-form-error-for="cmbITEM_UNIT_SCHEME" id="spanITEM_UNIT_SCHEME">
                                        Elige una opci&oacute;n.
                                    </span>
                                </label>
                                <label class="cell medium-6">Nombre
                                    <input type="text" id="txtITEM_UNIT_NAME" placeholder="p. ej. Nombre de la unidad" value="" title="Nombre de la unidad." />
                                    <span class="form-error" data-form-error-for="txtITEM_UNIT_NAME" id="spanITEM_UNIT_NAME">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">URI
                                    <input type="text" id="txtITEM_UNIT_URI" placeholder="p. ej. www.url.com" pattern="url" value="" title="Un URI le&iacute;ble por m&aacute;quina para la unidad de medida, dada por el esquema."/>
                                    <span class="form-error" data-form-error-for="txtITEM_UNIT_URI" id="spanITEM_UNIT_URI">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <fieldset class="fieldset cell">
                                    <legend title="El valor monetario de una unidad.">Valor</legend>
                                    <div class="grid-margin-x grid-x">
                                        <label class="cell medium-6">Monto
                                            <input type="number" id="txtITEM_UNIT_AMOUNT" placeholder="p. ej. 0" min="0" value="" title="Monto como una cifra."/>
                                            <span class="form-error" data-form-error-for="txtITEM_UNIT_AMOUNT" id="spanITEM_UNIT_AMOUNT">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-6">Moneda
                                            <select id="cmbITEM_UNIT_CURRENCY" title="La moneda del monto.">
                                                <option value="">--Elige una opci&oacute;n--</option>
                                                <c:forEach items="${tipoMoneda}" var="moneda">
                                                    <option value="${moneda.clave}">${moneda.valor}</option>
                                                </c:forEach>
                                            </select>
                                            <span class="form-error" data-form-error-for="cmbITEM_UNIT_CURRENCY" id="spanITEM_UNIT_CURRENCY">
                                                Elige una opci&oacute;n.
                                            </span>
                                        </label>
                                    </div>
                                </fieldset>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-item"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-item"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un articulo (BOTTOM) --%>
<%-- formulario de una clasificacion (TOP) --%>
<div id="modalFrmClassification" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Clasificaci&oacute;n</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmClassification" id="frmClassification" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-classification" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-6">ID
                            <input type="text" id="txtCLASSIFICATION_ID" required placeholder="p. ej. 1" value="" title="El c&oacute;digo de clasificaci&oacute;n que se obtiene del esquema."/>
                            <span class="form-error" data-form-error-for="txtCLASSIFICATION_ID" id="spanCLASSIFICATION_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Esquema
                            <select id="cmbCLASSIFICATION_SCHEME" title="El esquema o lista de c&oacute;digo del cual se obtiene el c&oacute;digo de clasificaci&oacute;n. Para clasificaciones de art&iacute;culos de linea.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${esquemasDeClasificacion}" var="esquema">
                                    <option value="${esquema.clave}" >${esquema.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbCLASSIFICATION_SCHEME" id="spanCLASSIFICATION_SCHEME">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                        <label class="cell medium-6">Descripci&oacute;n
                            <input type="text" id="txtCLASSIFICATION_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n de la clasificaci&oacute;n" value="" title="Una descripci&oacute;n textual o t&iacute;tulo del c&oacute;digo de clasificaci&oacute;n." />
                            <span class="form-error" data-form-error-for="txtCLASSIFICATION_DESCRIPTION" id="spanCLASSIFICATION_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">URI
                            <input type="text" id="txtCLASSIFICATION_URI" placeholder="p. ej. www.url.com" pattern="url" value="" title="Un URI para identificar &uacute;nicamente el c&oacute;digo de clasificaci&oacute;n."/>
                            <span class="form-error" data-form-error-for="txtCLASSIFICATION_URI" id="spanCLASSIFICATION_URI">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-classification"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-classification"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de una clasificacion (BOTTOM) --%>
<%-- formulario de un proceso relacionado (TOP) --%>
<div id="modalFrmRelatedProcess" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Proceso relacionado</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmRelatedProcess" id="frmRelatedProcess" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-relatedProcess" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-6">ID de relaci&oacute;n
                            <input type="text" id="txtRELATED_PROCESS_ID" required placeholder="p. ej. 1" value="" title="Un identificador local para esta relaci&oacute;n, &uacute;nico dentro de esta lista." />
                            <span class="form-error" data-form-error-for="txtRELATED_PROCESS_ID" id="spanRELATED_PROCESS_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">T&iacute;tulo de proceso relacionado
                            <input type="text" name="txtRELATED_PROCESS_TITLE" id="txtRELATED_PROCESS_TITLE" placeholder="p. ej. T&iacute;tulo de proceso relacionado" value="" title="El t&iacute;tulo del proceso relacionado, cuando se hace referencia a un proceso de contrataci&oacute;n abierta, este debe de ser el mismo que el campo de tender/title en el proceso relacionado."/>
                            <span class="form-error" data-form-error-for="txtRELATED_PROCESS_TITLE" id="spanRELATED_PROCESS_TITLE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Esquema
                            <select name="cmbRELATED_PROCESS_SCHEME" id="cmbRELATED_PROCESS_SCHEME" title="El esquema de identificaci&oacute;n usado por esta referencia cruzada.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${esquemasProcesosRelacionados}" var="esquema">
                                    <option value="${esquema.clave}">${esquema.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbRELATED_PROCESS_SCHEME" id="spanRELATED_PROCESS_SCHEME">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                        <label class="cell medium-4">Identificador
                            <input type="text" name="txtRELATED_PROCESS_IDENTIFIER" id="txtRELATED_PROCESS_IDENTIFIER" placeholder="p. ej. Identificador" value="" title="El identificador del proceso relacionado. Si el esquema es &#39;ocid&#39;, debe ser un ID de Open Contracting (odid)."/>
                            <span class="form-error" data-form-error-for="txtRELATED_PROCESS_IDENTIFIER" id="spanRELATED_PROCESS_IDENTIFIER">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">URI de proceso relacionado
                            <input type="text" name="txtRELATED_PROCESS_URI" id="txtRELATED_PROCESS_URI" placeholder="p. ej. www.url.com" pattern="url" value="" title="Una URI que apunta a un documento legible por computadora, entrega o paquete de registro que contenga el proceso relacionado identificado."/>
                            <span class="form-error" data-form-error-for="txtRELATED_PROCESS_URI" id="spanRELATED_PROCESS_URI">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <fieldset class="cell fieldset">
                            <legend title="El tipo de relaci&oacute;n.">Relaci&oacute;n</legend>
                            <div class="grid-x grid-margin-x">
                                <c:forEach items="${relacionesProcesosRelacionados}" var="relacion">
                                    <label class="cell medium-4 text-truncate">
                                        <input type="checkbox" name="chckRELATED_PROCESS_RELATION_SHIP" value="${relacion.clave}"/>
                                        ${relacion.valor}
                                    </label>
                                </c:forEach>
                                <span class="form-error" id="spanRELATED_PROCESS_RELATION_SHIP">
                                    Campo requerido, selecciona al menos una opci&oacute;n.
                                </span>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-relatedProcess"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-relatedProcess"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un proceso relacionado (BOTTOM) --%>
<%-- formulario de un documento (TOP) --%>
<div id="modalFrmDocument" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Documento</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmDocument" id="frmDocument" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-document" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-4">ID <span style="color: red">**</span>
                            <input type="text" id="txtDOCUMENT_ID" required placeholder="p. ej. 1" value="" title="Identificador local y &uacute;nico para este documento. Este campo se utiliza para darle seguimiento a las m&uacute;ltiples versiones de un documento en el proceso de creaci&oacute;n de un registro del proceso de contrataci&oacute;n (record) que se genera a partir de las entregas (release)."/>
                            <span class="form-error" data-form-error-for="txtDOCUMENT_ID" id="spanDOCUMENT_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Tipo de Documento
                            <select id="cmbDOCUMENT_TYPE" title="Una clasificaci&oacute;n del documento descrito.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${tiposDeDocumentoContrato}" var="tipoDocumento">
                                    <option value="${tipoDocumento.clave}">${tipoDocumento.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbDOCUMENT_TYPE" id="spanDOCUMENT_TYPE">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                        <label class="cell medium-4">T&iacute;tulo
                            <input type="text" id="txtDOCUMENT_TITLE" placeholder="p. ej. T&iacute;tulo del documento" value="" title="El t&iacute;tulo del documento."/>
                            <span class="form-error" data-form-error-for="txtDOCUMENT_TITLE" id="spanDOCUMENT_TITLE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Descripci&oacute;n
                            <input type="text" id="txtDOCUMENT_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n del documento" value="" title="Una descripci&oacute;n corta del documento. Se recomienda que las descripciones no excedan 250 palabras. En el caso en que el documento no est&eacute; accesible en l&iacute;nea, el campo de descripci&oacute;n puede utilizarse para describir los arreglos necesarios para obtener una copia del documento."/>
                            <span class="form-error" data-form-error-for="txtDOCUMENT_DESCRIPTION" id="spanDOCUMENT_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">URL
                            <input type="text" id="txtDOCUMENT_URL" placeholder="p. ej. www.url.com" value="" pattern="url" title="Un enlace directo al documento o archivo adjunto. El servidor que da acceso a este documento debe de estar configurado para reportar correctamente el tipo MIME de documento."/>
                            <span class="form-error" data-form-error-for="txtDOCUMENT_URL" id="spanDOCUMENT_URL">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de publicaci&oacute;n
                            <input type="date" id="txtDOCUMENT_DATE_PUBLISHED" value="" pattern="fecha" title="La fecha de publicaci&oacute;n del documento. Esto es particularmente importante para documentos relevantes desde el punto de vista legal, como los avisos de licitaci&oacute;n."/>
                            <span class="form-error" data-form-error-for="txtDOCUMENT_DATE_PUBLISHED" id="spanDOCUMENT_DATE_PUBLISHED">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de modificaci&oacute;n
                            <input type="date" id="txtDOCUMENT_DATE_MODIFIED" value="" pattern="fecha" title="Fecha en que se modific&oacute; por &uacute;ltima vez el documento."/>
                            <span class="form-error" data-form-error-for="txtDOCUMENT_DATE_MODIFIED" id="spanDOCUMENT_DATE_MODIFIED">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Formato
                            <input type="text" id="txtDOCUMENT_FORMAT" placeholder="p. ej. text/html" value="" title="El formato del documento, utilizando la lista de c&oacute;digos abierta [IANA Media Types] (vea los valores en la columna &#39;Template&#39;), o utilizando el c&oacute;digo &#39;offline/print&#39;  si el documento descrito se publica offline."/>
                            <span class="form-error" data-form-error-for="txtDOCUMENT_FORMAT" id="spanDOCUMENT_FORMAT">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Idioma
                            <select id="cmbDOCUMENT_LANGUAGE" title="El idioma predeterminado de los datos con dos letras [ISO639-1].">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${tiposDeIdioma}" var="idioma">
                                    <option value="${idioma.codigo}" >${idioma.nombre}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbDOCUMENT_LANGUAGE" id="spanDOCUMENT_LANGUAGE">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-document"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-document"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un documento (BOTTOM) --%>
<%-- formulario de un hito (TOP) --%>
<div id="modalFrmMilestone" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Hito</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmMilestone" id="frmMilestone" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-milestone" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-4">ID
                            <input type="text" id="txtMILESTONE_ID" required placeholder="p. ej. 1" value="" title="Un identificador local para este hito, &uacute;nico dentro de este bloque. Este campo se usa para llevar registro de m&uacute;ltiples revisiones de un hito a trav&eacute;s de la compilaci&oacute;n de entrega a mecanismo de registro." />
                            <span class="form-error" data-form-error-for="txtMILESTONE_ID" id="spanMILESTONE_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Tipo de hito
                            <select id="cmbMILESTONE_TYPE" title="El tipo del hito.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${tiposDeHito}" var="tipo">
                                    <option value="${tipo.clave}">${tipo.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbMILESTONE_TYPE" id="spanMILESTONE_TYPE">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                        <label class="cell medium-4">T&iacute;tulo
                            <input type="text" id="txtMILESTONE_TITLE" placeholder="p. ej. T&iacute;tulo del hito" value="" title="T&iacute;tulo del hito." />
                            <span class="form-error" data-form-error-for="txtMILESTONE_TITLE" id="spanMILESTONE_TITLE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Descripci&oacute;n
                            <input type="text" id="txtMILESTONE_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n del hito" value="" title="Una descripci&oacute;n del hito." />
                            <span class="form-error" data-form-error-for="txtMILESTONE_DESCRIPTION" id="spanMILESTONE_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">C&oacute;digo de hito
                            <input type="text" id="txtMILESTONE_CODE" placeholder="p. ej. C&oacute;digo de hito" value="" title="Los c&oacute;digos de hito pueden usarse para seguir eventos espec&iacute;ficos que toman lugar para un tipo particular de proceso de contrataciones. Por ejemplo, un c&oacute;digo de &#39;approvalLetter&#39; puede usarse para permitir a las aplicaciones entender que el hito representa una fecha cuando el approvalLetter debe entregarse o firmarse."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_CODE" id="spanMILESTONE_CODE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha l&iacute;mite
                            <input type="date" id="txtMILESTONE_DUE_DATE" value="" pattern="fecha" title="La fecha l&iacute;mite del hito."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_DUE_DATE" id="spanMILESTONE_DUE_DATE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de cumplimiento
                            <input type="date" id="txtMILESTONE_DATE_MET" value="" pattern="fecha" title="La fecha en que el hito se cumpli&oacute;."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_DATE_MET" id="spanMILESTONE_DATE_MET">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de modificaci&oacute;n
                            <input type="date" id="txtMILESTONE_DATE_MODIFIED" value="" pattern="fecha" title="La fecha en que el hito fue revisado o modificado y se cambi&oacute; el estado o se confirm&oacute; que continuaba siendo correcto."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_DATE_MODIFIED" id="spanMILESTONE_DATE_MODIFIED">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Estado
                            <select id="cmbMILESTONE_STATUS" title="El estatus que se realiz&oacute; en la fecha en &#39;dateModified&#39;.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${estadosHito}" var="estado">
                                    <option value="${estado.clave}">${estado.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbMILESTONE_STATUS" id="spanMILESTONE_STATUS">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-milestone"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-milestone"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un hito (BOTTOM) --%>
<%-- formulario de un documento - hito (TOP) --%>
<div id="modalFrmMilestoneDocument" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Documento - Hito</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmMilestoneDocument" id="frmMilestoneDocument" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-milestone-document" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-4">ID <span style="color: red">**</span>
                            <input type="text" id="txtMILESTONE_DOCUMENT_ID" required placeholder="p. ej. 1" value="" title="Identificador local y &uacute;nico para este documento. Este campo se utiliza para darle seguimiento a las m&uacute;ltiples versiones de un documento en el proceso de creaci&oacute;n de un registro del proceso de contrataci&oacute;n (record) que se genera a partir de las entregas (release)."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_DOCUMENT_ID" id="spanMILESTONE_DOCUMENT_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Tipo de Documento
                            <select id="cmbMILESTONE_DOCUMENT_TYPE" title="Una clasificaci&oacute;n del documento descrito.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${tiposDeDocumentoContrato}" var="tipoDocumento">
                                    <option value="${tipoDocumento.clave}">${tipoDocumento.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbMILESTONE_DOCUMENT_TYPE" id="spanMILESTONE_DOCUMENT_TYPE">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                        <label class="cell medium-4">T&iacute;tulo
                            <input type="text" id="txtMILESTONE_DOCUMENT_TITLE" placeholder="p. ej. T&iacute;tulo del documento" value="" title="El t&iacute;tulo del documento."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_DOCUMENT_TITLE" id="spanMILESTONE_DOCUMENT_TITLE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Descripci&oacute;n
                            <input type="text" id="txtMILESTONE_DOCUMENT_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n del documento" value="" title="Una descripci&oacute;n corta del documento. Se recomienda que las descripciones no excedan 250 palabras. En el caso en que el documento no est&eacute; accesible en l&iacute;nea, el campo de descripci&oacute;n puede utilizarse para describir los arreglos necesarios para obtener una copia del documento."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_DOCUMENT_DESCRIPTION" id="spanMILESTONE_DOCUMENT_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">URL
                            <input type="text" id="txtMILESTONE_DOCUMENT_URL" placeholder="p. ej. www.url.com" value="" pattern="url" title="Un enlace directo al documento o archivo adjunto. El servidor que da acceso a este documento debe de estar configurado para reportar correctamente el tipo MIME de documento."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_DOCUMENT_URL" id="spanMILESTONE_DOCUMENT_URL">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de publicaci&oacute;n
                            <input type="date" id="txtMILESTONE_DOCUMENT_DATE_PUBLISHED" value="" pattern="fecha" title="La fecha de publicaci&oacute;n del documento. Esto es particularmente importante para documentos relevantes desde el punto de vista legal, como los avisos de licitaci&oacute;n."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_DOCUMENT_DATE_PUBLISHED" id="spanMILESTONE_DOCUMENT_DATE_PUBLISHED">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de modificaci&oacute;n
                            <input type="date" id="txtMILESTONE_DOCUMENT_DATE_MODIFIED" value="" pattern="fecha" title="Fecha en que se modific&oacute; por &uacute;ltima vez el documento."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_DOCUMENT_DATE_MODIFIED" id="spanMILESTONE_DOCUMENT_DATE_MODIFIED">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Formato
                            <input type="text" id="txtMILESTONE_DOCUMENT_FORMAT" placeholder="p. ej. text/html" value="" title="El formato del documento, utilizando la lista de c&oacute;digos abierta [IANA Media Types] (vea los valores en la columna &#39;Template&#39;), o utilizando el c&oacute;digo &#39;offline/print&#39;  si el documento descrito se publica offline."/>
                            <span class="form-error" data-form-error-for="txtMILESTONE_DOCUMENT_FORMAT" id="spanMILESTONE_DOCUMENT_FORMAT">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Idioma
                            <select id="cmbMILESTONE_DOCUMENT_LANGUAGE" title="El idioma predeterminado de los datos con dos letras [ISO639-1].">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${tiposDeIdioma}" var="idioma">
                                    <option value="${idioma.codigo}" >${idioma.nombre}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbMILESTONE_DOCUMENT_LANGUAGE" id="spanMILESTONE_DOCUMENT_LANGUAGE">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-milestone-document"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-milestone-document"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un documento - hito (BOTTOM) --%>
<%-- formulario de una enmienda (TOP) --%>
<div id="modalFrmAmendment" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Enmienda</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmAmendment" id="frmAmendment" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-amendment" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-4">ID <span style="color: red">**</span>
                            <input type="text" required id="txtAMENDMENT_ID" placeholder="p. ej. 1" value="" title="Un identificador para esta enmienda: com&uacute;nmente el n&uacute;mero de enmienda."/>
                            <span class="form-error" data-form-error-for="txtAMENDMENT_ID" id="spanAMENDMENT_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de enmienda
                            <input type="date" id="txtAMENDMENT_DATE" value="" title="La fecha de esta enmienda." />
                            <span class="form-error" data-form-error-for="txtAMENDMENT_DATE" id="spanAMENDMENT_DATE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Justificaci&oacute;n
                            <input type="text" id="txtAMENDMENT_RATIONALE" placeholder="p. ej. Explicaci&oacute;n de la enmienda" value="" title="Una explicaci&oacute;n de la enmienda."/>
                            <span class="form-error" data-form-error-for="txtAMENDMENT_RATIONALE" id="spanAMENDMENT_RATIONALE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Descripci&oacute;n
                            <input type="text" id="txtAMENDMENT_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n de la enmienda" value="" title="Un texto libre o semi estructurado, describiendo los cambios hechos en esta enmienda."/>
                            <span class="form-error" data-form-error-for="txtAMENDMENT_DESCRIPTION" id="spanAMENDMENT_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Entrega enmendada (identificador)
                            <input type="text" id="txtAMENDMENT_AMENDS_RELEASE_ID" placeholder="p. ej. 1" value="" title="Provee el identificador (release.id) de la entrega OCDS (de este proceso de contrataci&oacute;n) que provee los valores para este proceso de contrataci&oacute;n &#42;&#42;antes&#42;&#42; de realizada la enmienda."/>
                            <span class="form-error" data-form-error-for="txtAMENDMENT_AMENDS_RELEASE_ID" id="spanAMENDMENT_AMENDS_RELEASE_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Entrega de enmienda (identificador)
                            <input type="text" id="txtAMENDMENT_RELEASE_ID" placeholder="p. ej. 1" value="" title="Provee el identificador (release.id) de la entrega OCDS (de este proceso de contrataci&oacute;n) que provee los valores para este proceso de contrataci&oacute;n &#42;&#42;despu&eacute;s&#42;&#42; de realizada la enmienda." />
                            <span class="form-error" data-form-error-for="txtAMENDMENT_RELEASE_ID" id="spanAMENDMENT_RELEASE_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-amendment"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-amendment"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de una enmienda (BOTTOM) --%>
<%-- formulario de una transaccion - implementacion (TOP) --%>
<div id="modalFrmImplementationTransaction" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Transacci&oacute;n - Implementaci&oacute;n</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmImplementationTransaction" id="frmImplementationTransaction" method="post" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-implementation-transaction" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-6">ID
                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_ID" required placeholder="p. ej. 1" value="" title="Un identificador &uacute;nico para esta transacci&oacute;n de pago. Este identificador debe de permitir referenciar contra la fuente de datos provista. Para IATI esta es la referencia de transacci&oacute;n."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_ID" id="spanIMPLEMENTATION_TRANSACTION_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Fuente de los Datos
                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_SOURCE" placeholder="p. ej. www.url.com" value="" pattern="url" title="Usado para apuntar a un Fiscal Data Package, un archivo IATI o una fuente legible por computadora o por un humano donde los usuarios puedan obtener m&aacute;s informaci&oacute;n sobre los identificadores de partida presupuestaria o identificadores de proyectos, provistos aqu&iacute;."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_SOURCE" id="spanIMPLEMENTATION_TRANSACTION_SOURCE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Fecha
                            <input type="date" id="txtIMPLEMENTATION_TRANSACTION_DATE_PUBLISHED" value="" pattern="fecha" title="La fecha de la transacci&oacute;n."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_DATE_PUBLISHED" id="spanIMPLEMENTATION_TRANSACTION_DATE_PUBLISHED">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Informaci&oacute;n de gasto vinculado
                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_URI" placeholder="p. ej. www.url.com" value="" pattern="url" title="Una URI apuntando directamente al registro legible por computadora sobre esta transacci&oacute;n."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_URI" id="spanIMPLEMENTATION_TRANSACTION_URI">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <fieldset class="fieldset cell">
                            <legend title="El monto o valor de la transacci&oacute;n.">Valor</legend>
                            <div class="grid-margin-x grid-x">
                                <label class="cell medium-6">Monto
                                    <input type="number" id="txtIMPLEMENTATION_TRANSACTION_VALUE_AMOUNT" placeholder="p. ej. 0" min="0" value="" title="Monto como una cifra."/>
                                    <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_VALUE_AMOUNT" id="spanIMPLEMENTATION_TRANSACTION_VALUE_AMOUNT">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Moneda
                                    <select id="cmbIMPLEMENTATION_TRANSACTION_VALUE_CURRENCY" title="La moneda del monto.">
                                        <option value="">--Elige una opci&oacute;n--</option>
                                        <c:forEach items="${tipoMoneda}" var="moneda">
                                            <option value="${moneda.clave}">${moneda.valor}</option>
                                        </c:forEach>
                                    </select>
                                    <span class="form-error" data-form-error-for="cmbIMPLEMENTATION_TRANSACTION_VALUE_CURRENCY" id="spanIMPLEMENTATION_TRANSACTION_VALUE_CURRENCY">
                                        Elige una opci&oacute;n.
                                    </span>
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="fieldset cell">
                            <legend title="El valor de la transacci&oacute;n. Un valor negativo indica un reembolso o una correcci&oacute;n.">Monto</legend>
                            <div class="grid-margin-x grid-x">
                                <label class="cell medium-6">Monto
                                    <input type="number" id="txtIMPLEMENTATION_TRANSACTION_AMOUNT_AMOUNT" placeholder="p. ej. 0" min="0" value="" title="Monto como una cifra."/>
                                    <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_AMOUNT_AMOUNT" id="spanIMPLEMENTATION_TRANSACTION_AMOUNT_AMOUNT">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Moneda
                                    <select id="cmbIMPLEMENTATION_TRANSACTION_AMOUNT_CURRENCY" title="La moneda del monto.">
                                        <option value="">--Elige una opci&oacute;n--</option>
                                        <c:forEach items="${tipoMoneda}" var="moneda">
                                            <option value="${moneda.clave}">${moneda.valor}</option>
                                        </c:forEach>
                                    </select>
                                    <span class="form-error" data-form-error-for="cmbIMPLEMENTATION_TRANSACTION_AMOUNT_CURRENCY" id="spanIMPLEMENTATION_TRANSACTION_AMOUNT_CURRENCY">
                                        Elige una opci&oacute;n.
                                    </span>
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="fieldset cell">
                            <legend title="Una referencia a la organizaci&oacute;n que provee los fondos para esta transacci&oacute;n de pago.">Pagador</legend>
                            <div class="grid-margin-x grid-x">
                                <label class="cell medium-6">ID de Entidad <span style="color: red">**</span>
                                    <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYER_ID" placeholder="p. ej. 1" value="" title="El ID utilizado para hacer referencia a esta parte involucrada desde otras secciones de la entrega. Este campo puede construirse con la siguiente estructura {identifier.scheme}-{identifier.id}(-{department-identifier})."/>
                                    <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYER_ID" id="spanIMPLEMENTATION_TRANSACTION_PAYER_ID">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Nombre
                                    <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYER_NAME" placeholder="p. ej. Nombre com&uacute;n" value="" title="Un nombre com&uacute;n para esta organizaci&oacute;n u otro participante en el proceso de contrataciones. El objeto identificador da un espacio para un nombre legal formal, y esto podr&iacute;a repetir el valor o dar un nombre com&uacute;n por el cual se conoce a la organizaci&oacute;n o entidad. Este campo tambi&eacute;n pude incluir detalles del departamento o sub-unidad involucrada en este proceso de contrataciones."/>
                                    <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYER_NAME" id="spanNAME">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <fieldset class="cell fieldset">
                                    <legend title="El identificador primario para esta organizaci&oacute;n o participante. Son preferibles los identificadores que denotan de forma &uacute;nica a una entidad legal. Consulta la [gu&iacute;a de identificadores de organizaci&oacute;n] para el esquema e identificador preferido.">Identificador principal</legend>
                                    <div class="grid-margin-x grid-x">
                                        <label class="cell medium-6">ID
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_ID" placeholder="p. ej. 1" value="" title="El identificador de la organizaci&oacute;n en el esquema seleccionado."/>
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_ID" id="spanIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_ID">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-6">Esquema
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_SCHEME" placeholder="p. ej. Esquema" value="" title="Los identificadores de organizaci&oacute;n deben de obtenerse de una lista de identificadores de organizaciones existentes. El campo scheme se usa para indicar o registrar de d&oacute;nde se toma el identificador. Este valor deber&iacute;a tomarse del [Esquema de Identificadores de Organizaciones]."/>
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_SCHEME" id="spanIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_SCHEME">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-6">Nombre Legal
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_LEGAL_NAME" placeholder="p. ej. Nombre Legal" value="" title="El nombre legalmente registrado de la organizaci&oacute;n." />
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_LEGAL_NAME" id="spanIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_LEGAL_NAME">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-6">URI
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_URI" placeholder="p. ej. www.url.com" value="" pattern="url" title="Una URI para identificar a la organizaci&oacute;n, como los prove&iacute;dos por [Open Corporates] o alg&uacute;n otro proveedor relevante de URIs. Este campo no debe ser utilizado para especificar el sitio web de la organizaci&oacute;n, el cual puede ser especificado en el campo URL del punto de contacto de la Organizaci&oacute;n."/>
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_URI" id="spanIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_URI">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                    </div>
                                </fieldset>
                                <fieldset class="cell fieldset">
                                    <legend title="Una direcci&oacute;n. Esta puede ser la direcci&oacute;n legalmente registrada de la organizaci&oacute;n o puede ser una direcci&oacute;n donde se reciba correspondencia para este proceso de contrataci&oacute;n particular.">Direcci&oacute;n</legend>
                                    <div class="grid-margin-x grid-x">
                                        <label class="cell medium-4">Direcci&oacute;n
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_STREET_ADDRESS" placeholder="p. ej. 1600 Amphitheatre Pkwy." value="" title="La direcci&oacute;n de la calle."/>
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_STREET_ADDRESS" id="spanIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_STREET_ADDRESS">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-4">Localidad
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_LOCALITY" placeholder="p. ej. Mountain View." value="" title="La localidad." />
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_LOCALITY" id="spanIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_LOCALITY">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-4">Regi&oacute;n
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_REGION" placeholder="p. ej. CA." value="" title="La regi&oacute;n."/>
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_REGION" id="spanIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_REGION">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-4">C&oacute;digo postal
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_POSTAL_CODE" placeholder="p. ej. 94043" value="" pattern="codigo_postal" title="El c&oacute;digo postal." />
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_POSTAL_CODE" id="spanIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_POSTAL_CODE">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-4">Pa&iacute;s
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_COUNTRY_NAME" placeholder="p. ej. M&eacute;xico" value="" title="El nombre del pa&iacute;s." />
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_COUNTRY_NAME" id="spanIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_COUNTRY_NAME">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                    </div>
                                </fieldset>
                                <fieldset class="cell fieldset">
                                    <legend title="Detalles de contacto que pueden usarse para esta parte involucrada.">Punto de contacto</legend>
                                    <div class="grid-margin-x grid-x">
                                        <label class="cell medium-4">Nombre
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_NAME" placeholder="p. ej. Carlos" value="" title="El nombre de la persona de contacto, departamento o punto de contacto en relaci&oacute;n a este proceso de contrataci&oacute;n."/>
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_NAME" id="spanIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_NAME">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-4">Correo electr&oacute;nico
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_EMAIL" placeholder="p. ej. email@gmail.com" value="" title="La direcci&oacute;n de correo del punto o persona de contacto."/>
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_EMAIL" id="spanIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_EMAIL">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-4">Tel&eacute;fono
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_TELEPHONE" placeholder="p. ej. 5555555555 o 555-555-5555" value="" title="El n&uacute;mero de tel&eacute;fono del punto o persona de contacto. Este debe de incluir el c&oacute;digo de marcaci&oacute;n internacional." />
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_TELEPHONE" id="spanIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_TELEPHONE">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-4">N&uacute;mero de fax
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_FAXNUMBER" placeholder="p. ej. 5555555555 o 555-555-5555" value="" title="El n&uacute;mero de fax del punto o persona de contacto. Este debe de incluir el c&oacute;digo de marcaci&oacute;n internacional."/>
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_FAXNUMBER" id="spanIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_FAXNUMBER">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-4">URL
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_URL" placeholder="p. ej. www.url.com" value="" pattern="url" title="Una direcci&oacute;n web para el punto o persona de contacto."/>
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_URL" id="spanIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_URL">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                    </div>
                                </fieldset>
                            </div>
                        </fieldset>
                        <fieldset class="fieldset cell">
                            <legend title="Una referencia a la organizaci&oacute;n que recibe los fondos en esta transacci&oacute;n.">Beneficiario</legend>
                            <div class="grid-margin-x grid-x">
                                <label class="cell medium-6">ID de Entidad <span style="color: red">**</span>
                                    <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_ID" placeholder="p. ej. 1" value="" title="El ID utilizado para hacer referencia a esta parte involucrada desde otras secciones de la entrega. Este campo puede construirse con la siguiente estructura {identifier.scheme}-{identifier.id}(-{department-identifier})."/>
                                    <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYEE_ID" id="spanIMPLEMENTATION_TRANSACTION_PAYEE_ID">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Nombre
                                    <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_NAME" placeholder="p. ej. Nombre com&uacute;n" value="" title="Un nombre com&uacute;n para esta organizaci&oacute;n u otro participante en el proceso de contrataciones. El objeto identificador da un espacio para un nombre legal formal, y esto podr&iacute;a repetir el valor o dar un nombre com&uacute;n por el cual se conoce a la organizaci&oacute;n o entidad. Este campo tambi&eacute;n pude incluir detalles del departamento o sub-unidad involucrada en este proceso de contrataciones."/>
                                    <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYEE_NAME" id="spanNAME">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <fieldset class="cell fieldset">
                                    <legend title="El identificador primario para esta organizaci&oacute;n o participante. Son preferibles los identificadores que denotan de forma &uacute;nica a una entidad legal. Consulta la [gu&iacute;a de identificadores de organizaci&oacute;n] para el esquema e identificador preferido.">Identificador principal</legend>
                                    <div class="grid-margin-x grid-x">
                                        <label class="cell medium-6">ID
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_ID" placeholder="p. ej. 1" value="" title="El identificador de la organizaci&oacute;n en el esquema seleccionado."/>
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_ID" id="spanIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_ID">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-6">Esquema
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_SCHEME" placeholder="p. ej. Esquema" value="" title="Los identificadores de organizaci&oacute;n deben de obtenerse de una lista de identificadores de organizaciones existentes. El campo scheme se usa para indicar o registrar de d&oacute;nde se toma el identificador. Este valor deber&iacute;a tomarse del [Esquema de Identificadores de Organizaciones]."/>
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_SCHEME" id="spanIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_SCHEME">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-6">Nombre Legal
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_LEGAL_NAME" placeholder="p. ej. Nombre Legal" value="" title="El nombre legalmente registrado de la organizaci&oacute;n." />
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_LEGAL_NAME" id="spanIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_LEGAL_NAME">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-6">URI
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_URI" placeholder="p. ej. www.url.com" value="" pattern="url" title="Una URI para identificar a la organizaci&oacute;n, como los prove&iacute;dos por [Open Corporates] o alg&uacute;n otro proveedor relevante de URIs. Este campo no debe ser utilizado para especificar el sitio web de la organizaci&oacute;n, el cual puede ser especificado en el campo URL del punto de contacto de la Organizaci&oacute;n."/>
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_URI" id="spanIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_URI">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                    </div>
                                </fieldset>
                                <fieldset class="cell fieldset">
                                    <legend title="Una direcci&oacute;n. Esta puede ser la direcci&oacute;n legalmente registrada de la organizaci&oacute;n o puede ser una direcci&oacute;n donde se reciba correspondencia para este proceso de contrataci&oacute;n particular.">Direcci&oacute;n</legend>
                                    <div class="grid-margin-x grid-x">
                                        <label class="cell medium-4">Direcci&oacute;n
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_STREET_ADDRESS" placeholder="p. ej. 1600 Amphitheatre Pkwy." value="" title="La direcci&oacute;n de la calle."/>
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_STREET_ADDRESS" id="spanIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_STREET_ADDRESS">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-4">Localidad
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_LOCALITY" placeholder="p. ej. Mountain View." value="" title="La localidad." />
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_LOCALITY" id="spanIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_LOCALITY">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-4">Regi&oacute;n
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_REGION" placeholder="p. ej. CA." value="" title="La regi&oacute;n."/>
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_REGION" id="spanIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_REGION">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-4">C&oacute;digo postal
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_POSTAL_CODE" placeholder="p. ej. 94043" value="" pattern="codigo_postal" title="El c&oacute;digo postal." />
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_POSTAL_CODE" id="spanIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_POSTAL_CODE">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-4">Pa&iacute;s
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_COUNTRY_NAME" placeholder="p. ej. M&eacute;xico" value="" title="El nombre del pa&iacute;s." />
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_COUNTRY_NAME" id="spanIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_COUNTRY_NAME">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                    </div>
                                </fieldset>
                                <fieldset class="cell fieldset">
                                    <legend title="Detalles de contacto que pueden usarse para esta parte involucrada.">Punto de contacto</legend>
                                    <div class="grid-margin-x grid-x">
                                        <label class="cell medium-4">Nombre
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_NAME" placeholder="p. ej. Carlos" value="" title="El nombre de la persona de contacto, departamento o punto de contacto en relaci&oacute;n a este proceso de contrataci&oacute;n."/>
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_NAME" id="spanIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_NAME">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-4">Correo electr&oacute;nico
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_EMAIL" placeholder="p. ej. email@gmail.com" value="" title="La direcci&oacute;n de correo del punto o persona de contacto."/>
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_EMAIL" id="spanIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_EMAIL">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-4">Tel&eacute;fono
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_TELEPHONE" placeholder="p. ej. 5555555555 o 555-555-5555" value="" title="El n&uacute;mero de tel&eacute;fono del punto o persona de contacto. Este debe de incluir el c&oacute;digo de marcaci&oacute;n internacional." />
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_TELEPHONE" id="spanIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_TELEPHONE">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-4">N&uacute;mero de fax
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_FAXNUMBER" placeholder="p. ej. 5555555555 o 555-555-5555" value="" title="El n&uacute;mero de fax del punto o persona de contacto. Este debe de incluir el c&oacute;digo de marcaci&oacute;n internacional."/>
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_FAXNUMBER" id="spanIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_FAXNUMBER">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-4">URL
                                            <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_URL" placeholder="p. ej. www.url.com" value="" pattern="url" title="Una direcci&oacute;n web para el punto o persona de contacto."/>
                                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_URL" id="spanIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_URL">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                    </div>
                                </fieldset>
                            </div>
                        </fieldset>
                        <fieldset class="fieldset cell">
                            <legend title="El Identificador de Organizaci&oacute;n para la organizaci&oacute;n de la cu&aacute;l se originan los fondos en esta transacci&oacute;n. Expresados siguiendo el est&aacute;ndar de Identificador Organizacional - consulta la documentaci&oacute;n y la lista de c&oacute;digos.">Organizaci&oacute;n proveedora</legend>
                            <div class="grid-margin-x grid-x">
                                <label class="cell medium-6">ID
                                    <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_ID" placeholder="p. ej. 1" value="" title="El identificador de la organizaci&oacute;n en el esquema seleccionado."/>
                                    <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_ID" id="spanIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_ID">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Esquema
                                    <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_SCHEME" placeholder="p. ej. Esquema" value="" title="Los identificadores de organizaci&oacute;n deben de obtenerse de una lista de identificadores de organizaciones existentes. El campo scheme se usa para indicar o registrar de d&oacute;nde se toma el identificador. Este valor deber&iacute;a tomarse del [Esquema de Identificadores de Organizaciones]."/>
                                    <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_SCHEME" id="spanIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_SCHEME">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Nombre Legal
                                    <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_LEGAL_NAME" placeholder="p. ej. Nombre Legal" value="" title="El nombre legalmente registrado de la organizaci&oacute;n." />
                                    <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_LEGAL_NAME" id="spanIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_LEGAL_NAME">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">URI
                                    <input type="text" id="txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_URI" placeholder="p. ej. www.url.com" value="" pattern="url" title="Una URI para identificar a la organizaci&oacute;n, como los prove&iacute;dos por [Open Corporates] o alg&uacute;n otro proveedor relevante de URIs. Este campo no debe ser utilizado para especificar el sitio web de la organizaci&oacute;n, el cual puede ser especificado en el campo URL del punto de contacto de la Organizaci&oacute;n."/>
                                    <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_URI" id="spanIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_URI">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="fieldset cell">
                            <legend title="El Identificador de Organizaci&oacute;n para la organizaci&oacute;n que recibe los fondos en esta transacci&oacute;n. Expresados siguiendo el est&aacute;ndar de Identificador Organizacional - consulta la documentaci&oacute;n y la lista de c&oacute;digos.">Organizaci&oacute;n receptora</legend>
                            <div class="grid-margin-x grid-x">
                                <label class="cell medium-6">ID
                                    <input type="text" id="txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_ID" placeholder="p. ej. 1" value="" title="El identificador de la organizaci&oacute;n en el esquema seleccionado."/>
                                    <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_ID" id="spanIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_ID">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Esquema
                                    <input type="text" id="txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_SCHEME" placeholder="p. ej. Esquema" value="" title="Los identificadores de organizaci&oacute;n deben de obtenerse de una lista de identificadores de organizaciones existentes. El campo scheme se usa para indicar o registrar de d&oacute;nde se toma el identificador. Este valor deber&iacute;a tomarse del [Esquema de Identificadores de Organizaciones]."/>
                                    <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_SCHEME" id="spanIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_SCHEME">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Nombre Legal
                                    <input type="text" id="txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_LEGAL_NAME" placeholder="p. ej. Nombre Legal" value="" title="El nombre legalmente registrado de la organizaci&oacute;n." />
                                    <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_LEGAL_NAME" id="spanIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_LEGAL_NAME">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">URI
                                    <input type="text" id="txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_URI" placeholder="p. ej. www.url.com" value="" pattern="url" title="Una URI para identificar a la organizaci&oacute;n, como los prove&iacute;dos por [Open Corporates] o alg&uacute;n otro proveedor relevante de URIs. Este campo no debe ser utilizado para especificar el sitio web de la organizaci&oacute;n, el cual puede ser especificado en el campo URL del punto de contacto de la Organizaci&oacute;n."/>
                                    <span class="form-error" data-form-error-for="txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_URI" id="spanIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_URI">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-implementation-transaction"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-implementation-transaction"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de una transaccion - implementacion (BOTTOM) --%>
<%-- formulario de un documento - implementacion (TOP) --%>
<div id="modalFrmImplementationDocument" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Documento - Implementaci&oacute;n</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmImplementationDocument" id="frmImplementationDocument" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-implementation-document" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-4">ID <span style="color: red">**</span>
                            <input type="text" id="txtIMPLEMENTATION_DOCUMENT_ID" required placeholder="p. ej. 1" value="" title="Identificador local y &uacute;nico para este documento. Este campo se utiliza para darle seguimiento a las m&uacute;ltiples versiones de un documento en el proceso de creaci&oacute;n de un registro del proceso de contrataci&oacute;n (record) que se genera a partir de las entregas (release)."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_DOCUMENT_ID" id="spanIMPLEMENTATION_DOCUMENT_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Tipo de Documento
                            <select id="cmbIMPLEMENTATION_DOCUMENT_TYPE" title="Una clasificaci&oacute;n del documento descrito.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${tiposDeDocumentoImplementacion}" var="tipoDocumento">
                                    <option value="${tipoDocumento.clave}">${tipoDocumento.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbIMPLEMENTATION_DOCUMENT_TYPE" id="spanIMPLEMENTATION_DOCUMENT_TYPE">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                        <label class="cell medium-4">T&iacute;tulo
                            <input type="text" id="txtIMPLEMENTATION_DOCUMENT_TITLE" placeholder="p. ej. T&iacute;tulo del documento" value="" title="El t&iacute;tulo del documento."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_DOCUMENT_TITLE" id="spanIMPLEMENTATION_DOCUMENT_TITLE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Descripci&oacute;n
                            <input type="text" id="txtIMPLEMENTATION_DOCUMENT_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n del documento" value="" title="Una descripci&oacute;n corta del documento. Se recomienda que las descripciones no excedan 250 palabras. En el caso en que el documento no est&eacute; accesible en l&iacute;nea, el campo de descripci&oacute;n puede utilizarse para describir los arreglos necesarios para obtener una copia del documento."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_DOCUMENT_DESCRIPTION" id="spanIMPLEMENTATION_DOCUMENT_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">URL
                            <input type="text" id="txtIMPLEMENTATION_DOCUMENT_URL" placeholder="p. ej. www.url.com" value="" pattern="url" title="Un enlace directo al documento o archivo adjunto. El servidor que da acceso a este documento debe de estar configurado para reportar correctamente el tipo MIME de documento."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_DOCUMENT_URL" id="spanIMPLEMENTATION_DOCUMENT_URL">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de publicaci&oacute;n
                            <input type="date" id="txtIMPLEMENTATION_DOCUMENT_DATE_PUBLISHED" value="" pattern="fecha" title="La fecha de publicaci&oacute;n del documento. Esto es particularmente importante para documentos relevantes desde el punto de vista legal, como los avisos de licitaci&oacute;n."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_DOCUMENT_DATE_PUBLISHED" id="spanIMPLEMENTATION_DOCUMENT_DATE_PUBLISHED">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de modificaci&oacute;n
                            <input type="date" id="txtIMPLEMENTATION_DOCUMENT_DATE_MODIFIED" value="" pattern="fecha" title="Fecha en que se modific&oacute; por &uacute;ltima vez el documento."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_DOCUMENT_DATE_MODIFIED" id="spanIMPLEMENTATION_DOCUMENT_DATE_MODIFIED">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Formato
                            <input type="text" id="txtIMPLEMENTATION_DOCUMENT_FORMAT" placeholder="p. ej. text/html" value="" title="El formato del documento, utilizando la lista de c&oacute;digos abierta [IANA Media Types] (vea los valores en la columna &#39;Template&#39;), o utilizando el c&oacute;digo &#39;offline/print&#39;  si el documento descrito se publica offline."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_DOCUMENT_FORMAT" id="spanIMPLEMENTATION_DOCUMENT_FORMAT">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Idioma
                            <select id="cmbIMPLEMENTATION_DOCUMENT_LANGUAGE" title="El idioma predeterminado de los datos con dos letras [ISO639-1].">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${tiposDeIdioma}" var="idioma">
                                    <option value="${idioma.codigo}">${idioma.nombre}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbIMPLEMENTATION_DOCUMENT_LANGUAGE" id="spanIMPLEMENTATION_DOCUMENT_LANGUAGE">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-implementation-document"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-implementation-document"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un documento - implementacion (BOTTOM) --%>
<%-- formulario de un hito - implementacion (TOP) --%>
<div id="modalFrmImplementationMilestone" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Hito - Implementaci&oacute;n</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmImplementationMilestone" id="frmImplementationMilestone" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-implementation-milestone" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-4">ID
                            <input type="text" id="txtIMPLEMENTATION_MILESTONE_ID" required placeholder="p. ej. 1" value="" title="Un identificador local para este hito, &uacute;nico dentro de este bloque. Este campo se usa para llevar registro de m&uacute;ltiples revisiones de un hito a trav&eacute;s de la compilaci&oacute;n de entrega a mecanismo de registro." />
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_MILESTONE_ID" id="spanIMPLEMENTATION_MILESTONE_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Tipo de hito
                            <select id="cmbIMPLEMENTATION_MILESTONE_TYPE" title="El tipo del hito.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${tiposDeHito}" var="tipo">
                                    <option value="${tipo.clave}">${tipo.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbIMPLEMENTATION_MILESTONE_TYPE" id="spanIMPLEMENTATION_MILESTONE_TYPE">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                        <label class="cell medium-4">T&iacute;tulo
                            <input type="text" id="txtIMPLEMENTATION_MILESTONE_TITLE" placeholder="p. ej. T&iacute;tulo del hito" value="" title="T&iacute;tulo del hito." />
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_MILESTONE_TITLE" id="spanIMPLEMENTATION_MILESTONE_TITLE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Descripci&oacute;n
                            <input type="text" id="txtIMPLEMENTATION_MILESTONE_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n del hito" value="" title="Una descripci&oacute;n del hito." />
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_MILESTONE_DESCRIPTION" id="spanIMPLEMENTATION_MILESTONE_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">C&oacute;digo de hito
                            <input type="text" id="txtIMPLEMENTATION_MILESTONE_CODE" placeholder="p. ej. C&oacute;digo de hito" value="" title="Los c&oacute;digos de hito pueden usarse para seguir eventos espec&iacute;ficos que toman lugar para un tipo particular de proceso de contrataciones. Por ejemplo, un c&oacute;digo de &#39;approvalLetter&#39; puede usarse para permitir a las aplicaciones entender que el hito representa una fecha cuando el approvalLetter debe entregarse o firmarse."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_MILESTONE_CODE" id="spanIMPLEMENTATION_MILESTONE_CODE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha l&iacute;mite
                            <input type="date" id="txtIMPLEMENTATION_MILESTONE_DUE_DATE" value="" pattern="fecha" title="La fecha l&iacute;mite del hito."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_MILESTONE_DUE_DATE" id="spanIMPLEMENTATION_MILESTONE_DUE_DATE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de cumplimiento
                            <input type="date" id="txtIMPLEMENTATION_MILESTONE_DATE_MET" value="" pattern="fecha" title="La fecha en que el hito se cumpli&oacute;."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_MILESTONE_DATE_MET" id="spanIMPLEMENTATION_MILESTONE_DATE_MET">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de modificaci&oacute;n
                            <input type="date" id="txtIMPLEMENTATION_MILESTONE_DATE_MODIFIED" value="" pattern="fecha" title="La fecha en que el hito fue revisado o modificado y se cambi&oacute; el estado o se confirm&oacute; que continuaba siendo correcto."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_MILESTONE_DATE_MODIFIED" id="spanIMPLEMENTATION_MILESTONE_DATE_MODIFIED">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Estado
                            <select id="cmbIMPLEMENTATION_MILESTONE_STATUS" title="El estatus que se realiz&oacute; en la fecha en &#39;dateModified&#39;.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${estadosHito}" var="estado">
                                    <option value="${estado.clave}">${estado.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbIMPLEMENTATION_MILESTONE_STATUS" id="spanIMPLEMENTATION_MILESTONE_STATUS">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-implementation-milestone"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-implementation-milestone"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un hito - implementacion (BOTTOM) --%>
<%-- formulario de un documento - implementacion.hito (TOP) --%>
<div id="modalFrmImplementationMilestoneDocument" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Documento - Implementaci&oacute;n de hito</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmImplementationMilestoneDocument" id="frmImplementationMilestoneDocument" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-implementation-milestone-document" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-4">ID <span style="color: red">**</span>
                            <input type="text" id="txtIMPLEMENTATION_MILESTONE_DOCUMENT_ID" required placeholder="p. ej. 1" value="" title="Identificador local y &uacute;nico para este documento. Este campo se utiliza para darle seguimiento a las m&uacute;ltiples versiones de un documento en el proceso de creaci&oacute;n de un registro del proceso de contrataci&oacute;n (record) que se genera a partir de las entregas (release)."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_MILESTONE_DOCUMENT_ID" id="spanIMPLEMENTATION_MILESTONE_DOCUMENT_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Tipo de Documento
                            <select id="cmbIMPLEMENTATION_MILESTONE_DOCUMENT_TYPE" title="Una clasificaci&oacute;n del documento descrito.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${tiposDeDocumentoImplementacion}" var="tipoDocumento">
                                    <option value="${tipoDocumento.clave}" >${tipoDocumento.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbIMPLEMENTATION_MILESTONE_DOCUMENT_TYPE" id="spanIMPLEMENTATION_MILESTONE_DOCUMENT_TYPE">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                        <label class="cell medium-4">T&iacute;tulo
                            <input type="text" id="txtIMPLEMENTATION_MILESTONE_DOCUMENT_TITLE" placeholder="p. ej. T&iacute;tulo del documento" value="" title="El t&iacute;tulo del documento."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_MILESTONE_DOCUMENT_TITLE" id="spanIMPLEMENTATION_MILESTONE_DOCUMENT_TITLE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Descripci&oacute;n
                            <input type="text" id="txtIMPLEMENTATION_MILESTONE_DOCUMENT_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n del documento" value="" title="Una descripci&oacute;n corta del documento. Se recomienda que las descripciones no excedan 250 palabras. En el caso en que el documento no est&eacute; accesible en l&iacute;nea, el campo de descripci&oacute;n puede utilizarse para describir los arreglos necesarios para obtener una copia del documento."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_MILESTONE_DOCUMENT_DESCRIPTION" id="spanIMPLEMENTATION_MILESTONE_DOCUMENT_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">URL
                            <input type="text" id="txtIMPLEMENTATION_MILESTONE_DOCUMENT_URL" placeholder="p. ej. www.url.com" value="" pattern="url" title="Un enlace directo al documento o archivo adjunto. El servidor que da acceso a este documento debe de estar configurado para reportar correctamente el tipo MIME de documento."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_MILESTONE_DOCUMENT_URL" id="spanIMPLEMENTATION_MILESTONE_DOCUMENT_URL">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de publicaci&oacute;n
                            <input type="date" id="txtIMPLEMENTATION_MILESTONE_DOCUMENT_DATE_PUBLISHED" value="" pattern="fecha" title="La fecha de publicaci&oacute;n del documento. Esto es particularmente importante para documentos relevantes desde el punto de vista legal, como los avisos de licitaci&oacute;n."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_MILESTONE_DOCUMENT_DATE_PUBLISHED" id="spanIMPLEMENTATION_MILESTONE_DOCUMENT_DATE_PUBLISHED">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de modificaci&oacute;n
                            <input type="date" id="txtIMPLEMENTATION_MILESTONE_DOCUMENT_DATE_MODIFIED" value="" pattern="fecha" title="Fecha en que se modific&oacute; por &uacute;ltima vez el documento."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_MILESTONE_DOCUMENT_DATE_MODIFIED" id="spanIMPLEMENTATION_MILESTONE_DOCUMENT_DATE_MODIFIED">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Formato
                            <input type="text" id="txtIMPLEMENTATION_MILESTONE_DOCUMENT_FORMAT" placeholder="p. ej. text/html" value="" title="El formato del documento, utilizando la lista de c&oacute;digos abierta [IANA Media Types] (vea los valores en la columna &#39;Template&#39;), o utilizando el c&oacute;digo &#39;offline/print&#39;  si el documento descrito se publica offline."/>
                            <span class="form-error" data-form-error-for="txtIMPLEMENTATION_MILESTONE_DOCUMENT_FORMAT" id="spanIMPLEMENTATION_MILESTONE_DOCUMENT_FORMAT">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Idioma
                            <select id="cmbIMPLEMENTATION_MILESTONE_DOCUMENT_LANGUAGE" title="El idioma predeterminado de los datos con dos letras [ISO639-1].">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${tiposDeIdioma}" var="idioma">
                                    <option value="${idioma.codigo}" >${idioma.nombre}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbIMPLEMENTATION_MILESTONE_DOCUMENT_LANGUAGE" id="spanIMPLEMENTATION_MILESTONE_DOCUMENT_LANGUAGE">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-implementation-milestone-document"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-implementation-milestone-document"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un documento - implementacion.hito (BOTTOM) --%>
<%-- formulario de un cambio en campos enmendados (TOP) --%>
<div id="modalFrmChange" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Campos enmendados</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmChange" id="frmChange" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-change" value="">
                    <div class="grid-margin-x grid-x">
                        <label class='cell medium-6'>Propiedad <span style='color: red'>**</span>
                            <input type='text' id='txtCHANGE_PROPERTY' required placeholder='p. ej. Propiedad' title='El nombre de propiedad que ha sido cambiado con relaci&oacute;n al lugar donde se encuentra la enmienda. Por ejemplo si el valor del contrato ha cambiado.'/>
                            <span class='form-error' data-form-error-for='txtCHANGE_PROPERTY' id='spanCHANGE_PROPERTY'>
                                 Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class='cell medium-6'>Valor anterior
                            <input type='text' id='txtCHANGE_FORMATER_VALUE' placeholder='p. ej. Valor previo' title='El valor previo de la propiedad cambiada en cualquier tipo de propiedad.'/>
                            <span class='form-error' data-form-error-for='txtCHANGE_FORMATER_VALUE' id='spanCHANGE_FORMATER_VALUE'>
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-change"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-change"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un cambio en campos enmendados (BOTTOM) --%>
<%-- formulario de un cambio en campos enmendados de la enmienda principal (TOP) --%>
<div id="modalFrmAmendmentPrincipalChange" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Campos enmendados</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmAmendmentPrincipalChange" id="frmAmendmentPrincipalChange" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-amendment-principal-change" value="">
                    <div class="grid-margin-x grid-x">
                        <label class='cell medium-6'>Propiedad <span style='color: red'>**</span>
                            <input type='text' id='txtAMENDMENT_PRINCIPAL_CHANGE_PROPERTY' required placeholder='p. ej. Propiedad' title='El nombre de propiedad que ha sido cambiado con relaci&oacute;n al lugar donde se encuentra la enmienda. Por ejemplo si el valor del contrato ha cambiado.'/>
                            <span class='form-error' data-form-error-for='txtAMENDMENT_PRINCIPAL_CHANGE_PROPERTY' id='spanAMENDMENT_PRINCIPAL_CHANGE_PROPERTY'>
                                 Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class='cell medium-6'>Valor anterior
                            <input type='text' id='txtAMENDMENT_PRINCIPAL_CHANGE_FORMATER_VALUE' placeholder='p. ej. Valor previo' title='El valor previo de la propiedad cambiada en cualquier tipo de propiedad.'/>
                            <span class='form-error' data-form-error-for='txtAMENDMENT_PRINCIPAL_CHANGE_FORMATER_VALUE' id='spanAMENDMENT_PRINCIPAL_CHANGE_FORMATER_VALUE'>
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-amendment-principal-change"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-amendment-principal-change"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un cambio en campos enmendados de la enmienda principal (BOTTOM) --%>
<%-- formulario de un identificador - payer (TOP) --%>
<div id="modalFrmIdentifierPayer" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Identificador - Pagador</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmIdentifierPayer" id="frmIdentifierPayer" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-identifier-payer" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-6">ID <span style="color: red">**</span>
                            <input type="text" required id="txtIDENTIFIER_PAYER_ID" placeholder="p. ej. 1" value="" title="El identificador de la organizaci&oacute;n en el esquema seleccionado."/>
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_PAYER_ID" id="spanIDENTIFIER_PAYER_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Esquema
                            <input type="text" id="txtIDENTIFIER_PAYER_SCHEME" placeholder="p. ej. Esquema" value="" title="Los identificadores de organizaci&oacute;n deben de obtenerse de una lista de identificadores de organizaciones existentes. El campo scheme se usa para indicar o registrar de d&oacute;nde se toma el identificador. Este valor deber&iacute;a tomarse del [Esquema de Identificadores de Organizaciones]."/>
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_PAYER_SCHEME" id="spanIDENTIFIER_PAYER_SCHEME">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Nombre Legal
                            <input type="text" id="txtIDENTIFIER_PAYER_LEGAL_NAME" placeholder="p. ej. Nombre Legal" value="" title="El nombre legalmente registrado de la organizaci&oacute;n." />
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_PAYER_LEGAL_NAME" id="spanIDENTIFIER_PAYER_LEGAL_NAME">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">URI
                            <input type="text" id="txtIDENTIFIER_PAYER_URI" placeholder="p. ej. www.url.com" value="" pattern="url" title="Una URI para identificar a la organizaci&oacute;n, como los prove&iacute;dos por [Open Corporates] o alg&uacute;n otro proveedor relevante de URIs. Este campo no debe ser utilizado para especificar el sitio web de la organizaci&oacute;n, el cual puede ser especificado en el campo URL del punto de contacto de la Organizaci&oacute;n." />
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_PAYER_URI" id="spanIDENTIFIER_PAYER_URI">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-identifier-payer"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-identifier-payer"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un identificador -payer (BOTTOM) --%>
<%-- formulario de un identificador - payee (TOP) --%>
<div id="modalFrmIdentifierPayee" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Identificador - Beneficiario</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmIdentifierPayee" id="frmIdentifierPayee" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-identifier-payee" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-6">ID <span style="color: red">**</span>
                            <input type="text" required id="txtIDENTIFIER_PAYEE_ID" placeholder="p. ej. 1" value="" title="El identificador de la organizaci&oacute;n en el esquema seleccionado."/>
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_PAYEE_ID" id="spanIDENTIFIER_PAYEE_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Esquema
                            <input type="text" id="txtIDENTIFIER_PAYEE_SCHEME" placeholder="p. ej. Esquema" value="" title="Los identificadores de organizaci&oacute;n deben de obtenerse de una lista de identificadores de organizaciones existentes. El campo scheme se usa para indicar o registrar de d&oacute;nde se toma el identificador. Este valor deber&iacute;a tomarse del [Esquema de Identificadores de Organizaciones]."/>
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_PAYEE_SCHEME" id="spanIDENTIFIER_PAYEE_SCHEME">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Nombre Legal
                            <input type="text" id="txtIDENTIFIER_PAYEE_LEGAL_NAME" placeholder="p. ej. Nombre Legal" value="" title="El nombre legalmente registrado de la organizaci&oacute;n." />
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_PAYEE_LEGAL_NAME" id="spanIDENTIFIER_PAYEE_LEGAL_NAME">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">URI
                            <input type="text" id="txtIDENTIFIER_PAYEE_URI" placeholder="p. ej. www.url.com" value="" pattern="url" title="Una URI para identificar a la organizaci&oacute;n, como los prove&iacute;dos por [Open Corporates] o alg&uacute;n otro proveedor relevante de URIs. Este campo no debe ser utilizado para especificar el sitio web de la organizaci&oacute;n, el cual puede ser especificado en el campo URL del punto de contacto de la Organizaci&oacute;n." />
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_PAYEE_URI" id="spanIDENTIFIER_PAYEE_URI">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-identifier-payee"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-identifier-payee"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un identificador - payee (BOTTOM) --%>
            
<script>
    
    function validarFormulario(idFormulario)
    { 
        
        let strMensajeFrm;          
        let strTituloMensaje;       
        let bolTipoFormulario;      

        bolTipoFormulario = document.getElementById("txtEsFormularioDeEdicion").value;
        bolTipoFormulario = Boolean(bolTipoFormulario);
        strMensajeFrm = (bolTipoFormulario === true) ? "\u00bfLos datos son correctos\u003F" : "Una vez guardada la informaci\u00F3n de esta secci\u00F3n no se podr\u00E1 volver a modificar los valores ingresados hasta que se finalice con el proceso de registro de la contrataci\u00F3n p\u00FAblica.";
        strTituloMensaje = (bolTipoFormulario === true) ? "Confirmar" : "\u00bf Los datos son correctos\u003F";

        swal({
            title: strTituloMensaje,
            text: strMensajeFrm,
            icon: "warning",
            buttons: ["Cancelar", "Aceptar"],
            dangerMode: true,
            closeOnClickOutside: false,
            closeOnEsc: false
        })
        .then((seAcepta) => {
            if (seAcepta)
            { 
                document.getElementById(idFormulario).submit();
            } 
            else
            { 
                swal("Se ha cancelado la operaci\u00f3n", {
                    icon: "error",
                    buttons: false,
                    timer: 2000
                });
                return false;
            } 
        });
                    
    } 
    
    window.onload = function (){
        
        document.getElementById("estructura-por-defecto-contract").value = obtenerDatosInicialesElementos("contract", []);
        
    };
</script>   