<%-- 
    Autor: Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>
<div id="seccion8">
    <h3>Adjudicaciones</h3>
    <div class="button-group">
        <button type="button" class="button" onclick="abrirNuevoFormulario('award', []);">Agregar adjudicaci&oacute;n</button>
    </div>
    <div class="callout padding-horizontal-3">
        <form name="frmRegistrarSeccionAdjudicaciones" id="frmRegistrarSeccionAdjudicaciones" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post" data-abide novalidate onSubmit="return false;" autocomplete="off">
            <input type="hidden" name="accion" value="${(bolEsFormulariosDeEdicion == true) ? 'editarSeccion' : 'registrarSeccion'}"/>
            <input type="hidden" name="txtEsFormularioDeEdicion" id="txtEsFormularioDeEdicion" value="${bolEsFormulariosDeEdicion}"/>
            <input type="hidden" name="txtNUMERO_SECCION" value="8"/>
            <input type="hidden" name="txtID_CONTRATACION_SPDN" value="${idContratacion}"/>
            <input type="hidden" id="estructura-por-defecto-award" value="">
            <input type="hidden" id="siguiente-identificador-award" value="" data-valor-por-defecto="">
            <input type="hidden" id="lista-identificadores-por-registrar-award" name="lista-identificadores-por-registrar-award" value="" data-valor-por-defecto="">
            <c:choose>
                <c:when test="${contratacion.awards.size() > 0}">
                    <c:set var="contadorElementosAward" value="0"></c:set>
                    <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-award">
                    <c:forEach items="${contratacion.awards}" var="award">
                        <li class="accordion-item" data-accordion-item id="award_${contadorElementosAward}">
                            <a href="#" class="accordion-title"><span style="text-transform: capitalize;">adjudicaci&oacute;n</span> - <span id="titulo-elemento-award_${contadorElementosAward}">${award.id}</span></a>
                            <div class="accordion-content" data-tab-content>
                                <div class="grid-margin-x grid-x">
                                    <div class="cell padding-horizontal-1" style="display:flex; justify-content: flex-end; gap: 10px; flex-wrap: wrap;">
                                        <button class="button margin-0 btnFrmContrataciones" onclick="abrirFormularioPrevio('award', [${contadorElementosAward}])"><i class="material-icons">edit</i> Editar adjudicaci&oacute;n</button>
                                        <button class="button margin-0 btnFrmContrataciones" onclick="eliminarElemento('award', 'award_${contadorElementosAward}', [], '${contadorElementosAward}')"><i class="material-icons">remove_circle</i> Eliminar adjudicaci&oacute;n</button>
                                    </div>
                                    <div id="datos-elemento-award_${contadorElementosAward}">
                                        <input type="hidden" name="txtAWARD_ID_${contadorElementosAward}" id="txtAWARD_ID_${contadorElementosAward}" value="${award.id}">
                                        <input type="hidden" name="txtAWARD_TITLE_${contadorElementosAward}" id="txtAWARD_TITLE_${contadorElementosAward}" value="${award.title}">
                                        <input type="hidden" name="txtAWARD_DESCRIPTION_${contadorElementosAward}" id="txtAWARD_DESCRIPTION_${contadorElementosAward}" value="${award.description}">
                                        <input type="hidden" name="cmbAWARD_STATUS_${contadorElementosAward}" id="cmbAWARD_STATUS_${contadorElementosAward}" value="${award.status}">
                                        <input type="hidden" name="txtAWARD_DATE_${contadorElementosAward}" id="txtAWARD_DATE_${contadorElementosAward}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(award.date)}">
                                        <input type="hidden" name="txtAWARD_AMOUNT_${contadorElementosAward}" id="txtAWARD_AMOUNT_${contadorElementosAward}" value="${award.value.amount}">
                                        <input type="hidden" name="cmbAWARD_CURRENCY_${contadorElementosAward}" id="cmbAWARD_CURRENCY_${contadorElementosAward}" value="${award.value.currency}">
                                        <input type="hidden" name="txtAWARD_PERIOD_START_DATE_${contadorElementosAward}" id="txtAWARD_PERIOD_START_DATE_${contadorElementosAward}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(award.contractPeriod.startDate)}">
                                        <input type="hidden" name="txtAWARD_PERIOD_END_DATE_${contadorElementosAward}" id="txtAWARD_PERIOD_END_DATE_${contadorElementosAward}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(award.contractPeriod.endDate)}">
                                        <input type="hidden" name="txtAWARD_PERIOD_MAX_EXTENT_DATE_${contadorElementosAward}" id="txtAWARD_PERIOD_MAX_EXTENT_DATE_${contadorElementosAward}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(award.contractPeriod.maxExtentDate)}">
                                        <input type="hidden" name="txtAWARD_PERIOD_DURATION_IN_DATE_${contadorElementosAward}" id="txtAWARD_PERIOD_DURATION_IN_DATE_${contadorElementosAward}" value="${award.contractPeriod.durationInDays}">
                                        <input type="hidden" name="txtAWARD_AMENDMENT_ID_${contadorElementosAward}" id="txtAWARD_AMENDMENT_ID_${contadorElementosAward}" value="${award.amendment.id}">
                                        <input type="hidden" name="txtAWARD_AMENDMENT_DATE_${contadorElementosAward}" id="txtAWARD_AMENDMENT_DATE_${contadorElementosAward}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(award.amendment.date)}">
                                        <input type="hidden" name="txtAWARD_AMENDMENT_RATIONALE_${contadorElementosAward}" id="txtAWARD_AMENDMENT_RATIONALE_${contadorElementosAward}" value="${award.amendment.rationale}">
                                        <input type="hidden" name="txtAWARD_AMENDMENT_DESCRIPTION_${contadorElementosAward}" id="txtAWARD_AMENDMENT_DESCRIPTION_${contadorElementosAward}" value="${award.amendment.description}">
                                        <input type="hidden" name="txtAWARD_AMENDMENT_AMENDS_RELEASE_ID_${contadorElementosAward}" id="txtAWARD_AMENDMENT_AMENDS_RELEASE_ID_${contadorElementosAward}" value="${award.amendment.amendsReleaseID}">
                                        <input type="hidden" name="txtAWARD_AMENDMENT_RELEASE_ID_${contadorElementosAward}" id="txtAWARD_AMENDMENT_RELEASE_ID_${contadorElementosAward}" value="${award.amendment.releaseID}">
                                    </div>
                                    <fieldset class="cell fieldset"> 
                                        <input type="hidden" id="estructura-por-defecto-supplier_${contadorElementosAward}" value=""> 
                                        <input type="hidden" id="siguiente-identificador-supplier_${contadorElementosAward}" value="" data-valor-por-defecto=""> 
                                        <input type="hidden" id="lista-identificadores-por-registrar-supplier_${contadorElementosAward}" name="lista-identificadores-por-registrar-supplier_${contadorElementosAward}" value="" data-valor-por-defecto="">
                                        <legend title="Una lista de transacciones de pago realizadas contra este contrato.">Transacciones</legend>
                                        <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                            <a onclick="restaurarEstadoInicialElemento('supplier', [${contadorElementosAward}])"><i class="material-icons secondary">restart_alt</i></a> 
                                            <a onclick="abrirNuevoFormulario('supplier', [${contadorElementosAward}])"><i class="material-icons secondary">add_circle</i></a> 
                                        </div>
                                        <c:choose>
                                            <c:when test="${award.suppliers.size() > 0}">
                                                <c:set var="contadorElementosSupplier" value="0"></c:set>
                                                <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-supplier_${contadorElementosAward}">
                                                <c:forEach items="${award.suppliers}" var="supplier">
                                                    <li class="accordion-item" data-accordion-item id="supplier_${contadorElementosAward}_${contadorElementosSupplier}">
                                                        <a href="#" class="accordion-title"><span style="text-transform: capitalize;">proveedor</span> - <span id="titulo-elemento-supplier_${contadorElementosAward}_${contadorElementosSupplier}">${supplier.id}</span></a>
                                                        <div class="accordion-content" data-tab-content>
                                                            <div class="grid-margin-x grid-x">
                                                                <div class="cell padding-horizontal-1" style="display:flex; justify-content: flex-end; gap: 10px; flex-wrap: wrap;">
                                                                    <button class="button margin-0 btnFrmContrataciones" onclick="abrirFormularioPrevio('supplier', [${contadorElementosAward}, ${contadorElementosSupplier}])"><i class="material-icons">edit</i> Editar proveedor</button>
                                                                    <button class="button margin-0 btnFrmContrataciones" onclick="eliminarElemento('supplier', 'supplier_${contadorElementosAward}_${contadorElementosSupplier}', [${contadorElementosAward}], '${contadorElementosSupplier}')"><i class="material-icons">remove_circle</i> Eliminar proveedor</button>
                                                                </div>
                                                                <div id="datos-elemento-supplier_${contadorElementosAward}_${contadorElementosSupplier}">
                                                                    <input type="hidden" name="txtSUPPLIER_ID_${contadorElementosAward}_${contadorElementosSupplier}" id="txtSUPPLIER_ID_${contadorElementosAward}_${contadorElementosSupplier}" value="${supplier.id}" >
                                                                    <input type="hidden" name="txtSUPPLIER_NAME_${contadorElementosAward}_${contadorElementosSupplier}" id="txtSUPPLIER_NAME_${contadorElementosAward}_${contadorElementosSupplier}" value="${supplier.name}" >
                                                                    <input type="hidden" name="txtSUPPLIER_IDENTIFIER_ID_${contadorElementosAward}_${contadorElementosSupplier}" id="txtSUPPLIER_IDENTIFIER_ID_${contadorElementosAward}_${contadorElementosSupplier}" value="${supplier.identifier.id}" >
                                                                    <input type="hidden" name="txtSUPPLIER_IDENTIFIER_SCHEME_${contadorElementosAward}_${contadorElementosSupplier}" id="txtSUPPLIER_IDENTIFIER_SCHEME_${contadorElementosAward}_${contadorElementosSupplier}" value="${supplier.identifier.scheme}" >
                                                                    <input type="hidden" name="txtSUPPLIER_IDENTIFIER_LEGAL_NAME_${contadorElementosAward}_${contadorElementosSupplier}" id="txtSUPPLIER_IDENTIFIER_LEGAL_NAME_${contadorElementosAward}_${contadorElementosSupplier}" value="${supplier.identifier.legalName}" >
                                                                    <input type="hidden" name="txtSUPPLIER_IDENTIFIER_URI_${contadorElementosAward}_${contadorElementosSupplier}" id="txtSUPPLIER_IDENTIFIER_URI_${contadorElementosAward}_${contadorElementosSupplier}" value="${supplier.identifier.uri}" >
                                                                    <input type="hidden" name="txtSUPPLIER_ADDRESS_STREET_ADDRESS_${contadorElementosAward}_${contadorElementosSupplier}" id="txtSUPPLIER_ADDRESS_STREET_ADDRESS_${contadorElementosAward}_${contadorElementosSupplier}" value="${supplier.address.streetAddress}" >
                                                                    <input type="hidden" name="txtSUPPLIER_ADDRESS_LOCALITY_${contadorElementosAward}_${contadorElementosSupplier}" id="txtSUPPLIER_ADDRESS_LOCALITY_${contadorElementosAward}_${contadorElementosSupplier}" value="${supplier.address.locality}" >
                                                                    <input type="hidden" name="txtSUPPLIER_ADDRESS_REGION_${contadorElementosAward}_${contadorElementosSupplier}" id="txtSUPPLIER_ADDRESS_REGION_${contadorElementosAward}_${contadorElementosSupplier}" value="${supplier.address.region}" >
                                                                    <input type="hidden" name="txtSUPPLIER_ADDRESS_POSTAL_CODE_${contadorElementosAward}_${contadorElementosSupplier}" id="txtSUPPLIER_ADDRESS_POSTAL_CODE_${contadorElementosAward}_${contadorElementosSupplier}" value="${supplier.address.postalCode}" >
                                                                    <input type="hidden" name="txtSUPPLIER_ADDRESS_COUNTRY_NAME_${contadorElementosAward}_${contadorElementosSupplier}" id="txtSUPPLIER_ADDRESS_COUNTRY_NAME_${contadorElementosAward}_${contadorElementosSupplier}" value="${supplier.address.countryName}" >
                                                                    <input type="hidden" name="txtSUPPLIER_CONTACT_NAME_${contadorElementosAward}_${contadorElementosSupplier}" id="txtSUPPLIER_CONTACT_NAME_${contadorElementosAward}_${contadorElementosSupplier}" value="${supplier.contactPoint.name}" >
                                                                    <input type="hidden" name="txtSUPPLIER_CONTACT_EMAIL_${contadorElementosAward}_${contadorElementosSupplier}" id="txtSUPPLIER_CONTACT_EMAIL_${contadorElementosAward}_${contadorElementosSupplier}" value="${supplier.contactPoint.email}" >
                                                                    <input type="hidden" name="txtSUPPLIER_CONTACT_TELEPHONE_${contadorElementosAward}_${contadorElementosSupplier}" id="txtSUPPLIER_CONTACT_TELEPHONE_${contadorElementosAward}_${contadorElementosSupplier}" value="${supplier.contactPoint.telephone}" >
                                                                    <input type="hidden" name="txtSUPPLIER_CONTACT_FAXNUMBER_${contadorElementosAward}_${contadorElementosSupplier}" id="txtSUPPLIER_CONTACT_FAXNUMBER_${contadorElementosAward}_${contadorElementosSupplier}" value="${supplier.contactPoint.faxNumber}" >
                                                                    <input type="hidden" name="txtSUPPLIER_CONTACT_URL_${contadorElementosAward}_${contadorElementosSupplier}" id="txtSUPPLIER_CONTACT_URL_${contadorElementosAward}_${contadorElementosSupplier}" value="${supplier.contactPoint.url}" >
                                                                </div>
                                                                <fieldset class="cell fieldset"> 
                                                                    <input type="hidden" id="estructura-por-defecto-identifier_${contadorElementosAward}_${contadorElementosSupplier}" value=""> 
                                                                    <input type="hidden" id="siguiente-identificador-identifier_${contadorElementosAward}_${contadorElementosSupplier}" value="" data-valor-por-defecto=""> 
                                                                    <input type="hidden" id="lista-identificadores-por-registrar-identifier_${contadorElementosAward}_${contadorElementosSupplier}" name="lista-identificadores-por-registrar-identifier_${contadorElementosAward}_${contadorElementosSupplier}" value="" data-valor-por-defecto="">
                                                                    <legend title="Una lista adicional/suplementaria de identificadores para la organizaci�n, utilizando la [gu�a de identificadores de organizaciones]. Esto puede usarse para dar un identificador utilizado internamente por esta organizaci�n adem�s del identificador primario legal de la entidad.">Identificadores adicionales</legend>
                                                                    <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                                                        <a onclick="restaurarEstadoInicialElemento('identifier', [${contadorElementosAward}, ${contadorElementosSupplier}])"><i class="material-icons secondary">restart_alt</i></a> 
                                                                        <a onclick="abrirNuevoFormulario('identifier', [${contadorElementosAward}, ${contadorElementosSupplier}])"><i class="material-icons secondary">add_circle</i></a> 
                                                                    </div>
                                                                    <c:choose>
                                                                        <c:when test="${supplier.additionalIdentifiers.size() > 0}">
                                                                            <c:set var="contadorElementosIdentifier" value="0"></c:set>
                                                                            <div id="contenedor-identifier_${contadorElementosAward}_${contadorElementosSupplier}" style="display: flex; flex-direction: column; gap: 10px;">
                                                                            <c:forEach items="${supplier.additionalIdentifiers}" var="identifier">
                                                                                <div id="identifier_${contadorElementosAward}_${contadorElementosSupplier}_${contadorElementosIdentifier}">
                                                                                    <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                                                                        <div class="cell medium-8">
                                                                                            <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">identificador</span> - <span id="titulo-elemento-identifier_${contadorElementosAward}_${contadorElementosSupplier}_${contadorElementosIdentifier}">${identifier.id}</span></h6>
                                                                                        </div>
                                                                                        <div id="datos-elemento-identifier_${contadorElementosAward}_${contadorElementosSupplier}_${contadorElementosIdentifier}">
                                                                                            <input type="hidden" name="txtIDENTIFIER_ID_${contadorElementosAward}_${contadorElementosSupplier}_${contadorElementosIdentifier}" id="txtIDENTIFIER_ID_${contadorElementosAward}_${contadorElementosSupplier}_${contadorElementosIdentifier}" value="${identifier.id}" >
                                                                                            <input type="hidden" name="txtIDENTIFIER_SCHEME_${contadorElementosAward}_${contadorElementosSupplier}_${contadorElementosIdentifier}" id="txtIDENTIFIER_SCHEME_${contadorElementosAward}_${contadorElementosSupplier}_${contadorElementosIdentifier}" value="${identifier.scheme}" >
                                                                                            <input type="hidden" name="txtIDENTIFIER_LEGAL_NAME_${contadorElementosAward}_${contadorElementosSupplier}_${contadorElementosIdentifier}" id="txtIDENTIFIER_LEGAL_NAME_${contadorElementosAward}_${contadorElementosSupplier}_${contadorElementosIdentifier}" value="${identifier.legalName}" >
                                                                                            <input type="hidden" name="txtIDENTIFIER_URI_${contadorElementosAward}_${contadorElementosSupplier}_${contadorElementosIdentifier}" id="txtIDENTIFIER_URI_${contadorElementosAward}_${contadorElementosSupplier}_${contadorElementosIdentifier}" value="${identifier.uri}" >                                                        
                                                                                        </div>
                                                                                        <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                                                            <button class="button margin-0" onclick="abrirFormularioPrevio('identifier', [${contadorElementosAward}, ${contadorElementosSupplier}, ${contadorElementosIdentifier}])"><i class="material-icons">edit</i> Editar</button>
                                                                                            <button class="button margin-0" onclick="eliminarElemento('identifier', 'identifier_${contadorElementosAward}_${contadorElementosSupplier}_${contadorElementosIdentifier}', [${contadorElementosAward}, ${contadorElementosSupplier}], '${contadorElementosIdentifier}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <c:set var="contadorElementosIdentifier" value="${contadorElementosIdentifier + 1}"></c:set>
                                                                            </c:forEach>
                                                                            </div>
                                                                            <input type="hidden" name="ultimo-identificador-elementos-identifier_${contadorElementosAward}_${contadorElementosSupplier}" id="ultimo-identificador-elementos-identifier_${contadorElementosAward}_${contadorElementosSupplier}" value="${contadorElementosIdentifier}">
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <div id="contenedor-identifier_${contadorElementosAward}_${contadorElementosSupplier}" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                                                            <input type="hidden" name="ultimo-identificador-elementos-identifier_${contadorElementosAward}_${contadorElementosSupplier}" id="ultimo-identificador-elementos-identifier_${contadorElementosAward}_${contadorElementosSupplier}" value="0">
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                    <script>
                                                                        document.getElementById("estructura-por-defecto-identifier_${contadorElementosAward}_${contadorElementosSupplier}").value = obtenerDatosInicialesElementos("identifier", [${contadorElementosAward}, ${contadorElementosSupplier}]);
                                                                    </script>
                                                                </fieldset> 
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <c:set var="contadorElementosSupplier" value="${contadorElementosSupplier + 1}"></c:set>
                                                </c:forEach>
                                                </ul>
                                                <input type="hidden" name="ultimo-identificador-elementos-supplier_${contadorElementosAward}" id="ultimo-identificador-elementos-supplier_${contadorElementosAward}" value="${contadorElementosSupplier}">
                                            </c:when>
                                            <c:otherwise>
                                                <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-supplier_${contadorElementosAward}"></ul>
                                                <input type="hidden" name="ultimo-identificador-elementos-supplier_${contadorElementosAward}" id="ultimo-identificador-elementos-supplier_${contadorElementosAward}" value="0">
                                            </c:otherwise>
                                        </c:choose>
                                        <script>
                                            document.getElementById("estructura-por-defecto-supplier_${contadorElementosAward}").value = obtenerDatosInicialesElementos("supplier", [${contadorElementosAward}]);
                                        </script>
                                    </fieldset>
                                    <fieldset class="cell fieldset"> 
                                        <input type="hidden" id="estructura-por-defecto-item_${contadorElementosAward}" value=""> 
                                        <input type="hidden" id="siguiente-identificador-item_${contadorElementosAward}" value="" data-valor-por-defecto=""> 
                                        <input type="hidden" id="lista-identificadores-por-registrar-item_${contadorElementosAward}" name="lista-identificadores-por-registrar-item_${contadorElementosAward}" value="" data-valor-por-defecto="">
                                        <legend title="Los bienes, servicios y cualquier resultado intangible de este contrato. Nota: No repetir si los art&iacute;culos son los mismos que la adjudicaci&oacute;n.">Art&iacute;culos que se adquirir&aacute;n</legend>
                                        <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                            <a onclick="restaurarEstadoInicialElemento('item', [${contadorElementosAward}])"><i class="material-icons secondary">restart_alt</i></a> 
                                            <a onclick="abrirNuevoFormulario('item', [${contadorElementosAward}])"><i class="material-icons secondary">add_circle</i></a> 
                                        </div>
                                        <c:choose>
                                            <c:when test="${award.items.size() > 0}">
                                                <c:set var="contadorElementosItem" value="0"></c:set>
                                                <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-item_${contadorElementosAward}">
                                                <c:forEach items="${award.items}" var="item">
                                                    <li class="accordion-item" data-accordion-item id="item_${contadorElementosAward}_${contadorElementosItem}">
                                                        <a href="#" class="accordion-title"><span style="text-transform: capitalize;">art&iacute;culo</span> - <span id="titulo-elemento-item_${contadorElementosAward}_${contadorElementosItem}">${item.id}</span></a>
                                                        <div class="accordion-content" data-tab-content>
                                                            <div class="grid-margin-x grid-x">
                                                                <div class="cell padding-horizontal-1" style="display:flex; justify-content: flex-end; gap: 10px; flex-wrap: wrap;">
                                                                    <button class="button margin-0 btnFrmContrataciones" onclick="abrirFormularioPrevio('item', [${contadorElementosAward}, ${contadorElementosItem}])"><i class="material-icons">edit</i> Editar art&iacute;culo</button>
                                                                    <button class="button margin-0 btnFrmContrataciones" onclick="eliminarElemento('item', 'item_${contadorElementosAward}_${contadorElementosItem}', [${contadorElementosAward}], '${contadorElementosItem}')"><i class="material-icons">remove_circle</i> Eliminar art&iacute;culo</button>
                                                                </div>
                                                                <div id="datos-elemento-item_${contadorElementosAward}_${contadorElementosItem}">
                                                                    <input type="hidden" name="txtITEM_ID_${contadorElementosAward}_${contadorElementosItem}" id="txtITEM_ID_${contadorElementosAward}_${contadorElementosItem}" value="${item.id}">
                                                                    <input type="hidden" name="txtITEM_DESCRIPTION_${contadorElementosAward}_${contadorElementosItem}" id="txtITEM_DESCRIPTION_${contadorElementosAward}_${contadorElementosItem}" value="${item.description}">
                                                                    <input type="hidden" name="txtITEM_CLASSIFICATION_ID_${contadorElementosAward}_${contadorElementosItem}" id="txtITEM_CLASSIFICATION_ID_${contadorElementosAward}_${contadorElementosItem}" value="${item.classification.id}">
                                                                    <input type="hidden" name="cmbITEM_CLASSIFICATION_SCHEME_${contadorElementosAward}_${contadorElementosItem}" id="cmbITEM_CLASSIFICATION_SCHEME_${contadorElementosAward}_${contadorElementosItem}" value="${item.classification.scheme}">
                                                                    <input type="hidden" name="txtITEM_CLASSIFICATION_DESCRIPTION_${contadorElementosAward}_${contadorElementosItem}" id="txtITEM_CLASSIFICATION_DESCRIPTION_${contadorElementosAward}_${contadorElementosItem}" value="${item.classification.description}">
                                                                    <input type="hidden" name="txtITEM_CLASSIFICATION_URI_${contadorElementosAward}_${contadorElementosItem}" id="txtITEM_CLASSIFICATION_URI_${contadorElementosAward}_${contadorElementosItem}" value="${item.classification.uri}">
                                                                    <input type="hidden" name="txtITEM_QUANTITY_${contadorElementosAward}_${contadorElementosItem}" id="txtITEM_QUANTITY_${contadorElementosAward}_${contadorElementosItem}" value="${item.quantity}">
                                                                    <input type="hidden" name="txtITEM_UNIT_ID_${contadorElementosAward}_${contadorElementosItem}" id="txtITEM_UNIT_ID_${contadorElementosAward}_${contadorElementosItem}" value="${item.unit.id}">
                                                                    <input type="hidden" name="cmbITEM_UNIT_SCHEME_${contadorElementosAward}_${contadorElementosItem}" id="cmbITEM_UNIT_SCHEME_${contadorElementosAward}_${contadorElementosItem}" value="${item.unit.scheme}">
                                                                    <input type="hidden" name="txtITEM_UNIT_NAME_${contadorElementosAward}_${contadorElementosItem}" id="txtITEM_UNIT_NAME_${contadorElementosAward}_${contadorElementosItem}" value="${item.unit.name}">
                                                                    <input type="hidden" name="txtITEM_UNIT_URI_${contadorElementosAward}_${contadorElementosItem}" id="txtITEM_UNIT_URI_${contadorElementosAward}_${contadorElementosItem}" value="${item.unit.uri}">
                                                                    <input type="hidden" name="txtITEM_UNIT_AMOUNT_${contadorElementosAward}_${contadorElementosItem}" id="txtITEM_UNIT_AMOUNT_${contadorElementosAward}_${contadorElementosItem}" value="${item.unit.value.amount}">
                                                                    <input type="hidden" name="cmbITEM_UNIT_CURRENCY_${contadorElementosAward}_${contadorElementosItem}" id="cmbITEM_UNIT_CURRENCY_${contadorElementosAward}_${contadorElementosItem}" value="${item.unit.value.currency}">
                                                                </div>
                                                                <fieldset class="cell fieldset"> 
                                                                    <input type="hidden" id="estructura-por-defecto-classification_${contadorElementosAward}_${contadorElementosItem}" value=""> 
                                                                    <input type="hidden" id="siguiente-identificador-classification_${contadorElementosAward}_${contadorElementosItem}" value="" data-valor-por-defecto=""> 
                                                                    <input type="hidden" id="lista-identificadores-por-registrar-classification_${contadorElementosAward}_${contadorElementosItem}" name="lista-identificadores-por-registrar-classification_${contadorElementosAward}_${contadorElementosItem}" value="" data-valor-por-defecto="">
                                                                    <legend title="Una lista de clasificaciones adicionales para el art&iacute;culo.">Clasificaciones adicionales</legend>
                                                                    <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                                                        <a onclick="restaurarEstadoInicialElemento('classification', [${contadorElementosAward}, ${contadorElementosItem}])"><i class="material-icons secondary">restart_alt</i></a> 
                                                                        <a onclick="abrirNuevoFormulario('classification', [${contadorElementosAward}, ${contadorElementosItem}])"><i class="material-icons secondary">add_circle</i></a> 
                                                                    </div>
                                                                    <c:choose>
                                                                        <c:when test="${item.additionalClassifications.size() > 0}">
                                                                            <c:set var="contadorElementosClassification" value="0"></c:set>
                                                                            <div id="contenedor-classification_${contadorElementosAward}_${contadorElementosItem}" style="display: flex; flex-direction: column; gap: 10px;">
                                                                            <c:forEach items="${item.additionalClassifications}" var="clasificacion">
                                                                                <div id="classification_${contadorElementosAward}_${contadorElementosItem}_${contadorElementosClassification}">
                                                                                    <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                                                                        <div class="cell medium-8">
                                                                                            <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">clasificaci&oacute;n</span> - <span id="titulo-elemento-classification_${contadorElementosAward}_${contadorElementosItem}_${contadorElementosClassification}">${clasificacion.id}</span></h6>
                                                                                        </div>
                                                                                        <div id="datos-elemento-classification_${contadorElementosAward}_${contadorElementosItem}_${contadorElementosClassification}">
                                                                                            <input type="hidden" name="txtCLASSIFICATION_ID_${contadorElementosAward}_${contadorElementosItem}_${contadorElementosClassification}" id="txtCLASSIFICATION_ID_${contadorElementosAward}_${contadorElementosItem}_${contadorElementosClassification}" value="${clasificacion.id}">
                                                                                            <input type="hidden" name="cmbCLASSIFICATION_SCHEME_${contadorElementosAward}_${contadorElementosItem}_${contadorElementosClassification}" id="cmbCLASSIFICATION_SCHEME_${contadorElementosAward}_${contadorElementosItem}_${contadorElementosClassification}" value="${clasificacion.scheme}">
                                                                                            <input type="hidden" name="txtCLASSIFICATION_DESCRIPTION_${contadorElementosAward}_${contadorElementosItem}_${contadorElementosClassification}" id="txtCLASSIFICATION_DESCRIPTION_${contadorElementosAward}_${contadorElementosItem}_${contadorElementosClassification}" value="${clasificacion.description}">
                                                                                            <input type="hidden" name="txtCLASSIFICATION_URI_${contadorElementosAward}_${contadorElementosItem}_${contadorElementosClassification}" id="txtCLASSIFICATION_URI_${contadorElementosAward}_${contadorElementosItem}_${contadorElementosClassification}" value="${clasificacion.uri}">
                                                                                        </div>
                                                                                        <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                                                            <button class="button margin-0" onclick="abrirFormularioPrevio('classification', [${contadorElementosAward}, ${contadorElementosItem}, ${contadorElementosClassification}])"><i class="material-icons">edit</i> Editar</button>
                                                                                            <button class="button margin-0" onclick="eliminarElemento('classification', 'classification_${contadorElementosAward}_${contadorElementosItem}_${contadorElementosClassification}', [${contadorElementosAward}, ${contadorElementosItem}], '${contadorElementosClassification}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <c:set var="contadorElementosClassification" value="${contadorElementosClassification + 1}"></c:set>
                                                                            </c:forEach>
                                                                            </div>
                                                                            <input type="hidden" name="ultimo-identificador-elementos-classification_${contadorElementosAward}_${contadorElementosItem}" id="ultimo-identificador-elementos-classification_${contadorElementosAward}_${contadorElementosItem}" value="${contadorElementosClassification}">
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <div id="contenedor-classification_${contadorElementosAward}_${contadorElementosItem}" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                                                            <input type="hidden" name="ultimo-identificador-elementos-classification_${contadorElementosAward}_${contadorElementosItem}" id="ultimo-identificador-elementos-classification_${contadorElementosAward}_${contadorElementosItem}" value="0">
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                    <script>
                                                                        document.getElementById("estructura-por-defecto-classification_${contadorElementosAward}_${contadorElementosItem}").value = obtenerDatosInicialesElementos("classification", [${contadorElementosAward}, ${contadorElementosItem}]);
                                                                    </script>
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <c:set var="contadorElementosItem" value="${contadorElementosItem + 1}"></c:set>
                                                </c:forEach>
                                                </ul>
                                                <input type="hidden" name="ultimo-identificador-elementos-item_${contadorElementosAward}" id="ultimo-identificador-elementos-item_${contadorElementosAward}" value="${contadorElementosItem}">
                                            </c:when>
                                            <c:otherwise>
                                                <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-item_${contadorElementosAward}"></ul>
                                                <input type="hidden" name="ultimo-identificador-elementos-item_${contadorElementosAward}" id="ultimo-identificador-elementos-item_${contadorElementosAward}" value="0">
                                            </c:otherwise>
                                        </c:choose>
                                        <script>
                                            document.getElementById("estructura-por-defecto-item_${contadorElementosAward}").value = obtenerDatosInicialesElementos("item", [${contadorElementosAward}]);
                                        </script>
                                    </fieldset>
                                    <fieldset class="cell fieldset"> 
                                        <input type="hidden" id="estructura-por-defecto-document_${contadorElementosAward}" value=""> 
                                        <input type="hidden" id="siguiente-identificador-document_${contadorElementosAward}" value="" data-valor-por-defecto=""> 
                                        <input type="hidden" id="lista-identificadores-por-registrar-document_${contadorElementosAward}" name="lista-identificadores-por-registrar-document_${contadorElementosAward}" value="" data-valor-por-defecto="">
                                        <legend title="Todos los documentos y archivos adjuntos relacionados con el contrato, incluyendo cualquier aviso o notificaci&oacute;n.">Documentos</legend>
                                        <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                            <a onclick="restaurarEstadoInicialElemento('document', [${contadorElementosAward}])"><i class="material-icons secondary">restart_alt</i></a> 
                                            <a onclick="abrirNuevoFormulario('document', [${contadorElementosAward}])"><i class="material-icons secondary">add_circle</i></a> 
                                        </div>
                                        <c:choose>
                                            <c:when test="${award.documents.size() > 0}">
                                                <c:set var="contadorElementosDocument" value="0"></c:set>
                                                <div id="contenedor-document_${contadorElementosAward}" style="display: flex; flex-direction: column; gap: 10px;">
                                                <c:forEach items="${award.documents}" var="documento">
                                                    <div id="document_${contadorElementosAward}_${contadorElementosDocument}">
                                                        <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                                            <div class="cell medium-8">
                                                                <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">documento</span> - <span id="titulo-elemento-document_${contadorElementosAward}_${contadorElementosDocument}">${documento.id}</span></h6>
                                                            </div>
                                                            <div id="datos-elemento-document_${contadorElementosAward}_${contadorElementosDocument}">
                                                                <input type="hidden" name="txtDOCUMENT_ID_${contadorElementosAward}_${contadorElementosDocument}" id="txtDOCUMENT_ID_${contadorElementosAward}_${contadorElementosDocument}" value="${documento.id}">
                                                                <input type="hidden" name="cmbDOCUMENT_TYPE_${contadorElementosAward}_${contadorElementosDocument}" id="cmbDOCUMENT_TYPE_${contadorElementosAward}_${contadorElementosDocument}" value="${documento.documentType}">
                                                                <input type="hidden" name="txtDOCUMENT_TITLE_${contadorElementosAward}_${contadorElementosDocument}" id="txtDOCUMENT_TITLE_${contadorElementosAward}_${contadorElementosDocument}" value="${documento.title}">
                                                                <input type="hidden" name="txtDOCUMENT_DESCRIPTION_${contadorElementosAward}_${contadorElementosDocument}" id="txtDOCUMENT_DESCRIPTION_${contadorElementosAward}_${contadorElementosDocument}" value="${documento.description}">
                                                                <input type="hidden" name="txtDOCUMENT_URL_${contadorElementosAward}_${contadorElementosDocument}" id="txtDOCUMENT_URL_${contadorElementosAward}_${contadorElementosDocument}" value="${documento.url}">
                                                                <input type="hidden" name="txtDOCUMENT_DATE_PUBLISHED_${contadorElementosAward}_${contadorElementosDocument}" id="txtDOCUMENT_DATE_PUBLISHED_${contadorElementosAward}_${contadorElementosDocument}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(documento.datePublished)}">
                                                                <input type="hidden" name="txtDOCUMENT_DATE_MODIFIED_${contadorElementosAward}_${contadorElementosDocument}" id="txtDOCUMENT_DATE_MODIFIED_${contadorElementosAward}_${contadorElementosDocument}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(documento.dateModified)}">
                                                                <input type="hidden" name="txtDOCUMENT_FORMAT_${contadorElementosAward}_${contadorElementosDocument}" id="txtDOCUMENT_FORMAT_${contadorElementosAward}_${contadorElementosDocument}" value="${documento.format}">
                                                                <input type="hidden" name="cmbDOCUMENT_LANGUAGE_${contadorElementosAward}_${contadorElementosDocument}" id="cmbDOCUMENT_LANGUAGE_${contadorElementosAward}_${contadorElementosDocument}" value="${documento.language}">
                                                            </div>
                                                            <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                                <button class="button margin-0" onclick="abrirFormularioPrevio('document', [${contadorElementosAward}, ${contadorElementosDocument}])"><i class="material-icons">edit</i> Editar</button>
                                                                <button class="button margin-0" onclick="eliminarElemento('document', 'document_${contadorElementosAward}_${contadorElementosDocument}', [${contadorElementosAward}], '${contadorElementosDocument}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <c:set var="contadorElementosDocument" value="${contadorElementosDocument + 1}"></c:set>
                                                </c:forEach>
                                                </div>
                                                <input type="hidden" name="ultimo-identificador-elementos-document_${contadorElementosAward}" id="ultimo-identificador-elementos-document_${contadorElementosAward}" value="${contadorElementosDocument}">
                                            </c:when>
                                            <c:otherwise>
                                                <div id="contenedor-document_${contadorElementosAward}" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                                <input type="hidden" name="ultimo-identificador-elementos-document_${contadorElementosAward}" id="ultimo-identificador-elementos-document_${contadorElementosAward}" value="0">
                                            </c:otherwise>
                                        </c:choose>
                                        <script>
                                            document.getElementById("estructura-por-defecto-document_${contadorElementosAward}").value = obtenerDatosInicialesElementos("document", [${contadorElementosAward}]);
                                        </script>
                                    </fieldset>
                                    <fieldset class="cell fieldset"> 
                                        <input type="hidden" id="estructura-por-defecto-amendment_${contadorElementosAward}" value=""> 
                                        <input type="hidden" id="siguiente-identificador-amendment_${contadorElementosAward}" value="" data-valor-por-defecto=""> 
                                        <input type="hidden" id="lista-identificadores-por-registrar-amendment_${contadorElementosAward}" name="lista-identificadores-por-registrar-amendment_${contadorElementosAward}" value="" data-valor-por-defecto="">
                                        <legend title="Una enmienda de contrato es un cambio formal a los detalles del mismo y generalmente implica la publicaci&oacute;n de una nueva entrega o aviso de una nueva licitaci&oacute;n, as&iacute; como de otros documentos de detallan el cambio. La raz&oacute;n y una descripci&oacute;n de los cambios realizados se pueden proveer aqu&iacute;.">Enmiendas</legend>
                                        <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                            <a onclick="restaurarEstadoInicialElemento('amendment', [${contadorElementosAward}])"><i class="material-icons secondary">restart_alt</i></a> 
                                            <a onclick="abrirNuevoFormulario('amendment', [${contadorElementosAward}])"><i class="material-icons secondary">add_circle</i></a> 
                                        </div>
                                        <c:choose>
                                            <c:when test="${award.amendments.size() > 0}">
                                                <c:set var="contadorElementosAmendment" value="0"></c:set>
                                                <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-amendment_${contadorElementosAward}">
                                                <c:forEach items="${award.amendments}" var="amendment">
                                                    <li class="accordion-item" data-accordion-item id="amendment_${contadorElementosAward}_${contadorElementosAmendment}">
                                                        <a href="#" class="accordion-title"><span style="text-transform: capitalize;">enmienda</span> - <span id="titulo-elemento-amendment_${contadorElementosAward}_${contadorElementosAmendment}">${amendment.id}</span></a>
                                                        <div class="accordion-content" data-tab-content>
                                                            <div class="grid-margin-x grid-x">
                                                                <div class="cell padding-horizontal-1" style="display:flex; justify-content: flex-end; gap: 10px; flex-wrap: wrap;">
                                                                    <button class="button margin-0 btnFrmContrataciones" onclick="abrirFormularioPrevio('amendment', [${contadorElementosAward}, ${contadorElementosAmendment}])"><i class="material-icons">edit</i> Editar enmienda</button>
                                                                    <button class="button margin-0 btnFrmContrataciones" onclick="eliminarElemento('amendment', 'amendment_${contadorElementosAward}_${contadorElementosAmendment}', [${contadorElementosAward}], '${contadorElementosAmendment}')"><i class="material-icons">remove_circle</i> Eliminar enmienda</button>
                                                                </div>
                                                                <div id="datos-elemento-amendment_${contadorElementosAward}_${contadorElementosAmendment}">
                                                                    <input type="hidden" name="txtAMENDMENT_ID_${contadorElementosAward}_${contadorElementosAmendment}" id="txtAMENDMENT_ID_${contadorElementosAward}_${contadorElementosAmendment}" value="${amendment.id}">
                                                                    <input type="hidden" name="txtAMENDMENT_DATE_${contadorElementosAward}_${contadorElementosAmendment}" id="txtAMENDMENT_DATE_${contadorElementosAward}_${contadorElementosAmendment}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(amendment.date)}">
                                                                    <input type="hidden" name="txtAMENDMENT_RATIONALE_${contadorElementosAward}_${contadorElementosAmendment}" id="txtAMENDMENT_RATIONALE_${contadorElementosAward}_${contadorElementosAmendment}" value="${amendment.rationale}">
                                                                    <input type="hidden" name="txtAMENDMENT_DESCRIPTION_${contadorElementosAward}_${contadorElementosAmendment}" id="txtAMENDMENT_DESCRIPTION_${contadorElementosAward}_${contadorElementosAmendment}" value="${amendment.description}">
                                                                    <input type="hidden" name="txtAMENDMENT_AMENDS_RELEASE_ID_${contadorElementosAward}_${contadorElementosAmendment}" id="txtAMENDMENT_AMENDS_RELEASE_ID_${contadorElementosAward}_${contadorElementosAmendment}" value="${amendment.amendsReleaseID}">
                                                                    <input type="hidden" name="txtAMENDMENT_RELEASE_ID_${contadorElementosAward}_${contadorElementosAmendment}" id="txtAMENDMENT_RELEASE_ID_${contadorElementosAward}_${contadorElementosAmendment}" value="${amendment.releaseID}">
                                                                </div>
                                                                <fieldset class="cell fieldset"> 
                                                                    <input type="hidden" id="estructura-por-defecto-change_${contadorElementosAward}_${contadorElementosAmendment}" value=""> 
                                                                    <input type="hidden" id="siguiente-identificador-change_${contadorElementosAward}_${contadorElementosAmendment}" value="" data-valor-por-defecto=""> 
                                                                    <input type="hidden" id="lista-identificadores-por-registrar-change_${contadorElementosAward}_${contadorElementosAmendment}" name="lista-identificadores-por-registrar-change_${contadorElementosAward}_${contadorElementosAmendment}" value="" data-valor-por-defecto="">
                                                                    <legend title="Una lista de cambios que describen los campos que cambiaron y sus valores previos.">Campos enmendados</legend>
                                                                    <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                                                        <a onclick="restaurarEstadoInicialElemento('change', [${contadorElementosAward}, ${contadorElementosAmendment}])"><i class="material-icons secondary">restart_alt</i></a> 
                                                                        <a onclick="abrirNuevoFormulario('change', [${contadorElementosAward}, ${contadorElementosAmendment}])"><i class="material-icons secondary">add_circle</i></a> 
                                                                    </div>
                                                                    <c:choose>
                                                                        <c:when test="${amendment.changes.size() > 0}">
                                                                            <c:set var="contadorElementosAmendmentChange" value="0"></c:set>
                                                                            <div id="contenedor-change_${contadorElementosAward}_${contadorElementosAmendment}" style="display: flex; flex-direction: column; gap: 10px;">
                                                                            <c:forEach items="${amendment.changes}" var="amendmentChange">
                                                                                <div id="change_${contadorElementosAward}_${contadorElementosAmendment}_${contadorElementosAmendmentChange}">
                                                                                    <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                                                                        <div class="cell medium-8">
                                                                                            <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">cambio</span> - <span id="titulo-elemento-change_${contadorElementosAward}_${contadorElementosAmendment}_${contadorElementosAmendmentChange}">${amendmentChange.property}</span></h6>
                                                                                        </div>
                                                                                        <div id="datos-elemento-change_${contadorElementosAward}_${contadorElementosAmendment}_${contadorElementosAmendmentChange}">
                                                                                            <input type="hidden" name="txtCHANGE_PROPERTY_${contadorElementosAward}_${contadorElementosAmendment}_${contadorElementosAmendmentChange}" id="txtCHANGE_PROPERTY_${contadorElementosAward}_${contadorElementosAmendment}_${contadorElementosAmendmentChange}" value="${amendmentChange.property}">
                                                                                            <input type="hidden" name="txtCHANGE_FORMATER_VALUE_${contadorElementosAward}_${contadorElementosAmendment}_${contadorElementosAmendmentChange}" id="txtCHANGE_FORMATER_VALUE_${contadorElementosAward}_${contadorElementosAmendment}_${contadorElementosAmendmentChange}" value="${amendmentChange.former_value}">
                                                                                        </div>
                                                                                        <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                                                            <button class="button margin-0" onclick="abrirFormularioPrevio('change', [${contadorElementosAward}, ${contadorElementosAmendment}, ${contadorElementosAmendmentChange}])"><i class="material-icons">edit</i> Editar</button>
                                                                                            <button class="button margin-0" onclick="eliminarElemento('change', 'change_${contadorElementosAward}_${contadorElementosAmendment}_${contadorElementosAmendmentChange}', [${contadorElementosAward}, ${contadorElementosAmendment}], '${contadorElementosAmendmentChange}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <c:set var="contadorElementosAmendmentChange" value="${contadorElementosAmendmentChange + 1}"></c:set>
                                                                            </c:forEach>
                                                                            </div>
                                                                            <input type="hidden" name="ultimo-identificador-elementos-change_${contadorElementosAward}_${contadorElementosAmendment}" id="ultimo-identificador-elementos-change_${contadorElementosAward}_${contadorElementosAmendment}" value="${contadorElementosAmendmentChange}">
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <div id="contenedor-change_${contadorElementosAward}_${contadorElementosAmendment}" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                                                            <input type="hidden" name="ultimo-identificador-elementos-change_${contadorElementosAward}_${contadorElementosAmendment}" id="ultimo-identificador-elementos-change_${contadorElementosAward}_${contadorElementosAmendment}" value="0">
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                    <script>
                                                                        document.getElementById("estructura-por-defecto-change_${contadorElementosAward}_${contadorElementosAmendment}").value = obtenerDatosInicialesElementos("change", [${contadorElementosAward}, ${contadorElementosAmendment}]);
                                                                    </script>
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <c:set var="contadorElementosAmendment" value="${contadorElementosAmendment + 1}"></c:set>
                                                </c:forEach>
                                                </ul>
                                                <input type="hidden" name="ultimo-identificador-elementos-amendment_${contadorElementosAward}" id="ultimo-identificador-elementos-amendment_${contadorElementosAward}" value="${contadorElementosAmendment}">
                                            </c:when>
                                            <c:otherwise>
                                                <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-amendment_${contadorElementosAward}"></ul>
                                                <input type="hidden" name="ultimo-identificador-elementos-amendment_${contadorElementosAward}" id="ultimo-identificador-elementos-amendment_${contadorElementosAward}" value="0">
                                            </c:otherwise>
                                        </c:choose>
                                        <script>
                                            document.getElementById("estructura-por-defecto-amendment_${contadorElementosAward}").value = obtenerDatosInicialesElementos("amendment", [${contadorElementosAward}]);
                                        </script>
                                    </fieldset>
                                    <fieldset class="cell fieldset"> 
                                        <input type="hidden" id="estructura-por-defecto-amendment-principal-change_${contadorElementosAward}" value=""> 
                                        <input type="hidden" id="siguiente-identificador-amendment-principal-change_${contadorElementosAward}" value="" data-valor-por-defecto=""> 
                                        <input type="hidden" id="lista-identificadores-por-registrar-amendment-principal-change_${contadorElementosAward}" name="lista-identificadores-por-registrar-amendment-principal-change_${contadorElementosAward}" value="" data-valor-por-defecto="">
                                        <legend title="Una lista de cambios que describen los campos que cambiaron y sus valores previos.">Campos enmendados de la enmienda principal</legend>
                                        <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                            <a onclick="restaurarEstadoInicialElemento('amendment-principal-change', [${contadorElementosAward}])"><i class="material-icons secondary">restart_alt</i></a> 
                                            <a onclick="abrirNuevoFormulario('amendment-principal-change', [${contadorElementosAward}])"><i class="material-icons secondary">add_circle</i></a> 
                                        </div>
                                        <c:choose>
                                            <c:when test="${award.amendment.changes.size() > 0}">
                                                <c:set var="contadorElementosAmendmetPrincipalChange" value="0"></c:set>
                                                <div id="contenedor-amendment-principal-change_${contadorElementosAward}" style="display: flex; flex-direction: column; gap: 10px;">
                                                <c:forEach items="${award.amendment.changes}" var="amendmentPrincipalChange">
                                                    <div id="amendment-principal-change_${contadorElementosAward}_${contadorElementosAmendmetPrincipalChange}">
                                                        <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                                            <div class="cell medium-8">
                                                                <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">cambio</span> - <span id="titulo-elemento-amendment-principal-change_${contadorElementosAward}_${contadorElementosAmendmetPrincipalChange}">${amendmentPrincipalChange.property}</span></h6>
                                                            </div>
                                                            <div id="datos-elemento-amendment-principal-change_${contadorElementosAward}_${contadorElementosAmendmetPrincipalChange}">
                                                                <input type="hidden" name="txtAMENDMENT_PRINCIPAL_CHANGE_PROPERTY_${contadorElementosAward}_${contadorElementosAmendmetPrincipalChange}" id="txtAMENDMENT_PRINCIPAL_CHANGE_PROPERTY_${contadorElementosAward}_${contadorElementosAmendmetPrincipalChange}" value="${amendmentPrincipalChange.property}">
                                                                <input type="hidden" name="txtAMENDMENT_PRINCIPAL_CHANGE_FORMATER_VALUE_${contadorElementosAward}_${contadorElementosAmendmetPrincipalChange}" id="txtAMENDMENT_PRINCIPAL_CHANGE_FORMATER_VALUE_${contadorElementosAward}_${contadorElementosAmendmetPrincipalChange}" value="${amendmentPrincipalChange.former_value}">
                                                            </div>
                                                            <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                                <button class="button margin-0" onclick="abrirFormularioPrevio('amendment-principal-change', [${contadorElementosAward}, ${contadorElementosAmendmetPrincipalChange}])"><i class="material-icons">edit</i> Editar</button>
                                                                <button class="button margin-0" onclick="eliminarElemento('amendment-principal-change', 'amendment-principal-change_${contadorElementosAward}_${contadorElementosAmendmetPrincipalChange}', [${contadorElementosAward}], '${contadorElementosAmendmetPrincipalChange}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <c:set var="contadorElementosAmendmetPrincipalChange" value="${contadorElementosAmendmetPrincipalChange + 1}"></c:set>
                                                </c:forEach>
                                                </div>
                                                <input type="hidden" name="ultimo-identificador-elementos-amendment-principal-change_${contadorElementosAward}" id="ultimo-identificador-elementos-amendment-principal-change_${contadorElementosAward}" value="${contadorElementosAmendmetPrincipalChange}">
                                            </c:when>
                                            <c:otherwise>
                                                <div id="contenedor-amendment-principal-change_${contadorElementosAward}" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                                <input type="hidden" name="ultimo-identificador-elementos-amendment-principal-change_${contadorElementosAward}" id="ultimo-identificador-elementos-amendment-principal-change_${contadorElementosAward}" value="0">
                                            </c:otherwise>
                                        </c:choose>
                                        <script>
                                            document.getElementById("estructura-por-defecto-amendment-principal-change_${contadorElementosAward}").value = obtenerDatosInicialesElementos("amendment-principal-change", [${contadorElementosAward}]);
                                        </script>
                                    </fieldset>
                                </div>
                            </div>
                        </li>
                        <c:set var="contadorElementosAward" value="${contadorElementosAward + 1}"></c:set>
                    </c:forEach>
                    </ul>
                    <input type="hidden" name="ultimo-identificador-elementos-award" id="ultimo-identificador-elementos-award" value="${contadorElementosAward}">
                </c:when>
                <c:otherwise>
                    <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-award"></ul>
                    <input type="hidden" name="ultimo-identificador-elementos-award" id="ultimo-identificador-elementos-award" value="0">
                </c:otherwise>
            </c:choose>   
            <div class="grid-x grid-margin-x margin-top-1">
                <fieldset class="cell medium-6">
                    <input type="button" class="button expanded secondary" onclick="restaurarEstadoInicialElemento('award', []);" value="${(bolEsFormulariosDeEdicion == true) ? 'Restaurar' : 'Reiniciar'}"/>
                </fieldset>
                <fieldset class="cell medium-6">
                    <input type="button" onclick="validarFormulario('frmRegistrarSeccionAdjudicaciones');" class="button expanded" id="btnRegistrarValoresFormulario" value="${(bolEsFormulariosDeEdicion == true) ? 'Modificar datos' : 'Continuar'}"/>
                </fieldset>
            </div>
        </form>
    </div>
    
</div>
      
<%-- formulario de una adjudicacion (TOP) --%>
<div id="modalFrmAward" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Adjudicaci&oacute;n</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmAward" id="frmAward" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-award" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-4">ID de adjudicaci&oacute;n
                            <input type="text" id="txtAWARD_ID" required placeholder="p. ej. 1" value="" title="El identificador para esta adjudicaci&oacute;n. Debe ser &uacute;nico y no debe cambiar en el Proceso de Contrataciones Abiertas del cual es parte (definido por un ocid &uacute;nico)."/>
                            <span class="form-error" data-form-error-for="txtAWARD_ID" id="spanAWARD_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">T&iacute;tulo
                            <input type="text" id="txtAWARD_TITLE" placeholder="p. ej. T&iacute;tulo de la adjudicaci&oacute;n" value="" title="T&iacute;tulo de la adjudicaci&oacute;n."/>
                            <span class="form-error" data-form-error-for="txtAWARD_TITLE" id="spanAWARD_TITLE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Descripci&oacute;n
                            <input type="text" id="txtAWARD_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n de la adjudicaci&oacute;n" value="" title="Descripci&oacute;n de la adjudicaci&oacute;n."/>
                            <span class="form-error" data-form-error-for="txtAWARD_DESCRIPTION" id="spanAWARD_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Estado de la adjudicaci&oacute;n
                            <select id="cmbAWARD_STATUS" title="El estatus actual de la adjudicaci&oacute;n.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${estadosAdjudicacion}" var="estado">
                                    <option value="${estado.clave}">${estado.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbAWARD_STATUS" id="spanAWARD_STATUS">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de adjudicaci&oacute;n
                            <input type="date" id="txtAWARD_DATE" value="" title="La fecha de la adjudicaci&oacute;n del contrato. Usualmente es la fecha en que se tom&oacute; la decisi&oacute;n de adjudicar."/>
                            <span class="form-error" data-form-error-for="txtAWARD_DATE" id="spanAWARD_DATE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <fieldset class="fieldset cell">
                            <legend title="El valor o monto total de esta adjudicaci&oacute;n. En el caso de un contracto marco este puede ser el estimado del valor total o valor m&aacute;ximo del acuerdo. Puede haber m&aacute;s de una adjudicaci&oacute;n por proceso de contrataci&oacute;n. Un valor negativo indica que la adjudicaci&oacute;n puede implicar pagos del proveedor al comprador (com&uacute;nmente utilizado en contratos de concesiones).">Valor</legend>
                            <div class="grid-margin-x grid-x">
                                <label class="cell medium-6">Monto
                                    <input type="number" id="txtAWARD_AMOUNT" placeholder="p. ej. 0" min="0" value="" title="Monto como una cifra."/>
                                    <span class="form-error" data-form-error-for="txtAWARD_AMOUNT" id="spanAWARD_AMOUNT">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Moneda
                                    <select id="cmbAWARD_CURRENCY" title="La moneda del monto.">
                                        <option value="">--Elige una opci&oacute;n--</option>
                                        <c:forEach items="${tipoMoneda}" var="moneda">
                                            <option value="${moneda.clave}" >${moneda.valor}</option>
                                        </c:forEach>
                                    </select>
                                    <span class="form-error" data-form-error-for="cmbAWARD_CURRENCY" id="spanAWARD_CURRENCY">
                                        Elige una opci&oacute;n.
                                    </span>
                                </label>
                            </div>
                        </fieldset>         
                        <fieldset class="fieldset cell">
                            <legend title="El periodo por el cual se ha adjudicado el contrato.">Periodo de contrato</legend>
                            <div class="grid-margin-x grid-x">
                                <label class="cell medium-6">Fecha de inicio
                                    <input type="date" id="txtAWARD_PERIOD_START_DATE" value="" title="La fecha de inicio del per&iacute;odo. Cuando se sabe, se debe dar una fecha de inicio precisa."/>
                                    <span class="form-error" data-form-error-for="txtAWARD_PERIOD_START_DATE" id="spanAWARD_PERIOD_START_DATE">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Fecha de fin
                                    <input type="date" id="txtAWARD_PERIOD_END_DATE" value="" title="La fecha de conclusi&oacute;n del per&iacute;odo. Cuando se sabe, se debe dar una fecha de conclusi&oacute;n precisa."/>
                                    <span class="form-error" data-form-error-for="txtAWARD_PERIOD_END_DATE" id="spanAWARD_PERIOD_END_DATE">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Extensi&oacute;n m&aacute;xima
                                    <input type="date" id="txtAWARD_PERIOD_MAX_EXTENT_DATE" value="" title="El per&iacute;odo no puede extenderse despu&eacute;s de esta fecha. Este campo debe usarse para expresar la fecha m&aacute;xima disponible para la extensi&oacute;n o renovaci&oacute;n de este per&iacute;odo."/>
                                    <span class="form-error" data-form-error-for="txtAWARD_PERIOD_MAX_EXTENT_DATE" id="spanAWARD_PERIOD_MAX_EXTENT_DATE">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Duraci&oacute;n (d&iacute;as)
                                    <input type="number" id="txtAWARD_PERIOD_DURATION_IN_DATE" value="" min="0" placeholder="p. ej. 0" title="El nivel m&aacute;ximo de duraci&oacute;n de este per&iacute;odo en d&iacute;as." />
                                    <span class="form-error" data-form-error-for="txtAWARD_PERIOD_DURATION_IN_DATE" id="spanAWARD_PERIOD_DURATION_IN_DATE">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="fieldset cell">
                            <legend title="Informaci&oacute;n de enmienda">Enmienda principal</legend>
                            <div class="grid-margin-x grid-x">
                                <label class="cell medium-4">ID <span style="color: red">**</span>
                                    <input type="text" id="txtAWARD_AMENDMENT_ID" placeholder="p. ej. 1" value="" title="Un identificador para esta enmienda: com&uacute;nmente el n&uacute;mero de enmienda."/>
                                    <span class="form-error" data-form-error-for="txtAWARD_AMENDMENT_ID" id="spanAWARD_AWARD_AMENDMENT_ID">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Fecha de enmienda
                                    <input type="date" id="txtAWARD_AMENDMENT_DATE" value="" title="La fecha de esta enmienda." />
                                    <span class="form-error" data-form-error-for="txtAWARD_AMENDMENT_DATE" id="spanAWARD_AMENDMENT_DATE">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Justificaci&oacute;n
                                    <input type="text" id="txtAWARD_AMENDMENT_RATIONALE" placeholder="p. ej. Explicaci&oacute;n de la enmienda" value="" title="Una explicaci&oacute;n de la enmienda."/>
                                    <span class="form-error" data-form-error-for="txtAWARD_AMENDMENT_RATIONALE" id="spanAWARD_AMENDMENT_RATIONALE">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Descripci&oacute;n
                                    <input type="text" id="txtAWARD_AMENDMENT_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n de la enmienda" value="" title="Un texto libre o semi estructurado, describiendo los cambios hechos en esta enmienda."/>
                                    <span class="form-error" data-form-error-for="txtAWARD_AMENDMENT_DESCRIPTION" id="spanAWARD_AMENDMENT_DESCRIPTION">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Entrega enmendada (identificador)
                                    <input type="text" id="txtAWARD_AMENDMENT_AMENDS_RELEASE_ID" placeholder="p. ej. 1" value="" title="Provee el identificador (release.id) de la entrega OCDS (de este proceso de contrataci&oacute;n) que provee los valores para este proceso de contrataci&oacute;n &#42;&#42;antes&#42;&#42; de realizada la enmienda."/>
                                    <span class="form-error" data-form-error-for="txtAWARD_AMENDMENT_AMENDS_RELEASE_ID" id="spanAWARD_AMENDMENT_AMENDS_RELEASE_ID">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Entrega de enmienda (identificador)
                                    <input type="text" id="txtAWARD_AMENDMENT_RELEASE_ID" placeholder="p. ej. 1" value="" title="Provee el identificador (release.id) de la entrega OCDS (de este proceso de contrataci&oacute;n) que provee los valores para este proceso de contrataci&oacute;n &#42;&#42;despu&eacute;s&#42;&#42; de realizada la enmienda." />
                                    <span class="form-error" data-form-error-for="txtAWARD_AMENDMENT_RELEASE_ID" id="spanAWARD_AMENDMENT_RELEASE_ID">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-award"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-award"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de una adjudicacion (BOTTOM) --%>
<%-- formulario de un proveedor (TOP) --%>
<div id="modalFrmSupplier" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Proveedor</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmSupplier" id="frmSupplier" method="post" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-supplier" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-6">ID de Entidad <span style="color: red">**</span>
                            <input type="text" id="txtSUPPLIER_ID" required placeholder="p. ej. 1" value="" title="El ID utilizado para hacer referencia a esta parte involucrada desde otras secciones de la entrega. Este campo puede construirse con la siguiente estructura {identifier.scheme}-{identifier.id}(-{department-identifier})."/>
                            <span class="form-error" data-form-error-for="txtSUPPLIER_ID" id="spanSUPPLIER_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Nombre
                            <input type="text" id="txtSUPPLIER_NAME" placeholder="p. ej. Nombre com&uacute;n" value="" title="Un nombre com&uacute;n para esta organizaci&oacute;n u otro participante en el proceso de contrataciones. El objeto identificador da un espacio para un nombre legal formal, y esto podr&iacute;a repetir el valor o dar un nombre com&uacute;n por el cual se conoce a la organizaci&oacute;n o entidad. Este campo tambi&eacute;n pude incluir detalles del departamento o sub-unidad involucrada en este proceso de contrataciones."/>
                            <span class="form-error" data-form-error-for="txtSUPPLIER_NAME" id="spanNAME">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <fieldset class="cell fieldset">
                            <legend title="El identificador primario para esta organizaci&oacute;n o participante. Son preferibles los identificadores que denotan de forma &uacute;nica a una entidad legal. Consulta la [gu&iacute;a de identificadores de organizaci&oacute;n] para el esquema e identificador preferido.">Identificador principal</legend>
                            <div class="grid-margin-x grid-x">
                                <label class="cell medium-6">ID
                                    <input type="text" id="txtSUPPLIER_IDENTIFIER_ID" placeholder="p. ej. 1" value="" title="El identificador de la organizaci&oacute;n en el esquema seleccionado."/>
                                    <span class="form-error" data-form-error-for="txtSUPPLIER_IDENTIFIER_ID" id="spanSUPPLIER_IDENTIFIER_ID">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Esquema
                                    <input type="text" id="txtSUPPLIER_IDENTIFIER_SCHEME" placeholder="p. ej. Esquema" value="" title="Los identificadores de organizaci&oacute;n deben de obtenerse de una lista de identificadores de organizaciones existentes. El campo scheme se usa para indicar o registrar de d&oacute;nde se toma el identificador. Este valor deber&iacute;a tomarse del [Esquema de Identificadores de Organizaciones]."/>
                                    <span class="form-error" data-form-error-for="txtSUPPLIER_IDENTIFIER_SCHEME" id="spanSUPPLIER_IDENTIFIER_SCHEME">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Nombre Legal
                                    <input type="text" id="txtSUPPLIER_IDENTIFIER_LEGAL_NAME" placeholder="p. ej. Nombre Legal" value="" title="El nombre legalmente registrado de la organizaci&oacute;n." />
                                    <span class="form-error" data-form-error-for="txtSUPPLIER_IDENTIFIER_LEGAL_NAME" id="spanSUPPLIER_IDENTIFIER_LEGAL_NAME">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">URI
                                    <input type="text" id="txtSUPPLIER_IDENTIFIER_URI" placeholder="p. ej. www.url.com" value="" pattern="url" title="Una URI para identificar a la organizaci&oacute;n, como los prove&iacute;dos por [Open Corporates] o alg&uacute;n otro proveedor relevante de URIs. Este campo no debe ser utilizado para especificar el sitio web de la organizaci&oacute;n, el cual puede ser especificado en el campo URL del punto de contacto de la Organizaci&oacute;n."/>
                                    <span class="form-error" data-form-error-for="txtSUPPLIER_IDENTIFIER_URI" id="spanSUPPLIER_IDENTIFIER_URI">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="cell fieldset">
                            <legend title="Una direcci&oacute;n. Esta puede ser la direcci&oacute;n legalmente registrada de la organizaci&oacute;n o puede ser una direcci&oacute;n donde se reciba correspondencia para este proceso de contrataci&oacute;n particular.">Direcci&oacute;n</legend>
                            <div class="grid-margin-x grid-x">
                                <label class="cell medium-4">Direcci&oacute;n
                                    <input type="text" id="txtSUPPLIER_ADDRESS_STREET_ADDRESS" placeholder="p. ej. 1600 Amphitheatre Pkwy." value="" title="La direcci&oacute;n de la calle."/>
                                    <span class="form-error" data-form-error-for="txtSUPPLIER_ADDRESS_STREET_ADDRESS" id="spanSUPPLIER_ADDRESS_STREET_ADDRESS">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Localidad
                                    <input type="text" id="txtSUPPLIER_ADDRESS_LOCALITY" placeholder="p. ej. Mountain View." value="" title="La localidad." />
                                    <span class="form-error" data-form-error-for="txtSUPPLIER_ADDRESS_LOCALITY" id="spanSUPPLIER_ADDRESS_LOCALITY">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Regi&oacute;n
                                    <input type="text" id="txtSUPPLIER_ADDRESS_REGION" placeholder="p. ej. CA." value="" title="La regi&oacute;n."/>
                                    <span class="form-error" data-form-error-for="txtSUPPLIER_ADDRESS_REGION" id="spanSUPPLIER_ADDRESS_REGION">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">C&oacute;digo postal
                                    <input type="text" id="txtSUPPLIER_ADDRESS_POSTAL_CODE" placeholder="p. ej. 94043" value="" pattern="codigo_postal" title="El c&oacute;digo postal." />
                                    <span class="form-error" data-form-error-for="txtSUPPLIER_ADDRESS_POSTAL_CODE" id="spanSUPPLIER_ADDRESS_POSTAL_CODE">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Pa&iacute;s
                                    <input type="text" id="txtSUPPLIER_ADDRESS_COUNTRY_NAME" placeholder="p. ej. M&eacute;xico" value="" title="El nombre del pa&iacute;s." />
                                    <span class="form-error" data-form-error-for="txtSUPPLIER_ADDRESS_COUNTRY_NAME" id="spanSUPPLIER_ADDRESS_COUNTRY_NAME">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="cell fieldset">
                            <legend title="Detalles de contacto que pueden usarse para esta parte involucrada.">Punto de contacto</legend>
                            <div class="grid-margin-x grid-x">
                                <label class="cell medium-4">Nombre
                                    <input type="text" id="txtSUPPLIER_CONTACT_NAME" placeholder="p. ej. Carlos" value="" title="El nombre de la persona de contacto, departamento o punto de contacto en relaci&oacute;n a este proceso de contrataci&oacute;n."/>
                                    <span class="form-error" data-form-error-for="txtSUPPLIER_CONTACT_NAME" id="spanSUPPLIER_CONTACT_NAME">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Correo electr&oacute;nico
                                    <input type="text" id="txtSUPPLIER_CONTACT_EMAIL" placeholder="p. ej. email@gmail.com" value="" title="La direcci&oacute;n de correo del punto o persona de contacto."/>
                                    <span class="form-error" data-form-error-for="txtSUPPLIER_CONTACT_EMAIL" id="spanSUPPLIER_CONTACT_EMAIL">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Tel&eacute;fono
                                    <input type="text" id="txtSUPPLIER_CONTACT_TELEPHONE" placeholder="p. ej. 5555555555 o 555-555-5555" value="" title="El n&uacute;mero de tel&eacute;fono del punto o persona de contacto. Este debe de incluir el c&oacute;digo de marcaci&oacute;n internacional." />
                                    <span class="form-error" data-form-error-for="txtSUPPLIER_CONTACT_TELEPHONE" id="spanSUPPLIER_CONTACT_TELEPHONE">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">N&uacute;mero de fax
                                    <input type="text" id="txtSUPPLIER_CONTACT_FAXNUMBER" placeholder="p. ej. 5555555555 o 555-555-5555" value="" title="El n&uacute;mero de fax del punto o persona de contacto. Este debe de incluir el c&oacute;digo de marcaci&oacute;n internacional."/>
                                    <span class="form-error" data-form-error-for="txtSUPPLIER_CONTACT_FAXNUMBER" id="spanSUPPLIER_CONTACT_FAXNUMBER">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">URL
                                    <input type="text" id="txtSUPPLIER_CONTACT_URL" placeholder="p. ej. www.url.com" value="" pattern="url" title="Una direcci&oacute;n web para el punto o persona de contacto."/>
                                    <span class="form-error" data-form-error-for="txtSUPPLIER_CONTACT_URL" id="spanSUPPLIER_CONTACT_URL">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-supplier"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-supplier"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un proveedor (BOTTOM) --%>
<%-- formulario de un articulo (TOP) --%>
<div id="modalFrmItem" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Art&iacute;culo</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmItem" id="frmItem" method="post" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-item" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-6">ID
                            <input type="text" id="txtITEM_ID" required placeholder="p. ej. 1" value="" title="Un identificador local al cual hacer referencia y con el cual unir los art&iacute;culos. Debe de ser &uacute;nico en relaci&oacute;n a los dem&aacute;s &iacute;tems del mismo proceso de contrataci&oacute;n presentes en la matriz de art&iacute;culos."/>
                            <span class="form-error" data-form-error-for="txtITEM_ID" id="spanITEM_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Descripci&oacute;n
                            <input type="text" id="txtITEM_DESCRIPTION" placeholder="p. ej. Breve descripci&oacute;n de los bienes o servicios" value="" title="Una descripci&oacute;n de los bienes o servicios objetos del procedimiento de contrataci&oacute;n." />
                            <span class="form-error" data-form-error-for="txtITEM_DESCRIPTION" id="spanITEM_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <fieldset class="fieldset cell">
                            <legend title="La clasificaci&oacute;n primaria para un art&iacute;culo.">Clasificaci&oacute;n</legend>
                            <div class="grid-x grid-margin-x">
                                <label class="cell medium-6">ID
                                    <input type="text" id="txtITEM_CLASSIFICATION_ID" placeholder="p. ej. 1" value="" title="El c&oacute;digo de clasificaci&oacute;n que se obtiene del esquema." />
                                    <span class="form-error" data-form-error-for="txtITEM_CLASSIFICATION_ID" id="spanITEM_CLASSIFICATION_ID">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Esquema
                                    <select id="cmbITEM_CLASSIFICATION_SCHEME" title="El esquema o lista de c&oacute;digo del cual se obtiene el c&oacute;digo de clasificaci&oacute;n. Para clasificaciones de art&iacute;culos de linea.">
                                        <option value="">--Elige una opci&oacute;n--</option>
                                        <c:forEach items="${esquemasDeClasificacion}" var="esquema">
                                            <option value="${esquema.clave}">${esquema.valor}</option>
                                        </c:forEach>
                                    </select>
                                    <span class="form-error" data-form-error-for="cmbITEM_CLASSIFICATION_SCHEME" id="spanITEM_CLASSIFICATION_SCHEME">
                                        Elige una opci&oacute;n.
                                    </span>
                                </label>
                                <label class="cell medium-6">Descripci&oacute;n
                                    <input type="text" id="txtITEM_CLASSIFICATION_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n de la clasificaci&oacute;n" value="" title="Una descripci&oacute;n textual o t&iacute;tulo del c&oacute;digo de clasificaci&oacute;n." />
                                    <span class="form-error" data-form-error-for="txtITEM_CLASSIFICATION_DESCRIPTION" id="spanITEM_CLASSIFICATION_DESCRIPTION">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">URI
                                    <input type="text" id="txtITEM_CLASSIFICATION_URI" placeholder="p. ej. www.url.com" pattern="url" value="" title="Un URI para identificar &uacute;nicamente el c&oacute;digo de clasificaci&oacute;n."/>
                                    <span class="form-error" data-form-error-for="txtITEM_CLASSIFICATION_URI" id="spanITEM_CLASSIFICATION_URI">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                            </div>
                        </fieldset>
                        <label class="cell">Cantidad
                            <input type="number" id="txtITEM_QUANTITY" placeholder="p. ej. 1" min="0" value="" title="El n&uacute;mero de unidades que se dan."/>
                            <span class="form-error" data-form-error-for="txtITEM_QUANTITY" id="spanITEM_QUANTITY">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <fieldset class="fieldset cell">
                            <legend title="Una descripci&oacute;n de la unidad en la cual los suministros, servicios o trabajos est&aacute;n provistos (ej. horas, kilos) y el precio por unidad.">Unidad</legend>
                            <div class="grid-x grid-margin-x">
                                <label class="cell medium-6">ID
                                    <input type="text" id="txtITEM_UNIT_ID" placeholder="p. ej. 1" value="" title="El identificador de la lista de c&oacute;digos a la que se hace referencia en la propiedad de esquema."/>
                                    <span class="form-error" data-form-error-for="txtITEM_UNIT_ID" id="spanITEM_UNIT_ID">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Esquema
                                    <select id="cmbITEM_UNIT_SCHEME" title="La lista de la cual se obtienen los identificadores de unidades de medida. Se recomienda &#39;UNCEFACT&#39;.">
                                        <option value="">--Elige una opci&oacute;n--</option>
                                        <c:forEach items="${esquemasDeUnidad}" var="esquema">
                                            <option value="${esquema.clave}" >${esquema.valor}</option>
                                        </c:forEach>
                                    </select>
                                    <span class="form-error" data-form-error-for="cmbITEM_UNIT_SCHEME" id="spanITEM_UNIT_SCHEME">
                                        Elige una opci&oacute;n.
                                    </span>
                                </label>
                                <label class="cell medium-6">Nombre
                                    <input type="text" id="txtITEM_UNIT_NAME" placeholder="p. ej. Nombre de la unidad" value="" title="Nombre de la unidad." />
                                    <span class="form-error" data-form-error-for="txtITEM_UNIT_NAME" id="spanITEM_UNIT_NAME">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">URI
                                    <input type="text" id="txtITEM_UNIT_URI" placeholder="p. ej. www.url.com" pattern="url" value="" title="Un URI le&iacute;ble por m&aacute;quina para la unidad de medida, dada por el esquema."/>
                                    <span class="form-error" data-form-error-for="txtITEM_UNIT_URI" id="spanITEM_UNIT_URI">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <fieldset class="fieldset cell">
                                    <legend title="El valor monetario de una unidad.">Valor</legend>
                                    <div class="grid-margin-x grid-x">
                                        <label class="cell medium-6">Monto
                                            <input type="number" id="txtITEM_UNIT_AMOUNT" placeholder="p. ej. 0" min="0" value="" title="Monto como una cifra."/>
                                            <span class="form-error" data-form-error-for="txtITEM_UNIT_AMOUNT" id="spanITEM_UNIT_AMOUNT">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-6">Moneda
                                            <select id="cmbITEM_UNIT_CURRENCY" title="La moneda del monto.">
                                                <option value="">--Elige una opci&oacute;n--</option>
                                                <c:forEach items="${tipoMoneda}" var="moneda">
                                                    <option value="${moneda.clave}">${moneda.valor}</option>
                                                </c:forEach>
                                            </select>
                                            <span class="form-error" data-form-error-for="cmbITEM_UNIT_CURRENCY" id="spanITEM_UNIT_CURRENCY">
                                                Elige una opci&oacute;n.
                                            </span>
                                        </label>
                                    </div>
                                </fieldset>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-item"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-item"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un articulo (BOTTOM) --%>
<%-- formulario de una clasificacion (TOP) --%>
<div id="modalFrmClassification" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Clasificaci&oacute;n</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmClassification" id="frmClassification" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-classification" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-6">ID
                            <input type="text" id="txtCLASSIFICATION_ID" required placeholder="p. ej. 1" value="" title="El c&oacute;digo de clasificaci&oacute;n que se obtiene del esquema."/>
                            <span class="form-error" data-form-error-for="txtCLASSIFICATION_ID" id="spanCLASSIFICATION_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Esquema
                            <select id="cmbCLASSIFICATION_SCHEME" title="El esquema o lista de c&oacute;digo del cual se obtiene el c&oacute;digo de clasificaci&oacute;n. Para clasificaciones de art&iacute;culos de linea.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${esquemasDeClasificacion}" var="esquema">
                                    <option value="${esquema.clave}">${esquema.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbCLASSIFICATION_SCHEME" id="spanCLASSIFICATION_SCHEME">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                        <label class="cell medium-6">Descripci&oacute;n
                            <input type="text" id="txtCLASSIFICATION_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n de la clasificaci&oacute;n" value="" title="Una descripci&oacute;n textual o t&iacute;tulo del c&oacute;digo de clasificaci&oacute;n." />
                            <span class="form-error" data-form-error-for="txtCLASSIFICATION_DESCRIPTION" id="spanCLASSIFICATION_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">URI
                            <input type="text" id="txtCLASSIFICATION_URI" placeholder="p. ej. www.url.com" pattern="url" value="" title="Un URI para identificar &uacute;nicamente el c&oacute;digo de clasificaci&oacute;n."/>
                            <span class="form-error" data-form-error-for="txtCLASSIFICATION_URI" id="spanCLASSIFICATION_URI">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-classification"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-classification"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de una clasificacion (BOTTOM) --%>
<%-- formulario de un documento (TOP) --%>
<div id="modalFrmDocument" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Documento</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmDocument" id="frmDocument" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-document" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-4">ID <span style="color: red">**</span>
                            <input type="text" id="txtDOCUMENT_ID" required placeholder="p. ej. 1" value="" title="Identificador local y &uacute;nico para este documento. Este campo se utiliza para darle seguimiento a las m&uacute;ltiples versiones de un documento en el proceso de creaci&oacute;n de un registro del proceso de contrataci&oacute;n (record) que se genera a partir de las entregas (release)."/>
                            <span class="form-error" data-form-error-for="txtDOCUMENT_ID" id="spanDOCUMENT_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Tipo de Documento
                            <select id="cmbDOCUMENT_TYPE" title="Una clasificaci&oacute;n del documento descrito.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${tiposDeDocumento}" var="tipoDocumento">
                                    <option value="${tipoDocumento.clave}" ${documento.documentType == tipoDocumento.clave ? 'selected': ''} >${tipoDocumento.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbDOCUMENT_TYPE" id="spanDOCUMENT_TYPE">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                        <label class="cell medium-4">T&iacute;tulo
                            <input type="text" id="txtDOCUMENT_TITLE" placeholder="p. ej. T&iacute;tulo del documento" value="" title="El t&iacute;tulo del documento."/>
                            <span class="form-error" data-form-error-for="txtDOCUMENT_TITLE" id="spanDOCUMENT_TITLE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Descripci&oacute;n
                            <input type="text" id="txtDOCUMENT_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n del documento" value="" title="Una descripci&oacute;n corta del documento. Se recomienda que las descripciones no excedan 250 palabras. En el caso en que el documento no est&eacute; accesible en l&iacute;nea, el campo de descripci&oacute;n puede utilizarse para describir los arreglos necesarios para obtener una copia del documento."/>
                            <span class="form-error" data-form-error-for="txtDOCUMENT_DESCRIPTION" id="spanDOCUMENT_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">URL
                            <input type="text" id="txtDOCUMENT_URL" placeholder="p. ej. www.url.com" value="" pattern="url" title="Un enlace directo al documento o archivo adjunto. El servidor que da acceso a este documento debe de estar configurado para reportar correctamente el tipo MIME de documento."/>
                            <span class="form-error" data-form-error-for="txtDOCUMENT_URL" id="spanDOCUMENT_URL">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de publicaci&oacute;n
                            <input type="date" id="txtDOCUMENT_DATE_PUBLISHED" value="" pattern="fecha" title="La fecha de publicaci&oacute;n del documento. Esto es particularmente importante para documentos relevantes desde el punto de vista legal, como los avisos de licitaci&oacute;n."/>
                            <span class="form-error" data-form-error-for="txtDOCUMENT_DATE_PUBLISHED" id="spanDOCUMENT_DATE_PUBLISHED">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de modificaci&oacute;n
                            <input type="date" id="txtDOCUMENT_DATE_MODIFIED" value="" pattern="fecha" title="Fecha en que se modific&oacute; por &uacute;ltima vez el documento."/>
                            <span class="form-error" data-form-error-for="txtDOCUMENT_DATE_MODIFIED" id="spanDOCUMENT_DATE_MODIFIED">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Formato
                            <input type="text" id="txtDOCUMENT_FORMAT" placeholder="p. ej. text/html" value="" title="El formato del documento, utilizando la lista de c&oacute;digos abierta [IANA Media Types] (vea los valores en la columna &#39;Template&#39;), o utilizando el c&oacute;digo &#39;offline/print&#39;  si el documento descrito se publica offline."/>
                            <span class="form-error" data-form-error-for="txtDOCUMENT_FORMAT" id="spanDOCUMENT_FORMAT">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Idioma
                            <select id="cmbDOCUMENT_LANGUAGE" title="El idioma predeterminado de los datos con dos letras [ISO639-1].">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${tiposDeIdioma}" var="idioma">
                                    <option value="${idioma.codigo}" >${idioma.nombre}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbDOCUMENT_LANGUAGE" id="spanDOCUMENT_LANGUAGE">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-document"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-document"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un documento (BOTTOM) --%>    
<%-- formulario de una enmienda (TOP) --%>
<div id="modalFrmAmendment" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Enmienda</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmAmendment" id="frmAmendment" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-amendment" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-4">ID <span style="color: red">**</span>
                            <input type="text" id="txtAMENDMENT_ID" placeholder="p. ej. 1" value="" title="Un identificador para esta enmienda: com&uacute;nmente el n&uacute;mero de enmienda."/>
                            <span class="form-error" data-form-error-for="txtAMENDMENT_ID" id="spanAMENDMENT_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de enmienda
                            <input type="date" id="txtAMENDMENT_DATE" value="" title="La fecha de esta enmienda." />
                            <span class="form-error" data-form-error-for="txtAMENDMENT_DATE" id="spanAMENDMENT_DATE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Justificaci&oacute;n
                            <input type="text" id="txtAMENDMENT_RATIONALE" placeholder="p. ej. Explicaci&oacute;n de la enmienda" value="" title="Una explicaci&oacute;n de la enmienda."/>
                            <span class="form-error" data-form-error-for="txtAMENDMENT_RATIONALE" id="spanAMENDMENT_RATIONALE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Descripci&oacute;n
                            <input type="text" id="txtAMENDMENT_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n de la enmienda" value="" title="Un texto libre o semi estructurado, describiendo los cambios hechos en esta enmienda."/>
                            <span class="form-error" data-form-error-for="txtAMENDMENT_DESCRIPTION" id="spanAMENDMENT_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Entrega enmendada (identificador)
                            <input type="text" id="txtAMENDMENT_AMENDS_RELEASE_ID" placeholder="p. ej. 1" value="" title="Provee el identificador (release.id) de la entrega OCDS (de este proceso de contrataci&oacute;n) que provee los valores para este proceso de contrataci&oacute;n &#42;&#42;antes&#42;&#42; de realizada la enmienda."/>
                            <span class="form-error" data-form-error-for="txtAMENDMENT_AMENDS_RELEASE_ID" id="spanAMENDMENT_AMENDS_RELEASE_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Entrega de enmienda (identificador)
                            <input type="text" id="txtAMENDMENT_RELEASE_ID" placeholder="p. ej. 1" value="" title="Provee el identificador (release.id) de la entrega OCDS (de este proceso de contrataci&oacute;n) que provee los valores para este proceso de contrataci&oacute;n &#42;&#42;despu&eacute;s&#42;&#42; de realizada la enmienda." />
                            <span class="form-error" data-form-error-for="txtAMENDMENT_RELEASE_ID" id="spanAMENDMENT_RELEASE_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-amendment"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-amendment"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de una enmienda (BOTTOM) --%>
<%-- formulario de un cambio en campos enmendados (TOP) --%>
<div id="modalFrmChange" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Campos enmendados</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmChange" id="frmChange" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-change" value="">
                    <div class="grid-margin-x grid-x">
                        <label class='cell medium-6'>Propiedad <span style='color: red'>**</span>
                            <input type='text' id='txtCHANGE_PROPERTY' placeholder='p. ej. Propiedad' title='El nombre de propiedad que ha sido cambiado con relaci&oacute;n al lugar donde se encuentra la enmienda. Por ejemplo si el valor del contrato ha cambiado.'/>
                            <span class='form-error' data-form-error-for='txtCHANGE_PROPERTY' id='spanCHANGE_PROPERTY'>
                                 Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class='cell medium-6'>Valor anterior
                            <input type='text' id='txtCHANGE_FORMATER_VALUE' placeholder='p. ej. Valor previo' title='El valor previo de la propiedad cambiada en cualquier tipo de propiedad.'/>
                            <span class='form-error' data-form-error-for='txtCHANGE_FORMATER_VALUE' id='spanCHANGE_FORMATER_VALUE'>
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-change"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-change"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un cambio en campos enmendados (BOTTOM) --%>
<%-- formulario de un cambio en campos enmendados de la enmienda principal (TOP) --%>
<div id="modalFrmAmendmentPrincipalChange" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Campos enmendados</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmAmendmentPrincipalChange" id="frmAmendmentPrincipalChange" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-amendment-principal-change" value="">
                    <div class="grid-margin-x grid-x">
                        <label class='cell medium-6'>Propiedad <span style='color: red'>**</span>
                            <input type='text' id='txtAMENDMENT_PRINCIPAL_CHANGE_PROPERTY' placeholder='p. ej. Propiedad' title='El nombre de propiedad que ha sido cambiado con relaci&oacute;n al lugar donde se encuentra la enmienda. Por ejemplo si el valor del contrato ha cambiado.'/>
                            <span class='form-error' data-form-error-for='txtAMENDMENT_PRINCIPAL_CHANGE_PROPERTY' id='spanAMENDMENT_PRINCIPAL_CHANGE_PROPERTY'>
                                 Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class='cell medium-6'>Valor anterior
                            <input type='text' id='txtAMENDMENT_PRINCIPAL_CHANGE_FORMATER_VALUE' placeholder='p. ej. Valor previo' title='El valor previo de la propiedad cambiada en cualquier tipo de propiedad.'/>
                            <span class='form-error' data-form-error-for='txtAMENDMENT_PRINCIPAL_CHANGE_FORMATER_VALUE' id='spanAMENDMENT_PRINCIPAL_CHANGE_FORMATER_VALUE'>
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-amendment-principal-change"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-amendment-principal-change"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un cambio en campos enmendados de la enmienda principal (BOTTOM) --%>
<%-- formulario de un identificador (TOP) --%>
<div id="modalFrmIdentifier" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Identificador</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmIdentifier" id="frmIdentifier" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-identifier" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-6">ID <span style="color: red">**</span>
                            <input type="text" required id="txtIDENTIFIER_ID" placeholder="p. ej. 1" value="" title="El identificador de la organizaci&oacute;n en el esquema seleccionado."/>
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_ID" id="spanIDENTIFIER_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Esquema
                            <input type="text" id="txtIDENTIFIER_SCHEME" placeholder="p. ej. Esquema" value="" title="Los identificadores de organizaci&oacute;n deben de obtenerse de una lista de identificadores de organizaciones existentes. El campo scheme se usa para indicar o registrar de d&oacute;nde se toma el identificador. Este valor deber&iacute;a tomarse del [Esquema de Identificadores de Organizaciones]."/>
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_SCHEME" id="spanIDENTIFIER_SCHEME">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Nombre Legal
                            <input type="text" id="txtIDENTIFIER_LEGAL_NAME" placeholder="p. ej. Nombre Legal" value="" title="El nombre legalmente registrado de la organizaci&oacute;n." />
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_LEGAL_NAME" id="spanIDENTIFIER_LEGAL_NAME">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">URI
                            <input type="text" id="txtIDENTIFIER_URI" placeholder="p. ej. www.url.com" value="" pattern="url" title="Una URI para identificar a la organizaci&oacute;n, como los prove&iacute;dos por [Open Corporates] o alg&uacute;n otro proveedor relevante de URIs. Este campo no debe ser utilizado para especificar el sitio web de la organizaci&oacute;n, el cual puede ser especificado en el campo URL del punto de contacto de la Organizaci&oacute;n." />
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_URI" id="spanIDENTIFIER_URI">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-identifier"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-identifier"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un identificador - payee (BOTTOM) --%>

<script>
                
    function validarFormulario(idFormulario)
    { 
        
        let strMensajeFrm;          
        let strTituloMensaje;       
        let bolTipoFormulario;      

        bolTipoFormulario = document.getElementById("txtEsFormularioDeEdicion").value;
        bolTipoFormulario = Boolean(bolTipoFormulario);
        strMensajeFrm = (bolTipoFormulario === true) ? "\u00bf Los datos son correctos\u003F" : "Una vez guardada la informaci\u00F3n de esta secci\u00F3n no se podr\u00E1 volver a modificar los valores ingresados hasta que se finalice con el proceso de registro de la contrataci\u00F3n p\u00FAblica.";
        strTituloMensaje = (bolTipoFormulario === true) ? "Confirmar" : "\u00bfLos datos son correctos\u003F";

        swal({
            title: strTituloMensaje,
            text: strMensajeFrm,
            icon: "warning",
            buttons: ["Cancelar", "Aceptar"],
            dangerMode: true,
            closeOnClickOutside: false,
            closeOnEsc: false
        })
        .then((seAcepta) => {
            if (seAcepta)
            { 
                document.getElementById(idFormulario).submit();
            } 
            else
            { 
                swal("Se ha cancelado la operaci\u00f3n", {
                    icon: "error",
                    buttons: false,
                    timer: 2000
                });
                return false;
            } 
        });
                    
    } 
    
    window.onload = function (){
        
        document.getElementById("estructura-por-defecto-award").value = obtenerDatosInicialesElementos("award", []);
        
    };
    
</script>   