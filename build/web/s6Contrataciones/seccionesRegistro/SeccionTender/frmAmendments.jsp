<%-- 
    Autor: Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>
<div id="seccion7-bloque10">
    <h4>Enmiendas</h4>
    <div class="button-group">
        <button type="button" class="button" onclick="abrirNuevoFormulario('amendment', []);">Agregar enmienda</button>
    </div>
    <div class="callout padding-horizontal-3">
        <form name="frmRegistrarSeccionLicitacionEnmiendas" id="frmRegistrarSeccionLicitacionEnmiendas" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post" data-abide novalidate onSubmit="return false;" autocomplete="off">
            <input type="hidden" name="accion" value="${(bolEsFormulariosDeEdicion == true) ? 'editarSeccion' : 'registrarSeccion'}"/>
            <input type="hidden" name="txtEsFormularioDeEdicion" id="txtEsFormularioDeEdicion" value="${bolEsFormulariosDeEdicion}"/>
            <input type="hidden" name="txtNUMERO_SECCION" value="7"/>
            <input type="hidden" name="txtNUMERO_BLOQUE" value="10"/>
            <input type="hidden" name="txtID_CONTRATACION_SPDN" value="${idContratacion}"/>
            <input type="hidden" id="estructura-por-defecto-amendment" value=""> 
            <input type="hidden" id="siguiente-identificador-amendment" value="" data-valor-por-defecto=""> 
            <input type="hidden" id="lista-identificadores-por-registrar-amendment" name="lista-identificadores-por-registrar-amendment" value="" data-valor-por-defecto="">
            <c:choose>
                <c:when test="${contratacion.tender.amendments.size() > 0}">
                    <c:set var="contadorElementosAmendment" value="0"></c:set>
                    <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-amendment">
                    <c:forEach items="${contratacion.tender.amendments}" var="amendment">
                        <li class="accordion-item" data-accordion-item id="amendment_${contadorElementosAmendment}">
                            <a href="#" class="accordion-title"><span style="text-transform: capitalize;">enmienda</span> - <span id="titulo-elemento-amendment_${contadorElementosAmendment}">${amendment.id}</span></a>
                            <div class="accordion-content" data-tab-content>
                                <div class="grid-margin-x grid-x">
                                    <div class="cell padding-horizontal-1" style="display:flex; justify-content: flex-end; gap: 10px; flex-wrap: wrap;">
                                        <button class="button margin-0 btnFrmContrataciones" onclick="abrirFormularioPrevio('amendment', [${contadorElementosAmendment}])"><i class="material-icons">edit</i> Editar enmienda</button>
                                        <button class="button margin-0 btnFrmContrataciones" onclick="eliminarElemento('amendment', 'amendment_${contadorElementosAmendment}', [], '${contadorElementosAmendment}')"><i class="material-icons">remove_circle</i> Eliminar enmienda</button>
                                    </div>
                                    <div id="datos-elemento-amendment_${contadorElementosAmendment}">
                                        <input type="hidden" name="txtAMENDMENT_ID_${contadorElementosAmendment}" id="txtAMENDMENT_ID_${contadorElementosAmendment}" value="${amendment.id}">
                                        <input type="hidden" name="txtAMENDMENT_DATE_${contadorElementosAmendment}" id="txtAMENDMENT_DATE_${contadorElementosAmendment}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(amendment.date)}">
                                        <input type="hidden" name="txtAMENDMENT_RATIONALE_${contadorElementosAmendment}" id="txtAMENDMENT_RATIONALE_${contadorElementosAmendment}" value="${amendment.rationale}">
                                        <input type="hidden" name="txtAMENDMENT_DESCRIPTION_${contadorElementosAmendment}" id="txtAMENDMENT_DESCRIPTION_${contadorElementosAmendment}" value="${amendment.description}">
                                        <input type="hidden" name="txtAMENDMENT_AMENDS_RELEASE_ID_${contadorElementosAmendment}" id="txtAMENDMENT_AMENDS_RELEASE_ID_${contadorElementosAmendment}" value="${amendment.amendsReleaseID}">
                                        <input type="hidden" name="txtAMENDMENT_RELEASE_ID_${contadorElementosAmendment}" id="txtAMENDMENT_RELEASE_ID_${contadorElementosAmendment}" value="${amendment.releaseID}">
                                    </div>
                                    <fieldset class="cell fieldset"> 
                                        <input type="hidden" id="estructura-por-defecto-change_${contadorElementosAmendment}" value=""> 
                                        <input type="hidden" id="siguiente-identificador-change_${contadorElementosAmendment}" value="" data-valor-por-defecto=""> 
                                        <input type="hidden" id="lista-identificadores-por-registrar-change_${contadorElementosAmendment}" name="lista-identificadores-por-registrar-change_${contadorElementosAmendment}" value="" data-valor-por-defecto="">
                                        <legend title="Una lista de cambios que describen los campos que cambiaron y sus valores previos.">Campos enmendados</legend>
                                        <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                            <a onclick="restaurarEstadoInicialElemento('change', [${contadorElementosAmendment}])"><i class="material-icons secondary">restart_alt</i></a> 
                                            <a onclick="abrirNuevoFormulario('change', [${contadorElementosAmendment}])"><i class="material-icons secondary">add_circle</i></a> 
                                        </div>
                                        <c:choose>
                                            <c:when test="${amendment.changes.size() > 0}">
                                                <c:set var="contadorElementosAmendmentChange" value="0"></c:set>
                                                <div id="contenedor-change_${contadorElementosAmendment}" style="display: flex; flex-direction: column; gap: 10px;">
                                                <c:forEach items="${amendment.changes}" var="amendmentChange">
                                                    <div id="change_${contadorElementosAmendment}_${contadorElementosAmendmentChange}">
                                                        <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                                            <div class="cell medium-8">
                                                                <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">cambio</span> - <span id="titulo-elemento-change_${contadorElementosAmendment}_${contadorElementosAmendmentChange}">${amendmentChange.property}</span></h6>
                                                            </div>
                                                            <div id="datos-elemento-change_${contadorElementosAmendment}_${contadorElementosAmendmentChange}">
                                                                <input type="hidden" name="txtCHANGE_PROPERTY_${contadorElementosAmendment}_${contadorElementosAmendmentChange}" id="txtCHANGE_PROPERTY_${contadorElementosAmendment}_${contadorElementosAmendmentChange}" value="${amendmentChange.property}">
                                                                <input type="hidden" name="txtCHANGE_FORMATER_VALUE_${contadorElementosAmendment}_${contadorElementosAmendmentChange}" id="txtCHANGE_FORMATER_VALUE_${contadorElementosAmendment}_${contadorElementosAmendmentChange}" value="${amendmentChange.former_value}">
                                                            </div>
                                                            <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                                <button class="button margin-0" onclick="abrirFormularioPrevio('change', [${contadorElementosAmendment}, ${contadorElementosAmendmentChange}])"><i class="material-icons">edit</i> Editar</button>
                                                                <button class="button margin-0" onclick="eliminarElemento('change', 'change_${contadorElementosAmendment}_${contadorElementosAmendmentChange}', [${contadorElementosAmendment}], '${contadorElementosAmendmentChange}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <c:set var="contadorElementosAmendmentChange" value="${contadorElementosAmendmentChange + 1}"></c:set>
                                                </c:forEach>
                                                </div>
                                                <input type="hidden" name="ultimo-identificador-elementos-change_${contadorElementosAmendment}" id="ultimo-identificador-elementos-change_${contadorElementosAmendment}" value="${contadorElementosAmendmentChange}">
                                            </c:when>
                                            <c:otherwise>
                                                <div id="contenedor-change_${contadorElementosAmendment}" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                                <input type="hidden" name="ultimo-identificador-elementos-change_${contadorElementosAmendment}" id="ultimo-identificador-elementos-change_${contadorElementosAmendment}" value="0">
                                            </c:otherwise>
                                        </c:choose>
                                        <script>
                                            document.getElementById("estructura-por-defecto-change_${contadorElementosAmendment}").value = obtenerDatosInicialesElementos("change", [${contadorElementosAmendment}]);
                                        </script>
                                    </fieldset>
                                </div>
                            </div>
                        </li>
                        <c:set var="contadorElementosAmendment" value="${contadorElementosAmendment + 1}"></c:set>
                    </c:forEach>
                    </ul>
                    <input type="hidden" name="ultimo-identificador-elementos-amendment" id="ultimo-identificador-elementos-amendment" value="${contadorElementosAmendment}">
                </c:when>
                <c:otherwise>
                    <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-amendment"></ul>
                    <input type="hidden" name="ultimo-identificador-elementos-amendment" id="ultimo-identificador-elementos-amendment" value="0">
                </c:otherwise>
            </c:choose>
            <div class="grid-x grid-margin-x margin-top-1">
                <fieldset class="cell medium-6">
                    <input type="button" class="button expanded secondary" onclick="restaurarEstadoInicialElemento('amendment', []);" value="${(bolEsFormulariosDeEdicion == true) ? 'Restaurar' : 'Reiniciar'}"/>
                </fieldset>
                <fieldset class="cell medium-6">
                    <input type="button" onclick="validarFormulario('frmRegistrarSeccionLicitacionEnmiendas');" class="button expanded" id="btnRegistrarValoresFormulario" value="${(bolEsFormulariosDeEdicion == true) ? 'Modificar datos' : 'Continuar'}"/>
                </fieldset>
            </div>
        </form>
    </div>
</div>
            
<%-- formulario de una enmienda (TOP) --%>
<div id="modalFrmAmendment" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Enmienda</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmAmendment" id="frmAmendment" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-amendment" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-4">ID <span style="color: red">**</span>
                            <input type="text" required id="txtAMENDMENT_ID" placeholder="p. ej. 1" value="" title="Un identificador para esta enmienda: com&uacute;nmente el n&uacute;mero de enmienda."/>
                            <span class="form-error" data-form-error-for="txtAMENDMENT_ID" id="spanAMENDMENT_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de enmienda
                            <input type="date" id="txtAMENDMENT_DATE" value="" title="La fecha de esta enmienda." />
                            <span class="form-error" data-form-error-for="txtAMENDMENT_DATE" id="spanAMENDMENT_DATE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Justificaci&oacute;n
                            <input type="text" id="txtAMENDMENT_RATIONALE" placeholder="p. ej. Explicaci&oacute;n de la enmienda" value="" title="Una explicaci&oacute;n de la enmienda."/>
                            <span class="form-error" data-form-error-for="txtAMENDMENT_RATIONALE" id="spanAMENDMENT_RATIONALE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Descripci&oacute;n
                            <input type="text" id="txtAMENDMENT_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n de la enmienda" value="" title="Un texto libre o semi estructurado, describiendo los cambios hechos en esta enmienda."/>
                            <span class="form-error" data-form-error-for="txtAMENDMENT_DESCRIPTION" id="spanAMENDMENT_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Entrega enmendada (identificador)
                            <input type="text" id="txtAMENDMENT_AMENDS_RELEASE_ID" placeholder="p. ej. 1" value="" title="Provee el identificador (release.id) de la entrega OCDS (de este proceso de contrataci&oacute;n) que provee los valores para este proceso de contrataci&oacute;n &#42;&#42;antes&#42;&#42; de realizada la enmienda."/>
                            <span class="form-error" data-form-error-for="txtAMENDMENT_AMENDS_RELEASE_ID" id="spanAMENDMENT_AMENDS_RELEASE_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Entrega de enmienda (identificador)
                            <input type="text" id="txtAMENDMENT_RELEASE_ID" placeholder="p. ej. 1" value="" title="Provee el identificador (release.id) de la entrega OCDS (de este proceso de contrataci&oacute;n) que provee los valores para este proceso de contrataci&oacute;n &#42;&#42;despu&eacute;s&#42;&#42; de realizada la enmienda." />
                            <span class="form-error" data-form-error-for="txtAMENDMENT_RELEASE_ID" id="spanAMENDMENT_RELEASE_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-amendment"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-amendment"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de una enmienda (BOTTOM) --%>
<%-- formulario de un cambio en campos enmendados (TOP) --%>
<div id="modalFrmChange" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Campos enmendados</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmChange" id="frmChange" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-change" value="">
                    <div class="grid-margin-x grid-x">
                        <label class='cell medium-6'>Propiedad <span style='color: red'>**</span>
                            <input type='text' required id='txtCHANGE_PROPERTY' placeholder='p. ej. Propiedad' title='El nombre de propiedad que ha sido cambiado con relaci&oacute;n al lugar donde se encuentra la enmienda. Por ejemplo si el valor del contrato ha cambiado.'/>
                            <span class='form-error' data-form-error-for='txtCHANGE_PROPERTY' id='spanCHANGE_PROPERTY'>
                                 Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class='cell medium-6'>Valor anterior
                            <input type='text' id='txtCHANGE_FORMATER_VALUE' placeholder='p. ej. Valor previo' title='El valor previo de la propiedad cambiada en cualquier tipo de propiedad.'/>
                            <span class='form-error' data-form-error-for='txtCHANGE_FORMATER_VALUE' id='spanCHANGE_FORMATER_VALUE'>
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-change"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-change"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un cambio en campos enmendados (BOTTOM) --%>

<script>
                
    function validarFormulario(idFormulario)
    { 
        
        let strMensajeFrm;          
        let strTituloMensaje;       
        let bolTipoFormulario;      

        bolTipoFormulario = document.getElementById("txtEsFormularioDeEdicion").value;
        bolTipoFormulario = Boolean(bolTipoFormulario);
        strMensajeFrm = (bolTipoFormulario === true) ? "\u00bfLos datos son correctos\u003F" : "Una vez guardada la informaci\u00F3n de esta secci\u00F3n no se podr\u00E1 volver a modificar los valores ingresados hasta que se finalice con el proceso de registro de la contrataci\u00F3n p\u00FAblica.";
        strTituloMensaje = (bolTipoFormulario === true) ? "Confirmar" : "\u00bfLos datos son correctos\u003F";

        swal({
            title: strTituloMensaje,
            text: strMensajeFrm,
            icon: "warning",
            buttons: ["Cancelar", "Aceptar"],
            dangerMode: true,
            closeOnClickOutside: false,
            closeOnEsc: false
        })
        .then((seAcepta) => {
            if (seAcepta)
            { 
                document.getElementById(idFormulario).submit();
            } 
            else
            { 
                swal("Se ha cancelado la operaci\u00f3n", {
                    icon: "error",
                    buttons: false,
                    timer: 2000
                });
                return false;
            } 
        });
                    
    } 
    
    window.onload = function (){
        
        document.getElementById("estructura-por-defecto-amendment").value = obtenerDatosInicialesElementos("amendment", []);
        
    };
    
</script> 
