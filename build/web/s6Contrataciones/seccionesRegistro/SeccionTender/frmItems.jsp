<%-- 
    Autor: Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>
<div id="seccion7-bloque6">
    <h4>Art&iacute;culos que se adquirir&aacute;n</h4>
    <div class="button-group">
        <button type="button" class="button" onclick="abrirNuevoFormulario('item', []);">Agregar art&iacute;culo</button>
    </div>
    <div class="callout padding-horizontal-3">
        <form name="frmRegistrarSeccionLicitacionItems" id="frmRegistrarSeccionLicitacionItems" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post" data-abide novalidate onSubmit="return false;" autocomplete="off">
            <input type="hidden" name="accion" value="${(bolEsFormulariosDeEdicion == true) ? 'editarSeccion' : 'registrarSeccion'}"/>
            <input type="hidden" name="txtEsFormularioDeEdicion" id="txtEsFormularioDeEdicion" value="${bolEsFormulariosDeEdicion}"/>
            <input type="hidden" name="txtNUMERO_SECCION" value="7"/>
            <input type="hidden" name="txtNUMERO_BLOQUE" value="6"/>
            <input type="hidden" name="txtID_CONTRATACION_SPDN" value="${idContratacion}"/>
            <input type="hidden" id="estructura-por-defecto-item" value=""> 
            <input type="hidden" id="siguiente-identificador-item" value="" data-valor-por-defecto=""> 
            <input type="hidden" id="lista-identificadores-por-registrar-item" name="lista-identificadores-por-registrar-item" value="" data-valor-por-defecto="">
            <c:choose>
                <c:when test="${contratacion.tender.items.size() > 0}">
                    <c:set var="contadorElementosItem" value="0"></c:set>
                    <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-item">
                    <c:forEach items="${contratacion.tender.items}" var="item">
                        <li class="accordion-item" data-accordion-item id="item_${contadorElementosItem}">
                            <a href="#" class="accordion-title"><span style="text-transform: capitalize;">art&iacute;culo</span> - <span id="titulo-elemento-item_${contadorElementosItem}">${item.id}</span></a>
                            <div class="accordion-content" data-tab-content>
                                <div class="grid-margin-x grid-x">
                                    <div class="cell padding-horizontal-1" style="display:flex; justify-content: flex-end; gap: 10px; flex-wrap: wrap;">
                                        <button class="button margin-0 btnFrmContrataciones" onclick="abrirFormularioPrevio('item', [${contadorElementosItem}])"><i class="material-icons">edit</i> Editar art&iacute;culo</button>
                                        <button class="button margin-0 btnFrmContrataciones" onclick="eliminarElemento('item', 'item_${contadorElementosItem}', [], '${contadorElementosItem}')"><i class="material-icons">remove_circle</i> Eliminar art&iacute;culo</button>
                                    </div>
                                    <div id="datos-elemento-item_${contadorElementosItem}">
                                        <input type="hidden" name="txtITEM_ID_${contadorElementosItem}" id="txtITEM_ID_${contadorElementosItem}" value="${item.id}">
                                        <input type="hidden" name="txtITEM_DESCRIPTION_${contadorElementosItem}" id="txtITEM_DESCRIPTION_${contadorElementosItem}" value="${item.description}">
                                        <input type="hidden" name="txtITEM_CLASSIFICATION_ID_${contadorElementosItem}" id="txtITEM_CLASSIFICATION_ID_${contadorElementosItem}" value="${item.classification.id}">
                                        <input type="hidden" name="cmbITEM_CLASSIFICATION_SCHEME_${contadorElementosItem}" id="cmbITEM_CLASSIFICATION_SCHEME_${contadorElementosItem}" value="${item.classification.scheme}">
                                        <input type="hidden" name="txtITEM_CLASSIFICATION_DESCRIPTION_${contadorElementosItem}" id="txtITEM_CLASSIFICATION_DESCRIPTION_${contadorElementosItem}" value="${item.classification.description}">
                                        <input type="hidden" name="txtITEM_CLASSIFICATION_URI_${contadorElementosItem}" id="txtITEM_CLASSIFICATION_URI_${contadorElementosItem}" value="${item.classification.uri}">
                                        <input type="hidden" name="txtITEM_QUANTITY_${contadorElementosItem}" id="txtITEM_QUANTITY_${contadorElementosItem}" value="${item.quantity}">
                                        <input type="hidden" name="txtITEM_UNIT_ID_${contadorElementosItem}" id="txtITEM_UNIT_ID_${contadorElementosItem}" value="${item.unit.id}">
                                        <input type="hidden" name="cmbITEM_UNIT_SCHEME_${contadorElementosItem}" id="cmbITEM_UNIT_SCHEME_${contadorElementosItem}" value="${item.unit.scheme}">
                                        <input type="hidden" name="txtITEM_UNIT_NAME_${contadorElementosItem}" id="txtITEM_UNIT_NAME_${contadorElementosItem}" value="${item.unit.name}">
                                        <input type="hidden" name="txtITEM_UNIT_URI_${contadorElementosItem}" id="txtITEM_UNIT_URI_${contadorElementosItem}" value="${item.unit.uri}">
                                        <input type="hidden" name="txtITEM_UNIT_AMOUNT_${contadorElementosItem}" id="txtITEM_UNIT_AMOUNT_${contadorElementosItem}" value="${item.unit.value.amount}">
                                        <input type="hidden" name="cmbITEM_UNIT_CURRENCY_${contadorElementosItem}" id="cmbITEM_UNIT_CURRENCY_${contadorElementosItem}" value="${item.unit.value.currency}">
                                    </div>
                                    <fieldset class="cell fieldset"> 
                                        <input type="hidden" id="estructura-por-defecto-classification_${contadorElementosItem}" value=""> 
                                        <input type="hidden" id="siguiente-identificador-classification_${contadorElementosItem}" value="" data-valor-por-defecto=""> 
                                        <input type="hidden" id="lista-identificadores-por-registrar-classification_${contadorElementosItem}" name="lista-identificadores-por-registrar-classification_${contadorElementosItem}" value="" data-valor-por-defecto="">
                                        <legend title="Una lista de clasificaciones adicionales para el art&iacute;culo.">Clasificaciones adicionales</legend>
                                        <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                            <a onclick="restaurarEstadoInicialElemento('classification', [${contadorElementosItem}])"><i class="material-icons secondary">restart_alt</i></a> 
                                            <a onclick="abrirNuevoFormulario('classification', [${contadorElementosItem}])"><i class="material-icons secondary">add_circle</i></a> 
                                        </div>
                                        <c:choose>
                                            <c:when test="${item.additionalClassifications.size() > 0}">
                                                <c:set var="contadorElementosClassification" value="0"></c:set>
                                                <div id="contenedor-classification_${contadorElementosItem}" style="display: flex; flex-direction: column; gap: 10px;">
                                                <c:forEach items="${item.additionalClassifications}" var="clasificacion">
                                                    <div id="classification_${contadorElementosItem}_${contadorElementosClassification}">
                                                        <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                                            <div class="cell medium-8">
                                                                <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">clasificaci&oacute;n</span> - <span id="titulo-elemento-classification_${contadorElementosItem}_${contadorElementosClassification}">${clasificacion.id}</span></h6>
                                                            </div>
                                                            <div id="datos-elemento-classification_${contadorElementosItem}_${contadorElementosClassification}">
                                                                <input type="hidden" name="txtCLASSIFICATION_ID_${contadorElementosItem}_${contadorElementosClassification}" id="txtCLASSIFICATION_ID_${contadorElementosItem}_${contadorElementosClassification}" value="${clasificacion.id}">
                                                                <input type="hidden" name="cmbCLASSIFICATION_SCHEME_${contadorElementosItem}_${contadorElementosClassification}" id="cmbCLASSIFICATION_SCHEME_${contadorElementosItem}_${contadorElementosClassification}" value="${clasificacion.scheme}">
                                                                <input type="hidden" name="txtCLASSIFICATION_DESCRIPTION_${contadorElementosItem}_${contadorElementosClassification}" id="txtCLASSIFICATION_DESCRIPTION_${contadorElementosItem}_${contadorElementosClassification}" value="${clasificacion.description}">
                                                                <input type="hidden" name="txtCLASSIFICATION_URI_${contadorElementosItem}_${contadorElementosClassification}" id="txtCLASSIFICATION_URI_${contadorElementosItem}_${contadorElementosClassification}" value="${clasificacion.uri}">
                                                            </div>
                                                            <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                                <button class="button margin-0" onclick="abrirFormularioPrevio('classification', [${contadorElementosItem}, ${contadorElementosClassification}])"><i class="material-icons">edit</i> Editar</button>
                                                                <button class="button margin-0" onclick="eliminarElemento('classification', 'classification_${contadorElementosItem}_${contadorElementosClassification}', [${contadorElementosItem}], '${contadorElementosClassification}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <c:set var="contadorElementosClassification" value="${contadorElementosClassification + 1}"></c:set>
                                                </c:forEach>
                                                </div>
                                                <input type="hidden" name="ultimo-identificador-elementos-classification_${contadorElementosItem}" id="ultimo-identificador-elementos-classification_${contadorElementosItem}" value="${contadorElementosClassification}">
                                            </c:when>
                                            <c:otherwise>
                                                <div id="contenedor-classification_${contadorElementosItem}" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                                <input type="hidden" name="ultimo-identificador-elementos-classification_${contadorElementosItem}" id="ultimo-identificador-elementos-classification_${contadorElementosItem}" value="0">
                                            </c:otherwise>
                                        </c:choose>
                                        <script>
                                            document.getElementById("estructura-por-defecto-classification_${contadorElementosItem}").value = obtenerDatosInicialesElementos("classification", [${contadorElementosItem}]);
                                        </script>
                                    </fieldset>
                                </div>
                            </div>
                        </li>
                        <c:set var="contadorElementosItem" value="${contadorElementosItem + 1}"></c:set>
                    </c:forEach>
                    </ul>
                    <input type="hidden" name="ultimo-identificador-elementos-item" id="ultimo-identificador-elementos-item" value="${contadorElementosItem}">
                </c:when>
                <c:otherwise>
                    <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-item"></ul>
                    <input type="hidden" name="ultimo-identificador-elementos-item" id="ultimo-identificador-elementos-item" value="0">
                </c:otherwise>
            </c:choose>
            <div class="grid-x grid-margin-x margin-top-1">
                <fieldset class="cell medium-6">
                    <input type="button" class="button expanded secondary" onclick="restaurarEstadoInicialElemento('item', []);" value="${(bolEsFormulariosDeEdicion == true) ? 'Restaurar' : 'Reiniciar'}"/>
                </fieldset>
                <fieldset class="cell medium-6">
                    <input type="button" onclick="validarFormulario('frmRegistrarSeccionLicitacionItems');" class="button expanded" id="btnRegistrarValoresFormulario" value="${(bolEsFormulariosDeEdicion == true) ? 'Modificar datos' : 'Continuar'}"/>
                </fieldset>
            </div>
        </form>
    </div>
</div>
    
<%-- formulario de un articulo (TOP) --%>
<div id="modalFrmItem" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Art&iacute;culo</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmItem" id="frmItem" method="post" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-item" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-6">ID
                            <input type="text" id="txtITEM_ID" required placeholder="p. ej. 1" value="" title="Un identificador local al cual hacer referencia y con el cual unir los art&iacute;culos. Debe de ser &uacute;nico en relaci&oacute;n a los dem&aacute;s &iacute;tems del mismo proceso de contrataci&oacute;n presentes en la matriz de art&iacute;culos."/>
                            <span class="form-error" data-form-error-for="txtITEM_ID" id="spanITEM_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Descripci&oacute;n
                            <input type="text" id="txtITEM_DESCRIPTION" placeholder="p. ej. Breve descripci&oacute;n de los bienes o servicios" value="" title="Una descripci&oacute;n de los bienes o servicios objetos del procedimiento de contrataci&oacute;n." />
                            <span class="form-error" data-form-error-for="txtITEM_DESCRIPTION" id="spanITEM_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <fieldset class="fieldset cell">
                            <legend title="La clasificaci&oacute;n primaria para un art&iacute;culo.">Clasificaci&oacute;n</legend>
                            <div class="grid-x grid-margin-x">
                                <label class="cell medium-6">ID
                                    <input type="text" id="txtITEM_CLASSIFICATION_ID" placeholder="p. ej. 1" value="" title="El c&oacute;digo de clasificaci&oacute;n que se obtiene del esquema." />
                                    <span class="form-error" data-form-error-for="txtITEM_CLASSIFICATION_ID" id="spanITEM_CLASSIFICATION_ID">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Esquema
                                    <select id="cmbITEM_CLASSIFICATION_SCHEME" title="El esquema o lista de c&oacute;digo del cual se obtiene el c&oacute;digo de clasificaci&oacute;n. Para clasificaciones de art&iacute;culos de linea.">
                                        <option value="">--Elige una opci&oacute;n--</option>
                                        <c:forEach items="${esquemasDeClasificacion}" var="esquema">
                                            <option value="${esquema.clave}">${esquema.valor}</option>
                                        </c:forEach>
                                    </select>
                                    <span class="form-error" data-form-error-for="cmbITEM_CLASSIFICATION_SCHEME" id="spanITEM_CLASSIFICATION_SCHEME">
                                        Elige una opci&oacute;n.
                                    </span>
                                </label>
                                <label class="cell medium-6">Descripci&oacute;n
                                    <input type="text" id="txtITEM_CLASSIFICATION_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n de la clasificaci&oacute;n" value="" title="Una descripci&oacute;n textual o t&iacute;tulo del c&oacute;digo de clasificaci&oacute;n." />
                                    <span class="form-error" data-form-error-for="txtITEM_CLASSIFICATION_DESCRIPTION" id="spanITEM_CLASSIFICATION_DESCRIPTION">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">URI
                                    <input type="text" id="txtITEM_CLASSIFICATION_URI" placeholder="p. ej. www.url.com" pattern="url" value="" title="Un URI para identificar &uacute;nicamente el c&oacute;digo de clasificaci&oacute;n."/>
                                    <span class="form-error" data-form-error-for="txtITEM_CLASSIFICATION_URI" id="spanITEM_CLASSIFICATION_URI">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                            </div>
                        </fieldset>
                        <label class="cell">Cantidad
                            <input type="number" id="txtITEM_QUANTITY" placeholder="p. ej. 1" min="0" value="" title="El n&uacute;mero de unidades que se dan."/>
                            <span class="form-error" data-form-error-for="txtITEM_QUANTITY" id="spanITEM_QUANTITY">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <fieldset class="fieldset cell">
                            <legend title="Una descripci&oacute;n de la unidad en la cual los suministros, servicios o trabajos est&aacute;n provistos (ej. horas, kilos) y el precio por unidad.">Unidad</legend>
                            <div class="grid-x grid-margin-x">
                                <label class="cell medium-6">ID
                                    <input type="text" id="txtITEM_UNIT_ID" placeholder="p. ej. 1" value="" title="El identificador de la lista de c&oacute;digos a la que se hace referencia en la propiedad de esquema."/>
                                    <span class="form-error" data-form-error-for="txtITEM_UNIT_ID" id="spanITEM_UNIT_ID">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Esquema
                                    <select id="cmbITEM_UNIT_SCHEME" title="La lista de la cual se obtienen los identificadores de unidades de medida. Se recomienda &#39;UNCEFACT&#39;.">
                                        <option value="">--Elige una opci&oacute;n--</option>
                                        <c:forEach items="${esquemasDeUnidad}" var="esquema">
                                            <option value="${esquema.clave}">${esquema.valor}</option>
                                        </c:forEach>
                                    </select>
                                    <span class="form-error" data-form-error-for="cmbITEM_UNIT_SCHEME" id="spanITEM_UNIT_SCHEME">
                                        Elige una opci&oacute;n.
                                    </span>
                                </label>
                                <label class="cell medium-6">Nombre
                                    <input type="text" id="txtITEM_UNIT_NAME" placeholder="p. ej. Nombre de la unidad" value="" title="Nombre de la unidad." />
                                    <span class="form-error" data-form-error-for="txtITEM_UNIT_NAME" id="spanITEM_UNIT_NAME">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">URI
                                    <input type="text" id="txtITEM_UNIT_URI" placeholder="p. ej. www.url.com" pattern="url" value="" title="Un URI le&iacute;ble por m&aacute;quina para la unidad de medida, dada por el esquema."/>
                                    <span class="form-error" data-form-error-for="txtITEM_UNIT_URI" id="spanITEM_UNIT_URI">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <fieldset class="fieldset cell">
                                    <legend title="El valor monetario de una unidad.">Valor</legend>
                                    <div class="grid-margin-x grid-x">
                                        <label class="cell medium-6">Monto
                                            <input type="number" id="txtITEM_UNIT_AMOUNT" placeholder="p. ej. 0" min="0" value="" title="Monto como una cifra."/>
                                            <span class="form-error" data-form-error-for="txtITEM_UNIT_AMOUNT" id="spanITEM_UNIT_AMOUNT">
                                                Campo requerido, verifica el formato de tu registro.
                                            </span>
                                        </label>
                                        <label class="cell medium-6">Moneda
                                            <select id="cmbITEM_UNIT_CURRENCY" title="La moneda del monto.">
                                                <option value="">--Elige una opci&oacute;n--</option>
                                                <c:forEach items="${tipoMoneda}" var="moneda">
                                                    <option value="${moneda.clave}" >${moneda.valor}</option>
                                                </c:forEach>
                                            </select>
                                            <span class="form-error" data-form-error-for="cmbITEM_UNIT_CURRENCY" id="spanITEM_UNIT_CURRENCY">
                                                Elige una opci&oacute;n.
                                            </span>
                                        </label>
                                    </div>
                                </fieldset>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-item"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-item"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un articulo (BOTTOM) --%>
<%-- formulario de una clasificacion (TOP) --%>
<div id="modalFrmClassification" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Clasificaci&oacute;n</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmClassification" id="frmClassification" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-classification" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-6">ID
                            <input type="text" id="txtCLASSIFICATION_ID" required placeholder="p. ej. 1" value="" title="El c&oacute;digo de clasificaci&oacute;n que se obtiene del esquema."/>
                            <span class="form-error" data-form-error-for="txtCLASSIFICATION_ID" id="spanCLASSIFICATION_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Esquema
                            <select id="cmbCLASSIFICATION_SCHEME" title="El esquema o lista de c&oacute;digo del cual se obtiene el c&oacute;digo de clasificaci&oacute;n. Para clasificaciones de art&iacute;culos de linea.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${esquemasDeClasificacion}" var="esquema">
                                    <option value="${esquema.clave}">${esquema.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbCLASSIFICATION_SCHEME" id="spanCLASSIFICATION_SCHEME">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                        <label class="cell medium-6">Descripci&oacute;n
                            <input type="text" id="txtCLASSIFICATION_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n de la clasificaci&oacute;n" value="" title="Una descripci&oacute;n textual o t&iacute;tulo del c&oacute;digo de clasificaci&oacute;n." />
                            <span class="form-error" data-form-error-for="txtCLASSIFICATION_DESCRIPTION" id="spanCLASSIFICATION_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">URI
                            <input type="text" id="txtCLASSIFICATION_URI" placeholder="p. ej. www.url.com" pattern="url" value="" title="Un URI para identificar &uacute;nicamente el c&oacute;digo de clasificaci&oacute;n."/>
                            <span class="form-error" data-form-error-for="txtCLASSIFICATION_URI" id="spanCLASSIFICATION_URI">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-classification"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-classification"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de una clasificacion (BOTTOM) --%>

<script>
                
    function validarFormulario(idFormulario)
    { 
        
        let strMensajeFrm;          
        let strTituloMensaje;       
        let bolTipoFormulario;      

        bolTipoFormulario = document.getElementById("txtEsFormularioDeEdicion").value;
        bolTipoFormulario = Boolean(bolTipoFormulario);
        strMensajeFrm = (bolTipoFormulario === true) ? "\u00bf Los datos son correctos\u003F" : "Una vez guardada la informaci\u00F3n de esta secci\u00F3n no se podr\u00E1 volver a modificar los valores ingresados hasta que se finalice con el proceso de registro de la contrataci\u00F3n p\u00FAblica.";
        strTituloMensaje = (bolTipoFormulario === true) ? "Confirmar" : "\u00bfLos datos son correctos\u003F";

        swal({
            title: strTituloMensaje,
            text: strMensajeFrm,
            icon: "warning",
            buttons: ["Cancelar", "Aceptar"],
            dangerMode: true,
            closeOnClickOutside: false,
            closeOnEsc: false
        })
        .then((seAcepta) => {
            if (seAcepta)
            { 
                document.getElementById(idFormulario).submit();
            } 
            else
            { 
                swal("Se ha cancelado la operaci\u00f3n", {
                    icon: "error",
                    buttons: false,
                    timer: 2000
                });
                return false;
            } 
        });
                    
    } 
    
    window.onload = function (){
        
        document.getElementById("estructura-por-defecto-item").value = obtenerDatosInicialesElementos("item", []);
        
    };
    
</script> 
