<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fieldset class="fieldset cell">
    <legend>Licitantes</legend>
        <c:choose>
            <c:when test="${contratacion.tender.tenderers == null || contratacion.tender.tenderers.size() == 0}">
                Dato no proporcionado
            </c:when>
            <c:otherwise>
            <ul class="accordion" data-accordion data-allow-all-closed="true">
                <c:set var="contador" value="1"></c:set>
                <c:forEach items="${contratacion.tender.tenderers}" var="tender">
                    <li class="accordion-item ${ contador == 1 ? "is-active" : "" }" data-accordion-item>
                        <a href="#" class="accordion-title">Licitante #${contador}</a>
                        <div class="accordion-content" data-tab-content>
                            <dl class="grid-x grid-margin-x medium-up-1 large-up-2">    
                                <dt class="cell callout primary">Nombre de la organizaci&oacute;n</dt>
                                <dd class="cell callout">
                                <c:choose>
                                    <c:when test="${tender.name != null && tender.name.equals('') == false}">
                                        ${tender.name}
                                    </c:when>
                                    <c:otherwise>
                                        Dato no proporcionado
                                    </c:otherwise>            
                                </c:choose>
                                </dd>
                                <dt class="cell callout primary">ID de la organizaci&oacute;n</dt>
                                <dd class="cell callout">
                                <c:choose>
                                    <c:when test="${tender.id != null}">
                                        ${tender.id}
                                    </c:when>
                                    <c:otherwise>
                                        Dato no proporcionado
                                    </c:otherwise>            
                                </c:choose>
                                </dd>
                            </dl>
                        </div>
                    </li>
                    <c:set var="contador" value="${contador+1}"></c:set>
                </c:forEach>
            </ul>
            </c:otherwise>
        </c:choose>
</fieldset>