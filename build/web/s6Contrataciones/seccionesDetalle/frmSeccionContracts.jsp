<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="tabs-panel" id="panela8v">
    <h3>Contratos</h3>
    <dl class="callout">
        <c:set var="contadorPrincipal" value="1"></c:set>
        <c:choose>
            <c:when test="${contratacion.contracts == null || contratacion.contracts.size() == 0}">
                <dd>Dato no proporcionado</dd>
            </c:when>
            <c:otherwise>
                <ul class="accordion" data-accordion data-allow-all-closed="true">
                    <c:forEach items="${contratacion.contracts}" var="contract">
                        <li class="accordion-item ${ contadorPrincipal == 1 ? "is-active" : "" }" data-accordion-item>
                            <a href="#" class="accordion-title">Contrato #${contadorPrincipal}</a>
                            <div class="accordion-content" data-tab-content>
                                <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                    <dt class="cell callout primary">ID del Contrato</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${contract.id != null}">
                                            ${contract.id}
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                    <dt class="cell callout primary">ID de Adjudicaci&oacute;n</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${contract.awardID != null}">
                                            ${contract.awardID}
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                    <dt class="cell callout primary">T&iacute;tulo del contrato</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${contract.title != null && contract.title.equals('') == false}">
                                            ${contract.title}
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                    <dt class="cell callout primary">Descripci&oacute;n del contrato</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${contract.description != null && contract.description.equals('') == false}">
                                            ${contract.description}
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                    <dt class="cell callout primary">Estado del Contrato</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${contract.status != null && contract.status.equals('') == false}">
                                            ${contract.status}
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                </dl>

                                <fieldset class="fieldset">
                                    <legend>Periodo</legend>
                                    <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                        <dt class="cell callout primary">Fecha de inicio</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${contract.period.startDate != null && contract.period.startDate.equals('') == false}">
                                                ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(contract.period.startDate).toLowerCase()}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Fecha de fin</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${contract.period.endDate != null && contract.period.endDate.equals('') == false}">
                                                ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(contract.period.endDate).toLowerCase()}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Extensi&oacute;n m&aacute;xima</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${contract.period.maxExtentDate != null && contract.period.maxExtentDate.equals('') == false}">
                                                ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(contract.period.maxExtentDate).toLowerCase()}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Extensi&oacute;n m&aacute;xima</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${contract.period.durationInDays != null}">
                                                ${contract.period.durationInDays}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                    </dl>
                                </fieldset>
                                <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                    <dt class="cell callout primary">Valor</dt>
                                    <dd class="cell callout valorMonetario">
                                    <c:choose>
                                        <c:when test="${contract.value.amount != null}">
                                            &#36;${formateador.format(contract.value.amount)} 
                                            <c:choose> 
                                                <c:when test="${contract.value.currency != null && contract.value.currency.equals('') == false}">
                                                    ${contract.value.currency}
                                                </c:when>
                                                <c:otherwise>
                                                    (El tipo de moneda no fue proporcionado)
                                                </c:otherwise>
                                            </c:choose>
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                </dl>

                                <%@include file="SeccionContracts/frmSeccionContractsItems.jsp" %>

                                <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                    <dt class="cell callout primary">Fecha de firma</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${contract.dateSigned != null && contract.dateSigned.equals('') == false}">
                                            ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(contract.dateSigned).toLowerCase()}
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                </dl>

                                <fieldset class="fieldset">
                                    <legend>Documentos</legend>
                                    <c:choose>
                                        <c:when test="${contract.documents == null || contract.documents.size() == 0}">
                                            Dato no proporcionado
                                        </c:when>
                                        <c:otherwise>
                                            <ul class="accordion" data-accordion data-allow-all-closed="true">
                                                <c:set var="contador" value="1"></c:set>
                                                <c:forEach items="${contract.documents}" var="document">
                                                    <li class="accordion-item ${ contador == 1 ? "is-active" : "" }" data-accordion-item>
                                                        <a href="#" class="accordion-title">Documento #${contador}</a>
                                                        <div class="accordion-content" data-tab-content>
                                                            <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                                                <dt class="cell callout primary">ID</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${document.id != null}">
                                                                        ${document.id}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Tipo de Documento</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${document.documentType != null && document.documentType.equals('') == false}">
                                                                        ${document.documentType}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">T&iacute;tulo</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${document.title != null && document.title.equals('') == false}">
                                                                        ${document.title}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Descripci&oacute;n</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${document.description != null && document.description.equals('') == false}">
                                                                        ${document.description}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">URL</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${document.url != null && document.url.equals('') == false}">
                                                                        <a href="${(fn:contains(document.url, 'http')) ? '' : '//' }${document.url}" target="_blank">${document.url}</a>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Fecha de publicaci&oacute;n</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${document.datePublished != null && document.datePublished.equals('') == false}">
                                                                        ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(document.datePublished).toLowerCase()}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Fecha de modificaci&oacute;n</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${document.dateModified != null && document.dateModified.equals('') == false}">
                                                                        ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(document.dateModified).toLowerCase()}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Formato</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${document.format != null && document.format.equals('') == false}">
                                                                        ${document.format}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Idioma</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${document.language != null && document.language.equals('') == false}">
                                                                        ${document.language}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                            </dl>
                                                        </div>
                                                    </li>
                                                    <c:set var="contador" value="${contador+1}"></c:set>
                                                </c:forEach>
                                            </ul>
                                        </c:otherwise>
                                    </c:choose>
                                </fieldset>

                                <%@include file="SeccionContracts/frmSeccionContractsImplementation.jsp" %>

                                <fieldset class="fieldset">
                                    <legend>Procesos relacionados</legend>
                                    <c:choose>
                                        <c:when test="${contract.relatedProcesses == null || contract.relatedProcesses.size() == 0}">
                                            Dato no proporcionado
                                        </c:when>
                                        <c:otherwise>
                                            <ul class="accordion" data-accordion data-allow-all-closed="true">
                                                <c:set var="contador" value="1"></c:set>
                                                <c:forEach items="${contract.relatedProcesses}" var="relatedProcess">
                                                    <li class="accordion-item ${ contador == 1 ? "is-active" : "" }" data-accordion-item>
                                                        <a href="#" class="accordion-title">Proceso Relacionado #${contador}</a>
                                                        <div class="accordion-content" data-tab-content>
                                                            <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                                                <dt class="cell callout primary">ID</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${relatedProcess.id != null && relatedProcess.id.equals('') == false}">
                                                                        ${relatedProcess.id}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Relaci&oacute;n</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${relatedProcess.relationship == null || relatedProcess.relationship.size() == 0}">
                                                                        Dato no proporcionado
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <ul>
                                                                            <c:forEach items="${relatedProcess.relationship}" var="relation">
                                                                                <li>${relation}</li>
                                                                            </c:forEach>
                                                                        </ul>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">T&iacute;tulo de proceso relacionado</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${relatedProcess.title != null && relatedProcess.title.equals('') == false}">
                                                                        ${relatedProcess.title}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Esquema</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${relatedProcess.scheme != null && relatedProcess.scheme.equals('') == false}">
                                                                        ${relatedProcess.scheme}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Identificador</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${relatedProcess.identifier != null && relatedProcess.identifier.equals('') == false}">
                                                                        ${relatedProcess.identifier}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">URI de proceso relacionado</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${relatedProcess.uri != null && relatedProcess.uri.equals('') == false}">
                                                                        <a href="${(fn:contains(relatedProcess.uri, 'http')) ? '' : '//' }${relatedProcess.uri}" target="_blank">${relatedProcess.uri}</a>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                            </dl>
                                                        </div>
                                                    </li>
                                                    <c:set var="contador" value="${contador+1}"></c:set>
                                                </c:forEach>
                                            </ul>
                                        </c:otherwise>
                                    </c:choose>
                                </fieldset>
                                <fieldset class="fieldset">
                                    <legend>Hitos del contrato</legend>
                                    <c:choose>
                                        <c:when test="${contract.milestones == null || contract.milestones.size() == 0}">
                                            Dato no proporcionado
                                        </c:when>
                                        <c:otherwise>
                                            <ul class="accordion" data-accordion data-allow-all-closed="true">
                                                <c:set var="contador" value="1"></c:set>
                                                <c:forEach items="${contract.milestones}" var="milestone">
                                                    <li class="accordion-item ${ contador == 1 ? "is-active" : "" }" data-accordion-item>
                                                        <a href="#" class="accordion-title">Hito #${contador}</a>
                                                        <div class="accordion-content" data-tab-content>
                                                            <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                                                <dt class="cell callout primary">ID</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${milestone.id != null}">
                                                                        ${milestone.id}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">T&iacute;tulo</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${milestone.title != null && milestone.title.equals('') == false}">
                                                                        ${milestone.title}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Tipo de hito</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${milestone.type != null && milestone.type.equals('') == false}">
                                                                        ${milestone.type}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Descripci&oacute;n</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${milestone.description != null && milestone.description.equals('') == false}">
                                                                        ${milestone.description}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">C&oacute;digo de hito</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${milestone.code != null && milestone.code.equals('') == false}">
                                                                        ${milestone.code}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Fecha l&iacute;mite</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${milestone.dueDate != null && milestone.dueDate.equals('') == false}">
                                                                        ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(milestone.dueDate).toLowerCase()}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Fecha de cumplimiento</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${milestone.dateMet != null && milestone.dateMet.equals('') == false}">
                                                                        ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(milestone.dateMet).toLowerCase()}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Fecha de modificaci&oacute;n</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${milestone.dateModified != null && milestone.dateModified.equals('') == false}">
                                                                        ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(milestone.dateModified).toLowerCase()}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Estado</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${milestone.status != null && milestone.status.equals('') == false}">
                                                                        ${milestone.status}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                            </dl>
                                                        </div>
                                                    </li>
                                                    <c:set var="contador" value="${contador+1}"></c:set>
                                                </c:forEach>
                                            </ul>
                                        </c:otherwise>
                                    </c:choose>
                                </fieldset>
                                <fieldset class="fieldset">
                                    <legend>Enmiendas</legend>
                                    <c:choose>
                                        <c:when test="${contract.amendments == null || contract.amendments.size() == 0}">
                                            Dato no proporcionado
                                        </c:when>
                                        <c:otherwise>
                                            <ul class="accordion" data-accordion data-allow-all-closed="true">
                                                <c:set var="contador" value="1"></c:set>
                                                <c:forEach items="${contract.amendments}" var="amendment">
                                                    <li class="accordion-item ${ contador == 1 ? "is-active" : "" }" data-accordion-item>
                                                        <a href="#" class="accordion-title">Enmienda #${contador}</a>
                                                        <div class="accordion-content" data-tab-content>
                                                            <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                                                <dt class="cell callout primary">Fecha de enmienda</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${amendment.date != null && amendment.date.equals('') == false}">
                                                                        ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(amendment.date).toLowerCase()}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Justificaci&oacute;n</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${amendment.rationale != null && amendment.rationale.equals('') == false}">
                                                                        ${amendment.rationale}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">ID</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${amendment.id != null && amendment.id.equals('') == false}">
                                                                        ${amendment.id}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Descripci&oacute;n</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${amendment.description != null && amendment.description.equals('') == false}">
                                                                        ${amendment.description}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Entrega enmendada (identificador)</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${amendment.amendsReleaseID != null && amendment.amendsReleaseID.equals('') == false}">
                                                                        ${amendment.amendsReleaseID}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Entrega de enmienda (identificador)</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${amendment.releaseID != null && amendment.releaseID.equals('') == false}">
                                                                        ${amendment.releaseID}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                            </dl>
                                                        </div>
                                                    </li>
                                                    <c:set var="contador" value="${contador+1}"></c:set>
                                                </c:forEach>
                                            </ul>
                                        </c:otherwise>
                                    </c:choose>
                                </fieldset>
                            </div>
                        </li>
                        <c:set var="contadorPrincipal" value="${contadorPrincipal+1}"></c:set>
                    </c:forEach>
                </ul>       
            </c:otherwise>
        </c:choose>
    </dl>
</div>
