<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="tabs-panel" id="panela2v">
    <h3>Etiqueta de entrega</h3>
    <dl class="callout">
        <dd>
            <c:choose>
                <c:when test="${contratacion.tag == null || contratacion.tag.size() == 0}">
                    Dato no proporcionado
                </c:when>
                <c:otherwise>
                    <ul>
                        <c:forEach items="${contratacion.tag}" var="item">
                            <li>${item}</li>
                        </c:forEach>
                    </ul>
                </c:otherwise>
            </c:choose>
        </dd>
    </dl>
</div>