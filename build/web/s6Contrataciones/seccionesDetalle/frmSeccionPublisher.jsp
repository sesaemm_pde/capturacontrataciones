<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="tabs-panel is-active" id="panela1v">
    <h3>Publicador</h3>
    <div class="callout">
        <dl class="grid-x grid-margin-x medium-up-1 large-up-2">   
            <dt class="cell callout primary">Nombre</dt>
            <dd class="cell callout">
            <c:choose>
                <c:when test="${contratacion.publisher.name != null && contratacion.publisher.name.equals('') == false}">
                    ${contratacion.publisher.name}
                </c:when>
                <c:otherwise>
                    Dato no proporcionado
                </c:otherwise>            
            </c:choose>
            </dd>
            <dt class="cell callout primary">Esquema</dt>
            <dd class="cell callout">
            <c:choose>
                <c:when test="${contratacion.publisher.scheme != null && contratacion.publisher.scheme.equals('') == false}">
                    ${contratacion.publisher.scheme}
                </c:when>
                <c:otherwise>
                    Dato no proporcionado
                </c:otherwise>            
            </c:choose>
            </dd>
            <dt class="cell callout primary">UID</dt>
            <dd class="cell callout">
            <c:choose>
                <c:when test="${contratacion.publisher.uid != null && contratacion.publisher.uid.equals('') == false}">
                    ${contratacion.publisher.uid}
                </c:when>
                <c:otherwise>
                    Dato no proporcionado
                </c:otherwise>            
            </c:choose>
            </dd>
            <dt class="cell callout primary">URI</dt>
            <dd class="cell callout">
            <c:choose>
                <c:when test="${contratacion.publisher.uri != null && contratacion.publisher.uri.equals('') == false}">
                    <a href="${(fn:contains(contratacion.publisher.uri, 'http')) ? '' : '//' }${contratacion.publisher.uri}" target="_blank">${contratacion.publisher.uri}</a>
                </c:when>
                <c:otherwise>
                    Dato no proporcionado
                </c:otherwise>            
            </c:choose>
            </dd>
        </dl>
    </div>
</div>