<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="tabs-panel" id="panela7v">
    <h3>Adjudicaci&oacute;n</h3>
    <div class="callout">
        <c:set var="contadorPrincipal" value="1"></c:set>
        <c:choose>
            <c:when test="${contratacion.awards == null || contratacion.awards.size() == 0}">
                <dd>Dato no proporcionado</dd>
            </c:when>
            <c:otherwise>
                <ul class="accordion" data-accordion data-allow-all-closed="true">
                    <c:forEach items="${contratacion.awards}" var="award">
                        <li class="accordion-item ${ contadorPrincipal == 1 ? "is-active" : "" }" data-accordion-item>
                            <a href="#" class="accordion-title">Adjudicaci&oacute;n #${contadorPrincipal}</a>
                            <div class="accordion-content" data-tab-content>
                                <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                    <dt class="cell callout primary">ID de Adjudicaci&oacute;n</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${award.id != null}">
                                            ${award.id}
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                    <dt class="cell callout primary">T&iacute;tulo</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${award.title != null && award.title.equals('') == false}">
                                            ${award.title}
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                    <dt class="cell callout primary">Descripci&oacute;n</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${award.description != null && award.description.equals('') == false}">
                                            ${award.description}
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                    <dt class="cell callout primary">Estado de la Adjudicaci&oacute;n</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${award.status != null && award.status.equals('') == false}">
                                            ${award.status}
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                    <dt class="cell callout primary">Fecha de adjudicaci&oacute;n</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${award.date != null && award.date.equals('') == false}">
                                            ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(award.date).toLowerCase()}
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                    <dt class="cell callout primary">Valor</dt>
                                    <dd class="cell callout valorMonetario">
                                    <c:choose>
                                        <c:when test="${award.value.amount != null}">
                                            &#36;${formateador.format(award.value.amount)} 
                                            <c:choose> 
                                                <c:when test="${award.value.currency != null && award.value.currency.equals('') == false}">
                                                    ${award.value.currency}
                                                </c:when>
                                                <c:otherwise>
                                                    (El tipo de moneda no fue proporcionado)
                                                </c:otherwise>
                                            </c:choose>
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>  
                                </dl>
                                <fieldset class="fieldset">
                                    <legend>Proveedores</legend>
                                    <ul class="accordion" data-accordion data-allow-all-closed="true">
                                        <c:set var="contador" value="1"></c:set>
                                        <c:forEach items="${award.suppliers}" var="supplier">
                                            <li class="accordion-item ${ contador == 1 ? "is-active" : "" }" data-accordion-item>
                                                <a href="#" class="accordion-title">Proveedor #${contador}</a>
                                                <div class="accordion-content" data-tab-content>
                                                    <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                                        <dt class="cell callout primary">Nombre de la Organizaci&oacute;n</dt>
                                                        <dd class="cell callout">
                                                        <c:choose>
                                                            <c:when test="${supplier.name != null && supplier.name.equals('') == false}">
                                                                ${supplier.name}
                                                            </c:when>
                                                            <c:otherwise>
                                                                Dato no proporcionado
                                                            </c:otherwise>            
                                                        </c:choose>
                                                        </dd>
                                                        <dt class="cell callout primary">ID de Organizaci&oacute;n</dt>
                                                        <dd class="cell callout">
                                                        <c:choose>
                                                            <c:when test="${supplier.id != null}">
                                                                ${supplier.id}
                                                            </c:when>
                                                            <c:otherwise>
                                                                Dato no proporcionado
                                                            </c:otherwise>            
                                                        </c:choose>
                                                        </dd>
                                                    </dl>
                                                </div>
                                            </li>
                                            <c:set var="contador" value="${contador+1}"></c:set>
                                        </c:forEach>
                                    </ul>
                                </fieldset>

                                <%@include file="SeccionAwards/frmSeccionAwardsItems.jsp" %>

                                <fieldset class="fieldset">
                                    <legend>Periodo de contrato</legend>
                                    <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                        <dt class="cell callout primary">Fecha de inicio</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${award.contractPeriod.startDate != null && award.contractPeriod.startDate.equals('') == false}">
                                                ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(award.contractPeriod.startDate).toLowerCase()}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Fecha de fin</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${award.contractPeriod.endDate != null && award.contractPeriod.endDate.equals('') == false}">
                                                ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(award.contractPeriod.endDate).toLowerCase()}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Extensi&oacute;n m&aacute;xima</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${award.contractPeriod.maxExtentDate != null && award.contractPeriod.maxExtentDate.equals('') == false}">
                                                ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(award.contractPeriod.maxExtentDate).toLowerCase()}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Duraci&oacute;n (d&iacute;as)</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${award.contractPeriod.durationInDays != null}">
                                                ${award.contractPeriod.durationInDays}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                    </dl>
                                </fieldset>
                                <fieldset class="fieldset">
                                    <legend>Documentos</legend>
                                    <c:choose>
                                        <c:when test="${award.documents == null || award.documents.size() == 0}">
                                            Dato no proporcionado
                                        </c:when>
                                        <c:otherwise>
                                            <ul class="accordion" data-accordion data-allow-all-closed="true">
                                                <c:set var="contador" value="1"></c:set>
                                                <c:forEach items="${award.documents}" var="document">
                                                    <li class="accordion-item ${ contador == 1 ? "is-active" : "" }" data-accordion-item>
                                                        <a href="#" class="accordion-title">Documento #${contador}</a>
                                                        <div class="accordion-content" data-tab-content>
                                                            <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                                                <dt class="cell callout primary">ID</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${document.id != null && document.id.equals('') == false}">
                                                                        ${document.id}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Tipo de Documento</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${document.documentType != null && document.documentType.equals('') == false}">
                                                                        ${document.documentType}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">T&iacute;tulo</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${document.title != null && document.title.equals('') == false}">
                                                                        ${document.title}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Descripci&oacute;n</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${document.description != null && document.description.equals('') == false}">
                                                                        ${document.description}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">URL</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${document.url != null && document.url.equals('') == false}">
                                                                        <a href="${(fn:contains(document.url, 'http')) ? '' : '//' }${document.url}" target="_blank">${document.url}</a>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Fecha de publicaci&oacute;n</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${document.datePublished != null && document.datePublished.equals('') == false}">
                                                                        ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(document.datePublished).toLowerCase()}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Fecha de modificaci&oacute;n</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${document.dateModified != null && document.dateModified.equals('') == false}">
                                                                        ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(document.dateModified).toLowerCase()}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Formato</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${document.format != null && document.format.equals('') == false}">
                                                                        ${document.format}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Idioma</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${document.language != null && document.language.equals('') == false}">
                                                                        ${document.language}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                            </dl>
                                                        </div>
                                                    </li>
                                                    <c:set var="contador" value="${contador+1}"></c:set>
                                                </c:forEach>
                                            </ul>
                                        </c:otherwise>
                                    </c:choose>
                                </fieldset>
                                <fieldset class="fieldset">
                                    <legend>Enmiendas</legend>
                                    <c:choose>
                                        <c:when test="${award.amendments == null || award.amendments.size() == 0}">
                                            Dato no proporcionado
                                        </c:when>
                                        <c:otherwise>
                                            <ul class="accordion" data-accordion data-allow-all-closed="true">
                                                <c:set var="contador" value="1"></c:set>
                                                <c:forEach items="${award.amendments}" var="amendment">
                                                    <li class="accordion-item ${ contador == 1 ? "is-active" : "" }" data-accordion-item>
                                                        <a href="#" class="accordion-title">Enmienda #${contador}</a>
                                                        <div class="accordion-content" data-tab-content>
                                                            <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                                                <dt class="cell callout primary">Fecha de enmienda</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${amendment.date != null && amendment.date.equals('') == false}">
                                                                        ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(amendment.date).toLowerCase()}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Justificaci&oacute;n</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${amendment.rationale != null && amendment.rationale.equals('') == false}">
                                                                        ${amendment.rationale}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">ID</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${amendment.id != null && amendment.id.equals('') == false}">
                                                                        ${amendment.id}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Descripci&oacute;n</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${amendment.description != null && amendment.description.equals('') == false}">
                                                                        ${amendment.description}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Entrega enmendada (identificador)</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${amendment.amendsReleaseID != null && amendment.amendsReleaseID.equals('') == false}">
                                                                        ${amendment.amendsReleaseID}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Entrega de enmienda (identificador)</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${amendment.releaseID != null && amendment.releaseID.equals('') == false}">
                                                                        ${amendment.releaseID}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                            </dl>
                                                        </div>
                                                    </li>
                                                    <c:set var="contador" value="${contador+1}"></c:set>
                                                </c:forEach>
                                            </ul>
                                        </c:otherwise>
                                    </c:choose>
                                </fieldset>
                            </div>
                        </li>
                        <c:set var="contadorPrincipal" value="${contadorPrincipal+1}"></c:set>
                    </c:forEach>
                </ul>
            </c:otherwise>
        </c:choose>
    </div>
</div>
