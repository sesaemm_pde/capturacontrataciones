<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fieldset class="fieldset">
    <legend>Art&iacute;culos Adjudicados</legend>
    <c:choose>
        <c:when test="${award.items == null || award.items.size() == 0}">
            Dato no proporcionado
        </c:when>
        <c:otherwise>
            <ul class="accordion" data-accordion data-allow-all-closed="true">
                <c:set var="contadorAwardsItems" value="1"></c:set>
                <c:forEach items="${award.items}" var="item">
                    <li class="accordion-item ${ contadorAwardsItems == 1 ? "is-active" : "" }" data-accordion-item>
                        <a href="#" class="accordion-title">Art&iacute;culo #${contadorAwardsItems}</a>
                        <div class="accordion-content" data-tab-content>
                            <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                <dt class="cell callout primary">ID</dt>
                                <dd class="cell callout">
                                <c:choose>
                                    <c:when test="${item.id != null}">
                                        ${item.id}
                                    </c:when>
                                    <c:otherwise>
                                        Dato no proporcionado
                                    </c:otherwise>            
                                </c:choose>
                                </dd>
                                <dt class="cell callout primary">Descripci&oacute;n</dt>
                                <dd class="cell callout">
                                <c:choose>
                                    <c:when test="${item.description != null && item.description.equals('') == false}">
                                        ${item.description}
                                    </c:when>
                                    <c:otherwise>
                                        Dato no proporcionado
                                    </c:otherwise>            
                                </c:choose>
                                </dd>
                            </dl> 
                            <fieldset class="fieldset">
                                <legend>Clasificaci&oacute;n</legend>
                                <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                    <dt class="cell callout primary">Esquema</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${item.classification.scheme != null && item.classification.scheme.equals('') == false}">
                                            ${item.classification.scheme}
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                    <dt class="cell callout primary">ID</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${item.classification.id != null}">
                                            ${item.classification.id}
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                    <dt class="cell callout primary">Descripci&oacute;n</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${item.classification.description != null && item.classification.description.equals('') == false}">
                                            ${item.classification.description}
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                    <dt class="cell callout primary">URI</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${item.classification.uri != null && item.classification.uri.equals('') == false}">
                                            <a href="${(fn:contains(item.classification.uri, 'http')) ? '' : '//' }${item.classification.uri}" target="_blank">${item.classification.uri}</a>
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                </dl>
                            </fieldset>
                            <fieldset class="fieldset">
                                <legend>Clasificaciones adicionales</legend>
                                <c:choose>
                                    <c:when test="${item.additionalClassifications == null || item.additionalClassifications.size() == 0}">
                                        Dato no proporcionado
                                    </c:when>
                                    <c:otherwise>
                                        <ul class="accordion" data-accordion data-allow-all-closed="true">
                                            <c:set var="contador" value="1"></c:set>
                                            <c:forEach items="${item.additionalClassifications}" var="additionalClassification">
                                                <li class="accordion-item ${ contador == 1 ? "is-active" : "" }" data-accordion-item>
                                                    <a href="#" class="accordion-title">Clasificaci&oacute;n #${contadorAwardsItems}</a>
                                                    <div class="accordion-content" data-tab-content>
                                                        <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                                            <dt class="cell callout primary">Esquema</dt>
                                                            <dd class="cell callout">
                                                            <c:choose>
                                                                <c:when test="${additionalClassification.scheme != null && additionalClassification.scheme.equals('') == false}">
                                                                    ${additionalClassification.scheme}
                                                                </c:when>
                                                                <c:otherwise>
                                                                    Dato no proporcionado
                                                                </c:otherwise>            
                                                            </c:choose>
                                                            </dd>
                                                            <dt class="cell callout primary">ID</dt>
                                                            <dd class="cell callout">
                                                            <c:choose>
                                                                <c:when test="${additionalClassification.id != null}">
                                                                    ${additionalClassification.id}
                                                                </c:when>
                                                                <c:otherwise>
                                                                    Dato no proporcionado
                                                                </c:otherwise>            
                                                            </c:choose>
                                                            </dd>
                                                            <dt class="cell callout primary">Descripci&oacute;n</dt>
                                                            <dd class="cell callout">
                                                            <c:choose>
                                                                <c:when test="${additionalClassification.description != null && additionalClassification.description.equals('') == false}">
                                                                    ${additionalClassification.description}
                                                                </c:when>
                                                                <c:otherwise>
                                                                    Dato no proporcionado
                                                                </c:otherwise>            
                                                            </c:choose>
                                                            </dd>
                                                            <dt class="cell callout primary">URI</dt>
                                                            <dd class="cell callout">
                                                            <c:choose>
                                                                <c:when test="${additionalClassification.uri != null && additionalClassification.uri.equals('') == false}">
                                                                    <a href="${(fn:contains(additionalClassification.uri, 'http')) ? '' : '//' }${additionalClassification.uri}" target="_blank">${additionalClassification.uri}</a>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    Dato no proporcionado
                                                                </c:otherwise>            
                                                            </c:choose>
                                                            </dd>
                                                        </dl>
                                                    </div>
                                                </li>
                                                <c:set var="contador" value="${contador+1}"></c:set>
                                            </c:forEach>
                                        </ul>
                                    </c:otherwise>
                                </c:choose>
                            </fieldset>
                            <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                <dt class="cell callout primary">Cantidad</dt>
                                <dd class="cell callout">
                                <c:choose>
                                    <c:when test="${item.quantity != null && item.quantity.equals('') == false}">
                                        ${item.quantity}
                                    </c:when>
                                    <c:otherwise>
                                        Dato no proporcionado
                                    </c:otherwise>            
                                </c:choose>
                                </dd>
                            </dl>
                            <fieldset class="fieldset">
                                <legend>Unidad</legend>
                                <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                    <dt class="cell callout primary">Esquema</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${item.unit.scheme != null && item.unit.scheme.equals('') == false}">
                                            ${item.unit.scheme}
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                    <dt class="cell callout primary">ID</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${item.unit.id != null && item.unit.id.equals('') == false}">
                                            ${item.unit.id}
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                    <dt class="cell callout primary">Nombre</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${item.unit.name != null && item.unit.name.equals('') == false}">
                                            ${item.unit.name}
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                    <dt class="cell callout primary">Valor</dt>
                                    <dd class="cell callout valorMonetario">
                                    <c:choose>
                                        <c:when test="${item.unit.value.amount != null}">
                                            &#36;${formateador.format(item.unit.value.amount)} 
                                            <c:choose> 
                                                <c:when test="${item.unit.value.currency != null && item.unit.value.currency.equals('') == false}">
                                                    ${item.unit.value.currency}
                                                </c:when>
                                                <c:otherwise>
                                                    (El tipo de moneda no fue proporcionado)
                                                </c:otherwise>
                                            </c:choose>
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd> 
                                    <dt class="cell callout primary">URI</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${item.unit.uri != null && item.unit.uri.equals('') == false}">
                                            <a href="${(fn:contains(item.unit.uri, 'http')) ? '' : '//' }${item.unit.uri}" target="_blank">${item.unit.uri}</a>
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                </dl>
                            </fieldset>
                        </div>
                    </li>
                    <c:set var="contadorAwardsItems" value="${contadorAwardsItems+1}"></c:set>
                </c:forEach>
            </ul>
        </c:otherwise>
    </c:choose>
</fieldset>