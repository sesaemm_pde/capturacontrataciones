<%--
    Autor: Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>


<%@include file="/encabezado.jsp" %>

<div class="orbit" role="region" aria-label="Hero PDE" data-orbit="data-orbit">
    <ul class="orbit-container">
        <li class="orbit-slide">
            <img class="orbit-image" src="framework/img/png/banner-pde-sis-min.png" alt="PDE"/>
        </li>
    </ul>
</div>

<%@include file="/migas.jsp" %>

<jsp:useBean id="now" class="java.util.Date"/>
<div class="grid-container display-top">
    <div class="grid-x">
        <div class="large-12 display-top">
            <h1>Plataforma Digital Estatal</h1>
            <h2>Sistema VI</h2>
            <h4>Informaci&oacute;n p&uacute;blica de contrataciones.</h4>
            <div id="cargaDB" class="reveal-overlay" style="display: none;">
              <div id="loader"></div>
              <div id="textDB">
                Actualizando base de datos...
              </div>
            </div>
            <div class="stacked-for-small expanded large button-group">
                <a class="button"><i class="material-icons md-4t">list</i> Listado de contrataciones</a>
                <c:if test="${nivelDeAcceso.id == 1 || nivelDeAcceso.id == 2}">
                    <a class="button hollow" onclick="document.frmNuevo.submit()"><i class="material-icons md-4t">playlist_add</i> Agregar nueva contrataci&oacute;n</a>
                    <form name="frmNuevo" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                        <input type="hidden" name="accion" value="mostrarPaginaRegistroNuevaContratacion"/>
                    </form>
                </c:if>
            </div>
            <form name="frmFiltroContratacionesPublicas" id="frmFiltroContratacionesPublicas" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post">
                <input type="hidden" name="accion" value="consultarContrataciones"/>
                <fieldset class="fieldset">
                    <legend>Busca una contrataci&oacute;n p&uacute;blica</legend>
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-4">
                            Ciclo 
                            <select name="cmbCiclo" id="cmbCiclo">
                                <fmt:formatDate var="anioActual" value="${now}" pattern="yyyy"/>
                                <c:set var="year" value="${anioActual+1}" scope="page"></c:set>
                                <c:set var="iterado" value="${anioActual}" scope="page"></c:set>
                                    <option value="">-Seleccione-</option>
                                <c:forEach  begin="2001" end="${year}" var="anio">
                                    <option value="${iterado}" ${iterado==filtros.ciclo ? 'selected' : ''}>${iterado}</option>
                                    <c:set var="iterado" value="${iterado-1}"></c:set>
                                </c:forEach>
                            </select>
                        </label>
                        <label class="cell medium-4">
                            Publicador
                            <input type="text" name="txtPublicador" id="txtPublicador" value="${filtros.publicador}" placeholder="p. ej. Secretar&iacute;a de ..."/>
                        </label>
                        <label class="cell medium-4">
                            Dependencia
                            <input type="text" name="txtDependencia" id="txtDependencia" value="${filtros.dependencia}" placeholder="p. ej. Gubernatura"/>
                        </label>
                    </div>
                    <div class="grid-margin-x grid-x">
                        <input type="submit" value="Filtrar" class="button cell expanded medium-6"/>                        
                        <input type="reset" id="btnReiniciar" value="Reiniciar" onclick="reiniciarValoresS6();" class="button cell expanded medium-6 secondary"/>
                    </div>                        
                </fieldset>
                <div class="grid-x grid-margin-x align-middle">
                    <div class="cell medium-4">
                        <label>Mostrando <code>${(contrataciones.size() > 0 ? 1 : 0) + (paginacion.registrosMostrar * (paginacion.numeroPagina - 1))}</code> a <code>${(paginacion.registrosMostrar * (paginacion.numeroPagina - 1)) + contrataciones.size()}</code> de <code>${paginacion.totalRegistros}</code> registros</label>
                    </div>
                    <div class="cell medium-4">
                        <label>Registros por p&aacute;gina
                            <select name="cmbPaginacion" id="cmbPaginacion" onchange="consultarContratacionesS6()">
                                <option value="10" ${paginacion.registrosMostrar == 10 ? 'selected': ''}>10</option>
                                <option value="25" ${paginacion.registrosMostrar == 25 ? 'selected': ''}>25</option>
                                <option value="50" ${paginacion.registrosMostrar == 50 ? 'selected': ''}>50</option>
                                <option value="100" ${paginacion.registrosMostrar == 100 ? 'selected': ''}>100</option>
                            </select>
                        </label>
                    </div>
                    <div class="cell medium-4">
                        <label id ="resultadosServidores">Ordenado por
                            <select name="cmbOrdenacion" id="cmbOrdenacion" onchange="consultarContratacionesS6()">
                                <option value="ciclo" ${filtros.orden == 'ciclo' ? 'selected' : '' }>Ciclo</option>
                                <option value="publicador" ${filtros.orden == 'publicador' ? 'selected' : '' }>Publicador</option>
                                <option value="dependencia" ${filtros.orden == 'dependencia' ? 'selected' : '' }>Dependencia</option>
                            </select>
                        </label>
                    </div>
                </div>
            </form>

            <h2>Contrataciones</h2>
            
            <table class="hover unstriped stack" id="listado">
                <thead>
                    <tr>
                        <th>Ciclo</th>
                        <th>Publicador</th>
                        <th>Dependencia</th>
                        <c:if test="${nivelDeAcceso.id == 5 || nivelDeAcceso.id == 1}">
                            <th>Usuario de captura</th>
                        </c:if>
                        <th>Estado</th>
                        <th style="text-align: center;">Detalle</th>
                        <c:if test="${nivelDeAcceso.id == 1 || nivelDeAcceso.id == 2}">
                            <th>Editar</th>
                            <th>Publicar</th>
                            <th>Ocultar</th>
                        </c:if>
                        <c:if test="${nivelDeAcceso.id == 1}">
                            <th>Borrar</th>
                        </c:if>
                    </tr>
                </thead>
                <tbody id="resultados">
                    <c:forEach items="${contrataciones}" var="contratacion">
                        <tr>
                            <td><strong class="hide-for-large">Ciclo: </strong> ${contratacion.ciclo}</td>
                            <td><strong class="hide-for-large">Publicador: </strong> ${contratacion.publicador}</td>
                            <td><strong class="hide-for-large">Dependencia: </strong> ${contratacion.dependencia}</td>
                            <c:if test="${nivelDeAcceso.id == 5 || nivelDeAcceso.id == 1}">
                                <td><strong class="hide-for-large">Usuario de captura:</strong> ${contratacion.usuarioCaptura}</td>
                            </c:if>
                            <td><strong class="hide-for-large">Estado </strong> ${(contratacion.contratacionCompleta == true) ? 'Completo' : 'Incompleto'}</td>
                            <td style="text-align: center;">
                                <form name="frm${contratacion.idRegistro}" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                                    <input type="hidden" name="accion" value="verDetalleDeContratacion"/>
                                    <input type="hidden" name="cmbCiclo" value="${filtros.ciclo}"/>
                                    <input type="hidden" name="txtPublicador" value="${filtros.publicador}"/>
                                    <input type="hidden" name="txtDependencia" value="${filtros.dependencia}"/>
                                    <input type="hidden" name="txtID" value="${contratacion.idRegistro}"/>
                                    <input type="hidden" name="cmbOrdenacion" value="${filtros.orden}"/>
                                    <input type="hidden" name="cmbPaginacion" value="${paginacion.registrosMostrar}"/>
                                    <input type="hidden" name="txtNumeroPagina" value="${paginacion.numeroPagina}"/>
                                    <a onclick="document.frm${contratacion.idRegistro}.submit()"><i class="material-icons">info</i> <span class="hide-for-large">Detalle</span></a>
                                </form>
                            </td>
                            <c:if test="${nivelDeAcceso.id == 1 || nivelDeAcceso.id == 2}">
                                <td>
                                    <form name="frmEditar${contratacion.idRegistro}" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                                        <c:choose>                                        
                                            <c:when test="${contratacion.publicar == 0}">
                                                <input type="hidden" name="accion" value="${(contratacion.contratacionCompleta == true) ? 'mostrarPaginaEditarContratacion' : 'mostrarPaginaRegistroNuevaContratacion'}"/>
                                                <input type="hidden" name="cmbCiclo" value="${filtros.ciclo}"/>
                                                <input type="hidden" name="txtPublicador" value="${filtros.publicador}"/>
                                                <input type="hidden" name="txtDependencia" value="${filtros.dependencia}"/>
                                                <input type="hidden" name="txtID" value="${contratacion.idRegistro}"/>
                                                <input type="hidden" name="cmbOrdenacion" value="${filtros.orden}"/>
                                                <input type="hidden" name="cmbPaginacion" value="${paginacion.registrosMostrar}"/>
                                                <input type="hidden" name="txtNumeroPagina" value="${paginacion.numeroPagina}"/>
                                                <input type="hidden" name="txtSeccionConFoco" value="1"/>
                                                <input type="hidden" name="txtBloqueConFoco" value="0"/>
                                                <a onclick="document.frmEditar${contratacion.idRegistro}.submit()"><i class="material-icons">edit</i> <span class="hide-for-large">Editar</span></a>
                                            </c:when>
                                            <c:otherwise>
                                                <a class="deshabilitado"><i class="material-icons">edit</i> <span class="hide-for-large">Editar</span></a>
                                            </c:otherwise>
                                        </c:choose>
                                    </form>
                                </td>
                                <td>
                                    <form name="frmPublicar${contratacion.idRegistro}" id="frmPublicar${contratacion.idRegistro}" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                                        <c:choose>
                                            <c:when test="${contratacion.publicar == 0}">
                                                <input type="hidden" name="accion" value="publicarContratacion"/>
                                                <input type="hidden" name="cmbCiclo" value="${filtros.ciclo}"/>
                                                <input type="hidden" name="txtPublicador" value="${filtros.publicador}"/>
                                                <input type="hidden" name="txtDependencia" value="${filtros.dependencia}"/>
                                                <input type="hidden" name="txtID" value="${contratacion.idRegistro}"/>
                                                <input type="hidden" name="cmbOrdenacion" value="${filtros.orden}"/>
                                                <input type="hidden" name="cmbPaginacion" value="${paginacion.registrosMostrar}"/>
                                                <input type="hidden" name="txtNumeroPagina" value="${paginacion.numeroPagina}"/>
                                                <c:choose>
                                                    <c:when test="${contratacion.contratacionCompleta == true}">
                                                        <a onclick="confirmar('frmPublicar${contratacion.idRegistro}', '\u00bfEst\u00e1 seguro de publicar el registro\u003F')"><i class="material-icons">public</i> <span class="hide-for-large">Publicar</span></a>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <a onclick="confirmar('frmPublicar${contratacion.idRegistro}', 'La contrataci\u00F3n p\u00FAblica no cuenta con la captura de toda su informaci\u00F3n. \u00bfEst\u00e1 seguro de publicar el registro\u003F')"><i class="material-icons">public</i> <span class="hide-for-large">Publicar</span></a>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:when>
                                            <c:otherwise>
                                                <a class="deshabilitado"><i class="material-icons">public</i> <span class="hide-for-large ">Publicar</span></a>
                                            </c:otherwise>
                                        </c:choose>
                                    </form>
                                </td>
                                <td>
                                    <form name="frmDespublicar${contratacion.idRegistro}" id="frmDespublicar${contratacion.idRegistro}" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                                        <c:choose>
                                            <c:when test="${contratacion.publicar != 0}">
                                                <input type="hidden" name="accion" value="despublicarContratacion"/>
                                                <input type="hidden" name="cmbCiclo" value="${filtros.ciclo}"/>
                                                <input type="hidden" name="txtPublicador" value="${filtros.publicador}"/>
                                                <input type="hidden" name="txtDependencia" value="${filtros.dependencia}"/>
                                                <input type="hidden" name="txtID" value="${contratacion.idRegistro}"/>
                                                <input type="hidden" name="cmbOrdenacion" value="${filtros.orden}"/>
                                                <input type="hidden" name="cmbPaginacion" value="${paginacion.registrosMostrar}"/>
                                                <input type="hidden" name="txtNumeroPagina" value="${paginacion.numeroPagina}"/>
                                                <a onclick="confirmar('frmDespublicar${contratacion.idRegistro}', '\u00bfEst\u00e1 seguro de ocultar el registro\u003F')"><i class="material-icons">visibility_off</i> <span class="hide-for-large">Desactivar</span></a>
                                            </c:when>
                                            <c:otherwise>
                                                <a class="deshabilitado"><i class="material-icons">visibility_off</i> <span class="hide-for-large ">Ocultar</span></a>
                                            </c:otherwise>
                                        </c:choose>
                                    </form>
                                </td>
                            </c:if>
                            <c:if test="${nivelDeAcceso.id == 1}">
                                <td>
                                    <form name="frmEliminar${contratacion.idRegistro}" id="frmEliminar${contratacion.idRegistro}" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                                        <c:choose>
                                            <c:when test="${contratacion.publicar == 0}">
                                                <input type="hidden" name="accion" value="eliminarContratacion"/>
                                                <input type="hidden" name="cmbCiclo" value="${filtros.ciclo}"/>
                                                <input type="hidden" name="txtPublicador" value="${filtros.publicador}"/>
                                                <input type="hidden" name="txtDependencia" value="${filtros.dependencia}"/>
                                                <input type="hidden" name="txtID" value="${contratacion.idRegistro}"/>
                                                <input type="hidden" name="cmbOrdenacion" value="${filtros.orden}"/>
                                                <input type="hidden" name="cmbPaginacion" value="${paginacion.registrosMostrar}"/>
                                                <input type="hidden" name="txtNumeroPagina" value="${paginacion.numeroPagina}"/>
                                                <a onclick="confirmar('frmEliminar${contratacion.idRegistro}', '\u00bfEst\u00e1 seguro de eliminar el registro\u003F')"><i class="material-icons">delete</i> <span class="hide-for-large">Borrar</span></a>
                                            </c:when>
                                            <c:otherwise>
                                                <a class="deshabilitado"><i class="material-icons">delete</i> <span class="hide-for-large">Borrar</span></a>
                                            </c:otherwise>
                                        </c:choose>
                                    </form>
                                </td>
                            </c:if>
                        </tr>
                    </c:forEach>
                    <c:if test="${contrataciones.size() == 0}">
                        <tr>
                            <td colspan="100%" style="text-align: center;">Sin resultados</td>
                        </tr>
                    </c:if>
                </tbody>
            </table>
            
            <c:if test="${paginacion.isEmpty() == false}">
                <p class="text-center">Mostrando <strong>${(contrataciones.size() > 0 ? 1 : 0) + (paginacion.registrosMostrar * (paginacion.numeroPagina - 1))}</strong> a <strong>${(paginacion.registrosMostrar * (paginacion.numeroPagina - 1)) + contrataciones.size()}</strong> de <strong>${paginacion.totalRegistros}</strong> registros</p>
                <c:if test="${contrataciones != null}">
                    <nav aria-label="Pagination">
                        <ul class="pagination text-center">
                            <c:set var="totalPaginas" value="${paginacion.totalPaginas}"></c:set>
                            <c:set var="numeroPaginaActual" value="${paginacion.numeroPagina}"></c:set>
                            <c:set var="numeroPaginas" value="${paginacion.numeroPagina}"></c:set>
                            <c:set var="totalRegistros" value="${paginacion.totalRegistros}"></c:set>
                            <c:if test="${numeroPaginas > 1 }">
                                <li class="pagination-previous">
                                    <a onclick="frmAnterior.submit()" aria-label="P&aacute;gina anterior">Anterior</a>
                                </li>
                                <c:choose>
                                    <c:when test="${numeroPaginaActual == 1}">
                                        <li class="current">
                                            1
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li>
                                            <a onclick="frm1.submit()" aria-label="P&aacute;gina 1">1</a>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                                <c:if test="${numeroPaginas != 2}">
                                    <li class="ellipsis">
                                    </li>
                                </c:if>
                            </c:if>
                            <c:set var="limiteSuperior" value="${(totalPaginas - numeroPaginas < 3 ? totalPaginas : numeroPaginas + 3)}"></c:set>
                            <c:forEach begin="${numeroPaginas}" end="${limiteSuperior -1}" >
                                <c:choose>
                                    <c:when test="${numeroPaginaActual == numeroPaginas}">
                                        <li class="current">
                                            ${numeroPaginas}
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li>
                                            <a onclick="frm${numeroPaginas}.submit()" aria-label="P&aacute;gina ${numeroPaginas}">
                                                ${numeroPaginas}
                                            </a>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                                <c:set var="numeroPaginas" value="${numeroPaginas + 1 }"></c:set>
                            </c:forEach>
                            <c:if test="${numeroPaginaActual <= totalPaginas }">
                                <c:if test="${numeroPaginas < totalPaginas}">
                                    <li class="ellipsis">
                                    </li>
                                </c:if>
                                <c:choose>
                                    <c:when test="${numeroPaginaActual == numeroPaginas}">
                                        <li class="current">
                                            ${totalPaginas}
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li>
                                            <a onclick="frm${numeroPaginas}.submit()" aria-label="P&aacute;gina ${totalPaginas}">${totalPaginas}</a>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                            <c:if test="${numeroPaginaActual < totalPaginas }">
                                <li class="pagination-next">
                                    <a onclick="frmSiguiente.submit();" aria-label="P&aacute;gina siguiente">Siguiente</a>
                                </li>
                            </c:if>
                        </ul>
                    </nav>

                    <c:set var="totalPaginas" value="${paginacion.totalPaginas}"></c:set>
                    <c:set var="numeroPaginaActual" value="${paginacion.numeroPagina}"></c:set>
                    <c:set var="numeroPaginas" value="${paginacion.numeroPagina}"></c:set>
                    <c:set var="totalRegistros" value="${paginacion.totalRegistros}"></c:set>
                    <c:if test="${numeroPaginas > 1 }">
                        <form name="frmAnterior" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post">
                            <input type="hidden" name="accion" value="consultarContrataciones"/>
                            <input type="hidden" name="txtOrigen" value="contrataciones"/>
                            <input type="hidden" name="cmbPaginacion" value="${paginacion.registrosMostrar}"/>
                            <input type="hidden" name="txtNumeroPagina" value="${numeroPaginas - 1}"/>
                            <input type="hidden" name="cmbOrdenacion" value="${filtros.orden}"/>
                            <input type="hidden" name="cmbCiclo" value="${filtros.ciclo}"/>
                            <input type="hidden" name="txtPublicador" value="${filtros.publicador}"/>
                            <input type="hidden" name="txtDependencia" value="${filtros.dependencia}"/>
                        </form>
                        <form name="frm1" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post">
                            <input type="hidden" name="accion" value="consultarContrataciones"/>
                            <input type="hidden" name="txtOrigen" value="contrataciones"/>
                            <input type="hidden" name="cmbPaginacion" value="${paginacion.registrosMostrar}"/>
                            <input type="hidden" name="txtNumeroPagina" value="1"/>
                            <input type="hidden" name="cmbOrdenacion" value="${filtros.orden}"/>
                            <input type="hidden" name="cmbCiclo" value="${filtros.ciclo}"/>
                            <input type="hidden" name="txtPublicador" value="${filtros.publicador}"/>
                            <input type="hidden" name="txtDependencia" value="${filtros.dependencia}"/>
                        </form>
                    </c:if>
                    <c:set var="limiteSuperior" value="${(totalPaginas - numeroPaginas < 3 ? totalPaginas : numeroPaginas + 3)}"></c:set>
                    <c:forEach begin="${numeroPaginas}" end="${limiteSuperior -1}" >
                        <form name="frm${numeroPaginas}" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post">
                            <input type="hidden" name="accion" value="consultarContrataciones"/>
                            <input type="hidden" name="txtOrigen" value="contrataciones"/>
                            <input type="hidden" name="cmbPaginacion" value="${paginacion.registrosMostrar}"/>
                            <input type="hidden" name="txtNumeroPagina" value="${numeroPaginas}"/>
                            <input type="hidden" name="cmbOrdenacion" value="${filtros.orden}"/>
                            <input type="hidden" name="cmbCiclo" value="${filtros.ciclo}"/>
                            <input type="hidden" name="txtPublicador" value="${filtros.publicador}"/>
                            <input type="hidden" name="txtDependencia" value="${filtros.dependencia}"/>
                        </form>
                        <c:set var="numeroPaginas" value="${numeroPaginas + 1 }"></c:set>
                    </c:forEach>
                    <c:if test="${numeroPaginaActual <= totalPaginas }">
                        <form name="frm${numeroPaginas}" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post">
                            <input type="hidden" name="accion" value="consultarContrataciones"/>
                            <input type="hidden" name="txtOrigen" value="contrataciones"/>
                            <input type="hidden" name="cmbPaginacion" value="${paginacion.registrosMostrar}"/>
                            <input type="hidden" name="txtNumeroPagina" value="${totalPaginas}"/>
                            <input type="hidden" name="cmbOrdenacion" value="${filtros.orden}"/>
                            <input type="hidden" name="cmbCiclo" value="${filtros.ciclo}"/>
                            <input type="hidden" name="txtPublicador" value="${filtros.publicador}"/>
                            <input type="hidden" name="txtDependencia" value="${filtros.dependencia}"/>
                        </form>
                    </c:if>
                    <c:if test="${numeroPaginaActual < totalPaginas }">
                        <form name="frmSiguiente" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post">
                            <input type="hidden" name="accion" value="consultarContrataciones"/>
                            <input type="hidden" name="txtOrigen" value="contrataciones"/>
                            <input type="hidden" name="cmbPaginacion" value="${paginacion.registrosMostrar}"/>
                            <input type="hidden" name="txtNumeroPagina" value="${numeroPaginaActual + 1}"/>
                            <input type="hidden" name="cmbOrdenacion" value="${filtros.orden}"/>
                            <input type="hidden" name="cmbCiclo" value="${filtros.ciclo}"/>
                            <input type="hidden" name="txtPublicador" value="${filtros.publicador}"/>
                            <input type="hidden" name="txtDependencia" value="${filtros.dependencia}"/>
                        </form>
                    </c:if>
                </c:if>
            </c:if>
        </div>
    </div>
</div>
                                
<c:if test="${irASeccion == true}">
    <script>
        var loc = window.location.href;
        window.location.href = loc + "#resultadosServidores";
    </script>
</c:if>

<%@include file="/piePagina.jsp" %>

<c:if test="${mostrarMensaje == true}">
    <script>
        var json = ${mensaje};
        swal(json.mensaje, {
            icon: json.tipo,
            buttons: {
                confirm: {
                text: "OK",
                value: true,
                visible: true,
                className: "",
                closeModal: true
                }
            }
        });
    </script>
</c:if>