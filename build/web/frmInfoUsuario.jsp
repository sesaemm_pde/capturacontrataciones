<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<div class="dropdown-pane bottom" id="dropdown-user-details" data-dropdown="data-dropdown" data-closable>
    <button class="close-button" aria-label="Close alert" type="button" data-close>
        <span aria-hidden="true">&times;</span>
    </button>
    <h6 class="small">Informaci&oacute;n personal</h6>
    <small>
        <i class="material-icons md-1t">person</i> ${usuario.nombre} ${usuario.primer_apellido}  ${usuario.segundo_apellido}<br/>
        <i class="material-icons md-1t">work</i> ${usuario.puesto}<br/>
        <i class="material-icons md-1t">business</i> ${usuario.dependencia.valor}
    </small>
    <hr/>
    <h6>Datos de contacto</h6>
    <small>
        <i class="material-icons md-1t">place</i> ${usuario.domicilio.vialidad.nom_vial} ${usuario.domicilio.numExt},  C.P. ${usuario.domicilio.cp}, ${usuario.domicilio.municipio.nom_mun}, ${usuario.domicilio.entidad_federativa.nom_ent} <br/>
        <i class="material-icons md-1t">mail</i> ${usuario.correo_electronico}<br/>
        <i class="material-icons md-1t">phone</i> ${usuario.telefono}
    </small>
</div>