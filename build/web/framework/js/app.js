Foundation.Abide.defaults.patterns['curp'] = /^[A-Za-z]{1}[AEIOUaeiou]{1}[A-Za-z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HMhm]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE|as|bc|bs|cc|cs|ch|cl|cm|df|dg|gt|gr|hg|jc|mc|mn|ms|nt|nl|oc|pl|qt|qr|sp|sl|sr|tc|ts|tl|vz|yn|zs|ne)[B-DF-HJ-NP-TV-Zb-df-hj-np-tv-z]{3}[0-9A-Za-z]{1}[0-9]{1}$/; 

Foundation.Abide.defaults.patterns['rfc_pf'] = /^[A-Za-z]{1}[AEIOUaeiou]{1}[A-Za-z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Za-z0-9]{3}$|^[A-Za-z]{1}[AEIOUaeiou]{1}[A-Za-z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])$/; 

Foundation.Abide.defaults.patterns['rfc_pm'] = /^[A-Za-z]{3}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Za-z0-9]{3}$/; 

Foundation.Abide.defaults.patterns['year'] = /^(198|199|200|201|202|203|204|205|206|207)\d{1}$/; 

Foundation.Abide.defaults.patterns['url'] = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/; 

Foundation.Abide.defaults.patterns['codigo_postal'] = /^((?!(0))[0-9]{5})$/;

Foundation.Abide.defaults.patterns['telefono'] = /^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/;

Foundation.Abide.defaults.patterns['solo_enteros'] = /^\d+$/;

Foundation.Abide.defaults.patterns['fecha'] = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;

Foundation.Abide.defaults.patterns['rationale_s6'] = /^(rationale_(((([A-Za-z]{2,3}(-([A-Za-z]{3}(-[A-Za-z]{3}){0,2}))?)|[A-Za-z]{4}|[A-Za-z]{5,8})(-([A-Za-z]{4}))?(-([A-Za-z]{2}|[0-9]{3}))?(-([A-Za-z0-9]{5,8}|[0-9][A-Za-z0-9]{3}))*(-([0-9A-WY-Za-wy-z](-[A-Za-z0-9]{2,8})+))*(-(x(-[A-Za-z0-9]{1,8})+))?)|(x(-[A-Za-z0-9]{1,8})+)))$/;
Foundation.Abide.defaults.patterns['title_s6'] = /^(title_(((([A-Za-z]{2,3}(-([A-Za-z]{3}(-[A-Za-z]{3}){0,2}))?)|[A-Za-z]{4}|[A-Za-z]{5,8})(-([A-Za-z]{4}))?(-([A-Za-z]{2}|[0-9]{3}))?(-([A-Za-z0-9]{5,8}|[0-9][A-Za-z0-9]{3}))*(-([0-9A-WY-Za-wy-z](-[A-Za-z0-9]{2,8})+))*(-(x(-[A-Za-z0-9]{1,8})+))?)|(x(-[A-Za-z0-9]{1,8})+)))$/;
Foundation.Abide.defaults.patterns['description_s6'] = /^(description_(((([A-Za-z]{2,3}(-([A-Za-z]{3}(-[A-Za-z]{3}){0,2}))?)|[A-Za-z]{4}|[A-Za-z]{5,8})(-([A-Za-z]{4}))?(-([A-Za-z]{2}|[0-9]{3}))?(-([A-Za-z0-9]{5,8}|[0-9][A-Za-z0-9]{3}))*(-([0-9A-WY-Za-wy-z](-[A-Za-z0-9]{2,8})+))*(-(x(-[A-Za-z0-9]{1,8})+))?)|(x(-[A-Za-z0-9]{1,8})+)))$/;
Foundation.Abide.defaults.patterns['procurementMethodRationale_s6'] = /^(procurementMethodRationale_(((([A-Za-z]{2,3}(-([A-Za-z]{3}(-[A-Za-z]{3}){0,2}))?)|[A-Za-z]{4}|[A-Za-z]{5,8})(-([A-Za-z]{4}))?(-([A-Za-z]{2}|[0-9]{3}))?(-([A-Za-z0-9]{5,8}|[0-9][A-Za-z0-9]{3}))*(-([0-9A-WY-Za-wy-z](-[A-Za-z0-9]{2,8})+))*(-(x(-[A-Za-z0-9]{1,8})+))?)|(x(-[A-Za-z0-9]{1,8})+)))$/;
Foundation.Abide.defaults.patterns['awardCriteriaDetails_s6'] = /^(awardCriteriaDetails_(((([A-Za-z]{2,3}(-([A-Za-z]{3}(-[A-Za-z]{3}){0,2}))?)|[A-Za-z]{4}|[A-Za-z]{5,8})(-([A-Za-z]{4}))?(-([A-Za-z]{2}|[0-9]{3}))?(-([A-Za-z0-9]{5,8}|[0-9][A-Za-z0-9]{3}))*(-([0-9A-WY-Za-wy-z](-[A-Za-z0-9]{2,8})+))*(-(x(-[A-Za-z0-9]{1,8})+))?)|(x(-[A-Za-z0-9]{1,8})+)))$/;
Foundation.Abide.defaults.patterns['submissionMethodDetails_s6'] = /^(submissionMethodDetails_(((([A-Za-z]{2,3}(-([A-Za-z]{3}(-[A-Za-z]{3}){0,2}))?)|[A-Za-z]{4}|[A-Za-z]{5,8})(-([A-Za-z]{4}))?(-([A-Za-z]{2}|[0-9]{3}))?(-([A-Za-z0-9]{5,8}|[0-9][A-Za-z0-9]{3}))*(-([0-9A-WY-Za-wy-z](-[A-Za-z0-9]{2,8})+))*(-(x(-[A-Za-z0-9]{1,8})+))?)|(x(-[A-Za-z0-9]{1,8})+)))$/;
Foundation.Abide.defaults.patterns['eligibilityCriteria_s6'] = /^(eligibilityCriteria_(((([A-Za-z]{2,3}(-([A-Za-z]{3}(-[A-Za-z]{3}){0,2}))?)|[A-Za-z]{4}|[A-Za-z]{5,8})(-([A-Za-z]{4}))?(-([A-Za-z]{2}|[0-9]{3}))?(-([A-Za-z0-9]{5,8}|[0-9][A-Za-z0-9]{3}))*(-([0-9A-WY-Za-wy-z](-[A-Za-z0-9]{2,8})+))*(-(x(-[A-Za-z0-9]{1,8})+))?)|(x(-[A-Za-z0-9]{1,8})+)))$/;
Foundation.Abide.defaults.patterns['source_s6'] = /^(source_(((([A-Za-z]{2,3}(-([A-Za-z]{3}(-[A-Za-z]{3}){0,2}))?)|[A-Za-z]{4}|[A-Za-z]{5,8})(-([A-Za-z]{4}))?(-([A-Za-z]{2}|[0-9]{3}))?(-([A-Za-z0-9]{5,8}|[0-9][A-Za-z0-9]{3}))*(-([0-9A-WY-Za-wy-z](-[A-Za-z0-9]{2,8})+))*(-(x(-[A-Za-z0-9]{1,8})+))?)|(x(-[A-Za-z0-9]{1,8})+)))$/;
Foundation.Abide.defaults.patterns['name_s6'] = /^(name_(((([A-Za-z]{2,3}(-([A-Za-z]{3}(-[A-Za-z]{3}){0,2}))?)|[A-Za-z]{4}|[A-Za-z]{5,8})(-([A-Za-z]{4}))?(-([A-Za-z]{2}|[0-9]{3}))?(-([A-Za-z0-9]{5,8}|[0-9][A-Za-z0-9]{3}))*(-([0-9A-WY-Za-wy-z](-[A-Za-z0-9]{2,8})+))*(-(x(-[A-Za-z0-9]{1,8})+))?)|(x(-[A-Za-z0-9]{1,8})+)))$/;
Foundation.Abide.defaults.patterns['legalName_s6'] = /^(legalName_(((([A-Za-z]{2,3}(-([A-Za-z]{3}(-[A-Za-z]{3}){0,2}))?)|[A-Za-z]{4}|[A-Za-z]{5,8})(-([A-Za-z]{4}))?(-([A-Za-z]{2}|[0-9]{3}))?(-([A-Za-z0-9]{5,8}|[0-9][A-Za-z0-9]{3}))*(-([0-9A-WY-Za-wy-z](-[A-Za-z0-9]{2,8})+))*(-(x(-[A-Za-z0-9]{1,8})+))?)|(x(-[A-Za-z0-9]{1,8})+)))$/;
Foundation.Abide.defaults.patterns['countryName_s6'] = /^(countryName_(((([A-Za-z]{2,3}(-([A-Za-z]{3}(-[A-Za-z]{3}){0,2}))?)|[A-Za-z]{4}|[A-Za-z]{5,8})(-([A-Za-z]{4}))?(-([A-Za-z]{2}|[0-9]{3}))?(-([A-Za-z0-9]{5,8}|[0-9][A-Za-z0-9]{3}))*(-([0-9A-WY-Za-wy-z](-[A-Za-z0-9]{2,8})+))*(-(x(-[A-Za-z0-9]{1,8})+))?)|(x(-[A-Za-z0-9]{1,8})+)))$/;
Foundation.Abide.defaults.patterns['project_s6'] = /^(project_(((([A-Za-z]{2,3}(-([A-Za-z]{3}(-[A-Za-z]{3}){0,2}))?)|[A-Za-z]{4}|[A-Za-z]{5,8})(-([A-Za-z]{4}))?(-([A-Za-z]{2}|[0-9]{3}))?(-([A-Za-z0-9]{5,8}|[0-9][A-Za-z0-9]{3}))*(-([0-9A-WY-Za-wy-z](-[A-Za-z0-9]{2,8})+))*(-(x(-[A-Za-z0-9]{1,8})+))?)|(x(-[A-Za-z0-9]{1,8})+)))$/;


$(document).foundation(); 

setTimeout(function ()
    {
        $('a[target="_blank"]').each(function ()
            {
                var a = $(this).html();
                0 == a.includes('<i class') && $(this).append('<i class="material-icons md-1t">exit_to_app</i>')
            })
    }, 1e3), window.setInterval(function ()
    {
        $('a[target="_blank"]').each(function ()
            {
                var a = $(this).html();
                0 == a.includes('<i class') && $(this).append('<i class="material-icons md-1t">exit_to_app</i>')
            })
    }, 1e3);

function reiniciarValoresS6()
{ 
    document.getElementById('cmbCiclo').setAttribute('value', '');
    document.getElementById('txtPublicador').setAttribute('value', '');
    document.getElementById('txtDependencia').setAttribute('value', '');
} 

function consultarContratacionesS6()
{ 
    var form = document.createElement("form");
    form.method = "POST";
    form.action = document.getElementById("frmFiltroContratacionesPublicas").action;

    var accion = document.createElement("input");
    accion.value = 'consultarContrataciones';
    accion.name = "accion";
    accion.type = "hidden";
    form.appendChild(accion);

    var cmbPaginacion = document.createElement("input");
    cmbPaginacion.value = document.getElementById("cmbPaginacion").value;
    cmbPaginacion.name = "cmbPaginacion";
    cmbPaginacion.type = "hidden";
    form.appendChild(cmbPaginacion);

    var cmbOrdenacion = document.createElement("input");
    cmbOrdenacion.value = document.getElementById("cmbOrdenacion").value;
    cmbOrdenacion.name = "cmbOrdenacion";
    cmbOrdenacion.type = "hidden";
    form.appendChild(cmbOrdenacion);
    

    var cmbCiclo = document.createElement("input");
    cmbCiclo.value = document.getElementById("cmbCiclo").value;
    cmbCiclo.name = "cmbCiclo";
    cmbCiclo.type = "hidden";
    form.appendChild(cmbCiclo);

    var txtPublicador = document.createElement("input");
    txtPublicador.value = document.getElementById("txtPublicador").value;
    txtPublicador.name = "txtPublicador";
    txtPublicador.type = "hidden";
    form.appendChild(txtPublicador);

    var txtDependencia = document.createElement("input");
    txtDependencia.value = document.getElementById("txtDependencia").value;
    txtDependencia.name = "txtDependencia";
    txtDependencia.type = "hidden";
    form.appendChild(txtDependencia);

    document.body.appendChild(form);
    form.submit();
} 

function existeUrl(url)
    {
        $.get(url)
                .done(function ()
                    {
                        return true;
                    })
                .fail(function ()
                    {
                        var form = document.createElement("form");
                        form.method = "POST";
                        form.action = "/pde";

                        var accion = document.createElement("input");
                        accion.value = 'noFound';
                        accion.name = "accion";
                        accion.type = "hidden";
                        form.appendChild(accion);

                        document.body.appendChild(form);
                        form.submit();
                        return false;
                    });

    }

function confirmar(id, mensaje) {

  swal({
      title: "Confirmar",
      text: mensaje,
      icon: "warning",
      buttons: ["Cancelar", "Aceptar"],
      dangerMode: true,
      closeOnClickOutside: false,
      closeOnEsc: false,
    })
    .then((seAcepta) => {
      if (seAcepta) {
          mostrarLoader();
        document.getElementById(id).submit();
      } else {
        swal("Se ha cancelado la operaci\u00f3n", {
          icon: "error",
          buttons: false,
          timer: 2000,
        });
      }
    });
}

var myVar;

function mostrarLoader() {
    document.getElementById("cargaDB").style.display = "block"; 
}

const abrirNuevoFormulario = (strNombreSeccion, arregloDimensiones) =>
{ 

    let siguienteIdentificador;
    let nombreModal;
    let datosElemento;
    let identificadorDatosElemento = "";
    let inputIdentificadorAUtilizar;
    let contenedorBtnAgregarElemento;
    let contenedorBtnCerrarElemento;

    datosElemento = obtenerDatosFormularioS6(strNombreSeccion);

    if(arregloDimensiones && arregloDimensiones.length >= 1)
    { 
        arregloDimensiones.map(nivel => {
            identificadorDatosElemento += `_${nivel}`;
        });
    } 

    siguienteIdentificador = document.getElementById("siguiente-identificador-" + strNombreSeccion + identificadorDatosElemento);
    inputIdentificadorAUtilizar = document.getElementById("identificador-utilizado-" + strNombreSeccion);
    contenedorBtnAgregarElemento = document.getElementById(datosElemento.btnAgregarElementoFormulario);
    contenedorBtnCerrarElemento = document.getElementById(datosElemento.btnCerrarElementoFormulario);

    nombreModal = datosElemento.modal;
    inputIdentificadorAUtilizar.value = siguienteIdentificador.value;

    contenedorBtnAgregarElemento.innerHTML = `<input type="button" onclick="agregarElemento('${strNombreSeccion}', [${arregloDimensiones}])" class="button expanded" value="Agregar"/>`;
    contenedorBtnCerrarElemento.innerHTML = `<input type="button" onclick="cerrarFormulario('${strNombreSeccion}')" class="button expanded secondary" value="Cerrar"/>`;
    
    
    $('#' + nombreModal).foundation('open');

} 

const abrirFormularioPrevio = (strNombreSeccion, arregloDimensiones) =>
{ 

    let nombreModal;
    let datosElemento;
    let identificadorDatosElemento = "";
    let contenedorBtnAgregarElemento;
    let contenedorBtnCerrarElemento;
    let campo;
    let campoPrincipal;
    let campoFormulario;
    let tipoDeCampo;
    let valorCampo;

    datosElemento = obtenerDatosFormularioS6(strNombreSeccion);

    if(arregloDimensiones && arregloDimensiones.length >= 1)
    { 
        arregloDimensiones.map(nivel =>{
            identificadorDatosElemento += `_${nivel}`;
        });
    } 

    datosElemento.campos.map(dato => {

        let arregloValoresSeleccionadosAux;
        let arregloValoresSeleccionados = [];

        tipoDeCampo = dato.tipo;
        campo = `${dato.id}${identificadorDatosElemento}`;

        if(tipoDeCampo === "text" || tipoDeCampo === "select")
        { 

            campoPrincipal = document.getElementById(campo);
            campoFormulario = document.getElementById(dato.id);

            if(campoFormulario && campoPrincipal)
            { 
                valorCampo = campoPrincipal.value;
                campoFormulario.value = valorCampo;
            } 

        } 
        else if(tipoDeCampo === "radio")
        { 

            campoPrincipal = document.getElementById(campo);
            campoFormulario = document.getElementsByName(dato.id);
            
            if(campoFormulario && campoPrincipal)
            { 
                
                valorCampo = campoPrincipal.value;

                for(let i = 0; i < campoFormulario.length; i++)
                { 
                    if(campoFormulario[i].value === valorCampo)
                    { 
                        campoFormulario[i].checked = true;
                    } 
                } 

            } 

        } 
        else if(tipoDeCampo === "checkbox")
        { 

            campoPrincipal = document.getElementById(campo);
            campoFormulario = document.getElementsByName(dato.id);

            if(campoFormulario && campoPrincipal)
            { 

                valorCampo = campoPrincipal.value;

                if(valorCampo.includes(",") === true)
                { 
                    arregloValoresSeleccionadosAux = valorCampo.split(",");
                    arregloValoresSeleccionadosAux.map(elemento => {
                        if(elemento.trim() !== "")
                        {
                            arregloValoresSeleccionados.push(elemento.trim());
                        }
                    });
                } 
                else if(valorCampo !== "")
                { 
                    arregloValoresSeleccionados.push(arregloValoresSeleccionados.trim());
                } 
                
                for(let i = 0; i < campoFormulario.length; i++)
                { 
                    for(let j = 0; j < arregloValoresSeleccionados.length; j++)
                    { 
                        if(campoFormulario[i].value === arregloValoresSeleccionados[j])
                        { 
                            campoFormulario[i].checked = true;
                        } 
                    } 
                } 

            } 

        } 

    });

    contenedorBtnAgregarElemento = document.getElementById(datosElemento.btnAgregarElementoFormulario);
    contenedorBtnCerrarElemento = document.getElementById(datosElemento.btnCerrarElementoFormulario);

    nombreModal = datosElemento.modal;

    contenedorBtnAgregarElemento.innerHTML = `<input type="button" onclick="actualizarElemento('${strNombreSeccion}', [${arregloDimensiones}], 'datos-elemento-${strNombreSeccion}${identificadorDatosElemento}')" class="button expanded" value="Guardar"/>`;
    contenedorBtnCerrarElemento.innerHTML = `<input type="button" onclick="cerrarFormulario('${strNombreSeccion}')" class="button expanded secondary" value="Cerrar"/>`;
    

    
    $('#' + nombreModal).foundation('open');

} 

const cerrarFormulario = (strNombreSeccion) =>
{ 

    let nombreModal;
    let datosElemento;
    let contenedorBtnAgregarElemento;
    let contenedorBtnCerrarElemento;
    let valorCampo;
    let formulario;
 
    datosElemento = obtenerDatosFormularioS6(strNombreSeccion);

    contenedorBtnAgregarElemento = document.getElementById(datosElemento.btnAgregarElementoFormulario);
    contenedorBtnCerrarElemento = document.getElementById(datosElemento.btnCerrarElementoFormulario);

    nombreModal = datosElemento.modal;
    formulario = datosElemento.formulario;
    contenedorBtnAgregarElemento.innerHTML = "";
    contenedorBtnCerrarElemento.innerHTML = "";
    
    
    datosElemento.campos.map(campo => {
                    
        valorCampo = document.getElementById(campo.id);

        if(valorCampo)
        { 
            valorCampo.value = "";
        } 

    });
    

    
    $('#' + nombreModal).foundation('close');
    $('#' + formulario).foundation('resetForm');
    Foundation.reInit("abide"); 

} 

const agregarElemento = (strNombreSeccion, arregloDimensiones) =>
{ 
    
    let inputIdentificadorAUtilizar;
    let datosElemento;
    let formulario;
    let inputClavePrincipal;
    let clavePrincipal;
    let identificadorDatosElemento = "";
    let listaIdentificadoresPorRegistrar;

    if(arregloDimensiones && arregloDimensiones.length >= 1)
    { 
        arregloDimensiones.map(nivel => {
            identificadorDatosElemento += `_${nivel}`;
        });
    } 

    datosElemento = obtenerDatosFormularioS6(strNombreSeccion);

    inputClavePrincipal = document.getElementById(datosElemento.idPrincipal);
    clavePrincipal = inputClavePrincipal.value;

    inputIdentificadorAUtilizar = document.getElementById("identificador-utilizado-" + strNombreSeccion);
    listaIdentificadoresPorRegistrar = document.getElementById("lista-identificadores-por-registrar-" + strNombreSeccion + identificadorDatosElemento);
                
    if(validarClavesPrincipales(datosElemento.idPrincipal, clavePrincipal, identificadorDatosElemento, listaIdentificadoresPorRegistrar.value, inputIdentificadorAUtilizar.value) === false)
    {
        swal({
            title: "Identificador principal repetido",
            text: "El valor ingresado para el identificador principal ya se encuentra en uso.\n\nFavor de ingresar un identificador diferente.",
            icon: "error",
            button: 'ok'
        });
        inputClavePrincipal.value = "";
        return false;
    }
    else
    {

        formulario = datosElemento.formulario;

        $('#'+formulario).foundation('validateForm');
        $("#"+formulario).on("formvalid.zf.abide", function (ev, frm){
            swal({
                title: "Confirmar",
                text: "\u00bf Los datos son correctos\u003F",
                icon: "warning",
                buttons: ["Cancelar", "Aceptar"],
                dangerMode: true,
                closeOnClickOutside: false,
                closeOnEsc: false
            })
            .then((seAcepta) => {
                if (seAcepta)
                { 
                    
                    let estructuraHTML;
                    let siguienteIdentificador;
                    let contenedorElementos;
                    let tipoDeEtiqueta;
                    let elementosHijo;

                    tipoDeEtiqueta = datosElemento.tipoDeEtiqueta;
                    elementosHijo = datosElemento.elementosHijo;
                    contenedorElementos = document.getElementById(datosElemento.contenedorElementos + identificadorDatosElemento);

                    siguienteIdentificador = inputIdentificadorAUtilizar.value;
                    siguienteIdentificador = parseInt(siguienteIdentificador);
                    siguienteIdentificador++;

                    estructuraHTML = generarEstructuraDeElemento(strNombreSeccion, clavePrincipal, arregloDimensiones, inputIdentificadorAUtilizar.value);
                    contenedorElementos.innerHTML += estructuraHTML;
                    
                    if(tipoDeEtiqueta === "ul")
                    { 
                        
                        let totalDeElementosHijosPrevios = parseInt(inputIdentificadorAUtilizar.value) - 1;
                        totalDeElementosHijosPrevios = (totalDeElementosHijosPrevios < 0) ? 0 : totalDeElementosHijosPrevios;
                        
                        if(elementosHijo && elementosHijo.length > 0)
                        { 
                            elementosHijo.map(hijo => {

                                for(let identificadorElementoHijo = 0; identificadorElementoHijo <= totalDeElementosHijosPrevios; identificadorElementoHijo++)
                                { 
                                    
                                    if(hijo.tipo === "objeto")
                                    { 

                                        let elementosInternos = hijo.elementosHijo;

                                        elementosInternos.map(elementoInterno => {
                                            if(elementoInterno.tipo === "ul")
                                            { 
                                                if(document.getElementById('contenedor-' + elementoInterno.elemento + '_' + identificadorElementoHijo))
                                                { 
                                                    ($(document.getElementById('contenedor-' + elementoInterno.elemento + '_' + identificadorElementoHijo))).foundation();
                                                } 
                                            } 
                                        });

                                    } 
                                    else if(hijo.tipo === "ul")
                                    { 
                                        if(document.getElementById('contenedor-' + hijo.elemento + '_' + identificadorElementoHijo))
                                        { 
                                            ($(document.getElementById('contenedor-' + hijo.elemento + '_' + identificadorElementoHijo))).foundation();
                                        } 
                                    } 
                                } 
                                
                            });
                        } 
                        
                        Foundation.reInit($(contenedorElementos)); 
                    } 

                    if(elementosHijo && elementosHijo.length > 0)
                    { 
                        elementosHijo.map(hijo => {
                            
                            if(hijo.tipo === "objeto")
                            { 

                                let elementosInternos = hijo.elementosHijo;

                                elementosInternos.map(elementoInterno => {
                                    
                                    if(elementoInterno.tipo === "ul")
                                    { 
                                        
                                        if(identificadorDatosElemento !== "")
                                        {
                                            ($(document.getElementById('contenedor-' + elementoInterno.elemento + '_' + identificadorDatosElemento + "_" + inputIdentificadorAUtilizar.value))).foundation();
                                        }
                                        else
                                        {
                                            ($(document.getElementById('contenedor-' + elementoInterno.elemento + '_' + inputIdentificadorAUtilizar.value))).foundation();
                                        }
                                        
                                    } 

                                });

                            } 
                            else if(hijo.tipo === "ul")
                            { 
                                
                                if(identificadorDatosElemento !== "")
                                {
                                    ($(document.getElementById('contenedor-' + hijo.elemento + '_' + identificadorDatosElemento + "_" + inputIdentificadorAUtilizar.value))).foundation();
                                }
                                else
                                {
                                    ($(document.getElementById('contenedor-' + hijo.elemento + '_' + inputIdentificadorAUtilizar.value))).foundation();
                                }
                                
                            } 
                        });
                    } 

                    listaIdentificadoresPorRegistrar.value = (listaIdentificadoresPorRegistrar.value === "") ? inputIdentificadorAUtilizar.value : listaIdentificadoresPorRegistrar.value + `, ${inputIdentificadorAUtilizar.value}`;
                    document.getElementById("siguiente-identificador-" + strNombreSeccion + identificadorDatosElemento).value = `${siguienteIdentificador}`;

                    cerrarFormulario(strNombreSeccion);

                } 
                else
                { 
                    swal("Se ha cancelado la operaci\u00f3n", {
                        icon: "error",
                        buttons: false,
                        timer: 2000
                    });
                    return false;
                } 
            });
            
        });

    }

} 

const actualizarElemento = (strNombreSeccion, arregloDimensiones, strIdContenedorDatosElemento) =>
{ 

    let datosElemento;
    let formulario;
    let identificadorDatosElemento = "";
    let identificadorDatosElementoParaClavesPrincipales = "";
    let inputClavePrincipal;
    let clavePrincipal;
    let listaIdentificadoresPorRegistrar;
    let identificadorActualElemento = "";

    datosElemento = obtenerDatosFormularioS6(strNombreSeccion);

    if(arregloDimensiones && arregloDimensiones.length >= 1)
    { 
        arregloDimensiones.map(function(nivel, index){
            identificadorDatosElemento += `_${nivel}`;
            if(arregloDimensiones.length > 1 && index !== (arregloDimensiones.length - 1))
            {
                identificadorDatosElementoParaClavesPrincipales += `_${nivel}`;
            }
            else
            {
                identificadorActualElemento = `${nivel}`;
            }
        });
    } 

    inputClavePrincipal = document.getElementById(datosElemento.idPrincipal);
    clavePrincipal = inputClavePrincipal.value;

    listaIdentificadoresPorRegistrar = document.getElementById("lista-identificadores-por-registrar-" + strNombreSeccion + identificadorDatosElementoParaClavesPrincipales);
    
    if(validarClavesPrincipales(datosElemento.idPrincipal, clavePrincipal, identificadorDatosElementoParaClavesPrincipales, listaIdentificadoresPorRegistrar.value, identificadorActualElemento) === false)
    {
        swal({
            title: "Identificador principal repetido",
            text: "El valor ingresado para el identificador principal ya se encuentra en uso.\n\nFavor de ingresar un identificador diferente.",
            icon: "error",
            button: 'ok'
        });
        inputClavePrincipal.value = "";
        return false;
    }
    else
    {
        formulario = datosElemento.formulario;

        $('#'+formulario).foundation('validateForm');
        $("#"+formulario).on("formvalid.zf.abide", function (ev, frm){
            swal({
                title: "Confirmar",
                text: "\u00bf Los datos son correctos\u003F",
                icon: "warning",
                buttons: ["Cancelar", "Aceptar"],
                dangerMode: true,
                closeOnClickOutside: false,
                closeOnEsc: false
            })
            .then((seAcepta) => {
                if (seAcepta)
                { 
                    
                    let estructuraHTMLCamposElemento;
                    let camposDelElemento;
                    let contenedorElementos;
                    let tituloDelElemento;

                    contenedorElementos = document.getElementById(strIdContenedorDatosElemento);
                    camposDelElemento = datosElemento.campos;

                    tituloDelElemento = document.getElementById("titulo-elemento-" + strNombreSeccion + identificadorDatosElemento);
                    estructuraHTMLCamposElemento = obtenerValoresCamposElementos(camposDelElemento, identificadorDatosElemento)
                    contenedorElementos.innerHTML = estructuraHTMLCamposElemento;

                    tituloDelElemento.innerHTML = clavePrincipal;
                    
                    cerrarFormulario(strNombreSeccion);

                } 
                else
                { 
                    swal("Se ha cancelado la operaci\u00f3n", {
                        icon: "error",
                        buttons: false,
                        timer: 2000
                    });
                    return false;
                } 
            });
            
        });
    }
    
} 

const eliminarElemento = (strNombreSeccion, strIdElemento, arregloDimensiones, strIdentificadorElemento) =>
{ 

    let elemento;
    let listaIdentificadoresPorRegistrar;
    let arregloListaIdentificadoresPorRegistrar = [];
    let arregloAuxiliarParaDatos;
    let valorListaIdentificadoresPorRegistrar;
    let identificadorDatosElemento = "";

    elemento = document.getElementById(strIdElemento);

    if(elemento)
    {

        if(arregloDimensiones && arregloDimensiones.length >= 1)
        { 
            arregloDimensiones.map(nivel => {
                identificadorDatosElemento += `_${nivel}`;
            });
        } 

        listaIdentificadoresPorRegistrar = document.getElementById("lista-identificadores-por-registrar-" + strNombreSeccion + identificadorDatosElemento);
        
        valorListaIdentificadoresPorRegistrar = listaIdentificadoresPorRegistrar.value;

        if(valorListaIdentificadoresPorRegistrar.includes(",") === true)
        {

            arregloAuxiliarParaDatos = valorListaIdentificadoresPorRegistrar.split(",");
            arregloAuxiliarParaDatos.map(elemento => {
                if(elemento.trim() !== "")
                {
                    arregloListaIdentificadoresPorRegistrar.push(elemento.trim());
                }
            });

        }
        else
        {
            arregloListaIdentificadoresPorRegistrar.push(valorListaIdentificadoresPorRegistrar.trim());
        }

        valorListaIdentificadoresPorRegistrar = arregloListaIdentificadoresPorRegistrar.filter((item) => item !== strIdentificadorElemento);

        listaIdentificadoresPorRegistrar.value = valorListaIdentificadoresPorRegistrar.join(", ");

        elemento.remove();
        Foundation.reInit("abide"); 
    }

} 

const restaurarEstadoInicialElemento = (strNombreSeccion, arregloDimensiones) => 
{ 

    let contenedorElementos;
    let datosElemento;
    let listaIdentificadoresPorRegistrar;
    let indicadorDelElemento;
    let estructuraHTMLPorDefecto;
    let valorPorDefectoListaIdentificadoresPorRegistrar;
    let valorPorDefectoIndicadorDelElemento;
    let identificadorDatosElemento = "";
    let tipoDeEtiqueta;
    let elementosHijos;

    if(arregloDimensiones && arregloDimensiones.length >= 1)
    { 
        arregloDimensiones.map(nivel => {
            identificadorDatosElemento += `_${nivel}`;
        });
    } 

    datosElemento = obtenerDatosFormularioS6(strNombreSeccion);

    tipoDeEtiqueta = datosElemento.tipoDeEtiqueta;
    elementosHijos = datosElemento.elementosHijo;
    contenedorElementos = document.getElementById(datosElemento.contenedorElementos + identificadorDatosElemento);
    listaIdentificadoresPorRegistrar = document.getElementById('lista-identificadores-por-registrar-' + strNombreSeccion + identificadorDatosElemento);
    indicadorDelElemento = document.getElementById('siguiente-identificador-' + strNombreSeccion + identificadorDatosElemento);
    estructuraHTMLPorDefecto = document.getElementById('estructura-por-defecto-' + strNombreSeccion + identificadorDatosElemento);

    valorPorDefectoListaIdentificadoresPorRegistrar = listaIdentificadoresPorRegistrar.dataset.valorPorDefecto;
    valorPorDefectoIndicadorDelElemento = indicadorDelElemento.dataset.valorPorDefecto;

    listaIdentificadoresPorRegistrar.value = valorPorDefectoListaIdentificadoresPorRegistrar;
    indicadorDelElemento.value = valorPorDefectoIndicadorDelElemento;
    contenedorElementos.innerHTML = estructuraHTMLPorDefecto.value;

    
    if(elementosHijos && elementosHijos.length > 0)
    { 
        
        let arregloIdentificadoresARestaurar = [];
        let arregloAuxiliardentificadoresPorDefecto;
        
        if(valorPorDefectoListaIdentificadoresPorRegistrar.includes(",") === true)
        {
            
            arregloAuxiliardentificadoresPorDefecto = valorPorDefectoListaIdentificadoresPorRegistrar.split(",");
            arregloAuxiliardentificadoresPorDefecto.map(identificador => {
                if(identificador.trim() !== "")
                {
                    arregloIdentificadoresARestaurar.push(identificador.trim());
                }
            });
            
        }
        else if(valorPorDefectoListaIdentificadoresPorRegistrar.trim() !== "")
        {
            arregloIdentificadoresARestaurar.push(valorPorDefectoListaIdentificadoresPorRegistrar.trim());
        }
        
        arregloIdentificadoresARestaurar.map(identificador => {
            elementosHijos.map(hijo => {
                if(hijo.tipo === "ul")
                {
                    ($(document.getElementById("contenedor-" + hijo.elemento + "_" + identificador))).foundation();
                }
            });
        });
        
    } 
    
    if(tipoDeEtiqueta === "ul")
    { 
        Foundation.reInit($(contenedorElementos));
    } 

    Foundation.reInit("abide"); 
    

} 

const obtenerValoresCamposElementos = (objCamposDelElemento, strIdentificadorParaElCampo) =>
{ 

    let campo;
    let valorCampo;
    let nombreParaCampo;
    let estructuraHTML = "";
    let tipoDeCampo;

    objCamposDelElemento.map(dato => {
                    
        tipoDeCampo = dato.tipo;

        if(tipoDeCampo === "text" || tipoDeCampo === "select")
        { 
            campo = document.getElementById(dato.id);
            valorCampo = (campo) ? campo.value : "";
        } 
        else if(tipoDeCampo === "radio")
        { 
            campo = document.querySelector(`input[name='${dato.id}']:checked`);
            valorCampo = (campo) ? campo.value : "";
        } 
        else if(tipoDeCampo === "checkbox")
        { 
            campo = document.getElementsByName(dato.id);
            valorCampo = "";
            for(let i = 0; i < campo.length; i++)
            { 
                if(campo[i].checked === true)
                { 
                    valorCampo += `${campo[i].value}, `
                } 
            } 
        } 

        nombreParaCampo = dato.id + strIdentificadorParaElCampo;
        estructuraHTML += `<input type="hidden" name="${nombreParaCampo}" id="${nombreParaCampo}" value="${valorCampo}">`;

    }); 

    return estructuraHTML;

} 

const generarEstructuraDeElemento = (strNombreSeccion, strClavePrincipalElemento, arregloDimensiones, strIdentificadorElemento) =>
{ 

    let identificadorDatosElemento;
    let identificadorDatosElementoArreglo = "";
    let estructuraHTML = "";
    let estructuraHTMLElementosHijo = "";
    let estructuraHTMLCamposElemento;
    let arregloDimensionesParaAbrirFormulario = [];
    let camposDelElemento;
    let datosElemento;
    let tipoDeEtiqueta;
    let nombreParaElemento;
    let elementosHijo;

    datosElemento = obtenerDatosFormularioS6(strNombreSeccion);
    camposDelElemento = datosElemento.campos;
    tipoDeEtiqueta = datosElemento.tipoDeEtiqueta;
    nombreParaElemento = datosElemento.nombreParaElemento;
    elementosHijo = datosElemento.elementosHijo;

    if(arregloDimensiones && arregloDimensiones.length >= 1)
    { 
        arregloDimensiones.map(function(nivel, index){
            
            if(index === 0)
            { 
                identificadorDatosElementoArreglo += `${nivel}`;
            } 
            else
            { 
                identificadorDatosElementoArreglo += `_${nivel}`;
            } 
            
            arregloDimensionesParaAbrirFormulario.push(nivel);
        });
        identificadorDatosElemento = `${identificadorDatosElementoArreglo}_${strIdentificadorElemento}`;
        arregloDimensionesParaAbrirFormulario.push(strIdentificadorElemento);
    } 
    else
    { 
        identificadorDatosElemento = `${strIdentificadorElemento}`;
        arregloDimensionesParaAbrirFormulario.push(strIdentificadorElemento);
    } 
        
    estructuraHTMLCamposElemento = obtenerValoresCamposElementos(camposDelElemento, `_${identificadorDatosElemento}`);
    
    if(tipoDeEtiqueta === "div")
    { 

        estructuraHTML += `<div id="${strNombreSeccion}_${identificadorDatosElemento}">`;
        estructuraHTML += `    <div class="grid-x" style="background-color: #e8222d; color: #fff;">`;
        estructuraHTML += `        <div class="cell medium-8">`;
        estructuraHTML += `            <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">${nombreParaElemento}</span> - <span id="titulo-elemento-${strNombreSeccion}_${identificadorDatosElemento}">${strClavePrincipalElemento}</span></h6>`;
        estructuraHTML += `        </div>`;
        estructuraHTML += `        <div id="datos-elemento-${strNombreSeccion}_${identificadorDatosElemento}">`;
        estructuraHTML += estructuraHTMLCamposElemento;
        estructuraHTML += `        </div>`;
        estructuraHTML += `        <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">`;
        estructuraHTML += `            <button class="button margin-0" onclick="abrirFormularioPrevio('${strNombreSeccion}', [${arregloDimensionesParaAbrirFormulario}])"><i class="material-icons">edit</i> Editar</button>`;
        estructuraHTML += `            <button class="button margin-0" onclick="eliminarElemento('${strNombreSeccion}', '${strNombreSeccion}_${identificadorDatosElemento}', [${arregloDimensiones}], '${strIdentificadorElemento}')"><i class="material-icons">remove_circle</i> Eliminar</button>`;
        estructuraHTML += `        </div>`;
        estructuraHTML += `    </div>`;
        estructuraHTML += `</div>`;

    } 
    else if(tipoDeEtiqueta === "ul")
    { 

        estructuraHTML += `<li class="accordion-item" data-accordion-item id="${strNombreSeccion}_${identificadorDatosElemento}">`;
        estructuraHTML += `    <a href="#" class="accordion-title"><span style="text-transform: capitalize;">${nombreParaElemento}</span> - <span id="titulo-elemento-${strNombreSeccion}_${identificadorDatosElemento}">${strClavePrincipalElemento}</span></a>`;
        estructuraHTML += `    <div class="accordion-content" data-tab-content>`;
        estructuraHTML += `        <div class="grid-margin-x grid-x">`;
        estructuraHTML += `            <div class="cell padding-horizontal-1" style="display:flex; justify-content: flex-end; gap: 10px; flex-wrap: wrap;">`;
        estructuraHTML += `                <button class="button margin-0 btnFrmContrataciones" onclick="abrirFormularioPrevio('${strNombreSeccion}', [${arregloDimensionesParaAbrirFormulario}])"><i class="material-icons">edit</i> Editar ${nombreParaElemento}</button>`;
        estructuraHTML += `                <button class="button margin-0 btnFrmContrataciones" onclick="eliminarElemento('${strNombreSeccion}', '${strNombreSeccion}_${identificadorDatosElemento}', [${arregloDimensiones}], '${strIdentificadorElemento}')"><i class="material-icons">remove_circle</i> Eliminar ${nombreParaElemento}</button>`;
        estructuraHTML += `            </div>`;
        estructuraHTML += `            <div id="datos-elemento-${strNombreSeccion}_${identificadorDatosElemento}">`;
        estructuraHTML += estructuraHTMLCamposElemento;
        estructuraHTML += `            </div>`;

        elementosHijo.map(hijo => {

            if(hijo.tipo === "objeto")
            { 

                let elementosInternos = hijo.elementosHijo;
                let estructuraHTMLElementoInterno = "";

                estructuraHTMLElementosHijo += `    <fieldset class="cell fieldset">`;
                estructuraHTMLElementosHijo += `        <legend title="${hijo.leyenda}">${hijo.titulo}</legend>`;
                estructuraHTMLElementosHijo += `        <div class="grid-x grid-margin-x">`;

                elementosInternos.map(elementoInterno => {

                    estructuraHTMLElementoInterno += `    <fieldset class="cell fieldset">`;
                    estructuraHTMLElementoInterno += `        <input type="hidden" id="estructura-por-defecto-${elementoInterno.elemento}_${identificadorDatosElemento}" value="">`;
                    estructuraHTMLElementoInterno += `        <input type="hidden" id="siguiente-identificador-${elementoInterno.elemento}_${identificadorDatosElemento}" value="0" data-valor-por-defecto="0">`;
                    estructuraHTMLElementoInterno += `        <input type="hidden" id="lista-identificadores-por-registrar-${elementoInterno.elemento}_${identificadorDatosElemento}" name="lista-identificadores-por-registrar-${elementoInterno.elemento}_${identificadorDatosElemento}" value="" data-valor-por-defecto="">`;
                    estructuraHTMLElementoInterno += `        <legend title="${elementoInterno.leyenda}">${elementoInterno.titulo}</legend>`;
                    estructuraHTMLElementoInterno += `        <div class="button-group" style="justify-content: flex-end; gap:10px;">`;
                    estructuraHTMLElementoInterno += `            <a onclick="restaurarEstadoInicialElemento('${elementoInterno.elemento}', [${arregloDimensionesParaAbrirFormulario}])"><i class="material-icons secondary">restart_alt</i></a>`;
                    estructuraHTMLElementoInterno += `            <a onclick="abrirNuevoFormulario('${elementoInterno.elemento}', [${arregloDimensionesParaAbrirFormulario}])"><i class="material-icons secondary">add_circle</i></a>`;
                    estructuraHTMLElementoInterno += `        </div>`;

                    if(elementoInterno.tipo === "div")
                    { 
                        estructuraHTMLElementoInterno += `        <div id="contenedor-${elementoInterno.elemento}_${identificadorDatosElemento}" style="display: flex; flex-direction: column; gap: 10px;">`;
                        estructuraHTMLElementoInterno += `        </div>`;   
                    } 
                    else if(elementoInterno.tipo === "ul")
                    { 
                        estructuraHTMLElementoInterno += `        <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-${elementoInterno.elemento}_${identificadorDatosElemento}">`;
                        estructuraHTMLElementoInterno += `        </ul>`;   
                    } 

                    estructuraHTMLElementoInterno += `    </fieldset>`; 

                });

                estructuraHTMLElementosHijo += estructuraHTMLElementoInterno;
                estructuraHTMLElementosHijo += `        </div>`;
                estructuraHTMLElementosHijo += `    </fieldset>`; 
            } 
            else if(hijo.tipo === "div")
            { 
                estructuraHTMLElementosHijo += `    <fieldset class="cell fieldset">`;
                estructuraHTMLElementosHijo += `        <input type="hidden" id="estructura-por-defecto-${hijo.elemento}_${identificadorDatosElemento}" value="">`;
                estructuraHTMLElementosHijo += `        <input type="hidden" id="siguiente-identificador-${hijo.elemento}_${identificadorDatosElemento}" value="0" data-valor-por-defecto="0">`;
                estructuraHTMLElementosHijo += `        <input type="hidden" id="lista-identificadores-por-registrar-${hijo.elemento}_${identificadorDatosElemento}" name="lista-identificadores-por-registrar-${hijo.elemento}_${identificadorDatosElemento}" value="" data-valor-por-defecto="">`;
                estructuraHTMLElementosHijo += `        <legend title="${hijo.leyenda}">${hijo.titulo}</legend>`;
                estructuraHTMLElementosHijo += `        <div class="button-group" style="justify-content: flex-end; gap:10px;">`;
                estructuraHTMLElementosHijo += `            <a onclick="restaurarEstadoInicialElemento('${hijo.elemento}', [${arregloDimensionesParaAbrirFormulario}])"><i class="material-icons secondary">restart_alt</i></a>`;
                estructuraHTMLElementosHijo += `            <a onclick="abrirNuevoFormulario('${hijo.elemento}', [${arregloDimensionesParaAbrirFormulario}])"><i class="material-icons secondary">add_circle</i></a>`;
                estructuraHTMLElementosHijo += `        </div>`;
                estructuraHTMLElementosHijo += `        <div id="contenedor-${hijo.elemento}_${identificadorDatosElemento}" style="display: flex; flex-direction: column; gap: 10px;">`;
                estructuraHTMLElementosHijo += `        </div>`;   
                estructuraHTMLElementosHijo += `    </fieldset>`; 
            } 
            else if(hijo.tipo === "ul")
            { 
                estructuraHTMLElementosHijo += `    <fieldset class="cell fieldset">`;
                estructuraHTMLElementosHijo += `        <input type="hidden" id="estructura-por-defecto-${hijo.elemento}_${identificadorDatosElemento}" value="">`;
                estructuraHTMLElementosHijo += `        <input type="hidden" id="siguiente-identificador-${hijo.elemento}_${identificadorDatosElemento}" value="0" data-valor-por-defecto="0">`;
                estructuraHTMLElementosHijo += `        <input type="hidden" id="lista-identificadores-por-registrar-${hijo.elemento}_${identificadorDatosElemento}" name="lista-identificadores-por-registrar-${hijo.elemento}_${identificadorDatosElemento}" value="" data-valor-por-defecto="">`;
                estructuraHTMLElementosHijo += `        <legend title="${hijo.leyenda}">${hijo.titulo}</legend>`;
                estructuraHTMLElementosHijo += `        <div class="button-group" style="justify-content: flex-end; gap:10px;">`;
                estructuraHTMLElementosHijo += `            <a onclick="restaurarEstadoInicialElemento('${hijo.elemento}', [${arregloDimensionesParaAbrirFormulario}])"><i class="material-icons secondary">restart_alt</i></a>`;
                estructuraHTMLElementosHijo += `            <a onclick="abrirNuevoFormulario('${hijo.elemento}', [${arregloDimensionesParaAbrirFormulario}])"><i class="material-icons secondary">add_circle</i></a>`;
                estructuraHTMLElementosHijo += `        </div>`;
                estructuraHTMLElementosHijo += `        <ul class="accordion" data-accordion data-allow-all-closed="true" id="contenedor-${hijo.elemento}_${identificadorDatosElemento}">`;
                estructuraHTMLElementosHijo += `        </ul>`;    
                estructuraHTMLElementosHijo += `    </fieldset>`; 
            } 

        });

        estructuraHTML += estructuraHTMLElementosHijo;
        estructuraHTML += `        </div>`;
        estructuraHTML += `    </div>`;
        estructuraHTML += `</li>`;

    } 

    return estructuraHTML;

} 

const validarClavesPrincipales = (strNombreIdPrincipal, strClaveAValidar, strIdentificadorDimensionElemento, strListaDeIdentificadoresRegistrados, strIdentificadorActual) =>
{ 

    let listaIdentificadoresRegistrados = [];
    let bolIdentificadorValido = true;
    let inputClavePrincipal;

    if(strListaDeIdentificadoresRegistrados.includes(",") === true)
    { 
        strListaDeIdentificadoresRegistrados = strListaDeIdentificadoresRegistrados.split(",");
        strListaDeIdentificadoresRegistrados.map(identificador => {
            if(identificador.trim() !== "")
            { 
                listaIdentificadoresRegistrados.push(identificador.trim());
            } 
        });
    } 
    else if(strListaDeIdentificadoresRegistrados.trim() !== "")
    { 
        listaIdentificadoresRegistrados.push(strListaDeIdentificadoresRegistrados.trim());
    } 

    listaIdentificadoresRegistrados.map(identificador => {

        if(identificador !== strIdentificadorActual)
        { 
            
            inputClavePrincipal = document.getElementById(strNombreIdPrincipal + strIdentificadorDimensionElemento + "_" + identificador);
            
            if(inputClavePrincipal.value === strClaveAValidar)
            { 
                bolIdentificadorValido = false;
            } 

        } 

    });


    return bolIdentificadorValido;

} 