<%--
    Autor: Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@include file="/encabezado.jsp" %>

<div class="orbit" role="region" aria-label="Hero PDE" data-orbit="data-orbit">
    <ul class="orbit-container">
        <li class="orbit-slide">
            <img class="orbit-image" src="framework/img/png/banner-pde-sis-min.png" alt="PDE"/>
        </li>
    </ul>
</div>

<%@include file="/migas.jsp" %>

<div class="grid-container display-top">
    <div class="grid-x">
        <div class="large-12 display-top">
            <h1>Plataforma Digital Estatal</h1>
            <h2>Sistema VI</h2>
            <h4>Informaci&oacute;n p&uacute;blica de contrataciones.</h4>
            <div id="cargaDB" class="reveal-overlay" style="display: none;">
                <div id="loader"></div>
                <div id="textDB">
                    Actualizando base de datos...
                </div>
            </div>
            <div class="stacked-for-small expanded large button-group">
                <a class="button hollow" onclick="document.frmListado.submit()"><i class="material-icons md-4t">list</i> Listado de contrataciones</a>
                <a class="button"><i class="material-icons md-4t">playlist_add</i> Agregar nueva contrataci&oacute;n</a>
                <form name="frmListado" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                    <input type="hidden" name="accion" value="ingresarSistema"/>
                </form>
            </div>
            <c:choose>
                <c:when test="${seContinuaRegistro == true}">
                    <h2>Continuar registro de una contrataci&oacute;n p&uacute;blica.</h2>
                    <div class="callout primary margin-vertical-2 padding-2">
                        <h4>Contrataci&oacute;n p&uacute;blica incompleta.</h4>
                        <p style="text-align: justify;">El registro de la contrataci&oacute;n p&uacute;blica no cuenta con la captura de la informaci&oacute;n dentro de todas sus secciones, por lo que se solicita que se realice la captura de la informaci&oacute;n faltante en el menor tiempo posible.<p>
                        <p style="text-align: justify;">Secciones finalizadas: <p>
                        <ul class="">
                            <c:forEach items="${estadoSeccionesContratacion}" var="estadoSeccion">
                                <c:if test="${estadoSeccion.numero_seccion == 1 && estadoSeccion.estado_seccion.equals('1') == true}">
                                    <li>Datos generales</li>
                                </c:if>
                                <c:if test="${estadoSeccion.numero_seccion == 2 && estadoSeccion.estado_seccion.equals('1') == true}">
                                    <li>Publicador</span>
                                </c:if>
                                <c:if test="${estadoSeccion.numero_seccion == 3 && estadoSeccion.estado_seccion.equals('1') == true}">
                                    <li>Etiquetas de entrega</li>
                                </c:if>
                                <c:if test="${estadoSeccion.numero_seccion == 4 && estadoSeccion.estado_seccion.equals('1') == true}">
                                    <li>Partes involucradas</li>
                                </c:if>
                                <c:if test="${estadoSeccion.numero_seccion == 5 && estadoSeccion.estado_seccion.equals('1') == true}">
                                    <li>Comprador</li>
                                </c:if>
                                <c:if test="${estadoSeccion.numero_seccion == 6 && estadoSeccion.estado_seccion.equals('1') == true}">
                                    <li>Planeaci&oacute;n</li>
                                </c:if>
                                <c:if test="${estadoSeccion.numero_seccion == 7 && estadoSeccion.estado_seccion.equals('1') == true}">
                                    <li>Licitaci&oacute;n</li>
                                </c:if>
                                <c:if test="${estadoSeccion.numero_seccion == 8 && estadoSeccion.estado_seccion.equals('1') == true}">
                                    <li>Adjudicaciones</li>
                                </c:if>
                                <c:if test="${estadoSeccion.numero_seccion == 9 && estadoSeccion.estado_seccion.equals('1') == true}">
                                    <li>Contratos</li>
                                </c:if>
                            </c:forEach>
                        </ul>
                    </div>
                    <div class="grid-container">
                        <div class="grid-margin-x grid-x">   
                            <a class="button cell expanded medium-6 secondary" onclick="frmRegresar.submit()">Regresar</a>
                            <a class="button cell expanded medium-6" onclick="frmIniciarRegistro.submit()">Reanudar registro</a>
                            <form name="frmRegresar" id="frmRegresar" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post">
                                <input type="hidden" name="accion" value="consultarContrataciones"/>
                                <input type="hidden" name="cmbPaginacion" value="${filtros.registrosMostrar}"/>
                                <input type="hidden" name="cmbOrdenacion" value="${filtros.orden}"/>
                                <input type="hidden" name="txtNumeroPagina" value="${filtros.numeroPagina}"/>
                                <input type="hidden" name="cmbCiclo" value="${filtros.ciclo}"/>
                                <input type="hidden" name="txtPublicador" value="${filtros.publicador}"/>
                                <input type="hidden" name="txtDependencia" value="${filtros.dependencia}"/>
                            </form>
                            <form name="frmIniciarRegistro" id="frmIniciarRegistro" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post">
                                <input type="hidden" name="accion" value="iniciarContinuarRegistroDeContratacion"/> 
                                <input type="hidden" name="txtID_CONTRATACION_SPDN" value="${idContratacion}"/> 
                            </form>
                        </div> 
                    </div>
                </c:when>
                <c:otherwise>
                    <h2>Agregar nueva contrataci&oacute;n p&uacute;blica.</h2>
                    <div class="callout primary margin-vertical-2 padding-2">
                        <h4>Registro de una nueva contrataci&oacute;n p&uacute;blica.</h4>
                        <p style="text-align: justify;">La generaci&oacute;n de un nuevo registro en relaci&oacute;n a una contrataci&oacute;n p&uacute;blica se compone de la captura de la informaci&oacute;n dentro de 9 secciones que integran en su totalidad cada uno de los apartados establecidos en el est&aacute;ndar de datos de la PDE referente al Sistema 6 - Contrataciones.</p>
                        <p style="text-align: justify;">Para el registro de una nueva contrataci&oacute;n p&uacute;blica no es obligatorio terminar con la captura de la informaci&oacute;n de todas las secciones, &uacute;nicamente es requerido que se finalize la primera secci&oacute;n para que el registro se encuentre contemplado dentro de la PDE, sin embargo se recomienda que realize la captura de la informaci&oacute;n en la mayor&iacute;a de las secciones posibles.</p>
                    </div>
                    <div class="grid-container">
                        <div class="grid-margin-x grid-x">   
                            <a class="button cell expanded medium-6 secondary" onclick="frmRegresar.submit()">Regresar</a>
                            <a class="button cell expanded medium-6" onclick="frmIniciarRegistro.submit()">Iniciar registro</a>
                            <form name="frmRegresar" id="frmRegresar" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post">
                                <input type="hidden" name="accion" value="ingresarSistema"/>
                            </form>
                            <form name="frmIniciarRegistro" id="frmIniciarRegistro" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post">
                                <input type="hidden" name="accion" value="iniciarContinuarRegistroDeContratacion"/> 
                            </form>
                        </div> 
                    </div>
                </c:otherwise>
            </c:choose>
            
        </div>
    </div>
</div>
                
<%@include file="/piePagina.jsp" %>