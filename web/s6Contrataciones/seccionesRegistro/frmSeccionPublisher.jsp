<%-- 
    Autor: Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<div id="seccion2">
    <h3>Publicador</h3>
    <div class="callout padding-horizontal-3">
        <form name="frmRegistrarSeccionPublisher" id="frmRegistrarSeccionPublisher" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post" data-abide novalidate onSubmit="event.preventDefault(); validarFormularioPublicador(this.id);" autocomplete="off">
            <input type="hidden" name="accion" value="${(bolEsFormulariosDeEdicion == true) ? 'editarSeccion' : 'registrarSeccion'}"/>
            <input type="hidden" name="txtEsFormularioDeEdicion" id="txtEsFormularioDeEdicion" value="${bolEsFormulariosDeEdicion}"/>
            <input type="hidden" name="txtNUMERO_SECCION" value="2"/>
            <input type="hidden" name="txtID_CONTRATACION_SPDN" value="${idContratacion}"/>
            <fieldset class="margin-vertical-2">
                <div class="grid-margin-x grid-x">
                    <label class="cell medium-6">UID
                        <input type="text" name="txtUID" id="txtUID" placeholder="p. ej. uid" value="${contratacion.publisher.uid}" title="El ID &uacute;nico para esta entidad en el esquema de identificadores dado."/>
                        <span class="form-error" data-form-error-for="txtUID">
                            Campo requerido, verifica el formato de tu registro.
                        </span>
                    </label>
                    <label class="cell medium-6">Nombre <span style="color: red">*</span>
                        <input type="text" name="txtNAME" id="txtNAME" placeholder="p. ej. Nombre de la organizaci&oacute;n" value="${contratacion.publisher.name}" required title="El nombre de la organizaci&oacute;n o departamento responsable de publicar estos datos."/>
                        <span class="form-error" data-form-error-for="txtNAME">
                            Campo requerido, verifica el formato de tu registro.
                        </span>
                    </label>
                    <label class="cell medium-6">Esquema
                        <input type="text" name="txtSCHEME" id="txtSCHEME" placeholder="p. ej. esquema" value="${contratacion.publisher.scheme}" title="El esquema que tiene los identificadores &uacute;nicos utilizados para identificar el art&iacute;culo que se identifica."/>
                        <span class="form-error" data-form-error-for="txtSCHEME">
                            Campo requerido, verifica el formato de tu registro.
                        </span>
                    </label>
                    <label class="cell medium-6">URI
                        <input type="text" name="txtURI" id="txtURI" placeholder="p. ej. www.url.com" pattern="url" value="${contratacion.publisher.uri}" title="Una URI para identificar al publicador."/>
                        <span class="form-error" data-form-error-for="txtURI">
                            Campo requerido, verifica el formato de tu registro.
                        </span>
                    </label>
                </div>
            </fieldset>
            <div class="grid-x grid-margin-x">
                <fieldset class="cell medium-6">
                    <input type="reset" class="button expanded secondary" name="btnResetPublicador" value="${(bolEsFormulariosDeEdicion == true) ? 'Restaurar' : 'Reiniciar'}"/>
                </fieldset>
                <fieldset class="cell medium-6">
                    <input type="submit" class="button expanded" name="btnRegistrarPublicador" value="${(bolEsFormulariosDeEdicion == true) ? 'Modificar datos' : 'Continuar'}"/>
                </fieldset>
            </div>
        </form>
        <div data-abide-error="data-abide-error" class="alert callout alertFooter" style="display: none;">
            <p>
                <i class="material-icons">warning</i>
                Existen algunos errores en tu registro de la secci&oacute;n 'Publicador'. Verifica nuevamente.
            </p>
        </div>
    </div>
</div>
                      
<script>
                
    function validarFormularioPublicador (idFormulario)
    { 
        
        $("#frmRegistrarSeccionPublisher").on("formvalid.zf.abide", function (ev, frm){
            
            let strMensajeFrm;          
            let strTituloMensaje;       
            let bolTipoFormulario;      
            
            bolTipoFormulario = document.getElementById("txtEsFormularioDeEdicion").value;
            bolTipoFormulario = Boolean(bolTipoFormulario);
            strMensajeFrm = (bolTipoFormulario === true) ? "\u00bf Los datos son correctos\u003F" : "Una vez guardada la informaci\u00F3n de esta secci\u00F3n no se podr\u00E1 volver a modificar los valores ingresados hasta que se finalice con el proceso de registro de la contrataci\u00F3n p\u00FAblica.";
            strTituloMensaje = (bolTipoFormulario === true) ? "Confirmar" : "\u00bf Los datos son correctos\u003F";
            
            $(".alertFooter").css("display", "none");
            
            swal({
                title: strTituloMensaje,
                text: strMensajeFrm,
                icon: "warning",
                buttons: ["Cancelar", "Aceptar"],
                dangerMode: true,
                closeOnClickOutside: false,
                closeOnEsc: false
            })
            .then((seAcepta) => {
                if (seAcepta)
                { 
                    document.getElementById(idFormulario).submit();
                } 
                else
                { 
                    swal("Se ha cancelado la operaci\u00f3n", {
                        icon: "error",
                        buttons: false,
                        timer: 2000
                    });
                    return false;
                } 
            });
            
        })
        .on("forminvalid.zf.abide", function(ev,frm) {
            $(".alertFooter").css("display", "block");
        });
                    
    }; 

                
</script>      