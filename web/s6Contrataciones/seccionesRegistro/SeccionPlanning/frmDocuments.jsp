<%-- 
    Autor: Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>
<div id="seccion6-bloque3">
    <h4>Documentos</h4>
    <div class="button-group">
        <button type="button" class="button" onclick="abrirNuevoFormulario('document', []);">Agregar documento</button>
    </div>
    <div class="callout padding-horizontal-3">
        <form name="frmRegistrarSeccionPlaneacionDocumentos" id="frmRegistrarSeccionPlaneacionDocumentos" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post" data-abide novalidate onSubmit="return false;" autocomplete="off">
            <input type="hidden" name="accion" value="${(bolEsFormulariosDeEdicion == true) ? 'editarSeccion' : 'registrarSeccion'}"/>
            <input type="hidden" name="txtEsFormularioDeEdicion" id="txtEsFormularioDeEdicion" value="${bolEsFormulariosDeEdicion}"/>
            <input type="hidden" name="txtNUMERO_SECCION" value="6"/>
            <input type="hidden" name="txtNUMERO_BLOQUE" value="3"/>
            <input type="hidden" name="txtID_CONTRATACION_SPDN" value="${idContratacion}"/>
            <input type="hidden" id="estructura-por-defecto-document" value=""> 
            <input type="hidden" id="siguiente-identificador-document" value="" data-valor-por-defecto=""> 
            <input type="hidden" id="lista-identificadores-por-registrar-document" name="lista-identificadores-por-registrar-document" value="" data-valor-por-defecto="">
            <c:choose>
                <c:when test="${contratacion.planning.documents.size() > 0}">
                    <c:set var="contadorElementosDocument" value="0"></c:set>
                    <div id="contenedor-document" style="display: flex; flex-direction: column; gap: 10px;">
                    <c:forEach items="${contratacion.planning.documents}" var="documento">
                        <div id="document_${contadorElementosDocument}">
                            <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                <div class="cell medium-8">
                                    <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">documento</span> - <span id="titulo-elemento-document_${contadorElementosDocument}">${documento.id}</span></h6>
                                </div>
                                <div id="datos-elemento-document_${contadorElementosDocument}">
                                    <input type="hidden" name="txtDOCUMENT_ID_${contadorElementosDocument}" id="txtDOCUMENT_ID_${contadorElementosDocument}" value="${documento.id}">
                                    <input type="hidden" name="cmbDOCUMENT_TYPE_${contadorElementosDocument}" id="cmbDOCUMENT_TYPE_${contadorElementosDocument}" value="${documento.documentType}">
                                    <input type="hidden" name="txtDOCUMENT_TITLE_${contadorElementosDocument}" id="txtDOCUMENT_TITLE_${contadorElementosDocument}" value="${documento.title}">
                                    <input type="hidden" name="txtDOCUMENT_DESCRIPTION_${contadorElementosDocument}" id="txtDOCUMENT_DESCRIPTION_${contadorElementosDocument}" value="${documento.description}">
                                    <input type="hidden" name="txtDOCUMENT_URL_${contadorElementosDocument}" id="txtDOCUMENT_URL_${contadorElementosDocument}" value="${documento.url}">
                                    <input type="hidden" name="txtDOCUMENT_DATE_PUBLISHED_${contadorElementosDocument}" id="txtDOCUMENT_DATE_PUBLISHED_${contadorElementosDocument}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(documento.datePublished)}">
                                    <input type="hidden" name="txtDOCUMENT_DATE_MODIFIED_${contadorElementosDocument}" id="txtDOCUMENT_DATE_MODIFIED_${contadorElementosDocument}" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(documento.dateModified)}">
                                    <input type="hidden" name="txtDOCUMENT_FORMAT_${contadorElementosDocument}" id="txtDOCUMENT_FORMAT_${contadorElementosDocument}" value="${documento.format}">
                                    <input type="hidden" name="cmbDOCUMENT_LANGUAGE_${contadorElementosDocument}" id="cmbDOCUMENT_LANGUAGE_${contadorElementosDocument}" value="${documento.language}">
                                </div>
                                <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                    <button class="button margin-0" onclick="abrirFormularioPrevio('document', [${contadorElementosDocument}])"><i class="material-icons">edit</i> Editar</button>
                                    <button class="button margin-0" onclick="eliminarElemento('document', 'document_${contadorElementosDocument}', [], '${contadorElementosDocument}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                </div>
                            </div>
                        </div>
                        <c:set var="contadorElementosDocument" value="${contadorElementosDocument + 1}"></c:set>
                    </c:forEach>
                    </div>
                    <input type="hidden" name="ultimo-identificador-elementos-document" id="ultimo-identificador-elementos-document" value="${contadorElementosDocument}">
                </c:when>
                <c:otherwise>
                    <div id="contenedor-document" style="display: flex; flex-direction: column; gap: 10px;"></div>
                    <input type="hidden" name="ultimo-identificador-elementos-document" id="ultimo-identificador-elementos-document" value="0">
                </c:otherwise>
            </c:choose>   
            <div class="grid-x grid-margin-x margin-top-1">
                <fieldset class="cell medium-6">
                    <input type="button" class="button expanded secondary" onclick="restaurarEstadoInicialElemento('document', []);" value="${(bolEsFormulariosDeEdicion == true) ? 'Restaurar' : 'Reiniciar'}"/>
                </fieldset>
                <fieldset class="cell medium-6">
                    <input type="button" onclick="validarFormulario('frmRegistrarSeccionPlaneacionDocumentos');" class="button expanded" id="btnRegistrarValoresFormulario" value="${(bolEsFormulariosDeEdicion == true) ? 'Modificar datos' : 'Continuar'}"/>
                </fieldset>
            </div> 
        </form>
    </div>
</div>
                
<%-- formulario de un documento (TOP) --%>
<div id="modalFrmDocument" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Documento</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmDocument" id="frmDocument" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-document" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-4">ID <span style="color: red">**</span>
                            <input type="text" id="txtDOCUMENT_ID" required placeholder="p. ej. 1" value="" title="Identificador local y &uacute;nico para este documento. Este campo se utiliza para darle seguimiento a las m&uacute;ltiples versiones de un documento en el proceso de creaci&oacute;n de un registro del proceso de contrataci&oacute;n (record) que se genera a partir de las entregas (release)."/>
                            <span class="form-error" data-form-error-for="txtDOCUMENT_ID" id="spanDOCUMENT_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Tipo de Documento
                            <select id="cmbDOCUMENT_TYPE" title="Una clasificaci&oacute;n del documento descrito.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${tiposDeDocumentoPlaneacion}" var="tipoDocumento">
                                    <option value="${tipoDocumento.clave}">${tipoDocumento.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbDOCUMENT_TYPE" id="spanDOCUMENT_TYPE">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                        <label class="cell medium-4">T&iacute;tulo
                            <input type="text" id="txtDOCUMENT_TITLE" placeholder="p. ej. T&iacute;tulo del documento" value="" title="El t&iacute;tulo del documento."/>
                            <span class="form-error" data-form-error-for="txtDOCUMENT_TITLE" id="spanDOCUMENT_TITLE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Descripci&oacute;n
                            <input type="text" id="txtDOCUMENT_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n del documento" value="" title="Una descripci&oacute;n corta del documento. Se recomienda que las descripciones no excedan 250 palabras. En el caso en que el documento no est&eacute; accesible en l&iacute;nea, el campo de descripci&oacute;n puede utilizarse para describir los arreglos necesarios para obtener una copia del documento."/>
                            <span class="form-error" data-form-error-for="txtDOCUMENT_DESCRIPTION" id="spanDOCUMENT_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">URL
                            <input type="text" id="txtDOCUMENT_URL" placeholder="p. ej. www.url.com" value="" pattern="url" title="Un enlace directo al documento o archivo adjunto. El servidor que da acceso a este documento debe de estar configurado para reportar correctamente el tipo MIME de documento."/>
                            <span class="form-error" data-form-error-for="txtDOCUMENT_URL" id="spanDOCUMENT_URL">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de publicaci&oacute;n
                            <input type="date" id="txtDOCUMENT_DATE_PUBLISHED" value="" pattern="fecha" title="La fecha de publicaci&oacute;n del documento. Esto es particularmente importante para documentos relevantes desde el punto de vista legal, como los avisos de licitaci&oacute;n."/>
                            <span class="form-error" data-form-error-for="txtDOCUMENT_DATE_PUBLISHED" id="spanDOCUMENT_DATE_PUBLISHED">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de modificaci&oacute;n
                            <input type="date" id="txtDOCUMENT_DATE_MODIFIED" value="" pattern="fecha" title="Fecha en que se modific&oacute; por &uacute;ltima vez el documento."/>
                            <span class="form-error" data-form-error-for="txtDOCUMENT_DATE_MODIFIED" id="spanDOCUMENT_DATE_MODIFIED">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Formato
                            <input type="text" id="txtDOCUMENT_FORMAT" placeholder="p. ej. text/html" value="" title="El formato del documento, utilizando la lista de c&oacute;digos abierta [IANA Media Types] (vea los valores en la columna &#39;Template&#39;), o utilizando el c&oacute;digo &#39;offline/print&#39;  si el documento descrito se publica offline."/>
                            <span class="form-error" data-form-error-for="txtDOCUMENT_FORMAT" id="spanDOCUMENT_FORMAT">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Idioma
                            <select id="cmbDOCUMENT_LANGUAGE" title="El idioma predeterminado de los datos con dos letras [ISO639-1].">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${tiposDeIdioma}" var="idioma">
                                    <option value="${idioma.codigo}" >${idioma.nombre}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbDOCUMENT_LANGUAGE" id="spanDOCUMENT_LANGUAGE">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-document"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-document"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un documento (BOTTOM) --%> 
            
<script>
                
    function validarFormulario(idFormulario)
    { 
        
        let strMensajeFrm;          
        let strTituloMensaje;       
        let bolTipoFormulario;      

        bolTipoFormulario = document.getElementById("txtEsFormularioDeEdicion").value;
        bolTipoFormulario = Boolean(bolTipoFormulario);
        strMensajeFrm = (bolTipoFormulario === true) ? "\u00bfLos datos son correctos\u003F" : "Una vez guardada la informaci\u00F3n de esta secci\u00F3n no se podr\u00E1 volver a modificar los valores ingresados hasta que se finalice con el proceso de registro de la contrataci\u00F3n p\u00FAblica.";
        strTituloMensaje = (bolTipoFormulario === true) ? "Confirmar" : "\u00bfLos datos son correctos\u003F";

        swal({
            title: strTituloMensaje,
            text: strMensajeFrm,
            icon: "warning",
            buttons: ["Cancelar", "Aceptar"],
            dangerMode: true,
            closeOnClickOutside: false,
            closeOnEsc: false
        })
        .then((seAcepta) => {
            if (seAcepta)
            { 
                document.getElementById(idFormulario).submit();
            } 
            else
            { 
                swal("Se ha cancelado la operaci\u00f3n", {
                    icon: "error",
                    buttons: false,
                    timer: 2000
                });
                return false;
            } 
        });
                    
    } 
    
    window.onload = function (){
        
        document.getElementById("estructura-por-defecto-document").value = obtenerDatosInicialesElementos("document", []);
        
    };
    
</script> 
