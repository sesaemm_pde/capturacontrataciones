<%-- 
    Autor: Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>
<div id="seccion7-bloque1">
    <h4>Informaci&oacute;n general</h4>
    <div class="callout padding-horizontal-3">
        <form name="frmRegistrarSeccionLicitacionBloqueInformacionGeneral" id="frmRegistrarSeccionLicitacionBloqueInformacionGeneral" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post" data-abide novalidate onSubmit="return false;" autocomplete="off">
            <input type="hidden" name="accion" value="${(bolEsFormulariosDeEdicion == true) ? 'editarSeccion' : 'registrarSeccion'}"/>
            <input type="hidden" name="txtEsFormularioDeEdicion" id="txtEsFormularioDeEdicion" value="${bolEsFormulariosDeEdicion}"/>
            <input type="hidden" name="txtNUMERO_SECCION" value="7"/>
            <input type="hidden" name="txtNUMERO_BLOQUE" value="1"/>
            <input type="hidden" name="txtID_CONTRATACION_SPDN" value="${idContratacion}"/>
            <div class="grid-x grid-margin-x">
                <label class="cell medium-6">ID  <span style="color: red">*</span>
                    <input type="text" name="txtID" id="txtID" placeholder="p. ej. 1" required value="${contratacion.tender.id}" title="Un identificador para el proceso de licitaci&oacute;n. Esto puede ser igual al ocid, o un identificador interno para esta licitaci&oacute;n."/>
                    <span class="form-error" data-form-error-for="txtID" id="spanID">
                        Campo requerido, verifica el formato de tu registro.
                    </span>
                </label>
                <label class="cell medium-6">T&iacute;tulo de la licitaci&oacute;n
                    <input type="text" name="txtTITLE" id="txtTITLE" placeholder="p. ej. T&iacute;tulo de la licitaci&oacute;n" value="${contratacion.tender.title}" title="Un t&iacute;tulo para esta licitaci&oacute;n. &Eacute;ste se usar&aacute; por aplicaciones como una cabecera para atraer inter&eacute;s y ayudar a los analistas a entender la naturaleza de esta contrataci&oacute;n."/>
                    <span class="form-error" data-form-error-for="txtTITLE" id="spanTITLE">
                        Campo requerido, verifica el formato de tu registro.
                    </span>
                </label>
                <label class="cell medium-6">Descripci&oacute;n de la licitaci&oacute;n
                    <input type="text" name="txtDESCRIPTION" id="txtDESCRIPTION" placeholder="p. ej. Descripci&oacute;n de la licitaci&oacute;n" value="${contratacion.tender.description}" title="Una descripci&oacute;n breve de la licitaci&oacute;n. Esto complementa cualquier informaci&oacute;n estructurada que se da utilizando la lista de art&iacute;culos. Las descripciones deben de ser cortas y f&aacute;ciles de leer. Evite utilizar MAY&Uacute;SCULAS."/>
                    <span class="form-error" data-form-error-for="txtDESCRIPTION" id="spanDESCRIPTION">
                        Campo requerido, verifica el formato de tu registro.
                    </span>
                </label>
                <label class="cell medium-6">Estado de la licitaci&oacute;n
                    <select name="cmbSTATUS" id="cmbSTATUS" title="El estatus actual de la licitaci&oacute;n.">
                        <option value="">--Elige una opci&oacute;n--</option>
                        <c:forEach items="${estadosLicitacion}" var="estado">
                            <option value="${estado.clave}" ${(contratacion.tender.status == estado.clave) ? 'selected' : ''}>${estado.valor}</option>
                        </c:forEach>
                    </select>
                    <span class="form-error" data-form-error-for="cmbSTATUS" id="spanSTATUS">
                        Elige una opci&oacute;n.
                    </span>
                </label>
                <fieldset class="fieldset cell">
                    <legend title="La entidad que gestiona el proceso de contrataci&oacute;n. Esta puede ser distinta del comprador que paga/usa los art&iacute;culos adquiridos.">Entidad contratante</legend>
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-6">ID de Entidad <span style="color: red">**</span>
                            <input type="text" id="txtPROCURING_ENTITY_ID" name="txtPROCURING_ENTITY_ID" required placeholder="p. ej. 1" value="${contratacion.tender.procuringEntity.id}" title="El ID utilizado para hacer referencia a esta parte involucrada desde otras secciones de la entrega. Este campo puede construirse con la siguiente estructura {identifier.scheme}-{identifier.id}(-{department-identifier})."/>
                            <span class="form-error" data-form-error-for="txtPROCURING_ENTITY_ID" id="spanPROCURING_ENTITY_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Nombre
                            <input type="text" id="txtPROCURING_ENTITY_NAME" name="txtPROCURING_ENTITY_NAME" placeholder="p. ej. Nombre com&uacute;n" value="${contratacion.tender.procuringEntity.name}" title="Un nombre com&uacute;n para esta organizaci&oacute;n u otro participante en el proceso de contrataciones. El objeto identificador da un espacio para un nombre legal formal, y esto podr&iacute;a repetir el valor o dar un nombre com&uacute;n por el cual se conoce a la organizaci&oacute;n o entidad. Este campo tambi&eacute;n pude incluir detalles del departamento o sub-unidad involucrada en este proceso de contrataciones."/>
                            <span class="form-error" data-form-error-for="txtPROCURING_ENTITY_NAME" id="spanNAME">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <fieldset class="cell fieldset">
                            <legend title="El identificador primario para esta organizaci&oacute;n o participante. Son preferibles los identificadores que denotan de forma &uacute;nica a una entidad legal. Consulta la [gu&iacute;a de identificadores de organizaci&oacute;n] para el esquema e identificador preferido.">Identificador principal</legend>
                            <div class="grid-margin-x grid-x">
                                <label class="cell medium-6">ID
                                    <input type="text" id="txtPROCURING_ENTITY_IDENTIFIER_ID" name="txtPROCURING_ENTITY_IDENTIFIER_ID" placeholder="p. ej. 1" value="${contratacion.tender.procuringEntity.identifier.id}" title="El identificador de la organizaci&oacute;n en el esquema seleccionado."/>
                                    <span class="form-error" data-form-error-for="txtPROCURING_ENTITY_IDENTIFIER_ID" id="spanPROCURING_ENTITY_IDENTIFIER_ID">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Esquema
                                    <input type="text" id="txtPROCURING_ENTITY_IDENTIFIER_SCHEME" name="txtPROCURING_ENTITY_IDENTIFIER_SCHEME" placeholder="p. ej. Esquema" value="${contratacion.tender.procuringEntity.identifier.scheme}" title="Los identificadores de organizaci&oacute;n deben de obtenerse de una lista de identificadores de organizaciones existentes. El campo scheme se usa para indicar o registrar de d&oacute;nde se toma el identificador. Este valor deber&iacute;a tomarse del [Esquema de Identificadores de Organizaciones]."/>
                                    <span class="form-error" data-form-error-for="txtPROCURING_ENTITY_IDENTIFIER_SCHEME" id="spanPROCURING_ENTITY_IDENTIFIER_SCHEME">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">Nombre Legal
                                    <input type="text" id="txtPROCURING_ENTITY_IDENTIFIER_LEGAL_NAME" name="txtPROCURING_ENTITY_IDENTIFIER_LEGAL_NAME" placeholder="p. ej. Nombre Legal" value="${contratacion.tender.procuringEntity.identifier.legalName}" title="El nombre legalmente registrado de la organizaci&oacute;n." />
                                    <span class="form-error" data-form-error-for="txtPROCURING_ENTITY_IDENTIFIER_LEGAL_NAME" id="spanPROCURING_ENTITY_IDENTIFIER_LEGAL_NAME">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-6">URI
                                    <input type="text" id="txtPROCURING_ENTITY_IDENTIFIER_URI" name="txtPROCURING_ENTITY_IDENTIFIER_URI" placeholder="p. ej. www.url.com" value="${contratacion.tender.procuringEntity.identifier.uri}" pattern="url" title="Una URI para identificar a la organizaci&oacute;n, como los prove&iacute;dos por [Open Corporates] o alg&uacute;n otro proveedor relevante de URIs. Este campo no debe ser utilizado para especificar el sitio web de la organizaci&oacute;n, el cual puede ser especificado en el campo URL del punto de contacto de la Organizaci&oacute;n."/>
                                    <span class="form-error" data-form-error-for="txtPROCURING_ENTITY_IDENTIFIER_URI" id="spanPROCURING_ENTITY_IDENTIFIER_URI">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="cell fieldset"> 
                            <input type="hidden" id="estructura-por-defecto-identifier" value=""> 
                            <input type="hidden" id="siguiente-identificador-identifier" value="" data-valor-por-defecto=""> 
                            <input type="hidden" id="lista-identificadores-por-registrar-identifier" name="lista-identificadores-por-registrar-identifier" value="" data-valor-por-defecto="">
                            <legend title="Una lista adicional/suplementaria de identificadores para la organizaci�n, utilizando la [gu�a de identificadores de organizaciones]. Esto puede usarse para dar un identificador utilizado internamente por esta organizaci�n adem�s del identificador primario legal de la entidad.">Identificadores adicionales</legend>
                            <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                <a onclick="restaurarEstadoInicialElemento('identifier', [])"><i class="material-icons secondary">restart_alt</i></a> 
                                <a onclick="abrirNuevoFormulario('identifier', [])"><i class="material-icons secondary">add_circle</i></a> 
                            </div>
                            <c:choose>
                                <c:when test="${contratacion.tender.procuringEntity.additionalIdentifiers.size() > 0}">
                                    <c:set var="contadorElementosIdentifier" value="0"></c:set>
                                    <div id="contenedor-identifier" style="display: flex; flex-direction: column; gap: 10px;">
                                    <c:forEach items="${contratacion.tender.procuringEntity.additionalIdentifiers}" var="identifier">
                                        <div id="identifier_${contadorElementosIdentifier}">
                                            <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                                <div class="cell medium-8">
                                                    <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">identificador</span> - <span id="titulo-elemento-identifier_${contadorElementosIdentifier}">${identifier.id}</span></h6>
                                                </div>
                                                <div id="datos-elemento-identifier_${contadorElementosIdentifier}">
                                                    <input type="hidden" name="txtIDENTIFIER_ID_${contadorElementosIdentifier}" id="txtIDENTIFIER_ID_${contadorElementosIdentifier}" value="${identifier.id}" >
                                                    <input type="hidden" name="txtIDENTIFIER_SCHEME_${contadorElementosIdentifier}" id="txtIDENTIFIER_SCHEME_${contadorElementosIdentifier}" value="${identifier.scheme}" >
                                                    <input type="hidden" name="txtIDENTIFIER_LEGAL_NAME_${contadorElementosIdentifier}" id="txtIDENTIFIER_LEGAL_NAME_${contadorElementosIdentifier}" value="${identifier.legalName}" >
                                                    <input type="hidden" name="txtIDENTIFIER_URI_${contadorElementosIdentifier}" id="txtIDENTIFIER_URI_${contadorElementosIdentifier}" value="${identifier.uri}" >                                                        
                                                </div>
                                                <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                    <button class="button margin-0" onclick="abrirFormularioPrevio('identifier', [${contadorElementosIdentifier}])"><i class="material-icons">edit</i> Editar</button>
                                                    <button class="button margin-0" onclick="eliminarElemento('identifier', 'identifier_${contadorElementosIdentifier}', [], '${contadorElementosIdentifier}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                                </div>
                                            </div>
                                        </div>
                                        <c:set var="contadorElementosIdentifier" value="${contadorElementosIdentifier + 1}"></c:set>
                                    </c:forEach>
                                    </div>
                                    <input type="hidden" name="ultimo-identificador-elementos-identifier" id="ultimo-identificador-elementos-identifier" value="${contadorElementosIdentifier}">
                                </c:when>
                                <c:otherwise>
                                    <div id="contenedor-identifier" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                    <input type="hidden" name="ultimo-identificador-elementos-identifier" id="ultimo-identificador-elementos-identifier" value="0">
                                </c:otherwise>
                            </c:choose>
                            <script>
                                document.getElementById("estructura-por-defecto-identifier").value = obtenerDatosInicialesElementos("identifier", []);
                            </script>
                        </fieldset>
                        <fieldset class="cell fieldset">
                            <legend title="Una direcci&oacute;n. Esta puede ser la direcci&oacute;n legalmente registrada de la organizaci&oacute;n o puede ser una direcci&oacute;n donde se reciba correspondencia para este proceso de contrataci&oacute;n particular.">Direcci&oacute;n</legend>
                            <div class="grid-margin-x grid-x">
                                <label class="cell medium-4">Direcci&oacute;n
                                    <input type="text" id="txtPROCURING_ENTITY_ADDRESS_STREET_ADDRESS" name="txtPROCURING_ENTITY_ADDRESS_STREET_ADDRESS" placeholder="p. ej. 1600 Amphitheatre Pkwy." value="${contratacion.tender.procuringEntity.address.streetAddress}" title="La direcci&oacute;n de la calle."/>
                                    <span class="form-error" data-form-error-for="txtPROCURING_ENTITY_ADDRESS_STREET_ADDRESS" id="spanPROCURING_ENTITY_ADDRESS_STREET_ADDRESS">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Localidad
                                    <input type="text" id="txtPROCURING_ENTITY_ADDRESS_LOCALITY" name="txtPROCURING_ENTITY_ADDRESS_LOCALITY" placeholder="p. ej. Mountain View." value="${contratacion.tender.procuringEntity.address.locality}" title="La localidad." />
                                    <span class="form-error" data-form-error-for="txtPROCURING_ENTITY_ADDRESS_LOCALITY" id="spanPROCURING_ENTITY_ADDRESS_LOCALITY">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Regi&oacute;n
                                    <input type="text" id="txtPROCURING_ENTITY_ADDRESS_REGION" name="txtPROCURING_ENTITY_ADDRESS_REGION" placeholder="p. ej. CA." value="${contratacion.tender.procuringEntity.address.region}" title="La regi&oacute;n."/>
                                    <span class="form-error" data-form-error-for="txtPROCURING_ENTITY_ADDRESS_REGION" id="spanPROCURING_ENTITY_ADDRESS_REGION">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">C&oacute;digo postal
                                    <input type="text" id="txtPROCURING_ENTITY_ADDRESS_POSTAL_CODE" name="txtPROCURING_ENTITY_ADDRESS_POSTAL_CODE" placeholder="p. ej. 94043" value="${contratacion.tender.procuringEntity.address.postalCode}" pattern="codigo_postal" title="El c&oacute;digo postal." />
                                    <span class="form-error" data-form-error-for="txtPROCURING_ENTITY_ADDRESS_POSTAL_CODE" id="spanPROCURING_ENTITY_ADDRESS_POSTAL_CODE">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Pa&iacute;s
                                    <input type="text" id="txtPROCURING_ENTITY_ADDRESS_COUNTRY_NAME" name="txtPROCURING_ENTITY_ADDRESS_COUNTRY_NAME" placeholder="p. ej. M&eacute;xico" value="${contratacion.tender.procuringEntity.address.countryName}" title="El nombre del pa&iacute;s." />
                                    <span class="form-error" data-form-error-for="txtPROCURING_ENTITY_ADDRESS_COUNTRY_NAME" id="spanPROCURING_ENTITY_ADDRESS_COUNTRY_NAME">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="cell fieldset">
                            <legend title="Detalles de contacto que pueden usarse para esta parte involucrada.">Punto de contacto</legend>
                            <div class="grid-margin-x grid-x">
                                <label class="cell medium-4">Nombre
                                    <input type="text" id="txtPROCURING_ENTITY_CONTACT_NAME" name="txtPROCURING_ENTITY_CONTACT_NAME" placeholder="p. ej. Carlos" value="${contratacion.tender.procuringEntity.contactPoint.name}" title="El nombre de la persona de contacto, departamento o punto de contacto en relaci&oacute;n a este proceso de contrataci&oacute;n."/>
                                    <span class="form-error" data-form-error-for="txtPROCURING_ENTITY_CONTACT_NAME" id="spanPROCURING_ENTITY_CONTACT_NAME">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Correo electr&oacute;nico
                                    <input type="text" id="txtPROCURING_ENTITY_CONTACT_EMAIL" name="txtPROCURING_ENTITY_CONTACT_EMAIL" placeholder="p. ej. email@gmail.com" value="${contratacion.tender.procuringEntity.contactPoint.email}" title="La direcci&oacute;n de correo del punto o persona de contacto."/>
                                    <span class="form-error" data-form-error-for="txtPROCURING_ENTITY_CONTACT_EMAIL" id="spanPROCURING_ENTITY_CONTACT_EMAIL">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">Tel&eacute;fono
                                    <input type="text" id="txtPROCURING_ENTITY_CONTACT_TELEPHONE" name="txtPROCURING_ENTITY_CONTACT_TELEPHONE" placeholder="p. ej. 5555555555 o 555-555-5555" value="${contratacion.tender.procuringEntity.contactPoint.telephone}" title="El n&uacute;mero de tel&eacute;fono del punto o persona de contacto. Este debe de incluir el c&oacute;digo de marcaci&oacute;n internacional." />
                                    <span class="form-error" data-form-error-for="txtPROCURING_ENTITY_CONTACT_TELEPHONE" id="spanPROCURING_ENTITY_CONTACT_TELEPHONE">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">N&uacute;mero de fax
                                    <input type="text" id="txtPROCURING_ENTITY_CONTACT_FAXNUMBER" name="txtPROCURING_ENTITY_CONTACT_FAXNUMBER" placeholder="p. ej. 5555555555 o 555-555-5555" value="${contratacion.tender.procuringEntity.contactPoint.faxNumber}" title="El n&uacute;mero de fax del punto o persona de contacto. Este debe de incluir el c&oacute;digo de marcaci&oacute;n internacional."/>
                                    <span class="form-error" data-form-error-for="txtPROCURING_ENTITY_CONTACT_FAXNUMBER" id="spanPROCURING_ENTITY_CONTACT_FAXNUMBER">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                                <label class="cell medium-4">URL
                                    <input type="text" id="txtPROCURING_ENTITY_CONTACT_URL" name="txtPROCURING_ENTITY_CONTACT_URL" placeholder="p. ej. www.url.com" value="${contratacion.tender.procuringEntity.contactPoint.url}" pattern="url" title="Una direcci&oacute;n web para el punto o persona de contacto."/>
                                    <span class="form-error" data-form-error-for="txtPROCURING_ENTITY_CONTACT_URL" id="spanPROCURING_ENTITY_CONTACT_URL">
                                        Campo requerido, verifica el formato de tu registro.
                                    </span>
                                </label>
                            </div>
                        </fieldset>
                    </div>
                </fieldset>
                <fieldset class="fieldset cell medium-6">
                    <legend title="El valor m&aacute;ximo estimado de la contrataci&oacute;n. Un valor negativo indica que el proceso de contrataci&oacute;n puede incluir pagos del proveedor al comprador (usado com&uacute;nmente en contratos de concesi&oacute;n).">Valor</legend>
                    <div class="grid-margin-x grid-x">
                        <label class="cell">Monto
                            <input type="number" name="txtVALUE_AMOUNT" id="txtVALUE_AMOUNT" placeholder="p. ej. 0" min="0" value="${contratacion.tender.value.amount}" title="Monto como una cifra."/>
                            <span class="form-error" data-form-error-for="txtVALUE_AMOUNT" id="spanVALUE_AMOUNT">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell">Moneda
                            <select name="txtVALUE_CURRENCY" id="txtVALUE_CURRENCY" title="La moneda del monto.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${tipoMoneda}" var="moneda">
                                    <option value="${moneda.clave}" ${(contratacion.tender.value.currency == moneda.clave) ? 'selected' : ''}>${moneda.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="txtVALUE_CURRENCY" id="spanVALUE_CURRENCY">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                    </div>
                </fieldset>
                <fieldset class="fieldset cell medium-6">
                    <legend title="El valor m&iacute;nimo estimado de la contrataci&oacute;n. Un valor negativo indica que el proceso de contrataci&oacute;n puede incluir pagos del proveedor al comprador (usado com&uacute;nmente en contratos de concesi&oacute;n).">Valor m&iacute;nimo</legend>
                    <div class="grid-margin-x grid-x">
                        <label class="cell">Monto
                            <input type="number" name="txtMIN_VALUE_AMOUNT" id="txtMIN_VALUE_AMOUNT" placeholder="p. ej. 0" min="0" value="${contratacion.tender.value.amount}" title="Monto como una cifra."/>
                            <span class="form-error" data-form-error-for="txtMIN_VALUE_AMOUNT" id="spanMIN_VALUE_AMOUNT">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell">Moneda
                            <select name="txtMIN_VALUE_CURRENCY" id="txtMIN_VALUE_CURRENCY" title="La moneda del monto.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${tipoMoneda}" var="moneda">
                                    <option value="${moneda.clave}" ${(contratacion.tender.minValue.currency == moneda.clave) ? 'selected' : ''}>${moneda.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="txtMIN_VALUE_CURRENCY" id="spanMIN_VALUE_CURRENCY">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                    </div>
                </fieldset>
                <label class="cell medium-4">M&eacute;todo de contrataci&oacute;n
                    <select name="cmbPROCUREMENT_METHOD" id="cmbPROCUREMENT_METHOD" title="El m&eacute;todo de adquisici&oacute;n.">
                        <option value="">--Elige una opci&oacute;n--</option>
                        <c:forEach items="${metodosLicitacion}" var="metodo">
                            <option value="${metodo.clave}" ${(contratacion.tender.procurementMethod == metodo.clave) ? 'selected' : ''}>${metodo.valor}</option>
                        </c:forEach>
                    </select>
                    <span class="form-error" data-form-error-for="cmbPROCUREMENT_METHOD" id="spanPROCUREMENT_METHOD">
                        Elige una opci&oacute;n.
                    </span>
                </label>
                <label class="cell medium-4">Detalles del m&eacute;todo de contrataci&oacute;n 
                    <input type="text" name="txtPROCUREMENT_METHOD_DETAILS" id="txtPROCUREMENT_METHOD_DETAILS" placeholder="p. ej. Detalles adicionales" value="${contratacion.tender.procurementMethodDetails}" title="Detalles adicionales sobre el m&eacute;todo de adquisiciones utilizado. Este campo puede usarse para dar el nombre local del m&eacute;todo de adquisici&oacute;n particular que se utiliz&oacute;."/>
                    <span class="form-error" data-form-error-for="txtPROCUREMENT_METHOD_DETAILS" id="spanPROCUREMENT_METHOD_DETAILS">
                        Campo requerido, verifica el formato de tu registro.
                    </span>
                </label>
                <label class="cell medium-4">Justificaci&oacute;n para el m&eacute;todo de contrataci&oacute;n
                    <input type="text" name="txtPROCUREMENT_METHOD_RATIONALE" id="txtPROCUREMENT_METHOD_RATIONALE" placeholder="p. ej. Justificaci&oacute;n para el m&eacute;todo elegido" value="${contratacion.tender.procurementMethodRationale}" title="Justificaci&oacute;n para el m&eacute;todo de compra elegido. Esto es especialmente importante para dar una justificaci&oacute;n en el caso de licitaciones limitadas o adjudicaciones directas."/>
                    <span class="form-error" data-form-error-for="txtPROCUREMENT_METHOD_RATIONALE" id="spanPROCUREMENT_METHOD_RATIONALE">
                        Campo requerido, verifica el formato de tu registro.
                    </span>
                </label>
                <fieldset class="cell medium-6">
                    <div class="grid-x grid-margin-x">
                        <label class="cell">Categor&iacute;a principal de contrataci&oacute;n
                            <select name="cmbMAIN_PROCUREMENT_CATEGORY" id="cmbMAIN_PROCUREMENT_CATEGORY" title="La principal categor&iacute;a describiendo el objeto principal de este proceso de contrataciones.">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${categoriasPrincipales}" var="categoriaPrincipal">
                                    <option value="${categoriaPrincipal.clave}" ${(contratacion.tender.mainProcurementCategory == categoriaPrincipal.clave) ? 'selected' : ''} >${categoriaPrincipal.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbMAIN_PROCUREMENT_CATEGORY" id="spanMAIN_PROCUREMENT_CATEGORY">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                        <fieldset class="cell fieldset">
                            <legend title="Cualquier categor&iacute;a adicional que describe los objetos del proceso de contrataciones.">Categor&iacute;as adicionales de contrataci&oacute;n</legend> 
                            <c:forEach items="${categoriasAdicionales}" var="categoriaAdicional">
                                <c:set var="categoriaSeleccionada" value="false" scope="request" />
                                <c:forEach items="${contratacion.tender.additionalProcurementCategories}" var="categoriaAdicional_seleccionada">
                                    <c:if test="${categoriaAdicional_seleccionada == categoriaAdicional.clave}">
                                        <c:set var="categoriaSeleccionada" value="true" scope="request" />
                                    </c:if>
                                </c:forEach>
                                <label class="text-truncate">
                                    <input type="checkbox" name="chckADD_PROCUREMENT_CATEGORIES" value="${categoriaAdicional.clave}" ${categoriaSeleccionada ? 'checked' : ''} />
                                    ${categoriaAdicional.valor}
                                </label>
                            </c:forEach>
                            <span class="form-error" id="spanADD_PROCUREMENT_CATEGORIES">
                                Campo requerido, selecciona al menos una opci&oacute;n.
                            </span>
                        </fieldset>
                    </div>
                </fieldset>
                <fieldset class="cell medium-6">
                    <div class="grid-x grid-margin-x">
                        <label class="cell">Detalles del m&eacute;todo de presentaci&oacute;n
                            <input type="text" name="txtSUBMISSION_METHOD_DETAILS" id="txtSUBMISSION_METHOD_DETAILS" placeholder="p. ej. Cualquier detalle o informaci&oacute;n sobre el m&eacute;todo de entrega" value="${contratacion.tender.submissionMethodDetails}" title="Cualquier detalle o m&aacute;s informaci&oacute;n sobre el m&eacute;todo de entrega. Esto puede incluir la direcci&oacute;n, correo electr&oacute;nico o servicio en l&iacute;nea en el cual se entregan las ofertas, y cualquier requisito especial que debe de seguirse para entregarlas."/>
                            <span class="form-error" data-form-error-for="txtSUBMISSION_METHOD_DETAILS" id="spanSUBMISSION_METHOD_DETAILS">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <fieldset class="cell fieldset">
                            <legend title="Los m&eacute;todos por los cuales se entregan las ofertas.">M&eacute;todo de presentaci&oacute;n</legend> 
                            <c:forEach items="${tiposDePresentacionDeMetodos}" var="tipoPresentacion">
                                <c:set var="presentacionSeleccionada" value="false" scope="request" />
                                <c:forEach items="${contratacion.tender.submissionMethod}" var="presentacion_seleccionada">
                                    <c:if test="${presentacion_seleccionada == tipoPresentacion.clave}">
                                        <c:set var="presentacionSeleccionada" value="true" scope="request" />
                                    </c:if>
                                </c:forEach>
                                <label class="text-truncate">
                                    <input type="checkbox" name="chckSUBMISSION_METHOD" value="${tipoPresentacion.clave}" ${presentacionSeleccionada ? 'checked' : ''} />
                                    ${tipoPresentacion.valor}
                                </label>
                            </c:forEach>
                            <span class="form-error" id="spanSUBMISSION_METHOD">
                                Campo requerido, selecciona al menos una opci&oacute;n.
                            </span>
                        </fieldset>
                    </div>
                </fieldset>
                <label class="cell medium-6">Criterios de adjudicaci&oacute;n
                    <select name="cmbAWARD_CRITERIA" id="cmbAWARD_CRITERIA" title="Los criterios de adjudicaci&oacute;n para esta compra.">
                        <option value="">--Elige una opci&oacute;n--</option>
                        <c:forEach items="${criterioDeAdjudicacion}" var="criterio">
                            <option value="${criterio.clave}" ${(contratacion.tender.awardCriteria == criterio.clave) ? 'selected' : ''}>${criterio.valor}</option>
                        </c:forEach>
                    </select>
                    <span class="form-error" data-form-error-for="cmbAWARD_CRITERIA" id="spanAWARD_CRITERIA">
                        Elige una opci&oacute;n.
                    </span>
                </label>
                <label class="cell medium-6">Detalles de los criterios de adjudicaci&oacute;n
                    <input type="text" name="txtAWARD_CRITERIA_DETAILS" id="txtAWARD_CRITERIA_DETAILS" placeholder="p. ej. Cualquier detalle o informaci&oacute;n adicional" value="${contratacion.tender.awardCriteriaDetails}" title="Cualquier detalle o informaci&oacute;n adicional sobre la adjudicaci&oacute;n o criterios de selecci&oacute;n."/>
                    <span class="form-error" data-form-error-for="txtAWARD_CRITERIA_DETAILS" id="spanAWARD_CRITERIA_DETAILS">
                        Campo requerido, verifica el formato de tu registro.
                    </span>
                </label>
                <fieldset class="cell medium-6 fieldset">
                    <legend title="Campo para indicar si se recibieron consultas durante el proceso de licitaci&oacute;n. La informaci&oacute;n estructurada sobre preguntas que se recibieron y sus respuestas pueden proveerse usando la extensi&oacute;n de consultas.">&#191;Tiene consultas&#63;</legend>
                    <div class="grid-x grid-margin-x">
                        <div class="cell">
                            <input type="radio" name="radHAS_ENQUIRIES" value="si" id="radHAS_ENQUIRIES_TRUE" ${(contratacion.tender.hasEnquiries == true) ? 'checked': ''}><label for="radHAS_ENQUIRIES_TRUE">Si</label>
                        </div>
                        <div class="cell">
                            <input type="radio" name="radHAS_ENQUIRIES" value="no" id="radHAS_ENQUIRIES_FALSE" ${(contratacion.tender.hasEnquiries == false) ? 'checked': ''}><label for="radHAS_ENQUIRIES_FALSE">No</label>
                        </div>
                    </div>
                </fieldset>
                <label class="cell medium-6">Criterios de elegibilidad
                    <input type="text" name="txtELIGIBILITY_CRITERIA" id="txtELIGIBILITY_CRITERIA" placeholder="p. ej. Descripci&oacute;n de los criterios de elegibilidad para proveedores" value="${contratacion.tender.eligibilityCriteria}" title="Una descripci&oacute;n de los criterios de elegibilidad para proveedores potenciales. Requisitos y condiciones que deben cumplir los interesados para participar en el procedimiento de contrataci&oacute;n."/>
                    <span class="form-error" data-form-error-for="txtELIGIBILITY_CRITERIA" id="spanELIGIBILITY_CRITERIA">
                        Campo requerido, verifica el formato de tu registro.
                    </span>
                </label>
                <fieldset class="fieldset cell">
                    <legend title="Una enmienda de licitaci&oacute;n es un cambio formal a los detalles de la misma y generalmente implica la publicaci&oacute;n de una nueva entrega o aviso de una nueva licitaci&oacute;n. La justificaci&oacute;n y descripci&oacute;n de los cambios realizados pueden ser prove&iacute;dos aqu&iacute;.">Enmienda</legend>
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-4">ID <span style="color: red">**</span>
                            <input type="text" id="txtAMENDMENT_ID" name="txtAMENDMENT_ID" placeholder="p. ej. 1" value="${contratacion.tender.amendment.id}" title="Un identificador para esta enmienda: com&uacute;nmente el n&uacute;mero de enmienda."/>
                            <span class="form-error" data-form-error-for="txtAMENDMENT_ID" id="spanAMENDMENT_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de enmienda
                            <input type="date" id="txtAMENDMENT_DATE" name="txtAMENDMENT_DATE" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(contratacion.tender.amendment.date)}" title="La fecha de esta enmienda." />
                            <span class="form-error" data-form-error-for="txtAMENDMENT_DATE" id="spanAMENDMENT_DATE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Justificaci&oacute;n
                            <input type="text" id="txtAMENDMENT_RATIONALE" name="txtAMENDMENT_RATIONALE" placeholder="p. ej. Explicaci&oacute;n de la enmienda" value="${contratacion.tender.amendment.rationale}" title="Una explicaci&oacute;n de la enmienda."/>
                            <span class="form-error" data-form-error-for="txtAMENDMENT_RATIONALE" id="spanAMENDMENT_RATIONALE">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Descripci&oacute;n
                            <input type="text" id="txtAMENDMENT_DESCRIPTION" name="txtAMENDMENT_DESCRIPTION" placeholder="p. ej. Descripci&oacute;n de la enmienda" value="${contratacion.tender.amendment.description}" title="Un texto libre o semi estructurado, describiendo los cambios hechos en esta enmienda."/>
                            <span class="form-error" data-form-error-for="txtAMENDMENT_DESCRIPTION" id="spanAMENDMENT_DESCRIPTION">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Entrega enmendada (identificador)
                            <input type="text" id="txtAMENDMENT_AMENDS_RELEASE_ID" name="txtAMENDMENT_AMENDS_RELEASE_ID" placeholder="p. ej. 1" value="${contratacion.tender.amendment.amendsReleaseID}" title="Provee el identificador (release.id) de la entrega OCDS (de este proceso de contrataci&oacute;n) que provee los valores para este proceso de contrataci&oacute;n &#42;&#42;antes&#42;&#42; de realizada la enmienda."/>
                            <span class="form-error" data-form-error-for="txtAMENDMENT_AMENDS_RELEASE_ID" id="spanAMENDMENT_AMENDS_RELEASE_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Entrega de enmienda (identificador)
                            <input type="text" id="txtAMENDMENT_RELEASE_ID" name="txtAMENDMENT_RELEASE_ID" placeholder="p. ej. 1" value="${contratacion.tender.amendment.releaseID}" title="Provee el identificador (release.id) de la entrega OCDS (de este proceso de contrataci&oacute;n) que provee los valores para este proceso de contrataci&oacute;n &#42;&#42;despu&eacute;s&#42;&#42; de realizada la enmienda." />
                            <span class="form-error" data-form-error-for="txtAMENDMENT_RELEASE_ID" id="spanAMENDMENT_RELEASE_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <fieldset class="cell fieldset"> 
                            <input type="hidden" id="estructura-por-defecto-change" value=""> 
                            <input type="hidden" id="siguiente-identificador-change" value="" data-valor-por-defecto=""> 
                            <input type="hidden" id="lista-identificadores-por-registrar-change" name="lista-identificadores-por-registrar-change" value="" data-valor-por-defecto="">
                            <legend title="Una lista de cambios que describen los campos que cambiaron y sus valores previos.">Campos enmendados</legend>
                            <div class="button-group" style="justify-content: flex-end; gap:10px;"> 
                                <a onclick="restaurarEstadoInicialElemento('change', [])"><i class="material-icons secondary">restart_alt</i></a> 
                                <a onclick="abrirNuevoFormulario('change', [])"><i class="material-icons secondary">add_circle</i></a> 
                            </div>
                            <c:choose>
                                <c:when test="${contratacion.tender.amendment.changes.size() > 0}">
                                    <c:set var="contadorElementosAmendmentChange" value="0"></c:set>
                                    <div id="contenedor-change" style="display: flex; flex-direction: column; gap: 10px;">
                                    <c:forEach items="${contratacion.tender.amendment.changes}" var="amendmentChange">
                                        <div id="change_${contadorElementosAmendmentChange}">
                                            <div class="grid-x" style="background-color: #e8222d; color: #fff;">
                                                <div class="cell medium-8">
                                                    <h6 class="padding-1 margin-0"><span style="text-transform: capitalize;">cambio</span> - <span id="titulo-elemento-change_${contadorElementosAmendmentChange}">${amendmentChange.property}</span></h6>
                                                </div>
                                                <div id="datos-elemento-change_${contadorElementosAmendmentChange}">
                                                    <input type="hidden" name="txtCHANGE_PROPERTY_${contadorElementosAmendmentChange}" id="txtCHANGE_PROPERTY_${contadorElementosAmendmentChange}" value="${amendmentChange.property}">
                                                    <input type="hidden" name="txtCHANGE_FORMATER_VALUE_${contadorElementosAmendmentChange}" id="txtCHANGE_FORMATER_VALUE_${contadorElementosAmendmentChange}" value="${amendmentChange.former_value}">
                                                </div>
                                                <div class="cell medium-4" style="display:flex; justify-content: center; align-items: center; gap: 10px;">
                                                    <button class="button margin-0" onclick="abrirFormularioPrevio('change', [${contadorElementosAmendmentChange}])"><i class="material-icons">edit</i> Editar</button>
                                                    <button class="button margin-0" onclick="eliminarElemento('change', 'change_${contadorElementosAmendmentChange}', [], '${contadorElementosAmendmentChange}')"><i class="material-icons">remove_circle</i> Eliminar</button>
                                                </div>
                                            </div>
                                        </div>
                                        <c:set var="contadorElementosAmendmentChange" value="${contadorElementosAmendmentChange + 1}"></c:set>
                                    </c:forEach>
                                    </div>
                                    <input type="hidden" name="ultimo-identificador-elementos-change" id="ultimo-identificador-elementos-change" value="${contadorElementosAmendmentChange}">
                                </c:when>
                                <c:otherwise>
                                    <div id="contenedor-change" style="display: flex; flex-direction: column; gap: 10px;"></div>
                                    <input type="hidden" name="ultimo-identificador-elementos-change" id="ultimo-identificador-elementos-change" value="0">
                                </c:otherwise>
                            </c:choose>
                            <script>
                                document.getElementById("estructura-por-defecto-change").value = obtenerDatosInicialesElementos("change", []);
                            </script>
                        </fieldset>
                    </div>
                </fieldset>
            </div>
            <div class="grid-x grid-margin-x">
                <fieldset class="cell medium-6">
                    <input type="reset" onclick="reiniciarFormulario();" class="button expanded secondary" name="btnResetLicitacionInformacionGeneral" value="${(bolEsFormulariosDeEdicion == true) ? 'Restaurar' : 'Reiniciar'}"/>
                </fieldset>
                <fieldset class="cell medium-6">
                    <input type="button" onclick="validarFormulario('frmRegistrarSeccionLicitacionBloqueInformacionGeneral');" class="button expanded" name="btnRegistrarLicitacionInformacionGeneral" value="${(bolEsFormulariosDeEdicion == true) ? 'Modificar datos' : 'Continuar'}"/>
                </fieldset>
            </div>
        </form>
        <div data-abide-error="data-abide-error" class="alert callout alertFooter" style="display: none;">
            <p>
                <i class="material-icons">warning</i>
                Existen algunos errores en tu registro del bloque 'Informaci&oacute;n general' perteneciente a la secci&oacute;n 'Licitaci&oacute;n'. Verifica nuevamente.
            </p>
        </div>
    </div>
</div>

<%-- formulario de un cambio en campos enmendados (TOP) --%>
<div id="modalFrmChange" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Campos enmendados</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmChange" id="frmChange" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-change" value="">
                    <div class="grid-margin-x grid-x">
                        <label class='cell medium-6'>Propiedad <span style='color: red'>**</span>
                            <input type='text' required id='txtCHANGE_PROPERTY' placeholder='p. ej. Propiedad' title='El nombre de propiedad que ha sido cambiado con relaci&oacute;n al lugar donde se encuentra la enmienda. Por ejemplo si el valor del contrato ha cambiado.'/>
                            <span class='form-error' data-form-error-for='txtCHANGE_PROPERTY' id='spanCHANGE_PROPERTY'>
                                 Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class='cell medium-6'>Valor anterior
                            <input type='text' id='txtCHANGE_FORMATER_VALUE' placeholder='p. ej. Valor previo' title='El valor previo de la propiedad cambiada en cualquier tipo de propiedad.'/>
                            <span class='form-error' data-form-error-for='txtCHANGE_FORMATER_VALUE' id='spanCHANGE_FORMATER_VALUE'>
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-change"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-change"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un cambio en campos enmendados (BOTTOM) --%>
<%-- formulario de un identificador (TOP) --%>
<div id="modalFrmIdentifier" class="reveal full" data-reveal style="background-color: rgba(0, 0, 0, .7);">
    <div class="grid-container margin-top-2">
        <div class="grid-x grid-margin-x padding-vertical-1 padding-horizontal-2" style="background-color:#fff;">
            <div class="cell">
                <h4 class="margin-vertical-2">Identificador</h4>
                <hr class="hrheader" style="margin: 10px 0px;">
            </div>
            <div class="cell">
                <form name="frmIdentifier" id="frmIdentifier" data-abide novalidate autocomplete="off">
                    <input type="hidden" id="identificador-utilizado-identifier" value="">
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-6">ID <span style="color: red">**</span>
                            <input type="text" required id="txtIDENTIFIER_ID" placeholder="p. ej. 1" value="" title="El identificador de la organizaci&oacute;n en el esquema seleccionado."/>
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_ID" id="spanIDENTIFIER_ID">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Esquema
                            <input type="text" id="txtIDENTIFIER_SCHEME" placeholder="p. ej. Esquema" value="" title="Los identificadores de organizaci&oacute;n deben de obtenerse de una lista de identificadores de organizaciones existentes. El campo scheme se usa para indicar o registrar de d&oacute;nde se toma el identificador. Este valor deber&iacute;a tomarse del [Esquema de Identificadores de Organizaciones]."/>
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_SCHEME" id="spanIDENTIFIER_SCHEME">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Nombre Legal
                            <input type="text" id="txtIDENTIFIER_LEGAL_NAME" placeholder="p. ej. Nombre Legal" value="" title="El nombre legalmente registrado de la organizaci&oacute;n." />
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_LEGAL_NAME" id="spanIDENTIFIER_LEGAL_NAME">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">URI
                            <input type="text" id="txtIDENTIFIER_URI" placeholder="p. ej. www.url.com" value="" pattern="url" title="Una URI para identificar a la organizaci&oacute;n, como los prove&iacute;dos por [Open Corporates] o alg&uacute;n otro proveedor relevante de URIs. Este campo no debe ser utilizado para especificar el sitio web de la organizaci&oacute;n, el cual puede ser especificado en el campo URL del punto de contacto de la Organizaci&oacute;n." />
                            <span class="form-error" data-form-error-for="txtIDENTIFIER_URI" id="spanIDENTIFIER_URI">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6" id="contenedor-btn-cerrar-identifier"></div>
                    <div class="cell medium-6" id="contenedor-btn-agregar-identifier"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- formulario de un identificador (BOTTOM) --%>
                
<script>
                
    function validarFormulario(idFormulario)
    { 
        
        $("#frmRegistrarSeccionLicitacionBloqueInformacionGeneral").foundation('validateForm');
        $("#frmRegistrarSeccionLicitacionBloqueInformacionGeneral").on("formvalid.zf.abide", function (ev, frm){
            
            let strMensajeFrm;          
            let strTituloMensaje;       
            let bolTipoFormulario;      
            
            bolTipoFormulario = document.getElementById("txtEsFormularioDeEdicion").value;
            bolTipoFormulario = Boolean(bolTipoFormulario);
            strMensajeFrm = (bolTipoFormulario === true) ? "\u00bfLos datos son correctos\u003F" : "Una vez guardada la informaci\u00F3n de esta secci\u00F3n no se podr\u00E1 volver a modificar los valores ingresados hasta que se finalice con el proceso de registro de la contrataci\u00F3n p\u00FAblica.";
            strTituloMensaje = (bolTipoFormulario === true) ? "Confirmar" : "\u00bfLos datos son correctos\u003F";
            
            $(".alertFooter").css("display", "none");
            
            swal({
                title: strTituloMensaje,
                text: strMensajeFrm,
                icon: "warning",
                buttons: ["Cancelar", "Aceptar"],
                dangerMode: true,
                closeOnClickOutside: false,
                closeOnEsc: false
            })
            .then((seAcepta) => {
                if (seAcepta)
                { 
                    document.getElementById(idFormulario).submit();
                } 
                else
                { 
                    swal("Se ha cancelado la operaci\u00f3n", {
                        icon: "error",
                        buttons: false,
                        timer: 2000
                    });
                    return false;
                } 
            });
            
        })
        .on("forminvalid.zf.abide", function(ev,frm) {
            $(".alertFooter").css("display", "block");
        });
                    
    } 
    
    function reiniciarFormulario()
    {
        restaurarEstadoInicialElemento('identifier', []);
        restaurarEstadoInicialElemento('change', []);
    }
    
</script> 