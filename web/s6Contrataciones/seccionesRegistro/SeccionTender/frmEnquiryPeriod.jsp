<%-- 
    Autor: Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<div id="seccion7-bloque3">
    <h4>Periodo de consulta</h4>
    <div class="callout padding-horizontal-3">
        <form name="frmRegistrarSeccionLicitacionBloquePeriodoConsulta" id="frmRegistrarSeccionLicitacionBloquePeriodoConsulta" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post" data-abide novalidate onSubmit="event.preventDefault(); validarFormularioLicitacionPeriodoConsulta(this.id);" autocomplete="off">
            <input type="hidden" name="accion" value="${(bolEsFormulariosDeEdicion == true) ? 'editarSeccion' : 'registrarSeccion'}"/>
            <input type="hidden" name="txtEsFormularioDeEdicion" id="txtEsFormularioDeEdicion" value="${bolEsFormulariosDeEdicion}"/>
            <input type="hidden" name="txtNUMERO_SECCION" value="7"/>
            <input type="hidden" name="txtNUMERO_BLOQUE" value="3"/>
            <input type="hidden" name="txtID_CONTRATACION_SPDN" value="${idContratacion}"/>
            <div class="grid-x grid-margin-x">
                <label class="cell medium-6">Fecha de inicio
                    <input type="date" name="txtSTART_DATE" id="txtSTART_DATE" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(contratacion.tender.enquiryPeriod.startDate)}" title="La fecha de inicio del per&iacute;odo. Cuando se sabe, se debe dar una fecha de inicio precisa."/>
                    <span class="form-error" data-form-error-for="txtSTART_DATE" id="spanSTART_DATE">
                        Campo requerido, verifica el formato de tu registro.
                    </span>
                </label>
                <label class="cell medium-6">Fecha de fin
                    <input type="date" name="txtEND_DATE" id="txtEND_DATE" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(contratacion.tender.enquiryPeriod.endDate)}" title="La fecha de conclusi&oacute;n del per&iacute;odo. Cuando se sabe, se debe dar una fecha de conclusi&oacute;n precisa."/>
                    <span class="form-error" data-form-error-for="txtEND_DATE" id="spanEND_DATE">
                        Campo requerido, verifica el formato de tu registro.
                    </span>
                </label>
                <label class="cell medium-6">Extensi&oacute;n m&aacute;xima
                    <input type="date" name="txtMAX_EXTENT_DATE" id="txtMAX_EXTENT_DATE" value="${formateadorDeFechas.obtenerFormatoFechaAnioMesDiaDeFormatoISO8601(contratacion.tender.enquiryPeriod.maxExtentDate)}" title="El per&iacute;odo no puede extenderse despu&eacute;s de esta fecha. Este campo debe usarse para expresar la fecha m&aacute;xima disponible para la extensi&oacute;n o renovaci&oacute;n de este per&iacute;odo."/>
                    <span class="form-error" data-form-error-for="txtMAX_EXTENT_DATE" id="spanMAX_EXTENT_DATE">
                        Campo requerido, verifica el formato de tu registro.
                    </span>
                </label>
                <label class="cell medium-6">Duraci&oacute;n (d&iacute;as)
                    <input type="number" name="txtDURATION_IN_DATE" id="txtDURATION_IN_DATE" value="${contratacion.tender.enquiryPeriod.durationInDays}" min="0" placeholder="p. ej. 0" title="El nivel m&aacute;ximo de duraci&oacute;n de este per&iacute;odo en d&iacute;as."/>
                    <span class="form-error" data-form-error-for="txtDURATION_IN_DATE" id="spanDURATION_IN_DATE">
                        Campo requerido, verifica el formato de tu registro.
                    </span>
                </label>
            </div>
            <div class="grid-x grid-margin-x">
                <fieldset class="cell medium-6">
                    <input type="reset" class="button expanded secondary" name="btnResetPeriodoConsulta" value="${(bolEsFormulariosDeEdicion == true) ? 'Restaurar' : 'Reiniciar'}"/>
                </fieldset>
                <fieldset class="cell medium-6">
                    <input type="submit" class="button expanded" name="btnRegistrarPeriodoConsulta" value="${(bolEsFormulariosDeEdicion == true) ? 'Modificar datos' : 'Continuar'}"/>
                </fieldset>
            </div>
        </form>
        <div data-abide-error="data-abide-error" class="alert callout alertFooter" style="display: none;">
            <p>
                <i class="material-icons">warning</i>
                Existen algunos errores en tu registro del bloque 'Periodo de consulta' perteneciente a la secci&oacute;n 'Licitaci&oacute;n'. Verifica nuevamente.
            </p>
        </div>
    </div>
    
</div>
     
<script>
                
    function validarFormularioLicitacionPeriodoConsulta(idFormulario)
    { 
        
        $("#frmRegistrarSeccionLicitacionBloquePeriodoConsulta").on("formvalid.zf.abide", function (ev, frm){
            
            let strMensajeFrm;          
            let strTituloMensaje;       
            let bolTipoFormulario;      
            
            bolTipoFormulario = document.getElementById("txtEsFormularioDeEdicion").value;
            bolTipoFormulario = Boolean(bolTipoFormulario);
            strMensajeFrm = (bolTipoFormulario === true) ? "\u00bf Los datos son correctos\u003F" : "Una vez guardada la informaci\u00F3n de esta secci\u00F3n no se podr\u00E1 volver a modificar los valores ingresados hasta que se finalice con el proceso de registro de la contrataci\u00F3n p\u00FAblica.";
            strTituloMensaje = (bolTipoFormulario === true) ? "Confirmar" : "\u00bf Los datos son correctos\u003F";
            
            $(".alertFooter").css("display", "none");
            
            swal({
                title: strTituloMensaje,
                text: strMensajeFrm,
                icon: "warning",
                buttons: ["Cancelar", "Aceptar"],
                dangerMode: true,
                closeOnClickOutside: false,
                closeOnEsc: false
            })
            .then((seAcepta) => {
                if (seAcepta)
                { 
                    document.getElementById(idFormulario).submit();
                } 
                else
                { 
                    swal("Se ha cancelado la operaci\u00f3n", {
                        icon: "error",
                        buttons: false,
                        timer: 2000
                    });
                    return false;
                } 
            });
            
        })
        .on("forminvalid.zf.abide", function(ev,frm) {
            $(".alertFooter").css("display", "block");
        });
                    
    } 
    
</script> 
