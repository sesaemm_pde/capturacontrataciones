<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fieldset class="fieldset">
    <legend>Beneficiarios</legend>
    <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
        <dt class="cell callout primary">Nombre de la Organizaci&oacute;n</dt>
        <dd class="cell callout">
        <c:choose>
            <c:when test="${transaction.payee.name != null && transaction.payee.name.equals('') == false}">
                ${transaction.payee.name}
            </c:when>
            <c:otherwise>
                Dato no proporcionado
            </c:otherwise>            
        </c:choose>
        </dd>
        <dt class="cell callout primary">ID de Organizaci&oacute;n</dt>
        <dd class="cell callout">
        <c:choose>
            <c:when test="${transaction.payee.id != null}">
                ${transaction.payee.id}
            </c:when>
            <c:otherwise>
                Dato no proporcionado
            </c:otherwise>            
        </c:choose>
        </dd>
    </dl>
</fieldset>
