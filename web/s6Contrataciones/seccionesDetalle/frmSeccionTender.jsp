<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="tabs-panel" id="panela6v">
    <h3>Licitaci&oacute;n</h3>
    <div class="callout">
        <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
            <dt class="cell callout primary">ID de licitaci&oacute;n</dt>
            <dd class="cell callout">
            <c:choose>
                <c:when test="${contratacion.tender.id != null}">
                    ${contratacion.tender.id}
                </c:when>
                <c:otherwise>
                    Dato no proporcionado
                </c:otherwise>            
            </c:choose>
            </dd> 
            <dt class="cell callout primary">T&iacute;tulo de la licitaci&oacute;n</dt>
            <dd class="cell callout">
            <c:choose>
                <c:when test="${contratacion.tender.title != null && contratacion.tender.title.equals('') == false}">
                    ${contratacion.tender.title}
                </c:when>
                <c:otherwise>
                    Dato no proporcionado
                </c:otherwise>            
            </c:choose>
            </dd>
            <dt class="cell callout primary">Descripci&oacute;n de la licitaci&oacute;n</dt>
            <dd class="cell callout">
            <c:choose>
                <c:when test="${contratacion.tender.description != null && contratacion.tender.description.equals('') == false}">
                    ${contratacion.tender.description}
                </c:when>
                <c:otherwise>
                    Dato no proporcionado
                </c:otherwise>            
            </c:choose>
            </dd>
            <dt class="cell callout primary">Estado de la licitaci&oacute;n</dt>
            <dd class="cell callout">
            <c:choose>
                <c:when test="${contratacion.tender.status != null && contratacion.tender.status.equals('') == false}">
                    ${contratacion.tender.status}
                </c:when>
                <c:otherwise>
                    Dato no proporcionado
                </c:otherwise>            
            </c:choose>
            </dd>
        </dl>
    
        <%@include file="SeccionTender/frmSeccionTenderProcuringEntity.jsp"%>
        <%@include file="SeccionTender/frmSeccionTenderItems.jsp"%>
        
        <dl class="grid-x grid-margin-x medium-up-1 large-up-2">    
            <dt class="cell callout primary">Valor</dt>
            <dd class="cell callout valorMonetario">
            <c:choose>
                <c:when test="${contratacion.tender.value.amount != null}">
                    &#36;${formateador.format(contratacion.tender.value.amount)} 
                    <c:choose> 
                        <c:when test="${contratacion.tender.value.currency != null && contratacion.tender.value.currency.equals('') == false}">
                            ${contratacion.tender.value.currency}
                        </c:when>
                        <c:otherwise>
                            (El tipo de moneda no fue proporcionado)
                        </c:otherwise>
                    </c:choose>
                </c:when>
                <c:otherwise>
                    Dato no proporcionado
                </c:otherwise>            
            </c:choose>
            </dd>         
            <dt class="cell callout primary">Valor m&iacute;nimo</dt>
            <dd class="cell callout valorMonetario">
            <c:choose>
                <c:when test="${contratacion.tender.minValue.amount != null}">
                    &#36;${formateador.format(contratacion.tender.minValue.amount)} 
                    <c:choose> 
                        <c:when test="${contratacion.tender.minValue.currency != null && contratacion.tender.minValue.currency.equals('') == false}">
                            ${contratacion.tender.minValue.currency}
                        </c:when>
                        <c:otherwise>
                            (El tipo de moneda no fue proporcionado)
                        </c:otherwise>
                    </c:choose>
                </c:when>
                <c:otherwise>
                    Dato no proporcionado
                </c:otherwise>            
            </c:choose>
            </dd>   
            <dt class="cell callout primary">M&eacute;todo de contrataci&oacute;n</dt>
            <dd class="cell callout">
            <c:choose>
                <c:when test="${contratacion.tender.procurementMethod != null && contratacion.tender.procurementMethod.equals('') == false}">
                    ${contratacion.tender.procurementMethod}
                </c:when>
                <c:otherwise>
                    Dato no proporcionado
                </c:otherwise>            
            </c:choose>
            </dd>
            <dt class="cell callout primary">Detalles del m&eacute;todo de contrataci&oacute;n</dt>
            <dd class="cell callout">
            <c:choose>
                <c:when test="${contratacion.tender.procurementMethodDetails != null && contratacion.tender.procurementMethodDetails.equals('') == false}">
                    ${contratacion.tender.procurementMethodDetails}
                </c:when>
                <c:otherwise>
                    Dato no proporcionado
                </c:otherwise>            
            </c:choose>
            </dd>
            <dt class="cell callout primary">Justificaci&oacute;n para el m&eacute;todo de contrataci&oacute;n</dt>
            <dd class="cell callout">
            <c:choose>
                <c:when test="${contratacion.tender.procurementMethodRationale != null && contratacion.tender.procurementMethodRationale.equals('') == false}">
                    ${contratacion.tender.procurementMethodRationale}
                </c:when>
                <c:otherwise>
                    Dato no proporcionado
                </c:otherwise>            
            </c:choose>
            </dd>
            <dt class="cell callout primary">Categor&iacute;a principal de contrataci&oacute;n</dt>
            <dd class="cell callout">
            <c:choose>
                <c:when test="${contratacion.tender.mainProcurementCategory != null && contratacion.tender.mainProcurementCategory.equals('') == false}">
                    ${contratacion.tender.mainProcurementCategory}
                </c:when>
                <c:otherwise>
                    Dato no proporcionado
                </c:otherwise>            
            </c:choose>
            </dd>
            <dt class="cell callout primary">Categor&iacute;as adicionales de contrataci&oacute;n</dt>
            <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.tender.additionalProcurementCategories == null || contratacion.tender.additionalProcurementCategories.size() == 0}">
                        Dato no proporcionado
                    </c:when>
                    <c:otherwise>
                        <ul>
                            <c:forEach items="${contratacion.tender.additionalProcurementCategories}" var="categorie">
                                <li>${categorie}</li>
                            </c:forEach>
                        </ul>
                    </c:otherwise>
                </c:choose>
            </dd>
            <dt class="cell callout primary">Criterios de adjudicaci&oacute;n</dt>
            <dd class="cell callout">
            <c:choose>
                <c:when test="${contratacion.tender.awardCriteria != null && contratacion.tender.awardCriteria.equals('') == false}">
                    ${contratacion.tender.awardCriteria}
                </c:when>
                <c:otherwise>
                    Dato no proporcionado
                </c:otherwise>            
            </c:choose>
            </dd>
            <dt class="cell callout primary">Detalles de los criterios de adjudicaci&oacute;n</dt>
            <dd class="cell callout">
            <c:choose>
                <c:when test="${contratacion.tender.awardCriteriaDetails != null && contratacion.tender.awardCriteriaDetails.equals('') == false}">
                    ${contratacion.tender.awardCriteriaDetails}
                </c:when>
                <c:otherwise>
                    Dato no proporcionado
                </c:otherwise>            
            </c:choose>
            </dd>
            <dt class="cell callout primary">M&eacute;todo de presentaci&oacute;n</dt>
            <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.tender.submissionMethod == null || contratacion.tender.submissionMethod.size() == 0}">
                        Dato no proporcionado
                    </c:when>
                    <c:otherwise>
                        <ul>
                            <c:forEach items="${contratacion.tender.submissionMethod}" var="method">
                                <li>${method}</li>
                            </c:forEach>
                        </ul>
                    </c:otherwise>
                </c:choose>
            </dd>
            <dt class="cell callout primary">Detalles del m&eacute;todo de presentaci&oacute;n</dt>
            <dd class="cell callout">
            <c:choose>
                <c:when test="${contratacion.tender.submissionMethodDetails != null && contratacion.tender.submissionMethodDetails.equals('') == false}">
                    ${contratacion.tender.submissionMethodDetails}
                </c:when>
                <c:otherwise>
                    Dato no proporcionado
                </c:otherwise>            
            </c:choose>
            </dd>
        </dl>
        <fieldset class="fieldset">
            <legend>Periodo de licitaci&oacute;n</legend>
            <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                <dt class="cell callout primary">Fecha de inicio</dt>
                <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.tender.tenderPeriod.startDate != null && contratacion.tender.tenderPeriod.startDate.equals('') == false}">
                        ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(contratacion.tender.tenderPeriod.startDate).toLowerCase()}
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>
                <dt class="cell callout primary">Fecha de fin</dt>
                <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.tender.tenderPeriod.endDate != null && contratacion.tender.tenderPeriod.endDate.equals('') == false}">
                        ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(contratacion.tender.tenderPeriod.endDate).toLowerCase()}
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>
                <dt class="cell callout primary">Extensi&oacute;n m&aacute;xima</dt>
                <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.tender.tenderPeriod.maxExtentDate != null && contratacion.tender.tenderPeriod.maxExtentDate.equals('') == false}">
                        ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(contratacion.tender.tenderPeriod.maxExtentDate).toLowerCase()}
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>
                <dt class="cell callout primary">Duraci&oacute;n (d&iacute;as)</dt>
                <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.tender.tenderPeriod.durationInDays != null}">
                        ${contratacion.tender.tenderPeriod.durationInDays}
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>
            </dl>
        </fieldset>
        <fieldset class="fieldset">
            <legend>Periodo de consulta</legend>
            <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                <dt class="cell callout primary">Fecha de inicio</dt>
                <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.tender.enquiryPeriod.startDate != null && contratacion.tender.enquiryPeriod.startDate.equals('') == false}">
                        ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(contratacion.tender.enquiryPeriod.startDate).toLowerCase()}
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>
                <dt class="cell callout primary">Fecha de fin</dt>
                <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.tender.enquiryPeriod.endDate != null && contratacion.tender.enquiryPeriod.endDate.equals('') == false}">
                        ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(contratacion.tender.enquiryPeriod.endDate).toLowerCase()}
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>
                <dt class="cell callout primary">Extensi&oacute;n m&aacute;xima</dt>
                <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.tender.enquiryPeriod.maxExtentDate != null && contratacion.tender.enquiryPeriod.maxExtentDate.equals('') == false}">
                        ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(contratacion.tender.enquiryPeriod.maxExtentDate).toLowerCase()}
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>
                <dt class="cell callout primary">Duraci&oacute;n (d&iacute;as)</dt>
                <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.tender.enquiryPeriod.durationInDays != null}">
                        ${contratacion.tender.enquiryPeriod.durationInDays}
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>
            </dl>
        </fieldset>
        <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
            <dt class="cell callout primary">�Tiene consultas?</dt>
            <dd class="cell callout">
            <c:choose>
                <c:when test="${contratacion.tender.hasEnquiries != null}">
                    ${(contratacion.tender.hasEnquiries == true) ? "Si" : "No"}
                </c:when>
                <c:otherwise>
                    Dato no proporcionado
                </c:otherwise>            
            </c:choose>
            </dd>
            <dt class="cell callout primary">Criterios de elegibilidad</dt>
            <dd class="cell callout">
            <c:choose>
                <c:when test="${contratacion.tender.eligibilityCriteria != null && contratacion.tender.eligibilityCriteria.equals('') == false}">
                    ${contratacion.tender.eligibilityCriteria}
                </c:when>
                <c:otherwise>
                    Dato no proporcionado
                </c:otherwise>            
            </c:choose>
            </dd>
        </dl>
        <fieldset class="fieldset">
            <legend>Periodo de evaluaci&oacute;n y adjudicaci&oacute;n</legend>
            <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                <dt class="cell callout primary">Fecha de inicio</dt>
                <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.tender.awardPeriod.startDate != null && contratacion.tender.awardPeriod.startDate.equals('') == false}">
                        ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(contratacion.tender.awardPeriod.startDate).toLowerCase()}
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>
                <dt class="cell callout primary">Fecha de fin</dt>
                <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.tender.awardPeriod.endDate != null && contratacion.tender.awardPeriod.endDate.equals('') == false}">
                        ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(contratacion.tender.awardPeriod.endDate).toLowerCase()}
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>
                <dt class="cell callout primary">Extensi&oacute;n m&aacute;xima</dt>
                <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.tender.awardPeriod.maxExtentDate != null && contratacion.tender.awardPeriod.maxExtentDate.equals('') == false}">
                        ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(contratacion.tender.awardPeriod.maxExtentDate).toLowerCase()}
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>
                <dt class="cell callout primary">Duraci&oacute;n (d&iacute;as)</dt>
                <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.tender.awardPeriod.durationInDays != null}">
                        ${contratacion.tender.awardPeriod.durationInDays}
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>
            </dl>
        </fieldset>
        <fieldset class="fieldset">
            <legend>Periodo de contrato</legend>
            <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                <dt class="cell callout primary">Fecha de inicio</dt>
                <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.tender.contractPeriod.startDate != null && contratacion.tender.contractPeriod.startDate.equals('') == false}">
                        ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(contratacion.tender.contractPeriod.startDate).toLowerCase()}
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>
                <dt class="cell callout primary">Fecha de fin</dt>
                <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.tender.contractPeriod.endDate != null && contratacion.tender.contractPeriod.endDate.equals('') == false}">
                        ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(contratacion.tender.contractPeriod.endDate).toLowerCase()}
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>
                <dt class="cell callout primary">Extensi&oacute;n m&aacute;xima</dt>
                <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.tender.contractPeriod.maxExtentDate != null && contratacion.tender.contractPeriod.maxExtentDate.equals('') == false}">
                        ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(contratacion.tender.contractPeriod.maxExtentDate).toLowerCase()}
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>
                <dt class="cell callout primary">Duraci&oacute;n (d&iacute;as)</dt>
                <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.tender.contractPeriod.durationInDays != null}">
                        ${contratacion.tender.contractPeriod.durationInDays}
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>
            </dl>
        </fieldset>
        <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
            <dt class="cell callout primary">N&uacute;mero de licitantes</dt>
            <dd class="cell callout">
            <c:choose>
                <c:when test="${contratacion.tender.numberOfTenderers != null}">
                    ${contratacion.tender.numberOfTenderers}
                </c:when>
                <c:otherwise>
                    Dato no proporcionado
                </c:otherwise>            
            </c:choose>
            </dd>
        </dl>

        <%@include file="SeccionTender/frmSeccionTenderTenderers.jsp"%>

        <fieldset class="fieldset">
            <legend>Documentos</legend>
            <c:choose>
                <c:when test="${contratacion.tender.documents == null || contratacion.tender.documents.size() == 0}">
                    Dato no proporcionado
                </c:when>
                <c:otherwise>
                    <ul class="accordion" data-accordion data-allow-all-closed="true">
                        <c:set var="contador" value="1"></c:set>
                        <c:forEach items="${contratacion.tender.documents}" var="document">
                            <li class="accordion-item ${ contador == 1 ? "is-active" : "" }" data-accordion-item>
                                <a href="#" class="accordion-title">Documento #${contador}</a>
                                <div class="accordion-content" data-tab-content>
                                    <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                        <dt class="cell callout primary">ID</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${document.id != null}">
                                                ${document.id}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Tipo de Documento</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${document.documentType != null && document.documentType.equals('') == false}">
                                                ${document.documentType}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">T&iacute;tulo</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${document.title != null && document.title.equals('') == false}">
                                                ${document.title}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Descripci&oacute;n</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${document.description != null && document.description.equals('') == false}">
                                                ${document.description}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">URL</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${document.url != null && document.url.equals('') == false}">
                                                <a href="${(fn:contains(document.url, 'http')) ? '' : '//' }${document.url}" target="_blank">${document.url}</a>
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Fecha de publicaci&oacute;n</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${document.datePublished != null && document.datePublished.equals('') == false}">
                                                ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(document.datePublished).toLowerCase()}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Fecha de modificaci&oacute;n</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${document.dateModified != null && document.dateModified.equals('') == false}">
                                                ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(document.dateModified).toLowerCase()}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Formato</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${document.format != null && document.format.equals('') == false}">
                                                ${document.format}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Idioma</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${document.language != null && document.language.equals('') == false}">
                                                ${document.language}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                    </dl>
                                </div>
                            </li>
                            <c:set var="contador" value="${contador+1}"></c:set>
                        </c:forEach>
                    </ul>
                </c:otherwise>
            </c:choose>
        </fieldset>
        <fieldset class="fieldset">
            <legend>Hitos</legend>
            <c:choose>
                <c:when test="${contratacion.tender.milestones == null || contratacion.tender.milestones.size() == 0}">
                    Dato no proporcionado
                </c:when>
                <c:otherwise>
                    <ul class="accordion" data-accordion data-allow-all-closed="true">
                        <c:set var="contador" value="1"></c:set>
                        <c:forEach items="${contratacion.tender.milestones}" var="milestone">
                            <li class="accordion-item ${ contador == 1 ? "is-active" : "" }" data-accordion-item>
                                <a href="#" class="accordion-title">Hito #${contador}</a>
                                <div class="accordion-content" data-tab-content>
                                    <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                        <dt class="cell callout primary">ID</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${milestone.id != null}">
                                                ${milestone.id}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">T&iacute;tulo</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${milestone.title != null && milestone.title.equals('') == false}">
                                                ${milestone.title}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Tipo de hito</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${milestone.type != null && milestone.type.equals('') == false}">
                                                ${milestone.type}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Descripci&oacute;n</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${milestone.description != null && milestone.description.equals('') == false}">
                                                ${milestone.description}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">C&oacute;digo de hito</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${milestone.code != null && milestone.code.equals('') == false}">
                                                ${milestone.code}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Fecha l&iacute;mite</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${milestone.dueDate != null && milestone.dueDate.equals('') == false}">
                                                ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(milestone.dueDate).toLowerCase()}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Fecha de cumplimiento</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${milestone.dateMet != null && milestone.dateMet.equals('') == false}">
                                                ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(milestone.dateMet).toLowerCase()}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Fecha de modificaci&oacute;n</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${milestone.dateModified != null && milestone.dateModified.equals('') == false}">
                                                ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(milestone.dateModified).toLowerCase()}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Estado</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${milestone.status != null && milestone.status.equals('') == false}">
                                                ${milestone.status}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                    </dl>
                                </div>
                            </li>
                            <c:set var="contador" value="${contador+1}"></c:set>
                        </c:forEach>
                    </ul>
                </c:otherwise>
            </c:choose>
        </fieldset>
        <fieldset class="fieldset">
            <legend>Enmiendas</legend>
            <c:choose>
                <c:when test="${contratacion.tender.amendments == null || contratacion.tender.amendments.size() == 0}">
                    Dato no proporcionado
                </c:when>
                <c:otherwise>
                    <ul class="accordion" data-accordion data-allow-all-closed="true">
                        <c:set var="contador" value="1"></c:set>
                        <c:forEach items="${contratacion.tender.amendments}" var="amendment">
                            <li class="accordion-item ${ contador == 1 ? "is-active" : "" }" data-accordion-item>
                                <a href="#" class="accordion-title">Enmienda #${contador}</a>
                                <div class="accordion-content" data-tab-content>
                                    <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                        <dt class="cell callout primary">Fecha de enmienda</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${amendment.date != null && amendment.date.equals('') == false}">
                                                ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(amendment.date).toLowerCase()}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Justificaci&oacute;n</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${amendment.rationale != null && amendment.rationale.equals('') == false}">
                                                ${amendment.rationale}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">ID</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${amendment.id != null && amendment.id.equals('') == false}">
                                                ${amendment.id}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Descripci&oacute;n</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${amendment.description != null && amendment.description.equals('') == false}">
                                                ${amendment.description}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Entrega enmendada (identificador)</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${amendment.amendsReleaseID != null && amendment.amendsReleaseID.equals('') == false}">
                                                ${amendment.amendsReleaseID}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Entrega de enmienda (identificador)</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${amendment.releaseID != null && amendment.releaseID.equals('') == false}">
                                                ${amendment.releaseID}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                    </dl>
                                </div>
                            </li>
                            <c:set var="contador" value="${contador+1}"></c:set>
                        </c:forEach>
                    </ul>
                </c:otherwise>
            </c:choose>
        </fieldset>

    </div>
</div>
