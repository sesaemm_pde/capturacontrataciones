<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="tabs-panel" id="panela3v">
    <h3>Partes involucradas</h3>
    <div class="callout">
        <c:set var="contadorPrincipal" value="1"></c:set>
        <c:choose>
            <c:when test="${contratacion.parties == null || contratacion.parties.size() == 0}">
                <dd>Dato no proporcionado</dd>
            </c:when>
            <c:otherwise>
                <ul class="accordion" data-accordion data-allow-all-closed="true">
                    <c:forEach items="${contratacion.parties}" var="partie">
                        <li class="accordion-item ${ contadorPrincipal == 1 ? "is-active" : "" }" data-accordion-item>
                            <a href="#" class="accordion-title">Parte #${contadorPrincipal}</a>
                            <div class="accordion-content" data-tab-content>
                                <dl class="grid-x grid-margin-x medium-up-1 large-up-2">  
                                    <dt class="cell callout primary">Nombre com&uacute;n</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${partie.name != null && partie.name.equals('') == false}">
                                            ${partie.name}
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                    <dt class="cell callout primary">ID</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${partie.id != null && partie.id.equals('') == false}">
                                            ${partie.id}
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>            
                                    </c:choose>
                                    </dd>
                                </dl>
                                <fieldset class="fieldset">
                                    <legend>Identificador principal</legend>
                                    <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                        <dt class="cell callout primary">Esquema</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${partie.identifier.scheme != null && partie.identifier.scheme.equals('') == false}">
                                                ${partie.identifier.scheme}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">ID</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${partie.identifier.id != null}">
                                                ${partie.identifier.id}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Nombre Legal</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${partie.identifier.legalName != null && partie.identifier.legalName.equals('') == false}">
                                                ${partie.identifier.legalName}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">URI</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${partie.identifier.uri != null && partie.identifier.uri.equals('') == false}">
                                                <a href="${(fn:contains(partie.identifier.uri, 'http')) ? '' : '//' }${partie.identifier.uri}" target="_blank">${partie.identifier.uri}</a>
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                    </dl>
                                </fieldset>
                                <fieldset class="fieldset">
                                    <legend>Identificadores adicionales</legend>
                                    <c:choose>
                                        <c:when test="${partie.additionalIdentifiers == null || partie.additionalIdentifiers.size() == 0}">
                                            Dato no proporcionado
                                        </c:when>
                                        <c:otherwise>
                                            <ul class="accordion" data-accordion data-allow-all-closed="true">
                                                <c:set var="contador" value="1"></c:set>
                                                <c:forEach items="${partie.additionalIdentifiers}" var="identifier">
                                                    <li class="accordion-item ${ contador == 1 ? "is-active" : "" }" data-accordion-item>
                                                        <a href="#" class="accordion-title">Identificador #${contador}</a>
                                                        <div class="accordion-content" data-tab-content>
                                                            <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                                                <dt class="cell callout primary">Esquema</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${identifier.scheme != null && identifier.scheme.equals('') == false}">
                                                                        ${identifier.scheme}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">ID</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${identifier.id != null}">
                                                                        ${identifier.id}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">Nombre Legal</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${identifier.legalName != null && identifier.legalName.equals('') == false}">
                                                                        ${identifier.legalName}
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                                <dt class="cell callout primary">URI</dt>
                                                                <dd class="cell callout">
                                                                <c:choose>
                                                                    <c:when test="${identifier.uri != null && identifier.uri.equals('') == false}">
                                                                        <a href="${(fn:contains(identifier.uri, 'http')) ? '' : '//' }${identifier.uri}" target="_blank">${identifier.uri}</a>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        Dato no proporcionado
                                                                    </c:otherwise>            
                                                                </c:choose>
                                                                </dd>
                                                            </dl>
                                                        </div>
                                                    </li>
                                                    <c:set var="contador" value="${contador+1}"></c:set>
                                                </c:forEach>
                                            </ul>
                                        </c:otherwise>
                                    </c:choose>
                                </fieldset>
                                <fieldset class="fieldset">
                                    <legend>Direcci&oacute;n</legend>
                                    <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                        <dt class="cell callout primary">Direcci&oacute;n</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${partie.address.streetAddress != null && partie.address.streetAddress.equals('') == false}">
                                                ${partie.address.streetAddress}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Localidad</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${partie.address.locality != null && partie.address.locality.equals('') == false}">
                                                ${partie.address.locality}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Regi&oacute;n</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${partie.address.region != null && partie.address.region.equals('') == false}">
                                                ${partie.address.region}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">C&oacute;digo postal</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${partie.address.postalCode != null && partie.address.postalCode.equals('') == false}">
                                                ${partie.address.postalCode}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Pa&iacute;s</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${partie.address.countryName != null && partie.address.countryName.equals('') == false}">
                                                ${partie.address.countryName}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                    </dl>
                                </fieldset>
                                <fieldset class="fieldset">
                                    <legend>Punto de contacto</legend>
                                    <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                        <dt class="cell callout primary">Nombre</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${partie.contactPoint.name != null && partie.contactPoint.name.equals('') == false}">
                                                ${partie.contactPoint.name}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Correo electr&oacute;nico</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${partie.contactPoint.email != null && partie.contactPoint.email.equals('') == false}">
                                                ${partie.contactPoint.email}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Tel&eacute;fono</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${partie.contactPoint.telephone != null && partie.contactPoint.telephone.equals('') == false}">
                                                ${partie.contactPoint.telephone}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">N&uacute;mero de fax</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${partie.contactPoint.faxNumber != null && partie.contactPoint.faxNumber.equals('') == false}">
                                                ${partie.contactPoint.faxNumber}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">URL</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${partie.contactPoint.url != null && partie.contactPoint.url.equals('') == false}">
                                                <a href="${(fn:contains(partie.contactPoint.url, 'http')) ? '' : '//' }${partie.contactPoint.url}" target="_blank">${partie.contactPoint.url}</a>
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                    </dl>
                                </fieldset>
                                <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                    <dt class="cell callout primary">Roles de las partes</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${partie.roles == null || partie.roles.size() == 0}">
                                            Dato no proporcionado
                                        </c:when>
                                        <c:otherwise>
                                            <ul>
                                                <c:forEach items="${partie.roles}" var="rol">
                                                    <li>${rol}</li>
                                                </c:forEach>
                                            </ul>
                                        </c:otherwise>
                                    </c:choose>
                                    </dd>
                                    <dt class="cell callout primary">Detalles</dt>
                                    <dd class="cell callout">
                                    <c:choose>
                                        <c:when test="${partie.details.scale != null && partie.details.scale.equals('') == false}">
                                            ${partie.details.scale}
                                        </c:when>
                                        <c:otherwise>
                                            Dato no proporcionado
                                        </c:otherwise>
                                    </c:choose>
                                    </dd>
                                </dl>
                            </div>
                        </li>
                        <c:set var="contadorPrincipal" value="${contadorPrincipal+1}"></c:set>
                    </c:forEach>
                </ul>
            </c:otherwise>
        </c:choose>
    </div>
</div>
