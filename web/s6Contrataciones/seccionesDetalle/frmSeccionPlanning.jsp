<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="tabs-panel" id="panela5v">
    <h3>Planeaci&oacute;n</h3>
    <div class="callout">
        <dl class="grid-x grid-margin-x medium-up-1 large-up-2"> 
            <dt class="cell callout primary">Justificaci&oacute;n</dt>
            <dd class="cell callout">
            <c:choose>
                <c:when test="${contratacion.planning.rationale != null && contratacion.planning.rationale.equals('') == false}">
                    ${contratacion.planning.rationale}
                </c:when>
                <c:otherwise>
                    Dato no proporcionado
                </c:otherwise>            
            </c:choose>
            </dd>
        </dl>
        <fieldset class="fieldset">
            <legend>Presupuesto</legend>
            <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                <dt class="cell callout primary">ID</dt>
                <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.planning.budget.id != null}">
                        ${contratacion.planning.budget.id}
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>
                <dt class="cell callout primary">Fuente del Presupuesto</dt>
                <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.planning.budget.description != null && contratacion.planning.budget.description.equals('') == false}">
                        ${contratacion.planning.budget.description}
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>
                <dt class="cell callout primary">Monto</dt>
                <dd class="cell callout valorMonetario">
                <c:choose>
                    <c:when test="${contratacion.planning.budget.amount.amount != null}">
                        &#36;${formateador.format(contratacion.planning.budget.amount.amount)} 
                        <c:choose> 
                            <c:when test="${contratacion.planning.budget.amount.currency != null && contratacion.planning.budget.amount.currency.equals('') == false}">
                                ${contratacion.planning.budget.amount.currency}
                            </c:when>
                            <c:otherwise>
                                (El tipo de moneda no fue proporcionado)
                            </c:otherwise>
                        </c:choose>
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>  
                <dt class="cell callout primary">T&iacute;tulo del Proyecto</dt>
                <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.planning.budget.project != null && contratacion.planning.budget.project.equals('') == false}">
                        ${contratacion.planning.budget.project}
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>
                <dt class="cell callout primary">Identificador de proyecto</dt>
                <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.planning.budget.projectID != null}">
                        ${contratacion.planning.budget.projectID}
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>
                <dt class="cell callout primary">Informaci&oacute;n vinculada de presupuesto</dt>
                <dd class="cell callout">
                <c:choose>
                    <c:when test="${contratacion.planning.budget.uri != null && contratacion.planning.budget.uri.equals('') == false}">
                        <a href="${(fn:contains(contratacion.planning.budget.uri, 'http')) ? '' : '//' }${contratacion.planning.budget.uri}" target="_blank">${contratacion.planning.budget.uri}</a>
                    </c:when>
                    <c:otherwise>
                        Dato no proporcionado
                    </c:otherwise>            
                </c:choose>
                </dd>
            </dl> 
        </fieldset>
        <fieldset class="fieldset">
            <legend>Documentos</legend>
            <c:choose>
                <c:when test="${contratacion.planning.documents == null || contratacion.planning.documents.size() == 0}">
                    Dato no proporcionado
                </c:when>
                <c:otherwise>
                    <ul class="accordion" data-accordion data-allow-all-closed="true">
                        <c:set var="contador" value="1"></c:set>
                        <c:forEach items="${contratacion.planning.documents}" var="document">
                            <li class="accordion-item ${ contador == 1 ? "is-active" : "" }" data-accordion-item>
                                <a href="#" class="accordion-title">Documento #${contador}</a>
                                <div class="accordion-content" data-tab-content>
                                    <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                        <dt class="cell callout primary">ID</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${document.id  != null}">
                                                ${document.id}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Tipo de Documento</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${document.documentType  != null && document.documentType.equals('') == false}">
                                                ${document.documentType}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">T&iacute;tulo</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${document.title  != null && document.title.equals('') == false}">
                                                ${document.title}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Descripci&oacute;n</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${document.description  != null && document.description.equals('') == false}">
                                                ${document.description}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">URL</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${document.url  != null && document.url.equals('') == false}">
                                                <a href="${(fn:contains(document.url, 'http')) ? '' : '//' }${document.url}" target="_blank">${document.url}</a>
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Fecha de publicaci&oacute;n</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${document.datePublished  != null && document.datePublished.equals('') == false}">
                                                ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(document.datePublished).toLowerCase()}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Fecha de modificaci&oacute;n</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${document.dateModified  != null && document.dateModified.equals('') == false}">
                                                ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(document.dateModified).toLowerCase()}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Formato</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${document.format  != null && document.format.equals('') == false}">
                                                ${document.format}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Idioma</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${document.language  != null && document.language.equals('') == false}">
                                                ${document.language}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                    </dl>
                                </div>
                            </li>
                            <c:set var="contador" value="${contador+1}"></c:set>
                        </c:forEach>
                    </ul>
                </c:otherwise>
            </c:choose>
        </fieldset>
        <fieldset class="fieldset">
            <legend>Hitos de planeaci&oacute;n</legend>
            <c:choose>
                <c:when test="${contratacion.planning.milestones == null || contratacion.planning.milestones.size() == 0}">
                    Dato no proporcionado
                </c:when>
                <c:otherwise>
                    <ul class="accordion" data-accordion data-allow-all-closed="true">
                        <c:set var="contador" value="1"></c:set>
                        <c:forEach items="${contratacion.planning.milestones}" var="milestone">
                            <li class="accordion-item ${ contador == 1 ? "is-active" : "" }" data-accordion-item>
                                <a href="#" class="accordion-title">Hito #${contador}</a>
                                <div class="accordion-content" data-tab-content>
                                    <dl class="grid-x grid-margin-x medium-up-1 large-up-2">
                                        <dt class="cell callout primary">ID</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${milestone.id  != null}">
                                                ${milestone.id}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">T&iacute;tulo</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${milestone.title  != null && milestone.title.equals('') == false}">
                                                ${milestone.title}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Tipo de hito</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${milestone.type  != null && milestone.type.equals('') == false}">
                                                ${milestone.type}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Descripci&oacute;n</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${milestone.description  != null && milestone.description.equals('') == false}">
                                                ${milestone.description}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">C&oacute;digo de hito</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${milestone.code  != null && milestone.code.equals('') == false}">
                                                ${milestone.code}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Fecha l&iacute;mite</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${milestone.dueDate  != null && milestone.dueDate.equals('') == false}">
                                                ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(milestone.dueDate).toLowerCase()}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Fecha de cumplimiento</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${milestone.dateMet  != null && milestone.dateMet.equals('') == false}">
                                                ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(milestone.dateMet).toLowerCase()}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Fecha de modificaci&oacute;n</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${milestone.dateModified  != null && milestone.dateModified.equals('') == false}">
                                                ${formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(milestone.dateModified).toLowerCase()}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                        <dt class="cell callout primary">Estado</dt>
                                        <dd class="cell callout">
                                        <c:choose>
                                            <c:when test="${milestone.status  != null && milestone.status.equals('') == false}">
                                                ${milestone.status}
                                            </c:when>
                                            <c:otherwise>
                                                Dato no proporcionado
                                            </c:otherwise>            
                                        </c:choose>
                                        </dd>
                                    </dl>
                                </div>
                            </li>
                            <c:set var="contador" value="${contador+1}"></c:set>
                        </c:forEach>
                    </ul>
                </c:otherwise>
            </c:choose>
        </fieldset>
    </div>
</div>
