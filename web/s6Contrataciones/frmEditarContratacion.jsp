<%--
    Autor: Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@include file="/encabezado.jsp" %>

<div class="orbit" role="region" aria-label="Hero PDE" data-orbit="data-orbit">
    <ul class="orbit-container">
        <li class="orbit-slide">
            <img class="orbit-image" src="framework/img/png/banner-pde-sis-min.png" alt="PDE"/>
        </li>
    </ul>
</div>

<%@include file="/migas.jsp" %>

<style>
    .btnFrmContrataciones{
        background-color: #fff;
        color: #e8222d;
        border: #e8222d solid 1px;
        border-radius: 10px;
    }
    .btnFrmContrataciones:hover{
        background-color: #e8222d;
        color: #fff;
    }
    .swal-text {
        text-align: justify;
        padding: 0px 1.5rem;
    }
</style>
<script>
    
    function obtenerDatosInicialesElementos(strNombreSeccion, arregloDimensiones)
    { 

        let estructuraPorDefecto;
        let identificadorDatosElemento = "";
        let inputSiguienteIdentificadorAUtilizar;
        let inputUltimoIdentificador;
        let contenedorElementos;
        let inputListaIdentificadoresPorRegistrar;
        let strIdentificadoresPorRegistrar;
        let aregloIdentificadoresPorRegistrar = [];
        let totalDeElementos;
        
        if(arregloDimensiones && arregloDimensiones.length >= 1)
        { 
            arregloDimensiones.map(nivel =>{
                identificadorDatosElemento += "_" + nivel;
            });
        } 
        
        contenedorElementos = document.getElementById("contenedor-" + strNombreSeccion + identificadorDatosElemento);
        inputUltimoIdentificador = document.getElementById("ultimo-identificador-elementos-" + strNombreSeccion + identificadorDatosElemento);
        inputSiguienteIdentificadorAUtilizar = document.getElementById("siguiente-identificador-" + strNombreSeccion + identificadorDatosElemento);
        inputListaIdentificadoresPorRegistrar = document.getElementById("lista-identificadores-por-registrar-" + strNombreSeccion + identificadorDatosElemento);

        estructuraPorDefecto = contenedorElementos.innerHTML;
        totalDeElementos = (parseInt(inputUltimoIdentificador.value) - 1);

        inputSiguienteIdentificadorAUtilizar.value = inputUltimoIdentificador.value;
        inputSiguienteIdentificadorAUtilizar.dataset.valorPorDefecto = inputUltimoIdentificador.value;
       
        if (totalDeElementos >= 0)
        { 

            for(let i = 0; i <= totalDeElementos; i++)
            { 
                aregloIdentificadoresPorRegistrar.push(i);
            } 

            strIdentificadoresPorRegistrar = aregloIdentificadoresPorRegistrar.join(",");

        } 
        else
        {
            strIdentificadoresPorRegistrar = "";
        }

        
        inputListaIdentificadoresPorRegistrar.value = strIdentificadoresPorRegistrar;
        inputListaIdentificadoresPorRegistrar.dataset.valorPorDefecto = strIdentificadoresPorRegistrar;

        return estructuraPorDefecto;

    } 
    
    function obtenerDatosFormularioS6(strNombreSeccion)
    { 

        let datosFormulario = {};

        if(strNombreSeccion === "contract")
        {
            datosFormulario = {
                nombreParaElemento: "contrato",
                idPrincipal: "txtCONTRACT_ID",
                modal: "modalFrmContract",
                formulario: "frmContract",
                contenedorElementos: "contenedor-contract",
                btnAgregarElementoFormulario: "contenedor-btn-agregar-contract",
                btnCerrarElementoFormulario: "contenedor-btn-cerrar-contract",
                tipoDeEtiqueta: "ul",
                campos: [
                    { "id":"txtCONTRACT_ID", "tipo": "text" },
                    { "id":"txtCONTRACT_AWARD_ID", "tipo": "text" },
                    { "id":"txtCONTRACT_TITLE", "tipo": "text" },
                    { "id":"txtCONTRACT_DESCRIPTION", "tipo": "text" },
                    { "id":"cmbCONTRACT_STATUS", "tipo": "select" },
                    { "id":"txtCONTRACT_DATE_SIGNED", "tipo": "text" },
                    { "id":"txtCONTRACT_PERIOD_START_DATE", "tipo": "text" },
                    { "id":"txtCONTRACT_PERIOD_END_DATE", "tipo": "text" },
                    { "id":"txtCONTRACT_PERIOD_MAX_EXTENT_DATE", "tipo": "text" },
                    { "id":"txtCONTRACT_PERIOD_DURATION_IN_DATE", "tipo": "text" },
                    { "id":"txtCONTRACT_AMOUNT", "tipo": "text" },
                    { "id":"cmbCONTRACT_CURRENCY", "tipo": "select" },
                    { "id":"txtCONTRACT_AMENDMENT_ID", "tipo": "text" },
                    { "id":"txtCONTRACT_AMENDMENT_DATE", "tipo": "text" },
                    { "id":"txtCONTRACT_AMENDMENT_RATIONALE", "tipo": "text" },
                    { "id":"txtCONTRACT_AMENDMENT_DESCRIPTION", "tipo": "text" },
                    { "id":"txtCONTRACT_AMENDMENT_AMENDS_RELEASE_ID", "tipo": "text" },
                    { "id":"txtCONTRACT_AMENDMENT_RELEASE_ID", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_ID", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_SCHEME", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_LEGAL_NAME", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_URI", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_ID", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_SCHEME", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_LEGAL_NAME", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_URI", "tipo": "text" }
                ],
                elementosHijo:[
                    {
                        "elemento":"item",
                        "tipo":"ul",
                        "titulo":"Art&iacute;culos que se adquirir&aacute;n",
                        "leyenda":"Los bienes, servicios y cualquier resultado intangible de este contrato. Nota: No repetir si los art&iacute;culos son los mismos que la adjudicaci&oacute;n.",
                        "elementosHijo":[]
                    },
                    {
                        "elemento":"document",
                        "tipo":"div",
                        "titulo":"Documentos",
                        "leyenda":"Todos los documentos y archivos adjuntos relacionados con el contrato, incluyendo cualquier aviso o notificaci&oacute;n.",
                        "elementosHijo":[]
                    },
                    {
                        "elemento":"relatedProcess",
                        "tipo":"div",
                        "titulo":"Procesos relacionados",
                        "leyenda":"Los detalles de procesos relacionados, por ejemplo, si el proceso es seguido por uno o m&aacute;s procesos de contrataciones, representados bajo diferentes identificadores de contrataciones abiertas (ocid). Esto se utiliza normalmente para referir a subcontratos o renovaciones y reemplazos de este contrato.",
                        "elementosHijo":[]
                    },
                    {
                        "elemento":"implementation",
                        "tipo":"objeto",
                        "titulo":"Implementaci&oacute;n",
                        "leyenda":"La informaci&oacute;n relacionada con la implementaci&oacute;n del contrato de acuerdo con las obligaciones definidas en el mismo.",
                        "elementosHijo":[
                            {
                                "elemento":"implementation-transaction",
                                "tipo":"ul",
                                "titulo":"Transacciones",
                                "leyenda":"Una lista de transacciones de pago realizadas contra este contrato."
                            },
                            {
                                "elemento":"implementation-milestone",
                                "tipo":"ul",
                                "titulo":"Hitos",
                                "leyenda":"Cuando se completen los hitos, se deben de actualizar el estatus de los hitos y las fechas."
                            },
                            {
                                "elemento":"implementation-document",
                                "tipo":"div",
                                "titulo":"Documentos",
                                "leyenda":"Documentos y reportes que son parte de la fase de implementaci&oacute;n, por ejemplo, reportes de auditor&iacute;a y evaluaci&oacute;n."
                            }
                        ]
                    },
                    {
                        "elemento":"milestone",
                        "tipo":"ul",
                        "titulo":"Hitos",
                        "leyenda":"Una lista de hitos asociados con la terminaci&oacute;n de este contrato.",
                        "elementosHijo":[]
                    },
                    {
                        "elemento":"amendment",
                        "tipo":"ul",
                        "titulo":"Enmiendas",
                        "leyenda":"Una enmienda de contrato es un cambio formal a los detalles del mismo y generalmente implica la publicaci&oacute;n de una nueva entrega o aviso de una nueva licitaci&oacute;n, as&iacute; como de otros documentos de detallan el cambio. La raz&oacute;n y una descripci&oacute;n de los cambios realizados se pueden proveer aqu&iacute;.",
                        "elementosHijo":[]
                    },
                    {
                        "elemento":"amendment-principal-change",
                        "tipo":"div",
                        "titulo":"Campos enmendados de la enmienda principal",
                        "leyenda":"Una lista de cambios que describen los campos que cambiaron y sus valores previos.",
                        "elementosHijo":[]
                    }
                ]
            };
        }
        else if(strNombreSeccion === "award")
        {
            datosFormulario = {
                nombreParaElemento: "adjudicaci&oacute;n",
                idPrincipal: "txtAWARD_ID",
                modal: "modalFrmAward",
                formulario: "frmAward",
                contenedorElementos: "contenedor-award",
                btnAgregarElementoFormulario: "contenedor-btn-agregar-award",
                btnCerrarElementoFormulario: "contenedor-btn-cerrar-award",
                tipoDeEtiqueta: "ul",
                campos: [
                    { "id":"txtAWARD_ID", "tipo": "text" },
                    { "id":"txtAWARD_TITLE", "tipo": "text" },
                    { "id":"txtAWARD_DESCRIPTION", "tipo": "text" },
                    { "id":"cmbAWARD_STATUS", "tipo": "select" },
                    { "id":"txtAWARD_DATE", "tipo": "text" },
                    { "id":"txtAWARD_AMOUNT", "tipo": "text" },
                    { "id":"cmbAWARD_CURRENCY", "tipo": "select" },
                    { "id":"txtAWARD_PERIOD_START_DATE", "tipo": "text" },
                    { "id":"txtAWARD_PERIOD_END_DATE", "tipo": "text" },
                    { "id":"txtAWARD_PERIOD_MAX_EXTENT_DATE", "tipo": "text" },
                    { "id":"txtAWARD_PERIOD_DURATION_IN_DATE", "tipo": "text" },
                    { "id":"txtAWARD_AMENDMENT_ID", "tipo": "text" },
                    { "id":"txtAWARD_AMENDMENT_DATE", "tipo": "text" },
                    { "id":"txtAWARD_AMENDMENT_RATIONALE", "tipo": "text" },
                    { "id":"txtAWARD_AMENDMENT_DESCRIPTION", "tipo": "text" },
                    { "id":"txtAWARD_AMENDMENT_AMENDS_RELEASE_ID", "tipo": "text" },
                    { "id":"txtAWARD_AMENDMENT_RELEASE_ID", "tipo": "text" }
                ],
                elementosHijo:[
                    {
                        "elemento":"supplier",
                        "tipo":"ul",
                        "titulo":"Proveedores",
                        "leyenda":"Los proveedores adjudicados. Si hay diferentes proveedores adjudicados para los art&iacute;culos del procedimiento de contrataci&oacute;n, estos deben ser separados en diferentes bloques de adjudicaciones.",
                        "elementosHijo":[]
                    },
                    {
                        "elemento":"item",
                        "tipo":"ul",
                        "titulo":"Art&iacute;culos que se adquirir&aacute;n",
                        "leyenda":"Los bienes, servicios y cualquier resultado intangible de este contrato. Nota: No repetir si los art&iacute;culos son los mismos que la adjudicaci&oacute;n.",
                        "elementosHijo":[]
                    },
                    {
                        "elemento":"document",
                        "tipo":"div",
                        "titulo":"Documentos",
                        "leyenda":"Todos los documentos y archivos adjuntos relacionados con el contrato, incluyendo cualquier aviso o notificaci&oacute;n.",
                        "elementosHijo":[]
                    },
                    {
                        "elemento":"amendment",
                        "tipo":"ul",
                        "titulo":"Enmiendas",
                        "leyenda":"Una enmienda de contrato es un cambio formal a los detalles del mismo y generalmente implica la publicaci&oacute;n de una nueva entrega o aviso de una nueva licitaci&oacute;n, as&iacute; como de otros documentos de detallan el cambio. La raz&oacute;n y una descripci&oacute;n de los cambios realizados se pueden proveer aqu&iacute;.",
                        "elementosHijo":[]
                    },
                    {
                        "elemento":"amendment-principal-change",
                        "tipo":"div",
                        "titulo":"Campos enmendados de la enmienda principal",
                        "leyenda":"Una lista de cambios que describen los campos que cambiaron y sus valores previos.",
                        "elementosHijo":[]
                    }
                ]
            };
        }
        else if(strNombreSeccion === "supplier")
        {
            datosFormulario = {
                nombreParaElemento: "proveedor",
                idPrincipal: "txtSUPPLIER_ID",
                modal: "modalFrmSupplier",
                formulario: "frmSupplier",
                contenedorElementos: "contenedor-supplier",
                btnAgregarElementoFormulario: "contenedor-btn-agregar-supplier",
                btnCerrarElementoFormulario: "contenedor-btn-cerrar-supplier",
                tipoDeEtiqueta: "ul",
                campos: [
                    { "id":"txtSUPPLIER_ID", "tipo": "text" },
                    { "id":"txtSUPPLIER_NAME", "tipo": "text" },
                    { "id":"txtSUPPLIER_IDENTIFIER_ID", "tipo": "text" },
                    { "id":"txtSUPPLIER_IDENTIFIER_SCHEME", "tipo": "text" },
                    { "id":"txtSUPPLIER_IDENTIFIER_LEGAL_NAME", "tipo": "text" },
                    { "id":"txtSUPPLIER_IDENTIFIER_URI", "tipo": "text" },
                    { "id":"txtSUPPLIER_ADDRESS_STREET_ADDRESS", "tipo": "text" },
                    { "id":"txtSUPPLIER_ADDRESS_LOCALITY", "tipo": "text" },
                    { "id":"txtSUPPLIER_ADDRESS_REGION", "tipo": "text" },
                    { "id":"txtSUPPLIER_ADDRESS_POSTAL_CODE", "tipo": "text" },
                    { "id":"txtSUPPLIER_ADDRESS_COUNTRY_NAME", "tipo": "text" },
                    { "id":"txtSUPPLIER_CONTACT_NAME", "tipo": "text" },
                    { "id":"txtSUPPLIER_CONTACT_EMAIL", "tipo": "text" },
                    { "id":"txtSUPPLIER_CONTACT_TELEPHONE", "tipo": "text" },
                    { "id":"txtSUPPLIER_CONTACT_FAXNUMBER", "tipo": "text" },
                    { "id":"txtSUPPLIER_CONTACT_URL", "tipo": "text" }
                ],
                elementosHijo:[
                    {
                        "elemento":"identifier",
                        "tipo":"div",
                        "titulo":"Identificadores adicionales",
                        "leyenda":"Una lista adicional/suplementaria de identificadores para la organizaci&oacute;n, utilizando la [gu&iacute;a de identificadores de organizaciones]. Esto puede usarse para dar un identificador utilizado internamente por esta organizaci&oacute;n adem&aacute;s del identificador primario legal de la entidad.",
                        "elementosHijo":[]
                    }
                ]
            };
        }
        else if(strNombreSeccion === "tenderer")
        {
            datosFormulario = {
                nombreParaElemento: "licitante",
                idPrincipal: "txtTENDERER_ID",
                modal: "modalFrmTenderer",
                formulario: "frmTenderer",
                contenedorElementos: "contenedor-tenderer",
                btnAgregarElementoFormulario: "contenedor-btn-agregar-tenderer",
                btnCerrarElementoFormulario: "contenedor-btn-cerrar-tenderer",
                tipoDeEtiqueta: "ul",
                campos: [
                    { "id":"txtTENDERER_ID", "tipo": "text" },
                    { "id":"txtTENDERER_NAME", "tipo": "text" },
                    { "id":"txtTENDERER_IDENTIFIER_ID", "tipo": "text" },
                    { "id":"txtTENDERER_IDENTIFIER_SCHEME", "tipo": "text" },
                    { "id":"txtTENDERER_IDENTIFIER_LEGAL_NAME", "tipo": "text" },
                    { "id":"txtTENDERER_IDENTIFIER_URI", "tipo": "text" },
                    { "id":"txtTENDERER_ADDRESS_STREET_ADDRESS", "tipo": "text" },
                    { "id":"txtTENDERER_ADDRESS_LOCALITY", "tipo": "text" },
                    { "id":"txtTENDERER_ADDRESS_REGION", "tipo": "text" },
                    { "id":"txtTENDERER_ADDRESS_POSTAL_CODE", "tipo": "text" },
                    { "id":"txtTENDERER_ADDRESS_COUNTRY_NAME", "tipo": "text" },
                    { "id":"txtTENDERER_CONTACT_NAME", "tipo": "text" },
                    { "id":"txtTENDERER_CONTACT_EMAIL", "tipo": "text" },
                    { "id":"txtTENDERER_CONTACT_TELEPHONE", "tipo": "text" },
                    { "id":"txtTENDERER_CONTACT_FAXNUMBER", "tipo": "text" },
                    { "id":"txtTENDERER_CONTACT_URL", "tipo": "text" }
                ],
                elementosHijo:[
                    {
                        "elemento":"identifier",
                        "tipo":"div",
                        "titulo":"Identificadores adicionales",
                        "leyenda":"Una lista adicional/suplementaria de identificadores para la organizaci&oacute;n, utilizando la [gu&iacute;a de identificadores de organizaciones]. Esto puede usarse para dar un identificador utilizado internamente por esta organizaci&oacute;n adem&aacute;s del identificador primario legal de la entidad.",
                        "elementosHijo":[]
                    }
                ]
            };
        }
        else if(strNombreSeccion === "parties")
        {
            datosFormulario = {
                nombreParaElemento: "parte involucrada",
                idPrincipal: "txtPARTIES_ID",
                modal: "modalFrmParties",
                formulario: "frmParties",
                contenedorElementos: "contenedor-parties",
                btnAgregarElementoFormulario: "contenedor-btn-agregar-parties",
                btnCerrarElementoFormulario: "contenedor-btn-cerrar-parties",
                tipoDeEtiqueta: "ul",
                campos: [
                    { "id":"txtPARTIES_ID", "tipo": "text" },
                    { "id":"txtPARTIES_NAME", "tipo": "text" },
                    { "id":"txtPARTIES_IDENTIFIER_ID", "tipo": "text" },
                    { "id":"txtPARTIES_IDENTIFIER_SCHEME", "tipo": "text" },
                    { "id":"txtPARTIES_IDENTIFIER_LEGAL_NAME", "tipo": "text" },
                    { "id":"txtPARTIES_IDENTIFIER_URI", "tipo": "text" },
                    { "id":"txtPARTIES_ADDRESS_STREET_ADDRESS", "tipo": "text" },
                    { "id":"txtPARTIES_ADDRESS_LOCALITY", "tipo": "text" },
                    { "id":"txtPARTIES_ADDRESS_REGION", "tipo": "text" },
                    { "id":"txtPARTIES_ADDRESS_POSTAL_CODE", "tipo": "text" },
                    { "id":"txtPARTIES_ADDRESS_COUNTRY_NAME", "tipo": "text" },
                    { "id":"txtPARTIES_CONTACT_NAME", "tipo": "text" },
                    { "id":"txtPARTIES_CONTACT_EMAIL", "tipo": "text" },
                    { "id":"txtPARTIES_CONTACT_TELEPHONE", "tipo": "text" },
                    { "id":"txtPARTIES_CONTACT_FAXNUMBER", "tipo": "text" },
                    { "id":"txtPARTIES_CONTACT_URL", "tipo": "text" },
                    { "id":"chckPARTIES_ROLES", "tipo": "checkbox" },
                    { "id":"radPARTIES_DETAILS_SCALE", "tipo": "radio" }
                ],
                elementosHijo:[
                    {
                        "elemento":"identifier",
                        "tipo":"div",
                        "titulo":"Identificadores adicionales",
                        "leyenda":"Una lista adicional/suplementaria de identificadores para la organizaci&oacute;n, utilizando la [gu&iacute;a de identificadores de organizaciones]. Esto puede usarse para dar un identificador utilizado internamente por esta organizaci&oacute;n adem&aacute;s del identificador primario legal de la entidad.",
                        "elementosHijo":[]
                    }
                ]
            };
        }
        else if(strNombreSeccion === "item")
        {
            datosFormulario = {
                nombreParaElemento: "art&iacute;culo",
                idPrincipal: "txtITEM_ID",
                modal: "modalFrmItem",
                formulario: "frmItem",
                contenedorElementos: "contenedor-item",
                btnAgregarElementoFormulario: "contenedor-btn-agregar-item",
                btnCerrarElementoFormulario: "contenedor-btn-cerrar-item",
                tipoDeEtiqueta: "ul",
                campos: [
                    { "id":"txtITEM_ID", "tipo": "text" },
                    { "id":"txtITEM_DESCRIPTION", "tipo": "text" },
                    { "id":"txtITEM_CLASSIFICATION_ID", "tipo": "text" },
                    { "id":"cmbITEM_CLASSIFICATION_SCHEME", "tipo": "select" },
                    { "id":"txtITEM_CLASSIFICATION_DESCRIPTION", "tipo": "text" },
                    { "id":"txtITEM_CLASSIFICATION_URI", "tipo": "text" },
                    { "id":"txtITEM_QUANTITY", "tipo": "text" },
                    { "id":"txtITEM_UNIT_ID", "tipo": "text" },
                    { "id":"cmbITEM_UNIT_SCHEME", "tipo": "select" },
                    { "id":"txtITEM_UNIT_NAME", "tipo": "text" },
                    { "id":"txtITEM_UNIT_URI", "tipo": "text" },
                    { "id":"txtITEM_UNIT_AMOUNT", "tipo": "text" },
                    { "id":"cmbITEM_UNIT_CURRENCY", "tipo": "select" }
                ],
                elementosHijo:[
                    {
                        "elemento":"classification",
                        "tipo":"div",
                        "titulo":"Clasificaciones adicionales",
                        "leyenda":"Una lista de clasificaciones adicionales para el art&iacute;culo.",
                        "elementosHijo":[]
                    }
                ]
            };
        }
        else if(strNombreSeccion === "classification")
        {
            datosFormulario = {
                nombreParaElemento: "clasificaci&oacute;n",
                idPrincipal: "txtCLASSIFICATION_ID",
                modal: "modalFrmClassification",
                formulario: "frmClassification",
                contenedorElementos: "contenedor-classification",
                btnAgregarElementoFormulario: "contenedor-btn-agregar-classification",
                btnCerrarElementoFormulario: "contenedor-btn-cerrar-classification",
                tipoDeEtiqueta: "div",
                campos: [
                    { "id":"txtCLASSIFICATION_ID", "tipo": "text" },
                    { "id":"cmbCLASSIFICATION_SCHEME", "tipo": "select" },
                    { "id":"txtCLASSIFICATION_DESCRIPTION", "tipo": "text" },
                    { "id":"txtCLASSIFICATION_URI", "tipo": "text" }
                ],
                elementosHijo:[]
            };
        }
        else if(strNombreSeccion === "relatedProcess")
        {
            datosFormulario = {
                nombreParaElemento: "proceso relacionado",
                idPrincipal: "txtRELATED_PROCESS_ID",
                modal: "modalFrmRelatedProcess",
                formulario: "frmRelatedProcess",
                contenedorElementos: "contenedor-relatedProcess",
                btnAgregarElementoFormulario: "contenedor-btn-agregar-relatedProcess",
                btnCerrarElementoFormulario: "contenedor-btn-cerrar-relatedProcess",
                tipoDeEtiqueta: "div",
                campos: [
                    { "id":"txtRELATED_PROCESS_ID", "tipo": "text" },
                    { "id":"txtRELATED_PROCESS_TITLE", "tipo": "text" },
                    { "id":"cmbRELATED_PROCESS_SCHEME", "tipo": "select" },
                    { "id":"txtRELATED_PROCESS_IDENTIFIER", "tipo": "text" },
                    { "id":"txtRELATED_PROCESS_URI", "tipo": "text" },
                    { "id":"chckRELATED_PROCESS_RELATION_SHIP", "tipo": "checkbox" }
                ],
                elementosHijo:[]
            };
        }
        else if(strNombreSeccion === "document")
        {
            datosFormulario = {
                nombreParaElemento: "documento",
                idPrincipal: "txtDOCUMENT_ID",
                modal: "modalFrmDocument",
                formulario: "frmDocument",
                contenedorElementos: "contenedor-document",
                btnAgregarElementoFormulario: "contenedor-btn-agregar-document",
                btnCerrarElementoFormulario: "contenedor-btn-cerrar-document",
                tipoDeEtiqueta: "div",
                campos: [
                    { "id":"txtDOCUMENT_ID", "tipo": "text" },
                    { "id":"cmbDOCUMENT_TYPE", "tipo": "select" },
                    { "id":"txtDOCUMENT_TITLE", "tipo": "text" },
                    { "id":"txtDOCUMENT_DESCRIPTION", "tipo": "text" },
                    { "id":"txtDOCUMENT_URL", "tipo": "text" },
                    { "id":"txtDOCUMENT_DATE_PUBLISHED", "tipo": "text" },
                    { "id":"txtDOCUMENT_DATE_MODIFIED", "tipo": "text" },
                    { "id":"txtDOCUMENT_FORMAT", "tipo": "text" },
                    { "id":"cmbDOCUMENT_LANGUAGE", "tipo": "select" }
                ],
                elementosHijo:[]
            };
        }
        else if(strNombreSeccion === "milestone")
        {
            datosFormulario = {
                nombreParaElemento: "hito",
                idPrincipal: "txtMILESTONE_ID",
                modal: "modalFrmMilestone",
                formulario: "frmMilestone",
                contenedorElementos: "contenedor-milestone",
                btnAgregarElementoFormulario: "contenedor-btn-agregar-milestone",
                btnCerrarElementoFormulario: "contenedor-btn-cerrar-milestone",
                tipoDeEtiqueta: "ul",
                campos: [
                    { "id":"txtMILESTONE_ID", "tipo": "text" },
                    { "id":"cmbMILESTONE_TYPE", "tipo": "select" },
                    { "id":"txtMILESTONE_TITLE", "tipo": "text" },
                    { "id":"txtMILESTONE_DESCRIPTION", "tipo": "text" },
                    { "id":"txtMILESTONE_CODE", "tipo": "text" },
                    { "id":"txtMILESTONE_DUE_DATE", "tipo": "text" },
                    { "id":"txtMILESTONE_DATE_MET", "tipo": "text" },
                    { "id":"txtMILESTONE_DATE_MODIFIED", "tipo": "text" },
                    { "id":"cmbMILESTONE_STATUS", "tipo": "select" }
                ],
                elementosHijo:[
                    {
                        "elemento":"milestone-document",
                        "tipo":"div",
                        "titulo":"Documentos",
                        "leyenda":"Lista de documentos asociados con este hito",
                        "elementosHijo":[]
                    }
                ]
            };
        }
        else if(strNombreSeccion === "amendment")
        {
            datosFormulario = {
                nombreParaElemento: "enmienda",
                idPrincipal: "txtAMENDMENT_ID",
                modal: "modalFrmAmendment",
                formulario: "frmAmendment",
                contenedorElementos: "contenedor-amendment",
                btnAgregarElementoFormulario: "contenedor-btn-agregar-amendment",
                btnCerrarElementoFormulario: "contenedor-btn-cerrar-amendment",
                tipoDeEtiqueta: "ul",
                campos: [
                    { "id":"txtAMENDMENT_ID", "tipo": "text" },
                    { "id":"txtAMENDMENT_DATE", "tipo": "text" },
                    { "id":"txtAMENDMENT_RATIONALE", "tipo": "text" },
                    { "id":"txtAMENDMENT_DESCRIPTION", "tipo": "text" },
                    { "id":"txtAMENDMENT_AMENDS_RELEASE_ID", "tipo": "text" },
                    { "id":"txtAMENDMENT_RELEASE_ID", "tipo": "text" }
                ],
                elementosHijo:[
                    {
                        "elemento":"change",
                        "tipo":"div",
                        "titulo":"Campos enmendados",
                        "leyenda":"Una lista de cambios que describen los campos que cambiaron y sus valores previos.",
                        "elementosHijo":[]
                    }
                ]
            };
        }
        else if(strNombreSeccion === "change")
        {
            datosFormulario = {
                nombreParaElemento: "cambio",
                idPrincipal: "txtCHANGE_PROPERTY",
                modal: "modalFrmChange",
                formulario: "frmChange",
                contenedorElementos: "contenedor-change",
                btnAgregarElementoFormulario: "contenedor-btn-agregar-change",
                btnCerrarElementoFormulario: "contenedor-btn-cerrar-change",
                tipoDeEtiqueta: "div",
                campos: [
                    { "id":"txtCHANGE_PROPERTY", "tipo": "text" },
                    { "id":"txtCHANGE_FORMATER_VALUE", "tipo": "text" }
                ],
                elementosHijo:[]
            };
        }
        else if(strNombreSeccion === "amendment-principal-change")
        {
            datosFormulario = {
                nombreParaElemento: "cambio",
                idPrincipal: "txtAMENDMENT_PRINCIPAL_CHANGE_PROPERTY",
                modal: "modalFrmAmendmentPrincipalChange",
                formulario: "frmAmendmentPrincipalChange",
                contenedorElementos: "contenedor-amendment-principal-change",
                btnAgregarElementoFormulario: "contenedor-btn-agregar-amendment-principal-change",
                btnCerrarElementoFormulario: "contenedor-btn-cerrar-amendment-principal-change",
                tipoDeEtiqueta: "div",
                campos: [
                    { "id":"txtAMENDMENT_PRINCIPAL_CHANGE_PROPERTY", "tipo": "text" },
                    { "id":"txtAMENDMENT_PRINCIPAL_CHANGE_FORMATER_VALUE", "tipo": "text" }
                ],
                elementosHijo:[]
            };
        }
        else if(strNombreSeccion === "implementation-transaction")
        {
            datosFormulario = {
                nombreParaElemento: "transacci&oacute;n",
                idPrincipal: "txtIMPLEMENTATION_TRANSACTION_ID",
                modal: "modalFrmImplementationTransaction",
                formulario: "frmImplementationTransaction",
                contenedorElementos: "contenedor-implementation-transaction",
                btnAgregarElementoFormulario: "contenedor-btn-agregar-implementation-transaction",
                btnCerrarElementoFormulario: "contenedor-btn-cerrar-implementation-transaction",
                tipoDeEtiqueta: "ul",
                campos: [
                    { "id":"txtIMPLEMENTATION_TRANSACTION_ID", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_SOURCE", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_DATE_PUBLISHED", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_URI", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_VALUE_AMOUNT", "tipo": "text" },
                    { "id":"cmbIMPLEMENTATION_TRANSACTION_VALUE_CURRENCY", "tipo": "select" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_AMOUNT_AMOUNT", "tipo": "text" },
                    { "id":"cmbIMPLEMENTATION_TRANSACTION_AMOUNT_CURRENCY", "tipo": "select" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYER_ID", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYER_NAME", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_ID", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_SCHEME", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_LEGAL_NAME", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYER_IDENTIFIER_URI", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_STREET_ADDRESS", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_LOCALITY", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_REGION", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_POSTAL_CODE", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYER_ADDRESS_COUNTRY_NAME", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_NAME", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_EMAIL", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_TELEPHONE", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_FAXNUMBER", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYER_CONTACT_URL", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYEE_ID", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYEE_NAME", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_ID", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_SCHEME", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_LEGAL_NAME", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYEE_IDENTIFIER_URI", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_STREET_ADDRESS", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_LOCALITY", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_REGION", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_POSTAL_CODE", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYEE_ADDRESS_COUNTRY_NAME", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_NAME", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_EMAIL", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_TELEPHONE", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_FAXNUMBER", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PAYEE_CONTACT_URL", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_ID", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_SCHEME", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_LEGAL_NAME", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_PROVIDER_ORGANIZATION_URI", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_ID", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_SCHEME", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_LEGAL_NAME", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_TRANSACTION_RECEIVER_ORGANIZATION_URI", "tipo": "text" }
                ],
                elementosHijo:[
                    {
                        "elemento":"identifier-payer",
                        "tipo":"div",
                        "titulo":"Identificadores adicionales - Pagador",
                        "leyenda":"Una lista adicional/suplementaria de identificadores para la organizaci&oacute;n, utilizando la [gu&iacute;a de identificadores de organizaciones]. Esto puede usarse para dar un identificador utilizado internamente por esta organizaci&oacute;n adem&aacute;s del identificador primario legal de la entidad.",
                        "elementosHijo":[]
                    },
                    {
                        "elemento":"identifier-payee",
                        "tipo":"div",
                        "titulo":"Identificadores adicionales - Beneficiario",
                        "leyenda":"Una lista adicional/suplementaria de identificadores para la organizaci&oacute;n, utilizando la [gu&iacute;a de identificadores de organizaciones]. Esto puede usarse para dar un identificador utilizado internamente por esta organizaci&oacute;n adem&aacute;s del identificador primario legal de la entidad.",
                        "elementosHijo":[]
                    }
                ]
            };
        }
        else if(strNombreSeccion === "implementation-document")
        {
            datosFormulario = {
                nombreParaElemento: "documento",
                idPrincipal: "txtIMPLEMENTATION_DOCUMENT_ID",
                modal: "modalFrmImplementationDocument",
                formulario: "frmImplementationDocument",
                contenedorElementos: "contenedor-implementation-document",
                btnAgregarElementoFormulario: "contenedor-btn-agregar-implementation-document",
                btnCerrarElementoFormulario: "contenedor-btn-cerrar-implementation-document",
                tipoDeEtiqueta: "div",
                campos: [
                    { "id":"txtIMPLEMENTATION_DOCUMENT_ID", "tipo": "text" },
                    { "id":"cmbIMPLEMENTATION_DOCUMENT_TYPE", "tipo": "select" },
                    { "id":"txtIMPLEMENTATION_DOCUMENT_TITLE", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_DOCUMENT_DESCRIPTION", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_DOCUMENT_URL", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_DOCUMENT_DATE_PUBLISHED", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_DOCUMENT_DATE_MODIFIED", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_DOCUMENT_FORMAT", "tipo": "text" },
                    { "id":"cmbIMPLEMENTATION_DOCUMENT_LANGUAGE", "tipo": "select" }
                ],
                elementosHijo:[]
            };
        }
        else if(strNombreSeccion === "implementation-milestone")
        {
            datosFormulario = {
                nombreParaElemento: "hito",
                idPrincipal: "txtIMPLEMENTATION_MILESTONE_ID",
                modal: "modalFrmImplementationMilestone",
                formulario: "frmImplementationMilestone",
                contenedorElementos: "contenedor-implementation-milestone",
                btnAgregarElementoFormulario: "contenedor-btn-agregar-implementation-milestone",
                btnCerrarElementoFormulario: "contenedor-btn-cerrar-implementation-milestone",
                tipoDeEtiqueta: "ul",
                campos: [
                    { "id":"txtIMPLEMENTATION_MILESTONE_ID", "tipo": "text" },
                    { "id":"cmbIMPLEMENTATION_MILESTONE_TYPE", "tipo": "select" },
                    { "id":"txtIMPLEMENTATION_MILESTONE_TITLE", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_MILESTONE_DESCRIPTION", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_MILESTONE_CODE", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_MILESTONE_DUE_DATE", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_MILESTONE_DATE_MET", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_MILESTONE_DATE_MODIFIED", "tipo": "text" },
                    { "id":"cmbIMPLEMENTATION_MILESTONE_STATUS", "tipo": "select" }
                ],
                elementosHijo:[
                    {
                        "elemento":"implementation-milestone-document",
                        "tipo":"div",
                        "titulo":"Documentos",
                        "leyenda":"Lista de documentos asociados con este hito",
                        "elementosHijo":[]
                    }
                ]
            };
        }
        else if(strNombreSeccion === "identifier")
        {
            datosFormulario = {
                nombreParaElemento: "identificador",
                idPrincipal: "txtIDENTIFIER_ID",
                modal: "modalFrmIdentifier",
                formulario: "frmIdentifier",
                contenedorElementos: "contenedor-identifier",
                btnAgregarElementoFormulario: "contenedor-btn-agregar-identifier",
                btnCerrarElementoFormulario: "contenedor-btn-cerrar-identifier",
                tipoDeEtiqueta: "div",
                campos: [
                    { "id":"txtIDENTIFIER_ID", "tipo": "text" },
                    { "id":"txtIDENTIFIER_SCHEME", "tipo": "text" },
                    { "id":"txtIDENTIFIER_LEGAL_NAME", "tipo": "text" },
                    { "id":"txtIDENTIFIER_URI", "tipo": "text" }
                ],
                elementosHijo:[]
            };
        }
        else if(strNombreSeccion === "identifier-payee")
        {
            datosFormulario = {
                nombreParaElemento: "identificador beneficiario",
                idPrincipal: "txtIDENTIFIER_PAYEE_ID",
                modal: "modalFrmIdentifierPayee",
                formulario: "frmIdentifierPayee",
                contenedorElementos: "contenedor-identifier-payee",
                btnAgregarElementoFormulario: "contenedor-btn-agregar-identifier-payee",
                btnCerrarElementoFormulario: "contenedor-btn-cerrar-identifier-payee",
                tipoDeEtiqueta: "div",
                campos: [
                    { "id":"txtIDENTIFIER_PAYEE_ID", "tipo": "text" },
                    { "id":"txtIDENTIFIER_PAYEE_SCHEME", "tipo": "text" },
                    { "id":"txtIDENTIFIER_PAYEE_LEGAL_NAME", "tipo": "text" },
                    { "id":"txtIDENTIFIER_PAYEE_URI", "tipo": "text" }
                ],
                elementosHijo:[]
            };
        }
        else if(strNombreSeccion === "identifier-payer")
        {
            datosFormulario = {
                nombreParaElemento: "identificador pagador",
                idPrincipal: "txtIDENTIFIER_PAYER_ID",
                modal: "modalFrmIdentifierPayer",
                formulario: "frmIdentifierPayer",
                contenedorElementos: "contenedor-identifier-payer",
                btnAgregarElementoFormulario: "contenedor-btn-agregar-identifier-payer",
                btnCerrarElementoFormulario: "contenedor-btn-cerrar-identifier-payer",
                tipoDeEtiqueta: "div",
                campos: [
                    { "id":"txtIDENTIFIER_PAYER_ID", "tipo": "text" },
                    { "id":"txtIDENTIFIER_PAYER_SCHEME", "tipo": "text" },
                    { "id":"txtIDENTIFIER_PAYER_LEGAL_NAME", "tipo": "text" },
                    { "id":"txtIDENTIFIER_PAYER_URI", "tipo": "text" }
                ],
                elementosHijo:[]
            };
        }
        else if(strNombreSeccion === "milestone-document")
        {
            datosFormulario = {
                nombreParaElemento: "documento",
                idPrincipal: "txtMILESTONE_DOCUMENT_ID",
                modal: "modalFrmMilestoneDocument",
                formulario: "frmMilestoneDocument",
                contenedorElementos: "contenedor-milestone-document",
                btnAgregarElementoFormulario: "contenedor-btn-agregar-milestone-document",
                btnCerrarElementoFormulario: "contenedor-btn-cerrar-milestone-document",
                tipoDeEtiqueta: "div",
                campos: [
                    { "id":"txtMILESTONE_DOCUMENT_ID", "tipo": "text" },
                    { "id":"cmbMILESTONE_DOCUMENT_TYPE", "tipo": "select" },
                    { "id":"txtMILESTONE_DOCUMENT_TITLE", "tipo": "text" },
                    { "id":"txtMILESTONE_DOCUMENT_DESCRIPTION", "tipo": "text" },
                    { "id":"txtMILESTONE_DOCUMENT_URL", "tipo": "text" },
                    { "id":"txtMILESTONE_DOCUMENT_DATE_PUBLISHED", "tipo": "text" },
                    { "id":"txtMILESTONE_DOCUMENT_DATE_MODIFIED", "tipo": "text" },
                    { "id":"txtMILESTONE_DOCUMENT_FORMAT", "tipo": "text" },
                    { "id":"cmbMILESTONE_DOCUMENT_LANGUAGE", "tipo": "select" }
                ],
                elementosHijo:[]
            };
        }
        else if(strNombreSeccion === "implementation-milestone-document")
        {
            datosFormulario = {
                nombreParaElemento: "documento",
                idPrincipal: "txtIMPLEMENTATION_MILESTONE_DOCUMENT_ID",
                modal: "modalFrmImplementationMilestoneDocument",
                formulario: "frmImplementationMilestoneDocument",
                contenedorElementos: "contenedor-implementation-milestone-document",
                btnAgregarElementoFormulario: "contenedor-btn-agregar-implementation-milestone-document",
                btnCerrarElementoFormulario: "contenedor-btn-cerrar-implementation-milestone-document",
                tipoDeEtiqueta: "div",
                campos: [
                    { "id":"txtIMPLEMENTATION_MILESTONE_DOCUMENT_ID", "tipo": "text" },
                    { "id":"cmbIMPLEMENTATION_MILESTONE_DOCUMENT_TYPE", "tipo": "select" },
                    { "id":"txtIMPLEMENTATION_MILESTONE_DOCUMENT_TITLE", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_MILESTONE_DOCUMENT_DESCRIPTION", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_MILESTONE_DOCUMENT_URL", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_MILESTONE_DOCUMENT_DATE_PUBLISHED", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_MILESTONE_DOCUMENT_DATE_MODIFIED", "tipo": "text" },
                    { "id":"txtIMPLEMENTATION_MILESTONE_DOCUMENT_FORMAT", "tipo": "text" },
                    { "id":"cmbIMPLEMENTATION_MILESTONE_DOCUMENT_LANGUAGE", "tipo": "select" }
                ],
                elementosHijo:[]
            };
        }

        return datosFormulario;

    } 

</script>
<div class="grid-container fluid display-top padding-horizontal-3">
    <div class="grid-x">
        <div class="large-12 display-top" id="seccionesContratacion">
            <h1>Plataforma Digital Estatal</h1>
            <h2>Sistema VI</h2>
            <h4>Informaci&oacute;n p&uacute;blica de contrataciones.</h4>
            
            <h2>Modificar contrataci&oacute;n p&uacute;blica</h2>
            
            <div class="callout secondary">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-3">
                        <ul class="menu-registro-contratacion">
                            <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                                <div style="display: flex; justify-content: space-between; align-items: center;">
                                    <button class="button expanded margin-0 ${(seccionConFoco == 1) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(seccionConFoco == 1) ? '':'frmIrSeccion1.submit()'}">Datos generales</button>
                                </div>
                            </li>
                            <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                                <div style="display: flex; justify-content: space-between; align-items: center;">
                                    <button class="button expanded margin-0 ${(seccionConFoco == 2) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(seccionConFoco == 2) ? '':'frmIrSeccion2.submit()'}">Publicador</button>
                                </div>
                            </li>
                            <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                                <div style="display: flex; justify-content: space-between; align-items: center;">
                                    <button class="button expanded margin-0 ${(seccionConFoco == 3) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(seccionConFoco == 3) ? '':'frmIrSeccion3.submit()'}">Etiquetas de entrega</button>
                                </div>
                            </li>
                            <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                                <div style="display: flex; justify-content: space-between; align-items: center;">
                                    <button class="button expanded margin-0 ${(seccionConFoco == 4) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(seccionConFoco == 4) ? '':'frmIrSeccion4.submit()'}">Partes involucradas</button>
                                </div>
                            </li>
                            <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                                <div style="display: flex; justify-content: space-between; align-items: center;">
                                    <button class="button expanded margin-0 ${(seccionConFoco == 5) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(seccionConFoco == 5) ? '':'frmIrSeccion5.submit()'}">Comprador</button>
                                </div>
                            </li>
                            <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                                <div style="display: flex; justify-content: space-between; align-items: center;">
                                    <button class="button expanded margin-0 ${(seccionConFoco == 6) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(seccionConFoco == 6) ? '':'frmIrSeccion6.submit()'}">Planeaci&oacute;n</button>
                                </div>
                            </li>
                            <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                                <div style="display: flex; justify-content: space-between; align-items: center;">
                                    <button class="button expanded margin-0 ${(seccionConFoco == 7) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(seccionConFoco == 7) ? '':'frmIrSeccion7.submit()'}">Licitaci&oacute;n</button>
                                </div>
                            </li>
                            <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                                <div style="display: flex; justify-content: space-between; align-items: center;">
                                    <button class="button expanded margin-0 ${(seccionConFoco == 8) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(seccionConFoco == 8) ? '':'frmIrSeccion8.submit()'}">Adjudicaciones</button>
                                </div>
                            </li>
                            <li class="item-principal-menu-contrataciones" style="padding: 0px;">  
                                <div style="display: flex; justify-content: space-between; align-items: center;">
                                    <button class="button expanded margin-0 ${(seccionConFoco == 9) ? 'item-menu-activo':''}" style="padding: 1.25rem 1.5rem; text-align: left;" onclick="${(seccionConFoco == 9) ? '':'frmIrSeccion9.submit()'}">Contratos</button>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="cell medium-9">
                        <c:if test="${seccionConFoco == 1}">
                            <%@include file="seccionesRegistro/frmSeccionDatosGenerales.jsp"%>
                        </c:if>
                        <c:if test="${seccionConFoco == 2}">
                            <%@include file="seccionesRegistro/frmSeccionPublisher.jsp"%>
                        </c:if>
                        <c:if test="${seccionConFoco == 3}">
                            <%@include file="seccionesRegistro/frmSeccionTag.jsp"%>
                        </c:if>
                        <c:if test="${seccionConFoco == 4}">
                            <%@include file="seccionesRegistro/frmSeccionParties.jsp"%>
                        </c:if>
                        <c:if test="${seccionConFoco == 5}">
                            <%@include file="seccionesRegistro/frmSeccionBuyer.jsp"%>
                        </c:if>
                        <c:if test="${seccionConFoco == 6}">
                            <%@include file="seccionesRegistro/frmSeccionPlanning.jsp"%>
                        </c:if>
                        <c:if test="${seccionConFoco == 7}">
                            <%@include file="seccionesRegistro/frmSeccionTender.jsp"%>
                        </c:if>
                        <c:if test="${seccionConFoco == 8}">
                            <%@include file="seccionesRegistro/frmSeccionAwards.jsp"%>
                        </c:if>
                        <c:if test="${seccionConFoco == 9}">
                            <%@include file="seccionesRegistro/frmSeccionContracts.jsp"%>
                        </c:if>
                    </div>
                </div>
            </div>
            <form name="frmIrSeccion1" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="1"/>
                <input type="hidden" name="txtBloqueConFoco" value="0"/>
            </form>
            <form name="frmIrSeccion2" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="2"/>
                <input type="hidden" name="txtBloqueConFoco" value="0"/>
            </form>
            <form name="frmIrSeccion3" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="3"/>
                <input type="hidden" name="txtBloqueConFoco" value="0"/>
            </form>
            <form name="frmIrSeccion4" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="4"/>
                <input type="hidden" name="txtBloqueConFoco" value="0"/>
            </form>
            <form name="frmIrSeccion5" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="5"/>
                <input type="hidden" name="txtBloqueConFoco" value="0"/>
            </form>
            <form name="frmIrSeccion6" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="6"/>
                <input type="hidden" name="txtBloqueConFoco" value="1"/>
            </form>
            <form name="frmIrSeccion7" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="7"/>
                <input type="hidden" name="txtBloqueConFoco" value="1"/>
            </form>
            <form name="frmIrSeccion8" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="8"/>
                <input type="hidden" name="txtBloqueConFoco" value="0"/>
            </form>
            <form name="frmIrSeccion9" method="post" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones">
                <input type="hidden" name="accion" value="mostrarPaginaEditarContratacion"/>
                <input type="hidden" name="txtID" value="${idContratacion}"/>
                <input type="hidden" name="txtSeccionConFoco" value="9"/>
                <input type="hidden" name="txtBloqueConFoco" value="0"/>
            </form>
            <hr style="margin: 1.25rem auto !important;"/>
            <a class="button expanded" onclick="frmRegresar.submit()">Regresar</a>
        </div>
    </div>
</div>
<form name="frmRegresar" id="frmRegresar" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post">
    <input type="hidden" name="accion" value="ingresarSistema"/>
</form>

<%@include file="/piePagina.jsp" %>

<c:if test="${irASeccion == true}">
    <script>
        var loc = window.location.href;
        window.location.href = loc + "#seccionesContratacion";
    </script>
</c:if>

<c:if test="${mostrarMensaje == true}">
    <script>
        var json = ${mensaje};
        swal(json.mensaje, {
            icon: json.tipo,
            buttons: {
                confirm: {
                text: "OK",
                value: true,
                visible: true,
                className: "",
                closeModal: true
                }
            }
        });
    </script>
</c:if>