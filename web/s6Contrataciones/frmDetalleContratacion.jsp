<%--
    Autor: Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@include file="/encabezado.jsp" %>

<%@include file="/migas.jsp" %>

<div class="grid-container fluid display-top">
    <div class="grid-x">
        <div class="large-12 display-top">
            <div class="grid-x grid-margin-x" style="justify-content: center;">
                <div class="large-8 cell callout">
                    <div class="grid-x grid-margin-x">
                        <div class="cell medium-4">
                            <img src="framework/img/svg/s6.svg" />
                        </div>
                        <div class="cell medium-8">
                            <h5 class="separator-left">Ciclo: ${(contratacion.cycle != null) ? contratacion.cycle : "DATO NO PROPORCIONADO"}</h5>
                            <h5>Open Contracting ID:  ${(contratacion.ocid != null) ? contratacion.ocid : "DATO NO PROPORCIONADO"}</h5>
                            <h5>ID de Entrega:  ${(contratacion.id != null) ? contratacion.id : "DATO NO PROPORCIONADO"}</h5>
                            <h5>Fecha de entrega:  ${((contratacion.date != null) && (contratacion.date != "")) ? formateadorDeFechas.obtenerFormatoFechaDescriptivaDeFormatoISO8601(contratacion.date).toLowerCase() : "DATO NO PROPORCIONADO"}</h5>
                            <h5>Tipo de inicio:  ${(contratacion.initiationType != null) ? contratacion.initiationType : "DATO NO PROPORCIONADO"}</h5>
                            <h5>Idioma de la entrega:  ${(contratacion.language != null) ? contratacion.language : "DATO NO PROPORCIONADO"}</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="callout secondary">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-3">
                        <ul class="vertical tabs" data-tabs="data-tabs" id="informacion-tabs">
                            <li class="tabs-title is-active">
                                <a href="#panela1v" aria-selected="true">Publicador</a>
                            </li>
                            <li class="tabs-title">
                                <a href="#panela2v">Etiqueta de entrega</a>
                            </li>
                            <li class="tabs-title">
                                <a href="#panela3v">Partes involucradas</a>
                            </li>
                            <li class="tabs-title">
                                <a href="#panela4v">Comprador</a>
                            </li>
                            <li class="tabs-title">
                                <a href="#panela5v">Planeaci&oacute;n</a>
                            </li>
                            <li class="tabs-title">
                                <a href="#panela6v">Licitaci&oacute;n</a>
                            </li>
                            <li class="tabs-title">
                                <a href="#panela7v">Adjudicaci&oacute;n</a>
                            </li>
                            <li class="tabs-title">
                                <a href="#panela8v">Contrato</a>
                            </li>
                        </ul>
                    </div>
                    <div class="cell medium-9">
                        <div class="tabs-content vertical" data-tabs-content="informacion-tabs">
                            <%@include file="seccionesDetalle/frmSeccionPublisher.jsp"%>
                            <%@include file="seccionesDetalle/frmSeccionTag.jsp"%>
                            <%@include file="seccionesDetalle/frmSeccionParties.jsp"%>
                            <%@include file="seccionesDetalle/frmSeccionBuyer.jsp"%>
                            <%@include file="seccionesDetalle/frmSeccionPlanning.jsp"%>
                            <%@include file="seccionesDetalle/frmSeccionTender.jsp"%>
                            <%@include file="seccionesDetalle/frmSeccionAwards.jsp"%>
                            <%@include file="seccionesDetalle/frmSeccionContracts.jsp"%>
                        </div>
                    </div>
                </div>
            </div>
            <hr style="margin: 1.25rem auto !important;"/>
            <a class="button expanded" onclick="frmRegresar.submit()">Regresar</a>
        </div>
    </div>
</div>
<form name="frmRegresar" action="/sistemaCaptura${initParam.versionIntranet}/s_contrataciones" method="post">
    <input type="hidden" name="accion" value="consultarContrataciones"/>
    <input type="hidden" name="cmbPaginacion" value="${filtros.registrosMostrar}"/>
    <input type="hidden" name="cmbOrdenacion" value="${filtros.orden}"/>
    <input type="hidden" name="txtNumeroPagina" value="${filtros.numeroPagina}"/>
    <input type="hidden" name="cmbCiclo" value="${filtros.ciclo}"/>
    <input type="hidden" name="txtPublicador" value="${filtros.publicador}"/>
    <input type="hidden" name="txtDependencia" value="${filtros.dependencia}"/>
</form>
    
<%@include file="/piePagina.jsp" %>